#!/bin/bash

#脚本功能：一键部署jmodelica项目，只需修改$WORKDIR,$PROGRAME_SRC配置即可。
#运行方式：sudo sh install-jmodelica.sh
#
#开始执行此脚本前，请修改PROGRAME_SRC，WORKDIR
#WORKDIR 目录下文件
#/home/bright/workdir
#   ├── jmodelica-main
#   ├── install-jmodelica.sh
#
#1, install ipopt
#2, install python2
#3, install jmodelica

PROJECT_NAME="jmodelica"
WORKDIR="root@admired-spin-2:~/workspace"    #根据本机情况修改
PROGRAME_SRC="$WORKDIR/$PROJECT_NAME"  #根据本机情况修改
DESTDIR="$WORKDIR/bin/$PROJECT_NAME"
APPDIR="$DESTDIR/bdmcore"

#pre
sudo apt-get install automake gcc g++ make cmake 

#system config
bash -c "cat > /etc/apt/sources.list" <<EOF
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ focal-security main restricted universe multiverse
deb http://cn.archive.ubuntu.com/ubuntu/ focal main restricted
deb http://cn.archive.ubuntu.com/ubuntu/ focal universe
deb http://cn.archive.ubuntu.com/ubuntu/ focal multiverse
deb http://security.ubuntu.com/ubuntu focal-security main restricted
deb http://security.ubuntu.com/ubuntu focal-security universe
deb http://security.ubuntu.com/ubuntu focal-security multiverse
deb http://cn.archive.ubuntu.com/ubuntu/ trusty main restricted
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty main restricted
deb http://cn.archive.ubuntu.com/ubuntu/ trusty-updates main restricted
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty-updates main restricted
deb http://cn.archive.ubuntu.com/ubuntu/ trusty universe
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty universe
deb http://cn.archive.ubuntu.com/ubuntu/ trusty-updates universe
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty-updates universe
deb http://cn.archive.ubuntu.com/ubuntu/ trusty multiverse
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty multiverse
deb http://cn.archive.ubuntu.com/ubuntu/ trusty-updates multiverse
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty-updates multiverse
deb http://cn.archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse
deb-src http://cn.archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse
deb http://security.ubuntu.com/ubuntu trusty-security main restricted
deb-src http://security.ubuntu.com/ubuntu trusty-security main restricted
deb http://security.ubuntu.com/ubuntu trusty-security universe
deb-src http://security.ubuntu.com/ubuntu trusty-security universe
deb http://security.ubuntu.com/ubuntu trusty-security multiverse
deb-src http://security.ubuntu.com/ubuntu trusty-security multiverse
deb http://archive.ubuntu.com/ubuntu focal universe
EOF
#执行命令更新，忽略错误警告
apt update -y && apt upgrade -y


apt install -y openjdk-8-jdk g++ python2-minimal libpython2-stdlib python2 libboost-dev cmake gfortran libblas-dev build-essential ant subversion zlib1g-dev swig3.0 jcc curl libpython2-dev python-tk ipython python-jpype 

#curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
curl http://pkg.pan-support.com/download/python/pip/2.7/get-pip.py --output get-pip.py && python2 get-pip.py 


sudo bash -c "cat > /etc/profile.d/java.sh" <<EOF
export PATH=\$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin
export LD_LIBRARY_PATH=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/:/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
export JCC_JDK=\$JAVA_HOME
export JPYPE_JVM=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/amd64/server/libjvm.so
EOF
chmod +x /etc/profile.d/java.sh
. /etc/profile.d/java.sh

echo $JAVA_HOME $JCC_JDK
pip2 install numpy scipy matplotlib lxml nose flask ipython cython jcc


#install ipopt
echo start install ipopt
cd $WORKDIR
rm -rf Ipopt-3.12.2 
tar -zxf $PROGRAME_SRC/ThirdParty/Ipopt-3.12.2.tgz || echo Ipopt tar package not exists 
sed -i /BuildTools/'s#wgetcmd http.*#wgetcmd http://pkg.pan-support.com/download/\$coinblas;#g' Ipopt-3.12.2/ThirdParty/Blas/get.Blas
sed -i /BuildTools/'s#wgetcmd http.*#wgetcmd http://pkg.pan-support.com/download/lapack-\${lapack_ver}.tgz ;#g'  Ipopt-3.12.2/ThirdParty/Lapack/get.Lapack 
sed -i /wgetcmd/'s#wgetcmd http.*#wgetcmd http://pkg.pan-support.com/download/MUMPS_\${mumps_ver}.tar.gz ;#g'  Ipopt-3.12.2/ThirdParty/Mumps/get.Mumps 
sed -i  /wgetcmd/'s#wgetcmd http.*#wgetcmd http://pkg.pan-support.com/download/metis-4.0.3.tar.gz ;#g'  Ipopt-3.12.2/ThirdParty/Metis/get.Metis 


cd Ipopt-3.12.2/ThirdParty
cd Blas && ./get.Blas && cd ..|| exit 1
cd Lapack && ./get.Lapack && cd .. || exit 1
cd Mumps && ./get.Mumps && cd ..|| exit 1
cd Metis && ./get.Metis  && cd ..|| exit 1
cd .. && mkdir build && cd build && \
	../configure --prefix=$DESTDIR/ipopt && \
	make && \
	make install 

# install jmodelica
cd $PROGRAME_SRC/trunk && rm -rf build &&  mkdir build && cd build && \
	../configure --prefix=${DESTDIR}/jmodelica --with-ipopt=${DESTDIR}/ipopt && \
	make && make install && chmod 777 -R ${DESTDIR}/jmodelica
