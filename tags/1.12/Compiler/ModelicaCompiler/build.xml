<!--
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!-- Targets for working from terminal window:
       build (default) - generates java files and compiles them
       test            - runs junit test cases
       clean           - removes all generated files and class files
     Targets for working from Eclipse:
       gen             - generates java files
       gen-test        - generates junit test cases from test files
       cleanGen        - removes all generated files and their class files
 -->

<project name="ModelicaCompiler" default="build" basedir=".">
	
	<property name="modules" 
		value="ModelicaFrontEnd,GenericCodeGen,TestFramework,FmiXMLCodeGen,ModelicaFMUXBackEnd,ModelicaCBackEnd,ModelicaCompiler" />
	
	<property name="java_version" value="1.6"/>
	<property name="java_compiler" value="javac${java_version}"/>

    <property name="modelica_front_end_path" value="../ModelicaFrontEnd" />

	<property name="ast_package" value="org.jmodelica.modelica.compiler" />

    <property name="base_output" value="src/java-generated" />
    <property name="common_output" value="${base_output}/org/jmodelica/modelica" />
    <property name="ast_output" value="${common_output}/compiler" />
    <property name="parser_output" value="${common_output}/parser" />

	<property name="jar_output_dir" value="bin" />
	<property name="jastadd_dir" value="../../ThirdParty/JastAdd" />
	<property name="junit_dir" value="../../ThirdParty/Junit" />
	<property name="jflex_dir" value="../../ThirdParty/JFlex/jflex-1.4.3" />
	<property name="beaver_dir" value="../../ThirdParty/Beaver/beaver-0.9.6.1" />
	<property name="ant-contrib_dir" value="../../ThirdParty/Ant-Contrib/ant-contrib-1.0b3" />
	<property name="jmodelica_parser" value="${modelica_front_end_path}/src/parser" />

	<property name="test_junit_dir" value="src/test/junit" />
	<property name="test_junit-gen_dir" value="src/test/junit-generated" />


	<!-- for ant-contrib (foreach, etc) -->
	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="${ant-contrib_dir}/target/ant-contrib.jar" />
		</classpath>
	</taskdef>
	<!-- "jastadd" is an ant task class in jastadd2.jar -->
	<taskdef classname="org.jastadd.JastAddTask" name="jastadd" classpath="${jastadd_dir}/jastadd2.jar" />
	<!-- "jflex" is an ant task class for the scanner generator in JFlex.jar -->
	<taskdef name="jflex" classname="JFlex.anttask.JFlexTask" classpath="${jflex_dir}/lib/JFlex.jar" />
	<!-- "beaver" is an ant task class for the parser generator in beaver.jar -->
	<taskdef name="beaver" classname="beaver.comp.run.AntTask" classpath="${beaver_dir}/lib/beaver.jar" />
    
    <var name="index" value="0"/>
    <for list="${modules}" param="module">
        <sequential>
            <math result="index" operand1="${index}" operation="+" operand2="1" datatype="int" />
            <fileset dir=".." id="jastadd_files_temp">
                <include name="@{module}/**/*.ast" />
                <include name="@{module}/**/*.jadd" />
                <include name="@{module}/**/*.jrag" />
            </fileset>
            <if>
                <equals arg1="${index}" arg2="1" />
                <then>
                    <property name="jastadd_files_list_${index}" value="${toString:jastadd_files_temp}" />
                </then>
                <else>
                    <property name="jastadd_files_list_${index}" value="${last};${toString:jastadd_files_temp}" />
                </else>
            </if>
            <propertycopy name="last" from="jastadd_files_list_${index}" override="true" />
        </sequential>
    </for>
    <propertyregex property="jastadd_files_temp" input="${last}" regexp=";+" replace=" ../" />
	<property name="jastadd_files" value="../${jastadd_files_temp}" />


	<!-- TARGET build -->
	<target name="build" depends="compile-ast">
		<mkdir dir="${jar_output_dir}" />
		<jar destfile="${jar_output_dir}/ModelicaCompiler.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/modelica/compiler/*.class" />
				<include name="org/jmodelica/modelica/parser/*.class" />
			</fileset>
		</jar>
		<jar destfile="${jar_output_dir}/util.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/util/*.class" />
				<include name="org/jmodelica/util/formattedPrint/*.class" />
				<include name="org/jmodelica/util/logging/*.class" />
                <include name="org/jmodelica/util/exceptions/*.class" />
			</fileset>
		</jar>
		<jar destfile="${jar_output_dir}/graphs.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/graphs/*.class" />
			</fileset>
		</jar>
	</target>

	<target name="compile-ast" depends="gen">
		<mkdir dir="bin" />
		<javac classpath="${beaver_dir}/lib/beaver.jar" compiler="${java_compiler}" source="${java_version}" 
				target="${java_version}" debug="true" destdir="bin" includeantruntime="false">
			<src path="${base_output}" />
			<src path="${modelica_front_end_path}/src/java" />
			<include name="**/*.java" />
			<exclude name="**/*junit*/**" />
			<exclude name="**/*.aj" />
		</javac>
	</target>
    
    <!-- Meta target for generating all java files -->
    <target name="gen" depends="patch,parser,scanner" />

	<target name="compile-patcher">
		<mkdir dir="bin" />
		<javac classpath="${beaver_dir}/lib/beaver.jar" compiler="${java_compiler}" source="${java_version}" 
			target="${java_version}" debug="true" destdir="bin" includeantruntime="false">
			<src path="${modelica_front_end_path}/src/java" />
			<include name="**/GeneratedFilePatcher.java" />
		</javac>
	</target>

    <target name="patch" depends="ast,compile-patcher">
        <java classpath="bin:${beaver_dir}/lib/beaver-rt.jar" 
        	    classname="org.jmodelica.util.GeneratedFilePatcher" failonerror="true">
            <arg line="${ast_output}" />
        </java>
    </target>

	<!-- TARGET gen -->
	<target name="ast">
		<!-- create a directory for the generated files -->
		<mkdir dir="${base_output}" />
		<!-- run jastadd to generate AST files -->
        <java classname="org.jastadd.JastAdd" classpath="${jastadd_dir}/jastadd2.jar" failonerror="true">
            <arg value="--package=${ast_package}" />
            <arg value="--o=${base_output}" />
            <arg value="--beaver" />
            <arg value="--rewrite" />
            <arg value="--cacheCycle" />
            <arg value="--noComponentCheck" />
            <arg value="--noVisitCheck" />
            <arg value="--lazyMaps" />
            <arg value="--deterministic" />
            <arg line="${jastadd_files}" />
        </java>
		<!--
		<jastadd package="${ast_package}" beaver="true" rewrite="true" outdir="${base_output}" 
				NoCacheCycle="false" ComponentCheck="false" visitcheck="false" LazyMaps="true" 
				Deterministic="true" NoStatic="false" Debug="false">
            <fileset dir=".." files="${jastadd_files}" />
		</jastadd>
        -->
	</target>

    <target name="check_uptodate">
        <uptodate property="uptodate_parser" targetfile="${parser_output}/ModelicaParser.java">
            <srcfiles dir="${jmodelica_parser}">
                <include name="Modelica_header.parser"/>
                <include name="Modelica.parser"/>
                <include name="beaver.input"/>
            </srcfiles>
        </uptodate>
    	
        <uptodate property="uptodate_modelica_scanner" targetfile="${parser_output}/ModelicaScanner.java">
            <srcfiles dir="${jmodelica_parser}" includes="Modelica*.flex"/>
        </uptodate>

        <uptodate property="uptodate_flatmodelica_scanner" targetfile="${parser_output}/FlatModelicaScanner.java">
            <srcfiles dir="${jmodelica_parser}" includes="FlatModelica*.flex"/>
        </uptodate>

        <condition property="uptodate_scanner">
            <and>
                <istrue value="${uptodate_modelica_scanner}"/>
                <istrue value="${uptodate_flatmodelica_scanner}"/>
            </and>
        </condition>
    </target>

    <target name="parser" depends="check_uptodate" unless="uptodate_parser">
        <!-- generate the parser phase 1, translating .lalr to .beaver -->
        <concat destfile="${parser_output}/Modelica_cat.parser" force="no">
            <filelist dir="${jmodelica_parser}" files="Modelica_header.parser" />
            <filelist dir="${jmodelica_parser}" files="Modelica.parser" />
        </concat>
        <java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
            <arg line="${parser_output}/Modelica_cat.parser ${parser_output}/ModelicaParser_raw.beaver" />
        </java>
        <concat destfile="${parser_output}/ModelicaParser.beaver" force="no">
            <filelist dir="${jmodelica_parser}" files="beaver.input" />
            <filelist dir="${parser_output}" files="ModelicaParser_raw.beaver" />
        </concat>
        <!-- generate the parser phase 2, translating .beaver to .java -->
        <beaver file="${parser_output}/ModelicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes" />

        <!-- clean up intermediate files -->
        <delete deleteonexit="true">
            <fileset dir="${parser_output}">
                <include name="*.parser" />
                <include name="*.beaver" />
            </fileset>
        </delete>
    </target>

    <target name="scanner" depends="check_uptodate" unless="uptodate_scanner">
        <!-- Modelica -->
        <concat destfile="${parser_output}/Modelica_cat.flex" force="no">
            <filelist dir="${jmodelica_parser}" files="Modelica_header.flex" />
            <filelist dir="${jmodelica_parser}" files="Modelica.flex" />
        </concat>
        <jflex file="${parser_output}/Modelica_cat.flex" outdir="${parser_output}" nobak="yes" />

        <!-- FlatModelica -->
        <concat destfile="${parser_output}/FlatModelica_cat.flex" force="no">
            <filelist dir="." files="${jmodelica_parser}/FlatModelica_header.flex" />
            <filelist dir="${jmodelica_parser}" files="FlatModelica.flex" />
        </concat>
        <jflex file="${parser_output}/FlatModelica_cat.flex" outdir="${parser_output}" nobak="yes" />

        <!-- clean up intermediate files -->
        <delete deleteonexit="true">
            <fileset dir="${parser_output}">
                 <include name="*.flex" />
            </fileset>
        </delete>
    </target>

	<!-- TARGET clean -->
	<target name="clean" depends="cleanGen">
		<!-- Delete all classfiles in dir and recursively in subdirectories -->
		<delete dir="bin" />
		<delete dir="doc" />
		<delete dir="${test_junit-gen_dir}/org/jmodelica/test/modelica/junitgenerated" />
	</target>

	<!-- TARGET cleanGen -->
	<target name="cleanGen">
		<!-- Delete the directory containing generated files and their class files -->
		<delete dir="${base_output}" />
		<!--<delete dir="bin"/>-->
	</target>

	<!-- TARGET gen-test -->
	<target name="gen-test" depends="build">
		<mkdir dir="${test_junit-gen_dir}/org/jmodelica/test/modelica/junitgenerated" />
		<for param="module" list="${modules}">
            <sequential>
        		<for param="gen-test.path">
        			<path>
        				<fileset dir="../@{module}/src/test">
        					<include name="*.mo" />
        				</fileset>
        			</path>
        			<sequential>
        				<java classname="org.jmodelica.modelica.compiler.JunitGenerator" 
        						classpath=".:bin/ModelicaCompiler.jar:bin/util.jar:bin/graphs.jar:${beaver_dir}/lib/beaver.jar" 
        						fork="true" failonerror="true" dir="." maxmemory="768M">
        					<arg value="@{gen-test.path}" />
        					<arg value="${test_junit-gen_dir}/org/jmodelica/test/modelica/junitgenerated" />
        				</java>
        			</sequential>
        		</for>
            </sequential>
	    </for>
		<javac compiler="${java_compiler}" source="${java_version}" target="${java_version}" debug="true" 
				destdir="bin" classpath="${junit_dir}/junit-4.5.jar" includeantruntime="false">
			<src path="${test_junit-gen_dir}/" />
			<include name="org/jmodelica/test/modelica/junitgenerated/*.java" />
			<exclude name="**/*.aj" />
		</javac>
		<jar destfile="bin/junit-tests.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/test/modelica/junitgenerated/*.class" />
			</fileset>
		</jar>
	</target>

	<!-- TARGET test -->
	<target name="test" depends="gen-test">
		<!-- Run all tests in dir by using the TestAll java program -->
		<mkdir dir="doc/junit-reports" />
		<junit printsummary="yes" fork="true" maxmemory="512M">
			<classpath>
				<fileset dir="${junit_dir}">
					<include name="junit-4.5.jar" />
				</fileset>
				<fileset dir="bin">
					<include name="graphs.jar" />
					<include name="util.jar" />
					<include name="ModelicaCompiler.jar" />
					<include name="junit-tests.jar" />
				</fileset>
				<fileset dir="${beaver_dir}/lib">
					<include name="beaver.jar" />
				</fileset>
			</classpath>
			<formatter type="xml" />
			<batchtest fork="yes" todir="doc/junit-reports">
				<fileset dir="${test_junit-gen_dir}/">
					<include name="org/jmodelica/test/modelica/junitgenerated/*.java" />
				</fileset>
			</batchtest>
		</junit>
		<junitreport todir="doc/junit-reports">
			<fileset dir="doc/junit-reports" includes="TEST-*.xml" />
			<report todir="doc/junit-reports" />
		</junitreport>
	</target>

</project>
