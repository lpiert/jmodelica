<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="ch_intro" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Introduction</title>

  <section>
    <title>About JModelica.org</title>

    <para>JModelica.org is an extensible Modelica-based open source platform
    for optimization, simulation and analysis of complex dynamic systems. The
    main objective of the project is to create an industrially viable open
    source platform for optimization of Modelica models, while offering a
    flexible platform serving as a virtual lab for algorithm development and
    research. JModelica.org is intended to provide a platform for technology
    transfer where industrially relevant problems can inspire new research and
    where state of the art algorithms can be propagated form academia into
    industrial use. JModelica.org is a result of research at the Department of
    Automatic Control, Lund University, <citation>Jak2007</citation> and is
    now maintained and developed by Modelon AB in collaboration with
    academia.</para>
  </section>

  <section>
    <title>Mission Statement</title>

    <para>To offer a community-based, free, open source, accessible, user and
    application-oriented Modelica environment for optimization and simulation
    of complex dynamic systems, built on well-recognized technology and
    supporting major platforms.</para>
  </section>

  <section xml:id="intr_sec_tech">
    <title>Technology</title>

    <para>JModelica.org relies on the modeling language <link
    xlink:href="http://www.modelica.org">Modelica</link>. Modelica targets
    modeling of complex heterogeneous physical systems, and is becoming a de
    facto standard for dynamic model development and exchange. There are
    numerous model libraries for Modelica, both free and commercial, including
    the freely available Modelica Standard Library (MSL).</para>

    <para>A unique feature of JModelica.org is the support for the extension
    Optimica. Optimica enables users to conveniently formulate optimization
    problems based on Modelica models using simple but powerful constructs for
    encoding of optimization interval, cost function and constraints.</para>

    <para>The JModelica.org compilers are developed in the compiler
    construction framework <link
    xlink:href="http://jastadd.org">JastAdd</link>. JastAdd is based on a
    number of different concepts, including object-orientation,
    aspect-orientation and reference attributed grammars. Compilers developed
    in JastAdd are specified in terms of declarative attributes and equations
    which together forms an executable specification of the language
    semantics. In addition, JastAdd targets extensible compiler development
    which makes it easy to experiment with language extensions.</para>

    <para>For user interaction JModelica.org relies on the <link
    xlink:href="http://www.python.org/">Python</link> language. Python offers
    an interactive environment suitable for scripting, development of custom
    applications and prototype algorithm integration. The Python packages
    Numpy and Scipy provide support for numerical computation, including
    matrix and vector operations, basic linear algebra and plotting. The
    JModelica.org compilers as well as the model executables/dlls integrate
    seamlessly with Python and Numpy.</para>

    <para>JModelica.org offers strong support for the <link
    xlink:href="https://www.fmi-standard.org/">Functional Mock-up
    Interface</link> (FMI) standard. FMI specifies a format for exchange of
    compiled dynamic models and it is supported by a large number of modeling
    and simulation tools, including established Modelica tools such as Dymola,
    OpenModelica, and SimulationX. FMI defines a model execution interface
    consisting of a set of C-function signatures for handling the
    communication between the model and a simulation environment. Models are
    presented as ODEs with time, state and step events. FMI also specifies
    that all information related to a model, except the equations, should be
    stored in an XML formated text-file. The format is specified in the
    standard and specifically contains information about the variables, names,
    identifiers, types and start attributes. A model is distributed in a
    zip-file with the extension '.fmu', these zip-files containing the models
    are called FMUs (Functional Mock-up Units). FMI version 1.0 specifies two
    types of FMUs, either Model Exchange or Co-Simulation. The difference
    between them is that in a Co-Simulation FMU, the integrator for solving
    the system is contained in the model while in an Model Exchange FMU, an
    external integrator is needed to solve the system. The JModelica.org
    compiler supports export of FMUs and FMUs can be imported into Python
    using the Python packages included in the platform.</para>
  </section>

  <section xml:id="intro_sec_architecture">
    <title>Architecture</title>

    <para><figure>
        <title>JModelica.org platform architecture.</title>

        <mediaobject>
          <imageobject>
            <imagedata fileref="images/arch_0.preview.png" scalefit="1"
                       width="60%"/>
          </imageobject>
        </mediaobject>
      </figure>The JModelica.org platform consists of a number of different
    parts:</para>

    <itemizedlist>
      <listitem>
        <para>The compiler front-ends (one for Modelica and one for
        Modelica/Optimica) transforms Modelica and Optimica code into a flat
        model representation. The compilers also check the correctness of
        model descriptions and reports errors.</para>
      </listitem>

      <listitem>
        <para>The compiler back-ends generates C code and XML code for
        Modelica and Optimica. The C code contains the model equations, cost
        functions and constraints whereas the XML code contains model meta
        data such as variable names and parameter values. Export of Functional
        Mock-up Units (FMUs) is supported. There is also the option to export
        flattened Modelica models, including equations, in XML format. </para>
      </listitem>

      <listitem>
        <para>The JModelica.org runtime library is written in C and contains
        supporting functions needed to compile the generated model C code.
        Also, the runtime library contains an integration with CppAD, a tool
        for computation of high accuracy derivatives by means of automatic
        differentiation to provide derivatives for optimization algorithm. The
        runtime system also contains the functions provided in the FMI
        API.</para>
      </listitem>

      <listitem>
        <para>Currently, JModelica.org features four different algorithms for
        solving dynamic optimization problems. There are three different
        algorithms based on direct collocation, which rely on the solver IPOPT
        for obtaining a solution to the resulting NLP. The default algorithm
        is encoded in C and relies on CppAD for computing the NLP derivatives.
        The other two algorithms are developed in Python and rely on CasADi
        for computing derivatives. There is also a derivative free
        optimization algorithm for model calibration based on measurement data
        that is applicable to FMUs.</para>
      </listitem>

      <listitem>
        <para>JModelica.org uses Python for scripting. For this purpose,
        JModelica.org provides a number of different Python packages. The
        Assimulo package provides integration with state of the art DAE and
        ODE solvers (including the SUNDIALS suite), PyFMI provides FMU import,
        whereas PyModelica interacts with the JModelica.org compilers.
        Finally, PyJMI contains drivers for the optimization algorithms. All
        packages are available as part of JModelica.org, and Assimulo and
        PyFMI are also available as stand alone Python packages from <link
        xlink:href="http://www.assimulo.org">www.assimulo.org</link> and <link
        xlink:href="http://www.pyfmi.org">www.pyfmi.org</link>.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section>
    <title>Extensibility</title>

    <para>The JModelica.org platform is extensible in a number of different
    ways:</para>

    <itemizedlist>
      <listitem>
        <para>The JModelica.org platform supports export and import of FMUs,
        which are compliant with the FMI standard. In addition, JModelica.org
        features a C interface for efficient evaluation of model equations,
        the cost function and the constraints: the JModelica Model Interface
        (JMI). JMI also contains functions for evaluation of derivatives and
        sparsity and is intended to offer a convenient interface for
        integration of numerical algorithms. FMI is the default format for
        simulation, whereas JMI is the default interface for
        optimization.</para>
      </listitem>

      <listitem>
        <para>In addition to the FMI and JMI interfaces, JModelica.org
        supports export of flat Modelica models in XML format. This format is
        based on FMI, and is suitable for integration with symbolic algorithms
        that can exploit access to the equations in symbolic form.</para>
      </listitem>

      <listitem>
        <para>JastAdd produces compilers encoded in pure Java. As a result,
        the JModelica.org compilers are easily embedded in other applications
        aspiring to support Modelica and Optimica. In particular, a Java API
        for accessing the flat model representation and an extensible
        template-based code generation framework is offered.</para>
      </listitem>

      <listitem>
        <para>The JModelica.org compilers are developed using the compiler
        construction framework JastAdd. JastAdd features extensible compiler
        construction, both at the language level and at the implementation
        level. This feature is explored in JModelica.org where the Optimica
        compiler is implemented as a fully modular extension of the core
        Modelica compiler. The JModelica.org platform is a suitable choice for
        experimental language design and research.</para>
      </listitem>
    </itemizedlist>

    <para>An overview of the JModelica.org platform is given
    <citation>Jak2010</citation></para>
  </section>
</chapter>
