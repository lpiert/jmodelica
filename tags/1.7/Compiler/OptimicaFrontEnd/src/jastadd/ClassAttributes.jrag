/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ClassAttributes {

	syn lazy List OptClassDecl.getClassAttributeList() {

		//log.debug("Program.getPredefinedTypeList()");
		List l = new List();

	    // Build a string with a Modelica class corresponding to Real
		String builtInDef = "model ClassAttributes\n";
		builtInDef += "Real objective = 1;\n"; 
		builtInDef += "Real objectiveIntegrand = 1;\n"; 
		builtInDef += "parameter Real startTime=0;\n";
		builtInDef += "parameter Real finalTime=1;\n";
		builtInDef += "parameter Boolean static=false;\n";
    	builtInDef += "end ClassAttributes;\n";
   
   		//java.io.StringReader builtInDefReader = new java.io.StringReader(builtInDef);
 	  	//ModelicaParser parser = new ModelicaParser();
  		PrimitiveClassDecl pcd=null;
  
   		try {
   		
   			ParserHandler ph = new ParserHandler();
   			SourceRoot sr = ph.parseString(builtInDef,"");
 			Program p = sr.getProgram();
   			   			
  			FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(0));	

			for (int i=0;i<cd.getNumComponentDecl();i++) { 		
 				l.add(cd.getComponentDecl(i));	
			}	
 		
 		} catch(Exception e){e.printStackTrace(); System.exit(0);}
			
		//log.debug("Program.getPredefinedTypeList(): "+l.getNumChild());
		
		return l;
	}

	eq InstTimedVariable.getName().kind() = Kind.COMPONENT_ACCESS;
	
	syn lazy List InstOptClassDecl.getInstClassAttributeList() {
		List l = new List();
		for (ComponentDecl cd : ((OptClassDecl)getClassDecl()).getClassAttributes()) {
    		InstAccess name = dynamicClassName(cd.getClassName().newInstAccess());
			l.add(name.myInstClassDecl().newInstComponentDecl(cd));
		}
		return l;
	}

 	eq InstOptClassDecl.localInstModifications() {
		ArrayList<InstModification> l = new ArrayList<InstModification>();
		// Add modifications from the class modification list
		if (hasInstClassModification())
			l.add(getInstClassModification());			
		return l;
	}
	
	eq InstOptClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) {
		
		HashSet set = new HashSet();
		//log.debug("InstOptClassDecl.getInstClassModification().lookupInstComponentInInstElement: " + name);
		for (int i=0;i<getNumInstClassAttribute();i++) {
			if (getInstClassAttribute(i).matchInstComponentDecl(name))
				set.add(getInstClassAttribute(i));
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}

	// This equation is needed in order to ensure correct lookup of accesses
	// in the class attribute construct.
	eq InstOptClassDecl.getInstClassModification().myInstNode() =
		getInstClassAttribute(0);
	
	eq InstOptClassDecl.getInstClassModification().lookupInstComponent(String name) =
		memberInstComponent(name);
	
	eq InstOptClassDecl.memberInstComponent(String name) {
		log.debug("InstOptClassDecl.memberInstComponent: " + name);
		HashSet set = new HashSet(4);
		set.addAll(super.memberInstComponent(name));
		if (set.size()>0) {
			log.debug("Found component amongst the declared ones!");
			return set;
		}
		for (int i=0;i<getNumInstClassAttribute();i++) {
			if (getInstClassAttribute(i).matchInstComponentDecl(name))
				set.add(getInstClassAttribute(i));
		}
		if (set.size()>0) {
			//log.debug("InstOptClassDecl.memberInstComponent: " + name + " found");
			return set;
		} else {
			//log.debug("InstOptClassDecl.memberInstComponent: " + name + " not found");
			return emptyHashSet();	
		}
	}

	
}