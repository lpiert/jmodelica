
/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file CGenerator.java
*  \brief CGenerator class.
*/


import java.io.*;

/**
 * OptimicaCGenerator extends the functionality of CGenerator to also
 * include C code generation of Optimica quantities.
 *
 */
public class OptimicaCGenerator extends CADGenerator {

	class OptTag_C_numFreeDependentParameters extends OptTag {
		
		public OptTag_C_numFreeDependentParameters(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_free_dependent_real_parameters","Number of free dependent parameters",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numFreeDependentRealParameters());
		}	
	}

	class OptTag_C_numPointwisePenalty extends OptTag {
		
		public OptTag_C_numPointwisePenalty(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_j","Number of pointwise penalty functions",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.objectiveExp()!=null? "1": "0");
		}	
	}

	class OptTag_C_numLagrangeIntegrand extends OptTag {
		
		public OptTag_C_numLagrangeIntegrand(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_l","Number of Lagrange integrands",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.objectiveIntegrandExp()!=null? "1": "0");
		}	
	}
	
	class OptTag_C_numPathEqConstraints extends OptTag {
		
		public OptTag_C_numPathEqConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_ceq","Number of path equality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numPathEqConstraints());
		}	
	}

	class OptTag_C_numPathIneqConstraints extends OptTag {
		
		public OptTag_C_numPathIneqConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_cineq","Number of path inequality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numPathLeqConstraints() + " + " + 
					foptclass.numPathGeqConstraints());
		}	
	}

	class OptTag_C_numPointEqConstraints extends OptTag {
		
		public OptTag_C_numPointEqConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_heq","Number of point equality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numPointEqConstraints());
		}	
	}

	class OptTag_C_numPointIneqConstraints extends OptTag {
		
		public OptTag_C_numPointIneqConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_hineq","Number of point inequality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numPointLeqConstraints() + " + " + 
					foptclass.numPointGeqConstraints());
		}	
	}

	class OptTag_C_numTimePoints extends OptTag {
		
		public OptTag_C_numTimePoints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("n_tp","Number of point time points",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			genPrinter.print(foptclass.numTimePoints());
		}	
	}
	
	class OptTag_C_pointVariableAliases extends OptTag {
		
		public OptTag_C_pointVariableAliases(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_point_variable_aliases","C: macros for C point variable aliases",
			  myGenerator,foptclass);
		}
	
		private void generateVar(PrintStream genPrinter, FVariable fv, String offset, int index) {
			genPrinter.print("#define ");
			genPrinter.print(fv.name_C());
			genPrinter.print("_p_(j) ((*(jmi->z))[jmi->offs_");
			genPrinter.print(offset);
			genPrinter.print("+j*(jmi->n_real_dx + jmi->n_real_x + jmi->n_real_u + jmi->n_real_w) +");
			genPrinter.print(index);
			genPrinter.print("])\n");
		}

		private void generateVarList(PrintStream genPrinter, Iterable<? extends FVariable> list, String offset) {
			int index = 0;
			for (FVariable fv : list)
				generateVar(genPrinter, fv, offset, index++);
		}

		public void generate(PrintStream genPrinter) {
			generateVarList(genPrinter, foptclass.derivativeVariables(),         "real_dx_p");
			generateVarList(genPrinter, foptclass.differentiatedRealVariables(), "real_x_p");
			generateVarList(genPrinter, foptclass.realInputs(),                  "real_u_p");
			generateVarList(genPrinter, foptclass.algebraicRealVariables(),      "real_w_p");
		}
	}

	class OptTag_C_FreeDependentParameterResiduals extends OptTag {
		
		public OptTag_C_FreeDependentParameterResiduals(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_dependent_free_parameter_residuals","C: dependent parameter residuals",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FAbstractEquation e : foptclass.freeDependentParameterEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FAbstractEquation e : foptclass.freeDependentParameterEquations()) {
				e.genResidual_C(i, INDENT, genPrinter);				
				i += e.numScalarEquations();
			}			
		}
	}
	
	class OptTag_C_CostFunction extends OptTag {
		
		public OptTag_C_CostFunction(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_cost_function","C: cost function",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			FExp obj = foptclass.objectiveExp();
			if (obj != null) {
				obj.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				genPrinter.print(INDENT + "(*res)[0] = ");
				obj.prettyPrint_C(genPrinter,"");
				genPrinter.print(";\n");
			}
		}
	}

	class OptTag_C_LagrangeIntegrandFunction extends OptTag {
		
		public OptTag_C_LagrangeIntegrandFunction(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_lagrange_integrand","C: Lagrange integrand function",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			FExp obj = foptclass.objectiveIntegrandExp();
			if (obj != null) {
				obj.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				genPrinter.print(INDENT + "(*res)[0] = ");
				obj.prettyPrint_C(genPrinter,"");
				genPrinter.print(";\n");
			}
		}
	}

	
	class OptTag_C_PathEqualityConstraints extends OptTag {
	
		public OptTag_C_PathEqualityConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_path_equality_constraints","C: path equality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FConstraint c : foptclass.pathEqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintEq c : foptclass.pathEqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	class OptTag_C_PathInequalityConstraints extends OptTag {
		
		public OptTag_C_PathInequalityConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_path_inequality_constraints","C: path inequality constraints (leq)",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FConstraint c : foptclass.pathLeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FConstraint c : foptclass.pathGeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintLeq c : foptclass.pathLeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
			for (FConstraintGeq c : foptclass.pathGeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	class OptTag_C_PointEqualityConstraints extends OptTag {
		
		public OptTag_C_PointEqualityConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_point_equality_constraints","C: point equality constraints",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FConstraint c : foptclass.pointEqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintEq c : foptclass.pointEqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	class OptTag_C_PointInequalityConstraints extends OptTag {
		
		public OptTag_C_PointInequalityConstraints(AbstractGenerator myGenerator, 
		  FOptClass foptclass) {
			super("C_Opt_point_inequality_constraints","C: point inequality constraints (leq)",
			  myGenerator,foptclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FConstraint c : foptclass.pointLeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FConstraint c : foptclass.pointGeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintLeq c : foptclass.pointLeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
			for (FConstraintGeq c : foptclass.pointGeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	
	/**
	 * Constructor.
	 * 
	 * @param expPrinter Printer object used to generate code for expressions.
	 * @param escapeCharacter Escape characters used to decode tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public OptimicaCGenerator(Printer expPrinter, char escapeCharacter,
			FOptClass fclass) {
		super(expPrinter,escapeCharacter, fclass);
	}
}

