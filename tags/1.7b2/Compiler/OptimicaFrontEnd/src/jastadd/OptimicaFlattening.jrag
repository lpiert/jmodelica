/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect OptimicaFlattening {

    public InstFullClassDecl OptClassDecl.newInstClassDecl() {
    	InstOptClassDecl fcd = new InstOptClassDecl(this, new Opt(), newInstRestriction(), newInstExternalOpt(), new Opt());
	    fcd.setInstConstrainingOpt(newInstConstrainingClassOpt());
	    if (hasClassModification()) 
	    	fcd.setInstClassModification(getClassModification().newInstModification());
    	return copyLocationTo(fcd);
    }
    
    public InstOptimizationClass OptimizationClass.newInstRestriction() {
    	return copyLocationTo(new InstOptimizationClass());
    }
    
    public void InstOptClassDecl.flattenUsedFuncsAndEnums(FClass fc) {
		getFConstraints().flattenUsedFuncsAndEnums(fc);
		if (hasInstClassModification()) 
			getInstClassModification().flattenUsedFuncsAndEnums(fc);
		super.flattenUsedFuncsAndEnums(fc);
    }
        
    refine Flattening void InstClassDecl.flattenInstClassDecl(FClass fc) {
		fc.setFQName(new FQName(qualifiedName())); 
		fc.addFEquationBlock(new FEquationBlock(new List()));			
		FQName prefix = getFQName();
		for (FAbstractEquation ae : getFAbstractEquations()) {
		   ae.flatten(prefix, fc);
		   ae.buildConnectionSets(prefix, fc.getConnectionSetManager());
		}
		getInstComponentDeclList().flatten(fc);
		getInstExtendsList().flatten(fc);
		flattenUsedFuncsAndEnums(fc);
		
		if (this instanceof InstOptClassDecl) {
			FOptClass foc = (FOptClass)fc;
			InstOptClassDecl iocd = (InstOptClassDecl)this;
			for (FConstraint c : iocd.getFConstraints()) 
			       c.flatten(iocd.getFQName(),foc);
			OptClassDecl ocd = (OptClassDecl)iocd.getClassDecl();
			if (iocd.hasInstClassModification()) {
				List attr = new List();
				iocd.getInstClassModification().collectAttributes(attr, new FQName(new List()), AttributeExpRetriever.DEFAULT);
				foc.setFAttributeList(attr);
			}
		}
		
		fc.genConnectionEquations();

		//log.debug(fc.prettyPrint(""));
		//fc.dumpTree("");
				
		HashSet<InstAccess> instAccesses = fc.collectInstAccesses();
		if (instAccesses.size()>0) {
			log.error("Flat model contains InstAccesses!!!");
			//return null;
		}
	 	
    }
	
	refine Flattening public void InstNode.flatten(FClass fc) {
		FQName prefix = getFQName();
		for (FAbstractEquation ae : getFAbstractEquations()) {
		   ae.flatten(prefix, fc);
		   ae.buildConnectionSets(prefix, fc.getConnectionSetManager());
		}
        for (FConstraint c : getFConstraints()) {
           c.flatten(prefix, fc);
        }
		getInstComponentDeclList().flatten(fc);
		getInstExtendsList().flatten(fc);
	}	

	syn lazy ArrayList<Constraint> ClassDecl.constraints() = new ArrayList<Constraint>();
	eq OptClassDecl.constraints() {
	    ArrayList<Constraint> l = new ArrayList<Constraint>();
		for (Constraint c : getConstraints())
			l.add(c);
		return l;
	}
	//eq ShortClassDecl.constraints() = getExtendsClauseShortClass().getSuper().myClassDecl().constraints();
	
	syn ArrayList<Constraint> InstNode.constraints() = new ArrayList<Constraint>();
	eq InstClassRoot.constraints() = getClassDecl().constraints();
	eq InstOptClassDecl.constraints() = getClassDecl().constraints();
	eq InstComponentDecl.constraints() = myInstClass().constraints();
	eq InstExtends.constraints() = myInstClass().constraints();
	
    public abstract FConstraint Constraint.instantiate();
	
	public FConstraintEq ConstraintEq.instantiate() {
		return copyLocationTo(new FConstraintEq(getLeft().instantiate(),getRight().instantiate()));
	}

	public FConstraintLeq ConstraintLeq.instantiate() {
		return copyLocationTo(new FConstraintLeq(getLeft().instantiate(),getRight().instantiate()));
	}
	
	public FConstraintGeq ConstraintGeq.instantiate() {
		return copyLocationTo(new FConstraintGeq(getLeft().instantiate(),getRight().instantiate()));
	}
	
	public InstForClauseC ForClauseC.instantiate() {
		List<InstForIndex> forIndex = new List();
		List<FConstraint> cons = new List();
		for (ForIndex fi : getForIndexs()) 
    		forIndex.add(fi.instantiate());    	
		for (Constraint c : getConstraints()) 
			cons.add(c.instantiate());
		return new InstForClauseC(forIndex, cons);
	}

	
	public FTimedVariable TimedVariable.instantiate() {
		FIdUseInstAccess name = new FIdUseInstAccess(getName().newInstAccess());
		return copyLocationTo(new FTimedVariable(name, getArg().instantiate()));
	}
	
	
    public void FConstraint.flatten(FQName prefix, FClass fc) {}
	
	public void FConstraintEq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintEq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public void FConstraintLeq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintLeq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public void FConstraintGeq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintGeq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public void InstForClauseC.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		List forIndex = new List();
		List cons = new List();
		for (InstForIndex ifi : getInstForIndexs()) 
			forIndex.add(ifi.flatten(prefix));
		
		FOptClass focc = new FOptClass();
		focc.setFQName(new FQName("tmp")); 
		focc.setFConstraintList(cons);
		for (FConstraint c : getFConstraints()) 
			c.flatten(prefix, focc);
		
		foc.addFConstraint(new FForClauseC(forIndex, cons));
	}

	/*
	public FExp FTimedVariable.flatten(FQName prefix) {
		return new FTimedVariable(getName().flatten(prefix),
		   getArg().flatten(prefix));
	}
	*/

	syn nta List<FConstraint> InstNode.getFConstraintList()  { 
	    List l = new List();
	    for (Constraint c : constraints()) {
	    	l.add(c.instantiate());
	    }
	    return l;

	}

	syn List<FConstraint> InstNode.getFConstraints() {
		return getFConstraintList();	
	}

	
	public FExp InstTimedVariable.flatten(FQName prefix) {
		return new FTimedVariable(new FIdUse(getName().flatten(new FQName())),getArg().flatten(prefix));		
	}

	/*
	public FExp FTimedVariable.flatten(FQName prefix) {
		return new FTimedVariable(new FIdUse(getName().flatten(new FQName())),getArg().flatten(prefix));		
	}
*/
	
}