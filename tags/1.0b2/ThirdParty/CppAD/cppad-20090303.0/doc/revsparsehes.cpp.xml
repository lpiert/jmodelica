<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Reverse Mode Hessian Sparsity: Example and Test</title>
<meta name="description" id="description" content="Reverse Mode Hessian Sparsity: Example and Test"/>
<meta name="keywords" id="keywords" content=" Revsparsehes example sparsity Hessian test "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_revsparsehes.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="revsparsehes.xml" target="_top">Prev</a>
</td><td><a href="drivers.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>FunEval</option>
<option>Sparse</option>
<option>RevSparseHes</option>
<option>RevSparseHes.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>FunEval-&gt;</option>
<option>Forward</option>
<option>Reverse</option>
<option>Sparse</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Sparse-&gt;</option>
<option>ForSparseJac</option>
<option>RevSparseJac</option>
<option>RevSparseHes</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>RevSparseHes-&gt;</option>
<option>RevSparseHes.cpp</option>
</select>
</td>
<td>RevSparseHes.cpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>Reverse Mode Hessian Sparsity: Example and Test</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 

# include &lt;cppad/cppad.hpp&gt;
namespace { // -------------------------------------------------------------
// define the template function RevSparseHesCases&lt;Vector&gt; in empty namespace
template &lt;typename Vector&gt; // vector class, elements of type bool
bool RevSparseHesCases(void)
{	bool ok = true;
	using CppAD::AD;

	// domain space vector
	size_t n = 3; 
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt; X(n);
	X[0] = 0.; 
	X[1] = 1.;
	X[2] = 2.;

	// declare independent variables and start recording
	CppAD::Independent(X);

	// range space vector
	size_t m = 2;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt; Y(m);
	Y[0] = sin( X[2] );
	Y[1] = X[0] * X[1];

	// create f: X -&gt; Y and stop tape recording
	CppAD::ADFun&lt;double&gt; f(X, Y);

	// sparsity pattern for the identity matrix
	Vector r(n * n);
	size_t i, j;
	for(i = 0; i &lt; n; i++)
	{	for(j = 0; j &lt; n; j++)
			r[ i * n + j ] = false;
		r[ i * n + i ] = true;
	}

	// compute sparsity pattern for J(x) = F^{(1)} (x)
	f.ForSparseJac(n, r);

	// compute sparsity pattern for H(x) = F_0^{(2)} (x)
	Vector s(m);
	for(i = 0; i &lt; m; i++)
		s[i] = false;
	s[0] = true;
	Vector h(n * n);
	h    = f.RevSparseHes(n, s);

	// check values
	ok &amp;= (h[ 0 * n + 0 ] == false);  // second partial w.r.t X[0], X[0]
	ok &amp;= (h[ 0 * n + 1 ] == false);  // second partial w.r.t X[0], X[1]
	ok &amp;= (h[ 0 * n + 2 ] == false);  // second partial w.r.t X[0], X[2]

	ok &amp;= (h[ 1 * n + 0 ] == false);  // second partial w.r.t X[1], X[0]
	ok &amp;= (h[ 1 * n + 1 ] == false);  // second partial w.r.t X[1], X[1]
	ok &amp;= (h[ 1 * n + 2 ] == false);  // second partial w.r.t X[1], X[2]

	ok &amp;= (h[ 2 * n + 0 ] == false);  // second partial w.r.t X[2], X[0]
	ok &amp;= (h[ 2 * n + 1 ] == false);  // second partial w.r.t X[2], X[1]
	ok &amp;= (h[ 2 * n + 2 ] == true);   // second partial w.r.t X[2], X[2]

	// compute sparsity pattern for H(x) = F_1^{(2)} (x)
	for(i = 0; i &lt; m; i++)
		s[i] = false;
	s[1] = true;
	h    = f.RevSparseHes(n, s);

	// check values
	ok &amp;= (h[ 0 * n + 0 ] == false);  // second partial w.r.t X[0], X[0]
	ok &amp;= (h[ 0 * n + 1 ] == true);   // second partial w.r.t X[0], X[1]
	ok &amp;= (h[ 0 * n + 2 ] == false);  // second partial w.r.t X[0], X[2]

	ok &amp;= (h[ 1 * n + 0 ] == true);   // second partial w.r.t X[1], X[0]
	ok &amp;= (h[ 1 * n + 1 ] == false);  // second partial w.r.t X[1], X[1]
	ok &amp;= (h[ 1 * n + 2 ] == false);  // second partial w.r.t X[1], X[2]

	ok &amp;= (h[ 2 * n + 0 ] == false);  // second partial w.r.t X[2], X[0]
	ok &amp;= (h[ 2 * n + 1 ] == false);  // second partial w.r.t X[2], X[1]
	ok &amp;= (h[ 2 * n + 2 ] == false);  // second partial w.r.t X[2], X[2]

	return ok;
}
} // End empty namespace

# include &lt;vector&gt;
# include &lt;valarray&gt;
bool RevSparseHes(void)
{	bool ok = true;
	// Run with Vector equal to four different cases
	// all of which are Simple Vectors with elements of type bool.
	ok &amp;= RevSparseHesCases&lt; CppAD::vector  &lt;bool&gt; &gt;();
	ok &amp;= RevSparseHesCases&lt; CppAD::vectorBool     &gt;();
	ok &amp;= RevSparseHesCases&lt; std::vector    &lt;bool&gt; &gt;(); 
	ok &amp;= RevSparseHesCases&lt; std::valarray  &lt;bool&gt; &gt;(); 

	return ok;
}

</pre>
</font></code>


<hr/>Input File: example/rev_sparse_hes.cpp

</body>
</html>
