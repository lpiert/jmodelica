<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Sparse Hessian: Easy Driver</title>
<meta name="description" id="description" content="Sparse Hessian: Easy Driver"/>
<meta name="keywords" id="keywords" content=" Sparsehessian hessian sparse "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_sparse_hessian_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="sparse_jacobian.cpp.xml" target="_top">Prev</a>
</td><td><a href="sparse_hessian.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Drivers</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Drivers-&gt;</option>
<option>Jacobian</option>
<option>ForOne</option>
<option>RevOne</option>
<option>Hessian</option>
<option>ForTwo</option>
<option>RevTwo</option>
<option>sparse_jacobian</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>sparse_hessian-&gt;</option>
<option>sparse_hessian.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>w</option>
<option>p</option>
<option>hes</option>
<option>BaseVector</option>
<option>BoolVector</option>
<option>Uses Forward</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Sparse Hessian: Easy Driver</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>

<code><font color="blue"></font><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>f</span></font></i><font color="blue"><span style='white-space: nowrap'>.SparseHessian(</span></font><i><font color="black"><span style='white-space: nowrap'>x</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>w</span></font></i><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>

<code><font color="blue"></font><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>f</span></font></i><font color="blue"><span style='white-space: nowrap'>.SparseHessian(</span></font><i><font color="black"><span style='white-space: nowrap'>x</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>w</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>p</span></font></i><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 do denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>

corresponding to 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
. 
The syntax above sets 
<code><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i></code>
 to the Hessian 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>hes</mi>
<mo stretchy="false">=</mo>
<mfrac><mrow><msup><mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>d</mi>
</mstyle></mrow>
<mrow><mn>2</mn>
</mrow>
</msup>
</mrow>
<mrow><mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>d</mi>
</mstyle></mrow>
<mspace width='.18em'/>
<msup><mrow><mi mathvariant='italic'>x</mi>
</mrow>
<mrow><mn>2</mn>
</mrow>
</msup>
</mrow>
</mfrac>
<munderover><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>1</mn>
</mrow>
<mi mathvariant='italic'>m</mi>
</munderover>
<msub><mi mathvariant='italic'>w</mi>
<mi mathvariant='italic'>i</mi>
</msub>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

This is a preliminary implementation of a method for using the fact
that the matrix is sparse to reduce the amount of computation necessary.
One should use speed tests to verify that results are computed faster
than when using the routine <a href="hessian.xml" target="_top"><span style='white-space: nowrap'>Hessian</span></a>
.

<br/>
<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font><i><font color="black"><span style='white-space: nowrap'>Base</span></font></i><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>f</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
Note that the <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
 is not <code><font color="blue">const</font></code>
(see <a href="sparse_hessian.xml#Uses Forward" target="_top"><span style='white-space: nowrap'>Uses&#xA0;Forward</span></a>
 below).

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument 
<code><i><font color="black"><span style='white-space: nowrap'>x</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>BaseVector</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font><i><font color="black"><span style='white-space: nowrap'>x</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
(see <a href="sparse_hessian.xml#BaseVector" target="_top"><span style='white-space: nowrap'>BaseVector</span></a>
 below)
and its size 
must be equal to 
<code><i><font color="black"><span style='white-space: nowrap'>n</span></font></i></code>
, the dimension of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
.
It specifies
that point at which to evaluate the Hessian.

<br/>
<br/>
<b><big><a name="w" id="w">w</a></big></b>
<br/>
The argument 
<code><i><font color="black"><span style='white-space: nowrap'>w</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>BaseVector</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font><i><font color="black"><span style='white-space: nowrap'>w</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
and size 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
</mrow></math>

.
It specifies the value of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>w</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow></math>

 in the expression 
for 
<code><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i></code>
.
The more components of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>w</mi>
</mrow></math>

 that are identically zero,
the more spares the resulting Hessian may be (and hence the more efficient
the calculation of 
<code><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i></code>
 may be).

<br/>
<br/>
<b><big><a name="p" id="p">p</a></big></b>
<br/>
The argument 
<code><i><font color="black"><span style='white-space: nowrap'>p</span></font></i></code>
 is optional and has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BoolVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>p</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="sparse_hessian.xml#BoolVector" target="_top"><span style='white-space: nowrap'>BoolVector</span></a>
 below)
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.
It specifies a 
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for the Hessian; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

.

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<munder><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mi mathvariant='italic'>i</mi>
</munder>
<msub><mi mathvariant='italic'>w</mi>
<mi mathvariant='italic'>i</mi>
</msub>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mn>2</mn>
</msup>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>j</mi>
</msub>
<mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>k</mi>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
<mo stretchy="false">;</mo>
<mo stretchy="false">&#x021D2;</mo>
<mspace width='.3em'/>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>true</mi>
</mstyle></mrow>
</mrow></math>

<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>If this sparsity pattern does not change between calls to 

<code><font color="blue"><span style='white-space: nowrap'>SparseHessian</span></font></code>
, it should be faster to calculate 
<code><i><font color="black"><span style='white-space: nowrap'>p</span></font></i></code>
 once and
pass this argument to 
<code><font color="blue"><span style='white-space: nowrap'>SparseHessian</span></font></code>
.

<br/>
<br/>
<b><big><a name="hes" id="hes">hes</a></big></b>
<br/>
The result 
<code><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>BaseVector</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>hes</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.
For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>

 
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>



<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>hes</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mn>2</mn>
</msup>
<msup><mi mathvariant='italic'>w</mi>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>T</mi>
</mstyle></mrow>
</msup>
<mi mathvariant='italic'>F</mi>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>j</mi>
</msub>
<mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02113;</mo>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="BaseVector" id="BaseVector">BaseVector</a></big></b>
<br/>
The type 
<code><i><font color="black"><span style='white-space: nowrap'>BaseVector</span></font></i></code>
 must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type</span></a>


<code><i><font color="black"><span style='white-space: nowrap'>Base</span></font></i></code>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="BoolVector" id="BoolVector">BoolVector</a></big></b>
<br/>
The type 
<code><i><font color="black"><span style='white-space: nowrap'>BoolVector</span></font></i></code>
 must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;bool</span></a>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.
In order to save memory, 
you may want to use a class that packs multiple elements into one
storage location; for example,
<a href="cppad_vector.xml#vectorBool" target="_top"><span style='white-space: nowrap'>vectorBool</span></a>
.

<br/>
<br/>
<b><big><a name="Uses Forward" id="Uses Forward">Uses Forward</a></big></b>
<br/>
After each call to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
,
the object 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
 contains the corresponding 
<a href="glossary.xml#Taylor Coefficient" target="_top"><span style='white-space: nowrap'>Taylor&#xA0;coefficients</span></a>
.
After <code><font color="blue">SparseHessian</font></code>,
the previous calls to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 are undefined.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The routine
<a href="sparse_hessian.cpp.xml" target="_top"><span style='white-space: nowrap'>sparse_hessian.cpp</span></a>
 
is examples and tests of <code><font color="blue">sparse_hessian</font></code>.
It return <code><font color="blue">true</font></code>, if it succeeds and <code><font color="blue">false</font></code> otherwise.



<hr/>Input File: cppad/local/sparse_hessian.hpp

</body>
</html>
