<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Conditional Expressions: Example and Test</title>
<meta name="description" id="description" content="Conditional Expressions: Example and Test"/>
<meta name="keywords" id="keywords" content=" Condexp example test "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_condexp.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="condexp.xml" target="_top">Prev</a>
</td><td><a href="discrete.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>ADValued</option>
<option>CondExp</option>
<option>CondExp.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADValued-&gt;</option>
<option>Arithmetic</option>
<option>std_math_ad</option>
<option>MathOther</option>
<option>CondExp</option>
<option>Discrete</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>CondExp-&gt;</option>
<option>CondExp.cpp</option>
</select>
</td>
<td>CondExp.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Description</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Conditional Expressions: Example and Test</big></big></b></center>
<br/>
<b><big><a name="Description" id="Description">Description</a></big></b>
<br/>
Use <code><font color="blue">CondExp</font></code> to compute

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<munderover><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
<mrow><mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow>
</munderover>
<mi>log</mi>
<mo stretchy="false">(</mo>
<mo stretchy="false">|</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>j</mi>
</msub>
<mo stretchy="false">|</mo>
<mo stretchy="false">)</mo>
</mrow></math>

and its derivative at various argument values
with out having to re-tape; i.e.,
using only one <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object.

<code><font color="blue">
<pre style='display:inline'> 

# include &lt;cppad/cppad.hpp&gt;

namespace {
	double Infinity(double zero)
	{	return 1. / zero; }
}

bool CondExp(void)
{	bool ok = true;

	using CppAD::AD;
	using CppAD::NearEqual;
	using CppAD::log; 
	using CppAD::abs;

	// domain space vector
	size_t n = 5;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt; X(n);
	size_t j;
	for(j = 0; j &lt; n; j++)
		X[j] = 1.;

	// declare independent variables and start tape recording
	CppAD::Independent(X);

	// sum with respect to j of log of absolute value of X[j]
	// sould be - infinity if any of the X[j] are zero
	AD&lt;double&gt; MinusInfinity = - Infinity(0.);
	AD&lt;double&gt; Sum           = 0.;
	AD&lt;double&gt; Zero(0);
	for(j = 0; j &lt; n; j++)
	{	// if X[j] &gt; 0
		Sum += CppAD::CondExpGt(X[j], Zero, log(X[j]),     Zero);

		// if X[j] &lt; 0
		Sum += CppAD::CondExpLt(X[j], Zero, log(-X[j]),    Zero);

		// if X[j] == 0
		Sum += CppAD::CondExpEq(X[j], Zero, MinusInfinity, Zero);
	}

	// range space vector 
	size_t m = 1;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt; Y(m);
	Y[0] = Sum;

	// create f: X -&gt; Y and stop tape recording
	CppAD::ADFun&lt;double&gt; f(X, Y);

	// vectors for arguments to the function object f
	CPPAD_TEST_VECTOR&lt;double&gt; x(n);   // argument values
	CPPAD_TEST_VECTOR&lt;double&gt; y(m);   // function values 
	CPPAD_TEST_VECTOR&lt;double&gt; w(m);   // function weights 
	CPPAD_TEST_VECTOR&lt;double&gt; dw(n);  // derivative of weighted function

	// a case where abs( x[j] ) &gt; 0 for all j
	double check  = 0.;
	double sign   = 1.;
	for(j = 0; j &lt; n; j++)
	{	sign *= -1.;
		x[j] = sign * double(j + 1); 
		check += log( abs( x[j] ) );
	}

	// function value 
	y  = f.Forward(0, x);
	ok &amp;= ( y[0] == check );

	// compute derivative of y[0]
	w[0] = 1.;
	dw   = f.Reverse(1, w);
	for(j = 0; j &lt; n; j++)
	{	if( x[j] &gt; 0. )
			ok &amp;= NearEqual(dw[j], 1./abs( x[j] ), 1e-10, 1e-10); 
		else	ok &amp;= NearEqual(dw[j], -1./abs( x[j] ), 1e-10, 1e-10); 
	}

	// a case where x[0] is equal to zero
	sign = 1.;
	for(j = 0; j &lt; n; j++)
	{	sign *= -1.;
		x[j] = sign * double(j); 
	}

	// function value 
	y   = f.Forward(0, x);
	ok &amp;= ( y[0] == -Infinity(0.) );

	// compute derivative of y[0]
	w[0] = 1.;
	dw   = f.Reverse(1, w);
	for(j = 0; j &lt; n; j++)
	{	if( x[j] &gt; 0. )
			ok &amp;= NearEqual(dw[j], 1./abs( x[j] ), 1e-10, 1e-10); 
		else if( x[j] &lt; 0. )
			ok &amp;= NearEqual(dw[j], -1./abs( x[j] ), 1e-10, 1e-10); 
		else
		{	// in this case computing dw[j] ends up multiplying 
			// -infinity * zero and hence results in Nan
		}
	}
	
	return ok;
}</pre>
</font></code>


<hr/>Input File: example/cond_exp.cpp

</body>
</html>
