/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

 class Events extends Parser.Events {
  
    public void syntaxError(Symbol token) {
      StringBuffer s = new StringBuffer();
      s.append("Syntax error at line "+ Symbol.getLine(token.getStart()) + ", column " + Symbol.getColumn(token.getStart()) + "\n");
      if (token.value != null) {
      	s.append("   Unexpected token: \"" + token.value + "\"");
      } else if (token.getId()<Terminals.NAMES.length)
        s.append("  Unexpected token: " + Terminals.NAMES[token.getId()]);
      else 
        s.append("  *** Syntactic error");
      throw new Error(s.toString());
    }
    
  }

  {report = new Events();}  // Use error handler in parser

	public void syntaxError(Symbol token, String errorMessage) {
      StringBuffer s = new StringBuffer();
      s.append(Symbol.getLine(token.getStart()) + ", " + Symbol.getColumn(token.getStart()) + "\n");
        s.append("  *** Syntactic error: "+ errorMessage);
      throw new Error(s.toString());
    }

	public FlatRoot parseFile(String fileName) {
		FlatRoot fr = null;
		try {
			Reader reader = new FileReader(fileName);
			FlatModelicaScanner scanner = new FlatModelicaScanner(new BufferedReader(reader));
			fr = (FlatRoot)parse(scanner);
			fr.setFileName(fileName);
		}  catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return fr;
	}
	
		public FlatRoot parseString(String str, String fileName) {
		FlatRoot fr = null;
		try {
			FlatModelicaScanner scanner = new FlatModelicaScanner(new StringReader(str));
			fr = (FlatRoot)parse(scanner);
			fr.setFileName(fileName);
		}  catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return fr;
	}

:};


FlatRoot  flat_root = 
    fclass {: return new FlatRoot(fclass); :}
    ;

FClass fclass = 
    ID.restriction
    fqname
	fvariable_list
	fequation_section?
	END_ID.end_s 
	SEMICOLON {: List feq = null;
					if (fequation_section.getNumChild()==0)
						feq = new List();
					else 
						feq = (List)fequation_section.getChild(0);
	
					return new FClass(fqname,fvariable_list,new List(),new List().add(new FEquationBlock(feq))); :}
	;

List fvariable_list = 
	fvariable SEMICOLON    {: return new List().add(fvariable); :}
	| fvariable_list fvariable SEMICOLON {: fvariable_list.add(fvariable);
	                              return fvariable_list; :}
	;
	
FVariable fvariable =
    fvisibility_type?
    ftype_prefix_variability? 
    ftype_prefix_input_output? 
	fprimitive_type
	fqname
	fattributes?
	bexp?
	fstring_comment?        {: List fattr = null;
					if (fattributes.getNumChild()==0)
						fattr = new List();
					else 
						fattr = (List)fattributes.getChild(0);
					FTypePrefixVariability tpo = ftype_prefix_variability.getNumChild()==1?
					  (FTypePrefixVariability)ftype_prefix_variability.getChild(0):
					  new FContinuous();
                    FVisibilityType fvt = fvisibility_type.getNumChild()==1?
                      (FVisibilityType)fvisibility_type.getChild(0) :
                      new FPublicVisibilityType();
                    if (fprimitive_type.isReal()) {   
						return new FRealVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isInteger()) {   
						return new FIntegerVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isBoolean()) {   
						return new FBooleanVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isString()) {   
						return new FStringVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} 
					return null;
					:}
	;
  
FVisibilityType fvisibility_type =
  PUBLIC    {: return new FPublicVisibilityType(); :}  
  | PROTECTED    {: return new FProtectedVisibilityType(); :}  
  ;
FQName fqname =
  fqname_part {: return new FQName(new List().add(fqname_part)); :}
  | fqname DOT fqname_part {: fqname.addFQNamePart(fqname_part); 
                              return fqname;:}  
  ;
  
FQNamePart fqname_part =
  ID.name farray_subscripts? {: return new FQNamePart(name,farray_subscripts); :}
  ;

FArraySubscripts farray_subscripts = 
  LBRACK fsubscript_list RBRACK {: return new FArraySubscripts(fsubscript_list); :}
  ;
  
List fsubscript_list =
  fsubscript               {: return new List().add(fsubscript); :}
  | fsubscript_list COMMA fsubscript {: fsubscript_list.add(fsubscript);
                                      return fsubscript_list; :}
  ;
  
FSubscript fsubscript =
  COLON                 {: return new FColonSubscript(); :}
  | fexp                 {: return new FExpSubscript(fexp); :}                                                                           
  ;

FEach feach =
    EACH {: return new FEach(); :}  
	;
	
FFinal ffinal =
    FINAL {: return new FFinal(); :}  
	;	
	
List fattributes =
	LPAREN 
	fattribute_list
	RPAREN {: return fattribute_list; :}
	;
	
List fattribute_list = 
	fattribute    {: return new List().add(fattribute); :}
	| fattribute_list COMMA fattribute {: fattribute_list.add(fattribute);
	                              return fattribute_list; :}
	;

FAttribute fattribute =
    feach?
    ffinal?
	fid_decl
	ASSIGN
	fexp {: return new FAttribute(new FIdUse(new FQName(new List().add(new FQNamePart("UnknownType",new Opt())))),
	  fid_decl,new Opt(fexp),true,feach,ffinal,new List()); :}
	;
	
FExp bexp =
	ASSIGN
	fexp {: return fexp; :}
	;

List fequation_section = 
	EQUATION
	fequation* {: return fequation; :}
	;
	
FEquation fequation = 
	fexp.left 
	ASSIGN 
	fexp.right 
	SEMICOLON  {: return new FEquation(left,right); :} 
	;

FTypePrefix ftype_prefix_variability = 
   PARAMETER                          {: return new FParameter(); :}
  | CONSTANT                          {: return new FConstant(); :}
  | DISCRETE                          {: return new FDiscrete(); :}
  ;
  
FTypePrefix ftype_prefix_input_output = 
    INPUT                          {: return new FInput(); :}
  | OUTPUT                          {: return new FOutput(); :}
  ;

FStringComment fstring_comment =
  STRING.s      {: return new FStringComment(s); :}
  | fstring_comment PLUS STRING.s {: fstring_comment.setComment(fstring_comment.getComment().concat(s));
                                    return fstring_comment; :}
  
  ;

FExp fexp = 
  fprimary {: return fprimary; :}
  | fexp.a PLUS fexp.b {: return new FAddExp(a,b); :} 
  | fexp.a MINUS fexp.b {: return new FSubExp(a,b); :}
  | fexp.a MULT fexp.b {: return new FMulExp(a,b); :}
  | fexp.a DIV fexp.b {: return new FDivExp(a,b); :}  
  | fprimary.a POW fprimary.e {: return new FPowExp(a,e); :}
  ;
  
FExp fprimary =        
   UNSIGNED_NUMBER.n    {: return new FRealLitExp(n); :}
  | MINUS fprimary       {: return new FNegExp(fprimary); :}
  | STRING.s               {: return new FStringLitExp(s); :}
  | fid_use LPAREN arg_list RPAREN {: return new FFunctionCall(fid_use,arg_list); :}
  | fid_use        {: return new FIdUseExp(fid_use); :} 
  | LPAREN fexp.a RPAREN {: return a; :}
  | TIME {: return new FTimeExp(); :}
  | END                 {: return new FEndExp(); :} 
  ;

FPrimitiveType fprimitive_type =
  ID.name {: if (name.equals("Real")) return new FRealScalarType(); 
  	else if (name.equals("Boolean")) return new FBooleanScalarType();
  	else if (name.equals("String")) return new FStringScalarType();
  	else if (name.equals("Integer")) return new FIntegerScalarType();
  	else return null; :}
  ;
  
FIdUse fid_use = 
   fqname   {: return new FIdUse(fqname); :}
   ;
   
FIdDecl fid_decl = 
   fqname   {: 
   			 return new FIdDecl(fqname); :}
   ;
   
 List arg_list = 
  fexp                   {: return new List().add(fexp); :}
  | arg_list COMMA fexp  {: arg_list.add(fexp); 
                           return arg_list; :}
  ;
   
