
/**
 * \brief Contains transformations on the flattened model which converts
 * it into a canonical form.
 */
aspect TransformCanonical{

	/**
	 * \brief Transform the raw flattened model into a structured form.
	 * 
	 * Currently, the following operations are performed:
	 *  <ul>
	 *    <li> Binding equations for variables are converted into regular
	 *         equations by calling genBindingEquations().
	 *    <li> Derivative variables are generated and inserted in the
	 *         list of FVariables by calling addFDerivativeVariables().
	 *    <li> Generate initial equations based on start attribute.
	 *    <li> Sort dependent parameters.
	 *  </ul>
	 */
	public void FClass.transformCanonical() {
		genBindingEquations();
		addFDerivativeVariables();
		genInitialEquations();
		sortDependentParameters();
		genAlias();
		//flushCache(); // TODO: is this enough or should the entire tree be flushed?
 	}
		
	/**
	 * \brief Convert variable binding equations into regular equations.
	 */
	public void FClass.genBindingEquations() {
		for (FVariable fv : getFVariables()) {
			if (fv.hasBindingExp() && !fv.isParameter() && !fv.isConstant()) {
				FExp bexp = fv.getBindingExp();
				fv.setBindingExpOpt(new Opt());
				// Do not copy array indices
				FQName var_name = fv.getFQName().fullCopy();
				var_name.getFQNamePart(var_name.getNumFQNamePart()-1).
				   setFArraySubscriptsOpt(new Opt());
				FEquation feq = new FEquation(new FIdUseExp(
						new FIdUse(var_name)),bexp);
				getFEquationBlock(0).addFAbstractEquation(feq);
			}
		}
		flushAllRecursive();
	}

	/**
	 * \brief Add derivative variables to the list of FVariables, one for each
	 * differentiate variable.
	 */
	public void FClass.addFDerivativeVariables() {
		ArrayList<FDerivativeVariable> l = new ArrayList<FDerivativeVariable>();
		for (FVariable fv : differentiatedRealVariables()) {
			l.add(new FDerivativeVariable((FVisibilityType)fv.getFVisibilityType().fullCopy(),
					(FTypePrefixVariability)fv.getFTypePrefixVariability().fullCopy(),
					new Opt(),
					new List(),
					new Opt(),
					new Opt(),
					fv.getFQName().fullCopy()));
		}
		for (FVariable fv : l) {
			addFVariable(fv);
		}
		root().flushAllRecursive();
	}

	/**
	 * \brief Generate initial equations from variables with fixed start
	 * attributes.
	 * 
	 * Intitial equations corresponding to explicitly set start attributes of 
	 * differentiated variables are also generated, without taking the fixed
	 * attribute into account.
	 */
	public void FClass.genInitialEquations() {
		for (FRealVariable fv : realVariables()) {
			if (fv.fixedAttribute() ||
					(root().options.getBooleanOption("state_start_values_fixed") 
							&&  fv.isDifferentiatedVariable() && fv.startAttributeSet())) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeExp()));
			}	
		}
		for (FIntegerVariable fv : integerVariables()) {
			if (fv.fixedAttribute()) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeExp()));
			}	
		}
		for (FBooleanVariable fv : booleanVariables()) {
			if (fv.fixedAttribute()) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeExp()));
			}	
		}		
		root().flushAllRecursive();
	}
	
	ArrayList<String> FClass.aliasErrors = new ArrayList<String>();
	
	/**
	 * \brief Generate alias information and remove alias equations.
	 */
	public void FClass.genAlias() {
		aliasManager = new AliasManager();
		
		for (FAbstractEquation equation : 
			getFEquationBlock(0).getFAbstractEquations()) {
			// Iterate over all equations
			if (equation instanceof FEquation) {
				FEquation eqn = (FEquation)equation;
				if (eqn.getLeft() instanceof FIdUseExp && // x = y
 						eqn.getRight() instanceof FIdUseExp) {
 					AbstractFVariable fv1 = ((FIdUseExp)eqn.getLeft()).myFV();
 					AbstractFVariable fv2 = ((FIdUseExp)eqn.getRight()).myFV();	
 					if (fv1.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)eqn.getLeft()).name() + " is unknown.");
 						break;
 					}
 					if (fv2.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)eqn.getRight()).name() + " is unknown.");
 						break;
 					}
					if (!fv1.isUnknown() && !fv2.isUnknown()) { 					
						aliasManager.addAliasVariables((FVariable)fv1,
							(FVariable)fv2,
							false);
					}
				} else if (eqn.getLeft() instanceof FNegExp && // -x = y
						((FNegExp)eqn.getLeft()).getFExp() instanceof FIdUseExp &&
						eqn.getRight() instanceof FIdUseExp) {
 					AbstractFVariable fv1 = ((FIdUseExp)((FNegExp)eqn.getLeft()).getFExp()).myFV();
 					AbstractFVariable fv2 = ((FIdUseExp)eqn.getRight()).myFV();
 					if (fv1.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)((FNegExp)eqn.getLeft()).getFExp()).name() + " is unknown.");
 					}
 					if (fv2.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)eqn.getRight()).name() + " is unknown.");
  					}
					if (!fv1.isUnknown() && !fv2.isUnknown()) {
						aliasManager.addAliasVariables((FVariable)fv1,
							(FVariable)fv2,
							true); 
		            }
				} else if (eqn.getLeft() instanceof FIdUseExp && // x = -y
 						eqn.getRight() instanceof FNegExp &&
						((FNegExp)eqn.getRight()).getFExp() instanceof FIdUseExp) {
					AbstractFVariable fv1 = ((FIdUseExp)eqn.getLeft()).myFV();
 					AbstractFVariable fv2 = ((FIdUseExp)((FNegExp)eqn.getRight()).getFExp()).myFV();	
 					if (fv1.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)eqn.getLeft()).name() + " is unknown.");
  					}
 					if (fv2.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)((FNegExp)eqn.getRight()).getFExp()).name() + " is unknown.");
 						break;
 					}
					if (!fv1.isUnknown() && !fv2.isUnknown()) {						
						aliasManager.addAliasVariables((FVariable)fv1,
							(FVariable)fv2,
							true);
					}
				} else if (eqn.getLeft() instanceof FNegExp && // -x = -y
						((FNegExp)eqn.getLeft()).getFExp() instanceof FIdUseExp &&
						eqn.getRight() instanceof FNegExp &&
						((FNegExp)eqn.getRight()).getFExp() instanceof FIdUseExp
						) {
					AbstractFVariable fv1 = ((FIdUseExp)((FNegExp)eqn.getLeft()).getFExp()).myFV();
 					AbstractFVariable fv2 = ((FIdUseExp)((FNegExp)eqn.getRight()).getFExp()).myFV();	
 					if (fv1.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)((FNegExp)eqn.getLeft()).getFExp()).name() + " is unknown.");
 					}
 					if (fv2.isUnknown()) {
 						aliasErrors.add("During computation of alias sets: the variable " + ((FIdUseExp)((FNegExp)eqn.getRight()).getFExp()).name() + " is unknown.");
 					}	
					if (!fv1.isUnknown() && !fv2.isUnknown()) { 										
						aliasManager.addAliasVariables((FVariable)fv1,
							(FVariable)fv2,
							false);
					}
				}
			}
		}	
		
	}
	
	/**
	 * \brief Flush all caches, including collection attributes.
	 */
	public void ASTNode.flushAll() {
		flushCache();
		flushCollectionCache();
	}

	/**
	 * \brief Flush all caches, including collection attributes, and also
	 * flush all children.
	 */
	public void ASTNode.flushAllRecursive() {
		flushAll();
		for (int i=0;i<getNumChild();i++) {
			getChild(i).flushAllRecursive();
		}
	}

	
}

aspect ParameterSorting {

	/**
	 * \brief Indicate if there exist cyclic parameter dependencies.
	 */
	public boolean FClass.cyclicParameters = false;
	
	/**
	 * \brief Sort dependent parameters.
	 * 
	 * This is a simple implementation of Kahn's topological sorting algorithm.
	 * This implementation will most likely be replaced by other graph 
	 * algorithms later on.
	 */
	public void FClass.sortDependentParameters() {
		
		// Retrieve the dependent parameter that will be sorted
		ArrayList<FVariable> dps = dependentParameters();
		
		// Create data structures for the adjacency graph.
		ArrayList<ArrayList<FVariable>> toNodes = new ArrayList<ArrayList<FVariable>>();	
		ArrayList<ArrayList<FVariable>> fromNodes = new ArrayList<ArrayList<FVariable>>();	
		
		// Initialize data structures.
		for(int i=0;i<numDependentParameters();i++) {
			toNodes.add(new ArrayList<FVariable>());
			fromNodes.add(new ArrayList<FVariable>());
		}
		
		// For each flat variable
		for (FVariable fv : dps) {
			// Retreive all variables referenced in binding expression
			ArrayList<FVariable> deps = fv.referencedFVariablesInBindingExp();
			// Build the actual adjacency graph.
			for (FVariable fv_add : deps) {
				if (!(dps.indexOf(fv_add)<0) && !toNodes.get(dps.indexOf(fv_add)).contains(fv)) {
					toNodes.get(dps.indexOf(fv_add)).add(fv);		
					fromNodes.get(dps.indexOf(fv)).add(fv_add);
				}
			}
		}	
				
		// Sort using algorithm described at
		// http://en.wikipedia.org/wiki/Topological_sorting
		ArrayList<FVariable> L = new ArrayList<FVariable>();
		ArrayList<FVariable> S = new ArrayList<FVariable>();
		
		// Add all nodes without incoming edges to S
		int i = 0;
		for (ArrayList<FVariable> l : fromNodes) {
			if (l.isEmpty()) {
				S.add(dps.get(i));
			}
			i++;
		}
		// Repeat while S is not empty
		while (!S.isEmpty()) {
			// Remove a node n from S
			FVariable n = S.remove(0);
			// Insert n into L
			L.add(n);
			// For each node m with an edge e from n to m
			for (FVariable m : toNodes.get(dps.indexOf(n))) {
				// Remove edge e from fromNodes
				fromNodes.get(dps.indexOf(m)).remove(n);
				// If m has no incoming edges add m to S
				if (fromNodes.get(dps.indexOf(m)).isEmpty()) {
					S.add(m);
				}
			}
			// Remove e from toNodes
			toNodes.get(dps.indexOf(n)).clear();
		}
		
		// Not possible to find an ordering without cycles?
		if (L.size()!=dps.size()) {
			cyclicParameters = true;
			return;
		}
		
		// Remove all dependent parameters from (in reversed order)
		for (i=getNumFVariable()-1;i>=0;i--) {
			if (dps.contains(getFVariable(i))) {
				getFVariableList().removeChild(i);
			}
		}

		// Add all dependent parameters in sorted order.
		for (FVariable fv : L) {
			addFVariable(fv);
		}

		// Flush AST since the structure has changed.
		root().flushAllRecursive();
	}	
		
}

aspect TransformCanonicalErrorCheck {
	
	public void FClass.checkFClassDimensions() {
		// Check dimensions of DAE
		int n_eq_F = numEquations();
		int n_vars_F = numAlgebraicRealVariables() + numDifferentiatedRealVariables();
		if (n_eq_F!=n_vars_F) {
			error("The DAE system has " + n_eq_F + " equations and " + n_vars_F + " free variables.");
		}
		
		// Check dimensions of DAE initialization system
		int n_eq_F0 = numInitialEquations() + numEquations();
		int n_vars_F0 = numAlgebraicRealVariables() + 2*numDifferentiatedRealVariables();
		if (n_eq_F0>n_vars_F0) {
			error("The DAE initialization system has " + n_eq_F0 + " equations and " + n_vars_F0 + " free variables.");
		}
		
	}
	
	public void FClass.collectErrors() {
		if (cyclicParameters) {
			error("The model "+ name() +" contains cyclic parameter dependencies.");
		}
		if (getAliasManager().aliasError()) {
			error(getAliasManager().getAliasErrorMessage());
		}
		for (String str : aliasErrors) {
			error(str);
		}
		checkFClassDimensions();
	}
	
}

