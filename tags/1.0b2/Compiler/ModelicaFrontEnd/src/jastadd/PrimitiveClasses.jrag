/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


aspect UnknownDeclarations {
	
	syn lazy UnknownClassDecl Program.getUnknownClassDecl() {
		
		return new UnknownClassDecl(new PublicVisibilityType(),
		                            new Opt(),
		                            new Opt(),
		                            new MClass(),
		                            new IdDecl("Unknown"),
		                            new Opt(),
		                            new Opt(),
									new Opt(),
		                            new Opt(),
		                            new Opt(),
		                            new Opt(),
		                            new Opt(),
		                            new Opt(),
		                            new List(),
		                            new List(),
		                            new List(),
		                            new List(),
		                            new List(),
		                            new List(),
		                            new List(),
		                            new Opt(),
		                            "Unknown");
	}

	syn lazy UnknownComponentDecl Program.getUnknownComponentDecl() {

		return new UnknownComponentDecl(new Opt(),
										new Opt(),
										new Opt(),
										new Opt(),
										new Opt(),
										new Opt(),
										new Opt(),
										new Opt(),
										new ClassAccess("Unknown"),
										new Opt(),
										new PublicVisibilityType(),
										new IdDecl("Unknown"),
										new Opt(),
										new Opt(),
										new Opt(),
										new Comment(),
										new Opt(),
										new Comment());
	}		                            
	
	syn lazy UnknownClassDecl ASTNode.unknownClassDecl() = root().unknownClassDecl();
	eq SourceRoot.unknownClassDecl() = getProgram().getUnknownClassDecl();

	syn lazy UnknownComponentDecl ASTNode.unknownComponentDecl() = root().unknownComponentDecl();
	eq SourceRoot.unknownComponentDecl() = getProgram().getUnknownComponentDecl();

	syn lazy boolean ClassDecl.isUnknown() = false;
	eq UnknownClassDecl.isUnknown() = true;

	syn lazy boolean ComponentDecl.isUnknown() = false;
	eq UnknownComponentDecl.isUnknown() = true;
	
	syn boolean ClassDecl.isPrimitive() = false;
	eq PrimitiveClassDecl.isPrimitive() = true;
	eq BuiltInClassDecl.isPrimitive() = true;

		
	syn lazy UnknownInstClassDecl SourceRoot.getUnknownInstClassDecl() = getProgram().getInstProgramRoot().getUnknownInstClassDecl();
		
	syn lazy UnknownInstComponentDecl SourceRoot.getUnknownInstComponentDecl() = getProgram().getInstProgramRoot().getUnknownInstComponentDecl();
		
	syn lazy UnknownInstClassDecl InstProgramRoot.getUnknownInstClassDecl() = 
		new UnknownInstClassDecl(getProgram().getUnknownClassDecl(), new Opt());
	syn lazy UnknownInstComponentDecl InstProgramRoot.getUnknownInstComponentDecl() = 
	    new UnknownInstComponentDecl(new InstParseAccess("Unknown",new Opt()),new Opt(),getProgram().getUnknownComponentDecl(), new Opt(),new Opt());
		
	syn lazy UnknownInstClassDecl ASTNode.unknownInstClassDecl() = root().unknownInstClassDecl();
	eq SourceRoot.unknownInstClassDecl() = getUnknownInstClassDecl();

	syn lazy UnknownInstComponentDecl ASTNode.unknownInstComponentDecl() = root().unknownInstComponentDecl();
	eq SourceRoot.unknownInstComponentDecl() = getUnknownInstComponentDecl();

	syn lazy boolean InstClassDecl.isUnknown() = false;
	eq UnknownInstClassDecl.isUnknown() = true;

	syn lazy boolean InstComponentDecl.isUnknown() = false;
	eq UnknownInstComponentDecl.isUnknown() = true;
	
	syn boolean InstClassDecl.isPrimitive() = false;
	eq InstPrimitiveClassDecl.isPrimitive() = true;
	eq InstBuiltInClassDecl.isPrimitive() = true;

	syn boolean InstComponentDecl.isPrimitive() = false;
	eq InstPrimitive.isPrimitive() = true;


	
	syn boolean InstAccess.isUnknown() = true;
	eq InstComponentAccess.isUnknown() = myInstComponentDecl().isUnknown();
	eq InstClassAccess.isUnknown() = myInstClassDecl().isUnknown();                  
		                    
		                            
}