<!--
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!-- Targets for working from terminal window:
       build (default) - generates java files and compiles them
       test            - runs junit test cases
       clean           - removes all generated files and class files
     Targets for working from Eclipse:
       gen             - generates java files
       genClean        - removes all generated files and their class files
 -->

<project name="OptimicaCompiler" default="build" basedir=".">

	<property name = "modelica_front_end_path" value="../ModelicaFrontEnd"/>
	<property name = "optimica_front_end_path" value="../OptimicaFrontEnd"/>
	<property name = "modelica_xml_back_end_path" value="../ModelicaXMLBackEnd"/>
	<property name = "modelica_c_back_end_path" value="../ModelicaCBackEnd"/>
	<property name = "optimica_xml_back_end_path" value="../OptimicaXMLBackEnd"/>
	<property name = "optimica_c_back_end_path" value="../OptimicaCBackEnd"/>
	<property name = "modelica_compiler_path" value="../ModelicaCompiler"/>

	<!-- "package" is the directory where generated files will be stored -->
	<property name="ast_package" value="org.jmodelica.optimica.compiler"/>

	<property name="base_output" value="src/java-generated"/>
	<property name="parser_output" value="${base_output}/org/jmodelica/optimica/parser"/>

	<!-- "tools" is the directory where the needed tools (lexing, parsing, jastadd2, junit.jar) are located. -->
	<property name="jar_output_dir" value="bin"/>
	<property name="jastadd_dir" value="../../ThirdParty/JastAdd"/>
	<property name="junit_dir" value="../../ThirdParty/Junit"/>
	<property name="jflex_dir" value="../../ThirdParty/JFlex/jflex-1.4.3"/>
	<property name="beaver_dir" value="../../ThirdParty/Beaver/beaver-0.9.6.1"/>
	<property name="jmodelica_parser" value="${modelica_front_end_path}/src/parser"/>
	<property name="parser" value="${optimica_front_end_path}/src/parser"/>
		
	<property name="local_ast" value="src/jastadd"/>
	<property name="ast" value="${modelica_front_end_path}/src/jastadd"/>
	<property name="modelica_xml_back_end_ast" value="${modelica_xml_back_end_path}/src/jastadd"/>
	<property name="modelica_c_back_end_ast" value="${modelica_c_back_end_path}/src/jastadd"/>
	<property name="modelica_compiler_ast" value="${modelica_compiler_path}/src/jastadd"/>
	<property name="optimica_xml_back_end_ast" value="${optimica_xml_back_end_path}/src/jastadd"/>
	<property name="optimica_c_back_end_ast" value="${optimica_c_back_end_path}/src/jastadd"/>
	<property name="optimica_front_end_ast" value="${optimica_front_end_path}/src/jastadd"/>
	
	<property name="test_junit_dir" value="src/test/junit"/>
	<property name="test_junit-gen_dir" value="src/test/junit-generated"/>

	<property name="applications" value="${modelica_front_end_path}/${default_src_dir}/org/jmodelica/applications"/>
	<property name="debug" value="${modelica_front_end_path}/${default_src_dir}/org/jmodelica/debug"/>

	<!-- "jastadd" is an ant task class in jastadd2.jar -->
	<taskdef classname="jastadd.JastAddTask" name="jastadd" classpath="${jastadd_dir}/jastadd2.jar" />
	<!-- "jflex" is an ant task class for the scanner generator in JFlex.jar -->
	<taskdef name="jflex" classname="JFlex.anttask.JFlexTask" classpath="${jflex_dir}/lib/JFlex.jar"/>
	<!-- "beaver" is an ant task class for the parser generator in beaver.jar -->
	<taskdef name="beaver" 
  	       classname="beaver.comp.run.AntTask" 
  	       classpath="${beaver_dir}/lib/beaver.jar"/>


	<!-- TARGET build -->
	<target name="build" depends="compile-ast">
		<!-- compile all java files in srcdir and recursively in subdirectories -->
		<!-- you can use the jikes compiler by changing javac1.4 to jikes -->
		<mkdir dir="${jar_output_dir}"/>
		<jar destfile="${jar_output_dir}/OptimicaCompiler.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/optimica/compiler/*.class"/>
				<include name="org/jmodelica/optimica/parser/*.class"/>
			</fileset>
		</jar>
	  	<jar destfile="${jar_output_dir}/util.jar">
	  		<fileset dir="bin">
	  			<include name="org/jmodelica/util/*.class"/> 
	  		</fileset>
	  	 </jar>

	</target>

	<target name="compile-ast" depends="gen,gen-parser">
		<mkdir dir="bin"/>
		<!--<javac classpath="${beaver_dir}/lib/beaver.jar" compiler="javac1.5" debug="true" destdir="bin">-->
		<javac classpath="${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar" compiler="javac1.5" debug="true" destdir="bin"> 
			<src path="${base_output}"/>
			<src path="${modelica_front_end_path}/src/java"/>
			<include name = "**/*.java"/>
			<exclude name = "**/*junit*/**"/>
			<exclude name = "**/*.aj"/>
		</javac>
	</target>

	<!-- TARGET gen -->
	<target name="gen">
		<!-- create a directory for the generated files -->
		<mkdir dir="${base_output}"/>
		<!-- run jastadd to generate AST files -->
		<jastadd package="${ast_package}" doxygen="true" beaver="true" rewrite="true" outdir="${base_output}" NoCacheCycle="false" ComponentCheck="true" visitcheck="false" LazyMaps="true" Deterministic="true" NoStatic="false" Debug="false">
			<fileset dir="${local_ast}">
							<include name="*.ast"/>
							<include name="*.jadd"/>
							<include name="*.jrag"/>
						</fileset>
			<fileset dir="${ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${ast}/codegen">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
				<fileset dir="${ast}/test-framework">
					<include name="*.ast"/>
					<include name="*.jadd"/>
					<include name="*.jrag"/>
				</fileset>
			<fileset dir="${modelica_xml_back_end_ast}/codegen">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
				<exclude name="TestFramework.jrag"/>
			</fileset>
			<fileset dir="${modelica_xml_back_end_ast}/test-framework">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
				<exclude name="TestFramework.jrag"/>
			</fileset>
			<fileset dir="${modelica_c_back_end_ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${modelica_c_back_end_ast}/codegen">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${modelica_c_back_end_ast}/test-framework">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${modelica_compiler_ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>

			<fileset dir="${optimica_front_end_ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${optimica_xml_back_end_ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
				<exclude name="TestFramework.jrag"/>
			</fileset>
			<fileset dir="${optimica_xml_back_end_ast}/codegen">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
				<exclude name="TestFramework.jrag"/>
			</fileset>
			<fileset dir="${optimica_c_back_end_ast}">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>
			<fileset dir="${optimica_c_back_end_ast}/codegen">
				<include name="*.ast"/>
				<include name="*.jadd"/>
				<include name="*.jrag"/>
			</fileset>


		</jastadd>

	</target>
	
	<target name="gen-parser">
	
	    <!-- generate the scanner -->
        <concat destfile="${parser_output}/Modelica_all.flex" force="no">
          <filelist dir="." files="${parser}/Modelica_header.flex"/>
          <filelist dir="${jmodelica_parser}" files="Modelica.flex" />	
         </concat>
	    <jflex file="${parser_output}/Modelica_all.flex" outdir="${parser_output}" nobak="yes"/>

		<!-- generate the parser phase 1, translating .lalr to .beaver -->
        <concat destfile="${parser_output}/Modelica_all.parser" force="no">
          <filelist dir="." files="${parser}/Modelica_header.parser"/>
          <filelist dir="${jmodelica_parser}" files="Modelica.parser" />	
         </concat>
		<java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
	  	      <arg line="${parser_output}/Modelica_all.parser ${parser_output}/ModelicaParser_raw.beaver"/>
	  	    </java> 
	        <concat destfile="${parser_output}/ModelicaParser.beaver" force="no">
	          <filelist dir="." files="${jmodelica_parser}/beaver.input"/>
	          <filelist dir="${parser_output}" files="ModelicaParser_raw.beaver" />	
	         </concat>
	  	    <!-- generate the parser phase 2, translating .beaver to .java -->
	  	    <beaver file="${parser_output}/ModelicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes"/>
	  	  
	  	
        <concat destfile="${parser_output}/FlatModelica_all.flex" force="no">
          <filelist dir="." files="${parser}/FlatModelica_header.flex"/>
          <filelist dir="${jmodelica_parser}" files="FlatModelica.flex" />	
         </concat>
	  	    <jflex file="${parser_output}/FlatModelica_all.flex" outdir="${parser_output}" nobak="yes"/>

        <concat destfile="${parser_output}/FlatModelica_all.parser" force="no">
          <filelist dir="." files="${parser}/FlatModelica_header.parser"/>
          <filelist dir="${jmodelica_parser}" files="FlatModelica.parser" />	
         </concat>

		<!-- generate the parser phase 1, translating .lalr to .beaver -->
	  	 	   <java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
	  	  	      <arg line="${parser_output}/FlatModelica_all.parser ${parser_output}/FlatModelicaParser_raw.beaver"/>
	  	  	    </java> 
	  	        <concat destfile="${parser_output}/FlatModelicaParser.beaver" force="no">
	  	          <filelist dir="." files="${jmodelica_parser}/beaver.flat.input"/>
	  	          <filelist dir="${parser_output}" files="FlatModelicaParser_raw.beaver" />	
	  	         </concat>
	  	  	    <!-- generate the parser phase 2, translating .beaver to .java -->
	  	  	    <beaver file="${parser_output}/FlatModelicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes"/>

	    <!-- generate the scanner -->
	    <jflex file="${parser}/Optimica.flex" outdir="${parser_output}" nobak="yes"/>
	  	<!-- generate the parser phase 1, translating .lalr to .beaver -->
	       <concat destfile="${parser_output}/Modelica_all.parser" force="no">
	          <filelist dir="." files="${parser}/Optimica_header.parser"/>
	          <filelist dir="${jmodelica_parser}" files="Modelica.parser" />	
	         </concat>
		<concat destfile="${parser_output}/Optimica_cat.parser" force="no">
			          <filelist dir="${parser_output}" files="Modelica_all.parser"/>
			          <filelist dir="${parser}" files="Optimica.parser" />	
			         </concat>
			<java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
	  	      <arg line="${parser_output}/Optimica_cat.parser ${parser_output}/OptimicaParser_raw.beaver"/>
	  	    </java> 
	        <concat destfile="${parser_output}/OptimicaParser.beaver" force="no">
	          <filelist dir="." files="${jmodelica_parser}/beaver.input"/>
	          <filelist dir="${parser_output}" files="OptimicaParser_raw.beaver" />	
	         </concat>
	  	    <!-- generate the parser phase 2, translating .beaver to .java -->
	  	    <beaver file="${parser_output}/OptimicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes"/>

	</target>

	<!-- TARGET clean -->
	<target name="clean" depends="cleanGen">
		<!-- Delete all classfiles in dir and recursively in subdirectories -->
		<delete dir = "bin"/>
		<delete dir = "doc"/>
		<delete dir="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
	</target>

	<!-- TARGET cleanGen -->
	<target name="cleanGen">
		<!-- Delete the directory containing generated files and their class files -->
		<delete dir="${base_output}"/>
		<!--<delete dir="bin"/>-->
	</target>

	<!-- TARGET gen-test -->
	<target name="gen-test" depends="build">
		<!-- Generates teests -->
		<mkdir dir="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
	  	<java 
	  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
	  		      classpath=".:bin/OptimicaCompiler.jar:bin/util.jar:${beaver_dir}/lib/beaver.jar:bin/util.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar" 
	  		      fork="true" 
	  		      dir=".">
	  		    	<arg value="${modelica_front_end_path}/src/test/modelica/ArrayTests.mo"/>
	  		    	      <arg value="ArrayTests"/>
	  		    	      <arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
	  		    </java>
		 <java 
		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:bin/util.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar" 
		  		      fork="true" 
		  		      dir=".">
		  		    	<arg value="${optimica_front_end_path}/src/test/modelica/OptimicaTransformCanonicalTests.mo"/>
		  		    	      <arg value="OptimicaTransformCanonicalTests"/>
		  		    	      <arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		    </java>

		<java 
	      classname="org.jmodelica.optimica.compiler.JunitGenerator"
	      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
	      fork="true" 
	      dir="."
		  maxmemory="512M">
			<arg value="${modelica_front_end_path}/src/test/modelica/NameTests.mo"/>
			<arg value="NameTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>
		<java 
	  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
	  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
	  		      fork="true" 
	  		      dir=".">
			<arg value="${modelica_front_end_path}/src/test/modelica/ModificationTests.mo"/>
			<arg value="ModificationTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>

		<java 
	  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
	  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
	  		      fork="true" 
	  		      dir=".">
			<arg value="${modelica_front_end_path}/src/test/modelica/RedeclareTests.mo"/>
			<arg value="RedeclareTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>
		<java 
	  		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
	  		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
	  		  		      fork="true" 
	  		  		      dir=".">
			<arg value="${modelica_front_end_path}/src/test/modelica/ConnectTests.mo"/>
			<arg value="ConnectTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>
		<java 
 		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
 		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
 		  		      fork="true" 
 		  		      dir=".">
			<arg value="${modelica_front_end_path}/src/test/modelica/CodeGenTests.mo"/>
			<arg value="CodeGenTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>
	  	 <java 
		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
		  		      fork="true" 
		  		      dir=".">
		  		    	<arg value="${modelica_front_end_path}/src/test/modelica/TransformCanonicalTests.mo"/>
		  		    	      <arg value="TransformCanonicalTests"/>
		  		    	      <arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		    </java>
		<java 
 		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
 		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
 		  		      fork="true" 
 		  		      dir=".">
			<arg value="${modelica_c_back_end_path}/src/test/modelica/CCodeGenTests.mo"/>
			<arg value="CCodeGenTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>

	
		<!--
		<java 
 		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
 		  		      classpath=".:bin/ModelicaCompiler.jar:${beaver_dir}/lib/beaver.jar" 
 		  		      fork="true" 
 		  		      dir=".">
			<arg value="src/test/modelica/XMLCodeGenTests.mo"/>
			<arg value="XMLCodeGenTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>
	-->
		<java 
		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
		  		      fork="true" 
		  		      dir=".">
			<arg value="${modelica_front_end_path}/src/test/modelica/TypeTests.mo"/>
			<arg value="TypeTests"/>
			<arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		</java>

	  	 <java 
		  		      classname="org.jmodelica.optimica.compiler.JunitGenerator"
		  		      classpath=".:bin/OptimicaCompiler.jar:${beaver_dir}/lib/beaver.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" 
		  		      fork="true" 
		  		      dir=".">
		  		    	<arg value="${modelica_front_end_path}/src/test/modelica/TransformCanonicalTests.mo"/>
		  		    	      <arg value="TransformCanonicalTests"/>
		  		    	      <arg value="${test_junit-gen_dir}/org/jmodelica/test/junitgenerated"/>
		    </java>

		<javac compiler="javac1.5" debug="true" destdir="bin" 
	  		classpath=".:bin/ModelicaCompiler.jar:${beaver_dir}/lib/beaver.jar:${junit_dir}/junit-4.5.jar:../ModelicaCompiler/bin/ModelicaCompiler.jar:bin/util.jar" >
			<src path="${test_junit-gen_dir}/"/>
			<include name = "org/jmodelica/test/junitgenerated/*.java"/>
			<exclude name = "**/*.aj"/>
		</javac>
		<jar destfile="bin/junit-tests.jar">
			<fileset dir="bin">
				<include name="org/jmodelica/test/junitgenerated/*.class"/>
			</fileset>
		</jar>
	</target>

	<!-- TARGET test -->
	<target name="test" depends="gen-test">
		<!-- Run all tests in dir by using the TestAll java program -->
		<mkdir dir = "doc/junit-reports"/>
		<junit printsummary="yes"
			fork="true"
		    maxmemory="512M">
			<classpath>
				<fileset dir="${junit_dir}">
					<include name="junit-4.5.jar"/>
				</fileset>
				<fileset dir="bin">
					<include name="OptimicaCompiler.jar"/>
					<include name="junit-tests.jar"/>
					<include name="util.jar"/>
				</fileset>
				<fileset dir="../ModelicaCompiler/bin/">
					<include name="ModelicaCompiler.jar"/>
				</fileset>
				<fileset dir="${beaver_dir}/lib">
					<include name="beaver.jar"/>
				</fileset>
			</classpath>
			<formatter type="xml"/>
			<!-- <test todir="junit-reports"
  		    name="org.jmodelica.test.junitgenerated.NameTests"/> -->

			<batchtest fork="yes" todir="doc/junit-reports">
				<fileset dir="${test_junit-gen_dir}/">
					<include name="org/jmodelica/test/junitgenerated/*.java"/>
				</fileset>
			</batchtest>
		</junit>
		<junitreport todir="doc/junit-reports">
			<fileset dir="doc/junit-reports" includes="TEST-*.xml"/>
			<report todir="doc/junit-reports"/>
		</junitreport>
	</target>

</project>
