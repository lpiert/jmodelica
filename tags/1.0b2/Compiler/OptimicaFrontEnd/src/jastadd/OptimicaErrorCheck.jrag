/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect OptimicaErrorCheck {

  	public void InstOptClassDecl.collectErrors() {
    	super.collectErrors();
    	getFConstraintList().collectErrors();
    	getInstClassModificationOpt().collectErrors();
  	}

	public void FOptClass.checkFClassDimensions() {
		// Check dimensions of DAE
		int n_eq_F = numEquations() + numPathEqConstraints();
		int n_vars_F = numAlgebraicRealVariables() + numDifferentiatedRealVariables();
		if (n_eq_F!=n_vars_F) {
			error("The DAE system has " + n_eq_F + " equations and " + n_vars_F + " free variables.");
		}
		
		// Check dimensions of DAE initialization system
		// Notice that this check is stricter than in the Modelica case,
		// since here we really need initialization system to be balanced.
		// The F1 equations can in the simulation case be used to derive
		// initial conditions for the states, but for the simultaneous
		// optimization algorithm, this is not the case.
		int n_eq_F0 = numInitialEquations() + numEquations() + numPathEqConstraints();
		int n_vars_F0 = numAlgebraicRealVariables() + 2*numDifferentiatedRealVariables();
		if (n_eq_F0!=n_vars_F0) {
			error("The DAE initialization system has " + n_eq_F0 + " equations and " + n_vars_F0 + " free variables.");
		}
		
	}
  	
}