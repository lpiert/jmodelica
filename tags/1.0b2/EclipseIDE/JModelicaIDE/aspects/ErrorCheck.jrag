/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
aspect ErrorCheck {
	/**
	 * Returns the BaseClassDecl containing the given interval.
	 * 
	 *  @param off	offset of interval.
	 *  @param len	length of interval.
	 */
	syn BaseClassDecl ASTNode.containingClass(int off, int len) = null;
	eq StoredDefinition.containingClass(int off, int len) {
		for (Element e : getElements()) {
			BaseClassDecl res = e.containingClass(off, len);
			if (res != null)
				return res;
		}
		return null;
	}
	eq FullClassDecl.containingClass(int off, int len) {
		int end = off + (len == 0 ? 0 : len - 1);
		if (getBeginOffset() <= off && getEndOffset() >= end) {
			for (Element e : getClassDecls()) {
				BaseClassDecl res = e.containingClass(off, len);
				if (res != null)
					return res;
			}
			return this;
		} else {
			return null;
		}
	}
}