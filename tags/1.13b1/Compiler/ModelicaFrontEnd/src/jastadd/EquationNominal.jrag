/*
    Copyright (C) 2009-2014 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect EquationNominal{
    private void Equation.contributeNominal(FEquation equation) {
        AnnotationNode nominalNode = annotation().vendorNode().forPath("nominal");
        if (!nominalNode.exists())
            return;
        Exp valueExp = nominalNode.exp();
        if (valueExp == null) {
            nominalNode.ast().error("Nominal value is missing from nominal annotation");
            return;
        }
        
        FAttribute nominalAttr = new FInternalAttribute("Nominal", valueExp.instantiate());
        equation.addFAttribute(nominalAttr);
        
        AnnotationNode enabledAnnotation = nominalNode.forPath("enabled");
        Exp enabledExp = enabledAnnotation.exp();
        if (enabledExp != null)
            nominalAttr.addFAttribute(new FInternalAttribute("enabled", enabledExp.instantiate(), enabledAnnotation.isEachSet()));
    }
    
    protected void FAbstractEquation.typeCheckNominal(ErrorCheckType checkType) {
        FAttribute nominalAttr = findAttribute("Nominal");
        if (nominalAttr == null || !nominalAttr.hasValue())
            return;
        FExp exp = nominalAttr.getValue();
        FTypePrefixVariability variability = exp.variability();
        if (!variability.parameterOrLess())
            exp.error("Nominal expression should have parameter variability or less, %s has %s variability", exp, variability.toStringLiteral());
        Size size = size();
        Size expSize = exp.size();
        if (!size.equals(expSize))
            exp.error("Size of nominal expression %s is not the same size as the surrounding equation, size of expression %s, size of equation %s", exp, expSize, size);
        nominalAttr.getValue().typeCheck(checkType);
        FAttribute enabledAttr = nominalAttr.findAttribute("enabled");
        if (enabledAttr != null) {
            FExp enabledExp = enabledAttr.getValue();
            typeCheckHGTEnabled(checkType, enabledExp, type(), enabledAttr.hasFEach());
        }
    }
    
    syn FExp FAbstractEquation.getNominal() {
        FAttribute nominalAttr = findAttribute("Nominal");
        if (nominalAttr == null || !nominalAttr.hasValue())
            return null;
        FAttribute enabledAttr = nominalAttr.findAttribute("enabled");
        if (enabledAttr != null && enabledAttr.hasValue() && !enabledAttr.getValue().ceval().booleanValue())
            return null;
        return nominalAttr.getValue();
    }
    
    syn boolean FAbstractEquation.hasNominal() = getNominal() != null;
    
}