/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

aspect Connections {

	/* Machinery to manage connection sets */

	public ConnectionSetManager FClass.connectionSetManager = new ConnectionSetManager();
	
	public ConnectionSetManager FClass.getConnectionSetManager() {
		return connectionSetManager;
	}

	public void FClass.genConnectionEquations() {
	
		log.debug("<<<FClass.genConnectionEquations()");
		ArrayList<ConnectionSet> l = connectionSetManager.getConnectionSetList();
		log.debug("<<<FClass.genConnectionEquations(): "+ l.size() + " sets in manager");
		//getConnectionSetManager().printConnectionSets();
		//System.out.println(connectionSetManager.printConnectionSets());
		for (ConnectionSet set : l) {
			// TODO: use sorted data structure instead? would speed up use of ConnectionSetManager as well
			ConnectionSetEntry csee[] = set.toArray(new ConnectionSetEntry[set.size()]);
			java.util.Arrays.sort(csee);
			
			//log.debug("*** Array size: " + csee.length);
			
			if (csee[0].isFlow()) {
				// Treat first element separately
				FExp e = csee[0].getFQName().createFIdUseExp();
				if (csee[0].isOutside()) 
					e = new FNegExp(e);
			
				for (int j = 1; j < csee.length; j++) {
					FExp e2 = csee[j].getFQName().createFIdUseExp();
					e = csee[j].isOutside() ? new FSubExp(e, e2) : new FAddExp(e, e2);
				}
				FExp zero = csee[0].getVar().size().createZeroFExp();
				getFEquationBlock(0).addFAbstractEquation(new FEquation(e, zero));	
			} if (csee[0].isStream()) {
				// If the connection set has two member one being inside
				// and one being outside an additional equation should
				// be generated.
				if (csee.length==2) {
					if ((csee[0].isInside()&&csee[1].isOutside()) ||
							(csee[1].isInside()&&csee[0].isOutside())) {
						FExp e1 = csee[0].getFQName().createFIdUseExp();
						FExp e2 = csee[1].getFQName().createFIdUseExp();
						getFEquationBlock(0).addFAbstractEquation(new FEquation(e1,e2));
						FEquationBlock b = getFEquationBlock(0);
						int i = b.getNumChild();
					}
				}
			} else if (!csee[0].isStream() && !csee[0].isFlow()){
				// Treat first element separately
				FExp e1 = csee[0].getFQName().createFIdUseExp();
				for (int j = 1; j < csee.length; j++) {
					FExp e2 = csee[j].getFQName().createFIdUseExp();
					getFEquationBlock(0).addFAbstractEquation(new FEquation(e1, e2));
					e1 = e2;
				}
			}

		}

	}
		
	public void ASTNode.enableStreamsRewrite() {
		for (ASTNode n : this)
			n.enableStreamsRewrite();
	}
	
	public void FStreamBuiltIn.enableStreamsRewrite() {
		super.enableStreamsRewrite();
		rewriteStreams = true;
		is$Final = false;
	}
	
	protected boolean FStreamBuiltIn.rewriteStreams = false;

	
	/**
	 * \brief Rewrite the inSteam operator to equations. 
	 */
	rewrite FInStream {
		when (rewriteStreams) to FExp {
			// Always look for an inside occurrance of the stream variable.
			// If the variable is in an inside-inside connection, then
			// rewrite to an access. If it occurs in an inside-outside connection
			// rewrite to a new inStream operator operating on the outside
			// stream variable in the same connection set.
			FQName fqn = ((FIdUseExp) getFExp()).getFIdUse().getFQName().fullCopy();
			fqn.scalarized = false;
			String name = fqn.name();
			FArraySubscripts fas = fqn.hasFArraySubscripts() ? fqn.getFArraySubscripts() : null;
			ConnectionSet cs = myFClass().getConnectionSetManager().getConnectionSet(name, false);
			if (cs != null) {
				int N = 0; // # of inside
				for (ConnectionSetEntry cse : cs)
					if (cse.isInside())
						N++;
				int M = cs.size() - N; // # of outside
				
				Iterator<ConnectionSetEntry> it = cs.iterator();
				ConnectionSetEntry cse = it.next();
				if (N == 1 && M == 0) {
					return cse.getFQName().createFIdUseExp(fas);
				} else if (N == 1 && M == 1) {
					if (cse.isInside())
						cse = it.next();
					FInStream fis = new FInStream(cse.getFQName().createFIdUseExp(fas));
					fis.rewriteStreams = true;
					return fis;
				} else if (N == 2 && M == 0) {
					if (cse.name().equals(name))
						cse = it.next();
					return cse.getFQName().createFIdUseExp(fas);
				}
			}
			// TODO: should throw an exception here, but this situation is detected *after* this rewrite
			this.rewriteStreams = false;
			return this;
		}
	}

	rewrite FActualStream {
		when (rewriteStreams) to FExp {
			// 1) Get the stream variable name prefix
			FQName var_name = ((FIdUseExp)getFExp()).getFIdUse().getFQName();
			FQName prefix = var_name.copyFullPrefix();
			ArrayList<ConnectionSetEntry> cses = myFClass().getConnectionSetManager().
			  getFlowVariables(prefix.name());
			if (cses.size()==1) { // This condition should be checked earlier
				// 2) Get the name of the flow variable of the connector,
				//    there should only be one.
				FQName flow_var_name = cses.get(0).getFQName();
				// 3) Generate the expression
				FInStream then_exp = new FInStream(var_name.createFIdUseExp());
				then_exp.rewriteStreams = true; // Enable further rewrite of the inStream operator.
				FIdUseExp else_exp = var_name.createFIdUseExp();
				FGtExp guard = new FGtExp(flow_var_name.createFIdUseExp(),new FRealLitExp(0));
				FIfExp if_exp = new FIfExp(guard,then_exp,else_exp);
				return new FNoEventExp(if_exp);
			}
			throw new UnsupportedOperationException("Rewriting actualStream() for '" + var_name.name() + 
					"': found " + cses.size() + " matching flow variables");
		}
	}

	
	public class ConnectionSet extends LinkedHashSet<ConnectionSetEntry> {
		
		public int getNumInside() {
			int nInside = 0;
			for (ConnectionSetEntry cse : this) {
				if (cse.isInside()) {
					nInside++;
				}
			}
			return nInside;
		}
		
		public int getNumOutside() {
			int nOutside = 0;
			for (ConnectionSetEntry cse : this) {
				if (cse.isOutside()) {
					nOutside++;
				}
			}
			return nOutside;			
		}

		public ConnectionSetEntry getConnectionSetEntry(String name, boolean outside) {
			for (ConnectionSetEntry cse : this) {
				if (cse.equals(name, outside)) {
					return cse;
				}
			}
			return null;
		}
		
		/**
		 * Return all ConnectionSetEntrys corresponding to flow variables based
		 * on a name prefix. This method is useful when generating expressions
		 * for actualStream operators when the name of the flow variable in a
		 * stream connector is needed.
		 */
		public ArrayList<ConnectionSetEntry> getFlowVariables(String prefix) {
			ArrayList<ConnectionSetEntry> cses = new ArrayList<ConnectionSetEntry>();
			for (ConnectionSetEntry cse : this) 
				if (cse.isFlow() && cse.prefix().equals(prefix)) 
					cses.add(cse);
			return cses;
		}
		
		public int numStreamVariables() {
			int n_stream_vars = 0;
			for (ConnectionSetEntry e : this) {
				if (e.isStream()) {
					n_stream_vars++;
				}
			}
			return n_stream_vars;
		}
		
		public String toString() {
			StringBuffer str = new StringBuffer();
			
			str.append("Connection set (");
			if (this.iterator().next().isFlow()) {
				str.append("flow");
			} else if (this.iterator().next().isStream()) {
				str.append("stream");
			} else {
				str.append("potential");
			}
			str.append("): {");
			String set_str = super.toString();
			str.append(set_str.substring(1, set_str.length() - 1));
			str.append("}\n");
			return str.toString();
		}
		
	}

	static public class ConnectionSetManager {
	
		private ArrayList<ConnectionSet> list = new ArrayList<ConnectionSet>();
		
		public ArrayList<ConnectionSet> getConnectionSetList() {
			return list;
		}
			
		public void addInsideFlowVar(InstComponentDecl var1, FQName namePrefix1) {
			ConnectionSetEntry cse1 = new ConnectionSetEntry(var1, false, namePrefix1);
			
			if (getConnectionSet(cse1) == null) {
				ConnectionSet h = new ConnectionSet();
				h.add(cse1);
				list.add(h);
			}

		}
		
		public void addVars(InstComponentDecl var1, boolean outside1, FQName namePrefix1,
		                    InstComponentDecl var2, boolean outside2, FQName namePrefix2) {

//			log.debug("ConnectionSetManager.addVars");
		
//		    System.out.println(namePrefix1.name()+" . "+var1.name() + " outside: " + outside1);
//		    System.out.println(namePrefix2.name()+" . "+var2.name() + " outside: " + outside2);
		
			// Don't add parameters or constants to connection set
			if (var1.variability().parameterOrLess() || var2.variability().parameterOrLess()) 
				return;
			
			ConnectionSetEntry cse1 = new ConnectionSetEntry(var1,outside1,namePrefix1);
			ConnectionSetEntry cse2 = new ConnectionSetEntry(var2,outside2,namePrefix2);	

			ConnectionSet h1 = getConnectionSet(cse1);
			ConnectionSet h2 = getConnectionSet(cse2);
			
			if (h1!=null && h2==null)
				h1.add(cse2);
			else if (h2!=null && h1==null)
				h2.add(cse1);
			else if (h1!=null && h2!=null) {
				if (h1!=h2) {
					h1.addAll(h2);
					list.remove(h2);
				}
			} else {
				ConnectionSet h = new ConnectionSet();
				h.add(cse1);
				h.add(cse2);
				list.add(h);
			}
		}
	
		public ConnectionSet getConnectionSet(ConnectionSetEntry cse) {
			for (ConnectionSet set : list)
				if (set.contains(cse))
					return set;
			return null;
		}

		public ConnectionSet getConnectionSet(String name, boolean outside) {
			for (ConnectionSet set : list) {
				if (set.getConnectionSetEntry(name,outside)!=null) {
					return set;
				}		
			}
			return null;
		}

		public ArrayList<ConnectionSetEntry> getFlowVariables(String prefix) {
			ArrayList<ConnectionSetEntry> cses = new ArrayList<ConnectionSetEntry>();
			for (ConnectionSet set : list) {
				cses.addAll(set.getFlowVariables(prefix));		
			}
			return cses;
		}
		
		public String printConnectionSets() {
		
			StringBuffer str = new StringBuffer();
			
			str.append("Connection sets: " + list.size() + " sets\n");
			
			// Print connection sets 
			for(ConnectionSet set : list) {
				str.append(set);
			}

			return str.toString();
		}
	}

	
	static public class ConnectionSetEntry implements Comparable<ConnectionSetEntry> {
	
		private InstComponentDecl cd;
		private boolean outside;
		private FQName fqName;
		private String prefix;
		//private boolean flow;
		
		public ConnectionSetEntry(InstComponentDecl cd, boolean outside, FQName fqName) {
			//log.debug("Created ConnectionSetEntry: " + cd.getName().getID());
			this.cd = cd;
			this.outside = outside;
			this.fqName = fqName;
			boolean scalar = fqName.scalarized;
			fqName.scalarized = true;
			prefix = fqName.prefixName(-1);
			fqName.scalarized = scalar;
		}
	
		public boolean isOutside() {
			return outside;
		}

		public boolean isInside() {
			return !outside;
		}

		public InstComponentDecl getVar() {
			return cd;
		}
		
		public boolean isFlow() {
			return cd.getComponentDecl().isFlow();
		}

		public boolean isStream() {
			return cd.getComponentDecl().isStream();
		}
		
		public String name() {
		   return fqName.name();
		}
		
		public String prefix() {
			return prefix;
		}
		
		public boolean equals(String name, boolean outside) {
			return name.equals(this.name()) & (outside==this.outside);
		}
		
		public String toString() {
			return fqName.name() + (outside ? " (o)" : " (i)");
		}
		
		public FQName getFQName() {
			return fqName;
		}
				
		public int hashCode() {
			// TODO: Wouldn't toString().hashCode() be better?
			
			//log.debug("ConnectionSetEntry.hashCode");
			String tr = "true";
			String fa = "false";
			return name().intern().hashCode() + (outside? tr.intern().hashCode() : fa.intern().hashCode());
		}
		
		
		public boolean equals(Object o) {
			if (o instanceof ConnectionSetEntry) {
				ConnectionSetEntry cse = (ConnectionSetEntry) o;
				return name().equals(cse.name()) && outside == cse.isOutside();
			}
			return false;
		}
	
		public int compareTo(ConnectionSetEntry cse) {
			return name().compareTo(cse.name());
		}
	
	}
	
	
	public void InstAccess.connectTo(InstAccess right, FQName prefix, ConnectionSetManager csm) {
		FQName leftName  = flattenAndResolveIndices(prefix);
		FQName rightName = right.flattenAndResolveIndices(prefix);
		
		boolean leftOutside  = isOutsideConnector();
		boolean rightOutside = right.isOutsideConnector();
		
		if (isArray()) {
			Indices leftInd = indices();
			Indices rightInd = right.indices();
			Iterator<Index> leftIter = leftInd.iterator();
			Iterator<Index> rightIter = rightInd.iterator();
			while (leftIter.hasNext()) {
				Index leftI = leftInd.translate(leftIter.next());
				Index rightI = rightInd.translate(rightIter.next());
				FQName leftCellName = leftName.copyReplacingSubscripts(leftI);
				FQName rightCellName = rightName.copyReplacingSubscripts(rightI);
				InstComponentDecl leftComp  = lookupEvaluatingIndices();
				InstComponentDecl rightComp = right.lookupEvaluatingIndices();
				leftComp = leftComp.findCell(leftI, leftI.ndims() - leftComp.ndims());
				rightComp = rightComp.findCell(rightI, rightI.ndims() - rightComp.ndims());
				leftComp.connectTo(leftOutside, leftCellName, rightComp, rightOutside, rightCellName, csm);
			}
		} else {
			InstComponentDecl leftComp  = lookupEvaluatingIndices().findCell(leftName);
			InstComponentDecl rightComp = right.lookupEvaluatingIndices().findCell(rightName);
			leftComp.connectTo(leftOutside, leftName, rightComp, rightOutside, rightName, csm);
		}
	}
	
	public void InstComponentDecl.connectTo(boolean leftOutside, FQName leftName, 
			InstComponentDecl rightComp, boolean rightOutside, FQName rightName, ConnectionSetManager csm) {
		SortedSet<InstComponentDecl> rightChildren = rightComp.containedInstComponents();
		for (InstComponentDecl leftComp2 : containedInstComponents()) {
			InstComponentDecl rightComp2 = rightChildren.tailSet(leftComp2).first();
			FQName leftName2  = leftComp2.flattenedAccess(leftName);
			FQName rightName2 = rightComp2.flattenedAccess(rightName);
			leftComp2.connectTo(leftOutside, leftName2, rightComp2, rightOutside, rightName2, csm);
		}
	}

	public void InstPrimitive.connectTo(boolean leftOutside, FQName leftName, 
			InstComponentDecl rightComp, boolean rightOutside, FQName rightName, ConnectionSetManager csm) {
		csm.addVars(this, leftOutside, leftName, rightComp, rightOutside, rightName);
	}
	
	syn InstComponentDecl InstComponentDecl.findCell(Index i, int j) = 
		(j < i.ndims()) ? getInstComponentDecl(i.get(j) - 1).findCell(i, j + 1) : this;
	eq InstPrimitive.findCell(Index i, int j) = this;

	syn InstComponentDecl InstComponentDecl.findCell(FQName name) = 
		name.hasFArraySubscripts() ? findCell(name.getFArraySubscripts().asIndex(), 0) : this;

	syn boolean InstAccess.isOutsideConnector() = getFirstInstAccess().myInstComponentDecl().isConnector();
	
	syn boolean InstComponentDecl.isConnector() = myInstClass().isConnector();
	
	
	public void FAbstractEquation.buildConnectionSets(FQName prefix, ConnectionSetManager csm) { }
	
	public void FIfEquation.buildConnectionSets(FQName prefix, ConnectionSetManager csm) {
		try {
			if (getTest().ceval().booleanValue())
				for (FAbstractEquation equ : getFAbstractEquations())
				    equ.buildConnectionSets(prefix, csm);
		} catch (ConstantEvaluationException e) {
		}
	}
	
	public void InstForClauseE.buildConnectionSets(FQName prefix, ConnectionSetManager csm) {
		Indices indices = Indices.create(getInstForIndexs());
		for (Index i : indices) {
			int j = 0;
			int[] ii = indices.translate(i).index();
			for (InstForIndex fi : getInstForIndexs()) {
				fi.getInstPrimitive().setEvaluationValue(new CValueInteger(ii[j]), null);
				j++;
			}
			for (FAbstractEquation equ : getFAbstractEquations())
			    equ.buildConnectionSets(prefix, csm);
		}
	}
	
	public void FConnectClause.buildConnectionSets(FQName prefix, ConnectionSetManager csm) {
		if (!getConnector1().isDisabled() && !getConnector2().isDisabled()) {
			InstAccess left  = getConnector1().getInstAccess();
			InstAccess right = getConnector2().getInstAccess();
			left.connectTo(right, prefix, csm);
		}
	}

}