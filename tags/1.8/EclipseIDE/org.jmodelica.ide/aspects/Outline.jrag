/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.util.Arrays;
import java.util.Collections;
//import org.jmodelica.ide.ui.ImageLoader;

aspect Outline {
	/*
	 * Show interesting nodes
	 */
	eq Element.showInContentOutline() = !isError();
	eq ComponentDecl.showInContentOutline() = false;
	eq ExtendsClause.showInContentOutline() = false;
	eq ImportClause.showInContentOutline() = false;
	eq InstClassDecl.showInContentOutline() = true;
	eq InstComponentDecl.showInContentOutline() = true;
	eq SourceRoot.showInContentOutline() = true;
	
	// TODO: show components and extends in the content outline and the project explorer?
	
	
	/*
	 * Don't traverse into error nodes.
	 */
	eq BadDefinition.hasVisibleChildren() = false;
	eq BadClassDecl.hasVisibleChildren() = false;
	eq BadElement.hasVisibleChildren() = false;
	eq BadArgument.hasVisibleChildren() = false;
	
	
	/*
	 * Set labels of interesting nodes
	 */
	eq BaseClassDecl.contentOutlineLabel() = getName().getID();
	eq ComponentDecl.contentOutlineLabel() = getName().getID() + " : " + getClassName().typeString() + typePrefixes();
	eq BadDefinition.contentOutlineLabel() = "";
	eq LibNode.contentOutlineLabel() = getName();
	eq InstClassDecl.contentOutlineLabel() = getClassDecl().contentOutlineLabel();
	eq InstComponentDecl.contentOutlineLabel() = getComponentDecl().contentOutlineLabel();
	
	syn String Access.typeString() = getID();
	eq Dot.typeString() = getLeft().typeString() + "." + getRight().typeString();
	syn String ComponentDecl.typePrefixes() {
		StringBuilder buf = new StringBuilder();
		if (hasTypePrefixFlow()) {
			buf.append(", ");
			buf.append(getTypePrefixFlow());
		}
		if (hasTypePrefixVariability()) {
			buf.append(", ");
			buf.append(getTypePrefixVariability());
		}
		if (hasTypePrefixInputOutput()) {
			buf.append(", ");
			buf.append(getTypePrefixInputOutput());
		}
		return buf.toString();
	}
	
	
	/**
	 * Return the node that corresponds to the part of the code that we want to 
	 * select when selecting this node in the outline.
	 * @jastadd.client-api
	 */
	syn ASTNode ASTNode.getSelectionNode() = this;
	eq BaseClassDecl.getSelectionNode() = getName();
	eq ComponentDecl.getSelectionNode() = getName();
	eq InstClassDecl.getSelectionNode() = getClassDecl().getSelectionNode();
	eq InstComponentDecl.getSelectionNode() = getComponentDecl().getSelectionNode();

	
// This has been moved to ModelicaIcons.jrag	
//	/**
//	 * Return the image to show for this node in outline.
//	 * @jastadd.client-api
//	 */
//	eq ClassDecl.contentOutlineImage() = ImageLoader.getClassImage(getClassIconName(), false, false);
//	eq InstClassDecl.contentOutlineImage() = ImageLoader.getClassImage(getClassIconName(), false, true);
//	eq InstComponentDecl.contentOutlineImage() = ImageLoader.getClassImage(getClassIconName(), true, true);
//	eq ComponentDecl.contentOutlineImage() = ImageLoader.getFrequentImage(ImageLoader.COMPONENT_IMAGE);
	
	
	/**
	 * Return the class type for the node as defined by <pre>ImageLoader.*_CLASS</pre>, if applicable.
	 * @jastadd.client-api
	 */
	syn String ASTNode.getClassIconName() = null;
	eq BaseClassDecl.getClassIconName() = getRestriction().getClassIconName();
	eq InstClassDecl.getClassIconName() = getClassDecl().getClassIconName();
	eq InstComponentDecl.getClassIconName() = myInstClass().getClassIconName();
	eq InstPrimitive.getClassIconName() = ImageLoader.CLASS_CLASS;
	eq Block.getClassIconName() = ImageLoader.BLOCK_CLASS;
	eq MClass.getClassIconName() = ImageLoader.CLASS_CLASS;
	eq Connector.getClassIconName() = ImageLoader.CONNECTOR_CLASS;
	eq Function.getClassIconName() = ImageLoader.FUNCTION_CLASS;
	eq Model.getClassIconName() = ImageLoader.MODEL_CLASS;
	eq MPackage.getClassIconName() = ImageLoader.PACKAGE_CLASS;
	eq Record.getClassIconName() = ImageLoader.RECORD_CLASS;
	eq MType.getClassIconName() = ImageLoader.TYPE_CLASS;

	
	/*
	 * Lets us reorder nodes in outline. Higher category means show node later in tree.
	 */
	syn int ASTNode.outlineCategory()  = 0;
	eq BaseClassDecl.outlineCategory() = -1;
	eq InstClassDecl.outlineCategory() = -1;
	eq LibClassDecl.outlineCategory()  = name().equals("Modelica") && isTopLevel() ? -2 : -1;
	
	
	/*
     * To be able to sort nodes by declared order. We only need this for nodes that are shown in an outline.
     */
    syn int ASTNode.declareOrder()      = 0;
    eq ComponentDecl.declareOrder()     = myDeclaredIndex();
    eq InstComponentDecl.declareOrder() = myDeclaredIndex();
    eq InstClassDecl.declareOrder()     = myDeclaredIndex();
    eq ClassDecl.declareOrder()         = declaredIndex(this);
    
    inh int ClassDecl.declaredIndex(ClassDecl cd);
    eq FullClassDecl.getChild().declaredIndex(ClassDecl cd) {
    	if (cd.classesIndex() < 0)
    		classes();
    	return cd.classesIndex();
    }
    eq Program.getLibNode(int i).declaredIndex(ClassDecl cd)            = i;
    eq Program.getUnstructuredEntity(int i).declaredIndex(ClassDecl cd) = i + getNumLibNode();
    eq Program.getChild().declaredIndex(ClassDecl cd)                   = 0;
    eq BadDefinition.getChild().declaredIndex(ClassDecl cd)             = 0;

    inh int InstNode.myDeclaredIndex();
    inh int ClassDecl.myDeclaredIndex();
    inh int ComponentDecl.myDeclaredIndex();
    eq InstRoot.getInstClassDecl(int i).myDeclaredIndex()      = i;
    eq InstNode.getInstClassDecl(int i).myDeclaredIndex()      = i;
    eq InstNode.getInstComponentDecl(int i).myDeclaredIndex()  = i;
    eq FullClassDecl.getComponentDecl(int i).myDeclaredIndex() = i;
    eq BaseNode.getChild().myDeclaredIndex()                   = 0;
    
    
    // To compare nodes for opening paths and setting selections
    syn String ASTNode.outlineId()   = Integer.toHexString(hashCode());
    eq BaseClassDecl.outlineId()     = qualifiedName();
    eq ComponentDecl.outlineId()     = combineName(classNamePrefix(), name());
    eq InstClassDecl.outlineId()     = qualifiedName();
    eq InstComponentDecl.outlineId() = qualifiedName();

    
	syn lazy org.jmodelica.ide.outline.LoadedLibraries SourceRoot.loadedLibraries() = 
		new org.jmodelica.ide.outline.LoadedLibraries(this);
	
	private org.jmodelica.ide.outline.LoadedLibraries ClassDecl.loadedLibraries;
	public org.jmodelica.ide.outline.LoadedLibraries ASTNode.getLoadedLibraries() {
		return null;
	}
	public org.jmodelica.ide.outline.LoadedLibraries ClassDecl.getLoadedLibraries() {
		return loadedLibraries;
	}
	public void ASTNode.setLoadedLibraries(org.jmodelica.ide.outline.LoadedLibraries loadedLibraries) {
	}
	public void ClassDecl.setLoadedLibraries(org.jmodelica.ide.outline.LoadedLibraries loadedLibraries) {
		this.loadedLibraries = loadedLibraries;
	}
	
	
	syn boolean ASTNode.isInLibrary() = checkIsInLibrary();
	eq Root.isInLibrary()			  = false;
	eq InstClassDecl.isInLibrary()    = getClassDecl().isInLibrary();
	eq ClassDecl.isInLibrary()        = 
		(loadedLibraries != null) ? !loadedLibraries.filter(this) : checkIsInLibrary();
	inh boolean ASTNode.checkIsInLibrary();
	
	eq ClassDecl.getChild().checkIsInLibrary() = isInLibrary();
	eq Root.getChild().checkIsInLibrary()      = false;

	
	refine ContentOutline eq ASTNode.hasVisibleChildren() {
		for (ASTNode child : this) 
			if (child.showInContentOutline())
				return true;
		for (ASTNode child : this) 
			if (child.hasVisibleChildren())
				return true;
		return false;
	}
	
	// TODO: move laziness to interesting nodes?
	syn lazy boolean BaseNode.hasVisibleChildren() = super.hasVisibleChildren();
	
	eq SourceRoot.hasVisibleChildren() {
		for (StoredDefinition file : getProgram().getUnstructuredEntitys()) 
			if (file.getElements().getNumChild() > 0)
				return true;
		return loadedLibraries().hasFiltered();
	}
	eq SourceRoot.outlineChildren() {
    	ArrayList list = new ArrayList();
    	org.jmodelica.ide.outline.LoadedLibraries libs = loadedLibraries();
    	list.add(libs);
    	list.addAll(getProgram().getUnstructuredEntitys().outlineChildren());
    	if (libs.hasFiltered())
    		list.addAll(Arrays.asList(libs.getFiltered()));
     	return list;
	}
	
	eq LibNode.hasVisibleChildren() = getStoredDefinition().getNumElement() > 0;
	
	eq LibClassDecl.hasVisibleChildren() = true;
	eq LibClassDecl.outlineChildren() {
		ArrayList<ASTNode> res = new ArrayList<ASTNode>();
		for (ClassDecl cd : classes())
			if (cd.showInContentOutline())
				res.add(cd);
		return res;
	}
	
   	eq InstNode.hasVisibleChildren() {
   		/* Don't descend into error nodes. */
   		if (isError())
   			return false;
    	return getInstClassDecls().hasVisibleChildren() || getInstComponentDecls().hasVisibleChildren();
   	}
   	eq InstNode.outlineChildren() {
    	ArrayList list = new ArrayList();
    	list.addAll(getInstClassDecls().outlineChildren());
    	list.addAll(getInstComponentDecls().outlineChildren());
     	return list;
   	}


	public Object[] ASTNode.cachedOutlineChildren() {
		return outlineChildren().toArray();
	}

	public void ASTNode.updateOutlineCachedChildren() {}
	
	public boolean ASTNode.cachedOutlineChildrenIsCurrent() {
		return true;
	}
   	
   	// Add caching with delayed calculation for outline children
	public class BaseNode {
		
		private CachedOutlineChildren myCachedOutlineChildren = null;
		
		private CachedOutlineChildren myCachedOutlineChildren() {
			if (myCachedOutlineChildren == null)
				myCachedOutlineChildren = new CachedOutlineChildren();
			return myCachedOutlineChildren;
		}
		
		protected void copyCachedOutline(Object[] src, Object[] dst) {
			if (src != null && dst != null) {
				for (int s = 0, d = 0; s < src.length && d < dst.length; s++) {
					if (src[s] instanceof BaseNode) {
						BaseNode dn = null, sn = (BaseNode) src[s];
						boolean found = false;
						for (int i = d; !found && i < dst.length; i++) {
							if (dst[i] instanceof BaseNode) {
								dn = (BaseNode) dst[i];
								if (sn == dn || sn.outlineId().equals(dn.outlineId())) {
									d = i + 1;
									dn.copyCachedOutlineFrom(sn);
									found = true;
								}
							}
						}
					}
				}
			}
		}
		
		public void copyCachedOutlineFrom(BaseNode old) {
			if (this != old) {
				if (old.myCachedOutlineChildren != null && !myCachedOutlineChildren().iscurrent) 
					myCachedOutlineChildren.children = old.myCachedOutlineChildren.children;
				if (old.myCachedIcon != null && !myCachedIcon().iscurrent) 
					myCachedIcon.icon = old.myCachedIcon.icon;
			}
		}
		
		public Object[] cachedOutlineChildren() {
			return myCachedOutlineChildren().children;
		}
		
		public void updateOutlineCachedChildren() {
			myCachedOutlineChildren();
			try {
				Object[] children = outlineChildren().toArray();
				copyCachedOutline(myCachedOutlineChildren.children, children);
				myCachedOutlineChildren.children = children;
				for (Object ch : myCachedOutlineChildren.children) 
					if (ch instanceof BaseNode)
						((BaseNode) ch).hasVisibleChildren();
			} catch (Exception e) {
				// Can't really do anything constructive here
				e.printStackTrace();
			}
			myCachedOutlineChildren.iscurrent = true;
		}
		
		public boolean cachedOutlineChildrenIsCurrent() {
			return myCachedOutlineChildren != null && myCachedOutlineChildren.iscurrent;
		}
		
		public void flushAttributes() {
			super.flushAttributes();
			if (myCachedOutlineChildren != null)
				myCachedOutlineChildren.iscurrent = false;
		}

		private static class CachedOutlineChildren {
			public static final Object[] DEFAULT = new Object[] { "..." }; 
			
			public Object[] children = DEFAULT;
			public boolean iscurrent = false;
		}
	}
	
	private org.jmodelica.ide.outline.LoadedLibraries SourceRoot.oldLoadedLibraries;
	public void SourceRoot.setOldLoadedLibraries(org.jmodelica.ide.outline.LoadedLibraries oldLoadedLibraries) {
		this.oldLoadedLibraries = oldLoadedLibraries;
	}
	
	public void SourceRoot.updateOutlineCachedChildren() {
		if (oldLoadedLibraries != null) 
			copyCachedOutline(oldLoadedLibraries.getChildren(), loadedLibraries().getChildren());
		else
			loadedLibraries().hasChildren();
		oldLoadedLibraries = null;
		super.updateOutlineCachedChildren();
	}
	
	public void SourceRoot.copyCachedOutlineFrom(BaseNode old) {
		if (this != old && old instanceof SourceRoot) 
			setOldLoadedLibraries(((SourceRoot) old).loadedLibraries());
		super.copyCachedOutlineFrom(old);
	}
	
	public void SourceRoot.flushAttributes() {
		if (oldLoadedLibraries == null)
			setOldLoadedLibraries(loadedLibraries());
		super.flushAttributes();
	}
	
   	
	/*
	 * Explorer needs separate hasVisibleChildren, since it may need to load libraries.
	 */
	syn lazy boolean ClassDecl.hasClasses() = false;
	eq FullClassDecl.hasClasses() = getNumClassDecl() > 0;
	eq LibClassDecl.hasClasses() {
		if (super.hasClasses())
			return true;
		for (LibNode ln : getLibNodes()) {
			if (ln.getStoredDefinition().getNumElement() > 0)
				return true;
		}
		return false;
	}
	
	
	/*
	 * For opening files from instance tree.
	 */
	syn String ASTNode.containingFileName() = fileName();
	eq InstClassDecl.containingFileName() = getClassDecl().fileName();
	eq InstComponentDecl.containingFileName() = getComponentDecl().fileName();
	
	
	/*
	 * Prune uninteresting parts of tree.
	 */
	eq AbstractEquation.hasVisibleChildren() = false;
//	eq AbstractAlgorithm.hasVisibleChildren() = false;
	eq Access.hasVisibleChildren() = false;
	eq Annotation.hasVisibleChildren() = false;
	eq ComponentDecl.hasVisibleChildren() = false;
}