
/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file CGenerator.java
*  \brief CGenerator class.
*/


import java.io.*;

/**
 * OptimicaCGenerator extends the functionality of CGenerator to also
 * include C code generation of Optimica quantities.
 *
 */
public class OptimicaCGenerator extends CADGenerator {

	/**
	 * Number of free dependent parameters
	 */
	public class OptTag_C_numFreeDependentParameters extends OptTag {
		
		public OptTag_C_numFreeDependentParameters(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_free_dependent_real_parameters", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numFreeDependentRealParameters());
		}	
	}

	/**
	 * Number of pointwise penalty functions
	 */
	public class OptTag_C_numPointwisePenalty extends OptTag {
		
		public OptTag_C_numPointwisePenalty(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_j", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.objectiveExp()!=null? "1": "0");
		}	
	}

	/**
	 * Number of Lagrange integrands
	 */
	public class OptTag_C_numLagrangeIntegrand extends OptTag {
		
		public OptTag_C_numLagrangeIntegrand(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_l", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.objectiveIntegrandExp()!=null? "1": "0");
		}	
	}
	
	/**
	 * Number of path equality constraints
	 */
	public class OptTag_C_numPathEqConstraints extends OptTag {
		
		public OptTag_C_numPathEqConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_ceq", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numPathEqConstraints());
		}	
	}

	/**
	 * Number of path inequality constraints
	 */
	public class OptTag_C_numPathIneqConstraints extends OptTag {
		
		public OptTag_C_numPathIneqConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_cineq", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numPathLeqConstraints() + " + " + 
					foptclass.numPathGeqConstraints());
		}	
	}

	/**
	 * Number of point equality constraints
	 */
	public class OptTag_C_numPointEqConstraints extends OptTag {
		
		public OptTag_C_numPointEqConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_heq", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numPointEqConstraints());
		}	
	}

	/**
	 * Number of point inequality constraints
	 */
	public class OptTag_C_numPointIneqConstraints extends OptTag {
		
		public OptTag_C_numPointIneqConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_hineq", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numPointLeqConstraints() + " + " + 
					foptclass.numPointGeqConstraints());
		}	
	}

	/**
	 * Number of point time points
	 */
	public class OptTag_C_numTimePoints extends OptTag {
		
		public OptTag_C_numTimePoints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("n_tp", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			genPrinter.print(foptclass.numTimePoints());
		}	
	}
	
	/**
	 * C: macros for C point variable aliases
	 */
	public class OptTag_C_pointVariableAliases extends OptTag {
		
		public OptTag_C_pointVariableAliases(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_point_variable_aliases", myGenerator, foptclass);
		}
	
		private void generateVar(CodeStream genPrinter, FVariable fv, String offset, int index) {
			genPrinter.print("#define ");
			genPrinter.print(fv.name_C());
			genPrinter.print("_p_(j) ((*(jmi->z))[jmi->offs_");
			genPrinter.print(offset);
			genPrinter.print("+j*(jmi->n_real_dx + jmi->n_real_x + jmi->n_real_u + jmi->n_real_w) +");
			genPrinter.print(index);
			genPrinter.print("])\n");
		}

		private void generateVarList(CodeStream genPrinter, Iterable<? extends FVariable> list, String offset) {
			int index = 0;
			for (FVariable fv : list)
				generateVar(genPrinter, fv, offset, index++);
		}

		public void generate(CodeStream genPrinter) {
			generateVarList(genPrinter, foptclass.derivativeVariables(),             "real_dx_p");
			generateVarList(genPrinter, foptclass.differentiatedRealVariables(),     "real_x_p");
			generateVarList(genPrinter, foptclass.realInputs(),                      "real_u_p");
			generateVarList(genPrinter, foptclass.algebraicContinousRealVariables(), "real_w_p");
		}
	}

	/**
	 * C: dependent parameter residuals
	 */
	public class OptTag_C_FreeDependentParameterResiduals extends OptTag {
		
		public OptTag_C_FreeDependentParameterResiduals(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_dependent_free_parameter_residuals", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			for (FAbstractEquation e : foptclass.freeDependentParameterEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			Enumerator enumerator = new Enumerator();
			for (FAbstractEquation e : foptclass.freeDependentParameterEquations()) {
				e.genResidual_C(ASTNode.printer_C, genPrinter, INDENT, enumerator, null, null);
			}
		}
	}
	
	/**
	 * C: cost function
	 */
	public class OptTag_C_CostFunction extends OptTag {
		
		public OptTag_C_CostFunction(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_cost_function", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			FExp obj = foptclass.objectiveExp();
			if (obj != null) {
				obj.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				genPrinter.print(INDENT + "(*res)[0] = ");
				obj.prettyPrint_C(genPrinter,"");
				genPrinter.print(";\n");
			}
		}
	}

	/**
	 * C: Lagrange integrand function
	 */
	public class OptTag_C_LagrangeIntegrandFunction extends OptTag {
		
		public OptTag_C_LagrangeIntegrandFunction(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_lagrange_integrand", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			FExp obj = foptclass.objectiveIntegrandExp();
			if (obj != null) {
				obj.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				genPrinter.print(INDENT + "(*res)[0] = ");
				obj.prettyPrint_C(genPrinter,"");
				genPrinter.print(";\n");
			}
		}
	}

	
	/**
	 * C: path equality constraints
	 */
	public class OptTag_C_PathEqualityConstraints extends OptTag {
	
		public OptTag_C_PathEqualityConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_path_equality_constraints", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			for (FConstraint c : foptclass.pathEqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintEq c : foptclass.pathEqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	/**
	 * C: path inequality constraints (leq)
	 */
	public class OptTag_C_PathInequalityConstraints extends OptTag {
		
		public OptTag_C_PathInequalityConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_path_inequality_constraints", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			for (FConstraint c : foptclass.pathLeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FConstraint c : foptclass.pathGeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintLeq c : foptclass.pathLeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
			for (FConstraintGeq c : foptclass.pathGeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	/**
	 * C: point equality constraints
	 */
	public class OptTag_C_PointEqualityConstraints extends OptTag {
		
		public OptTag_C_PointEqualityConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_point_equality_constraints", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			for (FConstraint c : foptclass.pointEqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintEq c : foptclass.pointEqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	/**
	 * C: point inequality constraints (leq)
	 */
	public class OptTag_C_PointInequalityConstraints extends OptTag {
		
		public OptTag_C_PointInequalityConstraints(AbstractGenerator myGenerator, FOptClass foptclass) {
			super("C_Opt_point_inequality_constraints", myGenerator, foptclass);
		}
	
		public void generate(CodeStream genPrinter) {
			for (FConstraint c : foptclass.pointLeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FConstraint c : foptclass.pointGeqConstraints()) 
				c.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FConstraintLeq c : foptclass.pointLeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
			for (FConstraintGeq c : foptclass.pointGeqConstraints()) {
				c.genResidual_C(i, INDENT, genPrinter);				
				i++;
			}
		}	
	}

	
	/**
	 * Constructor.
	 * 
	 * @param expPrinter Printer object used to generate code for expressions.
	 * @param escapeCharacter Escape characters used to decode tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public OptimicaCGenerator(Printer expPrinter, char escapeCharacter,
			FOptClass fclass) {
		super(expPrinter,escapeCharacter, fclass);
	}
}

