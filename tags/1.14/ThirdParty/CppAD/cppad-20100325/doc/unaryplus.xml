<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>AD Unary Plus Operator</title>
<meta name="description" id="description" content="AD Unary Plus Operator"/>
<meta name="keywords" id="keywords" content=" unary Ad plus operator + "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_unaryplus_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="arithmetic.xml" target="_top">Prev</a>
</td><td><a href="unaryplus.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>ADValued</option>
<option>Arithmetic</option>
<option>UnaryPlus</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADValued-&gt;</option>
<option>Arithmetic</option>
<option>std_math_ad</option>
<option>MathOther</option>
<option>CondExp</option>
<option>Discrete</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Arithmetic-&gt;</option>
<option>UnaryPlus</option>
<option>UnaryMinus</option>
<option>ad_binary</option>
<option>compute_assign</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>UnaryPlus-&gt;</option>
<option>UnaryPlus.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>x</option>
<option>y</option>
<option>Operation Sequence</option>
<option>Derivative</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>AD Unary Plus Operator</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>


<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;+&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Performs the unary plus operation
(the result <i>y</i> is equal to the operand <i>x</i>).


<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The operand <i>x</i> has one of the following prototypes
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The result <i>y</i> has type
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>It is equal to the operand <i>x</i>.

<br/>
<br/>
<b><big><a name="Operation Sequence" id="Operation Sequence">Operation Sequence</a></big></b>
<br/>
This is an AD of <i>Base</i>
<a href="glossary.xml#Operation.Atomic" target="_top"><span style='white-space: nowrap'>atomic&#xA0;operation</span></a>

and hence is part of the current
AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.

<br/>
<br/>
<b><big><a name="Derivative" id="Derivative">Derivative</a></big></b>
<br/>
If 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>f</mi>
</mrow></math>

 is a 
<a href="glossary.xml#Base Function" target="_top"><span style='white-space: nowrap'>Base&#xA0;function</span></a>
,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<mo stretchy="false">[</mo>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>x</mi>
</mrow>
</mfrac>
<mo stretchy="false">=</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>x</mi>
</mrow>
</mfrac>
</mrow></math>

<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="unaryplus.cpp.xml" target="_top"><span style='white-space: nowrap'>UnaryPlus.cpp</span></a>

contains an example and test of this operation.


<hr/>Input File: cppad/local/unary_plus.hpp

</body>
</html>
