<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="ch_models"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Working with Models</title>

  <section xml:id="models_sec_introduction">
    <title>Introduction to models</title>

    <para>Modelica and Optimica models can be compiled and loaded in the
    JModelica.org Python interface as model objects. These model objects can
    then be used for simulation and optimization purposes. This chapter will
    cover how to compile Modelica and Optimica models, set compiler parameters
    and options, load the compiled model in a Python model object and use the
    model object to perform model manipulations such as setting and getting
    parameters.</para>
  </section>

  <section xml:id="models_sec_compilation">
    <title>Compilation</title>

    <para>In its simplest form, compilation only requires a few lines of code;
    importing a compiler function from JModelica.org and specifying a Modelica
    or Optimica model and file location. This will be demonstrated in <xref
    linkend="models_sec_simple_compilation_example" />. For more advanced
    usage there are compiler options and parameters which can be modified,
    this will be explained in <xref linkend="models_sec_compiler_settings" />.
    The last section in this part, <xref
    linkend="models_sec_compilation_in_more_detail" />, will go through the
    compilation process and how to compile a model step-by-step.</para>

    <section xml:id="models_sec_simple_compilation_example">
      <title>Simple compilation example</title>

      <para>The following steps compiles a model in the JModelica.org Python
      interface:<procedure>
          <step>
            <para>Import the JModelica.org compiler function
            <literal>compile_jmu</literal> from
            <literal>jmodelica.jmi</literal>.</para>
          </step>

          <step>
            <para>Specify the model and model file.</para>
          </step>

          <step>
            <para>Perform the compilation.</para>
          </step>
        </procedure></para>

      <para>This is demonstrated in the following code example.<programlisting
      language="python"># import the compiler function
from jmodelica.jmi import compile_jmu

# specify Modelica model and model file
model_name = 'myPackage.myModel'
mo_file = 'myPackage.mo'

# compile the model, return argument is the file name of the JMU
compile_jmu(model_name, mo_file)
&gt;&gt; '.\\myPackage_myModel.jmu'
</programlisting></para>

      <para>Once compilation has completed successfully a JMU file will have
      been created on the file system. The JMU file is essentially a
      compressed file containing files created during compilation that are
      needed when instantiating a model object. Return argument for
      <literal>compile_jmu</literal> is the full file path of the JMU that has
      just been created, this will be useful later when we want to create
      model objects. More about the JMU file and loading models can be found
      in <xref linkend="models_sec_loading_models" />.</para>

      <para>In the above example, compilation has been performed with default
      parameters and options. The only parameters specified are model name and
      file. <literal>compile_jmu</literal> has several other parameters which
      can be modified. The different parameters, their default values and
      interpretation will be explained in <xref
      linkend="models_sec_compiler_settings" />.</para>
    </section>

    <section xml:id="models_sec_compiler_settings">
      <title>Compiler settings</title>

      <section xml:id="models_sec_compile_jmu_parameters">
        <title>compile_jmu parameters</title>

        <para>The <literal>compile_jmu</literal> parameters can be listed with
        the interactive help. The compiler target, which is set with the
        parameter <literal>target</literal>, is further explained in <xref
        linkend="models_sec_compiler_targets" />.<programlisting># display the docstring for compile_jmu with the Python command 'help'
from jmodelica.jmi import compile_jmu
help(compile_jmu)

Help on function compile_jmu in module jmodelica.jmi:

compile_jmu(class_name, file_name=[], compiler='modelica', target='ipopt', 
            compiler_options={}, compile_to='.')
    Compile a Modelica or Optimica model to a JMU.

    A model class name must be passed, all other arguments have default values.
    The different scenarios are:

    * Only class_name is passed:
        - Default compiler is ModelicaCompiler.
        - Class is assumed to be in MODELICAPATH.

    * class_name and file_name is passed:
        - file_name can be a single file as a string or a list of file_names
          (strings).
        - Default compiler is ModelicaCompiler but will switch to
          OptimicaCompiler if a .mop file is found in file_name.

    Library directories can be added to MODELICAPATH by listing them in a
    special compiler option 'extra_lib_dirs', for example:

        compiler_options =
            {'extra_lib_dirs':['c:\MyLibs\MyLib1','c:\MyLibs\MyLib2']}

    Other options for the compiler should also be listed in the compiler_options
    dict.

    The compiler target is 'ipopt' by default which means that libraries for AD
    and optimization/initialization algortihms will be available as well as the
    JMI. The other targets are:

        'model' --
            AD and JMI is included.
        'algorithm' --
            AD and algorithm but no Ipopt linking.
        'model_noad' --
            Only JMI, that is no AD interface. (Must currently be used when
            model includes external functions.)

    Parameters::

        class_name --
            The name of the model class.

        file_name --
            Model file (string) or files (list of strings), can be both .mo or
            .mop files.
            Default: Empty list.

        compiler --
            'modelica' or 'optimica' depending on whether a ModelicaCompiler or
            OptimicaCompiler should be used. Set this argument if default
            behaviour should be overridden.
            Default: Depends on argument file_name.

        target --
            Compiler target. 'model', 'algorithm', 'ipopt' or 'model_noad'.
            Default: 'ipopt'

        compiler_options --
            Options for the compiler.
            Default: Empty dict.

        compile_to --
            Specify location of the compiled jmu. Directory will be created if
            it does not exist.
            Default: Current directory.

    Returns::

        Name of the JMU which has been created.
</programlisting></para>
      </section>

      <section xml:id="models_sec_compiler_options">
        <title>Compiler options</title>

        <para>Compiler options are read from an XML file,
        <filename>options.xml</filename>, which can be found in the
        JModelica.org installation folder under the folder
        <filename>Options</filename>. The options are loaded from the file
        when a compiler is created, that is when
        <literal>compile_jmu</literal> is run. Options for a compiler instance
        can be modified by editing the file before compiling. There are four
        type categories: string, real, integer and boolean. The available
        options, default values and description are listed in <xref
        linkend="models_tab_compiler_options" />.<table
            xml:id="models_tab_compiler_options">
            <title>Compiler options</title>

            <tgroup cols="3">
              <colspec align="left" colname="col–para" colwidth="2.75*" />

              <colspec align="left" colname="col–def" colwidth="1.25*" />

              <colspec align="left" colname="col–descr" colwidth="3.25*" />

              <thead>
                <row>
                  <entry align="center">Option</entry>

                  <entry align="center">Default</entry>

                  <entry align="center">Description</entry>
                </row>
              </thead>

              <tbody>
                <row>
                  <entry><literal>normalize_minimum_time_problems</literal></entry>

                  <entry><literal>true</literal></entry>

                  <entry>When this option is set to <literal>true</literal>
                  the minimum time optimal control problems encoded in
                  Optimica are converted to fixed interval problems by scaling
                  of the derivative variables. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>enable_variable_scaling</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is <literal>true</literal>, then the
                  "nominal" attribute will be used to scale variables in the
                  model. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>halt_on_warning</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is set to <literal>false</literal> one
                  or more compiler warnings will not stop compilation of the
                  model. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>automatic_add_initial_equations</literal></entry>

                  <entry><literal>true</literal></entry>

                  <entry>When this option is set to <literal>true</literal>,
                  then additional initial equations are added to the model
                  based on a the result of a matching algorithm. Initial
                  equations are added for states that are not matched to an
                  equation. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>generate_fmi_xml</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is <literal>true</literal> the model
                  description part of the XML variables file will be FMI
                  compliant. To generate an XML which will validate with FMI
                  schema the option <literal>generate_xml_equations</literal>
                  must also be set to <literal>false</literal>. (Boolean
                  option.)</entry>
                </row>

                <row>
                  <entry><literal>eliminate_alias_variables</literal></entry>

                  <entry><literal>true</literal></entry>

                  <entry>If this option is set to <literal>true</literal>,
                  then alias variables are eliminated from the model. (Boolean
                  option.)</entry>
                </row>

                <row>
                  <entry><literal>extra_lib_dirs</literal></entry>

                  <entry><literal>""</literal> (Empty string)</entry>

                  <entry>The value of this option is appended to the value of
                  the <literal>MODELICAPATH</literal> environment variable for
                  determining in what directories to search for libraries.
                  (String option.)</entry>
                </row>

                <row>
                  <entry><literal>generate_xml_equations</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is <literal>true</literal>, then model
                  equations are generated in XML format. (Boolean
                  option.)</entry>
                </row>

                <row>
                  <entry><literal>state_start_values_fixed</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>This option enables the user to specify if initial
                  equations should be generated automatically for
                  differentiated variables even though the fixed attribute is
                  equal to <literal>fixed</literal>. Setting this option to
                  <literal>true</literal> is, however, often practical in
                  optimization problems. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>equation_sorting</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is <literal>true</literal>, equations
                  are sorted using the BLT algorithm. (Boolean
                  option.)</entry>
                </row>

                <row>
                  <entry><literal>index_reduction</literal></entry>

                  <entry><literal>false</literal></entry>

                  <entry>If this option is <literal>true</literal>, index
                  reduction is performed. (Boolean option.)</entry>
                </row>

                <row>
                  <entry><literal>enable_symbolic_diagnosis</literal></entry>

                  <entry><literal>true</literal></entry>

                  <entry>Enable this option to invoke the structural error
                  diagnosis based on the matching algorithm. (Boolean
                  option.)</entry>
                </row>
              </tbody>
            </tgroup>
          </table></para>
      </section>

      <section xml:id="models_sec_compiler_targets">
        <title>Compiler targets</title>

        <para>There are four compiler targets available:<itemizedlist>
            <listitem>
              <para><literal>ipopt</literal>: Compiled model will include JMI
              interface, AD and linking to Ipopt libraries. There is support
              for optimization and initialization algorithm.</para>
            </listitem>

            <listitem>
              <para><literal>model</literal>: Compiled model will include
              support for JMI interface and AD.</para>
            </listitem>

            <listitem>
              <para><literal>algorithm</literal>: Compiled model will include
              support for JMI interface. AD and algorithm but not link with
              the Ipopt libraries.</para>
            </listitem>

            <listitem>
              <para><literal>model_noad</literal>: Compiled model will only
              include the JMI interface.</para>
            </listitem>
          </itemizedlist></para>

        <para>The <literal>compile_model</literal> parameter
        <literal>target</literal> is '<literal>ipopt</literal>' by default
        which will work for most cases. However, if JModelica.org has been
        built without Ipopt libraries the <literal>target</literal> parameter
        would have to be changed to any other suitable target that does not
        include the Ipopt libraries. The target <literal>model_noad</literal>
        must be used if the model contains external equations since external
        equations do not work with the AD interface at the moment.</para>
      </section>
    </section>

    <section xml:id="models_sec_compilation_in_more_detail">
      <title>Compilation in more detail</title>

      <para>Compiling with <literal>compile_jmu</literal> bundles quite a few
      steps required for the compilation. These steps will be described
      briefly here, for a more detailed review on the compilation steps see
      <xref linkend="intro_sec_architecture" /> in <xref
      linkend="ch_intro" />.</para>

      <section>
        <title>Create a compiler</title>

        <para>A compiler, can be either a Modelica or Optimica compiler, is
        created by importing the Python classes from the compiler module. The
        compiler constructors do not take any arguments. This example code
        will create a Modelica compiler.<programlisting language="python"># import the class ModelicaCompiler from the compiler module
from jmodelica.compiler import ModelicaCompiler

# create a compiler instance
mc = ModelicaCompiler()
</programlisting></para>
      </section>

      <section>
        <title>Source tree generation and flattening</title>

        <para>In the first step of the compilation, the model is parsed and
        instantiated. Then the model is transformed into a flat representation
        which can be used to generate C and XML code. If there are errors in
        the model, for example syntax or type errors, Python exceptions will
        be thrown during these steps. <programlisting language="python"># Parse the model and get a reference to the source root
source_root = mc.parse_model('myPackage.mo')

# Generate an instance tree representation and get a reference to the model instance
model_instance = mc.instantiate_model(source_root, 'myPackage.myModel')

# Perform flattening and get a flat representation
flat_rep = oc.flatten_model(model_instance)
</programlisting></para>
      </section>

      <section>
        <title>Code generation</title>

        <para>The next step is the code generation which produces C code
        containing the model equations and a couple of XML files containing
        model meta data such as variable names and types and parameter values.
        <programlisting language="python"># Generate code
mc.generate_code(flat_rep)
</programlisting></para>
      </section>

      <section>
        <title>Generate a shared object file</title>

        <para>Finally, the shared object file is built where the C code is
        linked with the JModelica.org Model Interface (JMI) runtime library.
        The <literal>compile_binary</literal> method takes one obligatory
        parameter, the name of the C file that was generated in the previous
        step. The parameter <literal>target</literal> must also be set here if
        something other than the default <literal>'model'</literal> is wanted.
        In this example, <literal>target</literal> is changed to
        <literal>'ipopt'</literal>. For more information on targets, see <xref
        linkend="models_sec_compiler_targets" />.<programlisting
        language="python"># Compile a shared object file
c_file = 'myPackage_myModel.c'
mc.compile_binary(c_file, target='ipopt')
</programlisting></para>
      </section>
    </section>
  </section>

  <section xml:id="models_sec_loading_models">
    <title>Loading models</title>

    <para>Compiled models, JMUs, are loaded in the JModelica.org Python
    interface with the <literal>JMUModel</literal> class from the
    <literal>jmodelica.jmi</literal> module. This will be demonstrated in
    <xref linkend="models_sec_loading_a_jmu" />. The
    <literal>JMUModel</literal> class contains many methods with which the
    model can be manipulated once it has been instantiated. Amongst the most
    important methods are the <literal>initialize</literal>,
    <literal>simulate</literal> and <literal>optimize</literal> methods. These
    are explained in <xref linkend="ch_simulation" /> and <xref
    linkend="ch_optimization" />. The more basic <literal>JMUModel</literal>
    methods for variable and parameter manipulation are explained in <xref
    linkend="models_sec_variable_and_parameter_manipulation" />.</para>

    <section xml:id="models_sec_the_jmu_file">
      <title>The JMU file</title>

      <para>The JMU file is a compressed file which contains all files needed
      to load and work with the compiled model in JModelica.org. The JMU
      contains the shared object file, the XML files with model variable and
      parameter data and some other files created during compilation of the
      model. The JMU file format is a JModelica.org specific format but is
      designed to follow the FMU file format from the FMI standard as much as
      possible. The JMU file is created when compiling with
      <literal>jmodelica.jmi.compile_jmu</literal>, see <xref
      linkend="models_sec_compilation" />.</para>
    </section>

    <section xml:id="models_sec_loading_a_jmu">
      <title>Loading a JMU</title>

      <para>A JMU file is loaded in JModelica.org with the class
      <literal>JMUModel</literal> in the <literal>jmodelica.jmi</literal>
      module. The following simple example demonstrates how to do this in a
      Python shell or script.<programlisting language="python"
      xml:space="preserve"># import JMUModel from jmodelica.jmi
from jmodelica.jmi import JMUModel
myModel = JMUModel('myPackage_myModel.jmu')
</programlisting></para>

      <para>The only parameter in the <literal>JMUModel</literal> constructor
      is the name of the JMU file, including any file path. When compiling and
      loading it is therefore practical to use the return argument from
      <literal>compile_jmu</literal>, which is the path to the JMU created.
      The following example demonstrates this.<programlisting
      language="python"># import compile_jmu and JMUModel
from jmodelica.jmi import compile_jmu
from jmodelica.jmi import JMUModel

# compile and load model
jmu_name = compile_jmu('myPackage.myModel','myPackage.mo')
myModel = JMUModel(jmu_name)
</programlisting></para>
    </section>

    <section>
      <title>Loading an FMU</title>

      <para>An FMU (Functional Mock-up Unit) is a compressed file which
      follows the FMI (Functional Mock-up Interface) standard. The FMU file
      can be loaded in JModelica.org with the class
      <literal>FMUModel</literal> in the <literal>jmodelica.fmi</literal>
      module. The following short example demonstrates how to do this in a
      Python shell or script.<programlisting># import FMUModel from jmodelica.fmi
from jmodelica.fmi import FMUModel
myModel = FMUModel('myFMU.fmu')
</programlisting></para>

      <para>The FMUModel instance can then be used to set parameters and used
      for simulations. Read more about the FMI and FMU in <xref
      linkend="ch_fmi" />. How to simulate an FMU is described in <xref
      linkend="ch_simulation" />.</para>
    </section>
  </section>

  <section xml:id="models_sec_variable_and_parameter_manipulation">
    <title>Variable and parameter manipulation</title>

    <para>Model variables and parameters can be manipulated with methods in
    <literal>JMUModel</literal> once the model has been loaded. Some short
    examples in <xref linkend="models_sec_setting_and_getting_variables" />
    will demonstrate this. Model variable meta data and parameter values are
    saved in XML files which are generated during compilation, these are
    briefly explained in <xref
    linkend="models_sec_model_variable_XML_files" />. The XML file containing
    the parameters can be used to save different sets of parameters for one
    model, see <xref
    linkend="models_sec_loading_and_saving_parameters" />.</para>

    <section xml:id="models_sec_model_variable_XML_files">
      <title>Model variable XML files</title>

      <para>The model variable meta data and parameter values are saved in XML
      files which are generated during the compilation. They follow the name
      convention:</para>

      <itemizedlist>
        <listitem>
          <para><filename>&lt;model class name&gt;.xml</filename></para>
        </listitem>

        <listitem>
          <para><filename>&lt;model class
          name&gt;_values.xml</filename></para>
        </listitem>
      </itemizedlist>

      <para>The variable meta data is saved in <filename>&lt;model class
      name&gt;.xml</filename> and the parameter values in <filename>&lt;model
      class name&gt;_values.xml</filename>. The name of the parameter is used
      to map a parameter value in the XML values file to a parameter
      specification in the XML variables file.</para>
    </section>

    <section xml:id="models_sec_setting_and_getting_variables">
      <title>Setting and getting variables</title>

      <para>The model variables can be accessed with via the
      <literal>jmi.JMUModel</literal> interface. It is possible to set and get
      one specific variable at a time or a whole list of variables.</para>

      <para>The following code example demonstrates how to get and set a
      specific variable using an example model from the
      <literal>jmodelica.examples</literal> package.<programlisting># compile and load the model
from jmodelica.jmi import compile_jmu
from jmodelica.jmi import JMUModel
jmu_name = compile_jmu('RLC_Circuit','RLC_Circuit.mo')
rlc_circuit = JMUModel(jmu_name)

# get the value of the variable 'resistor.R'
resistor_r = rlc_circuit.get('resistor.R')
resistor_r
&gt;&gt; 1.0

# give 'resistor.R' a new value
resistor_r = 2.0
rlc_circuit.set('resistor.R', resistor_r)
rlc_circuit.get('resistor.R')
&gt;&gt; 2.0
</programlisting></para>

      <para>The following example demonstrates how to get and set a list of
      variables using the same example model as above. The model is assumed to
      already be compiled and loaded.<programlisting># create a list of variables and values
vars = ['resistor.R', 'resistor.v', 'capacitor.C', 'capacitor.v']
values = rlc_circuit.get(vars)
values
&gt;&gt; [2.0, 0.0, 1.0, 0.0]

# change some of the values
values[0] = 3.0
values[3] = 1.0
rlc_circuit.set(vars, values)
rlc_circuit.get(vars)
&gt;&gt; [3.0, 0.0, 1.0, 1.0]
</programlisting></para>
    </section>

    <section xml:id="models_sec_loading_and_saving_parameters">
      <title>Loading and saving parameters</title>

      <section>
        <title>Loading XML values file</title>

        <para>It is possible to (re)load the parameter values from an XML file
        as is done automatically when the <literal>jmi.JMUModel</literal>
        object was first created. If, for example, there were many local
        changes to parameters it could be desirable to reset everything as it
        was from the beginning. The following example shows how reloading the
        parameter values from the XML file resets the parameters in the model.
        The model is taken from the <literal>jmodelica.examples</literal>
        package and is assumed to be compiled and loaded.</para>

        <programlisting language="python"># look at parameters 'resistor.R' and 'sine.offset'
rlc_circuit.get('resistor.R)
&gt;&gt; 1.0
rlc_circuit.get('sine.offset')
&gt;&gt; 0.0

# change them
rlc_circuit.set('resistor.R', 2.0)
rlc_circuit.set('sine.offset', 0.5)

# look at them again
rlc_circuit.get('resistor.R)
&gt;&gt; 2.0
rlc_circuit.get('sine.offset')
&gt;&gt; 0.5

# reset them by loading the original XML values file
rlc_circuit.load_parameters_from_XML()

# 'resistor.R' and 'sine.offset' have now been reset
rlc_circuit.get('resistor.R)
&gt;&gt; 1.0
rlc_circuit.get('sine.offset')
&gt;&gt; 0.0
</programlisting>

        <para>The default behaviour is to load the same file as was created
        during compilation. If another file should be used this must be passed
        as an argument to the method.</para>

        <programlisting language="python"># Load other XML file
rlc_circuit.load_parameters_from_XML('new_values.xml')
</programlisting>
      </section>

      <section>
        <title>Writing to XML values file</title>

        <para>Setting a parameter value with <literal>JMUModel.set</literal>
        only changes the value in the vector loaded when
        <literal>jmi.JMUModel</literal> was created, which means that it will
        not be saved when the model is discarded. To save all local changes
        made to the model parameters, the values have to be written to the XML
        values file.</para>

        <programlisting language="python"># set a parameter
rlc_circuit.set('inductor.L', 1.5)

# Save parameters to the XML values file
rlc_circuit.write_parameters_to_XML()

# load the XML values file once again and see that the changed parameter was saved in 
# the XML file
rlc_circuit.load_parameters_from_XML()
rlc_circuit.get('inductor.L')
&gt;&gt; 1.5
</programlisting>

        <para>If <literal>write_parameters_to_XML()</literal> is called
        without arguments the values will be written to the XML values file in
        the JMU which was created when the model was compiled (following the
        name conventions mentioned above). It is also possible to save the
        changes in a new XML file. This is quite convenient since different
        parameter value settings can then easily be saved and reloaded in the
        model.</para>

        <programlisting language="python"># Save to specific XML file
rlc_circuit.write_parameters_to_XML('test_values.xml')
</programlisting>
      </section>
    </section>
  </section>
</chapter>
