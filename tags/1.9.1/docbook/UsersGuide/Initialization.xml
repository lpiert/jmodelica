<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="ch_initialization"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Initialization</title>

  <section>
    <title>Solving DAE initialization problems</title>

    <para>Before a model can be simulated it must be initialized, i.e.
    consistent initial values must be computed. To do this, JModelica.org
    supplies the JMUModel member function <literal>initialize</literal>, which
    initializes the JMUModel. The function is called after compiling and
    creating a JMUModel:</para>

    <programlisting language="python"># Compile the stationary initialization model into a DLL
from pymodelica import compile_jmu
model_name = compile_jmu("My.Model", "/path/to/MyModel.mo")

# Load a model instance into Python
from pyjmi import JMUModel
init_model = JMUModel(model_name)
    
# Solve the DAE initialization system
init_result = init_model.initialize()
</programlisting>

    <para>The JMUModel instance <literal>init_model</literal> is now
    initialized and is ready to be simulated.</para>

    <para>The interactive help for the initialize method is shown by the
    command:</para>

    <programlisting>&gt;&gt;&gt; help(init_model.initialize)   
    The initialization method depends on which algorithm is used, this can 
    be set with the function argument 'algorithm'. Options for the algorithm 
    are passed as option classes or as pure dicts. See 
    JMUModel.initialize_options for more details.
        
    The default algorithm for this function is IpoptInitializationAlg. 
        
    Parameters::
        
        algorithm --
            The algorithm which will be used for the initialization is 
            specified by passing the algorithm class as string or class 
            object in this argument. 'algorithm' can be any class which 
            implements the abstract class AlgorithmBase (found in 
            algorithm_drivers.py). In this way it is possible to write own 
            algorithms and use them with this function.
            Default: 'IpoptInitializationAlg'
            
        options -- 
            The options that should be used in the algorithm. For details on 
            the options do:
            
                &gt;&gt; myModel = JMUModel(...)
                &gt;&gt; opts = myModel.initialize_options()
                &gt;&gt; opts?
    
            Valid values are: 
                - A dict which gives IpoptInitializationAlgOptions with 
                  default values on all options except the ones listed in 
                  the dict. Empty dict will thus give all options with 
                  default values.
                - An options object.
            Default: Empty dict
    
    Returns::
        
        Result object, subclass of algorithm_drivers.ResultBase.
</programlisting>

    <para>Options for the available initialization algorithms can be set by
    first retrieving an options object using the <literal>JMUModel</literal>
    method <literal>initialize_options:</literal><programlisting>&gt;&gt;&gt; help(init_model.initialize_options)
    Get an instance of the initialize options class, prefilled with default 
    values. If called without argument then the options class for the 
    default initialization algorithm will be returned.
    
    Parameters::
    
        algorithm --
            The algorithm for which the options class should be fetched. 
            Possible values are: 'IpoptInitializationAlg', 'KInitSolveAlg'.
            Default: 'IpoptInitializationAlg'
            
    Returns::
    
        Options class for the algorithm specified with default values.
</programlisting>Having solved the initialization problem, the result of the
    initialization can be retrieved from the return result object:</para>

    <programlisting language="python">x = init_result['x']
y = init_result['y']
</programlisting>
  </section>

  <section>
    <title>How JModelica.org creates the initialization system of
    equations</title>

    <para>To find a set of consistent initial values a system of non-linear
    equations, called the system of initialization equations, is solved. This
    system is composed from the DAE equations, the initial equations, some
    resulting from start attributes with the fixed attribute set to true.
    Start attributes with the fixed attribute set to false are treated as
    initial guesses for the numerical algorithm used to solve the
    initialization problem</para>

    <para>Some initialization algorithms require the system of initial
    equations to be well defined in the sense that the number of variables
    must be equal to the number of equations. If this is not the case,
    the</para>

    <itemizedlist>
      <listitem>
        <para>If the number of equations is greater than the number of
        variables the system is overdetermined. Such a system may not have a
        solution, and will be treated as ill-defined. An exception is thrown
        in this case.</para>
      </listitem>

      <listitem>
        <para>If the number of equations is less than the number of variables
        the system is underdetermined and such a system has infinitely many
        solutions. In this case, the compiler tries to balance the system by
        setting some fixed attributes to <literal>true</literal>. So if the
        user supplies too few initial conditions, some variables with the
        attribute <literal>fixed</literal> set to <literal>false</literal> may
        be changed to <literal>true</literal> during initialization.</para>
      </listitem>
    </itemizedlist>
  </section>

  <section>
    <title>Initialization algorithms</title>

    <section>
      <title>Initialization using IPOPT</title>

      <para>JModelica.org provides a method for DAE initialization that is
      based on IPOPT, the mathematical formulation of the algorithm can be
      found in the <link
      xlink:href="http://www.jmodelica.org/api-docs/jmi/">JMI API
      documentation</link>. Notice that this algorithm does not rely on the
      causalization procedure (in particular the BLT transformation) which is
      common. Instead, the DAE residual is introduced as an equality
      constraint when solving an optimization problem where the squared
      difference between the non-fixed start values and their corresponding
      variables are minimized. As a consequence, the algorithm relies on
      decent start values for <emphasis>all</emphasis> variables. This
      approach is generally more sensitive to lacking initial guesses for
      start values than are algorithms based on causalization.</para>

      <para>The algorithm provides the options summarized in <xref
      linkend="init-tab-collopts"/>.</para>

      <table xml:id="init-tab-collopts">
        <title>Options for the collocation-based optimization
        algorithm</title>

        <tgroup cols="3">
          <colspec align="left" colname="col–opt" colwidth="1*"/>

          <colspec align="left" colname="col–desc" colwidth="1*"/>

          <colspec align="left" colname="col–def" colwidth="2*"/>

          <thead>
            <row>
              <entry align="center">Option</entry>

              <entry align="center">Default</entry>

              <entry align="center">Description</entry>
            </row>
          </thead>

          <tbody>
            <row>
              <entry><literal>result_file_name</literal></entry>

              <entry>Empty string (default generated file name will be
              used)</entry>

              <entry>Specifies the name of the file where the optimization
              result is written. Setting this option to an empty string
              results in a default file name that is based on the name of the
              optimization class.</entry>
            </row>

            <row>
              <entry><literal>result_format</literal></entry>

              <entry>'txt'</entry>

              <entry>Specifies in which format to write the result. Currently
              only textual mode is supported.</entry>
            </row>

            <row>
              <entry><literal>write_scaled_result</literal></entry>

              <entry>False</entry>

              <entry>Write the scaled optimization result if set to true. This
              option is only applicable when automatic variable scaling is
              enabled. Only for debugging use.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>

      <para>In addition to the options for the collocation algorithm, IPOPT
      options can also be set by modifying the dictionary
      <literal>IPOPT_options</literal> contained in the collocation algorithm
      options object. Here, all valid IPOPT options can be specified, see the
      <link xlink:href="http://www.coin-or.org/Ipopt/documentation/">IPOPT
      documentation</link> for further information. For example, setting the
      option <literal>max_iter</literal>:</para>

      <programlisting language="python">opts['IPOPT_options']['max_iter'] = 300
</programlisting>

      <para>makes IPOPT terminate after 300 iterations even if no optimal
      solution has been found.</para>

      <para>Some statistics from IPOPT can be obtained by issuing the
      command:</para>

      <programlisting>&gt;&gt;&gt; res_init.solver.init_opt_ipopt_get_statistics()
</programlisting>

      <para>The return argument of this function can be found by using the
      interactive help:</para>

      <programlisting>&gt;&gt;&gt; help(res_init.solver.init_opt_ipopt_get_statistics)
    Get statistics from the last optimization run.
    
    Returns::
    
        return_status -- 
            The return status from IPOPT.
            
        nbr_iter -- 
            The number of iterations. 
            
        objective -- 
            The final value of the objective function.
            
        total_exec_time -- 
            The execution time.
</programlisting>
    </section>

    <section>
      <title>Initialization using KInitSolveAlg</title>

      <para>JModelica.org also provides a method for DAE initialization based
      on the non-linear equation solver KINSOL from the SUNDIALS suite. KINSOL
      is currently comprised in the <link
      xlink:href="http://www.jmodelica.org/page/199">Assimulo</link> package,
      included when installing JModelica.org. KINSOL is based on Newtons
      method for solving non-linear equations and is thus locally convergent.
      Attempts are made to make KInitSolveAlg as robust as possible but the
      possibility of finding a local minimum instead of the solution still
      remains. If the solution found by KInitSolveAlg is a local minimum a
      warning will be printed. The initial guesses passed to KINSOL are the
      ones supplied as start attributes in the current Modelica model.</para>

      <para>KInitSolveAlg also implements an improved linear solver connected
      to KINSOL. This linear solver implements Tikhonov regularization to
      handle the problems of singular Jacobians as well as support for
      SuperLU, an efficient sparse linear solver.</para>

      <para>The options providable are summarized in <xref
      linkend="tab_options"/>.</para>

      <table xml:id="tab_options">
        <title>Options for KInitSolveAlg</title>

        <tgroup cols="3" colsep="1">
          <thead>
            <row>
              <entry align="center">Option</entry>

              <entry align="center">Default</entry>

              <entry align="center">Description</entry>
            </row>
          </thead>

          <tbody>
            <row>
              <entry><literal>use_constraints</literal></entry>

              <entry>False</entry>

              <entry>A flag indicating whether constraints are to be used
              <emphasis>during</emphasis> initialization. Further explained in
              <xref linkend="sec_const_usage"/>.</entry>
            </row>

            <row>
              <entry><literal>constraints</literal></entry>

              <entry>None</entry>

              <entry>A <literal>numpy.array</literal> containing floats that,
              when supplied, defines the constraints on the variables. Further
              explained in <xref linkend="sec_const_usage"/>.</entry>
            </row>

            <row>
              <entry><literal>result_format</literal></entry>

              <entry>'txt'</entry>

              <entry>Specifies in which format to write the result. Currently
              only textual mode is supported.</entry>
            </row>

            <row>
              <entry><literal>result_file_name</literal></entry>

              <entry>Empty string (default generated file name will be
              used)</entry>

              <entry>Specifies the name of the file where the optimization
              result is written. Setting this option to an empty string
              results in a default file name that is based on the name of the
              optimization class.</entry>
            </row>

            <row>
              <entry><literal>KINSOL_options</literal></entry>

              <entry>A dictionary with the defalt KINSOL options</entry>

              <entry>These are the options sent to the KINSOL solver. These
              are reviewed in detail in <xref
              linkend="kinsol_options"/>.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>

      <table xml:id="kinsol_options">
        <title>Options for KINSOL contained in the
        <literal>KINSOL_options</literal> dictionary</title>

        <tgroup cols="3">
          <thead>
            <row>
              <entry align="center">Options</entry>

              <entry align="center">Default</entry>

              <entry align="center">Descriptions</entry>
            </row>
          </thead>

          <tbody>
            <row>
              <entry><literal>use_jac</literal></entry>

              <entry>True</entry>

              <entry>Flag indicating whether or not KINSOL uses the jacobian
              supplied by JModelica.org (True) or if KINSOL evaluates the
              Jacobian through finite differences (False). Finite differences
              is currently not available in sparse mode.</entry>
            </row>

            <row>
              <entry>sparse</entry>

              <entry>False</entry>

              <entry>Flag indicating whether the problem should be treated as
              sparse (True) or dense (False).</entry>
            </row>

            <row>
              <entry>verbosity</entry>

              <entry>0</entry>

              <entry>An integer regulating the level of output from KINSOL.
              Further explained in <xref linkend="sec_verbosity"/>.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>

      <section xml:id="sec_const_usage">
        <title>The use of constraints</title>

        <para>KINSOL, and hence also KInitSolvAlg, only support simple
        unilateral constraints, that is constraining a variable to being
        positive or negative. If the option <literal>use_constraints</literal>
        is set to True, constraints are use. What constraints that are used
        depends on whether or not the user has supplied constraints with the
        <literal>constraints</literal> option. If set, these will be used
        otherwise constraints will be computed by reading the min and max
        attributes from the Modelica file. How the constraint array is written
        is summarized in <xref linkend="constraints_array"/>.</para>

        <table pgwide="0" xml:id="constraints_array">
          <title>Values allowed in the <literal>constraints</literal>
          array</title>

          <tgroup cols="2">
            <thead>
              <row>
                <entry align="center">Value</entry>

                <entry align="center">Constraint</entry>
              </row>
            </thead>

            <tbody>
              <row>
                <entry>0.0</entry>

                <entry>Unconstrained.</entry>
              </row>

              <row>
                <entry>1.0</entry>

                <entry>Greater than, or equal to, zero.</entry>
              </row>

              <row>
                <entry>2.0</entry>

                <entry>Greater than zero.</entry>
              </row>

              <row>
                <entry>-1.0</entry>

                <entry>Less than, or equal to, zero.</entry>
              </row>

              <row>
                <entry>-2.0</entry>

                <entry>Less than zero.</entry>
              </row>
            </tbody>
          </tgroup>
        </table>

        <para>When the constraints are read from the Modelica file the value
        from <xref linkend="constraints_array"/> most fitting to the min and
        max values is chosen. For example a variable with min set to 3.2 and
        max set to 5.6 is constrained to be greater than zero. When the
        algorithm is finished however the result will be compared with the min
        and max values from the model testing if the solution fulfills the
        constraints set by the Modelica file.</para>
      </section>

      <section>
        <title xml:id="sec_verbosity">Verbosity of KINSOL</title>

        <para>There are four different levels of verbosity in KINSOL with 0
        being "silent" and 3 being the "loudest". What is output is reviewed
        in <xref linkend="verbosity"/>.</para>

        <table xml:id="verbosity">
          <title>Verbosity levels in KINSOL</title>

          <tgroup cols="2">
            <thead>
              <row>
                <entry align="center">Verbosity level</entry>

                <entry align="center">Output</entry>
              </row>
            </thead>

            <tbody>
              <row>
                <entry>0</entry>

                <entry>No information displayed.</entry>
              </row>

              <row>
                <entry>1</entry>

                <entry>In each nonlinear iteration the following information
                is displayed: the scaled Euclidean norm of the residual at the
                current iterate, the scaled Euclidian norm of the Newton step
                as well as the number of function evaluations performed so
                far.</entry>
              </row>

              <row>
                <entry>2</entry>

                <entry>Level 1 output as well as the Euclidian and in finity
                norm of the <emphasis>scaled</emphasis> residual at the
                current iterate</entry>
              </row>

              <row>
                <entry>3</entry>

                <entry>Level 2 output plus additional values used by the
                global strategy as well as statistical information from the
                linear solver.</entry>
              </row>
            </tbody>
          </tgroup>
        </table>
      </section>
    </section>
  </section>
</chapter>
