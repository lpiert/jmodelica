<project name="jmodelica_graphics" default="site" xmlns:a4e="antlib:org.ant4eclipse">
	
	<property name="graphical_root" value="../org.jmodelica.ide.graphical" />
	
	<property name="jmodelica_root" value="../JModelica" />
	<property name="jmodelica_build" value="${jmodelica_root}/Compiler/ModelicaCompiler/build.xml" />
	<property name="jmodelica_optimica_build" value="${jmodelica_root}/Compiler/OptimicaCompiler/build.xml" />
	
	<property name="ide_root" value="../org.jmodelica.ide" />
	<property name="ide_build" value="${ide_root}/build.xml" />
	<property name="ide_site" value="${ide_root}/site" />
	<property name="ide_java_dir" value="${ide_root}/java" />
	<property name="ide_test_dir" value="${ide_root}/tests" />
	<property name="ide_lib_dir" value="${ide_root}/lib" />
	<property name="ide_class_dir" value="${ide_root}/bin/classes" />
	

	<property name="core_dir" value="../org.jastadd.plugin" />
	<property name="core_java_dir" value="${core_dir}/src" />

	<property name="site_dir" value="../site" />

	<property name="bin_dir" value="bin" />
	<property name="output_dir" location="site" />
	<property name="java_dir" value="src" />
	
	<property name="graphic.project" value="org.jmodelica.ide.graphical" />
	<property name="graphic.feature.project" value="org.jmodelica.ide.graphical.feature" />
	
	<path id="plugin_classpath">
		<pathelement location="${ide_lib_dir}/beaver-rt.jar" />
		<fileset dir="${eclipse_path}/plugins">
			<include name="**/*.jar" />
			<exclude name="**/org.apache.ant*.jar"/>
		</fileset>
	</path>
	
	<macrodef name="jm_buildPlugin" description="creates jar for a single plug-in project">
		<attribute name="projectName" />
		<attribute name="path" default="../@{projectName}" />
		<attribute name="revPath" default="@{path}" />

		<sequential>
			<!-- read build.properties -->
			<property file="@{path}/build.properties" prefix="@{projectName}" />

			<!-- copy binary includes -->
			<copy todir="${bin_dir}/bundles/@{projectName}">
				<fileset dir="@{path}" includes="${@{projectName}.bin.includes}">
					<filename name="." negate="true" />
				</fileset>
			</copy>
			
			<!-- get svn revision number -->
			<exec executable="svnversion" outputproperty="jm.@{projectName}.revision">
				<arg value="-c"/>
				<arg value="@{revPath}"/>
			</exec>

			<!-- extract version and id -->
			<taskdef name="pluginVersion" classname="org.jmodelica.ant.ExtractPluginVersion" classpath="${ide_root}/${bin_dir}/classes" />
			<pluginVersion plugin="@{projectName}" 
				path="${graphical_root}/${bin_dir}/bundles/@{projectName}/META-INF/MANIFEST.MF" 
				datadir="${graphical_root}/${bin_dir}" revision="${jm.@{projectName}.revision}"/>
			<property file="${bin_dir}/plugin.properties" prefix="jm" />
			
			<!-- build jar -->
			<jar destfile="${bin_dir}/plugins/${jm.@{projectName}.jar}" 
				basedir="${bin_dir}/bundles/@{projectName}" 
				manifest="${bin_dir}/bundles/@{projectName}/META-INF/MANIFEST.MF" />
		</sequential>
	</macrodef>
	
	<macrodef name="jm_buildFeature" description="creates jar for a single feature project">
		<attribute name="projectName" />

		<sequential>
			<!-- read build.properties -->
			<property file="../@{projectName}/build.properties" prefix="@{projectName}" />

			<!-- copy binary includes -->
			<copy todir="${bin_dir}/bundles/@{projectName}">
				<fileset dir="../@{projectName}" includes="${@{projectName}.bin.includes}">
					<filename name="." negate="true" />
				</fileset>
			</copy>

			<!-- patch the feature.xml -->
			<taskdef name="patchFeature" classname="org.jmodelica.ant.PatchFeature" classpath="${ide_root}/${bin_dir}/classes" />
			<patchFeature feature="@{projectName}" 
				path="${graphical_root}/${bin_dir}/bundles/@{projectName}/feature.xml" 
				datadir="${graphical_root}/${bin_dir}" />
			<property file="${bin_dir}/feature.properties" prefix="jm" />
			<!-- TODO: automatically set download and installation sizes? -->

			<!-- build jar -->
			<jar destfile="${bin_dir}/features/${jm.@{projectName}.jar}" 
				basedir="${bin_dir}/bundles/@{projectName}" />
		</sequential>
	</macrodef>
	
	<macrodef name="jm_buildSite" description="builds the update site">
		<attribute name="dir" />
		<attribute name="name" />

		<sequential>
			<!-- update site.xml -->
			<taskdef name="patchSite" classname="org.jmodelica.ant.PatchSite" classpath="${ide_root}/${bin_dir}/classes" />
			<patchSite path="@{dir}/site.xml" datadir="${graphical_root}/${bin_dir}" />

			<!-- build site -->
			<pathconvert property="launcher">
				<path id="launcher.path">
					<fileset dir="${eclipse_path}/plugins">
						<include name="org.eclipse.equinox.launcher_*.jar" />
					</fileset>
				</path>
			</pathconvert>
			<java jar="${launcher}" fork="true" timeout="10800000" taskname="p2" failonerror="false" maxmemory="256m">
				<arg line=" -application org.eclipse.equinox.p2.publisher.UpdateSitePublisher" />
				<arg line=" -metadataRepository file:@{dir}" />
				<arg line=" -artifactRepository file:@{dir}" />
				<arg line=" -source @{dir}/ -configs ANY" />
				<arg line=" -metadataRepositoryName &quot;@{name} plugins&quot;" />
				<arg line=" -artifactRepositoryName &quot;@{name} artifacts&quot;" />
				<arg line=" -publishArtifacts -compress" />
			</java>
		</sequential>
	</macrodef>
	
	<macrodef name="emptyDir" description="creates or empties directory">
		<attribute name="dir" />
		<sequential>
			<delete dir="@{dir}" quiet="true" />
			<mkdir dir="@{dir}" />
		</sequential>
	</macrodef>
	
	<!-- remove compiled files -->
	<target name="clean">
		<delete dir="${bin_dir}" />
		<delete dir="${output_dir}" />
	</target>
	
	<target name="compileIde">
		<ant antfile="${ide_build}" target="compile" inheritAll="false" useNativeBasedir="true" />
	</target>

	<!-- compile files -->
	<target name="compile" depends="compileIde">
		<mkdir dir="${bin_dir}/classes" />
		<javac compiler="javac1.6" debug="true" destdir="${bin_dir}/classes">
			<classpath path="${ide_class_dir}"/>
			<classpath refid="plugin_classpath" />
			<src path="${java_dir}" />
			<include name="**/*.java" />
		</javac>
	</target>
	
	<target name="site" depends="clean, compile">
		<ant antfile="${ide_build}" target="site" inheritAll="false" useNativeBasedir="true" />
		
		<emptyDir dir="${output_dir}" />
		
		<copy todir="${output_dir}">
			<fileset file="${ide_site}/**">
				<exclude name="artifacts.jar" />
				<exclude name="content.jar" />
			</fileset>
		</copy>
		
		<!-- collect class files for org.jmodelica.graphical.editor -->
		<copy todir="${bin_dir}/bundles/${graphic.project}">
			<fileset dir="${bin_dir}/classes">
				<include name="**/*" />
			</fileset>
		</copy>
		
		<!-- build jars for plugins -->
		<emptyDir dir="${bin_dir}/plugins" />
		<jm_buildPlugin projectName="${graphic.project}" />
		
		<!-- build jars for features -->
		<emptyDir dir="${bin_dir}/features" />
		<jm_buildFeature projectName="${graphic.feature.project}" />
		
		<!-- build update site -->
		<copy todir="${output_dir}">
			<fileset dir="${bin_dir}">
				<include name="plugins/**/*"/>
				<include name="features/**/*"/>
			</fileset>
		</copy>
		<jm_buildSite dir="${output_dir}" name="JModelica" />
	</target>
	
</project>