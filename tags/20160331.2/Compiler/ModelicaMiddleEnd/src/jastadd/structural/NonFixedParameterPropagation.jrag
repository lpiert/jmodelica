/*
    Copyright (C) 2009-2015 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect NonFixedParametersPropagation {

    private boolean FClass.nonFixedParametersPropagated = false;

    syn boolean FClass.nonFixedParametersPropagated() = nonFixedParametersPropagated;

    public class FClass {
        /**
         * Propagate non-fixed parameters.
         * 
         * This transformation sets a parameter to fixed=false if it depends on
         * a parameter with fixed=false.
         */
        public class propagateNonFixedParameters extends Transformation {
            public void perform() {
                Set<FAbstractEquation> newInitEqns = new LinkedHashSet<FAbstractEquation>();
                Stack<FVariable> s = new Stack<FVariable>();
                
                // Add all parameters that depend on a non-fixed parameter
                for (FVariable v : nonFixedParameters())
                    s.addAll(v.dependentParameters());
                
                // Go through all variables that depend on a parameter with fixed=false and do:
                // - Add parameter equation to list of new initial equations
                // - Add dependent variables to the stack
                // - Set fixed=false
                while (!s.empty()) {
                    FAbstractEquation fae = s.pop().parameterEquation();
                    boolean visited = !newInitEqns.add(fae);
                    if (visited)
                        continue;
                    for (FVariable fv : fae.boundParameters()) {
                        s.addAll(fv.dependentParameters());
                        fv.setFixedAttribute(false);
                    }
                }
                
                // Construct new parameter equation list and add all but the new initial equations
                List<FAbstractEquation> newParameterEqns = new List<FAbstractEquation>();
                for (FAbstractEquation eqn : getFParameterEquationList())
                    if (!newInitEqns.contains(eqn))
                        newParameterEqns.add(eqn);
                setFParameterEquationList(newParameterEqns);
                
                // Add new initial equations to list of initial equations
                getFInitialEquations().addAll(newInitEqns);

                // Make a note that this is done
                nonFixedParametersPropagated = true;
                
                // Flush tree if change has been made
                if (!newInitEqns.isEmpty())
                    root().flushAllRecursive();
            }
        }
    }
}

