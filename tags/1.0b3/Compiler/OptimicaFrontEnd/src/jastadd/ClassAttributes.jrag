/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ClassAttributes {

	syn lazy List OptClassDecl.getClassAttributeList() {

		//debugPrint("Program.getPredefinedTypeList()");
		List l = new List();

	    // Build a string with a Modelica class corresponding to Real
		String builtInDef = "model ClassAttributes\n";
		builtInDef += "Real objective = 1;\n"; 
		builtInDef += "parameter Real startTime=0;\n";
		builtInDef += "parameter Real finalTime=0;\n";
		builtInDef += "parameter Boolean static=false;\n";
    	builtInDef += "end ClassAttributes;\n";
   
   		//java.io.StringReader builtInDefReader = new java.io.StringReader(builtInDef);
 	  	//ModelicaParser parser = new ModelicaParser();
  		PrimitiveClassDecl pcd=null;
  
   		try {
   		
   			//ModelicaScanner scanner = new ModelicaScanner(builtInDefReader);
   			ParserHandler ph = new ParserHandler();
   			SourceRoot sr = ph.parseString(builtInDef,"");
 			Program p = sr.getProgram();
   			   			
  			FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(0));	

			for (int i=0;i<cd.getNumComponentDecl();i++) { 		
 				l.add(cd.getComponentDecl(i));	
			}	
 		
 		} catch(Exception e){e.printStackTrace(); System.exit(0);}
			
		//debugPrint("Program.getPredefinedTypeList(): "+l.getNumChild());
		
		return l;
	}

	eq OptClassDecl.getClassModification().lookupComponentInClass(String name) {
		HashSet set = new HashSet();
		debugPrint("OptClassDecl.getClassModification().lookupComponentInClass: " + name);
		for (int i=0;i<getNumClassAttribute();i++) {
			if (getClassAttribute(i).matchComponentDecl(name))
				set.add(getClassAttribute(i));
		}
		debugPrint("OptClassDecl.getClassModification().lookupComponentInClass: " + name +": " + set.size());
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}

	eq OptClassDecl.memberComponent(String name) {
		HashSet set = new HashSet(4);
		set.addAll(super.memberComponent(name));
		for (int i=0;i<getNumClassAttribute();i++) {
			if (getClassAttribute(i).matchComponentDecl(name))
				set.add(getClassAttribute(i));
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();	
	}

	syn lazy List InstOptClassDecl.getInstClassAttributeList() {
		List l = new List();
		for (ComponentDecl cd : ((OptClassDecl)getClassDecl()).getClassAttributes()) {
    		addDynamicClassName(cd.getClassName().newInstAccess());
			l.add(getDynamicClassName(getNumDynamicClassName()-1).myInstClassDecl().newInstComponentDecl(cd));
		}
		return l;
	}

	eq InstOptClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) {
		
		HashSet set = new HashSet();
		//System.out.println("InstOptClassDecl.getInstClassModification().lookupInstComponentInInstElement: " + name);
		for (int i=0;i<getNumInstClassAttribute();i++) {
			if (getInstClassAttribute(i).matchInstComponentDecl(name))
				set.add(getInstClassAttribute(i));
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}

	// This equation is needed in order to ensure correct lookup of accesses
	// in the class attribute construct.
	eq InstOptClassDecl.getInstClassModification().myInstNode() =
		getInstClassAttribute(0);
	
	eq InstOptClassDecl.getInstClassModification().lookupInstComponent(String name) =
		memberInstComponent(name);
	
	eq InstOptClassDecl.memberInstComponent(String name) {
		//System.out.println("InstOptClassDecl.memberInstComponent: " + name);
		HashSet set = new HashSet(4);
		set.addAll(super.memberInstComponent(name));
		for (int i=0;i<getNumInstClassAttribute();i++) {
			if (getInstClassAttribute(i).matchInstComponentDecl(name))
				set.add(getInstClassAttribute(i));
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();	
	}

	
}