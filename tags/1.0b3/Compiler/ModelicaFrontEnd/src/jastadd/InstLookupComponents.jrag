/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.util.HashSet;

aspect InstLookupComponents {

	inh lazy HashSet InstAccess.lookupInstComponent(String name);	
	inh lazy HashSet InstNode.lookupInstComponent(String name);	
	eq InstBaseClassDecl.getChild().lookupInstComponent(String name) = genericLookupInstComponent(name); 	
	eq InstNode.getChild().lookupInstComponent(String name) = memberInstComponent(name); 
	
	eq InstComponentDecl.getInstModification().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstReplacingComposite.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstReplacingPrimitive.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstPrimitive.getInstModification().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstExtends.getInstClassModification().lookupInstComponent(String name) = lookupInstComponent(name);
	
	eq InstValueModification.getFExp().lookupInstComponent(String name) = myInstNode().lookupInstComponent(name);
	
	eq InstShortClassDecl.getChild().lookupInstComponent(String name) = lookupInstComponent(name);

	eq InstComponentDecl.getFArraySubscripts().lookupInstComponent(String name) = lookupInstComponent(name);
	
	eq InstPrimitive.getChild().lookupInstComponent(String name) = myInstClass().memberInstComponent(name); 
	eq InstPrimitive.getFArraySubscripts().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstDot.getRight().lookupInstComponent(String name) = getLeft().qualifiedLookupInstComponent(name);
	eq InstRoot.getChild().lookupInstComponent(String name) = emptyHashSet();
		
	syn lazy HashSet InstAccess.qualifiedLookupInstComponent(String name) = emptyHashSet();
	eq InstComponentAccess.qualifiedLookupInstComponent(String name) = myInstComponentDecl().memberInstComponent(name);	
	eq InstClassAccess.qualifiedLookupInstComponent(String name) = myInstClassDecl().memberInstComponent(name);
	
	eq SourceRoot.getChild().lookupInstComponent(String name) = emptyHashSet();
	// This equation is necessary since InstAccesses may be present in FExps.	
	eq FlatRoot.getChild().lookupInstComponent(String name) = emptyHashSet();
	
	syn lazy HashSet InstBaseClassDecl.genericLookupInstComponent(String name) {
		HashSet h = memberInstComponent(name);
		// TODO: add scoped lookup to find constants.
		return h;
	 }
	
	
	syn lazy HashSet InstNode.memberInstComponent(String name)  {

	 	HashSet set = new HashSet(4);
		 	
		for (InstComponentDecl ic : constrainingInstComponentDecls()) {
			if (ic.matchInstComponentDecl(name))
				set.add(ic);
		}

		for (InstExtends ie : constrainingInstExtends()) {
			set.addAll(ie.memberInstComponent(name));
		}
		
		if (set.size()>0) {
			return set;
		} else {
			return emptyHashSet();
		}
		
	}

	// This is needed since the member components of InstPrimitive:s (which are attributes)
	// are not instantiated
	eq InstPrimitive.memberInstComponent(String name) = myInstClass().memberInstComponent(name);
	eq InstExtends.memberInstComponent(String name) = extendsPrimitive()? myInstClass().memberInstComponent(name) : super.memberInstComponent(name);

	/**
	 * Simple matching of component names.
	 */
	syn boolean InstComponentDecl.matchInstComponentDecl(String name) = name().equals(name);
	
	syn lazy InstComponentDecl InstAccess.myInstComponentDecl() = unknownInstComponentDecl();
	eq InstComponentAccess.myInstComponentDecl() {
		HashSet set = lookupInstComponent(name());
		if (set.size() > 0) {
			return (InstComponentDecl)set.iterator().next();
		} else
			return unknownInstComponentDecl();
	}
	
	eq InstDot.myInstComponentDecl() {
		getLeft(); // FIXME: WHY??? Errors result if removed.
		return getRight().myInstComponentDecl();
	}
		
/*
	inh HashSet ForClauseE.lookupComponent(String name);	
	eq ForClauseE.getForEqns().lookupComponent(String name) {
		HashSet set = new HashSet(4);
		for (int i=0;i<getNumForIndex();i++) {
			if (getForIndex(i).getForIndexDecl().matchComponentDecl(name)) {
				set.add(getForIndex(i).getForIndexDecl());	
				return set;
			}
		}
		return lookupComponent(name);
	}


	inh HashSet SumRedExp.lookupComponent(String name);	
	eq SumRedExp.getExp().lookupComponent(String name) {
		debugPrint("SumRedExp.getExp().lookupComponent: "+ name);
		HashSet set = new HashSet(4);
		if (getForIndex().getForIndexDecl().matchComponentDecl(name)) {
			set.add(getForIndex().getForIndexDecl());
			return set;	
		}
		return lookupComponent(name);
	}
*/	
	
}

aspect LookupInstComponentSlices {
	
	syn InstComponentDecl[] InstComponentAccess.slice() {
		
		// Access refers to scalar declaration or, access has no array 
		// subscripts but refers to array declaration
		if (!hasFArraySubscripts()) {
			return myInstComponentDecl().slice();
		} else { // Access has array subscripts
			return myInstComponentDecl().slice(getFArraySubscripts());
		}
	}

	syn lazy InstComponentDecl[] InstComponentDecl.slice() {
		if (!hasFArraySubscripts()) { // Scalar variable
			InstComponentDecl s[] = new InstComponentDecl[1];
			s[0] = this;
			return s;
		} else { // Array declaration: return all "leaf" InstArrayComponentDecls
			int size[] = size();
			int n_s = 1;
			for (int i=0;i<size.length;i++) {
				n_s = n_s*size[i];
			}
			InstComponentDecl s[] = new InstComponentDecl[n_s];
			return s;			
		}
	}

	syn lazy InstComponentDecl[] InstComponentDecl.slice(FArraySubscripts fas) {
		int size[] = fas.size();
		int n_s = 1;
		for (int i=0;i<size.length;i++) {
			n_s = n_s*size[i];
		}
		InstComponentDecl s[] = new InstComponentDecl[n_s];
	
		return s;		
	}
}

aspect LookupInstComponentsInModifications {
      
    /**
     * The inherited attribute lookupInstComponentInInstElement defines 
     * the lookup mechanism for left hand component references in modifications.
     * InstComponents are looked up in InstComponentDecl:s sometimes and in InstClassDecl:s
     * sometimes. TODO: this should probably be fixed.
     * 
     */
	inh HashSet InstElementModification.lookupInstComponentInInstElement(String name);
	inh HashSet InstNamedModification.lookupInstComponentInInstElement(String name);
	
	eq InstElementModification.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstNamedModification.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstConstrainingClass.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	eq InstConstrainingComponent.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	     
	eq InstComponentDecl.getInstModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name); 
	
	eq InstPrimitive.getInstModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name); 
	
	eq InstElementModification.getInstModification().lookupInstComponentInInstElement(String name) = getName().myInstComponentDecl().memberInstComponent(name);
    
	eq InstExtends.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	
//	eq InstShortClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	
	inh HashSet InstComponentRedeclare.lookupInstComponentInInstElement(String name);
	eq InstComponentRedeclare.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstRoot.getChild().lookupInstComponentInInstElement(String name) {return emptyHashSet();}
	  
}

  