# -----------------------------------------------------------------------------
# CppAD: C++ Algorithmic Differentiation: Copyright (C) 2003-08 Bradley M. Bell
#
# CppAD is distributed under multiple licenses. This distribution is under
# the terms of the 
#                     Common Public License Version 1.0.
#
# A copy of this license is included in the COPYING file of this distribution.
# Please visit http://www.coin-or.org/CppAD/ for information on other licenses.
# -----------------------------------------------------------------------------
#
# automake input file
#
# Note that config.h is needed by VC++; i.e., for users that do not run
# the configure script but rather use the project files 
#
if CppAD_POSTFIX
postfix_dir  = $(POSTFIX_DIR)
else
postfix_dir  = .
endif
myincludedir = $(includedir)/$(postfix_dir)
nobase_myinclude_HEADERS =  \
	cppad/TrackNewDel.h \
	cppad/SpeedTest.h \
	cppad/Runge45.h \
	cppad/Rosen34.h \
	cppad/RombergOne.h \
	cppad/RombergMul.h \
	cppad/PowInt.h \
	cppad/Poly.h \
	cppad/OdeGearControl.h \
	cppad/OdeGear.h \
	cppad/OdeErrControl.h \
	cppad/NearEqual.h \
	cppad/LuSolve.h \
	cppad/LuInvert.h \
	cppad/LuFactor.h \
	cppad/ErrorHandler.h \
	cppad/CppAD_vector.h \
	cppad/CppAD.h \
	cppad/CheckSimpleVector.h \
	cppad/CheckNumericType.h \
	cppad/config.h \
	cppad/check_numeric_type.hpp \
	cppad/check_simple_vector.hpp \
	cppad/cppad.hpp \
	cppad/declare.hpp \
	cppad/vector.hpp \
	cppad/error_handler.hpp \
	cppad/lu_factor.hpp \
	cppad/lu_invert.hpp \
	cppad/lu_solve.hpp \
	cppad/nan.hpp \
	cppad/near_equal.hpp \
	cppad/ode_err_control.hpp \
	cppad/ode_gear.hpp \
	cppad/ode_gear_control.hpp \
	cppad/poly.hpp \
	cppad/pow_int.hpp \
	cppad/romberg_one.hpp \
	cppad/romberg_mul.hpp \
	cppad/rosen_34.hpp \
	cppad/runge_45.hpp \
	cppad/speed_test.hpp \
	cppad/std_math_unary.hpp \
	cppad/track_new_del.hpp \
	cppad/local/ad.hpp \
	cppad/local/ad_binary.hpp \
	cppad/local/ad_copy.hpp \
	cppad/local/ad_fun.hpp \
	cppad/local/ad_tape.hpp \
	cppad/local/ad_valued.hpp \
	cppad/local/abort_recording.hpp \
	cppad/local/abs.hpp \
	cppad/local/abs_op.hpp \
	cppad/local/acos_op.hpp \
	cppad/local/add.hpp \
	cppad/local/add_eq.hpp \
	cppad/local/add_op.hpp \
	cppad/local/arithmetic.hpp \
	cppad/local/asin_op.hpp \
	cppad/local/atan2.hpp \
	cppad/local/atan_op.hpp \
	cppad/local/base_complex.hpp \
	cppad/local/bender_quad.hpp \
	cppad/local/bool_fun.hpp \
	cppad/local/bool_fun_link.hpp \
	cppad/local/bool_valued.hpp \
	cppad/local/cap_taylor.hpp \
	cppad/local/compare.hpp \
	cppad/local/compute_assign.hpp \
	cppad/local/cond_exp.hpp \
	cppad/local/convert.hpp \
	cppad/local/cppad_assert.hpp \
	cppad/local/declare_ad.hpp \
	cppad/local/default.hpp \
	cppad/local/define.hpp \
	cppad/local/dependent.hpp \
	cppad/local/discrete.hpp \
	cppad/local/div.hpp \
	cppad/local/div_eq.hpp \
	cppad/local/div_op.hpp \
	cppad/local/drivers.hpp \
	cppad/local/equal_op_seq.hpp \
	cppad/local/erf.hpp \
	cppad/local/exp_op.hpp \
	cppad/local/for_one.hpp \
	cppad/local/for_two.hpp \
	cppad/local/for_jac_sweep.hpp \
	cppad/local/for_sparse_jac.hpp \
	cppad/local/forward.hpp \
	cppad/local/forward_sweep.hpp \
	cppad/local/forward0sweep.hpp \
	cppad/local/fun_check.hpp \
	cppad/local/fun_eval.hpp \
	cppad/local/fun_construct.hpp \
	cppad/local/hessian.hpp \
	cppad/local/identical.hpp \
	cppad/local/independent.hpp \
	cppad/local/integer.hpp \
	cppad/local/jacobian.hpp \
	cppad/local/log_op.hpp \
	cppad/local/lu_ratio.hpp \
	cppad/local/math_other.hpp \
	cppad/local/mul.hpp \
	cppad/local/mul_eq.hpp \
	cppad/local/mul_op.hpp \
	cppad/local/omp_max_thread.hpp \
	cppad/local/op.hpp \
	cppad/local/op_code.hpp \
	cppad/local/ordered.hpp \
	cppad/local/output.hpp \
	cppad/local/par_var.hpp \
	cppad/local/pow.hpp \
	cppad/local/preprocessor.hpp \
	cppad/local/print_for.hpp \
	cppad/local/near_equal_ext.hpp \
	cppad/local/reverse.hpp \
	cppad/local/rev_hes_sweep.hpp \
	cppad/local/rev_jac_sweep.hpp \
	cppad/local/rev_one.hpp \
	cppad/local/rev_sparse_jac.hpp \
	cppad/local/rev_sparse_hes.hpp \
	cppad/local/rev_two.hpp \
	cppad/local/reverse_sweep.hpp \
	cppad/local/sin_cos.hpp \
	cppad/local/sparse.hpp \
	cppad/local/sparse_hessian.hpp \
	cppad/local/sparse_jacobian.hpp \
	cppad/local/sqrt_op.hpp \
	cppad/local/std_math_ad.hpp \
	cppad/local/sub.hpp \
	cppad/local/sub_eq.hpp \
	cppad/local/sub_op.hpp \
	cppad/local/tape_link.hpp \
	cppad/local/recorder.hpp \
	cppad/local/player.hpp \
	cppad/local/test_vector.hpp \
	cppad/local/user_ad.hpp \
	cppad/local/unary_minus.hpp \
	cppad/local/unary_plus.hpp \
	cppad/local/vec_ad.hpp \
	cppad/local/undef.hpp \
	cppad/local/value.hpp \
	cppad/local/var2par.hpp \
	cppad/speed/det_33.hpp \
	cppad/speed/det_by_lu.hpp \
	cppad/speed/det_by_minor.hpp \
	cppad/speed/det_grad_33.hpp \
	cppad/speed/det_of_minor.hpp \
	cppad/speed/ode_evaluate.hpp \
	cppad/speed/sparse_evaluate.hpp \
	cppad/speed/uniform_01.hpp
#
#
# Did user specify --with-Introduction
if CppAD_INTRODUCTION
INTRODUCTION = \
	introduction/get_started \
	introduction/exp_apx
else
INTRODUCTION =
endif
#
# Did user specify --with-Example
if CppAD_EXAMPLE
EXAMPLE = example
else
EXAMPLE =
endif
#
# Did user specify --with-TestMore
if CppAD_TEST_MORE
TEST_MORE = test_more
else
TEST_MORE =
endif
#
if CppAD_SPEED
# user specified --with-Speed -------------------------------------
SPEED_CPPAD = speed/cppad speed/double speed/example speed/profile
#
if CppAD_ADOLC
# Did user specify a value for ADOLC_DIR in configure command line
SPEED_ADOLC    = speed/adolc
endif
#
# Did user specify a value for FADBAD_DIR in configure command line
if CppAD_FADBAD
SPEED_FADBAD = speed/fadbad
endif
#
# Did user specify a value for SACADO_DIR in configure command line
if CppAD_SACADO
SPEED_SACADO = speed/sacado
endif
# ----------------------------------------------------------------------
# user did not specify --with-Speed
else
SPEED_CPPAD  =
SPEED_ADOLC  = 
SPEED_FADBAD = 
SPEED_SACADO = 
endif
# ----------------------------------------------------------------------
# Did user specify --with-PrintFor
if CppAD_PRINTFOR
PRINTFOR = print_for
else
PRINTFOR =
endif
#
# ---------------------------------------------------------------
# Did user specify a value for IPOPT_DIR in configure command line 
#
# ---------------------------------------------------------------
# Did user specify a value for IPOPT_DIR in configure command line 
if CppAD_IPOPT_CPPAD
IPOPT_CPPAD = ipopt_cppad
else
IPOPT_CPPAD =
endif
#
SUBDIRS = \
	$(IPOPT_CPPAD) \
	$(IPOPT_CPPAD) \
	$(EXAMPLE) \
	$(INTRODUCTION) \
	$(PRINTFOR) \
	$(SPEED_CPPAD) \
	$(SPEED_ADOLC) \
	$(SPEED_FADBAD) \
	$(SPEED_SACADO) \
	$(TEST_MORE) 
#
EXTRA_DIST = \
	build.sh \
	check_example.sh \
	check_include_file.sh \
	check_include_omh.sh \
	check_include_def.sh \
	clean_cppad.sh \
	cpl1.0.txt \
	dev.omh \
	doc \
	doc.omh \
	doc.omh.in \
	fix_aclocal.sh \
	fix_makefile.sh \
	omh \
	openmp/example_a11c.cpp \
	openmp/multi_newton.cpp \
	openmp/multi_newton.hpp \
	openmp/sum_i_inv.cpp \
	openmp/run.sh \
	run_omhelp.sh \
	uw_copy_040507.html
#
dist-hook:
	rm -rf `find $(distdir)/omh $(distdir)/openmp -name .svn`
	rm $(distdir)/doc/error.wrd
#
if CppAD_DOCUMENTATION
doc_prefix  = $(DESTDIR)$(datadir)
doc_doc     = $(doc_prefix)/doc
doc_postfix = $(doc_doc)/$(postfix_dir)
doc_package = $(doc_postfix)/$(PACKAGE)-$(VERSION)
install-data-hook:
	if [ ! -e $(doc_prefix) ] ; then mkdir $(doc_prefix) ; fi
	if [ ! -e $(doc_doc) ] ; then mkdir $(doc_doc) ; fi
	if [ ! -e $(doc_postfix) ] ; then mkdir $(doc_postfix) ; fi
	if [ -e $(doc_package) ] ; then rm -rf $(doc_package) ; fi
	cp -a $(top_builddir)/doc $(doc_package)
	chmod -R a-w $(doc_package)
	chmod -R u+w $(doc_package)
	chmod -R a+r $(doc_package)
endif
#
