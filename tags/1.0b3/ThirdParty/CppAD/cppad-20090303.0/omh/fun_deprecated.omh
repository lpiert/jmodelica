/* --------------------------------------------------------------------------
CppAD: C++ Algorithmic Differentiation: Copyright (C) 2003-07 Bradley M. Bell

CppAD is distributed under multiple licenses. This distribution is under
the terms of the 
                    Common Public License Version 1.0.

A copy of this license is included in the COPYING file of this distribution.
Please visit http://www.coin-or.org/CppAD/ for information on other licenses.
-------------------------------------------------------------------------- */
$begin FunDeprecated$$
$spell 
	CppAD
	const
	Taylor
	sizeof
	var
$$

$section ADFun Object Deprecated Member Functions$$

$head Syntax$$
$syntax%%f%.Dependent(%y%)
%$$
$syntax%%o% = %f%.Order()
%$$
$syntax%%m% = %f%.Memory()
%$$
$syntax%%s% = %f%.Size()
%$$
$syntax%%t% = %f%.taylor_size()%$$


$head Purpose$$
The $syntax%ADFun<%Base%>%$$ functions documented here have been deprecated;
i.e., they are no longer approved of and may be removed from some future
version of CppAD.

$head Dependent$$
$index ADFun, Dependent deprecated$$
$index Dependent, ADFun deprecated$$
$index deprecated, Dependent ADFun$$
A recording of and AD of $italic Base$$
$xref/glossary/Operation/Sequence/operation sequence/1/$$
is started by a call of the form
$syntax%
	Independent(%x%)
%$$ 
If there is only one such recording at the current time,
you can use $syntax%%f%.Dependent(%y%)%$$ in place of
$syntax%
	%f%.Dependent(%x%, %y%)
%$$
See $cref/Dependent/$$ for a description of this operation.

$subhead Deprecated$$
This syntax was deprecated when CppAD was extended to allow
for more than one $syntax%AD<%Base%>%$$ recording to be
active at one time. 
This was necessary to allow for multiple threading applications.


$head Order$$
$index ADFun, Order deprecated$$
$index Order, ADFun deprecated$$
$index deprecated, Order ADFun$$
The result $italic o$$ has prototype 
$syntax%
	size_t %o%
%$$
and is the order of the previous forward operation
using the function $italic f$$.
This is the highest order of the 
$xref/glossary/Taylor Coefficient/Taylor coefficients/$$
that are currently stored in $italic f$$.

$subhead Deprecated$$
Zero order corresponds to function values being stored in $italic f$$.
In the future, we would like to be able to erase the function
values so that $italic f$$ uses less memory. 
In this case, the return value of $code Order$$ would not make sense.
Use $xref/size_taylor/$$ to obtain 
the number of Taylor coefficients currently stored
in the ADFun object $italic f$$ 
(which is equal to the order plus one).


$head Memory$$
$index ADFun, Memory deprecated$$
$index Memory, ADFun deprecated$$
$index deprecated, Memory ADFun$$
The result 
$syntax%
	size_t %m%
%$$
and is the number of memory units ($code sizeof$$) required for the
information currently stored in $italic f$$.
This memory is returned to the system when the destructor for 
$italic f$$ is called.

$subhead Deprecated$$
It used to be the case that an ADFun object just kept increasing its
buffers to the maximum size necessary during its lifetime.
It would then return the buffers to the system when its destructor
was called.
This is no longer the case, an ADFun object now returns memory
when it no longer needs the values stored in that memory.
Thus the $code Memory$$ function is no longer well defined.

$head Size$$
$index ADFun, Size deprecated$$
$index Size, ADFun deprecated$$
$index deprecated, Size ADFun$$
The result $italic s$$ has prototype
$syntax%
	size_t %s%
%$$
and is the number of variables in the operation sequence plus the following:
one for a phantom variable with tape address zero,
one for each component of the domain that is a parameter.
The amount of work and memory necessary for computing function values
and derivatives using $italic f$$ is roughly proportional to $italic s$$.

$subhead Deprecated$$
There are other sizes attached to an ADFun object, for example,
the number of operations in the sequence.
In order to avoid confusion with these other sizes,
use $xref/SeqProperty/size_var/size_var/$$ to obtain 
the number of variables in the operation sequence.

$head taylor_size$$
$index ADFun, taylor_size deprecated$$
$index taylor_size, ADFun deprecated$$
$index deprecated, taylor_size ADFun$$
The result $italic t$$ has prototype
$syntax%
	size_t %t%
%$$
and is the number of Taylor coefficients, 
per variable in the AD operation sequence,
currently calculated and stored in the ADFun object $italic f$$.

$subhead Deprecated$$
For the purpose of uniform naming,
this function has been replaced by $xref/size_taylor/$$.

$head Example$$
The file
$xref/Forward.cpp/$$
contains an example and test of this operation.
It returns true if it succeeds and false otherwise.

$end
