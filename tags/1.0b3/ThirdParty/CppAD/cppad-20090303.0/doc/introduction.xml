<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>An Introduction by Example to Algorithmic Differentiation</title>
<meta name="description" id="description" content="An Introduction by Example to Algorithmic Differentiation"/>
<meta name="keywords" id="keywords" content=" introduction Ad Algorithmic Differentiation Automatic "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_introduction_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="installwindows.xml" target="_top">Prev</a>
</td><td><a href="get_started.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Introduction</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>Introduction-&gt;</option>
<option>get_started.cpp</option>
<option>exp_2</option>
<option>exp_eps</option>
<option>exp_apx_main.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Preface</option>
<option>---..Algorithmic Differentiation</option>
<option>---..Forward Mode</option>
<option>---..Reverse Mode</option>
<option>---..Operation Count</option>
<option>---..Efficiency</option>
<option>Purpose</option>
<option>Outline</option>
<option>Reference</option>
<option>Contents</option>
</select>
</td>
</tr></table><br/>









<center><b><big><big>An Introduction by Example to Algorithmic Differentiation</big></big></b></center>
<br/>
<b><big><a name="Preface" id="Preface">Preface</a></big></b>


<br/>
<br/>
<b><a name="Preface.Algorithmic Differentiation" id="Preface.Algorithmic Differentiation">Algorithmic Differentiation</a></b>
<br/>
Algorithmic Differentiation 
(often referred to as Automatic Differentiation or just AD)
uses the software representation 
of a function to obtain an efficient method for calculating its derivatives.
These derivatives can be of arbitrary order and are analytic in nature 
(do not have any truncation error).

<br/>
<br/>
<b><a name="Preface.Forward Mode" id="Preface.Forward Mode">Forward Mode</a></b>
<br/>
A forward mode sweep computes 
the partial derivative of all the dependent variables with respect
to one independent variable (or independent variable direction). 

<br/>
<br/>
<b><a name="Preface.Reverse Mode" id="Preface.Reverse Mode">Reverse Mode</a></b>
<br/>
A reverse mode sweep computes 
the derivative of one dependent variable
(or one dependent variable direction) 
with respect to all the independent variables.

<br/>
<br/>
<b><a name="Preface.Operation Count" id="Preface.Operation Count">Operation Count</a></b>
<br/>
The number of floating point operations for either a 
forward or reverse mode sweep
is a small multiple of the number required to evaluate the original function. 
Thus, using reverse mode,
you can evaluate the derivative of a scalar valued function 
with respect to thousands of variables in a small multiple of the
work to evaluate the original function.

<br/>
<br/>
<b><a name="Preface.Efficiency" id="Preface.Efficiency">Efficiency</a></b>
<br/>
AD automatically takes advantage of the
speed of your algorithmic representation of a function.
For example,
if you calculate a determinant using LU factorization,
AD will use the LU representation for
the derivative of the determinant
(which is faster than using the definition of the determinant).

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
This is an introduction by example
to Algorithmic Differentiation.
Its purpose is to aid in understand what AD calculates,
how the calculations are preformed,
and the amount of computation and memory required 
for a forward or reverse sweep.

<br/>
<br/>
<b><big><a name="Outline" id="Outline">Outline</a></big></b>

<ol type="A"><li>
Demonstrate the use of CppAD to calculate derivatives of a 
polynomial: <a href="get_started.cpp.xml" target="_top"><span style='white-space: nowrap'>get_started.cpp</span></a>
.

</li><li>

Present two algorithms that approximate the exponential function.
The first algorithm <a href="exp_2.hpp.xml" target="_top"><span style='white-space: nowrap'>exp_2.hpp</span></a>
 is simpler and does not 
include any logical variables or loops.
The second algorithm <a href="exp_eps.hpp.xml" target="_top"><span style='white-space: nowrap'>exp_eps.hpp</span></a>
 includes
logical operations and a <code><font color="blue">while</font></code> loop.
For each of these algorithms, do the following:

<ol type="1"><li>
Define the mathematical function corresponding to the algorithm
(<a href="exp_2.xml" target="_top"><span style='white-space: nowrap'>exp_2</span></a>
 and <a href="exp_eps.xml" target="_top"><span style='white-space: nowrap'>exp_eps</span></a>
).
</li><li>

Write out the floating point operation sequence,
and corresponding values,
that correspond to executing the algorithm for a specific input
(<a href="exp_2_for0.xml" target="_top"><span style='white-space: nowrap'>exp_2_for0</span></a>
 and <a href="exp_eps_for0.xml" target="_top"><span style='white-space: nowrap'>exp_eps_for0</span></a>
).
</li><li>

Compute a forward sweep derivative of the operation sequence
(<a href="exp_2_for1.xml" target="_top"><span style='white-space: nowrap'>exp_2_for1</span></a>
 and <a href="exp_eps_for1.xml" target="_top"><span style='white-space: nowrap'>exp_eps_for1</span></a>
).
</li><li>

Compute a reverse sweep derivative of the operation sequence
(<a href="exp_2_rev1.xml" target="_top"><span style='white-space: nowrap'>exp_2_rev1</span></a>
 and <a href="exp_eps_rev1.xml" target="_top"><span style='white-space: nowrap'>exp_eps_rev1</span></a>
).
</li><li>

Use CppAD to compute both a forward and reverse sweep 
of the operation sequence
(<a href="exp_2_cppad.xml" target="_top"><span style='white-space: nowrap'>exp_2_cppad</span></a>
 and <a href="exp_eps_cppad.xml" target="_top"><span style='white-space: nowrap'>exp_eps_cppad</span></a>
).
</li></ol>


</li><li>

The program <a href="exp_apx_main.cpp.xml" target="_top"><span style='white-space: nowrap'>exp_apx_main.cpp</span></a>
 runs all of the test
routines that validate the calculations in the <a href="exp_2.xml" target="_top"><span style='white-space: nowrap'>exp_2</span></a>

and <a href="exp_eps.xml" target="_top"><span style='white-space: nowrap'>exp_eps</span></a>
 presentation.
</li></ol>


<br/>
<br/>
<b><big><a name="Reference" id="Reference">Reference</a></big></b>
<br/>
An in-depth review of AD theory and methods can be found in 
the book
<i>
Evaluating Derivatives:
Principles and Techniques of Algorithmic Differentiation
</i>,
Andreas Griewank,
SIAM Frontiers in Applied Mathematics, 
2000.

<br/>
<br/>
<b><big><a name="Contents" id="Contents">Contents</a></big></b>
<br/>
<table>
<tr><td><a href="get_started.cpp.xml" target="_top">get_started.cpp</a></td><td>A&#xA0;Simple&#xA0;Program&#xA0;Using&#xA0;CppAD&#xA0;to&#xA0;Compute&#xA0;Derivatives</td></tr><tr><td><a href="exp_2.xml" target="_top">exp_2</a></td><td>Second&#xA0;Order&#xA0;Exponential&#xA0;Approximation</td></tr><tr><td><a href="exp_eps.xml" target="_top">exp_eps</a></td><td>An&#xA0;Epsilon&#xA0;Accurate&#xA0;Exponential&#xA0;Approximation</td></tr><tr><td><a href="exp_apx_main.cpp.xml" target="_top">exp_apx_main.cpp</a></td><td>Run&#xA0;the&#xA0;exp_2&#xA0;and&#xA0;exp_eps&#xA0;Tests</td></tr></table>
<hr/>Input File: omh/introduction.omh

</body>
</html>
