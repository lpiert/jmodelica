<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Definition of a Simple Vector</title>
<meta name="description" id="description" content="Definition of a Simple Vector"/>
<meta name="keywords" id="keywords" content=" vector simple class template default constructor size copy element destructor assignment resize value_type [] exercise Ndebug "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_simplevector_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="checknumerictype.cpp.xml" target="_top">Prev</a>
</td><td><a href="simplevector.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>SimpleVector</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>opt_val_hes</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>SimpleVector-&gt;</option>
<option>SimpleVector.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Template Class Requirements</option>
<option>Elements of Specified Type</option>
<option>Default Constructor</option>
<option>Sizing Constructor</option>
<option>Copy Constructor</option>
<option>Element Constructor and Destructor</option>
<option>Assignment</option>
<option>Size</option>
<option>Resize</option>
<option>Value Type</option>
<option>Element Access</option>
<option>---..Using Value</option>
<option>---..Assignment</option>
<option>Example</option>
<option>Exercise</option>
</select>
</td>
</tr></table><br/>








<center><b><big><big>Definition of a Simple Vector</big></big></b></center>
<br/>
<b><big><a name="Template Class Requirements" id="Template Class Requirements">Template Class Requirements</a></big></b>
<br/>
A simple vector template class <i>SimpleVector</i>,
is any template class
that satisfies the requirements below.
The following is a list of some simple vector template classes:
<table><tr><td align='left'  valign='top'>

<b>Name</b> </td><td align='left'  valign='top'>
 <b>Documentation</b>   </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">std::vector</font></code>      </td><td align='left'  valign='top'>
 Section 16.3 of 
<a href="bib.xml#The C++ Programming Language" target="_top"><span style='white-space: nowrap'>The&#xA0;C++&#xA0;Programming&#xA0;Language</span></a>

</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">std::valarray</font></code>    </td><td align='left'  valign='top'>
 Section 22.4 of  
<a href="bib.xml#The C++ Programming Language" target="_top"><span style='white-space: nowrap'>The&#xA0;C++&#xA0;Programming&#xA0;Language</span></a>

</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">CppAD::vector</font></code>    </td><td align='left'  valign='top'>
 <a href="cppad_vector.xml" target="_top">The CppAD::vector Template Class</a>
</td></tr>
</table>
<br/>
<b><big><a name="Elements of Specified Type" id="Elements of Specified Type">Elements of Specified Type</a></big></b>
<br/>
A simple vector class with elements of type <i>Scalar</i>,
is any class that satisfies the requirements for a class of the form
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;<br/>
</span></font></code>The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 can be used to check
that a class is a simple vector class with a specified element type.

<br/>
<br/>
<b><big><a name="Default Constructor" id="Default Constructor">Default Constructor</a></big></b>


<br/>
The syntax 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>;<br/>
</span></font></code>creates an empty vector <i>x</i> (<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>.size()</span></font></code> is zero)
that can later contain elements of the specified type
(see <a href="simplevector.xml#Resize" target="_top"><span style='white-space: nowrap'>resize</span></a>
 below).

<br/>
<br/>
<b><big><a name="Sizing Constructor" id="Sizing Constructor">Sizing Constructor</a></big></b>


<br/>
If <i>n</i> has type <code><font color="blue">size_t</font></code>, 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>creates a vector <i>x</i> with <i>n</i> elements
each of the specified type.

<br/>
<br/>
<b><big><a name="Copy Constructor" id="Copy Constructor">Copy Constructor</a></big></b>


<br/>
If <i>x</i> is a <code><font color="blue"></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>creates a vector with the same type and number of elements
as <i>x</i>.
The <i>Scalar</i> assignment operator ( <code><font color="blue">=</font></code> )
is used to set each element of <i>y</i>
equal to the corresponding element of <i>x</i>.
This is a `deep copy' in that the values of the elements
of <i>x</i> and <i>y</i> can be set independently after the copy.
The argument <i>x</i> is passed by reference
and may be <code><font color="blue">const</font></code>.

<br/>
<br/>
<b><big><a name="Element Constructor and Destructor" id="Element Constructor and Destructor">Element Constructor and Destructor</a></big></b>


<br/>
The constructor for every element in a vector is called
when the vector element is created and
the corresponding destructor is called when it is removed
from the vector (this includes when the vector is destroyed).


<br/>
<br/>
<b><big><a name="Assignment" id="Assignment">Assignment</a></big></b>

<br/>
If <i>x</i> and <i>y</i> are 
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> objects,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>uses the <i>Scalar</i> assignment operator ( <code><font color="blue">=</font></code> )
to set each element of <i>y</i> equal to 
the corresponding element of <i>x</i>.
This is a `deep assignment' in that the values of the elements
of <i>x</i> and <i>y</i> can be set independently after the assignment.
The vectors <i>x</i> and <i>y</i> must have
the same number of elements.
The argument <i>x</i> is passed by reference
and may be <code><font color="blue">const</font></code>.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>The type returned by this assignment is unspecified; for example,
it might be void in which case the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>would not be valid.

<br/>
<br/>
<b><big><a name="Size" id="Size">Size</a></big></b>

<br/>
If <i>x</i> is a <code><font color="blue"></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object
and <code><font color="blue">n</font></code> has type <code><font color="blue">size_t</font></code>,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>.size()<br/>
</span></font></code>sets <i>n</i> to the number of elements in the vector <i>x</i>.
The object <i>x</i> may be <code><font color="blue">const</font></code>.

<br/>
<br/>
<b><big><a name="Resize" id="Resize">Resize</a></big></b>

<br/>
If <i>x</i> is a <code><font color="blue"></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object
and <code><font color="blue">n</font></code> has type <code><font color="blue">size_t</font></code>,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>.resize(</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>changes the number of elements contained in the vector <i>x</i>
to be <i>n</i>.
The value of the elements of <i>x</i>
are not specified after this operation; i.e.,
any values previously stored in <i>x</i> are lost.
(The object <i>x</i> can not be <code><font color="blue">const</font></code>.)

<br/>
<br/>
<b><big><a name="Value Type" id="Value Type">Value Type</a></big></b>

<br/>
If <i>Vector</i> is any simple vector class,
the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>::value_type<br/>
</span></font></code>is the type of the elements corresponding to the vector class; i.e.,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::value_type<br/>
</span></font></code>is equal to <i>Scalar</i>.

<br/>
<br/>
<b><big><a name="Element Access" id="Element Access">Element Access</a></big></b>

<br/>
If <i>x</i> is a <code><font color="blue"></font></code><i><span style='white-space: nowrap'>SimpleVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object
and <i>i</i> has type <code><font color="blue">size_t</font></code>,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]<br/>
</span></font></code>returns an object of an unspecified type,
referred to here as <i>elementType</i>.

<br/>
<br/>
<b><a name="Element Access.Using Value" id="Element Access.Using Value">Using Value</a></b>
<br/>
If <i>elementType</i> is not the same as <i>Scalar</i>,
the conversion operator
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;static_cast&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>])<br/>
</span></font></code>is used implicitly when <code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]</span></font></code> is used in an expression
with values of type <i>Scalar</i>.
For this type of usage, the object <i>x</i> may be <code><font color="blue">const</font></code>.

<br/>
<br/>
<b><a name="Element Access.Assignment" id="Element Access.Assignment">Assignment</a></b>
<br/>
If <i>y</i> is an object of type <i>Scalar</i>,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>assigns the <i>i</i>-th element of <i>x</i> to have value <i>y</i>.
For this type of usage, the object <i>x</i> can not be <code><font color="blue">const</font></code>.
The type returned by this assignment is unspecified; for example,
it might be void in which case the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>would not be valid.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="simplevector.cpp.xml" target="_top"><span style='white-space: nowrap'>SimpleVector.cpp</span></a>

contains an example and test of a Simple template class.
It returns true if it succeeds and false otherwise.
(It is easy to modify to test additional simple vector template classes.)

<br/>
<br/>
<b><big><a name="Exercise" id="Exercise">Exercise</a></big></b>


<ol type="1"><li>
If <i>Vector</i> is a simple vector template class,
the following code may not be valid:
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;double&gt;&#xA0;x(2);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;x[2]&#xA0;=&#xA0;1.;<br/>
</span></font></code>Create and run a program that executes the code segment
above where <i>Vector</i> is each of the following cases:
<code><font color="blue">std::vector</font></code>,
<code><font color="blue">CppAD::vector</font></code>.

Do this both where the compiler option 
<code><font color="blue">-DNDEBUG</font></code> is and is not present on the compilation command line.
</li><li>

If <i>Vector</i> is a simple vector template class,
the following code may not be valid:
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;int&gt;&#xA0;x(2);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&lt;int&gt;&#xA0;y(1);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;x[0]&#xA0;=&#xA0;0;<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;x[1]&#xA0;=&#xA0;1;<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;y&#xA0;&#xA0;&#xA0;&#xA0;=&#xA0;x;<br/>
</span></font></code>Create and run a program that executes the code segment
above where <i>Vector</i> is each of the following cases:
<code><font color="blue">std::valarray</font></code>,
<code><font color="blue">CppAD::vector</font></code>.
Do this both where the compiler option 
<code><font color="blue">-DNDEBUG</font></code> is and is not present on the compilation command line.
</li></ol>




<hr/>Input File: omh/simple_vector.omh

</body>
</html>
