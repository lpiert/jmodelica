/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.HashSet;

aspect InstLookupClasses {

	syn lazy HashSet InstAccess.lookupInstClass() circular [emptyHashSet()]{
	 	return lookupInstClass(name());
	}

	eq InstDot.lookupInstClass() {
	   return getRight().lookupInstClass();
    }
    
	inh HashSet InstAccess.lookupInstClass(String name) circular [emptyHashSet()];
	inh HashSet InstNode.lookupInstClass(String name);
    inh HashSet InstModification.lookupInstClass(String name);
		
	eq InstNode.getInstClassDecl(int i).lookupInstClass(String name) = genericLookupInstClass(name); 
	eq InstNode.getInstComponentDecl(int i).lookupInstClass(String name) = genericLookupInstClass(name); 
	eq InstNode.getInstExtends(int i).lookupInstClass(String name) = superLookupInstClass(name);

    eq InstNode.getDynamicClassName().lookupInstClass(String name) = genericLookupInstClass(name);

	eq InstComponentDecl.getInstClassDecl().lookupInstClass(String name) = genericLookupInstClassInComponent(name);	
	eq InstComponentDecl.getInstComponentDecl().lookupInstClass(String name) = genericLookupInstClassInComponent(name);	
	eq InstComponentDecl.getInstExtends().lookupInstClass(String name) = superLookupInstClassInComponent(name);
    eq InstComponentDecl.getDynamicClassName().lookupInstClass(String name) = genericLookupInstClassInComponent(name);
	
	eq InstExtends.getInstClassDecl().lookupInstClass(String name) = genericLookupInstClassInExtends(name);	
	eq InstExtends.getInstComponentDecl().lookupInstClass(String name) = genericLookupInstClassInExtends(name);	
	eq InstExtends.getInstExtends().lookupInstClass(String name) = superLookupInstClassInExtends(name);	
    eq InstExtends.getDynamicClassName().lookupInstClass(String name) = genericLookupInstClassInExtends(name);

	// The lexical scope of modifiers in short classes is the "outer" scope, although
	// the short class declaration is modeled as containing an extends clause
	eq InstExtendsShortClass.getInstClassModification().lookupInstClass(String name) = lookupInstClass(name);
	eq InstShortClassDecl.getChild().lookupInstClass(String name) = lookupInstClass(name);
	eq InstReplacingShortClassDecl.getChild().lookupInstClass(String name) = getInstClassRedeclare().lookupInstClass(name);
	eq InstReplacingFullClassDecl.getChild().lookupInstClass(String name) = getInstClassRedeclare().lookupInstClass(name);
	eq InstReplacingShortClassDecl.getOriginalInstClass().lookupInstClass(String name) = lookupInstClass(name);
	eq InstReplacingFullClassDecl.getOriginalInstClass().lookupInstClass(String name) = lookupInstClass(name);
	
	eq InstReplacingComposite.getOriginalInstComponent().lookupInstClass(String name) = lookupInstClass(name);
	eq InstReplacingPrimitive.getOriginalInstComponent().lookupInstClass(String name) = lookupInstClass(name);
 	eq InstReplacingComposite.getDynamicClassName().lookupInstClass(String name) = lookupInstClass(name);
 	eq InstReplacingPrimitive.getDynamicClassName().lookupInstClass(String name) = lookupInstClass(name);
  
	eq InstReplacingComposite.getClassName().lookupInstClass(String name) = getInstComponentRedeclare().lookupInstClass(name);
	eq InstReplacingPrimitive.getClassName().lookupInstClass(String name) = getInstComponentRedeclare().lookupInstClass(name);

	syn HashSet InstClassDecl.lookupInstClassInInstClassRedeclare(String name) = emptyHashSet();
	eq InstReplacingShortClassDecl.lookupInstClassInInstClassRedeclare(String name) = getInstClassRedeclare().lookupInstClass(name);

	syn boolean InstClassDecl.isRedeclared() = false;
	eq InstReplacingShortClassDecl.isRedeclared() = true;

	eq SourceRoot.getChild().lookupInstClass(String name) = emptyHashSet();
	
	// This equation is necessary since InstAccesses may be present in FExps.
	eq FlatRoot.getChild().lookupInstClass(String name) = emptyHashSet();
	
	syn lazy HashSet InstProgramRoot.lookupElementInstClass(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : instClassDecls()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	syn lazy HashSet InstProgramRoot.lookupInstPredefinedType(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstPredefinedTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}


	syn lazy HashSet InstProgramRoot.lookupInstBuiltInFunction(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstBuiltInFunctions()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		
		
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	InstFullClassDecl LibNode.instFullClassDecl = null;
	HashMap<String,LibNode> InstProgramRoot.libraryMap = 
		new HashMap<String,LibNode>();
	
	syn lazy HashSet InstProgramRoot.lookupLibrary(String name) {
		HashSet set = new HashSet(4);
		Program prog = ((SourceRoot)root()).getProgram();
		
		LibNode ln = null;
		// Check if library is loaded
		if (libraryMap.get(name) != null) {
			ln = libraryMap.get(name);
		} else { // If not search amongst LibNodes
			int i = 0;
			for  (LibNode ln_tmp : prog.getLibNodes()) {
				if (ln_tmp.getName().equals(name)) {
					ln = ln_tmp;
					break;
				}
				i ++;
			}
			if (ln != null) {
				libraryMap.put(name,ln);
			}
		}

		if (ln != null && ln.instFullClassDecl != null) { // Lib found and loaded
			set.add(ln.instFullClassDecl);
		} else if (ln != null) { // Lib found but not loaded
			// Only need to consider one class according to spec.
			if (ln.getStoredDefinition().getNumElement() > 0) {
   	    		ClassDecl cd = (ClassDecl)ln.getStoredDefinition().getElement(0);
   	    		// Instantiate class
   			    InstNode icd = createInstClassDecl(cd);
   			    if (icd != null) {
   			    	if (icd instanceof InstFullClassDecl) {
   			    		// Add to instance tree
   			    		addInstLibClassDecl((InstFullClassDecl)icd);
   			    		// Make sure is$Final is true;
   			    		getInstLibClassDecl(getNumInstLibClassDecl()-1);
   			    		ln.instFullClassDecl = (InstFullClassDecl)icd;
   			    		set.add((InstFullClassDecl)icd);
   			    	}
    	    	}
    	    }
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstProgramRoot.getChild().lookupInstClass(String name) = genericLookupInstClass(name);
	//eq SourceRoot.getLibNode().lookupInstClass(String name) = genericLookupClass(name);
	

	eq InstProgramRoot.getInstPredefinedType().lookupInstClass(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstBuiltInTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		
		

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}


	syn lazy  HashSet InstProgramRoot.genericLookupInstClass(String name) {
		HashSet set = new HashSet(4);
		
		if (lookupElementInstClass(name).size()>0)
			set.addAll(lookupElementInstClass(name));
		
		if (lookupInstPredefinedType(name).size()>0)
			set.addAll(lookupInstPredefinedType(name));
		
		if (lookupInstBuiltInFunction(name).size()>0)
			set.addAll(lookupInstBuiltInFunction(name));

		// TODO: propagate information about the class from where
		// the lookup is requested, so that version information
		// stored in annotations can be retreived. 
		if (lookupLibrary(name).size()>0)
			set.addAll(lookupLibrary(name));

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstDot.getRight().lookupInstClass(String name) {
		return getLeft().qualifiedLookupInstClass(name);
	}
	
	syn HashSet InstAccess.qualifiedLookupInstClass(String name) circular [emptyHashSet()] = emptyHashSet();	
	eq InstClassAccess.qualifiedLookupInstClass(String name) = myInstClassDecl().memberInstClass(name);

	// TODO: rewrite so that classes are only looked up in constraining types.	
	syn lazy HashSet InstNode.genericLookupInstClass(String name) circular [emptyHashSet()] {
	
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : instExtends()) {
			set.addAll(ie.memberInstClass(name));
		}

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}

		if (set.size()>0)
			return set;
		else
			return lookupInstClass(name);
	}
	
	syn HashSet InstNode.superLookupInstClass(String name) circular [emptyHashSet()] {
	
		HashSet set = new HashSet();

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}
						
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
					set.add(icd);
			}
		}		
	
		if (set.size()>0)
			return set;
		else
			return lookupInstClass(name);
	}

	// TODO: rewrite so that classes are only looked up in constraining types.	
	syn lazy HashSet InstComponentDecl.genericLookupInstClass(String name) circular [emptyHashSet()] {
	
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : instExtends()) {
			set.addAll(ie.memberInstClass(name));
		}

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}

		if (set.size()>0)
			return set;
		else
			return myInstClass().genericLookupInstClass(name);
	}


	// TODO: rewrite so that classes are only looked up in constraining types.	
	syn lazy HashSet InstComponentDecl.genericLookupInstClassInComponent(String name) circular [emptyHashSet()] {
	    if (myInstClass().isRedeclared())
			return myInstClass().lookupInstClassInInstClassRedeclare(name);
		
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : instExtends()) {
			set.addAll(ie.memberInstClass(name));
		}

		for (InstExtends ie : instExtends()) {
			HashSet h = ie.getClassName().lookupInstClass();
			if (h.size()==1)
				set.addAll(((InstClassDecl)h.iterator().next()).memberInstClass(name));
		}

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}

		if (set.size()>0) // Did not find any local class
			return set;
		else // continue search att myInstClass()
			return myInstClass().genericLookupInstClass(name);
	}
	
	syn HashSet InstComponentDecl.superLookupInstClassInComponent(String name) circular [emptyHashSet()] {
		if (myInstClass().isRedeclared())
			return myInstClass().lookupInstClassInInstClassRedeclare(name);
		
		HashSet set = new HashSet();

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}
							
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
					set.add(icd);
			}
		}		
	
		if (set.size()>0) //Found local class
			return set;
		else // else search for non-local class
			return myInstClass().superLookupInstClass(name);
	}

	
	// TODO: rewrite so that classes are only looked up in constraining types.	
	syn lazy HashSet InstExtends.genericLookupInstClassInExtends(String name) circular [emptyHashSet()] {
	   	if (getClassName().myInstClassDecl().isRedeclared())
			return getClassName().myInstClassDecl().lookupInstClassInInstClassRedeclare(name);
	
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : instExtends()) {
			set.addAll(ie.memberInstClass(name));
		}
	
		if (set.size()>0) // Did not find any local class
			return set;
		else // continue search att myInstClass()
			return getClassName().myInstClassDecl().genericLookupInstClass(name);
	}
	
	syn HashSet InstExtends.superLookupInstClassInExtends(String name) circular [emptyHashSet()] {
	   	if (getClassName().myInstClassDecl().isRedeclared())
			return getClassName().myInstClassDecl().lookupInstClassInInstClassRedeclare(name);
		
		HashSet set = new HashSet();
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
					set.add(icd);
			}
		}		
		if (set.size()>0) //Found local class
			return set;
		else // else search for non-local class
			return getClassName().myInstClassDecl().superLookupInstClass(name);
	}

	syn HashSet InstNode.memberInstClass(String name) circular [emptyHashSet()] = emptyHashSet();
	
	eq InstFullClassDecl.memberInstClass(String name) {
		HashSet set = new HashSet();
		
		for (InstClassDecl icd : constrainingInstClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : constrainingInstExtends()) {
			set.addAll(ie.memberInstClass(name));
		}
		
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstShortClassDecl.memberInstClass(String name) {
				
		if (hasInstConstraining())
			return getInstConstraining().getInstNode().memberInstClass(name);
		else {
			return getInstExtends(0).memberInstClass(name);			
		 }	
	
	}
	
	eq InstExtends.memberInstClass(String name) {
		HashSet set = new HashSet();
		
		for (InstClassDecl icd : constrainingInstClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : constrainingInstExtends()) {
			set.addAll(ie.memberInstClass(name));
		}
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstComponentDecl.memberInstClass(String name) {
		HashSet set = new HashSet();
		
		for (InstClassDecl icd : constrainingInstClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : constrainingInstExtends()) {
			set.addAll(ie.memberInstClass(name));
		}
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	syn boolean InstClassDecl.matchInstClassDecl(String name) = false;
	eq InstBuiltInClassDecl.matchInstClassDecl(String name) = name().equals(name); 
	eq InstBaseClassDecl.matchInstClassDecl(String name) = name().equals(name);
	
	// This attribute should to be circular due to lookup in import-statements
	// If the lookup is triggered by lookup of A in 'extends A', and
	// there are import statements, evaluation might get back to the 
	// ClassAccess A when when looking up the ClassAccess to import.
	// See NameTests.ImportTest1 for an example. 
	syn lazy InstClassDecl InstAccess.myInstClassDecl() circular [unknownInstClassDecl()] {
		 return unknownInstClassDecl();
	}
	eq InstClassAccess.myInstClassDecl()  {
		HashSet set = lookupInstClass(name());
		if (set.size() > 0)
			return (InstClassDecl)set.iterator().next();
		else
			return unknownInstClassDecl();
	}
	eq InstDot.myInstClassDecl() {
	 	return getRight().myInstClassDecl();
	}	
	
	syn lazy InstClassDecl InstNode.myInstClass() circular [unknownInstClassDecl()] = unknownInstClassDecl(); 
	eq InstComponentDecl.myInstClass() = getClassName().myInstClassDecl();
	//eq InstShortClassDecl.myInstClass()  = getClassName().myInstClassDecl();
	eq InstClassDecl.myInstClass() = this;
	eq InstExtends.myInstClass() = getClassName().myInstClassDecl();
	eq UnknownInstComponentDecl.myInstClass() = unknownInstClassDecl();	

	
}

aspect InstLookupImport {

	syn HashSet InstImport.lookupInstClassInImport(String name) circular [emptyHashSet()];
	
	eq InstImportQualified.lookupInstClassInImport(String name)   {
	    if (name.equals(getPackageName().getLastInstAccess().name())) {	
			// Use simple lookup: only classes which are not inherited can be
			// imported. 
	    	InstClassDecl icd = ((SourceRoot)root()).getProgram().getInstProgramRoot().
	           simpleLookupInstClassDecl(getPackageName().name());
	    	if (!icd.isUnknown()) {
	    		HashSet set = new HashSet(4);
	    		set.add(icd);
	    		return set;
	    	} 
		}
		return emptyHashSet();
	}
	
	eq InstImportUnqualified.lookupInstClassInImport(String name)  {
		// Use simple lookup: only classes which are not inherited can be
		// imported. 
        InstClassDecl myIcd = ((SourceRoot)root()).getProgram().getInstProgramRoot().
           simpleLookupInstClassDecl(getPackageName().name());
		
        return myIcd.memberInstClass(name);
	}
	
	eq InstImportRename.lookupInstClassInImport(String name)  {
	    
		// Does the alias name match?
		if (name.equals(((ImportClauseRename)getImportClause()).getIdDecl().getID())) {
			// Use simple lookup: only classes which are not inherited can be
			// imported. 
			InstClassDecl icd = ((SourceRoot)root()).getProgram().getInstProgramRoot().
	           simpleLookupInstClassDecl(getPackageName().name());
			if (!icd.isUnknown()) {
	    		HashSet set = new HashSet(4);
	    		set.add(icd);
	    		return set;
	    	} 
		} 
		return emptyHashSet();
	}
	
}

aspect InstLocalClasses {

	syn InstClassDecl InstClassDecl.localInstClassDecl(int i) = null;
	eq InstFullClassDecl.localInstClassDecl(int i) = instClassDecls().get(i);
	
	syn int InstClassDecl.numLocalInstClassDecl() = 0;
	eq InstFullClassDecl.numLocalInstClassDecl() = instClassDecls().size();

}

aspect InstLookupClassesInModifications {

	inh HashSet InstElementModification.lookupInstClassInInstClass(String name);	

	eq InstElementModification.getName().lookupInstClass(String name) = lookupInstClassInInstClass(name);

	eq InstComponentDecl.getInstModification().lookupInstClassInInstClass(String name) = /*myInstClass().*/memberInstClass(name);		
	eq InstElementModification.getInstModification().lookupInstClassInInstClass(String name) = getName().myInstComponentDecl().memberInstClass(name);
	eq InstExtends.getInstClassModification().lookupInstClassInInstClass(String name) = /*getClassName().myInstClassDecl().*/memberInstClass(name);
	
//	eq InstShortClassDecl.getInstClassModification().lookupInstClassInInstClass(String name) = getClassName().myInstClassDecl().memberInstClass(name);
	
	inh HashSet InstClassRedeclare.lookupInstClassInInstClass(String name);
	eq InstClassRedeclare.getName().lookupInstClass(String name) = lookupInstClassInInstClass(name);
	
	/**
	 * Terminating equation for attribute lookupInstClassInInstClass.
	 */
	eq InstRoot.getChild().lookupInstClassInInstClass(String name) {return emptyHashSet();}
	  
}

aspect SimpleInstClassLookup {
	
	/**
	 * This method offers a simplified way of looking up qualified class names
	 * in the instance class structure.
	 * 
	 * @param name A string containing a qualified name, for example 'A.B.C'.
	 */
	syn lazy InstClassDecl InstProgramRoot.simpleLookupInstClassDecl(String name) = 
		simpleLookupInstClassDeclRecursive(name);
	
	public InstClassDecl InstNode.simpleLookupInstClassDeclRecursive(String name) {
		// Split up name
		int ind = name.indexOf('.');
		String firstName = "";
		String lastName = "";
		if (ind==-1) {
			firstName = name;
			lastName = "";
		} else {
			firstName = name.substring(0,ind);
			lastName = name.substring(ind+1,name.length());
		}
		for (InstClassDecl icd_tmp : getInstClassDecls()) {
			if (icd_tmp.name().equals(firstName)) {
				if (lastName.equals("")) {
					return icd_tmp;
				} else {
					return icd_tmp.simpleLookupInstClassDeclRecursive(lastName);
				}
			}
		}
		return unknownInstClassDecl();
	}

	public InstClassDecl InstProgramRoot.simpleLookupInstClassDeclRecursive(String name) {
		// Split up name
		int ind = name.indexOf('.');
		String firstName = "";
		String lastName = "";
		if (ind==-1) {
			firstName = name;
			lastName = "";
		} else {
			firstName = name.substring(0,ind);
			lastName = name.substring(ind+1,name.length());
		}
		for (InstClassDecl icd_tmp : getInstClassDecls()) {
			if (icd_tmp.name().equals(firstName)) {
				if (lastName.equals("")) {
					return icd_tmp;
				} else {
					return icd_tmp.simpleLookupInstClassDeclRecursive(lastName);
				}
			}
		}
		HashSet<InstClassDecl> set = lookupLibrary(firstName);
		for (InstClassDecl icd_tmp : set) {
			if (icd_tmp.name().equals(firstName)) {
				if (lastName.equals("")) {
					return icd_tmp;
				} else {
					return icd_tmp.simpleLookupInstClassDeclRecursive(lastName);
				}
			}
		}

		return unknownInstClassDecl();
	}

	
}






