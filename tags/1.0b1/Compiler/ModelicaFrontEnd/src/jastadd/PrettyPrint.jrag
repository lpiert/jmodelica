import java.io.PrintStream;

/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


	// superclass to all classes that perform the dispatch
	class Printer {
  		public void toString(ASTNode node, PrintStream str, String indent, Object o) { 
  		}
	}

	// select first variant
	class PrettyPrinter extends Printer {
 		public void toString(ASTNode node, PrintStream str, String indent, Object o) { 
 			node.prettyPrint(this, str, indent, o); 
 		}
	}
	
	// select second variant
	class DumpTreePrinter extends Printer {
		public void toString(ASTNode node, PrintStream str, String indent, Object o) { 
 			node._dumpTree(this, str, indent, o); 
 		}
	}

aspect PrettyPrint {
	
	public void ASTNode._dumpTree(PrintStream str, String indent) {
 		_dumpTree(new DumpTreePrinter(),str,indent,null);
	}

	public void ASTNode._dumpTree(PrintStream str, String indent,Object o) {
 		_dumpTree(new DumpTreePrinter(),str,indent,o);
	}

	// dump node names
	public void ASTNode._dumpTree(Printer p, PrintStream str, String indent, Object o) {
 		str.print(indent + getClass().getName() + "\n");
 		for(int i = 0; i < getNumChild(); i++)
   			p.toString(getChild(i),str,indent + " ",o); // distpatch through Printer
	}

	public String ASTNode.prettyPrint(String indent) {
		StringOutputStream os = new StringOutputStream();
		PrintStream str = new PrintStream(os);
		prettyPrint(str,indent,null);
		return os.toString();
	}

	public String ASTNode.prettyPrint(String indent,Object o) {
		StringOutputStream os = new StringOutputStream();	
		PrintStream str = new PrintStream(os);
		prettyPrint(str,indent,o);
		return os.toString();
	}

	public void ASTNode.prettyPrint(PrintStream str,String indent) {
 		prettyPrint(new PrettyPrinter(),str,indent,null);
	}


	public void ASTNode.prettyPrint(PrintStream str,String indent, Object o) {
 		prettyPrint(new PrettyPrinter(),str,indent,o);
	}
	
	
	// firstPrint is default behavior for secondPrint
	public void ASTNode.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
 		//return _dumpTree(p,indent); // Create default behaviour
  		for(int i = 0; i < getNumChild(); i++)
   			p.toString(getChild(i),str,indent,o); // distpatch through Printer
	}
	
	public void Program.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		for (StoredDefinition sd : getUnstructuredEntitys()) {
			p.toString(sd,str,indent,o);
		}
	}
 	
 	public void StoredDefinition.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
 		if (hasWithin()) {
 			str.print(indent + "within");
 			if (getWithin().hasPackageName()) {
 				str.print(" ");
 				p.toString(getWithin().getPackageName(),str,indent,o);
 			}
 			str.print(";\n");
 		}
 		for (Element el : getElements()) {
	 		p.toString(el,str,indent,o);
 			str.print(";\n");		
		}
	}
	
	// specialize behavior for A
	
	
    public void ShortClassDecl.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
	    if (hasEncapsulated())
	    	str.print("encapsulated ");
	    if (hasPartial())
	    	str.print("partial ");
	    if (hasRedeclare())
	    	str.print("redeclare ");
	    if (hasFinal())
	    	str.print("final ");
	    if (hasInner())
	    	str.print("inner ");
	    if (hasOuter())
	    	str.print("outer ");
	    if (hasReplaceable())
	    	str.print("replaceable ");
	    
	    
	    str.print(indent + getRestriction().toString());
 		str.print(" " + getName().getID());
	    str.print(" = ");
	    str.print(getExtendsClauseShortClass().getSuper().name());
	    if (getExtendsClauseShortClass().hasArraySubscripts())
	    	p.toString(getExtendsClauseShortClass().getArraySubscripts(),str,indent+"  ",o);
	    if (getExtendsClauseShortClass().hasClassModification())
	    	p.toString(getExtendsClauseShortClass().getClassModification(),str,indent+"  ",o);
	    if (hasConstrainingClause())
	    	p.toString(getConstrainingClause(),str,indent+"  ",o);
	    
	}
	
	public void FullClassDecl.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
	    if (hasEncapsulated())
	    	str.print("encapsulated ");
	    if (hasPartial())
	    	str.print("partial "); 		
 	    if (hasRedeclare())
	    	str.print("redeclare ");
	    if (hasFinal())
	    	str.print("final ");
	    if (hasInner())
	    	str.print("inner ");
	    if (hasOuter())
	    	str.print("outer ");
	    if (hasReplaceable())
	    	str.print("replaceable ");
	    
 		
 		str.print(indent + getRestriction().toString());
 		str.print(" " + getName().getID() + "\n");

		
 		// Print all local classes
 		int numPubClass = 0;
// 		str.print(indent + "public\n");
 		for (int i=0;i<getNumClassDecl();i++)
 			if (((BaseClassDecl)getClassDecl(i)).isPublic()) {
 			 	numPubClass++;
	 			p.toString(getClassDecl(i),str,indent+"  ",o);
	 			str.print(";\n\n");
			}
			
		if (getNumClassDecl()-numPubClass>0) {	
	 		str.print(indent + "protected\n");
 			for (int i=0;i<getNumClassDecl();i++)
 				if (((BaseClassDecl)getClassDecl(i)).isProtected()) {
		 			p.toString(getClassDecl(i),str,indent+"  ",o);
		 			str.print(";\n\n");
		 		}
		}
			
		// Print all extends clauses
 		for (int i=0;i<getNumSuper();i++) {
 			p.toString(getSuper(i),str,indent+"  ",o);
 			str.print(";\n");
		} 			
			
 		// Print all components
 		int numPubComp = 0;
// 		str.print(indent + "public\n");
 		for (int i=0;i<getNumComponentDecl();i++)
 			if (getComponentDecl(i).isPublic()) {
 			 	numPubComp++;
	 			p.toString(getComponentDecl(i),str,indent+"  ",o);
	 			str.print(";\n");
			}
			
		if (getNumComponentDecl()-numPubComp>0) {	
	 		str.print(indent + "protected\n");
 			for (int i=0;i<getNumComponentDecl();i++)
 				if (getComponentDecl(i).isProtected()) {
		 			p.toString(getComponentDecl(i),str,indent+"  ",o);
		 			str.print(";\n");
			}
		}	
		
		if (getNumEquation()>0) {
			str.print(indent + "equation\n");
			for (int i=0;i<getNumEquation();i++) {
				str.print(indent + "  ");
				p.toString(getEquation(i),str,indent,o);
				str.print(";\n");
			}
		}
		
		str.print(indent + "end " + getName().getID());
	} 

	public void ComponentDecl.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(indent);//+getVisibilityType().toString()+" ";
		
		if (hasRedeclare())
			str.print(getRedeclare().toString() + " ");
		if (hasFinal())
			str.print(getFinal().toString() + " ");
		if (hasInner())
			str.print(getInner().toString() + " ");
		if (hasOuter())
			str.print(getOuter().toString() + " ");
		if (hasReplaceable())
			str.print(getReplaceable().toString() + " ");	
		if (hasTypePrefixFlow())
			str.print(getTypePrefixFlow().toString() + " ");
		if (hasTypePrefixVariability())
			str.print(getTypePrefixVariability().toString() + " ");
		if (hasTypePrefixInputOutput())
			str.print(getTypePrefixInputOutput().toString() + " ");
		
		p.toString(getClassName(),str,indent,o);
		if (hasTypeArraySubscripts())
			p.toString(getTypeArraySubscripts(),str,indent,o);
		str.print(" " + getName().getID()); 
	    if (hasVarArraySubscripts())
			p.toString(getVarArraySubscripts(),str,indent,o);
		 p.toString(getModificationOpt(),str,indent,o);
				 
		if (hasConstrainingClause()) {
		  p.toString(getConstrainingClause(),str,indent,o);
		
		}		 

				    
	}

	public void ConstrainingClause.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
       str.print(" constrainedby " + getAccess().name() +" ");
       if (hasClassModification())
	       p.toString(getClassModification(),str,indent,o);

	}

	public void ArraySubscripts.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (getNumSubscript()>0) {
			str.print("[");
			for (int i=0;i<getNumSubscript();i++) {
				p.toString(getSubscript(i),str,indent,o);
				if (i<getNumSubscript()-1)
					str.print(",");
			}
			str.print("]");
		}
	}

	public void ColonSubscript.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(":");
	}
	
	public void ExpSubscript.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getExp(),str,indent,o);
	}

	public void RangeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) { 
		for (int i=0;i<getNumExp();i++) {
			p.toString(getExp(i),str,indent,o);
			if (i<getNumExp()-1)
				str.print(":");
		}
	}

	public void ExtendsClause.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(indent + "extends ");
		p.toString(getSuper(),str,indent,o);
		p.toString(getClassModificationOpt(),str,indent,o);
	}

	public void CompleteModification.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getClassModification(),str,indent,o);
		if (hasValueModification())
		  p.toString(getValueModification(),str,indent,o);
	}
	
	public void ValueModification.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("=");
		p.toString(getExp(),str,indent,o);
	}
	
	public void ClassModification.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("(");
		for (int i=0;i<getNumArgument();i++) {
			p.toString(getArgument(i),str,indent,o);
			if (i<getNumArgument()-1)
				str.print(",");
		}
		str.print(")");
	}
	
	public void ElementModification.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (hasEach())
			str.print(getEach().toString() + " ");
		if (hasFinal())
		 	str.print(getFinal().toString() + " ");
		p.toString(getName(),str, indent,o);
		if (hasModification())
			p.toString(getModification(),str,indent,o);
	}


	public void ComponentRedeclare.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (hasEach())
			str.print(getEach().toString() + " ");
		if (hasFinal())
		 	str.print(getFinal().toString() + " ");
		p.toString(getComponentDecl(),str,indent,o);
		

	}

	public void ClassRedeclare.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (hasEach())
			str.print(getEach().toString() + " ");
		if (hasFinal())
		 	str.print(getFinal().toString() + " ");
		p.toString(getBaseClassDecl(),str,indent,o);
		

	}	

	public void Opt.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (getNumChild()>0)
			p.toString(getChild(0),str,indent,o);
	}

	public void Equation.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		str.print(" = ");
		p.toString(getRight(),str,indent,o);
	}

	public void ConnectClause.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		str.print("connect(");
		p.toString(getConnector1(),str,indent,o);
		str.print(",");
		p.toString(getConnector2(),str,indent,o);
		str.print(")");
	}
	
	public void AddExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		if(!(getRight() instanceof NegExp)) 
			str.print("+");
		p.toString(getRight(),str,indent,o);
	}
		
	public void SubExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		str.print("-");
		if (getRight().isAddOrSub()) {
			str.print("(");
			p.toString(getRight(),str,indent,o);
			str.print(")");
		} else
			p.toString(getRight(),str,indent,o);
	}
		
	public void MulExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		if (getLeft().isAddOrSub()) {
			str.print("(");
			p.toString(getLeft(),str,indent,o);
			str.print(")");
		} else
			p.toString(getLeft(),str,indent,o);	
		str.print("*");
		if (getRight().isAddOrSub()) {
			str.print("(");
			p.toString(getRight(),str,indent,o);
			str.print(")");
		} else
			p.toString(getRight(),str,indent,o);
	}
		
	public void DivExp.prettyPrint(Printer p, PrintStream str, String indent, Object o){
		if (getLeft().isAddOrSub()) {
			str.print("(");
			p.toString(getLeft(),str,indent,o);
			str.print(")");
		} else
			p.toString(getLeft(),str,indent,o);
		str.print("/");	
		str.print("(");
		p.toString(getRight(),str,indent,o);
		str.print(")");
	}
		
	public void PowExp.prettyPrint(Printer p, PrintStream str, String indent, Object o){
		if (!(getLeft().isPrimary())) {
			str.print("(");
			p.toString(getLeft(),str,indent,o);
			str.print(")");
		} else
			p.toString(getLeft(),str,indent,o);
		str.print("^");
		if (!(getRight().isPrimary())) {
			str.print("(");
			p.toString(getRight(),str,indent,o);
			str.print(")");
		} else
			p.toString(getRight(),str,indent,o);
	}
		
	public void NegExp.prettyPrint(Printer p,PrintStream str, String indent, Object o){
		str.print("-");
		if (getExp().isAddOrSub()) {
			str.print("(");
			p.toString(getExp(),str,indent,o);
			str.print(")");
		} else
			p.toString(getExp(),str,indent,o);
	}
	
	public void RealLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getUNSIGNED_NUMBER());
	}

	public void IntegerLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getUNSIGNED_INTEGER());
	}
	
	public void TimeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("time");
	}

	public void EndExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("end");
	}
	
	public void FunctionCall.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getName(),str,indent,o);
		str.print("("); 
		p.toString(getFunctionArgumentsOpt(),str,indent,o);
		str.print(")");
	}

	public void SumRedExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sum("); 
		p.toString(getExp(),str,indent,o);
		str.print(" for ");
		str.print(getForIndex().getForIndexDecl().name());
		str.print(" in ");
		if (getForIndex().hasExp())
			p.toString(getForIndex().getExp(),str,indent,o);
		str.print(")");
	}


	public void SizeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("size(");
		p.toString(getAccess(),str,"",o);
		str.print(",");
		p.toString(getDim(),str,"",o);
		str.print(")");
	}

	public void FunctionArguments.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		for (int i=0; i<getNumExp(); i++) {
			p.toString(getExp(i),str,indent,o);
			if (i<getNumExp()-1)
				str.print(",");
		}
	}
	
	public void ExpList.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		for (int i=0; i<getNumExp(); i++) {
			p.toString(getExp(i),str,indent,o);
			if (i<getNumExp()-1)
				str.print(",");
		}
	}

	public void Access.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getID());
	}
		
	public void Dot.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		str.print(".");
		p.toString(getRight(),str,indent,o);	
	}
	
	public void ComponentAccess.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getID());
		if (hasArraySubscripts())
			p.toString(getArraySubscripts(),str,indent,o);
	}
	
	public void StringLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("\"" + getSTRING() + "\"");
	}

	public void BooleanLitExpTrue.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("true");
	}

	public void BooleanLitExpFalse.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("false");
	}
	
	public void ArrayConstructor.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("{");
		p.toString(getFunctionArguments(),str,indent,o);
		str.print("}");
	}
	
	public void Matrix.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("[");
		for (int i=0;i<getNumExpList();i++) {
			p.toString(getExpList(i),str,indent,o);
			if (i<getNumExpList()-1)
				str.print(";");
		}
		str.print("]");
	}
	
	syn boolean Exp.isAddOrSub() = false;
	eq AddExp.isAddOrSub() = true;
	eq SubExp.isAddOrSub() = true;
	syn boolean Exp.isPrimary() = false;
	eq AccessExp.isPrimary() = true;
	eq RealLitExp.isPrimary() = true;

	public void LogBinExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		//str.print("((");
		p.toString(getLeft(),str,indent,o);
		//str.print(")"+op()+"(");
		str.print(op());
		p.toString(getRight(),str,indent,o);
		//str.print("))");
	}
	
	syn String LogBinExp.op();
	eq LtExp.op() = "<";
	eq LeqExp.op() = "<=";
	eq GtExp .op() = ">";
	eq GeqExp.op() = ">=";
	eq EqExp.op() = "==";
	eq NeqExp.op() = "<>";
	eq OrExp.op() = " or ";
	eq AndExp.op() = " and ";
	
	public void NotExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("not ");
		p.toString(getExp(),str,indent,o);
	}


	syn String Redeclare.toString() = "redeclare";
	syn String Final.toString() = "final";
	syn String Inner.toString() = "inner";
	syn String Outer.toString() = "outer";
	syn String Replaceable.toString() = "replaceable";
	syn String TypePrefixFlow.toString() = "flow";
	syn String TypePrefixInputOutput.toString();
	eq Input.toString() = "input";
	eq Output.toString()= "output";
	syn String TypePrefixVariability.toString();
	eq Parameter.toString() = "parameter";
	eq Discrete.toString() = "discrete";
	eq Constant.toString() = "constant";
	eq Continuous.toString() = "";
	
	syn String FPrimitiveType.toString();
	eq FRealScalarType.toString() = "Real";
	eq FIntegerScalarType.toString() = "Integer";
	eq FStringScalarType.toString() = "String";
	eq FBooleanScalarType.toString() = "Boolean";	
	
	eq FRealArrayType.toString() { 
		StringBuffer str = new StringBuffer("Real[");
		int[] size = size();
		for (int i=0;i<size.length;i++) {
			str.append(size[i]);
			if (i<size.length-1) {
				str.append(",");
			}
		}
		str.append("]");
		return str.toString();
	}
	eq FIntegerArrayType.toString() { 
		StringBuffer str = new StringBuffer("Integer[");
		int[] size = size();
		for (int i=0;i<size.length;i++) {
			str.append(size[i]);
			if (i<size.length-1) {
				str.append(",");
			}
		}
		str.append("]");
		return str.toString();
	}
	eq FStringArrayType.toString() = "String";
	eq FBooleanArrayType.toString() = "Boolean";	
	
	eq FUnknownScalarType.toString() = "FUnknownScalarType";	
	eq FUnknownArrayType.toString() = "FUnknownArrayType";	
	eq FUnknownType.toString() = "FUnknownType";	

}

aspect FlattPrettyPrint {
	
	public void FClass.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(indent+"fclass "+ name() +"\n");
		for (int i=0;i<getNumFVariable();i++) {
			debugPrint("FClass.prettyPrint(): FVariable: " + getFVariable(i).name() + " is$Final: " + is$Final);
	  		if (!getFVariable(i).isDerivativeVariable()) {
	  			p.toString(getFVariable(i),str,indent+" ",o);
	  		str.print(";\n");  
	  		}
		}

		if (getNumFInitialEquation()>0)
			str.print(indent + "initial equation \n");
	    for (int j=0;j<getNumFInitialEquation();j++) {
			debugPrint("FClass.prettyPrint(): Equation nr: " + j);
	  		p.toString(getFInitialEquation(j),str, indent+" ",o);
			str.print(";\n");
		}
	
		
		str.print(indent + "equation \n");
	  	for (int i=0;i<getNumFEquationBlock();i++) {
			for (int j=0;j<getFEquationBlock(i).getNumFAbstractEquation();j++) {
				debugPrint("FClass.prettyPrint(): Equation nr: " + j);
	  			p.toString(getFEquationBlock(i).getFAbstractEquation(j),str, indent+" ",o);
				str.print(";\n");
			}
		}
  		str.print(indent);
  		str.print("end ");
  		str.print(name());
  		str.print(";\n");
	}

	public void FVariable.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		
			str.print(indent);
			if (isProtected()) {
				str.print("protected ");
			}
			
			str.print(getFTypePrefixVariability().toString());
						
			if (hasFTypePrefixInputOutput()) {
				str.print(getFTypePrefixInputOutput().toString());
				str.print(" ");
			}
			
			str.print(type().scalarType());
			str.print(" ");
			p.toString(getFQName(),str,"",o);
		
	    	getFAttributeList().prettyPrintFAttributeList(str,p,o);
	    	
	   if (hasBindingExp()) {
				str.print(" = ");
				FExp bindingExp = getBindingExp();
				p.toString(bindingExp,str,indent,o);
				
			}
	    	
	    if (hasFStringComment()) {
	    	str.print(" \"");
	    	str.print(getFStringComment().getComment());
	    	str.print("\"");
	    }

	    if (isIndependentParameter() && hasBindingExp()) {
	    	str.print(" /* ");
	    	try {
		    	if (isReal()) {
					str.print(getBindingExp().ceval().realValue());
				} else if (isInteger()) {
					str.print(getBindingExp().ceval().intValue());
				} else if (isBoolean()) {
					str.print(getBindingExp().ceval().booleanValue());
				} else if (isString()) {
					str.print(getBindingExp().ceval().stringValue());
				}
			} catch (UnsupportedOperationException e){
				str.print("evalutation error");
			}
	    	str.print(" */");
	    }
	    
		}

	
	public void FDerivativeVariable.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(indent);
		if (isProtected()) {
			str.print("protected ");
		}
				
		str.print(type());
		str.print(" ");
		str.print(name());		
	}
	
	public void List.prettyPrintFAttributeList(PrintStream str, Printer p, Object o) {
	
	    boolean attrSet = false;
	    	boolean firstAttr = true;
	    	
	    	for (int i=0;i<getNumChild();i++) {
	    		if (((FAttribute)getChild(i)).getAttributeSet()) {
					attrSet=true;
					break;
	 		   	}
	    	}
	    
	    	if (attrSet){
	    		str.print("(");
	    		for (int i=0;i<getNumChild();i++) {
	    
	    			if (((FAttribute)getChild(i)).getAttributeSet()) {
	
						if (!firstAttr)	
							str.print(",");
						p.toString(((FAttribute)getChild(i)),str,"",o);
						
						firstAttr = false;
	 		   		}
	    		}
	    		str.print(")");
	   		}
	
	}
	
    public void FAttribute.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
			
						if (hasFEach())
							str.print("each ");
						if (hasFFinal())
							str.print("final ");	
		    			str.print(getName().name());
		    			getFAttributeList().prettyPrintFAttributeList(str,p,o);
		    			if (hasValue()) {
		    				str.print(" = ");
	    					p.toString(getValue(),str,"",o);
						}
	}

    public void FQNamePart.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
    	str.print(getName());
		if (hasFArraySubscripts())
			p.toString(getFArraySubscripts(),str,indent,o);				    	
    }
    
    public void FQName.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		for (int i = 0; i < getNumFQNamePart(); i++) {
			p.toString(getFQNamePart(i),str,indent,o);
			if (i<getNumFQNamePart()-1)
				str.print(".");
		}    	
    }
    
    public void FArraySubscripts.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		if (getNumFSubscript()>0) {
			str.print("[");
			for (int i=0;i<getNumFSubscript();i++) {
				p.toString(getFSubscript(i),str,indent,o);
				if (i<getNumFSubscript()-1)
					str.print(",");
			}
			str.print("]");
		}
	}

	public void FColonSubscript.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(":");
	}
	
	public void FExpSubscript.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getFExp(),str,indent,o);
	}

	public void FRangeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) { 
		for (int i=0;i<getNumFExp();i++) {
			p.toString(getFExp(i),str,indent,o);
			if (i<getNumFExp()-1)
				str.print(":");
		}
	}

	public void FEquation.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(indent);
    	p.toString(getLeft(),str,indent,o);
   		str.print(" = ");
   		p.toString(getRight(),str,indent,o);
	}
	
	public void FAddExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		if(!(getRight() instanceof FNegExp)) 
			str.print(" + ");
		p.toString(getRight(),str,indent,o);
	}
		
	public void FSubExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		str.print(" - ");
//		if (getRight().isAddOrSub()) {
			str.print("( ");
			p.toString(getRight(),str,indent,o);
			str.print(" )");
//		} else
//			p.toString(getRight(),str,indent,o);
	}
		
	public void FMulExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		str.print("( ");
		p.toString(getLeft(),str,indent,o);
		str.print(" )");
		str.print(" * ");
		str.print("( ");
		p.toString(getRight(),str,indent,o);
		str.print(" )");
	}
		
	public void FDivExp.prettyPrint(Printer p, PrintStream str, String indent, Object o){
		str.print("( ");
		p.toString(getLeft(),str,indent,o);
		str.print(" )");
		str.print(" / ");	
		str.print("( ");
		p.toString(getRight(),str,indent,o);
		str.print(" )");
	}
		
	public void FPowExp.prettyPrint(Printer p, PrintStream str, String indent, Object o){
		if (!(getLeft().isPrimary())) {
			str.print("( ");
			p.toString(getLeft(),str,indent,o);
			str.print(" )");
		} else
			p.toString(getLeft(),str,indent,o);
		str.print(" ^ ");
		if (!(getRight().isPrimary())) {
			str.print("( ");
			p.toString(getRight(),str,indent,o);
			str.print(" )");
		} else
			p.toString(getRight(),str,indent,o);
	}
		
	public void FNegExp.prettyPrint(Printer p,PrintStream str, String indent, Object o){
		str.print(" - ");
//		if (getFExp().isAddOrSub()) {
			str.print("( ");
			p.toString(getFExp(),str,indent,o);
			str.print(" )");
//		} else
//			p.toString(getFExp(),str,indent,o);
	}
	
	public void FLogBinExp.prettyPrint(Printer p,PrintStream str, String indent, Object o) {
		p.toString(getLeft(),str,indent,o);
		str.print(op());
		p.toString(getRight(),str,indent,o);
	}
	
	syn String FLogBinExp.op();
	eq FLtExp.op() = " < ";
	eq FLeqExp.op() = " <= ";
	eq FGtExp .op() = " > ";
	eq FGeqExp.op() = " >= ";
	eq FEqExp.op() = " == ";
	eq FNeqExp.op() = " <> ";
	eq FOrExp.op() = " or ";
	eq FAndExp.op() = " and ";
	
	public void FNotExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("not ");
		p.toString(getFExp(),str,indent,o);
	}
	
	public void FIfExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("**************************** FIfExp.prettyPrint");
		str.print("(if ");
		p.toString(getIfExp(),str,indent,o);
		str.print(" then ");
		p.toString(getThenExp(),str,indent,o);
		str.print("\n ");
		for (int i=0;i<getNumFElseIfExp();i++) {
			p.toString(getFElseIfExp(i),str,indent,o);
			str.print("\n ");
		}
		str.print("else ");
		p.toString(getElseExp(),str,indent,o);		
		str.print(")");
	}
	
	public void FElseIfExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("elseif ");
		p.toString(getIfExp(),str,indent,o);
		str.print(" then ");
		p.toString(getThenExp(),str,indent,o);
	}
	
	public void FRealLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getUNSIGNED_NUMBER());
	}

	public void FIntegerLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getUNSIGNED_INTEGER());
	}

	
	public void FTimeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("time");
	}

	public void FEndExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("end");
	}
	
	public void FStringLitExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("\"" + getString() + "\"");
	}

	public void FBooleanLitExpTrue.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("true");
	}

	public void FBooleanLitExpFalse.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("false");
	}
	
	public void FIdUse.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("FIdUse.prettyPrint(): " + name());
		str.print(name());
	}

	public void FIdUseExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("FIdUse.prettyPrint(): " + getFIdUse().name());
		str.print(getFIdUse().name());
		if (getFIdUse().hasFArraySubscripts()) {
			p.toString(getFIdUse().getFArraySubscripts(),str,indent,o);
		}
	}

	public void FInstAccessExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("FIdUse.prettyPrint(): " + name());
		str.print(getInstAccess().name());
	}
	
	public void FDer.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("FDer.prettyPrint");
		str.print(name());
	}

	public void FSinExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sin(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FCosExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("cos(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FTanExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("tan(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}
	
	public void FAsinExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("asin(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FAcosExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("acos(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FAtanExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("atan(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FAtan2Exp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("atan2(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FSinhExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sinh(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FCoshExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("cosh(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FTanhExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("tanh(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FExpExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("exp(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FLogExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("log(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FLog10Exp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("log10(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FSqrtExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sqrt(");
		p.toString(getFExp(), str, indent, o);
		str.print(")");
	}

	public void FSumExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sum(");
		p.toString(getFExp(),str,indent,o);
		str.print(")");
	}
	
	public void FScalar.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("scalar(");
		p.toString(getFExp(),str,indent,o);
		str.print(")");
	}
	
	public void FTranspose.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("transpose(");
		p.toString(getFExp(),str,indent,o);
		str.print(")");
	}
	
	public void FFunctionCall.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		debugPrint("FFunctionCall.prettyPrint:");
		str.print(getName().name());
		str.print("(");
		for (int i=0; i<getNumArgs(); i++) {
				p.toString(getArgs(i),str,"",o);
				if (i<getNumArgs()-1)
					str.print(",");
		}
		str.print(")");
	}

	public void InstFunctionCall.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print(getName().name());
		str.print("(");
		for (int i=0; i<getNumArgs(); i++) {
				p.toString(getArgs(i),str,"",o);
				if (i<getNumArgs()-1)
					str.print(",");
		}
		str.print(")");
	}

	
	public void FSumRedExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("sum("); 
		p.toString(getFExp(),str,indent,o);
		str.print(" for ");
		str.print(getFForIndex().getFIdDecl().name());
		str.print(" in ");
		if (getFForIndex().hasFExp())
			p.toString(getFForIndex().getFExp(),str,indent,o);
		str.print(")");
	}
	
	public void FForClauseE.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
	
		str.print(indent+"for ");
		str.print(getFForIndex(0).getFIdDecl().name());
		str.print(" in ");
		if (getFForIndex(0).hasFExp())
			p.toString(getFForIndex(0).getFExp(),str,indent,o);
		str.print(" loop\n");
		for (int j=0;j<getNumForEqns();j++) {
				//debugPrint("FClass.prettyPrint(): Equation nr: " + j);
	  			p.toString(getForEqns(j),str, indent+" ",o);
				str.print(";\n");
		}
		
		str.print(indent+"end loop");
	
	
	}
	
	public void FSizeExp.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("size(");
		p.toString(getFIdUseExp(),str,"",o);
		str.print(",");
		p.toString(getDim(),str,"",o);
		str.print(")");
	}
	
	public void FArray.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("{");
		for (int i=0; i<getNumFExp(); i++) {
			p.toString(getFExp(i),str,indent,o);
			if (i<getNumFExp()-1)
				str.print(",");
		}
		str.print("}");
	}
	
	public void FMatrix.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		str.print("[");
		for (int i=0;i<getNumFExpList();i++) {
			p.toString(getFExpList(i),str,indent,o);
			if (i<getNumFExpList()-1)
				str.print(";");
		}
		str.print("]");
	}
	
	public void FExpList.prettyPrint(Printer p, PrintStream str, String indent, Object o) {
		for (int i=0; i<getNumFExp(); i++) {
			p.toString(getFExp(i),str,indent,o);
			if (i<getNumFExp()-1)
				str.print(",");
		}
	}
	
	syn boolean FExp.isAddOrSub() = false;
	eq FAddExp.isAddOrSub() = true;
	eq FSubExp.isAddOrSub() = true;
	syn boolean FExp.isPrimary() = false;
	eq FIdUseExp.isPrimary() = true;
	eq FRealLitExp.isPrimary() = true;
	eq FIntegerLitExp.isPrimary() = true;
	eq FBooleanLitExp.isPrimary() = true;
	eq FStringLitExp.isPrimary() = true;

	syn String FTypePrefixInputOutput.toString();
	eq FInput.toString() = "input";
	eq FOutput.toString()= "output";
	syn String FTypePrefixVariability.toString();
	eq FParameter.toString() = "parameter ";
	eq FDiscrete.toString() = "discrete ";
	eq FConstant.toString() = "constant ";
	eq FContinuous.toString() = "";	
	
	syn String Restriction.toString();
	eq Model.toString() = "model";
	eq Block.toString() = "block";
	eq MClass.toString() = "class";
	eq Connector.toString() = "connector";
	eq MType.toString() = "type";
	eq MPackage.toString() = "package";
	eq Function.toString() = "function";
	eq Record.toString() = "record";

    syn String ComponentDecl.toString() = prettyPrint("");
    syn String Access.toString() = prettyPrint("");
    syn String Modification.toString() = prettyPrint("");
}