<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>First Order Derivative: Driver Routine</title>
<meta name="description" id="description" content="First Order Derivative: Driver Routine"/>
<meta name="keywords" id="keywords" content=" derivative first order driver easy "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_revone_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="forone.cpp.xml" target="_top">Prev</a>
</td><td><a href="revone.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Drivers</option>
<option>RevOne</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Drivers-&gt;</option>
<option>Jacobian</option>
<option>ForOne</option>
<option>RevOne</option>
<option>Hessian</option>
<option>ForTwo</option>
<option>RevTwo</option>
<option>sparse_jacobian</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>RevOne-&gt;</option>
<option>RevOne.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>i</option>
<option>dw</option>
<option>Vector</option>
<option>RevOne Uses Forward</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>












<center><b><big><big>First Order Derivative: Driver Routine</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>dw</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.RevOne(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 to denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>
 corresponding to <i>f</i>.
The syntax above sets <i>dw</i> to the
derivative of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow></math>

 with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>dw</mi>
<mo stretchy="false">=</mo>
<msubsup><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msubsup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">[</mo><mrow><mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">,</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
</mrow></math>

<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>Note that the <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i> is not <code><font color="blue">const</font></code>
(see <a href="revone.xml#RevOne Uses Forward" target="_top"><span style='white-space: nowrap'>RevOne&#xA0;Uses&#xA0;Forward</span></a>
 below).

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="revone.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size 
must be equal to <i>n</i>, the dimension of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for <i>f</i>.
It specifies
that point at which to evaluate the derivative.

<br/>
<br/>
<b><big><a name="i" id="i">i</a></big></b>
<br/>
The index <i>i</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is less than 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
</mrow></math>

, the dimension of the
<a href="seqproperty.xml#Range" target="_top"><span style='white-space: nowrap'>range</span></a>
 space for <i>f</i>.
It specifies the
component of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
</mrow></math>

 that we are computing the derivative of.

<br/>
<br/>
<b><big><a name="dw" id="dw">dw</a></big></b>
<br/>
The result <i>dw</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>dw</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="revone.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size is <i>n</i>, the dimension of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for <i>f</i>.
The value of <i>dw</i> is the derivative of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow></math>

 
evaluated at <i>x</i>; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>

 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mo stretchy="false">.</mo>
<mi mathvariant='italic'>dw</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>j</mi>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="Vector" id="Vector">Vector</a></big></b>
<br/>
The type <i>Vector</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type</span></a>

<i>Base</i>.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="RevOne Uses Forward" id="RevOne Uses Forward">RevOne Uses Forward</a></big></b>
<br/>
After each call to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
,
the object <i>f</i> contains the corresponding 
<a href="glossary.xml#Taylor Coefficient" target="_top"><span style='white-space: nowrap'>Taylor&#xA0;coefficients</span></a>
.
After <code><font color="blue">RevOne</font></code>,
the previous calls to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 are undefined.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The routine 
<a href="revone.cpp.xml" target="_top"><span style='white-space: nowrap'>RevOne</span></a>
 is both an example and test.
It returns <code><font color="blue">true</font></code>, if it succeeds and <code><font color="blue">false</font></code> otherwise.


<hr/>Input File: cppad/local/rev_one.hpp

</body>
</html>
