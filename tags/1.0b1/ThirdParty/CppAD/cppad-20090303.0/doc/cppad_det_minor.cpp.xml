<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>CppAD Speed: Gradient of Determinant by Minor Expansion</title>
<meta name="description" id="description" content="CppAD Speed: Gradient of Determinant by Minor Expansion"/>
<meta name="keywords" id="keywords" content=" cppad speed minor link_det_minor "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_cppad_det_minor.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="speed_cppad.xml" target="_top">Prev</a>
</td><td><a href="cppad_det_lu.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_cppad</option>
<option>cppad_det_minor.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed_cppad-&gt;</option>
<option>cppad_det_minor.cpp</option>
<option>cppad_det_lu.cpp</option>
<option>cppad_ode.cpp</option>
<option>cppad_poly.cpp</option>
<option>cppad_sparse_hessian.cpp</option>
</select>
</td>
<td>cppad_det_minor.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>link_det_minor</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>CppAD Speed: Gradient of Determinant by Minor Expansion</big></big></b></center>
<br/>
<b><big><a name="link_det_minor" id="link_det_minor">link_det_minor</a></big></b>

<br/>
Routine that computes the gradient of determinant using CppAD:
<code><font color='blue'><pre style='display:inline'> 
# include &lt;cppad/vector.hpp&gt;
# include &lt;cppad/speed/det_by_minor.hpp&gt;
# include &lt;cppad/speed/uniform_01.hpp&gt;

bool link_det_minor(
	size_t                     size     , 
	size_t                     repeat   , 
	bool                       retape   ,
	CppAD::vector&lt;double&gt;     &amp;matrix   ,
	CppAD::vector&lt;double&gt;     &amp;gradient )
{
	// -----------------------------------------------------
	// setup

	// object for computing determinant
	typedef CppAD::AD&lt;double&gt;       ADScalar; 
	typedef CppAD::vector&lt;ADScalar&gt; ADVector; 
	CppAD::det_by_minor&lt;ADScalar&gt;   Det(size);

	size_t i;               // temporary index
	size_t m = 1;           // number of dependent variables
	size_t n = size * size; // number of independent variables
	ADVector   A(n);        // AD domain space vector
	ADVector   detA(m);     // AD range space vector
	
	// vectors of reverse mode weights 
	CppAD::vector&lt;double&gt; w(1);
	w[0] = 1.;

	if(retape) while(repeat--)
	{
		// choose a matrix
		CppAD::uniform_01(n, matrix);
		for( i = 0; i &lt; size * size; i++)
			A[i] = matrix[i];
	
		// declare independent variables
		Independent(A);
	
		// AD computation of the determinant
		detA[0] = Det(A);
	
		// create function object f : A -&gt; detA
		CppAD::ADFun&lt;double&gt; f(A, detA);
	
		// get the next matrix
		CppAD::uniform_01(n, matrix);
	
		// evaluate the determinant at the new matrix value
		f.Forward(0, matrix);
	
		// evaluate and return gradient using reverse mode
		gradient = f.Reverse(1, w);
	}
	else
	{
		// choose a matrix
		CppAD::uniform_01(n, matrix);
		for( i = 0; i &lt; size * size; i++)
			A[i] = matrix[i];
	
		// declare independent variables
		Independent(A);
	
		// AD computation of the determinant
		detA[0] = Det(A);
	
		// create function object f : A -&gt; detA
		CppAD::ADFun&lt;double&gt; f(A, detA);
	
		// ------------------------------------------------------
		while(repeat--)
		{	// get the next matrix
			CppAD::uniform_01(n, matrix);
	
			// evaluate the determinant at the new matrix value
			f.Forward(0, matrix);
	
			// evaluate and return gradient using reverse mode
			gradient = f.Reverse(1, w);
		}
	}
	return true;
}
</pre></font></code>


<hr/>Input File: speed/cppad/det_minor.cpp

</body>
</html>
