<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Forward Mode Second Partial Derivative Driver</title>
<meta name="description" id="description" content="Forward Mode Second Partial Derivative Driver"/>
<meta name="keywords" id="keywords" content=" partial second order driver easy "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_fortwo_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="heslagrangian.cpp.xml" target="_top">Prev</a>
</td><td><a href="fortwo.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Drivers</option>
<option>ForTwo</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Drivers-&gt;</option>
<option>Jacobian</option>
<option>ForOne</option>
<option>RevOne</option>
<option>Hessian</option>
<option>ForTwo</option>
<option>RevTwo</option>
<option>sparse_jacobian</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>ForTwo-&gt;</option>
<option>ForTwo.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>j</option>
<option>k</option>
<option>ddy</option>
<option>VectorBase</option>
<option>VectorSize_t</option>
<option>ForTwo Uses Forward</option>
<option>Examples</option>
</select>
</td>
</tr></table><br/>













<center><b><big><big>Forward Mode Second Partial Derivative Driver</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>ddy</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.ForTwo(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>j</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>k</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 to denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>
 corresponding to <i>f</i>.
The syntax above sets 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>ddy</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mn>2</mn>
</msup>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
</mrow>
</msub>
<mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
</mrow>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>


and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
</mrow></math>

,
where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
</mrow></math>

 is the size of the vectors <i>j</i> and <i>k</i>.

<br/>
<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>Note that the <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i> is not <code><font color="blue">const</font></code>
(see <a href="fortwo.xml#ForTwo Uses Forward" target="_top"><span style='white-space: nowrap'>ForTwo&#xA0;Uses&#xA0;Forward</span></a>
 below).

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>VectorBase</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="fortwo.xml#VectorBase" target="_top"><span style='white-space: nowrap'>VectorBase</span></a>
 below)
and its size 
must be equal to <i>n</i>, the dimension of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for <i>f</i>.
It specifies
that point at which to evaluate the partial derivatives listed above.

<br/>
<br/>
<b><big><a name="j" id="j">j</a></big></b>
<br/>
The argument <i>j</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>VectorSize_t</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>j</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="fortwo.xml#VectorSize_t" target="_top"><span style='white-space: nowrap'>VectorSize_t</span></a>
 below)
We use <i>p</i> to denote the size of the vector <i>j</i>.
All of the indices in <i>j</i> 
must be less than <i>n</i>; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">&lt;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="k" id="k">k</a></big></b>
<br/>
The argument <i>k</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>VectorSize_t</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>k</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="fortwo.xml#VectorSize_t" target="_top"><span style='white-space: nowrap'>VectorSize_t</span></a>
 below)
and its size must be equal to <i>p</i>,
the size of the vector <i>j</i>.
All of the indices in <i>k</i> 
must be less than <i>n</i>; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">&lt;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="ddy" id="ddy">ddy</a></big></b>
<br/>
The result <i>ddy</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>VectorBase</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>ddy</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="fortwo.xml#VectorBase" target="_top"><span style='white-space: nowrap'>VectorBase</span></a>
 below)
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>p</mi>
</mrow></math>

.
It contains the requested partial derivatives; to be specific,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>

 
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>



<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>ddy</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mn>2</mn>
</msup>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
</mrow>
</msub>
<mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">[</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">]</mo>
</mrow>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="VectorBase" id="VectorBase">VectorBase</a></big></b>
<br/>
The type <i>VectorBase</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;Base</span></a>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="VectorSize_t" id="VectorSize_t">VectorSize_t</a></big></b>
<br/>
The type <i>VectorSize_t</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;size_t</span></a>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="ForTwo Uses Forward" id="ForTwo Uses Forward">ForTwo Uses Forward</a></big></b>
<br/>
After each call to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
,
the object <i>f</i> contains the corresponding 
<a href="glossary.xml#Taylor Coefficient" target="_top"><span style='white-space: nowrap'>Taylor&#xA0;coefficients</span></a>
.
After <code><font color="blue">ForTwo</font></code>,
the previous calls to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 are undefined.


<br/>
<br/>
<b><big><a name="Examples" id="Examples">Examples</a></big></b>

<br/>
The routine 
<a href="fortwo.cpp.xml" target="_top"><span style='white-space: nowrap'>ForTwo</span></a>
 is both an example and test.
It returns <code><font color="blue">true</font></code>, if it succeeds and <code><font color="blue">false</font></code> otherwise.


<hr/>Input File: cppad/local/for_two.hpp

</body>
</html>
