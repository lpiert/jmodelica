<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Limitations</title>

  <para>This page lists the current limitations of the JModelica.org platform,
  as of version 1.2.0. The development of the platform can be followed at the
  Trac site, where future releases and associated features are planned. In
  order to get an idea of the current Modelica compliance of the compiler
  front-end, you may look at the associated test suite. All models with a test
  annotation can be flattened.</para>

  <itemizedlist>
    <listitem>
      <para>The Modelica compliance of the front-end is limited; the following
      features are currently not supported:</para>

      <itemizedlist>
        <listitem>
          <para>If expressions are supported, but not:</para>

          <itemizedlist>
            <listitem>
              <para>When clauses</para>
            </listitem>

            <listitem>
              <para>If equations</para>
            </listitem>
          </itemizedlist>
        </listitem>

        <listitem>
          <para>Parsing of full Modelica 3.2 (Modelica 3.0 is
          supported)</para>
        </listitem>

        <listitem>
          <para>Integer and boolean variables (integer and boolean parameters
          and constants are supported)</para>
        </listitem>

        <listitem>
          <para>Strings</para>
        </listitem>

        <listitem>
          <para>Enumerations</para>
        </listitem>

        <listitem>
          <para>Generics (redeclare constructs) is only partially
          supported</para>
        </listitem>

        <listitem>
          <para>Limitations apply to the use of functions with arguments with
          unknown array sizes</para>
        </listitem>

        <listitem>
          <para>External functions</para>
        </listitem>

        <listitem>
          <para>The following built-in functions are not supported: sign(v),
          Integer(e), String(...), div(x,y), mod(x,y), rem(x,y), ceil(x),
          floor(x), integer(x), delay(...), cardinality(), semiLinear(...),
          Subtask.decouple(v), initial(), terminal(), smooth(p, expr),
          sample(start, interval), pre(y), edge(b), reinit(x, expr),
          scalar(A), vector(A), matrix(A), diagonal(v), product(...),
          outerProduct(v1, v2), symmetric(A), skew(x).</para>
        </listitem>

        <listitem>
          <para>Overloaded operators (chapter 14)</para>
        </listitem>

        <listitem>
          <para>Stream connectors (chapter 15)</para>
        </listitem>

        <listitem>
          <para>Mapping of models to execution environments (chapter
          16)</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>In the Optimica front-end the following constructs are not
      supported:</para>

      <itemizedlist>
        <listitem>
          <para>Annotations for transcription information</para>
        </listitem>

        <listitem>
          <para>Minimum time problems</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The JModelica.org Model Interface (JMI) has the following
      Limitations:</para>

      <itemizedlist>
        <listitem>
          <para>The ODE interface requires the Modelica model to be written on
          explicit ODE form in order to work.</para>
        </listitem>

        <listitem>
          <para>Second order derivatives (Hessians) are not provided</para>
        </listitem>

        <listitem>
          <para>The interface does not yet comply with FMI
          specification</para>
        </listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</chapter>
