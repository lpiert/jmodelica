/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.io.PrintStream;
import java.util.ArrayList;


/**
 * A generator class for XML code generation which takes a model described by
 * <FClass> and provides an XML document containing the values of the
 * independent parameters in the model. Uses a template for the static general
 * structure of tags and an internal class <TagGenerator> for the parts of the
 * XML that are dynamic, that is, may vary depending on the contents of the
 * underlying model.
 * 
 * @see AbstractGenerator
 * @see TagGenerator
 * 
 */
public class XMLValueGenerator extends GenericGenerator{
	
	/**
	 * Internal class used to generate code for the independent parameter tags.
	 * 
	 * The independent parameters vary from model to model and therefore the 
	 * use of the template file is quite limited. All tags are thus generated 
	 * in this tag class. 
	 * 
	 * @see DAETag
	 */
	class DAETag_XML_parameters extends DAETag {
		
		/**
		 * Constructor.
		 * 
		 * @param myGenerator
		 *            The generator of the tags.
		 * @param fclass
		 *            The FClass object for which the code is generated.
		 */
		public DAETag_XML_parameters(
		  AbstractGenerator myGenerator, FClass fclass) {
			super("XML_parameters","Parameters (choice/optional).",
			  myGenerator,fclass);
		}
	
		/*
		 * (non-Javadoc)
		 * @see org.jmodelica.codegen.AbstractTag#generate(java.io.PrintStream)
		 */
		public void generate(PrintStream genPrinter) {
			ArrayList<FBooleanVariable> booleans = fclass.independentBooleanParameters();
			for(FBooleanVariable variable: booleans) {
				genPrinter.print("\n\t <BooleanParameter ");
				genPrinter.print("name=\""+variable.name()+"\" ");
				if(variable.hasBindingExp()) {
					genPrinter.print("value=\""+variable.getBindingExp().ceval().booleanValue()+"\"");
				}else{
					genPrinter.print("value=\""+variable.startAttribute()+"\"");
				}
				genPrinter.print("/>");
			}
			
			ArrayList<FIntegerVariable> integers = fclass.independentIntegerParameters();
			for(FIntegerVariable variable: integers) {
				genPrinter.print("\n\t <IntegerParameter ");
				genPrinter.print("name=\""+variable.name()+"\" ");
				if(variable.hasBindingExp()) {
					genPrinter.print("value=\""+variable.getBindingExp().ceval().intValue()+"\"");
				}else{
					genPrinter.print("value=\""+variable.startAttribute()+"\"");
				}
				genPrinter.print("/>");

			}
						
			ArrayList<FRealVariable> reals = fclass.independentRealParameters();
			for(FRealVariable variable: reals) {
				genPrinter.print("\n\t <RealParameter ");
				genPrinter.print("name=\""+variable.name()+"\" ");
				if(variable.hasBindingExp()) {
					genPrinter.print("value=\""+variable.getBindingExp().ceval().realValue()+"\"");
				}else{
					genPrinter.print("value=\""+variable.startAttribute()+"\"");
				}
				genPrinter.print("/>");
			}
			
			ArrayList<FStringVariable> strings = fclass.independentStringParameters();			
			for(FStringVariable variable: strings) {
				genPrinter.print("\n\t <StringParameter ");
				genPrinter.print("name=\""+variable.name()+"\" ");
				if(variable.hasBindingExp()) {
					genPrinter.print("value=\""+variable.getBindingExp().ceval().stringValue()+"\"");
				}else{
					genPrinter.print("value=\""+variable.startAttribute()+"\"");
				}
				genPrinter.print("/>");

			}
		}
	}
	
	/**
	 * \brief Returns the string denoting the beginning of the copyright blurb.
	 */
	protected String startOfBlurb() { return "<!--"; }
	
	/**
	 * \brief Returns the string denoting the end of the copyright blurb.
	 */
	protected String endOfBlurb() { return "-->"; }

	/**
	 * Constructor.
	 * 
	 * @param expPrinter Printer object used to generate code for expressions.
	 * @param escapeCharacter Escape characters used to decode tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public XMLValueGenerator(Printer expPrinter, char escapeCharacter, FClass fclass) {
		super(expPrinter, escapeCharacter, fclass);
	}

}
