/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import org.jmodelica.util.OptionRegistry;

//import beaver.Parser.Exception;

/**
 * 
 * Main compiler class which bundles the tasks needed to compile an Optimica
 * model. This class is an extension of ModelicaCompiler.
 * <p>
 * There are two usages with this class:
 * 	-# Compile in one step either from the command line or by calling the static 
 * method <compileModel> in your own class.
 *	-# Split compilation into several steps by calling the static methods in your
 *  own class.
 *  <p>
 * Use (1) for a simple and compact way of compiling an Optimica model. As a
 * minimum, provide the modelfile name and class name as command line arguments.
 * Optional arguments are XML templates and c template files which are needed
 * for code generation. If any of these are ommitted no code generation will be
 * performed.
 * <p>
 * Example without code generation: <br>
 * <code>org.jmodelica.applications.OptimicaCompiler myModels/models.mo models.model1</code>
 * <p>
 * Example with code generation: <br> 
 * <code>org.jmodelica.applications.OptimicaCompiler myModels/models.mo 
 * models.model1 XMLtemplate1.xml XMLtemplate2.xml XMLtemplate3.xml cppTemplate.cpp</code>
 * <p>
 * Logging can be set with the optional argument -log=i, w or e where:
 *	- -i : log info, warning and error messages 
 *	- -w : log warning and error messages
 *	- -e : log error messages only (default if the log option is not used)
 * <p>
 * Example with log level set to INFO: <br>
 * <code>org.jmodelica.applications.OptimicaCompiler -i myModels/models.mo
 * models.model1</code> <br>
 * The logs will be printed to standard out.
 * <p>
 * 
 * For method (2), the compilation steps are divided into 4 tasks which can be
 * used via the methods:
 *	-# parseModel (source code -> attributed source representation) - ModelicaCompiler
 *	-# instantiateModel (source representation -> instance model) - ModelicaCompiler
 *	-# flattenModel (instance model -> flattened model)
 *	-# generateCode (flattened model -> c code and XML code)
 * 
 * <p>
 * They must be called in this order. Use provided methods in ModelicaCompiler
 * to get/set logging level.
 * 
 */
public class OptimicaCompiler extends ModelicaCompiler{
	
	public OptimicaCompiler(OptionRegistry options, String xmlTpl, 
			String xmlValuesTpl, String cTemplatefile) {
		super(options, xmlTpl, xmlValuesTpl, cTemplatefile);
	}	
		
	/**
	 * Compiles an Optimica model. A model file name and class must be provided.
	 * Prints an error and returns without completion if, for example, a file 
	 * can not be found or if the parsing fails. Supports multiple model files.
	 * 
	 * @param name
	 *            Array of names of the model files.
	 * @param cl
	 *            The name of the class in the model file to compile.
	 *            
	 * @throws beaver.Parser.Exception If there was an Beaver parsing exception.
	 * @throws CompilerException
	 *             If errors have been found during the parsing, instantiation
	 *             or flattening.
	 * @throws FileNotFoundException
	 *             If the model file can not be found.
	 * @throws IOException
	 *             If there was an error reading the model file. (Beaver
	 *             exception.)
	 * @throws IOException
	 *             If there was an error creating the .mof file.
	 * @throws ModelicaClassNotFoundException
	 *             If the Modelica class to parse, instantiate or flatten is not
	 *             found.
	 * 
	 */
	public void compileModel(String name[], String cl) 
	  throws ModelicaClassNotFoundException, CompilerException, FileNotFoundException, IOException, beaver.Parser.Exception {
		log.info("======= Compiling model =======");
		
		// build source tree
		SourceRoot sr = parseModel(name);

		// compute instance tree
		InstClassDecl icd = instantiateModel(sr, cl);
			
		// flattening
		FOptClass fc = flattenModel(icd);

		// Generate code?
		if (super.getXMLTpl() != null && super.getXMLValuesTpl() != null && super.getCTemplate() != null) {
			generateCode(fc);
		}
		
		log.info("====== Model compiled successfully =======");
	}

	
	/**
	 * \brief Create a new FOptClass object.
	 */
	protected FOptClass createFClass() {
		return new FOptClass();
	}

	/**
	 * Computes the flattened model representation from a model instance node.
	 * 
	 * @param icd
	 *            A reference to the model instance.
	 * 
	 * @return FOptClass object representing the flattened Optimica model.
	 * 
	 * @throws CompilerException
	 *             If errors have been found during the flattening.
	 * @throws IOException
	 *             If there was an error creating the .mof file.
	 * @throws ModelicaClassNotFoundException
	 *             If the Modelica class to flatten is not found.
	 */
	public FOptClass flattenModel(InstClassDecl icd) 
			throws CompilerException, ModelicaClassNotFoundException, IOException {
		return (FOptClass) super.flattenModel(icd);
	}
	
	/**
	 * \brief Create a new OptimicaXMLGenerator object.
	 */
	protected OptimicaXMLGenerator createXMLGenerator(FClass fc) {
		return new OptimicaXMLGenerator(new PrettyPrinter(), '$', (FOptClass) fc);
	}
	
	/**
	 * \brief Create a new OptimicaCGenerator object.
	 */
	protected OptimicaCGenerator createCGenerator(FClass fc) {
		return new OptimicaCGenerator(new PrettyPrinter(), '$', (FOptClass) fc);
	}

	protected OptimicaCompiler(String[] args, Hashtable<String, String> programarguments) {
		super("OptimicaCompiler", args, programarguments);
	}
	
	public static void main(String args[]) {
		// Get any program options set
		Hashtable<String, String> programarguments = extractProgramArguments(args, 0);
		// Create compiler
		OptimicaCompiler oc = new OptimicaCompiler(args, programarguments);
		// Compile model
		oc.compileModelFromCommandLine(args, programarguments);
	}	
}
