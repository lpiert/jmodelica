/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.jmodelica.util;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.TreeMap;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.EnumSet;
import org.jmodelica.util.FilteredIterator;

aspect Graphs {


	syn boolean FAbstractEquation.lhsVarsAreSolved() = false;
	eq FAlgorithm.lhsVarsAreSolved()                 = true;

public class BiPGraph {

	protected Map<String,Var> variableMap = new LinkedHashMap<String,Var>();
	protected Map<String,Eq> equationMap = new LinkedHashMap<String,Eq>();
	protected Map<FAbstractEquation,java.util.List<Eq>> equationIndexMap = new LinkedHashMap<FAbstractEquation,java.util.List<Eq>>();
	private boolean initialSystem = false;
	
	public BiPGraph() {
	}
	
	public BiPGraph(Collection<Eq> eqs, Collection<Var> vars) {
		for (Eq oldE : eqs)
			addEquation(oldE.getName(), oldE.getEquation(), oldE.groupNumber(), oldE.variability());
		for (Var oldV : vars)
			addVariable(oldV.getName(), oldV.getVariable());
		for (Eq oldE : eqs) {
			Eq e = getEquation(oldE.getName());
			for (Var oldV : oldE.getVariables()) {
				Var v = getVariable(oldV.getName());
				if (v != null)
					addInsidence(e, v);
			}
		}
	}
	
	public BiPGraph(Collection<Eq> block) {
		this(block, collectMatchingVariables(block));
		for (Eq e : block) {
			match(getEquation(e.getName()), getVariable(e.getMatching().getName()));
		}
	}
	
	private static Collection<Var> collectMatchingVariables(Collection<Eq> eqns) {
		Collection<Var> vars = new ArrayList<Var>();
		for (Eq e : eqns)
			vars.add(e.getMatching());
		return vars;
	}
	
	public boolean isComplete() {
		return getUnmatchedVariables().size() == 0 && getUnmatchedEquations().size() == 0;
	}
	
	public Collection<Eq> getEquations() {
		return equationMap.values();
	}

	public Collection<Var> getVariables() {
		return variableMap.values();
	}

	public Eq getEquation(String name) {
		return equationMap.get(name);
	}
	
	public void isInitialSystem() {
	    initialSystem = true;
	}
	
	public java.util.List<Eq> getEquations(FAbstractEquation eqn) {
		java.util.List<Eq> list = equationIndexMap.get(eqn);
		return (list == null) ? Collections.<Eq>emptyList() : list;
	}

	public Eq addEquation(String name, FAbstractEquation eqn, int groupNumber, FTypePrefixVariability variability) {
		Eq e = equationMap.get(name);
		if (e==null) {
			e = new Eq(name, eqn, groupNumber, variability);
			equationMap.put(name,e);
			insertIntoEquationIndexMap(eqn,e);
		}
		return e;
	}
	
	public Var addVariable(String name, FVariable var) {
		Var v = variableMap.get(name);
		if (v==null) {
			v = new Var(name, var);
			variableMap.put(name,v);
		}	
		return v;
	}
	
	public void removeEquation(Eq e) {
		for (Var v : e.getVariables()) {
			if (v.getMatching() == e) {
				v.setMatching(null);
			}
		}
		e.setMatching(null);
		e.getVariables().clear();
		equationMap.remove(e.getName());
		removeFromEquationIndexMap(e.getEquation(),e);
	}
	
	public void removeVariable(Var v) {
		for (Eq e : getEquations()) {
			if (e.getMatching() == v) {
				e.setMatching(null);
			}
			e.getVariables().remove(v);
			e.getSolvableVariables().remove(v);
		}
		v.setMatching(null);
		variableMap.remove(v.getName());
	}
	
	public Var getVariable(String name) {
		return variableMap.get(name);
	}
	
	public boolean addEdge(String equationName, String variableName) {
		Eq e = getEquation(equationName);
		Var v = getVariable(variableName);
		if (v==null || e==null) {
			return false;
		}
		if (e.getVariables().contains(v)) {
			return false;
		}
		e.addVariable(v);
		return true;
	}

	public boolean addEdge(Eq e, Var v) {
		return addEdge(e.getName(),v.getName());
	}
	
	
	public enum VarType {
		DERIVATIVE_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.derivativeVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.derivativeVariables(); }
			public boolean isOk(FTypePrefixVariability variability)               { return variability.continuousVariability(); }
		},
		DIFFERENTIATED_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.differentiatedRealVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.differentiatedRealVariables(); }
			public boolean isOk(FTypePrefixVariability variability)               { return variability.continuousVariability(); }
		}, 
		ALGEBRAIC_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.algebraicVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.algebraicVariables(); }
		}, 
		CONTINUOUS_ALGEBRAIC_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.algebraicContinousRealVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.algebraicContinousRealVariables(); }
			public boolean isOk(FTypePrefixVariability variability)               { return variability.continuousVariability(); }
		}, 
		DISCRETE_ALGEBRAIC_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.discreteVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.discreteVariables(); }
			public boolean isOk(FTypePrefixVariability variability)               { return variability.discreteVariability(); }
		}, 
		DISCRETE_PRE_VARIABLES { 
			public Collection<? extends FVariable> variables(FClass c)            { return c.discretePreVariables(); }
			public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.discretePreVariables(); }
			public boolean isOk(FTypePrefixVariability variability)               { return variability.discreteVariability(); }
		},
		NON_FIXED_PARAMETERS {
		    public Collection<? extends FVariable> variables(FClass c)            { return c.nonFixedParameters(); }
		    public Collection<? extends FVariable> variables(FAbstractEquation e) { return e.nonFixedParameters(); }
		    public boolean isOk(FTypePrefixVariability variability)               { return variability.parameterVariability(); }
		};
		
		public abstract Collection<? extends FVariable> variables(FClass c);
		public abstract Collection<? extends FVariable> variables(FAbstractEquation e);
		public boolean isOk(FTypePrefixVariability e) { return true; }
	}
		
	public void addVariables(FClass fclass, EnumSet<VarType> variableTypes) {
		for (VarType t : variableTypes)
			for (FVariable fv : t.variables(fclass))
				addVariable(fv.name(), fv);
	}
		
	public void addEquations(java.util.List<FAbstractEquation> eqns, EnumSet<VarType> variableTypes, String eqNamePrefix, int startIndex) {
		int k = startIndex;
		Eq eqn = null;
		Var v = null;
		Map<FVariable, Integer> emptyMap = Collections.emptyMap();
		Set<FVariable> emptySet = Collections.emptySet();
		for (FAbstractEquation e : eqns) {
			int n_eq = e.numScalarEquations();
			LinkedHashSet<Eq> groupMembers = new LinkedHashSet<Eq>();
			// For equations that counts as several scalar equations (function calls, algorithms), 
			// the left hand side variables depends on all the right-hand side ones, 
			// but not on the other left-hand side ones
			Set<FVariable> rhsVars = emptySet;
			FVariable lhsVars[] = new FVariable[n_eq];
			if (n_eq > 1) {
				rhsVars = e.referencedFVariablesInRHS();
				int i = 0;
				for (FVariable fv : e.referencedFVariablesInLHS())
				    lhsVars[i++] = fv;
				if (e.lhsVarsAreSolved())
				    rhsVars.removeAll(Arrays.asList(lhsVars));
			}
			for (int i = n_eq - 1; 0 <= i; i--) { // During tarjan the order will be reversed,
				                                  // need to reverse here to preserve the initial order.
                FTypePrefixVariability variability = lhsVars[i] != null ? lhsVars[i].variability().combineDown(lhsVars[i].type().funcOutputVariability()) : e.variability();
			    boolean add = false;
			    for (VarType varType : variableTypes)
			        if (varType.isOk(variability))
			            add = true;
			    if (!add)
			        continue;
			    eqn = addEquation(eqNamePrefix+k, e, i, variability);
			    groupMembers.add(eqn);
			    k++;

			    for (FVariable fVar : ASTNode.lookupFVariablesInSet(e.findFIdUseExpsInTree())) {
			        Var var = getVariable(fVar.name());
			        if (var == null)
			            continue;
			        if (lhsVars[i] == null || lhsVars[i] == fVar || rhsVars.contains(fVar))
			            addEdge(eqn, var);
			    }
			}
			for (Eq ee : groupMembers) {
				for (Eq eee : groupMembers) {
					ee.addGroupMember(eee);
				}
			}
		}
	}
	
	public boolean match(Eq e, Var v) {
		if (!canMatch(e, v))
			return false;
		if (e.getMatching() != null)
			e.getMatching().setMatching(null);
		if (v.getMatching() != null)
			v.getMatching().setMatching(null);
		v.setMatching(e);
		e.setMatching(v);
		return true;
	}
	
	public boolean canMatch(Eq e, Var v) {
	    if (initialSystem)
	        return true;
	    
		FAbstractEquation equation = e.getEquation();
		FVariable variable = v.getVariable();
		
		if (equation instanceof FAlgorithm)
			return true;
		
		// Discrete variables can only be matched to discrete equations.
		// Except discrete reals in initial system.
		return variable.isContinuous() || e.variability().discreteVariability() || equation.isInitial();
	}
	
	public void addInsidence(Eq e, Var v) {
		e.addVariable(v);
		v.occurrence();
	}
	
	public Collection<Eq> greedyMatching() {
		Collection<Eq> unmatched = new ArrayList<Eq>();
		for (Eq e : getEquations()) {
			if (e.getMatching() != null)
				continue;
			for (Var v : e.getVariables()) {
				if (v.getMatching()==null) {
					match(e, v);
					break;
				}
			}
			if (e.getMatching() == null)
				unmatched.add(e);
		}
		return unmatched;
	}
	
	private static class VarMatchingComparator implements Comparator<Var> {
		@Override
		public int compare(Var o1, Var o2) {
			int diff = (o1.getVariable().startAttributeSet() ? 1 : 0) - (o2.getVariable().startAttributeSet() ? 1 : 0);
			if (diff != 0)
				return diff;
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	public java.util.List<Map<Var,Set<Eq>>> bfs(Collection<Eq> startingNodes) {
		java.util.List<Map<Var,Set<Eq>>> Lv = new ArrayList<Map<Var,Set<Eq>>>();
		Set<Eq> Le_current = new LinkedHashSet<Eq>();
		Set<Eq> Le_next = new LinkedHashSet<Eq>();
		
		Le_current.addAll(startingNodes);
		// Reset nodes
		lightReset();
		
		int layer = 0;
		boolean freeVarNodeFound = false;
		//System.out.println("************** BFS ************* starting nodes: " + startingNodes);
		
		while (Le_current.size()>0 && !freeVarNodeFound) {
			//System.out.println("*** layer: " + layer);
			//System.out.println(Lv);
			//System.out.println(Le_current);
			Lv.add(new TreeMap<Var,Set<Eq>>(new VarMatchingComparator()));
			
			for (Eq s : Le_current) {
				//System.out.println(" eq: " + s);
				for (Var t : s.getVariables()) {
				    if (!canMatch(s, t))
				        continue;
					//System.out.println("  " + t + " layer: " + t.getLayer());
					if (t.getLayer() >= layer) {
						//System.out.println("    adding " + t);
						t.setLayer(layer);
						Set<Eq> h = Lv.get(layer).get(t);
						if (h==null) {
							h = new LinkedHashSet<Eq>();
							Lv.get(layer).put(t,h);
						}
						h.add(s);
						Eq u = t.getMatching();
						if (u!=null) {
							//System.out.println("     " + t + "'s matching is " + u);
							u.setLayer(layer);
							Le_next.add(u);
						} else {
							//System.out.println("     " + t + "has no matching");
							freeVarNodeFound = true;
					
						}
					}
				}
			}
			layer++;
			Le_current = Le_next;
			Le_next = new LinkedHashSet<Eq>();
		}
		
		java.util.List<Var> delQueue = new ArrayList<Var>();
		for (Var v : Lv.get(Lv.size()-1).keySet()) {
			if (v.getMatching()!=null) {
				delQueue.add(v);
			}
		}
		for (Var v : delQueue) {
			Lv.get(Lv.size()-1).remove(v);
		}
		//System.out.println(Lv);
		//System.out.println("************** BFS ends *************");
		return Lv;
	}

	public java.util.List<java.util.List<Edge>> dfs(java.util.List<Map<Var,Set<Eq>>> Lv) {
		lightReset();
		java.util.List<java.util.List<Edge>> P = new ArrayList<java.util.List<Edge>>();
	
		boolean found_path = true;
		for (Var v : Lv.get(Lv.size()-1).keySet()) {
			ArrayList<Edge> P_tmp = new ArrayList<Edge>();
			
			ListIterator<Map<Var,Set<Eq>>> iter = Lv.listIterator(Lv.size());
			while (iter.hasPrevious()) {
				Map<Var,Set<Eq>> l = iter.previous();
				v.setVisited(true);
				if (!found_path) {
					break;
				}
				found_path = false;
				for (Eq e : l.get(v)) {
					if (!e.isVisited() && canMatch(e, v)) {
						e.setVisited(true);
						P_tmp.add(new Edge(e,v));
						v = e.getMatching();
						found_path = true;
						break;
					}
				}
			}
			if (P_tmp.size() == Lv.size()) {
				P.add(P_tmp);
			}
		}
		//System.out.println(P);
		return P;
	}
		
	public void reassign(java.util.List<java.util.List<Edge>> P) {
		for (java.util.List<Edge> l : P) {
			for (Edge ed : l) {
				match(ed.getEquation(), ed.getVariable());
			}
		}
	}
	
	public void maximumMatching(boolean resetMatching) {
		if (resetMatching) {
			reset();
			greedyMatching();
		}
		//System.out.println(printMatching());
		
		// Initialize set of free equations
		Set<Eq> startingNodes = new LinkedHashSet<Eq>();
		for (Eq e : getEquations()) {
			if (e.getMatching()==null) {
				startingNodes.add(e);
			}
		}
	
		Set<Eq> unmatchedEquations = new LinkedHashSet<Eq>(getUnmatchedEquations());
		
		java.util.List<Map<Var,Set<Eq>>> Lv = null;
		java.util.List<java.util.List<Edge>> P = null;
		
		while (unmatchedEquations.size()>0) {
		
			Lv = bfs(unmatchedEquations);
			P = dfs(Lv);

			if (Lv.get(Lv.size()-1).size()==0) {
				break;
			}
			
			reassign(P);
		
			for (java.util.List<Edge> l : P) {
				unmatchedEquations.remove(l.get(l.size()-1).getEquation());
			}
		}
	}
	
	public boolean augmentPath(Eq e) {
		e.setVisited(true);
		for (Var v : e.getVariables()) {
			if (!canMatch(e, v))
				continue;
			if (v.getMatching()==null) {
				match(e, v);
				return true;
			} else if (!v.isVisited()) {
				v.setVisited(true);
				if (augmentPath(v.getMatching())) {
					match(e, v);
					return true;
				}
			}
		}
		return false;
	}
	
	protected class UnmatchedEquationsCriteria implements Criteria<Eq> {
		@Override
		public boolean test(Eq elem) {
			return elem.getMatching() == null;
		}
	}
	
	public Iterator<Eq> unmatchedEquationsIterator() {
		return new FilteredIterator<Eq>(getEquations().iterator(), new UnmatchedEquationsCriteria());
	}
	
	public Iterable<Eq> unmatchedEquationsIterable() {
		return new Iterable<Eq>() {
			@Override
			public Iterator<Eq> iterator() {
				return unmatchedEquationsIterator();
			}
		};
	}
	
	public Collection<Eq> getUnmatchedEquations() {
		java.util.List<Eq> l = new ArrayList<Eq>();
		for (Eq e : unmatchedEquationsIterable())
			l.add(e);
		return l;
	}

	protected class UnmatchedVariablesCriteria implements Criteria<Var> {
		@Override
		public boolean test(Var elem) {
			return elem.getMatching() == null;
		}
	}
	
	public Iterator<Var> unmatchedVariablesIterator() {
		return new FilteredIterator<Var>(getVariables().iterator(), new UnmatchedVariablesCriteria());
	}
	
	public Iterable<Var> unmatchedVariablesIterable() {
		return new Iterable<Var>() {
			@Override
			public Iterator<Var> iterator() {
				return unmatchedVariablesIterator();
			}
		};
	}
	
	public Collection<Var> getUnmatchedVariables() {
		java.util.List<Var> l = new ArrayList<Var>();
		for (Var v : unmatchedVariablesIterable())
			l.add(v);
		return l;
	}
	
	public java.util.List<Eq> getMatchedEquations() {
		java.util.List<Eq> l = new ArrayList<Eq>();
		for (Eq e : getEquations()) {
			if (e.getMatching()!=null) {
				l.add(e);
			}
		}
		return l;
	}

	public java.util.List<Var> getMatchedVariables() {
		java.util.List<Var> l = new ArrayList<Var>();
		for (Var v : getVariables()) {
			if (v.getMatching()!=null) {
				l.add(v);
			}
		}
		return l;
	}
	
	public Collection<Eq> getVisitedEquations() {
		Collection<Eq> l = new ArrayList<Eq>();
		for (Eq e: getEquations()) {
			if (e.isVisited())
				l.add(e);
		}
		return l;
	}
	
	public Collection<Var> getVisitedVariables() {
		Collection<Var> l = new ArrayList<Var>();
		for (Var v: getVariables()) {
			if (v.isVisited())
				l.add(v);
		}
		return l;
	}

	public Collection<Collection<Eq>> tarjan(boolean merge_blt_blocks) {
		Enumerator indexer = new Enumerator();
		Stack<Eq> stack = new Stack<Eq>();
		Collection<Collection<Eq>> components = new ArrayList<Collection<Eq>>();
		tarjanReset();
		
		Set<Eq> keepTogether = new LinkedHashSet<Eq>();
		if (merge_blt_blocks) {
			for (Eq e : getEquations()) {
				if (e.markedAsResidualEquation() || e.getMatching().markedAsIterationVariable()) {
					keepTogether.add(e);
				}
				if (e.getEquation().getHGTIterationVariable() != null) {
					FVariable fVar = e.getEquation().getHGTIterationVariable();
					Var var = getVariable(fVar.name());
					if (var != null) {
						keepTogether.add(var.getMatching());
					}
				}
			}
		}
		
		for (Eq e : getEquations()) {
			if (!e.isVisited() && !e.isRes())
				tarjan(indexer, stack, components, keepTogether, e);
		}
		
		return components;
	}
	
	public void tarjan(Enumerator indexer, Stack<Eq> stack, Collection<Collection<Eq>> components, Set<Eq> keepTogether,  Eq e) {
		stack.add(e);
		int index = indexer.next();
		e.setTarjanNbr(index);
		e.setTarjanLowLink(index);
		e.setVisited(true);
		Collection<Eq> eqToVisit = new ArrayList<Eq>();
		if (keepTogether.contains(e)) {
			for (Eq ee : keepTogether) {
				if (ee != e && !ee.isRes())
					eqToVisit.add(ee);
			}
		}
		for (Var v : e.getVariables()) {
			Eq ee = v.getMatching();
			if (!v.isIter() && ee != null && e != ee)
				eqToVisit.add(ee);
 		}
		for (Eq ee : e.getGroupMembers()) {
			if (!ee.isRes() && ee != null && e != ee)
				eqToVisit.add(ee);
		}
		for (Eq ee : eqToVisit) {
			if (!ee.isVisited()) {
				tarjan(indexer, stack, components, keepTogether, ee);
				e.setTarjanLowLink(Math.min(e.getTarjanLowLink(), ee.getTarjanLowLink()));
			} else if (stack.contains(ee)) { //TODO: should not be stack.contains, it's slow...
				e.setTarjanLowLink(Math.min(e.getTarjanLowLink(), ee.getTarjanNbr()));
			}
		}
		if (e.getTarjanNbr()==e.getTarjanLowLink()) {
			Collection<Eq> component = new ArrayList<Eq>();
			Eq ee;
			do {
				ee = stack.pop();
				component.add(ee);
			} while (ee != e);
			components.add(component);
		}
	}
	
	public void reset() {
		for (Eq e : getEquations()) {
			e.reset();
		}		
		for (Var v : getVariables()) {
			v.reset();
		}		
	}

	public void lightReset() {
		for (Eq e : getEquations()) {
			e.lightReset();
		}		
		for (Var v : getVariables()) {
			v.lightReset();
		}		
	}

	public void tarjanReset() {
		for (Eq e : getEquations()) {
			e.tarjanReset();
		}		
	}
	
	public void insertIntoEquationIndexMap(FAbstractEquation eqn, Eq e) {
		java.util.List<Eq> l = equationIndexMap.get(eqn);
		if (l==null) {
			l = new ArrayList<Eq>();
			equationIndexMap.put(eqn, l);
		}
		l.add(e);
	}

	public void removeFromEquationIndexMap(FAbstractEquation eqn, Eq e) {
		java.util.List<Eq> l = equationIndexMap.get(eqn);
		if (l!=null) {
			for (Eq ee : l) {
				if (e==ee) {
        			l.remove(e);
        			return;
        		}
        	}
		}
	}
	
	public String printMatching() {
		StringBuffer str = new StringBuffer();
		str.append("----------------------------------------\n");
		str.append("BiPGraph matching:\n");
		for (Eq e : getEquations()) {
			if (e.getMatching()!=null) {
				str.append(e);
				str.append(" : ");
				str.append(e.getMatching());
				str.append("\n");
			}
		}		
		str.append("Unmatched equations: {");
		for (Eq e : unmatchedEquationsIterable()) {
			str.append(e + " ");
		}
		str.append("}\n");

		str.append("Unmatched variables: {");
		for (Var v : unmatchedVariablesIterable()) {
			str.append(v + " ");
		}
		str.append("}\n");

		str.append("----------------------------------------\n");
		return str.toString();
	}
	
	public Object printMatchingObj() {
		return new Object() {
			@Override
			public String toString() {
				return printMatching();
			}
		};
	}
	
	public String toString() {
		StringBuffer str = new StringBuffer();
		str.append("BiPGraph (" + getEquations().size() + " equations, " + variableMap.size() + " variables)\n");
		str.append("Variables: {");
		for (String vName : variableMap.keySet()) {
			Var v = variableMap.get(vName);
			str.append(v);
			str.append(" ");
		}
		str.append("}\n");
		for (Eq e : getEquations()) {
			str.append(e);
			str.append(" : ");
			for (Var v : e.getVariables()) {
				str.append(v + (canMatch(e, v) ? "¤ " : "# "));
			}
			str.append("// " + e.getEquation() + "\n");
		}
		return str.toString();
	}
	
    public class Edge {
    	private Var variable;
    	private Eq equation;
    	
    	public Edge(Eq e, Var v) {
    		this.equation = e;
    		this.variable = v;
    	}

		public Var getVariable() {
			return variable;
		}

		public void setVariable(Var variable) {
			this.variable = variable;
		}

		public Eq getEquation() {
			return equation;
		}

		public void setEquation(Eq equation) {
			this.equation = equation;
		}
    	
		public String toString() {
			return "(" + equation + "," + variable + ")";
		}
    	
    }
}

public class SolvingBiPGraph extends BiPGraph {
	
	public SolvingBiPGraph(Collection<Eq> eqns, Collection<Var> vars) {
		super(eqns, vars);
	}
	
	public SolvingBiPGraph(Collection<Eq> block) {
		super(block);
	}
	
	@Override
	public boolean canMatch(Eq e, Var v) {
		return super.canMatch(e, v) && isSolved(e, v).isSolvable();
	}
	
	public Solvability isSolved(Eq e, Var v) {
		return e.getEquation().isSolved(v.getName(), true);
	}
	
	@Override
	public void addInsidence(Eq e, Var v) {
		super.addInsidence(e, v);
		if (e.getEquation().isReal() != v.getVariable().isReal())
		    return;
		e.sameTypeOccurrence();
		v.sameTypeOccurrence();
		Solvability solvability = isSolved(e, v);
		switch (solvability) {
		case ANALYTICALLY_SOLVABLE:
			e.addAnalyticallySolvableVariable(v);
			v.analyticallySolvableOccurrence();
			break;
		case NUMERICALLY_SOLVABLE:
			e.addNumericallySolvableVariable(v);
			v.numericallySolvableOccurrence();
			break;
		}
	}
	
}

public class Eq implements Comparable<Eq> {

	private final String name;
	private final FAbstractEquation eqn;
	private final int groupNumber;
	private final FTypePrefixVariability variability;
	private java.util.List<Var> variables = new ArrayList<Var>();
	private java.util.List<Var> solvableVariables = new ArrayList<Var>();
	private int analyticallySolvableOccurrences = 0;
	private int numericallySolvableOccurrences = 0;
	private int sameTypeOccurrences = 0;
	private Var matching = null;
	private boolean visited = false;
	private int layer = 1000000;
	private Eq meDifferentiated = null;
	private Eq meIntegrated = null;
	private boolean isRes = false;
	private int tarjanNbr = 0;
	private int tarjanLowLink = 0;
	private int depth = 1;
	/* In some cases, equations needs to be treated as a group,
	   e.g., in the case when several equations are generated from
	   a function call equation. In this case, all the "scalar"
	   equations generated for the function call equation are
	   members of each such equation. Note that every equation
	   is member of its own group: scalar equations therefor has
	   one member: itself. This approach makes handling of equation
	   groups more consistent, e.g., when a function call equation
	   needs to be differentiated. 
	*/
	private Set<Eq> groupMembers = new LinkedHashSet<Eq>();

	public Eq(String name, FAbstractEquation eqn, int groupNumber, FTypePrefixVariability variability) {
		this.name = name;
		this.eqn = eqn;
		this.groupNumber = groupNumber;
		this.variability = variability;
		this.groupMembers.add(this);
	}
			
	public void addVariable(Var v) {
		variables.add(v);
	}
	

	public void reset() {
		setMatching(null);
		setVisited(false);
		setLayer(1000000);
	}

	public void lightReset() {
		setVisited(false);
		setLayer(1000000);
	}
	
	public void tarjanReset() {
		setTarjanLowLink(0);
		setTarjanNbr(0);
		setVisited(false);
	}
	
	public String getName() {
		return name;
	}

	public FAbstractEquation getEquation() {
		return eqn;
	}
		
	public int groupNumber() {
		return groupNumber;
	}
	
	public java.util.List<Var> getVariables() {
		return variables;
	}

	public Var getMatching() {
		return matching;
	}

	public void setMatching(Var matching) {
		this.matching = matching;
	}
	
	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public int getTarjanNbr() {
		return tarjanNbr;
	}

	public void setTarjanNbr(int tarjanNbr) {
		this.tarjanNbr = tarjanNbr;
	}

	public int getTarjanLowLink() {
		return tarjanLowLink;
	}

	public void setTarjanLowLink(int tarjanLowLink) {
		this.tarjanLowLink = tarjanLowLink;
	}

	public int numDifferentiations() {
		return getMeIntegrated()==null? 0: getMeIntegrated().numDifferentiations() + 1;	
	}

	public void setMeDifferentiated(Eq e) {
		this.meDifferentiated = e;
		this.getEquation().setMeDifferentiated(e.getEquation());
	}
	
	public Eq getMeDifferentiated() {
		return this.meDifferentiated;
	}

	public void setMeIntegrated(Eq e) {
		this.meIntegrated = e;
		this.getEquation().setMeIntegrated(e.getEquation());
	}
	
	public Eq getMeIntegrated() {
		return this.meIntegrated;
	}
	
	public String toString() {
		return getName();
	}

	public void sameTypeOccurrence(){
	    sameTypeOccurrences++;
    }
    
    public int getNbrSameTypeOccurrences(){
        return sameTypeOccurrences;
    }
    

	public void addAnalyticallySolvableVariable(Var v) {
		solvableVariables.add(v);
		analyticallySolvableOccurrences++;
	}
	
	public void addNumericallySolvableVariable(Var v) {
		solvableVariables.add(v);
		numericallySolvableOccurrences++;
	}
	
	public java.util.List<Var> getSolvableVariables() {
		return solvableVariables;
	}
	
	public boolean isReal() {
	    return getEquation().isReal();
	}
	
	public void isRes(boolean bol){
		this.isRes=bol;
	}
	
	public boolean isRes(){
		return this.isRes;
	}
	
	public int getDepth(){
		return this.depth;
	}
	
	public void setDepth(int d){
		this.depth = d;
	}
	
	public void addGroupMember(Eq eqn) {
		groupMembers.add(eqn);
	}
	
	public Set<Eq> getGroupMembers() {
		return groupMembers;	
	}
	
	public Var getVariable(String name) {
		for (Var var : variables) {
			if (var.getName().equals(name)) {
				return var;
			}
		}
		return null;
	}
	
	public FTypePrefixVariability variability() {
	    return variability;
	}
	
	/**
	 * Returns integer greather than zero if this variable sutes better as iteration
	 * variable than the <code>other</code> variable. Zero is returned if they are equal
	 * and <0 is returned if the <code>other</code> variable is better.
	 */
	@Override
	public int compareTo(Eq other) {
	    int diff = (isReal() ? 1 : 0) - (other.isReal() ? 1 : 0);
	    if (diff != 0)
	        return diff;
		diff = (markedAsResidualEquation() ? 1 : 0) - (other.markedAsResidualEquation() ? 1 : 0);
		if (diff != 0)
			return diff;
		diff = getNbrSameTypeOccurrences() - other.getNbrSameTypeOccurrences();
		if (diff != 0)
			return diff;
		diff = numericallySolvableOccurrences - other.numericallySolvableOccurrences;
		if (diff != 0)
			return diff;
		diff = other.analyticallySolvableOccurrences - analyticallySolvableOccurrences;
		if (diff != 0)
			return diff;
		diff = (getEquation().hasNominal() ? 1 : 0) - (other.getEquation().hasNominal() ? 1 : 0);
		if (diff != 0)
			return diff;
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Eq))
			return false;
		Eq other = (Eq)o;
		return getName().equals(other.getName());
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	};


}

public class Var implements Comparable<Var> {

	private final String name;
	private final FVariable v;
	private Eq matching = null;
	private boolean visited = false;
	private int layer = 1000000;
	private Var meDifferentiated = null; // A reference to this variable differentiated once
	private Var meIntegrated = null;
	private int occurrences = 0;
	private int sameTypeOccurrences = 0;
	private int analyticallySolvableOccurrences = 0;
	private int numericallySolvableOccurrences = 0;
	private boolean isIter=false;
	
	public Var(String name, FVariable v) {
		this.name = name;
		this.v = v;
	}
		
	public void reset() {
		setMatching(null);
		setVisited(false);
		setLayer(1000000);
	}

	public void lightReset() {
		setVisited(false);
		setLayer(1000000);
	}
	
	public String getName() {
		return name;
	}
	
	public FVariable getVariable() {
		return v;
	}

	public Eq getMatching() {
		return matching;
	}

	public void setMatching(Eq matching) {
		this.matching = matching;
	}

	public boolean isVisited() {
		return visited;
	}

	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}
		
	public void setMeDifferentiated(Var var) {
		this.meDifferentiated = var;
		this.getVariable().setMeDifferentiated(var.getVariable());
	}
	
	public Var getMeDifferentiated() {
		return this.meDifferentiated;
	}

	public void setMeIntegrated(Var var) {
		this.meIntegrated = var;
		this.getVariable().setMeIntegrated(var.getVariable());
	}
	
	public Var getMeIntegrated() {
		return this.meIntegrated;
	}
	
	public void occurrence(){
		occurrences++;
	}
	
    public void sameTypeOccurrence(){
        sameTypeOccurrences++;
    }

	public void analyticallySolvableOccurrence(){
		analyticallySolvableOccurrences++;
	}
	
	public void numericallySolvableOccurrence(){
		numericallySolvableOccurrences++;
	}
	
	public int getNbrOccurrences(){
		return occurrences;
	}
	
	public int getNbrSameTypeOccurrences(){
	    return sameTypeOccurrences;
	}
	
	public int getNbrAnalyticallySolvableOccurrences(){
		return analyticallySolvableOccurrences;
	}
	
	public int getNbrNumericallySolvableOccurrences(){
		return numericallySolvableOccurrences;
	}
	
	public String toString() {
		return v.displayName();
	}
	
	public boolean isReal() {
	    return getVariable().isReal();
	}
	
	public void isIter(boolean bol){
		this.isIter=bol;
	}
	
	public boolean isIter(){
		return this.isIter;
	}
	
	public boolean markedAsIterationVariable() {
		return getVariable().isHGTVarComponent();
	}
	
	/**
	 * Returns integer greather than zero if this variable sutes better as iteration
	 * variable than the <code>other</code> variable. Zero is returned if they are equal
	 * and <0 is returned if the <code>other</code> variable is better.
	 */
	@Override
	public int compareTo(Var other) {
        int diff = (isReal() ? 1 : 0) - (other.isReal() ? 1 : 0);
        if (diff != 0)
            return diff;
		diff = (other.getVariable().isTemporary() ? 1 : 0) - (getVariable().isTemporary() ? 1 : 0);
		if (diff != 0)
			return diff;
		diff = (markedAsIterationVariable() ? 1 : 0) - (other.markedAsIterationVariable() ? 1 : 0);
		if (diff != 0)
			return diff;
		diff = getNbrSameTypeOccurrences() - other.getNbrSameTypeOccurrences();
		if (diff != 0)
			return diff;
		diff = getNbrNumericallySolvableOccurrences() - other.getNbrNumericallySolvableOccurrences();
		if (diff != 0)
			return diff;
		diff = other.getNbrAnalyticallySolvableOccurrences() - getNbrAnalyticallySolvableOccurrences();
		if (diff != 0)
			return diff;
		diff = (getVariable().attributeSet(FAttribute.START) ? 1 : 0) - (other.getVariable().attributeSet(FAttribute.START) ? 1 : 0);
		if (diff != 0)
			return diff;
		diff = other.getVariable().getFQName().numDots() - getVariable().getFQName().numDots();
		if (diff != 0)
			return diff;
		return 0;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Var))
			return false;
		Var other = (Var)o;
		return getName().equals(other.getName());
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	};
}

}