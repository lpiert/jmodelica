<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Limitations</title>

  <para>This page lists the current limitations of the JModelica.org platform.
  The development of the platform can be followed at the <link
  xlink:href="http://trac.jmodelica.org">Trac</link> site, where future
  releases and associated features are planned. The <link
  xlink:href="http://www.jmodelica.org/binary">JModelica.org platform download
  page</link> has links to compliance reports detailing the current MSL
  compliance.</para>

  <itemizedlist>
    <listitem>
      <para>The Modelica compliance of the front-end is limited; the following
      features are currently not supported:</para>

      <itemizedlist>
        <listitem>
          <para>Discrete variables are only supported in FMUs.</para>
        </listitem>

        <listitem>
          <para>The support for String variables and parameters is
          limited.</para>
        </listitem>

        <listitem>
          <para>Arrays indexed with enumerations or Booleans are not
          supported.</para>
        </listitem>

        <listitem>
          <para>Functions with array inputs with sizes declared as ':' are
          only partially supported (there are some operations on such arrays
          that are not supported).</para>
        </listitem>

        <listitem>
          <para>Partial support for external functions; records are not
          supported as arguments or return values.</para>
        </listitem>

        <listitem>
          <para>The following built-in functions are not
          supported:<informaltable border="1" frame="void" width="25%">
              <tr>
                <td>delay(...)</td>
              </tr>

              <tr>
                <td>spatialDistribution(..)</td>
              </tr>

              <tr>
                <td>terminal()</td>
              </tr>
            </informaltable></para>
        </listitem>

        <listitem>
          <para>The following built-in functions are only supported in
          FMUs:<informaltable border="1" frame="void" width="75%">
              <tr>
                <td>ceil(x)</td>

                <td>integer(x)</td>

                <td>reinit(x, expr)</td>
              </tr>

              <tr>
                <td>div(x,y)</td>

                <td>mod(x,y)</td>

                <td>sample(start,interval)</td>
              </tr>

              <tr>
                <td>edge(b)</td>

                <td>pre(y)</td>

                <td>semiLinear(...)</td>
              </tr>

              <tr>
                <td>floor(x)</td>

                <td>rem(x,y)</td>

                <td>sign(v)</td>
              </tr>

              <tr>
                <td>initial()</td>
              </tr>
            </informaltable></para>
        </listitem>

        <listitem>
          <para>Overloaded operators and the Complex type are not supported
          (Modelica Language Specification, chapter 14).</para>
        </listitem>

        <listitem>
          <para>Stream connections with more than two connectors are not
          supported.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>In the Optimica front-end the following constructs are not
      supported:</para>

      <itemizedlist>
        <listitem>
          <para>Annotations for transcription information.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The following limitations apply to JMUs compiled with
      JModelica.org:</para>

      <itemizedlist>
        <listitem>
          <para>The ODE interface requires the Modelica model to be written on
          explicit ODE form in order to work.</para>
        </listitem>

        <listitem>
          <para>Second order derivatives (Hessians) are not provided.</para>
        </listitem>

        <listitem>
          <para>The interface for interacting with JMUs does not comply with
          FMI specification.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The following limitations apply to FMUs compiled with
      JModelica.org:</para>

      <itemizedlist>
        <listitem>
          <para>The FMI interface only supports FMUs distributed with
          binaries, not source code.</para>
        </listitem>

        <listitem>
          <para>Functions for setting and getting string variables do not
          work.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The following limitations apply to optimization using CasADi-based
      collocation with JModelica.org:<itemizedlist>
          <listitem>
            <para>Incomplete support for the <literal>Integer</literal> and
            <literal>Boolean</literal> types: To the extent that they are
            supported, they be treated more or less like reals.</para>
          </listitem>

          <listitem>
            <para>No support for <literal>String</literal> and
            <literal>enumeration</literal> types.</para>
          </listitem>

          <listitem>
            <para>Attributes with any name can be set on any type of
            variable.</para>
          </listitem>
        </itemizedlist></para>
    </listitem>
  </itemizedlist>
</chapter>
