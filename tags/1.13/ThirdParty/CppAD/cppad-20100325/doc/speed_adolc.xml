<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Speed Test Derivatives Using Adolc</title>
<meta name="description" id="description" content="Speed Test Derivatives Using Adolc"/>
<meta name="keywords" id="keywords" content=" speed test adolc "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_speed_adolc_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="double_sparse_jacobian.cpp.xml" target="_top">Prev</a>
</td><td><a href="adolc_det_minor.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_adolc</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>speed_adolc-&gt;</option>
<option>adolc_det_minor.cpp</option>
<option>adolc_det_lu.cpp</option>
<option>adolc_ode.cpp</option>
<option>adolc_poly.cpp</option>
<option>adolc_sparse_hessian.cpp</option>
<option>adolc_sparse_jacobian.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Purpose</option>
<option>AdolcDir</option>
<option>C++ Compiler Flags</option>
<option>Contents</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>Speed Test Derivatives Using Adolc</big></big></b></center>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
CppAD has a set of speed tests that are used to compare
Adolc with other AD packages.
This section links to the source code the Adolc speed tests
(any suggestions to make the Adolc results faster are welcome).

<br/>
<br/>
<b><big><a name="AdolcDir" id="AdolcDir">AdolcDir</a></big></b>
<br/>
To run these tests, you must include the <code><font color="blue">configure</font></code> command line option
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADOLC_DIR=</span></font></code><i><span style='white-space: nowrap'>AdolcDir</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>during <a href="installunix.xml#AdolcDir" target="_top"><span style='white-space: nowrap'>installation</span></a>
.
After the <a href="installunix.xml#make test" target="_top"><span style='white-space: nowrap'>make&#xA0;test</span></a>
 command
had been run in the <code><font color="blue">speed/adolc</font></code> directory, 
you can then run the Adolc speed tests 
with the following commands (relative to the distribution directory):
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;speed/adolc/adolc&#xA0;correct&#xA0;</span></font></code><i><span style='white-space: nowrap'>seed</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>seed</i> is a positive integer see for the
random number generator <a href="uniform_01.xml" target="_top"><span style='white-space: nowrap'>uniform_01</span></a>
.
This will check that the speed tests have been built correctly.
You can run the command
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;speed/adolc/adolc&#xA0;speed&#xA0;</span></font></code><i><span style='white-space: nowrap'>seed</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>to see the results of all the speed tests.
See <a href="speed_main.xml" target="_top"><span style='white-space: nowrap'>speed_main</span></a>
 for more options.

<br/>
<br/>
<b><big><a name="C++ Compiler Flags" id="C++ Compiler Flags">C++ Compiler Flags</a></big></b>
<br/>
The C++ compiler flags used to build the Adolc speed tests are
<code><font color="blue">
<pre style='display:inline'> 
     AM_CXXFLAGS   = -O2 -DNDEBUG -DSPEED_ADOLC $(CXX_FLAGS)</pre>
</font></code>
where <code><font color="blue">CXX_FLAGS</font></code> is specified by the 
<a href="installunix.xml#Configure" target="_top"><span style='white-space: nowrap'>configure</span></a>
 command.

<br/>
<br/>
<b><big><a name="Contents" id="Contents">Contents</a></big></b>
<br/>
<div><a href="adolc_det_minor.cpp.xml" target="_top">Adolc&#xA0;Speed:&#xA0;Gradient&#xA0;of&#xA0;Determinant&#xA0;by&#xA0;Minor&#xA0;Expansion</a><br/>
<a href="adolc_det_lu.cpp.xml" target="_top">Adolc&#xA0;Speed:&#xA0;Gradient&#xA0;of&#xA0;Determinant&#xA0;Using&#xA0;Lu&#xA0;Factorization</a><br/>
<a href="adolc_ode.cpp.xml" target="_top">Adolc&#xA0;Speed:&#xA0;Ode</a><br/>
<a href="adolc_poly.cpp.xml" target="_top">Adolc&#xA0;Speed:&#xA0;Second&#xA0;Derivative&#xA0;of&#xA0;a&#xA0;Polynomial</a><br/>
<a href="adolc_sparse_hessian.cpp.xml" target="_top">Adolc&#xA0;Speed:&#xA0;Sparse&#xA0;Hessian</a><br/>
<a href="adolc_sparse_jacobian.cpp.xml" target="_top">adolc&#xA0;Speed:&#xA0;sparse_jacobian</a><br/>
</div>
<hr/>Input File: omh/speed_adolc.omh

</body>
</html>
