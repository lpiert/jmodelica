Build instructions for the JModelica.org project.

0. Requirements.

In order to compile JModelica.org you will need a unix-like environment.
Depending on you operating system, different procedures are required.

 - For Linux and Mac, make sure that the following tools are installed: 
   - The gcc compiler suite
   - Subversion
   - Apache Ant
   - Cmake
   - Java development kit
 - For Windows, it is recommended to use the JModelica.org-SDK installer which
   contains all the tools necessary to build the platform. See the README-SDK
   file in the install folder for build instructions. 

1. Get JModelica.org

Check out a working copy of JModelica.org: 

 > svn co https://svn.jmodelica.org/trunk JModelica 

Make sure that the full path to the directory where you check out JModelica.org
does not contain any spaces nor ~ character.

2. Get Ipopt 

Download Ipopt (version 3.9 or later) from https://projects.coin-or.org/Ipopt.
Unzip the tar-ball and put in a directory that does not hold any spaces nor ~
character in its path. Build the package according to its INSTALL file. Make
sure to run

 > make install

3. Get Sundials 

Download Sundials (version 2.4.0) from
https://computation.llnl.gov/casc/sundials/main.html. Unzip the tar-ball and
put in a directory that does not hold any spaces nor ~ character in its path.
Build the package according to its INSTALL_NOTES file.

4. Configure

Run the configure script. It is recommended that you create a new
directory for building the platform

 > cd JModelica
 > mkdir build
 > cd build
 > ../configure --with-ipopt=/path/to/ipopt-install-dir \
     --with-sundials=/path/to/sundials-install-dir

The arguments --with-ipopt and --with-sundials are optional. Notice, however,
that if they are not given, only parts of the software will be built and the
functionality of the platform will be limited. You may want to give additional
arguments to configure. Type configure --help for information. By default, the
installation directory (--prefix) is set to /usr/local/jmodelica - use the
--prefix argument to change the default location.

5. Build and install

In order to build, type 

 > make 

In order to install, type 

 > make install 

which will render the directories 'lib' and 'include' to be created in the
installation directory and the corresponding libraries and directories and
header files will be copied. In addition, templates, XML schemas, third party
dependencies and makefiles used to build the generated C code are copied into
the installation directory. Also, the Python code is copied into the
installation directory. 

6. Generate documentation.

The command 

 > make docs 

will generate documentation in the 'doc' directory. The generated documentation
is also available at www.jmodelica.org, where nightly generated docs are
published.

7. Running JModelica.org from Python

In order to run the JModelica.org compilers from Python, the packages
 
 - JPype 0.5.4.2 (http://jpype.sourceforge.net/) 
 - lxml 2.3 (http://codespeak.net/lxml/) 
 - NumPy 1.6.1 (http://numpy.scipy.org/)
 - SciPy 0.9.0 (http://www.scipy.org/)
 - Cython 0.15 (http://www.cython.org/)
 - Matplotlib 1.0.1 (http://matplotlib.sourceforge.net/)
 - wxPython 2.8 <http://www.wxpython.org/>
 - IPython 0.11 <http://ipython.org/>
 - Nose 1.1.2 <http://readthedocs.org/docs/nose/en/latest/> (Only needed to run
   the test suits.)

need to be installed manually or through a package manager such as apt-get or
mac ports. The JModelica.org has been tested on Python 2.6 and Python 2.7. 

JModelica.org supports the following environment variables:

 - JMODELICA_HOME containing the path to the JModelica.org installation
   directory (again, without spaces or ~ in the path).
 - PYTHONPATH containing the path to the directory $JMODELICA_HOME/Python.
 - JAVA_HOME containing the path to a Java JRE or SDK installation.
 - IPOPT_HOME containing the path to an Ipopt installation directory.
 - LD_LIBRARY_PATH containing the path to the $IPOPT_HOME/lib directory
   (Linux only.)
 - MODELICAPATH containing a sequence of paths representing directories
   where Modelica libraries are located, separated by colons.

We recommend using the scripts:

$JMODELICA_HOME/bin/jm_ipython.sh

or

$JMODELICA_HOME/bin/jm_python.sh

to start IPython (recommended) or Python. In these scripts, the default values
of the environment variables are set to match your particular installation
configuration.

The default settings can be overridden either by setting one of the supported
environment variables globally or by adding a local startup.py file located in
the directory $HOME/jmodelica.org and change a variable there.

8. Test the distribution

Run 

 > make test 

to run the JModelica.org test suites.

To test that the Python packages are working, start Python or IPython. Type

 > import jmodelica.examples.cstr as cstr
 > cstr.run_demo()

You should now see the output of Ipopt and windows containing plots showing the
optimization results should be opened.

9. Check Python packages

There is a function in the jmodelica package to check the status of the Python
packages required by JModelica. Type

 > import jmodelica
 > jmodelica.check_packages()

This will list all required packages, if they are installed and package version
(if available). Compare your status output to the list of required packages
which can be found in this file under: 'Running JModelica.org from Python' or
on the homepage www.jmodelica.org.
