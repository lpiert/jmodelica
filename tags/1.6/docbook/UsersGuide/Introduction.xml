<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="ch_intro" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Introduction</title>

  <section>
    <title>About JModelica.org</title>

    <para>JModelica.org is an extensible Modelica-based open source platform
    for optimization, simulation and analysis of complex dynamic systems. The
    main objective of the project is to create an industrially viable open
    source platform for optimization of Modelica models, while offering a
    flexible platform serving as a virtual lab for algorithm development and
    research. As such, JModelica.org is intended to provide a platform for
    technology transfer where industrially relevant problems can inspire new
    research and where state of the art algorithms can be propagated form
    academia into industrial use. JModelica.org is a result of research at the
    Department of Automatic Control, Lund University,
    <citation>Jak2007</citation>and is now maintained and developed by Modelon
    AB in collaboration with academia.</para>
  </section>

  <section>
    <title>Mission Statement</title>

    <para>To offer a community-based, free, open source, accessible, user and
    application oriented Modelica environment for optimization and simulation
    of complex dynamic systems, built on well-recognized technology and
    supporting major platforms.</para>
  </section>

  <section>
    <title>Technology</title>

    <para>JModelica.org relies on the established modeling language <link
    xlink:href="http://www.modelica.org">Modelica</link><indexterm
        class="singular">
        <primary>Modelica</primary>
      </indexterm>. Modelica targets modeling of complex heterogeneous
    physical systems, and is becoming a de facto standard for dynamic model
    development and exchange. There are numerous model libraries for Modelica,
    both free and commercial, including the freely available Modelica Standard
    Library (MSL).</para>

    <para>A unique feature of JModelica.org is the support for the innovative
    extension Optimica<indexterm class="singular">
        <primary>Optimica</primary>
      </indexterm>. Optimica enables you to conveniently formulate
    optimization problems based on Modelica models using simple but powerful
    constructs for encoding of optimization interval, cost function and
    constraints. Optimica also features annotations for choosing and tailoring
    the underlying numerical optimization algorithm a particular optimization
    problem.</para>

    <para>The JModelica.org compilers are developed in the compiler
    construction framework JastAdd<indexterm class="singular">
        <primary>JastAdd</primary>
      </indexterm>. JastAdd is based on established concepts, including object
    orientation, aspect orientation and reference attributed grammars.
    Compilers developed in JastAdd are specified in terms of declarative
    attributes and equations which together forms an executable specification
    of the language semantics. In addition, JastAdd targets extensible
    compiler development which makes it easy to experiment with language
    extensions.</para>

    <para>For user interaction JModelica.org relies on the Python language.
    Python offers an interactive environment suitable for scripting,
    development of custom applications and prototype algorithm integration.
    The Python packages Numpy and Scipy provide support for numerical
    computation, including matrix and vector operations, basic linear algebra
    and plotting. The JModelica.org compilers as well as the model
    executables/dlls integrate seemlessly with Python and Numpy.</para>
  </section>

  <section xml:id="intro_sec_architecture">
    <title>Architecture</title>

    <para><figure>
        <title>JModelica platform architecture.</title>

        <mediaobject>
          <imageobject>
            <imagedata fileref="images/arch_0.preview.png" scalefit="1"
                       width="60%"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>The JModelica.org platform consists of a number of different
    parts:</para>

    <itemizedlist>
      <listitem>
        <para>The compiler front-ends (one for Modelica and one for
        Modelica/Optimica) transforms Modelica and Optimica code into a flat
        model representation. The compilers also check the correctness of
        model descriptions and reports errors.</para>
      </listitem>

      <listitem>
        <para>The compiler back-ends generates C code and XML code for
        Modelica and Optimica. The C code contains the model equations, cost
        functions and constraints whereas the XML code contains model meta
        data such as variable names and parameter values.</para>
      </listitem>

      <listitem>
        <para>The JModelica.org runtime library is written in C and contains
        supporting functions needed to compile the generated model C code.
        Also, the runtime library contains an integration with CppAD<indexterm
            class="singular">
            <primary>CppAD</primary>
          </indexterm>, a tool for computation of high accuracy derivatives by
        means of automatic differentiation.</para>
      </listitem>

      <listitem>
        <para>Currently, JModelica.org features one particular algorithm for
        solving dynamic optimization problems. The algorithm is based on
        collocation on finite elements and relies on the solver
        IPOPT<indexterm class="singular">
            <primary>IPOPT</primary>
          </indexterm> for obtaining a solution of the resulting NLP.</para>
      </listitem>

      <listitem>
        <para>JModelica.org uses Python for scripting and prototyping. For
        this purpose, a Python package is under development with the objective
        of offering functions for driving the compilers and for accessing the
        (compiled) functions in the runtime library/generated C code.</para>
      </listitem>
    </itemizedlist>

    <para></para>
  </section>

  <section>
    <title>Extensibility</title>

    <para>The JModelica.org platform is extensible in a number of different
    ways:</para>

    <itemizedlist>
      <listitem>
        <para>JModelica.org features a C interface for efficient evaluation of
        model equations, the cost function and the constraints: the JModelica
        Model Interface (JMI)<indexterm class="singular">
            <primary>JModelica Model Interface</primary>

            <see>JMI</see>
          </indexterm><indexterm class="singular">
            <primary>JMI</primary>

            <seealso>JModelica Model Interface</seealso>
          </indexterm>. JMI also contains functions for evaluation of
        derivatives and sparsity and is intended to offer a convenient
        interface for integration of numerical algorithms.</para>
      </listitem>

      <listitem>
        <para>In addition to the the C interface, model meta data can be
        exported in XML<indexterm class="singular">
            <primary>XML</primary>
          </indexterm>. In the future this feature is intended to be extended
        to include full model export in XML, which in turn enables use of XML
        techniques such as XPATH<indexterm class="singular">
            <primary>XPATH</primary>
          </indexterm> and XSLT.</para>
      </listitem>

      <listitem>
        <para>JastAdd produces compilers encoded in pure Java. As a result,
        the JModelica.org compilers are easily embedded in other applications
        aspiring to support Modelica and Optimica. In particular, a Java API
        for accessing the flat model representation and an extensible
        template-based code generation framework is offered.</para>
      </listitem>

      <listitem>
        <para>The JModelica.org compilers are developed using the compiler
        construction framework JastAdd. JastAdd features extensible compiler
        construction, both at the language level and at the implementation
        level. This feature is explored in JModelica.org where the Optimica
        compiler is implemented as a fully modular extension of the core
        Modelica compiler. The JModelica.org platform is a suitable choice for
        experimental language design and research.</para>
      </listitem>
    </itemizedlist>

    <para>An overview of the JModelica.org platform is given
    <citation>Jak2010</citation></para>
  </section>
</chapter>
