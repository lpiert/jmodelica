<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Getting started</title>

  <para>This chapter is intended to give a brief introduction to using the
  JModelica.org Python interface and will therefore not go into any details.
  Please refer to the other chapters of this manual for more information on
  each specific topic.</para>

  <section xml:id="gs_sec_starting_a_python_session">
    <title>Starting a Python session</title>

    <para>Starting a Python session differs somewhat depending on your
    operating system.</para>

    <section>
      <title>Windows</title>

      <para>If you are running Windows, three different Python shells are
      available under the JModelica.org start menu.<itemizedlist>
          <listitem>
            <para><emphasis role="bold">Python</emphasis> - Normal command
            shell started with Python.</para>
          </listitem>

          <listitem>
            <para><emphasis role="bold">IPython</emphasis> - Interactive shell
            for Python with, for example, code highlighting and tab
            completion.</para>
          </listitem>

          <listitem>
            <para><emphasis role="bold">pylab</emphasis> - IPython shell which
            also loads the numeric computation environment <link
            xlink:href="http://www.scipy.org/PyLab">PyLab</link>.</para>
          </listitem>
        </itemizedlist></para>
    </section>

    <section>
      <title>Linux or Mac OS</title>

      <para>To start the IPython shell with <link
      xlink:href="PyLab">PyLab</link> on Linux or Mac OS X, open a terminal
      and enter the command:</para>

      <programlisting language="python">&gt; $JMODELICA_HOME/Python/jm_ipython.sh -pylab
</programlisting>
    </section>
  </section>

  <section>
    <title>Run an example</title>

    <para>There are several Python scripts in the
    <filename>examples</filename> folder under the Python package in the
    JModelica.org installation which run examples demonstrating compilation,
    loading and simulation or optimization of models. The model files are
    located in the subdirectory <filename>files</filename>. The following code
    demonstrates how to run such an example. First a Python session must be
    started, see <xref linkend="gs_sec_starting_a_python_session" /> above.
    The example scripts are preferably run in the pylab python shell.</para>

    <para>The following code will run the RLC example and plot some
    results.</para>

    <para><programlisting language="python"># Import the RLC example
from jmodelica.examples import RLC

# Run the RLC example and plot results
RLC.run_demo()</programlisting></para>

    <para> Open <filename>RLC.py</filename> in an editor and look at the
    Python code to see what happens when the script is run.</para>
  </section>

  <section>
    <title>Check your installation</title>

    <para>The JModelica.org Python interface requires some Python packages in
    order to run correctly. Use the function
    <literal>jmodelica.check_packages()</literal> to list which Python
    packages that are found on your computer. Missing or having the wrong
    version of a package can be a source of errors. Therefore it can be useful
    to run <literal>jmodelica.check_packages()</literal> after installation or
    when trouble-shooting.<programlisting language="python"># Import the function from jmodelica
from jmodelica import check_packages

# Run check_packages
check_packages()

Performing JModelica package check
==================================

Platform...................... win32
Python version:............... 2.7.2
JModelica version:............ 1.6b1

Dependencies:

Package                        Version
-------                        -------
numpy......................... 1.6.1                          Ok
scipy......................... 0.9.0                          Ok
matplotlib.................... 1.0.1                          Ok
jpype......................... n/a                            Ok
lxml.......................... 2.3.0                          Ok
nose.......................... 1.1.2                          Ok
assimulo...................... n/a                            Ok
wxPython...................... 2.8.12.1                       Ok
cython........................ n/a                            Ok
casadi........................ n/a                            Ok
pyreadline.................... 1.7                            Ok
setuptools.................... 0.6c11                         Ok

</programlisting></para>
  </section>

  <section>
    <title>Redefining the JModelica.org environment</title>

    <para>When importing <literal>jmodelica</literal> in Python the script
    <filename>startup.py</filename> is run which sets the environment used by
    JModelica.org for the current Python session. For example, the environment
    variable <literal>JMODELICA_HOME</literal> points at the JModelica.org
    installation directory and <literal>IPOPT_HOME</literal> points at the
    Ipopt installation directory. One or more of these environment variables
    set in <filename>startup.py</filename> can be overridden by a user defined
    script: <filename>user_startup.py</filename>.</para>

    <para>The script <filename>startup.py</filename> looks for
    <filename>user_startup.py</filename> in the folder</para>

    <itemizedlist>
      <listitem>
        <para><filename>$USERPROFILE/.jmodelica.org/</filename>
        (Windows)</para>
      </listitem>

      <listitem>
        <para><filename>$HOME/.jmodelica.org/</filename> (unix)</para>
      </listitem>
    </itemizedlist>

    <para>If the script <filename>user_startup.py</filename> is not found, the
    default environment variables will be used.</para>

    <section>
      <title>Example redefining IPOPT_HOME</title>

      <para>The following step-by-step procedure will redefine the
      JModelica.org environment variable <literal>IPOPT_HOME</literal>:</para>

      <orderedlist>
        <listitem>
          <para>Create a text file and name it
          <filename>user_startup.py</filename>.</para>
        </listitem>

        <listitem>
          <para>Place the file in
          <filename>$USERPROFILE/.jmodelica.org/</filename> (Windows) or
          <filename>$HOME/.jmodelica.org/</filename> (unix). To find out what
          <literal>$USERPROFILE</literal> or <literal>$HOME</literal> points
          to, open a Python shell and type:</para>

          <programlisting language="python">import os
os.environ['USERPROFILE']   // windows
os.environ['HOME']          // unix</programlisting>
        </listitem>

        <listitem>
          <para>Open the file and type</para>

          <programlisting>environ['IPOPT_HOME']='&lt;new path to Ipopt home&gt;'</programlisting>
        </listitem>

        <listitem>
          <para>Save and close.</para>
        </listitem>

        <listitem>
          <para>Check your changes by opening a Python shell, import
          <literal>jmodelica</literal> and check the
          <literal>IPOPT_HOME</literal> environment variable:</para>

          <programlisting language="python">import jmodelica
jmodelica.environ['IPOPT_HOME']</programlisting>
        </listitem>
      </orderedlist>
    </section>
  </section>

  <section>
    <title>The JModelica.org user forum</title>

    <para>Please use the <link
    xlink:href="http://www.jmodelica.org/forum">JModelica.org forum</link> for
    any questions related to JModelica.org. You can search in old threads to
    see if someone has asked your question before or, if you are a member,
    start a new thread.</para>
  </section>
</chapter>
