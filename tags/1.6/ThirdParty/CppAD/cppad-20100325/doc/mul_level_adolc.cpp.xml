<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Using Adolc with Multiple Levels of Taping: Example and Test</title>
<meta name="description" id="description" content="Using Adolc with Multiple Levels of Taping: Example and Test"/>
<meta name="keywords" id="keywords" content=" multiple Adolc level "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_mul_level_adolc.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="base_adolc.hpp.xml" target="_top">Prev</a>
</td><td><a href="adfun.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>base_require</option>
<option>base_adolc.hpp</option>
<option>mul_level_adolc.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>base_require-&gt;</option>
<option>base_complex.hpp</option>
<option>base_adolc.hpp</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>base_adolc.hpp-&gt;</option>
<option>mul_level_adolc.cpp</option>
</select>
</td>
<td>mul_level_adolc.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Purpose</option>
<option>Tracking New and Delete</option>
<option>Configuration Requirement</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Using Adolc with Multiple Levels of Taping: Example and Test</big></big></b></center>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
This is an example and test of using Adolc's <code><font color="blue">adouble</font></code> type,
together with CppAD's <code><font color="blue"><span style='white-space: nowrap'>AD&lt;adouble&gt;</span></font></code> type,
for multiple levels of taping.
The example computes

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mi mathvariant='italic'>d</mi>
</mrow>
<mrow><mi mathvariant='italic'>dx</mi>
</mrow>
</mfrac>
<mrow><mo stretchy="true">[</mo><mrow><msup><mi mathvariant='italic'>f</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>v</mi>
</mrow><mo stretchy="true">]</mo></mrow>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
</mrow></math>

 and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>v</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

.
The example <a href="hestimesdir.cpp.xml" target="_top"><span style='white-space: nowrap'>HesTimesDir.cpp</span></a>
 computes the same value using only
one level of taping (more efficient) and the identity

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mi mathvariant='italic'>d</mi>
</mrow>
<mrow><mi mathvariant='italic'>dx</mi>
</mrow>
</mfrac>
<mrow><mo stretchy="true">[</mo><mrow><msup><mi mathvariant='italic'>f</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>v</mi>
</mrow><mo stretchy="true">]</mo></mrow>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>f</mi>
<mrow><mo stretchy="false">(</mo>
<mn>2</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>v</mi>
</mrow></math>

The example <a href="mul_level.cpp.xml" target="_top"><span style='white-space: nowrap'>mul_level.cpp</span></a>
 computes the same values using
<code><font color="blue">AD&lt;double&gt;</font></code> and <code><font color="blue">AD&lt; AD&lt;double&gt; &gt;</font></code>.

<br/>
<br/>
<b><big><a name="Tracking New and Delete" id="Tracking New and Delete">Tracking New and Delete</a></big></b>
<br/>
Adolc uses raw memory arrays that depend on the number of 
dependent and independent variables, hence <code><font color="blue">new</font></code> and <code><font color="blue">delete</font></code>
are used to allocate this memory.
The preprocessor macros 
<small>
<a href="tracknewdel.xml#TrackNewVec" target="_top"><span style='white-space: nowrap'>CPPAD_TRACK_NEW_VEC</span></a>
 
</small>
and
<small>
<a href="tracknewdel.xml#TrackDelVec" target="_top"><span style='white-space: nowrap'>CPPAD_TRACK_DEL_VEC</span></a>
 
</small>
are used to check for errors in the
use of <code><font color="blue">new</font></code> and <code><font color="blue">delete</font></code> when the example is compiled for
debugging (when <code><font color="blue">NDEBUG</font></code> is not defined).


<br/>
<br/>
<b><big><a name="Configuration Requirement" id="Configuration Requirement">Configuration Requirement</a></big></b>
<br/>
This example will be compiled and tested provided that
the value <a href="installunix.xml#AdolcDir" target="_top"><span style='white-space: nowrap'>AdolcDir</span></a>
 is specified on the 
<a href="installunix.xml#Configure" target="_top"><span style='white-space: nowrap'>configure</span></a>
 command line.

<code><font color="blue">
<pre style='display:inline'> 
# include &lt;adolc/adouble.h&gt;
# include &lt;adolc/taping.h&gt;
# include &lt;adolc/interfaces.h&gt;

// adouble definitions not in Adolc distribution and 
// required in order to use CppAD::<a href="ad.xml" target="_top">AD</a>&lt;adouble&gt;
# include &quot;base_adolc.hpp&quot;

# include &lt;cppad/cppad.hpp&gt;



namespace { // put this function in the empty namespace

	// f(x) = |x|^2 = .5 * ( x[0]^2 + ... + x[n-1]^2 + .5 )
	template &lt;class Type&gt;
	Type f(CPPAD_TEST_VECTOR&lt;Type&gt; &amp;x)
	{	Type sum;

		// check assignment of <a href="ad.xml" target="_top">AD</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; = double
		sum  = .5;
		sum += .5;

		size_t i = x.size();
		while(i--)
			sum += x[i] * x[i];

		// check computed assignment <a href="ad.xml" target="_top">AD</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; -= int
		sum -= 1; 
	
		// check double * <a href="ad.xml" target="_top">AD</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; 
		return .5 * sum;
	} 
}

bool mul_level_adolc(void) 
{	bool ok = true;                   // initialize test result

	typedef adouble      ADdouble;         // for first level of taping
	typedef CppAD::<a href="ad.xml" target="_top">AD</a>&lt;ADdouble&gt; ADDdouble; // for second level of taping
	size_t n = 5;                          // number independent variables

	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;double&gt;       x(n);
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;ADdouble&gt;   a_x(n);
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;ADDdouble&gt; aa_x(n);

	// value of the independent variables
	int tag = 0;                         // Adolc setup
	int keep = 1;
	trace_on(tag, keep);
	size_t j;
	for(j = 0; j &lt; n; j++)
	{	x[j] = double(j);           // x[j] = j
		a_x[j] &lt;&lt;= x[j];            // a_x is independent for ADdouble
	}
	for(j = 0; j &lt; n; j++)
		aa_x[j] = a_x[j];          // track how aa_x depends on a_x
	CppAD::<a href="independent.xml" target="_top">Independent</a>(aa_x);          // aa_x is independent for ADDdouble

	// compute function
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;ADDdouble&gt; aa_f(1);    // scalar valued function
	aa_f[0] = f(aa_x);                 // has only one component

	// declare inner function (corresponding to ADDdouble calculation)
	CppAD::<a href="funconstruct.xml" target="_top">ADFun</a>&lt;ADdouble&gt; a_F(aa_x, aa_f);

	// compute f'(x) 
	size_t p = 1;                        // order of derivative of a_F
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;ADdouble&gt; a_w(1);  // weight vector for a_F
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;ADdouble&gt; a_df(n); // value of derivative
	a_w[0] = 1;                          // weighted function same as a_F
	a_df   = a_F.<a href="reverse.xml" target="_top">Reverse</a>(p, a_w);        // gradient of f

	// declare outter function 
	// (corresponding to the tape of adouble operations)
	double df_j;
	for(j = 0; j &lt; n; j++)
		a_df[j] &gt;&gt;= df_j;
	trace_off();

	// compute the d/dx of f'(x) * v = f''(x) * v
	size_t m      = n;                     // # dependent in f'(x)
	double *v = 0, *ddf_v = 0;
	v     = CPPAD_TRACK_NEW_VEC(m, v);     // track v = new double[m]
	ddf_v = CPPAD_TRACK_NEW_VEC(n, ddf_v); // track ddf_v = new double[n]
	for(j = 0; j &lt; n; j++)
		v[j] = double(n - j);
	fos_reverse(tag, int(m), int(n), v, ddf_v);

	// f(x)       = .5 * ( x[0]^2 + x[1]^2 + ... + x[n-1]^2 )
	// f'(x)      = (x[0], x[1], ... , x[n-1])
	// f''(x) * v = ( v[0], v[1],  ... , x[n-1] )
	for(j = 0; j &lt; n; j++)
		ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>(ddf_v[j], v[j], 1e-10, 1e-10);

	CPPAD_TRACK_DEL_VEC(v);                 // check usage of delete
	CPPAD_TRACK_DEL_VEC(ddf_v);
	return ok;
}</pre>
</font></code>


<hr/>Input File: example/mul_level_adolc.cpp

</body>
</html>
