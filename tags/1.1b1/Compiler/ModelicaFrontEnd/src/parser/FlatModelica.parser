/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

 class Events extends Parser.Events {
  
    public void syntaxError(Symbol token) {
      StringBuffer s = new StringBuffer();
      s.append("Syntax error at line "+ Symbol.getLine(token.getStart()) + ", column " + Symbol.getColumn(token.getStart()) + "\n");
      if (token.value != null) {
      	s.append("   Unexpected token: \"" + token.value + "\"");
      } else if (token.getId()<Terminals.NAMES.length)
        s.append("  Unexpected token: " + Terminals.NAMES[token.getId()]);
      else 
        s.append("  *** Syntactic error");
      throw new Error(s.toString());
    }
    
  }

  {report = new Events();}  // Use error handler in parser

	public void syntaxError(Symbol token, String errorMessage) {
      StringBuffer s = new StringBuffer();
      s.append(Symbol.getLine(token.getStart()) + ", " + Symbol.getColumn(token.getStart()) + "\n");
        s.append("  *** Syntactic error: "+ errorMessage);
      throw new Error(s.toString());
    }

	public FlatRoot parseFile(String fileName) {
		FlatRoot fr = null;
		try {
			Reader reader = new FileReader(fileName);
			FlatModelicaScanner scanner = new FlatModelicaScanner(new BufferedReader(reader));
			fr = (FlatRoot)parse(scanner);
			fr.setFileName(fileName);
		}  catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return fr;
	}
	
		public FlatRoot parseString(String str, String fileName) {
		FlatRoot fr = null;
		try {
			FlatModelicaScanner scanner = new FlatModelicaScanner(new StringReader(str));
			fr = (FlatRoot)parse(scanner);
			fr.setFileName(fileName);
		}  catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return fr;
	}

:};


FlatRoot  flat_root = 
    fclass {: return new FlatRoot(fclass); :}
    ;

FClass fclass = 
    ID.restriction
    fqname
	fvariable_list
	fequation_section
	ffunction*
	END_ID.end_s 
	SEMICOLON {: return new FClass(fqname, 
	                               fvariable_list, 
	                               new List(), 
	                               new List(), 
	                               new List().add(new FEquationBlock(fequation_section)), 
	                               new List()); :}
	;

List fvariable_list = 
	fvariable SEMICOLON    {: return new List().add(fvariable); :}
	| fvariable_list fvariable SEMICOLON {: fvariable_list.add(fvariable);
	                              return fvariable_list; :}
	;
	
FVariable fvariable =
    fvisibility_type?
    ftype_prefix_variability? 
    ftype_prefix_input_output? 
	fprimitive_type
	fqname
	fattributes?
	bexp?
	fstring_comment?        {: List fattr = null;
					if (fattributes.getNumChild()==0)
						fattr = new List();
					else 
						fattr = (List)fattributes.getChild(0);
					FTypePrefixVariability tpo = ftype_prefix_variability.getNumChild()==1?
					  (FTypePrefixVariability)ftype_prefix_variability.getChild(0):
					  new FContinuous();
                    FVisibilityType fvt = fvisibility_type.getNumChild()==1?
                      (FVisibilityType)fvisibility_type.getChild(0) :
                      new FPublicVisibilityType();
                    if (fprimitive_type.isReal()) {   
						return new FRealVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isInteger()) {   
						return new FIntegerVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isBoolean()) {   
						return new FBooleanVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} else if (fprimitive_type.isString()) {   
						return new FStringVariable(fvt,tpo,
	                       ftype_prefix_input_output,fattr,bexp,fstring_comment,fqname); 
					} 
					return null;
					:}
	;
  
FVisibilityType fvisibility_type =
  PUBLIC    {: return new FPublicVisibilityType(); :}  
  | PROTECTED    {: return new FProtectedVisibilityType(); :}  
  ;
FQName fqname =
  fqname_part {: return new FQName(new List().add(fqname_part)); :}
  | fqname DOT fqname_part {: fqname.addFQNamePart(fqname_part); 
                              return fqname;:}  
  ;
  
FQNamePart fqname_part =
  ID.name farray_subscripts? {: return new FQNamePart(name,farray_subscripts); :}
  ;

FArraySubscripts farray_subscripts = 
  LBRACK fsubscript_list RBRACK {: return new FArraySubscripts(fsubscript_list); :}
  ;
  
List fsubscript_list =
  fsubscript               {: return new List().add(fsubscript); :}
  | fsubscript_list COMMA fsubscript {: fsubscript_list.add(fsubscript);
                                      return fsubscript_list; :}
  ;
  
FSubscript fsubscript =
  COLON                 {: return new FColonSubscript(); :}
  | fexp                 {: return new FExpSubscript(fexp); :}                                                                           
  ;

FEach feach =
    EACH {: return new FEach(); :}  
	;
	
FFinal ffinal =
    FINAL {: return new FFinal(); :}  
	;	
	
List fattributes =
	LPAREN 
	fattribute_list
	RPAREN {: return fattribute_list; :}
	;
	
List fattribute_list = 
	fattribute    {: return new List().add(fattribute); :}
	| fattribute_list COMMA fattribute {: fattribute_list.add(fattribute);
	                              return fattribute_list; :}
	;

FAttribute fattribute =
    feach?
    ffinal?
	fid_decl
	EQUALS
	fexp {: return new FAttribute(new FIdUse(new FQName(new List().add(new FQNamePart("UnknownType",new Opt())))),
	  fid_decl,new Opt(fexp),true,feach,ffinal,new List()); :}
	;
	
FExp bexp =
	EQUALS
	fexp {: return fexp; :}
	;
	
FExp fbexp =
	ASSIGN
	fexp {: return fexp; :}
	;

FFunctionDecl ffunction = 
    FUNCTION 
    fqname.name
    ffunction_variable*
    falgorithm?
    END
    fqname.end
    SEMICOLON        {: return new FFunctionDecl(name, ffunction_variable, falgorithm); :}
    ;

FFunctionVariable ffunction_variable =
    ftype_prefix_input_output?
	fprimitive_type
	fqname
	fbexp?
	SEMICOLON        {: return new FFunctionVariable(ftype_prefix_input_output, 
	                                                 fprimitive_type, 
	                                                 fbexp, 
	                                                 fqname);  :}
    ;

List fequation_section = 
                                          {: return new List(); :}
  | fequation_section EQUATION fequation* {: for (Object e : fequation)
                                                 fequation_section.add((FEquation) e);
                                             return fequation_section; :}
  | fequation_section falgorithm          {: return fequation_section.add(falgorithm); :}
  ;

FEquation fequation = 
	fexp.left 
	EQUALS 
	fexp.right 
	SEMICOLON  {: return new FEquation(left,right); :} 
	;

FAlgorithmBlock falgorithm =
    ALGORITHM fstatement_list  {: return new FAlgorithmBlock(fstatement_list); :}
    ;

List fstatement_list =
                                {: return new List(); :}
  | fstatement                  {: return new List().add(fstatement); :}
  | fstatement_list fstatement  {: fstatement_list.add(fstatement); 
                                   return fstatement_list; :}
  ;  

FStatement fstatement =
  fid_use.left ASSIGN fexp.right SEMICOLON                  
                                      {: return new FAssignStmt(left, right); :}
//  | ffunction_call_stmt SEMICOLON     {: return ffunction_call_stmt; :}
  | BREAK SEMICOLON                   {: return new FBreakStmt(); :} 
  | RETURN SEMICOLON                  {: return new FReturnStmt(); :}
  | fif_stmt SEMICOLON                {: return fif_stmt; :}
  | fwhen_stmt SEMICOLON              {: return fwhen_stmt; :}
  | ffor_stmt SEMICOLON               {: return ffor_stmt; :}
  | fwhile_stmt SEMICOLON             {: return fwhile_stmt; :}
  ;

/*
 * Needs ffunction_call - commented out for now.  
FFunctionCallStmt ffunction_call_stmt =
    LPAREN fcs_left_list RPAREN ASSIGN ffunction_call
                      {: return new FFunctionCallStmt(fcs_left_list, ffunction_call); :}
  | ffunction_call    {: return new FFunctionCallStmt(new List(), ffunction_call); :}
  ;

List fcs_left_list =
    fid_use                      {: return new List().add(fid_use); :}
  | fcs_left_list COMMA fid_use  {: return fcs_left_list.add(fid_use); :}
  ;
*/

FIfStmt fif_stmt =
  IF fif_clause_list else_stmts END_IF  {: return new FIfStmt(fif_clause_list, else_stmts); :}
  ;  

List fif_clause_list =
    fif_clause                         {: return new List().add(fif_clause); :}
  | fif_clause_list ELSEIF fif_clause  {: return fif_clause_list.add(fif_clause); :}
  ;

FIfClause fif_clause =
  fexp THEN fstatement_list  {: return new FIfClause(fexp, fstatement_list); :}
  ;

List else_stmts =
                          {: return new List(); :}
  | ELSE fstatement_list  {: return fstatement_list; :}
  ;

FWhenStmt fwhen_stmt =
  WHEN fwhen_clause_list END_WHEN  {: return new FWhenStmt(fwhen_clause_list); :}
  ;  

List fwhen_clause_list =
    fwhen_clause                             {: return new List().add(fwhen_clause); :}
  | fwhen_clause_list ELSEWHEN fwhen_clause  {: return fwhen_clause_list.add(fwhen_clause); :}
  ;

FWhenClause fwhen_clause =
  fexp THEN fstatement_list  {: return new FWhenClause(fexp, fstatement_list); :}
  ;

FForStmt ffor_stmt =
  FOR ffor_indices LOOP fstatement_list END_FOR
                         {: return new FForStmt(ffor_indices, fstatement_list); :}
  ;

List ffor_indices = 
    ffor_index                     {: return new List().add(ffor_index); :}
  | ffor_indices COMMA ffor_index  {: return ffor_indices.add(ffor_index); :}
  ;


FForIndex ffor_index =
    fid_decl IN fexp    {: return new FForIndex(new FIntegerVariable(new FPublicVisibilityType(),
                                                              new FDiscrete(),
                                                              new Opt(),
                                                              new List(),
                                                              new Opt(),
                                                              new Opt(),
                                                              new FQName(fid_decl.name())), new Opt(fexp)); :}
  | fid_decl            {: return new FForIndex(new FIntegerVariable(new FPublicVisibilityType(),
                                                              new FDiscrete(),
                                                              new Opt(),
                                                              new List(),
                                                              new Opt(),
                                                              new Opt(),
                                                              new FQName(fid_decl.name())), new Opt()); :}
  ;

FWhileStmt fwhile_stmt = 
  WHILE fexp LOOP fstatement_list END_WHILE  {: return new FWhileStmt(fexp, fstatement_list); :}
  ;

FTypePrefix ftype_prefix_variability = 
   PARAMETER                          {: return new FParameter(); :}
  | CONSTANT                          {: return new FConstant(); :}
  | DISCRETE                          {: return new FDiscrete(); :}
  ;
  
FTypePrefix ftype_prefix_input_output = 
    INPUT                          {: return new FInput(); :}
  | OUTPUT                          {: return new FOutput(); :}
  ;

FStringComment fstring_comment =
  STRING.s      {: return new FStringComment(s); :}
  | fstring_comment PLUS STRING.s {: fstring_comment.setComment(fstring_comment.getComment().concat(s));
                                    return fstring_comment; :}
  
  ;

FExp fexp = 
  fprimary {: return fprimary; :}
  | fexp.a PLUS fexp.b {: return new FAddExp(a,b); :} 
  | fexp.a MINUS fexp.b {: return new FSubExp(a,b); :}
  | fexp.a MULT fexp.b {: return new FMulExp(a,b); :}
  | fexp.a DIV fexp.b {: return new FDivExp(a,b); :}  
  | fprimary.a POW fprimary.e {: return new FPowExp(a,e); :}
  ;
  
FExp fprimary =        
   UNSIGNED_NUMBER.n    {: return new FRealLitExp(n); :}
  | MINUS fprimary       {: return new FNegExp(fprimary); :}
  | STRING.s               {: return new FStringLitExp(s); :}
  | fid_use LPAREN arg_list RPAREN {: return new FFunctionCall(fid_use,arg_list); :}
  | fid_use        {: return new FIdUseExp(fid_use); :} 
  | LPAREN fexp.a RPAREN {: return a; :}
  | TIME {: return new FTimeExp(); :}
  | END                 {: return new FEndExp(); :} 
  ;

FPrimitiveType fprimitive_type =
  ID.name {: if (name.equals("Real")) return new FRealScalarType(); 
  	else if (name.equals("Boolean")) return new FBooleanScalarType();
  	else if (name.equals("String")) return new FStringScalarType();
  	else if (name.equals("Integer")) return new FIntegerScalarType();
  	else return null; :}
  ;
  
FIdUse fid_use = 
   fqname   {: return new FIdUse(fqname); :}
   ;
   
FIdDecl fid_decl = 
   fqname   {: 
   			 return new FIdDecl(fqname); :}
   ;
   
 List arg_list = 
  fexp                   {: return new List().add(fexp); :}
  | arg_list COMMA fexp  {: arg_list.add(fexp); 
                           return arg_list; :}
  ;
   
