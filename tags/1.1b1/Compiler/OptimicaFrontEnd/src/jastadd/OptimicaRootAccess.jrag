/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect OptimicaRootAccess {
	
	refine RootAccess eq SourceRoot.language() {
		return "Optimica";
	}
	
	refine RootAccess FClass ParserHandler.newFClass() {
		return new FOptClass();
	}

	refine RootAccess SourceRoot ParserHandler.parseFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		return parseOptimicaFile(fileName);
	}

	refine RootAccess SourceRoot ParserHandler.parseString(String str, String fileName)  
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		return parseOptimicaString(str,fileName);
	}
	
	refine RootAccess Exp ParserHandler.parseExpString(String str) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		return parseOptimicaExpString(str);
	}
	
	refine RootAccess SourceRoot ParserHandler.parseModelicaFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		SourceRoot sr = null;
		Reader reader = new FileReader(fileName);
		org.jmodelica.optimica.parser.ModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.ModelicaScanner(new BufferedReader(reader));
		org.jmodelica.optimica.parser.ModelicaParser parser = new org.jmodelica.optimica.parser.ModelicaParser();
		parser.setLineBreakMap(scanner.getLineBreakMap());
		sr = (SourceRoot)parser.parse(scanner);
		sr.setFileName(fileName);
		return sr;
	}
	
	refine RootAccess SourceRoot ParserHandler.parseModelicaString(String str, String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		SourceRoot sr = null;
		org.jmodelica.optimica.parser.ModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.ModelicaScanner(new StringReader(str));
		org.jmodelica.optimica.parser.ModelicaParser parser = 
			new org.jmodelica.optimica.parser.ModelicaParser();
		parser.setLineBreakMap(scanner.getLineBreakMap());
		sr = (SourceRoot)parser.parse(scanner);
		sr.setFileName(fileName);
		return sr;
	}
	
	refine RootAccess Exp ParserHandler.parseModelicaExpString(String str)
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		Exp exp = null;
		org.jmodelica.optimica.parser.ModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.ModelicaScanner(new StringReader(str));
		org.jmodelica.optimica.parser.ModelicaParser parser = 
			new org.jmodelica.optimica.parser.ModelicaParser();
		exp = (Exp)parser.parse(scanner,org.jmodelica.optimica.parser.ModelicaParser.AltGoals.exp);
		//System.out.println("ModelicaParser.parseExpString: "+str);
		//exp.dumpTreeBasic("");
		return exp;
	}

	refine RootAccess FlatRoot ParserHandler.parseFlatModelicaFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		FlatRoot fr = null;
		Reader reader = new FileReader(fileName);
		org.jmodelica.optimica.parser.FlatModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.FlatModelicaScanner(new BufferedReader(reader));
		org.jmodelica.optimica.parser.FlatModelicaParser parser = 
			new org.jmodelica.optimica.parser.FlatModelicaParser();
		fr = (FlatRoot)parser.parse(scanner);
		fr.setFileName(fileName);
		return fr;
	}
	
	refine RootAccess FlatRoot ParserHandler.parseFlatModelicaString(String str, String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		FlatRoot fr = null;
		org.jmodelica.optimica.parser.FlatModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.FlatModelicaScanner(new StringReader(str));
		org.jmodelica.optimica.parser.FlatModelicaParser parser = 
			new org.jmodelica.optimica.parser.FlatModelicaParser();
		fr = (FlatRoot)parser.parse(scanner);
		fr.setFileName(fileName);
		return fr;
	}
	
	refine RootAccess FExp ParserHandler.parseFlatModelicaExpString(String str)
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		FExp fexp = null;
		org.jmodelica.optimica.parser.FlatModelicaScanner scanner = 
			new org.jmodelica.optimica.parser.FlatModelicaScanner(new StringReader(str));
		org.jmodelica.optimica.parser.FlatModelicaParser parser = 
			new org.jmodelica.optimica.parser.FlatModelicaParser();
		fexp = (FExp)parser.parse(scanner,org.jmodelica.optimica.parser.FlatModelicaParser.AltGoals.fexp);
		return fexp;
	}

	public SourceRoot ParserHandler.parseOptimicaFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		SourceRoot sr = null;
		Reader reader = new FileReader(fileName);
		OptimicaScanner scanner = new OptimicaScanner(new BufferedReader(reader));
		OptimicaParser parser = new OptimicaParser();
		parser.setLineBreakMap(scanner.getLineBreakMap());
		sr = (SourceRoot)parser.parse(scanner);
		sr.setFileName(fileName);
		return sr;
	}

	public SourceRoot ParserHandler.parseOptimicaString(String str, String fileName)  
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		SourceRoot sr = null;
		OptimicaScanner scanner = new OptimicaScanner(new StringReader(str));
		OptimicaParser parser = new OptimicaParser();
		parser.setLineBreakMap(scanner.getLineBreakMap());
		sr = (SourceRoot)parser.parse(scanner);
		sr.setFileName(fileName);
		return sr;
	}
	
	public Exp ParserHandler.parseOptimicaExpString(String str) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException{
		Exp exp = null;
		OptimicaScanner scanner = new OptimicaScanner(new StringReader(str));
		OptimicaParser parser = new OptimicaParser();
		exp = (Exp)parser.parse(scanner,OptimicaParser.AltGoals.exp);
		//System.out.println("OptimicaParser.parseExpString: "+str);
		//exp.dumpTreeBasic("");		
		return exp;
	}
	
	
}