<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Source: ode_evaluate</title>
<meta name="description" id="description" content="Source: ode_evaluate"/>
<meta name="keywords" id="keywords" content=" ode_evaluate source "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_ode_evaluate.hpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="ode_evaluate.cpp.xml" target="_top">Prev</a>
</td><td><a href="sparse_evaluate.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_utility</option>
<option>ode_evaluate</option>
<option>ode_evaluate.hpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed_utility-&gt;</option>
<option>uniform_01</option>
<option>det_of_minor</option>
<option>det_by_minor</option>
<option>det_by_lu</option>
<option>det_33</option>
<option>det_grad_33</option>
<option>ode_evaluate</option>
<option>sparse_evaluate</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>ode_evaluate-&gt;</option>
<option>ode_evaluate.cpp</option>
<option>ode_evaluate.hpp</option>
</select>
</td>
<td>ode_evaluate.hpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>Source: ode_evaluate</big></big></b></center>
<code><font color="blue"># ifndef CPPAD_ODE_EVALUATE_INCLUDED 
<code><span style='white-space: nowrap'><br/>
</span></code># define CPPAD_ODE_EVALUATE_INCLUDED 

<pre style='display:inline'> 
# include &lt;cppad/vector.hpp&gt;
# include &lt;cppad/runge_45.hpp&gt;

namespace CppAD {  // BEGIN CppAD namespace

template &lt;class Float&gt;
class ode_evaluate_fun {
private:
	const size_t m_;
	const CppAD::vector&lt;Float&gt; x_;
public:
	ode_evaluate_fun(size_t m, const CppAD::vector&lt;Float&gt; &amp;x) 
	: m_(m), x_(x)
	{ }
	void Ode(
		const Float                  &amp;t, 
		const CppAD::vector&lt;Float&gt;   &amp;z, 
		CppAD::vector&lt;Float&gt;         &amp;h) 
	{
		if( m_ == 0 )
			ode_y(t, z, h);
		if( m_ == 1 )
			ode_z(t, z, h);
	}
	void ode_y(
		const Float                  &amp;t, 
		const CppAD::vector&lt;Float&gt;   &amp;y, 
		CppAD::vector&lt;Float&gt;         &amp;g) 
	{	// y_t = g(t, x, y)
		CPPAD_ASSERT_UNKNOWN( y.size() == x_.size() );

		size_t i, n = x_.size();
		Float yi1 = Float(1);
		for(i = 0; i &lt; n; i++)
		{	g[i]  = Float(int(i+1)) * x_[i] * yi1;
			yi1   = y[i];
		}

		// solution  for this equation is
		// y_0 (t) = x_0 * t
		// y_1 (t) = x_1 * x_0 * t^2 
		// y_2 (t) = x_2 * x_1 * x_0 * t^3
		// ...
	}
	void ode_z(
		const Float                  &amp;t , 
		const CppAD::vector&lt;Float&gt;   &amp;z , 
		CppAD::vector&lt;Float&gt;         &amp;h ) 
	{	// z    = [ y ; y_x ]
		// z_t  = h(t, x, z) = [ y_t , y_x_t ]
		size_t i, j, n = x_.size();
		CPPAD_ASSERT_UNKNOWN( z.size() == n + n * n );

		// y_t
		Float zi1 = Float(1);
		for(i = 0; i &lt; n; i++)
		{	h[i] = Float(int(i+1)) * x_[i] * zi1;
			for(j = 0; j &lt; n; j++)
				h[n + i * n + j] = 0.;
			zi1 = z[i];
		}
		size_t ij;
		Float gi_xi, gi_yi1, yi1_xj;

		// y0_x0_t
		h[n] += 1.;

		// yi_xj_t
		for(i = 1; i &lt; n; i++)
		{	// partial g[i] w.r.t. x[i]
			gi_xi  = Float(int(i+1)) * z[i-1];
			ij     = n + i * n + i;	
			h[ij] += gi_xi;
			// partial g[i] w.r.t y[i-1] 
			gi_yi1 = Float(int(i+1)) * x_[i];
			// multiply by partial y[i-1] w.r.t x[j];
			for(j = 0; j &lt; n; j++)
			{	ij     = n + (i-1) * n + j;
				yi1_xj = z[ij];
				ij     = n + i * n + j;
				h[ij] += gi_yi1 * yi1_xj;
			} 
		}
	}
};

template &lt;class Float&gt;
void ode_evaluate(
	CppAD::vector&lt;Float&gt; &amp;x  , 
	size_t m                 , 
	CppAD::vector&lt;Float&gt; &amp;fm )
{
	typedef CppAD::vector&lt;Float&gt; Vector;

	size_t n = x.size();
	size_t ell;
	CPPAD_ASSERT_KNOWN( m == 0 || m == 1,
		&quot;ode_evaluate: m is not zero or one&quot;
	);
	CPPAD_ASSERT_KNOWN( 
		((m==0) &amp; (fm.size()==1) ) || ((m==1) &amp; (fm.size()==n)),
		&quot;ode_evaluate: the size of fm is not correct&quot;
	);
	if( m == 0 )
		ell = n;
	else	ell = n + n * n;

	// set up the case we are integrating
	size_t M  = 10;
	Float  ti = 0.;
	Float  tf = 1.;
	Vector yi(ell);
	Vector yf(ell);

	size_t i;
	for(i = 0; i &lt; ell; i++)
		yi[i] = Float(0);

	// construct ode equation
	ode_evaluate_fun&lt;Float&gt; f(m, x);

	// solve differential equation
	yf = Runge45(f, M, ti, tf, yi);

	if( m == 0 )
		fm[0] = yf[n-1];
	else
	{	for(i = 0; i &lt; n; i++)
			fm[i] = yf[n + (n-1) * n + i];
	}
	return;
}

} // END CppAD namespace</pre>

# endif
</font></code>


<hr/>Input File: omh/ode_evaluate.omh

</body>
</html>
