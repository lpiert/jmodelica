<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Definition of a Numeric Type</title>
<meta name="description" id="description" content="Definition of a Numeric Type"/>
<meta name="keywords" id="keywords" content=" numeric type default constructor int copy exercise "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_numerictype_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="speed_program.cpp.xml" target="_top">Prev</a>
</td><td><a href="numerictype.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>NumericType</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>opt_val_hes</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>NumericType-&gt;</option>
<option>NumericType.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Type Requirements</option>
<option>Default Constructor</option>
<option>Constructor From Integer</option>
<option>Copy Constructor</option>
<option>Assignment</option>
<option>Operators</option>
<option>Example</option>
<option>Exercise</option>
</select>
</td>
</tr></table><br/>






<center><b><big><big>Definition of a Numeric Type</big></big></b></center>
<br/>
<b><big><a name="Type Requirements" id="Type Requirements">Type Requirements</a></big></b>
<br/>
A <i>NumericType</i> is any type
that satisfies the requirements below.
The following is a list of some numeric types:
<code><font color="blue">int</font></code>, <code><font color="blue">float</font></code>, <code><font color="blue">double</font></code>, 
<code><font color="blue">AD&lt;double&gt;</font></code>, <code><font color="blue">AD&lt; AD&lt;double&gt; &gt;</font></code>.
The routine <a href="checknumerictype.xml" target="_top"><span style='white-space: nowrap'>CheckNumericType</span></a>
 can be used to check
that a type satisfies these conditions.

<br/>
<br/>
<b><big><a name="Default Constructor" id="Default Constructor">Default Constructor</a></big></b>


<br/>
The syntax 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>NumericType</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>;<br/>
</span></font></code>creates a <i>NumericType</i> object
with an unspecified value.

<br/>
<br/>
<b><big><a name="Constructor From Integer" id="Constructor From Integer">Constructor From Integer</a></big></b>


<br/>
If <i>i</i> is an <code><font color="blue">int</font></code>,
the syntax 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>NumericType</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
</span></font></code>creates a <i>NumericType</i> object with a value
equal to <i>i</i> where <i>i</i> can be <code><font color="blue">const</font></code>.

<br/>
<br/>
<b><big><a name="Copy Constructor" id="Copy Constructor">Copy Constructor</a></big></b>


<br/>
If <i>x</i> is a <i>NumericType</i> object
the syntax 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>NumericType</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
</span></font></code>creates a <i>NumericType</i> object <i>y</i>
with the same value as <i>x</i>
where <i>x</i> can be <code><font color="blue">const</font></code>.

<br/>
<br/>
<b><big><a name="Assignment" id="Assignment">Assignment</a></big></b>
<br/>
If <i>x</i> and <i>y</i> are <i>NumericType</i> objects,
the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>sets the value of <i>x</i> equal to the value of <i>y</i>
where <i>y</i> can be <code><font color="blue">const</font></code>.
The expression corresponding to this operation is unspecified; i.e.,
it could be <code><font color="blue">void</font></code> and hence
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>may not be legal.

<br/>
<br/>
<b><big><a name="Operators" id="Operators">Operators</a></big></b>
<br/>
Suppose <i>x</i>, <i>y</i> and <i>z</i>
<i>NumericType</i> objects where
<i>x</i> and <i>y</i> may be <code><font color="blue">const</font></code>.
In the result type column,
<i>NumericType</i> can be replaced by any type that can 
be used just like a <i>NumericType</i> object.
<table><tr><td align='left'  valign='top'>

<b>Operation</b>     </td><td align='left'  valign='top'>
 <b>Description</b>               </td><td align='left'  valign='top'>

	<b>Result Type</b>  </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"><span style='white-space: nowrap'>+</span></font></code><i><span style='white-space: nowrap'>x</span></i>        </td><td align='left'  valign='top'>
 unary plus                        </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"><span style='white-space: nowrap'>-</span></font></code><i><span style='white-space: nowrap'>x</span></i>        </td><td align='left'  valign='top'>
 unary minus                       </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;+&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 binary addition                   </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;-&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 binary subtraction                </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;*&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 binary multiplication             </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;/&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 binary division                   </td><td align='left'  valign='top'>

	<i>NumericType</i>    </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;+=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 computed assignment addition       </td><td align='left'  valign='top'>

	unspecified          </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;-=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 computed assignment subtraction    </td><td align='left'  valign='top'>

	unspecified          </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;*=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 computed assignment multiplication </td><td align='left'  valign='top'>

	unspecified          </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;/=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>  </td><td align='left'  valign='top'>
 computed assignment division       </td><td align='left'  valign='top'>

	unspecified      
</td></tr>
</table>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="numerictype.cpp.xml" target="_top"><span style='white-space: nowrap'>NumericType.cpp</span></a>

contains an example and test of using numeric types.
It returns true if it succeeds and false otherwise.
(It is easy to modify to test additional numeric types.)

<br/>
<br/>
<b><big><a name="Exercise" id="Exercise">Exercise</a></big></b>



<ol type="1"><li>
List three operators that are not supported by every
numeric type but that are supported by the numeric types
<code><font color="blue">int</font></code>, 
<code><font color="blue">float</font></code>, 
<code><font color="blue">double</font></code>.

</li><li>

Which of the following are numeric types:
<code><font color="blue">std::complex&lt;double&gt;</font></code>,
<code><font color="blue">std::valarray&lt;double&gt;</font></code>,
<code><font color="blue">std::vector&lt;double&gt;</font></code> ?

</li></ol>



<hr/>Input File: omh/numeric_type.omh

</body>
</html>
