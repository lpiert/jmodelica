/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Base class for tags used in constant evaluation of external functions
 */
public abstract class ExternalCEvalTag extends AbstractTag {

    protected FClass fClass;
    protected FExternalStmt ext;

    /**
     * Default constructor.
     * 
     * @param name Tag name.
     * @param myGenerator The tag's generator.
     * @param fClass An FClass object used as a basis in the code generation.
     */
    public ExternalCEvalTag(String name, AbstractGenerator myGenerator, FClass fc) {
        super(name, myGenerator);
        this.fClass = fc;
        this.ext = null;
    }
    
    public void setExt(FExternalStmt ext) {
        this.ext = ext;
    }
    
    protected  void instantiateTags(Map<String,AbstractTag> map) {
        instantiateTags(getClass(), map, this, myGenerator, fClass);
    }
    
    public void generate(CodeStream genPrinter) {
        if (ext == null) {
            for (FExternalStmt ext : fClass.myExternals()) {
                if (ext.canEvaluate() && !ext.isConstructorStmt() && !ext.isDestructorStmt())
                    generate(genPrinter, ext);
            }
        } else {
            generate(genPrinter, ext);
        }
    }
    
    public abstract void generate(CodeStream genPrinter, FExternalStmt ext);
}

