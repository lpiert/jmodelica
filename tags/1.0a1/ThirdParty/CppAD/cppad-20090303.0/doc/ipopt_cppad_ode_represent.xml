<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>ipopt_cppad_nlp ODE Problem Representation</title>
<meta name="description" id="description" content="ipopt_cppad_nlp ODE Problem Representation"/>
<meta name="keywords" id="keywords" content=" representation ipopt_cppad_nlp ode "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_ipopt_cppad_ode_represent_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="ipopt_cppad_ode_simulate.xml" target="_top">Prev</a>
</td><td><a href="ipopt_cppad_ode.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Example</option>
<option>General</option>
<option>ipopt_cppad_nlp</option>
<option>ipopt_cppad_ode</option>
<option>ipopt_cppad_ode_represent</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>General-&gt;</option>
<option>ipopt_cppad_nlp</option>
<option>Interface2C.cpp</option>
<option>JacMinorDet.cpp</option>
<option>JacLuDet.cpp</option>
<option>HesMinorDet.cpp</option>
<option>HesLuDet.cpp</option>
<option>OdeStiff.cpp</option>
<option>ode_taylor.cpp</option>
<option>ode_taylor_adolc.cpp</option>
<option>StackMachine.cpp</option>
<option>mul_level</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ipopt_cppad_nlp-&gt;</option>
<option>ipopt_cppad_windows</option>
<option>ipopt_cppad_simple.cpp</option>
<option>ipopt_cppad_ode</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>ipopt_cppad_ode-&gt;</option>
<option>ipopt_cppad_ode_forward</option>
<option>ipopt_cppad_ode_inverse</option>
<option>ipopt_cppad_ode_simulate</option>
<option>ipopt_cppad_ode_represent</option>
<option>ipopt_cppad_ode.cpp</option>
</select>
</td>
<td>ipopt_cppad_ode_represent</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Purpose</option>
<option>Trapezoidal Time Grid</option>
<option>Argument Vector</option>
<option>Objective</option>
<option>Initial Condition</option>
<option>Trapezoidal Approximation</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>ipopt_cppad_nlp ODE Problem Representation</big></big></b></center>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
In this section we represent the objective and constraint functions,
(in the simultaneous forward and reverse optimization problem)
in terms of much simpler functions that are faster to differentiate
(either by hand coding or by using AD).

<br/>
<br/>
<b><big><a name="Trapezoidal Time Grid" id="Trapezoidal Time Grid">Trapezoidal Time Grid</a></big></b>
<br/>
The discrete time grid, used for the trapezoidal approximation, is 
denote by 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">{</mo>
<msub><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>M</mi>
</msub>
<mo stretchy="false">}</mo>
</mrow></math>

. 
For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>1</mn>
<mo stretchy="false">,</mo>
<mn>2</mn>
<mo stretchy="false">,</mo>
<mn>3</mn>
<mo stretchy="false">,</mo>
<mn>4</mn>
</mrow></math>

,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>ns</mi>
</mrow></math>

, we define

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<msub><mi mathvariant='normal'>&#x00394;</mi>
<mi mathvariant='italic'>k</mi>
</msub>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>s</mi>
<mi mathvariant='italic'>k</mi>
</msub>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>s</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mn>-1</mn>
</mrow>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">/</mo>
<mi mathvariant='italic'>ns</mi>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>t</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mn>-1</mn>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow>
</msub>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>s</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mn>-1</mn>
</mrow>
</msub>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='normal'>&#x00394;</mi>
<mi mathvariant='italic'>k</mi>
</msub>
<mo stretchy="false">*</mo>
<mo stretchy="false">&#x02113;</mo>
</mtd></mtr></mtable>
</mrow></math>

Note that for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">=</mo>
<mn>1</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mn>4</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mi mathvariant='italic'>M</mi>
</msup>
</mrow></math>

 denotes our approximation for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>M</mi>
</msub>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>t</mi>
<mn>0</mn>
</msub>
</mrow></math>

 is equal to 0,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>t</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msub>
</mrow></math>

 is equal to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>s</mi>
<mi mathvariant='italic'>k</mi>
</msub>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="Argument Vector" id="Argument Vector">Argument Vector</a></big></b>
<br/>
The argument vector that we are optimizing with respect to
( 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

 in <a href="ipopt_cppad_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
 )
has the following structure

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">(</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="center" >
<msup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
</msup>
</mtd></mtr><mtr><mtd columnalign="center" >
<mo stretchy="false">&#x022EE;</mo>
</mtd></mtr><mtr><mtd columnalign="center" >
<msup><mi mathvariant='italic'>y</mi>
<mrow><mn>4</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msup>
</mtd></mtr><mtr><mtd columnalign="center" >
<mi mathvariant='italic'>a</mi>
</mtd></mtr></mtable>
</mrow><mo stretchy="true">)</mo></mrow>
</mrow></math>

Note that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mo stretchy="false">(</mo>
<mn>4</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msup>
</mrow></math>

 and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<msup><mi mathvariant='italic'>y</mi>
<mi mathvariant='italic'>M</mi>
</msup>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msub>
<msup><mo stretchy="false">)</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>T</mi>
</mstyle></mrow>
</msup>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>a</mi>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>2</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
</mrow>
</msub>
<msup><mo stretchy="false">)</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>T</mi>
</mstyle></mrow>
</msup>
</mtd></mtr></mtable>
</mrow></math>

<br/>
<b><big><a name="Objective" id="Objective">Objective</a></big></b>
<br/>
The objective function
( 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>fg</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 in <a href="ipopt_cppad_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
 )
has the following representation,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>fg</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<munderover><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>1</mn>
</mrow>
<mn>4</mn>
</munderover>
<msub><mi mathvariant='italic'>H</mi>
<mi mathvariant='italic'>k</mi>
</msub>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<munderover><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
<mn>3</mn>
</munderover>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">)</mo>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">=</mo>
<msub><mi mathvariant='italic'>H</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msub>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mrow></math>


for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mn>1</mn>
<mo stretchy="false">,</mo>
<mn>2</mn>
<mo stretchy="false">,</mo>
<mn>3</mn>
</mrow></math>

.
The range index (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>fg</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 )
corresponding to each term in the objective is 0.
The domain components (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

)
corresponding to the <i>k</i>-th term are

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>2</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
</mrow>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>1</mn>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>2</mn>
</msub>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="Initial Condition" id="Initial Condition">Initial Condition</a></big></b>
<br/>
We define the function 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x000D7;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>3</mn>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
</mrow></math>

 for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>4</mn>
</mrow></math>

 by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mrow></math>

. 
Using this notation,
and the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>fg</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 in <a href="ipopt_cppad_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
,
the constraint becomes

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>fg</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msubsup><mi mathvariant='italic'>r</mi>
<mn>0</mn>
<mn>4</mn>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mn>4</mn>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msubsup><mi mathvariant='italic'>r</mi>
<mn>0</mn>
<mn>4</mn>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>fg</mi>
<mn>2</mn>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msubsup><mi mathvariant='italic'>r</mi>
<mn>1</mn>
<mn>4</mn>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mn>4</mn>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msubsup><mi mathvariant='italic'>r</mi>
<mn>1</mn>
<mn>4</mn>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

The range indices (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>fg</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 )
corresponding to this constraint are 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">,</mo>
<mn>2</mn>
<mo stretchy="false">)</mo>
</mrow></math>

.
The domain components (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

)
corresponding to this constraint are

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>2</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
</mrow>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mn>4</mn>
<mo stretchy="false">,</mo>
<mn>0</mn>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
<mn>0</mn>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>1</mn>
<mn>0</mn>
</msubsup>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
<mrow><mn>4</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>1</mn>
<mrow><mn>4</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>2</mn>
</msub>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="Trapezoidal Approximation" id="Trapezoidal Approximation">Trapezoidal Approximation</a></big></b>
<br/>
For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>5</mn>
<mo stretchy="false">,</mo>
<mn>6</mn>
<mo stretchy="false">,</mo>
<mn>7</mn>
<mo stretchy="false">,</mo>
<mn>8</mn>
</mrow></math>

,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>ns</mi>
</mrow></math>

,
define 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">-</mo>
<mn>5</mn>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow></math>

.
The corresponding trapezoidal approximation is represented by the constraint

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mn>0</mn>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msup>
<mo stretchy="false">-</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
</mrow>
</msup>
<mo stretchy="false">-</mo>
<mrow><mo stretchy="true">[</mo><mrow><mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
<mo stretchy="false">*</mo>
<mfrac><mrow><msub><mi mathvariant='normal'>&#x00394;</mi>
<mi mathvariant='italic'>k</mi>
</msub>
</mrow>
<mrow><mn>2</mn>
</mrow>
</mfrac>
</mrow></math>

For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>5</mn>
<mo stretchy="false">,</mo>
<mn>6</mn>
<mo stretchy="false">,</mo>
<mn>7</mn>
<mo stretchy="false">,</mo>
<mn>8</mn>
</mrow></math>

, we define the function

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x000D7;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x000D7;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>3</mn>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
</mrow></math>

 by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msup><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>w</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>w</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>w</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">*</mo>
<mfrac><mrow><msub><mi mathvariant='normal'>&#x00394;</mi>
<mi mathvariant='italic'>k</mi>
</msub>
</mrow>
<mrow><mn>2</mn>
</mrow>
</mfrac>
</mrow></math>

Using this notation, 
(and the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>fg</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 in <a href="ipopt_cppad_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
 )
the constraint becomes

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>fg</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>r</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow>
</msup>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msubsup><mi mathvariant='italic'>r</mi>
<mn>0</mn>
<mi mathvariant='italic'>k</mi>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mi mathvariant='italic'>M</mi>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>fg</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
</mrow>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>r</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow>
</msup>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msubsup><mi mathvariant='italic'>r</mi>
<mn>1</mn>
<mi mathvariant='italic'>k</mi>
</msubsup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>y</mi>
<mi mathvariant='italic'>M</mi>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">-</mo>
<mn>5</mn>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">*</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow></math>

.
The range indices (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>fg</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 )
corresponding to this constraint are 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">(</mo>
<mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
<mo stretchy="false">,</mo>
<mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
<mo stretchy="false">)</mo>
</mrow></math>

.
The domain components (in the vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

)
corresponding to this constraint are

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>2</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>2</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>2</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>3</mn>
</mrow>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>x</mi>
<mrow><mn>8</mn>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>ns</mi>
<mo stretchy="false">+</mo>
<mn>4</mn>
</mrow>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>u</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mo stretchy="false">(</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
<mi mathvariant='italic'>M</mi>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>1</mn>
<mi mathvariant='italic'>M</mi>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>0</mn>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msubsup><mi mathvariant='italic'>y</mi>
<mn>1</mn>
<mrow><mi mathvariant='italic'>M</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow>
</msubsup>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">,</mo>
<msub><mi mathvariant='italic'>a</mi>
<mn>2</mn>
</msub>
<mo stretchy="false">)</mo>
</mrow></math>


<hr/>Input File: omh/ipopt_cppad_ode2.omh

</body>
</html>
