<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>The CheckSimpleVector Function: Example and Test</title>
<meta name="description" id="description" content="The CheckSimpleVector Function: Example and Test"/>
<meta name="keywords" id="keywords" content=" Checksimplevector example check Simplevector test "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_checksimplevector.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="checksimplevector.xml" target="_top">Prev</a>
</td><td><a href="nan.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>CheckSimpleVector</option>
<option>CheckSimpleVector.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>CheckSimpleVector-&gt;</option>
<option>CheckSimpleVector.cpp</option>
</select>
</td>
<td>CheckSimpleVector.cpp</td>
<td>Headings</td>
</tr></table><br/>


<center><b><big><big>The CheckSimpleVector Function: Example and Test</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 

# include &lt;cppad/vector.hpp&gt;
# include &lt;cppad/check_simple_vector.hpp&gt;
# include &lt;iostream&gt;


// Chosing a value between 1 and 9 selects a simple vector properity to be 
// omitted and result in an error message being generated 
# define CppADMyVectorOmit 0

// -------------------------------------------------------------------------

// example class used for non-constant elements (different from Scalar)
template &lt;class Scalar&gt;
class MyElement {
private:
	Scalar *element;
public:
	// element constructor
	MyElement(Scalar *e)
	{	element = e; }
	// an example element assignment that returns void
	void operator = (const Scalar &amp;s)
	{	*element = s; }
	// conversion to Scalar
	operator Scalar() const
	{	return *element; }
}; 
	 

// example simple vector class 
template &lt;class Scalar&gt;
class MyVector {
private:
	size_t length;
	Scalar * data;
public:

# if CppADMyVectorOmit != 1
	// type of the elements in the vector
	typedef Scalar value_type;
# endif
# if CppADMyVectorOmit != 2
	// default constructor
	inline MyVector(void) : length(0) , data(0)
	{ }
# endif
# if CppADMyVectorOmit != 3
	// constructor with a specified size
	inline MyVector(size_t n) : length(n)
	{	if( length == 0 )
			data = 0;
		else	data = new Scalar[length]; 
	}
# endif
# if CppADMyVectorOmit != 4
	// copy constructor
	inline MyVector(const MyVector &amp;x) : length(x.length)
	{	size_t i;
		if( length == 0 )
			data = 0;
		else	data = new Scalar[length]; 

		for(i = 0; i &lt; length; i++)
			data[i] = x.data[i];
	}
# endif
# if CppADMyVectorOmit != 4 
# if CppADMyVectorOmit != 7
	// destructor (it is not safe to delete the pointer in cases 4 and 7)
	~MyVector(void)
	{	delete [] data; }
# endif
# endif
# if CppADMyVectorOmit != 5
	// size function
	inline size_t size(void) const
	{	return length; }
# endif
# if CppADMyVectorOmit != 6
	// resize function
	inline void resize(size_t n)
	{	if( length &gt; 0 )
			delete [] data;
		length = n;
		if( length &gt; 0 )
			data = new Scalar[length];
		else	data = 0;
	}
# endif
# if CppADMyVectorOmit != 7
	// assignment operator
	inline MyVector &amp; operator=(const MyVector &amp;x)
	{	size_t i;
		for(i = 0; i &lt; length; i++)
			data[i] = x.data[i];
		return *this;
	}
# endif
# if CppADMyVectorOmit != 8
	// non-constant element access
	MyElement&lt;Scalar&gt; operator[](size_t i)
	{	return data + i; }
# endif
# if CppADMyVectorOmit != 9
	// constant element access
	const Scalar &amp; operator[](size_t i) const
	{	return data[i]; }
# endif
};
// -------------------------------------------------------------------------

/*
Compute r = a * v, where a is a scalar with same type as the elements of 
the Simple Vector v. This routine uses the CheckSimpleVector function to ensure that 
the types agree.
*/ 
namespace { // Empty namespace
	template &lt;class Scalar, class Vector&gt;
	Vector Sscal(const Scalar &amp;a, const Vector &amp;v)
	{
		// invoke CheckSimpleVector function 
		CppAD::CheckSimpleVector&lt;Scalar, Vector&gt;();
	
		size_t n = v.size();
		Vector r(n);
	
		size_t i;
		for(i = 0; i &lt; n; i++)
			r[i] = a * v[i];
	
		return r;
	}
}

bool CheckSimpleVector(void)
{	bool ok  = true;
	using CppAD::vector;

	// --------------------------------------------------------
	// If you change double to float in the next statement,
	// CheckSimpleVector will generate an error message at compile time.
	double a = 3.;
	// --------------------------------------------------------

	size_t n = 2;
	MyVector&lt;double&gt; v(n);
	v[0]     = 1.;
	v[1]     = 2.;
	MyVector&lt;double&gt; r = Sscal(a, v);
	ok      &amp;= (r[0] == 3.);
	ok      &amp;= (r[1] == 6.);

	return ok;
}
</pre>
</font></code>


<hr/>Input File: example/check_simple_vector.cpp

</body>
</html>
