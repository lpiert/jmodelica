<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Jacobian Sparsity Pattern: Forward Mode</title>
<meta name="description" id="description" content="Jacobian Sparsity Pattern: Forward Mode"/>
<meta name="keywords" id="keywords" content=" Forsparsejac forward sparsity Jacobian pattern "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_forsparsejac_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="sparse.xml" target="_top">Prev</a>
</td><td><a href="forsparsejac.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>FunEval</option>
<option>Sparse</option>
<option>ForSparseJac</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>FunEval-&gt;</option>
<option>Forward</option>
<option>Reverse</option>
<option>Sparse</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Sparse-&gt;</option>
<option>ForSparseJac</option>
<option>RevSparseJac</option>
<option>RevSparseHes</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>ForSparseJac-&gt;</option>
<option>ForSparseJac.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>q</option>
<option>r</option>
<option>s</option>
<option>Vector</option>
<option>Entire Sparsity Pattern</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>Jacobian Sparsity Pattern: Forward Mode</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>s</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.ForSparseJac(</span></font></code><i><span style='white-space: nowrap'>q</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>r</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>
<code><span style='white-space: nowrap'><br/>
</span></code><br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 to denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>
 corresponding to <i>f</i>.
For a fixed 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>q</mi>
</mrow></math>

 matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
</mrow></math>

,
the Jacobian of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">]</mo>
</mrow></math>


with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>u</mi>
</mrow></math>

 at 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

 is

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>R</mi>
</mrow></math>

Given a
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
</mrow></math>

,
<code><font color="blue">ForSparseJac</font></code> returns a sparsity pattern for the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>Note that the <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i> is not <code><font color="blue">const</font></code>.
After this the sparsity pattern
for each of the variables in the operation sequence
is stored in the object <i>f</i>.


<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
If no <a href="vecad.xml" target="_top"><span style='white-space: nowrap'>VecAD</span></a>
 objects are used by the
AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
 
stored in <i>f</i>,
the sparsity pattern is valid for all values 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>If <a href="seqproperty.xml#use_VecAD" target="_top"><span style='white-space: nowrap'>f.useVecAD</span></a>
 is true,
the sparsity patter is only valid for the value of <i>x</i>
in the previous <a href="forwardzero.xml" target="_top"><span style='white-space: nowrap'>zero&#xA0;order&#xA0;forward&#xA0;mode</span></a>
 call
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Forward(0,&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>If there is no previous zero order forward mode call using <i>f</i>,
the value of the <a href="independent.xml" target="_top"><span style='white-space: nowrap'>independent</span></a>
 variables 
during the recording of the AD sequence of operations is used
for <i>x</i>.


<br/>
<br/>
<b><big><a name="q" id="q">q</a></big></b>
<br/>
The argument <i>q</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>q</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>It specifies the number of columns in the Jacobian 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

. 
Note that the memory required for the calculation is proportional 
to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>q</mi>
</mrow></math>

 times the total number of variables 
in the AD operation sequence corresponding to <i>f</i>
(<a href="seqproperty.xml#size_var" target="_top"><span style='white-space: nowrap'>f.size_var</span></a>
).
Smaller values for <i>q</i> can be used to
break the sparsity calculation 
into groups that do not require to much memory. 

<br/>
<br/>
<b><big><a name="r" id="r">r</a></big></b>
<br/>
The argument <i>r</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>r</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="forsparsejac.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
</mrow></math>

.
It specifies a 
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for the matrix <i>R</i> as follows:
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>q</mi>
<mn>-1</mn>
</mrow></math>

.

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>R</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
<mo stretchy="false">;</mo>
<mo stretchy="false">&#x021D2;</mo>
<mspace width='.3em'/>
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>true</mi>
</mstyle></mrow>
</mrow></math>

<br/>
<b><big><a name="s" id="s">s</a></big></b>
<br/>
The return value <i>s</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>s</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="forsparsejac.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
</mrow></math>

.
It specifies a 
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for the matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 as follows:
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>q</mi>
<mn>-1</mn>
</mrow></math>



<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<msub><mo stretchy="false">)</mo>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
<mo stretchy="false">;</mo>
<mo stretchy="false">&#x021D2;</mo>
<mspace width='.3em'/>
<mi mathvariant='italic'>s</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>true</mi>
</mstyle></mrow>
</mrow></math>

<br/>
<b><big><a name="Vector" id="Vector">Vector</a></big></b>
<br/>
The type <i>Vector</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;bool</span></a>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.
In order to save memory, 
you may want to use a class that packs multiple elements into one
storage location; for example,
<a href="cppad_vector.xml#vectorBool" target="_top"><span style='white-space: nowrap'>vectorBool</span></a>
.

<br/>
<br/>
<b><big><a name="Entire Sparsity Pattern" id="Entire Sparsity Pattern">Entire Sparsity Pattern</a></big></b>
<br/>
Suppose that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>q</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
</mrow></math>

 is the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 identity matrix,
If follows that 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">{</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>true</mi>
</mstyle></mrow>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>if</mi>
</mstyle></mrow>
<mspace width='.3em'/>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>j</mi>
</mtd></mtr><mtr><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>flase</mi>
</mstyle></mrow>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>otherwise</mi>
</mstyle></mrow>
</mtd></mtr></mtable>
</mrow><mo stretchy="true"> </mo></mrow>
</mrow></math>

is an efficient sparsity pattern for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
</mrow></math>

; 
i.e., the choice for <i>r</i> has as few true values as possible.
In this case, 
the corresponding value for <i>s</i> is a 
sparsity pattern for the Jacobian 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="forsparsejac.cpp.xml" target="_top"><span style='white-space: nowrap'>ForSparseJac.cpp</span></a>

contains an example and test of this operation.
It returns true if it succeeds and false otherwise.


<hr/>Input File: cppad/local/for_sparse_jac.hpp

</body>
</html>
