<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Enable use of AD&lt;Base&gt; where Base is std::complex&lt;double&gt;</title>
<meta name="description" id="description" content="Enable use of AD&lt;Base&gt; where Base is std::complex&lt;double&gt;"/>
<meta name="keywords" id="keywords" content=" complex double Base "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_base_complex.hpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="base_require.xml" target="_top">Prev</a>
</td><td><a href="complexpoly.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>base_require</option>
<option>base_complex.hpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>base_require-&gt;</option>
<option>base_complex.hpp</option>
<option>base_adolc.hpp</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>base_complex.hpp-&gt;</option>
<option>ComplexPoly.cpp</option>
<option>not_complex_ad.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Example</option>
<option>See Also</option>
<option>Include File</option>
<option>CondExpOp</option>
<option>EqualOpSeq</option>
<option>Identical</option>
<option>Ordered</option>
<option>Integer</option>
<option>Standard Functions</option>
<option>---..Valid Complex Functions</option>
<option>---..Invalid Complex Functions</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>Enable use of AD&lt;Base&gt; where Base is std::complex&lt;double&gt;</big></big></b></center>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>
<br/>
The file <a href="complexpoly.cpp.xml" target="_top"><span style='white-space: nowrap'>ComplexPoly.cpp</span></a>
 contains an example use of
<code><font color="blue">std::complex&lt;double&gt;</font></code> type for a CppAD <i>Base</i> type.
It returns true if it succeeds and false otherwise.

<br/>
<br/>
<b><big><a name="See Also" id="See Also">See Also</a></big></b>
<br/>
The file <a href="not_complex_ad.cpp.xml" target="_top"><span style='white-space: nowrap'>not_complex_ad.cpp</span></a>
 contains an example using
complex arithmetic where the function is not complex differentiable.

<br/>
<br/>
<b><big><a name="Include File" id="Include File">Include File</a></big></b>
<br/>
This file is included before <code><font color="blue">&lt;cppad/cppad.hpp&gt;</font></code>
so it is necessary to define the error handler
in addition to including
<a href="base_require.xml#declare.hpp" target="_top"><span style='white-space: nowrap'>declare.hpp</span></a>

<code><font color='blue'><pre style='display:inline'> 
# include &lt;complex&gt;
# include &lt;cppad/declare.hpp&gt;
# include &lt;cppad/error_handler.hpp&gt;
</pre></font></code>


<br/>
<br/>
<b><big><a name="CondExpOp" id="CondExpOp">CondExpOp</a></big></b>
<br/>
The conditional expressions <a href="condexp.xml" target="_top"><span style='white-space: nowrap'>CondExp</span></a>
 
requires ordered comparisons (e.g., <code><font color="blue">&lt;</font></code>)
and the C++ standard complex types do not allow for ordered comparisons.
Thus, we make it an error to use the conditional comparisons 
with complex types:
<code><font color='blue'><pre style='display:inline'> 
namespace CppAD {
	inline std::complex&lt;double&gt; CondExpOp(
		enum CppAD::CompareOp      cop        ,
		const std::complex&lt;double&gt; &amp;left      ,
		const std::complex&lt;double&gt; &amp;right     ,
		const std::complex&lt;double&gt; &amp;trueCase  ,
		const std::complex&lt;double&gt; &amp;falseCase )
	{	CppAD::ErrorHandler::Call(
			true     , __LINE__ , __FILE__ ,
			&quot;std::complex&lt;float&gt; CondExpOp(...)&quot;,
			&quot;Error: cannot use CondExp with a complex type&quot;
		);
		return std::complex&lt;double&gt;(0);
	}
}
</pre></font></code>


<br/>
<br/>
<b><big><a name="EqualOpSeq" id="EqualOpSeq">EqualOpSeq</a></big></b>
<br/>
Complex numbers do not carry operation sequence information. 
Thus they are equal in this sense if and only if there values are equal.  
<code><font color='blue'><pre style='display:inline'> 
namespace CppAD {
	inline bool EqualOpSeq(
		const std::complex&lt;double&gt; &amp;x , 
		const std::complex&lt;double&gt; &amp;y )
	{	return x == y; 
	}
}
</pre></font></code>


<br/>
<br/>
<b><big><a name="Identical" id="Identical">Identical</a></big></b>
<br/>
Complex numbers do not carry operation sequence information. 
Thus they are all parameters so the identical functions just check values.
<code><font color='blue'><pre style='display:inline'> 
namespace CppAD {
	inline bool IdenticalPar(const std::complex&lt;double&gt; &amp;x)
	{	return true; }
	inline bool IdenticalZero(const std::complex&lt;double&gt; &amp;x)
	{	return (x == std::complex&lt;double&gt;(0., 0.) ); }
	inline bool IdenticalOne(const std::complex&lt;double&gt; &amp;x)
	{	return (x == std::complex&lt;double&gt;(1., 0.) ); }
	inline bool IdenticalEqualPar(
		const std::complex&lt;double&gt; &amp;x, const std::complex&lt;double&gt; &amp;y)
	{	return (x == y); }
}
</pre></font></code>


<br/>
<br/>
<b><big><a name="Ordered" id="Ordered">Ordered</a></big></b>


<code><font color='blue'><pre style='display:inline'> 
namespace CppAD {
	inline bool GreaterThanZero(const std::complex&lt;double&gt; &amp;x)
	{	CppAD::ErrorHandler::Call(
			true     , __LINE__ , __FILE__ ,
			&quot;GreaterThanZero(x)&quot;,
			&quot;Error: cannot use GreaterThanZero with complex&quot;
		);
		return false;
	}
	inline bool GreaterThanOrZero(const std::complex&lt;double&gt; &amp;x)
	{	CppAD::ErrorHandler::Call(
			true     , __LINE__ , __FILE__ ,
			&quot;GreaterThanZero(x)&quot;,
			&quot;Error: cannot use GreaterThanZero with complex&quot;
		);
		return false;
	}
	inline bool LessThanZero(const std::complex&lt;double&gt; &amp;x)
	{	CppAD::ErrorHandler::Call(
			true     , __LINE__ , __FILE__ ,
			&quot;LessThanZero(x)&quot;,
			&quot;Error: cannot use LessThanZero with complex&quot;
		);
		return false;
	}
	inline bool LessThanOrZero(const std::complex&lt;double&gt; &amp;x)
	{	CppAD::ErrorHandler::Call(
			true     , __LINE__ , __FILE__ ,
			&quot;LessThanZero(x)&quot;,
			&quot;Error: cannot use LessThanZero with complex&quot;
		);
		return false;
	}
}
</pre></font></code>


<br/>
<br/>
<b><big><a name="Integer" id="Integer">Integer</a></big></b>
<br/>
The implementation of this function must agree
with the CppAD user specifications for complex arguments to the
<a href="integer.xml#x.Complex Types" target="_top"><span style='white-space: nowrap'>Integer</span></a>
 function:
<code><font color='blue'><pre style='display:inline'> 
namespace CppAD {
	inline int Integer(const std::complex&lt;double&gt; &amp;x)
	{	return static_cast&lt;int&gt;( x.real() ); }
}
</pre></font></code>


<br/>
<br/>
<b><big><a name="Standard Functions" id="Standard Functions">Standard Functions</a></big></b>


<br/>
<br/>
<b><a name="Standard Functions.Valid Complex Functions" id="Standard Functions.Valid Complex Functions">Valid Complex Functions</a></b>
<br/>
The following standard math functions,
that are required by <a href="base_require.xml" target="_top"><span style='white-space: nowrap'>base_require</span></a>
,
are defined by 
<code><font color="blue">std::complex</font></code>:
<code><font color="blue">cos</font></code>,
<code><font color="blue">cosh</font></code>,
<code><font color="blue">exp</font></code>,
<code><font color="blue">log</font></code>,
<code><font color="blue">pow</font></code>,
<code><font color="blue">sin</font></code>,
<code><font color="blue">sinh</font></code>,
<code><font color="blue">sqrt</font></code>.
<code><font color='blue'><pre style='display:inline'> 
# define CPPAD_USER_MACRO(function)                                   \
inline std::complex&lt;double&gt; function(const std::complex&lt;double&gt; &amp;x)   \
{	return std::function(x); }

namespace CppAD {
	CPPAD_USER_MACRO(cos)
	CPPAD_USER_MACRO(cosh)
	CPPAD_USER_MACRO(exp)
	CPPAD_USER_MACRO(log)
	inline std::complex&lt;double&gt; pow(
		const std::complex&lt;double&gt; &amp;x , 
		const std::complex&lt;double&gt; &amp;y )
	{	return std::pow(x, y); }
	CPPAD_USER_MACRO(sin)
	CPPAD_USER_MACRO(sinh)
	CPPAD_USER_MACRO(sqrt)
}
# undef CPPAD_USER_MACRO
</pre></font></code>


<br/>
<br/>
<b><a name="Standard Functions.Invalid Complex Functions" id="Standard Functions.Invalid Complex Functions">Invalid Complex Functions</a></b>
<br/>
The other standard math functions,
(and <code><font color="blue">abs</font></code>) required by <a href="base_require.xml" target="_top"><span style='white-space: nowrap'>base_require</span></a>

are not defined for complex types
(see <a href="abs.xml#Complex Types" target="_top"><span style='white-space: nowrap'>abs</span></a>
).
Hence we make it an error to use them.
(Note that the standard math functions are not defined in the CppAD namespace.)
<code><font color='blue'><pre style='display:inline'> 
# define CPPAD_USER_MACRO(function)                                          \
inline std::complex&lt;double&gt; function(const std::complex&lt;double&gt; &amp;x)          \
{      CppAD::ErrorHandler::Call(                                            \
               true     , __LINE__ , __FILE__ ,                              \
               &quot;std::complex&lt;double&gt;&quot;,                                       \
               &quot;Error: cannot use &quot; #function &quot; with complex&lt;double&gt; &quot;       \
       );                                                                    \
       return std::complex&lt;double&gt;(0);                                       \
}

namespace CppAD {
	CPPAD_USER_MACRO(acos)
	CPPAD_USER_MACRO(asin)
	CPPAD_USER_MACRO(atan)
}
# undef CPPAD_USER_MACRO
</pre></font></code>


<hr/>Input File: cppad/local/base_complex.hpp

</body>
</html>
