<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>An Important Reverse Mode Identity</title>
<meta name="description" id="description" content="An Important Reverse Mode Identity"/>
<meta name="keywords" id="keywords" content=" "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_reverse_identity_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="acosreverse.xml" target="_top">Prev</a>
</td><td><a href="glossary.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>Theory</option>
<option>reverse_identity</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Theory-&gt;</option>
<option>ForwardTheory</option>
<option>ReverseTheory</option>
<option>reverse_identity</option>
</select>
</td>
<td>reverse_identity</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Notation</option>
<option>Reverse Sweep</option>
<option>Theorem</option>
<option>Proof</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>An Important Reverse Mode Identity</big></big></b></center>
The theorem and the proof below is a restatement
of the results on page 236 of
<a href="bib.xml#Evaluating Derivatives" target="_top"><span style='white-space: nowrap'>Evaluating&#xA0;Derivatives</span></a>
.

<br/>
<br/>
<b><big><a name="Notation" id="Notation">Notation</a></big></b>
<br/>
Given a function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>v</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>


we use the notation

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>f</mi>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>u</mi>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>v</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">[</mo><mrow><mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>f</mi>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>u</mi>
<mn>1</mn>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>v</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">,</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>f</mi>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>u</mi>
<mi mathvariant='italic'>n</mi>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>v</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
</mrow></math>

<br/>
<b><big><a name="Reverse Sweep" id="Reverse Sweep">Reverse Sweep</a></big></b>
<br/>
When using <a href="reverse_any.xml" target="_top"><span style='white-space: nowrap'>reverse&#xA0;mode</span></a>

we are given a function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

,
a matrix of Taylor coefficients 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
</mrow></math>

,
and a weight vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>w</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

.
We define the functions 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x000D7;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>W</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x000D7;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<mi mathvariant='italic'>B</mi>
</mrow></math>

, and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>W</mi>
<mi mathvariant='italic'>j</mi>
</msub>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<mi mathvariant='italic'>B</mi>
</mrow></math>

 by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<msup><mi mathvariant='italic'>t</mi>
<mrow><mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow>
</msup>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>W</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>w</mi>
<mn>0</mn>
</msub>
<msub><mi mathvariant='italic'>F</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='italic'>w</mi>
<mrow><mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow>
</msub>
<msub><mi mathvariant='italic'>F</mi>
<mrow><mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow>
</msub>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mi mathvariant='italic'>W</mi>
<mi mathvariant='italic'>j</mi>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mi mathvariant='italic'>W</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 is the <i>j</i>-th column of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
</mrow></math>

.
The theorem below implies that

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>W</mi>
<mi mathvariant='italic'>j</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>W</mi>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

A <a href="reverse_any.xml" target="_top"><span style='white-space: nowrap'>general&#xA0;reverse&#xA0;sweep</span></a>
 calculates the values

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>W</mi>
<mrow><mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mspace width='1cm'/>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
<mo stretchy="false">)</mo>
</mrow></math>

But the return values for a reverse sweep are specified
in terms of the more useful values

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>W</mi>
<mi mathvariant='italic'>j</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mspace width='1cm'/>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="Theorem" id="Theorem">Theorem</a></big></b>
<br/>
Suppose that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 is a 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
</mrow></math>

 times
continuously differentiable function.
Define the functions 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x000D7;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x000D7;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

,
and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<msup><mi mathvariant='italic'>t</mi>
<mrow><mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow>
</msup>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 denotes the <i>j</i>-th column of 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>p</mi>
</mrow>
</msup>
</mrow></math>

.
It follows that
for all 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow></math>

 such that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">&#x02264;</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">&lt;</mo>
<mi mathvariant='italic'>p</mi>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

<br/>
<b><big><a name="Proof" id="Proof">Proof</a></big></b>
<br/>
If follows from the definitions that

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<msub><mrow><mo stretchy="true">[</mo><mrow><mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
<mrow><mi mathvariant='italic'>t</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
</msub>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<msub><mrow><mo stretchy="true">[</mo><mrow><mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
<mrow><mi mathvariant='italic'>t</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
</msub>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<msub><mrow><mo stretchy="true">{</mo><mrow><mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mrow><mo stretchy="true">[</mo><mrow><msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>i</mi>
</msup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
</mrow><mo stretchy="true">}</mo></mrow>
<mrow><mi mathvariant='italic'>t</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
</msub>
</mtd></mtr></mtable>
</mrow></math>

For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">&gt;</mo>
<mi mathvariant='italic'>i</mi>
</mrow></math>

, the <i>k</i>-th 
partial of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>i</mi>
</msup>
</mrow></math>

 with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>t</mi>
</mrow></math>

 is zero.
Thus, the partial with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>t</mi>
</mrow></math>

 is given by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mrow><mo stretchy="true">[</mo><mrow><msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>i</mi>
</msup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<munderover><mo displaystyle='true' largeop='true'>&#x02211;</mo>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
<mi mathvariant='italic'>i</mi>
</munderover>
<mrow><mo stretchy="true">(</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="center" >
<mi mathvariant='italic'>j</mi>
</mtd></mtr><mtr><mtd columnalign="center" >
<mi mathvariant='italic'>k</mi>
</mtd></mtr></mtable>
</mrow><mo stretchy="true">)</mo></mrow>
<mfrac><mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">!</mo>
</mrow>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<msup><mi mathvariant='italic'>t</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>k</mi>
</mrow>
</msup>
<mspace width='.3em'/>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>k</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>k</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mrow><mo stretchy="true">{</mo><mrow><mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mrow><mo stretchy="true">[</mo><mrow><msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>i</mi>
</msup>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow><mo stretchy="true">]</mo></mrow>
</mrow><mo stretchy="true">}</mo></mrow>
<mrow><mi mathvariant='italic'>t</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow>
</msub>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mrow><mo stretchy="true">(</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="center" >
<mi mathvariant='italic'>j</mi>
</mtd></mtr><mtr><mtd columnalign="center" >
<mi mathvariant='italic'>i</mi>
</mtd></mtr></mtable>
</mrow><mo stretchy="true">)</mo></mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">!</mo>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">!</mo>
</mrow>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd></mtr></mtable>
</mrow></math>

Applying this formula to the case where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
</mrow></math>


is replaced by 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
</mrow></math>

 is replaced by zero,
we obtain

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>j</mi>
<mo stretchy="false">-</mo>
<mi mathvariant='italic'>i</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02218;</mo>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

which completes the proof


<hr/>Input File: omh/reverse_identity.omh

</body>
</html>
