/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.util.ArrayList;
import org.eclipse.jface.text.Position;
import org.jmodelica.ide.folding.FilePosition;
import org.jmodelica.ide.folding.FileCharacterPosition;

aspect ModelicaFolding {
	
	protected ArrayList<Position> Annotation.makeFoldingPosition() {
		int start = getBeginOffset() + 1;
		int end = getEndOffset();
		int length = end - start + 1;
		ArrayList<Position> list = new ArrayList<Position>(1);
    	if (start > 0 && length > 0) 
	    	list.add(new FileCharacterPosition(start, length, fileName()));
    	return list;
    }

//    eq FullClassDecl.hasFolding() = true;
    eq Annotation.hasFolding() = true;
    eq Annotation.foldingPositions(IDocument document) = makeFoldingPosition();

    /*
     * We need to replace foldingPositions(), since we don't want any rewrites
     * (copied and altered from folding aspect in JastAdd IDE plugin).
     */
    refine Folding eq ASTNode.foldingPositions(IDocument document) {
        ArrayList list = new ArrayList();
        if (hasFolding()) {
            try {
                int lineStart = ASTNode.getLine(getStart());
                int lineEnd = ASTNode.getLine(getEnd());
                int nbrOfLines = document.getNumberOfLines();
                int startOffset = document.getLineOffset(lineStart > 1 ? lineStart - 1 : 0);
                int endOffset = document.getLineOffset(lineEnd < nbrOfLines ? lineEnd : lineEnd - 1);
                int foldLength = endOffset - startOffset;
                if ((lineEnd - lineStart) > 0) {
                    list.add(new Position(startOffset, foldLength));
                }
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < getNumChildNoTransform(); i++) {
            list.addAll(getChildNoTransform(i).foldingPositions(document));
        }
        return list;
    }

    /*
     * Since all children isn't always returned by getChild(), 
     * ClassDecl needs new foldingPositions() (copied and altered from folding 
     * aspect in JastAdd IDE plugin).
     */
	eq ClassDecl.foldingPositions(IDocument document) {
		ArrayList list = new ArrayList();
		if (hasFolding()) {
			try {
				int lineStart = ASTNode.getLine(getStart());
				int lineEnd = ASTNode.getLine(getEnd());
				int nbrOfLines = document.getNumberOfLines();
				int startOffset = document.getLineOffset(lineStart > 1 ? lineStart - 1 : 0);
				int endOffset = document.getLineOffset(lineEnd < nbrOfLines ? lineEnd : lineEnd - 1);
				int foldLength = endOffset - startOffset;
				if ((lineEnd - lineStart) > 0) {
					list.add(new FilePosition(startOffset, foldLength, fileName()));
				}
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < getNumChildNoTransform(); i++) {
			if (!(getChildNoTransform(i) instanceof ClassDecl))
				list.addAll(getChildNoTransform(i).foldingPositions(document));
		}
		for (ClassDecl decl : classes())
			list.addAll(decl.foldingPositions(document));
		return list;
	}

	/* 
	 * Don't traverse entire tree. We should avoid error nodes and nodes that can't contain folds anyway.
	 */
	protected static ArrayList ASTNode.EMPTY_LIST = new ArrayList();
	eq BadDefinition.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadClassDecl.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadElement.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadArgument.foldingPositions(IDocument document) = EMPTY_LIST;
	eq AbstractExp.foldingPositions(IDocument document) = EMPTY_LIST;
	eq Access.foldingPositions(IDocument document) = EMPTY_LIST;
	eq Equation.foldingPositions(IDocument document) = EMPTY_LIST;
    
}