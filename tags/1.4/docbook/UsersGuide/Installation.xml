<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Installation</title>

  <section xml:id="inst_sec_supported_platforms">
    <title>Supported platforms</title>

    <para>JModelica.org can be installed on Linux, Mac OS X, and Windows (XP,
    Vista, 7) with 32-bit or 64-bit architectures. Most development work is
    carried out on 32-bit Mac OS X , 32 and 64-bit Linux and 32-bit Windows
    XP, so these platforms tend to be best tested.</para>
  </section>

  <section xml:id="inst_sec_prerequisites">
    <title>Prerequisites</title>

    <para>Make sure to install the required software components listed in this
    section before installing JModelica.org.</para>

    <section>
      <title>Java</title>

      <para>It is required to have a Java Runtime Environment (JRE) version 6
      installed on your computer.<task>
          <title>Install a JRE</title>

          <procedure>
            <step>
              <para>Get a JRE installer suitable for your platform <link
              xlink:href="http://www.java.com/en/download/index.jsp">here</link>.</para>
            </step>

            <step>
              <para>Run the installer.</para>
            </step>
          </procedure>
        </task></para>
    </section>

    <section xml:id="inst_sec_prereq_python">
      <title>Python</title>

      <para><link xlink:href="http://www.python.org/download/releases/">Python
      2.6</link> with the following additional packages are required:</para>

      <table>
        <title>Python prerequisites</title>

        <tgroup cols="3">
          <colspec align="left" colname="col–para" colwidth="2*" />

          <colspec align="left" colname="col–def" colwidth="2*" />

          <colspec align="left" colname="col–descr" colwidth="3*" />

          <thead>
            <row>
              <entry align="center">Package</entry>

              <entry align="center">Recommended version</entry>

              <entry align="center">Description</entry>
            </row>
          </thead>

          <tbody>
            <row>
              <entry><link
              xlink:href="http://sourceforge.net/projects/numpy/files/">NumPy</link></entry>

              <entry>1.3.0</entry>

              <entry>The fundamental package needed for scientific computing
              with Python.</entry>
            </row>

            <row>
              <entry><link
              xlink:href="http://sourceforge.net/projects/scipy/files/">SciPy</link></entry>

              <entry>0.7.1</entry>

              <entry>A library of algorithms and mathematical tools for
              Python.</entry>
            </row>

            <row>
              <entry><link
              xlink:href="http://sourceforge.net/projects/matplotlib/files/">matplotlib</link></entry>

              <entry>0.99.1.1</entry>

              <entry>A plotting library for Python, with a MATLAB like
              interface.</entry>
            </row>

            <row>
              <entry><link
              xlink:href="http://ipython.scipy.org/moin/Download">IPython</link></entry>

              <entry>0.10</entry>

              <entry>An interactive shell for Python with additional shell
              syntax, code highlighting, tab completion, string completion,
              and rich history.</entry>
            </row>

            <row>
              <entry><link
              xlink:href="https://launchpad.net/pyreadline/+download">PyReadline</link></entry>

              <entry>1.5</entry>

              <entry>Note: Windows only. A readline for Windows required by
              IPython.</entry>
            </row>
          </tbody>
        </tgroup>
      </table>
    </section>
  </section>

  <section xml:id="inst_sec_binary_dist">
    <title>Binary distribution</title>

    <section>
      <title>Windows</title>

      <para>Pre-built binary distributions for Windows are available in the
      Download section of <link
      xlink:href="www.jmodelica.org">www.jmodelica.org</link>.</para>

      <para>The JModelica.org Windows installer contains a binary distribution
      of JModelica.org built using the JModelica.org-SDK, bundled with
      required third-party software components. The JModelica.org Windows
      installer sets up a pre-configured complete environment with convenient
      start menu shortcuts.</para>

      <section>
        <title>Installing JModelica.org with Windows installer</title>

        <para>Make sure that all prerequisite components described in <xref
        linkend="inst_sec_prerequisites" /> are installed before
        continuing.</para>

        <procedure>
          <step>
            <para>Download a <link
            xlink:href="http://www.jmodelica.org/page/12">JModelica.org
            Windows binary installer</link>.</para>
          </step>

          <step>
            <para>Run the installer and follow the wizard. Choose to install
            the additional Python packages when prompted, unless the are
            already installed.</para>
          </step>
        </procedure>
      </section>
    </section>

    <section>
      <title>Linux</title>

      <para>Currently, no pre-built binary distributions are provided for
      Linux.</para>
    </section>

    <section>
      <title>Mac OS X</title>

      <para>Currently, no pre-built binary distributions are provided for Mac
      OS X.</para>
    </section>
  </section>

  <section xml:id="inst_sec_sdk">
    <title>Software Development Kit</title>

    <para>The JModelica.org Software Development Kit (SDK) is a bundle of
    tools needed to build the JModelica.org sources on Windows. The
    JModelica.org SDK windows installer sets up a complete pre-configured
    environment with convenient start menu shortcuts.</para>

    <section xml:id="inst_sec_sdk_prereq">
      <title>Prerequisites</title>

      <para>The Python prerequisites for installing the JModelica.org SDK are
      the same as listed in <xref linkend="inst_sec_prereq_python" />. For
      Java however it is required to have a JDK installed, not only a JRE as
      for the binary installation.</para>

      <task>
        <title>Install a JDK</title>

        <procedure>
          <step>
            <para>Get a JDK <link
            xlink:href="http://java.sun.com/javase/downloads/">here</link>.</para>
          </step>

          <step>
            <para>Run the installer.</para>
          </step>
        </procedure>
      </task>
    </section>

    <section>
      <title>Installing the JModelica.org SDK</title>

      <para>Make sure that the prerequisites listed in <xref
      linkend="inst_sec_sdk_prereq" /> are installed before starting the
      JModelica.org SDK installation.</para>

      <section>
        <title>Bundled tools</title>

        <para>The following tools are bundled in the JModelica.org SDK.</para>

        <itemizedlist>
          <listitem>
            <para><link xlink:href="http://www.mingw.org">MinGW,
            MSYS</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://www.coin-or.org/Ipopt">Ipopt</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://jpype.sourceforge.net">JPype</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://codespeak.net/lxml">lxml</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://somethingaboutorange.com/mrl/projects/nose/">nose</link></para>
          </listitem>

          <listitem>
            <para><link xlink:href="http://www.open.collab.net/">CollabNet
            Subversion Client</link></para>
          </listitem>

          <listitem>
            <para><link xlink:href="http://ant.apache.org">Apache
            Ant</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="https://computation.llnl.gov/casc/sundials">SUNDIALS</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://www.cython.org/">Cython</link></para>
          </listitem>

          <listitem>
            <para><link
            xlink:href="http://www.docbook.org/">DocBook</link></para>
          </listitem>
        </itemizedlist>
      </section>

      <section>
        <title>Step-by-step instructions</title>

        <para><orderedlist>
            <listitem>
              <para>Download the JModelica.org SDK <link
              xlink:href="http://www.jmodelica.org/sdk">here</link> and save
              the executable file somewhere on your computer.</para>
            </listitem>

            <listitem>
              <para>Run the file by double-clicking and selecting Run if
              prompted with a security warning. This will launch an installer
              which should be self-explanatory. Select "Yes" at the end when
              asked if the source code should be checked out from
              JModelica.org as this is needed in the next step.</para>
            </listitem>

            <listitem>
              <para>Build the sources:<orderedlist>
                  <listitem>
                    <para>Start the msys shell from the JModelica.org start
                    menu.</para>
                  </listitem>

                  <listitem>
                    <para>Configure the sources for build and installation
                    (this will create the subdirectories
                    <filename>build</filename> and
                    <filename>install</filename>) with:<programlisting>cd /JModelica.org-SDK
./configure.sh</programlisting></para>
                  </listitem>

                  <listitem>
                    <para>Compile the sources with <literal>make</literal>
                    from the <filename>build</filename>
                    folder:<programlisting>cd build
make
make install</programlisting></para>
                  </listitem>
                </orderedlist></para>
            </listitem>

            <listitem>
              <para>Test the installation by starting a Python shell from the
              JModelica.org start menu and running an example. Starting the
              Python session from this start menu will set all the environment
              variables required to run the JModelica.org Python
              interface.<programlisting># import the vdp example
from jmodelica.examples import vdp

# run the vdp
vdp.run_demo()</programlisting></para>
            </listitem>
          </orderedlist></para>
      </section>
    </section>
  </section>
</chapter>
