<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Jacobian Sparsity Pattern: Reverse Mode</title>
<meta name="description" id="description" content="Jacobian Sparsity Pattern: Reverse Mode"/>
<meta name="keywords" id="keywords" content=" Revsparsejac reverse sparse Jacobian pattern "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_revsparsejac_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="forsparsejac.cpp.xml" target="_top">Prev</a>
</td><td><a href="revsparsejac.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>FunEval</option>
<option>Sparse</option>
<option>RevSparseJac</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>seq_property</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>optimize</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>FunEval-&gt;</option>
<option>Forward</option>
<option>Reverse</option>
<option>Sparse</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Sparse-&gt;</option>
<option>ForSparseJac</option>
<option>RevSparseJac</option>
<option>RevSparseHes</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>RevSparseJac-&gt;</option>
<option>RevSparseJac.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>p</option>
<option>s</option>
<option>r</option>
<option>VectorSet</option>
<option>Entire Sparsity Pattern</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Jacobian Sparsity Pattern: Reverse Mode</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>

<code><i><font color="black"><span style='white-space: nowrap'>r</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>F</span></font></i><font color="blue"><span style='white-space: nowrap'>.RevSparseJac(</span></font><i><font color="black"><span style='white-space: nowrap'>p</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>s</span></font></i><font color="blue"><span style='white-space: nowrap'>)</span></font></code>

<code><span style='white-space: nowrap'><br/>
</span></code>
<code><i><font color="black"><span style='white-space: nowrap'>r</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>F</span></font></i><font color="blue"><span style='white-space: nowrap'>.RevSparseJac(</span></font><i><font color="black"><span style='white-space: nowrap'>p</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>s</span></font></i><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>packed</span></font></i><font color="blue"><span style='white-space: nowrap'>)</span></font></code>



<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>R</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 to denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>
 corresponding to 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
.
For a fixed 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>S</mi>
</mrow></math>

,
the Jacobian of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>S</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>


with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

 is

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>S</mi>
<mo stretchy="false">*</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

Given a
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>S</mi>
</mrow></math>

,
<code><font color="blue">RevSparseJac</font></code> returns a sparsity pattern for the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object 
<code><i><font color="black"><span style='white-space: nowrap'>f</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font><i><font color="black"><span style='white-space: nowrap'>Base</span></font></i><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>f</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
the sparsity pattern is valid for all values of the independent 
variables in 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>


(even if it has <a href="condexp.xml" target="_top"><span style='white-space: nowrap'>CondExp</span></a>
 or <a href="vecad.xml" target="_top"><span style='white-space: nowrap'>VecAD</span></a>
 operations).

<br/>
<br/>
<b><big><a name="p" id="p">p</a></big></b>
<br/>
The argument 
<code><i><font color="black"><span style='white-space: nowrap'>p</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>p</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
It specifies the number of rows in

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>S</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mrow><mi mathvariant='italic'>p</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow>
</msup>
</mrow></math>

 and the 
Jacobian 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mrow><mi mathvariant='italic'>p</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow>
</msup>
</mrow></math>

. 

<br/>
<br/>
<b><big><a name="s" id="s">s</a></big></b>
<br/>
The argument 
<code><i><font color="black"><span style='white-space: nowrap'>s</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>VectorSet</span></font></i><font color="blue"><span style='white-space: nowrap'>&amp;&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>s</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
(see <a href="revsparsejac.xml#VectorSet" target="_top"><span style='white-space: nowrap'>VectorSet</span></a>
 below).
If it has elements of type <code><font color="blue">bool</font></code>,
its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

.
If it has elements of type <code><font color="blue">std::set&lt;size_t&gt;</font></code>,
its size is 
<code><i><font color="black"><span style='white-space: nowrap'>p</span></font></i></code>
 and all its set elements are between
zero and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>

.
It specifies a 
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for the matrix 
<code><i><font color="black"><span style='white-space: nowrap'>S</span></font></i></code>
.

<br/>
<br/>
<b><big><a name="r" id="r">r</a></big></b>
<br/>
The return value 
<code><i><font color="black"><span style='white-space: nowrap'>r</span></font></i></code>
 has prototype

<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>VectorSet</span></font></i><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font><i><font color="black"><span style='white-space: nowrap'>r</span></font></i><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>
(see <a href="revsparsejac.xml#VectorSet" target="_top"><span style='white-space: nowrap'>VectorSet</span></a>
 below).
If it has elements of type <code><font color="blue">bool</font></code>,
its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.
If it has elements of type <code><font color="blue">std::set&lt;size_t&gt;</font></code>,
its size is 
<code><i><font color="black"><span style='white-space: nowrap'>p</span></font></i></code>
.
It specifies a 
<a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 
for the matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="VectorSet" id="VectorSet">VectorSet</a></big></b>
<br/>
The type 
<code><i><font color="black"><span style='white-space: nowrap'>VectorSet</span></font></i></code>
 must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type</span></a>

<code><font color="blue">bool</font></code> or <code><font color="blue">std::set&lt;size_t&gt;</font></code>;
see <a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
 for a discussion
of the difference.

<br/>
<br/>
<b><big><a name="Entire Sparsity Pattern" id="Entire Sparsity Pattern">Entire Sparsity Pattern</a></big></b>
<br/>
Suppose that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>S</mi>
</mrow></math>

 is the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 identity matrix.
In this case, 
the corresponding value for 
<code><i><font color="black"><span style='white-space: nowrap'>r</span></font></i></code>
 is a 
sparsity pattern for the Jacobian 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>J</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="revsparsejac.cpp.xml" target="_top"><span style='white-space: nowrap'>RevSparseJac.cpp</span></a>

contains an example and test of this operation.
It returns true if it succeeds and false otherwise.


<hr/>Input File: cppad/local/rev_sparse_jac.hpp

</body>
</html>
