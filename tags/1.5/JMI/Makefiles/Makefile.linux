#    Copyright (C) 2009 Modelon AB
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published 
#    by the Free Software Foundation, or optionally, under the terms of the 
#    Common Public License version 1.0 as published by IBM.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License, or the Common Public License, for more details.
#
#    You should have received copies of the GNU General Public License
#    and the Common Public License along with this program.  If not, 
#    see <http://www.gnu.org/licenses/> or 
#    <http://www.ibm.com/developerworks/library/os-cpl.html/> respectively.

#
# NOTICE: This make file is intended to be run in a Linux shell.
#
# Usage:
#   This makefile is intended to be used by the code generated by the
#   JModelica compilers. The following arguments are supported:
#   
#   FILE_NAME             (mandatory) The name of the C file.
#   JMODELICA_HOME        (mandatory) The directory where JModelica has been built,
#                         or equivalently, the directory of a binary JModelica
#                         distribution.
#   CPPAD_HOME            (mandatory) The directory of CppAD.
#                         IPOPT interface is built.
#   IPOPT_HOME            (mandatory if BUILD_WITH_ALGORITHMS=true) An IPOPT build
#                         directory.
#   SUNDIALS_HOME         (mandatory) The directory of Sundials.
#   EXT_LIB_DIRS          List of external library directories.
#   EXT_LIBS              List of external libraries.
#   EXT_INC_DIRS          List of external include directories.
#
# The following targets are supported:
#    model        Build a shared object file containing the generated code
#                 and the low level JMI interface (contained in jmi.a)
#    algorithms   Build a shared object file including the content resulting 
#                 in the target 'basic' and in addition JMI algorithms.
#    ipopt        Build a shared object file including the content resulting 
#                 in the target 'algorithms' and in addition the Ipopt algorithm
#                 interface.
#    model_noad   Build a shared object file containing the generated code,
#                 external libraries and the low level JMI interface (not CppAD).
#        
# Example:
#  make -f Makefile.linux ipopt \
#    FILE_NAME=CCodeGenTests.CCodeGenTest1 \
#    JMODELICA_HOME=/home/jakesson/projects/JModelica/build \
#    CPPAD_HOME=/home/jakesson/packages/cppad-20081116/ \
#    IPOPT_HOME=/home/jakesson/packages/Ipopt/3.5.4/build
#

# Name of shared library
FILE_NAME = 

# JModelica home
JMODELICA_HOME = 

# CPPAD home
CPPAD_HOME = 

# IPOPT home
IPOPT_HOME = 

# SUNDIALS home
SUNDIALS_HOME =

# Additional library directories
EXT_LIB_DIRS =
EXTERNAL_DIRS = $(EXT_LIB_DIRS:%=-L%)

# Additional libraries
EXT_LIBS = 
EXTERNAL_LIBS = $(EXT_LIBS:%=-l%)

# Additional include directories
EXT_INC_DIRS =
EXTERNAL_INCLUDES = $(EXT_INC_DIRS:%=-I%)

# Build with algorithms (requires IPOPT)
BUILD_WITH_ALGORITHMS = false

SHARED = $(FILE_NAME).so

# Object files to be included in the shared library
OBJS = $(FILE_NAME).o

# C++ Compiler command
CXX = g++

# C Compiler command
CC = gcc

# Compile with or without CppAD
JMI_AD=JMI_AD_CPPAD

# C++ Compiler options
CXXFLAGS = -g -O2 -DJMI_AD=${JMI_AD} -fPIC

# C Compiler options
CFLAGS = -g -O2 -std=c89 -pedantic -DJMI_AD=$(JMI_AD) -fPIC

SHARED_LDFLAGS = -shared

# Directories with header files
JMI_INC_DIR = ${JMODELICA_HOME}/include
CPPAD_INC_DIR = $(CPPAD_HOME)
SUNDIALS_INC_DIR = $(SUNDIALS_HOME)/include

# Directory with jmi_cppad.a
JMI_LIB_DIR = ${JMODELICA_HOME}/lib

# Libraries necessary to link with jmi
LIBS_CPPAD = -L$(JMI_LIB_DIR) -ljmi_cppad -lfmi -L$(SUNDIALS_HOME)/lib -lsundials_kinsol -lsundials_nvecserial
LIBS_NOCPPAD = -L${JMI_LIB_DIR} -ljmi -lfmi -lmodelica_utilities -llapack -lblas -lgfortran -L$(SUNDIALS_HOME)/lib -lsundials_kinsol -lsundials_nvecserial
LIBS_IPOPT = -L$(JMI_LIB_DIR) -ljmi_cppad -ljmi_algorithm_cppad -ljmi_solver_cppad -lfmi -L$(IPOPT_HOME)/lib -lipopt -llapack -lblas -lm -lgfortran -lpthread -L$(SUNDIALS_HOME)/lib -lsundials_kinsol -lsundials_nvecserial

# Include paths for compilation
INCL_CPPAD = -I$(JMI_INC_DIR) -I$(CPPAD_INC_DIR) -I$(SUNDIALS_INC_DIR)
INCL_NOCPPAD = -I$(JMI_INC_DIR) $(EXTERNAL_INCLUDES) -I$(SUNDIALS_INC_DIR)

# set variable to correct makefile (for recursive call in model_noad)
MAKEFILE = $(lastword $(MAKEFILE_LIST))

ifeq ($(JMI_AD),JMI_AD_CPPAD)
model: $(OBJS)
	$(CXX) $(SHARED_LDFLAGS) $(CXXLINKFLAGS) $(CXXFLAGS) -o $(SHARED) $(OBJS) $(LIBS_CPPAD)
	$(RM) $(OBJS)
else
model: $(OBJS)
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o $(SHARED) $(OBJS) $(LIBS_NOCPPAD) $(EXTERNAL_DIRS) $(EXTERNAL_LIBS)
	$(RM) $(OBJS)
endif

model_noad: 
	$(MAKE) -f $(MAKEFILE) JMI_AD=JMI_AD_NONE model

algorithms: $(OBJS)
# This is to ensure that the contents of libjmi_algorithm_cppad and
# is included in the resulting shared object file.
ifeq "$(wildcard .jmi_objs )" ""
	mkdir .jmi_objs
endif
	cd .jmi_objs && $(AR) -x $(JMODELICA_HOME)/lib/libjmi_algorithm_cppad.a   
	$(CXX) $(SHARED_LDFLAGS) $(CXXLINKFLAGS) $(CXXFLAGS) -o $(SHARED) $(OBJS) ./.jmi_objs/*.o $(LIBS_CPPAD)
	$(RM) .jmi_objs/*
	rmdir .jmi_objs
	$(RM) $(OBJS)

ipopt: $(OBJS)
# This is to ensure that the contents of libjmi_algorithm_cppad and
# libjmi_solver_cppade is included in the resulting shared object file.
ifeq "$(wildcard .jmi_objs )" ""
	mkdir .jmi_objs
endif
	cd .jmi_objs && $(AR) -x $(JMODELICA_HOME)/lib/libjmi_algorithm_cppad.a  && $(AR) -x $(JMODELICA_HOME)/lib/libjmi_solver_cppad.a  
	$(CXX) $(SHARED_LDFLAGS) $(CXXLINKFLAGS) $(CXXFLAGS) -o $(SHARED) $(OBJS) ./.jmi_objs/*.o $(LIBS_IPOPT)
	$(RM) .jmi_objs/*
	rmdir .jmi_objs
	$(RM) $(OBJS)

# Compile
.cpp.o:
ifeq ($(JMI_AD),JMI_AD_CPPAD)
	$(CXX) $(CXXFLAGS) $(INCL_CPPAD) -c -o $@ $<
else
	$(CXX) $(CXXFLAGS) $(INCL_NOCPPAD) -c -o $@ $<
endif

.c.o:
ifeq ($(JMI_AD),JMI_AD_CPPAD)
	$(CXX) $(CXXFLAGS) $(INCL_CPPAD) -c -o $@ $<
else
	$(CC) $(CFLAGS) $(INCL_NOCPPAD) -c -o $@ $<
endif

clean:
	$(RM) $(SHARED) $(OBJS)
