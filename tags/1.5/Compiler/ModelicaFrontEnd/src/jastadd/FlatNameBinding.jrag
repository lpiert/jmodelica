/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.HashMap;

aspect FlatNameBinding {
	
	inh AbstractFVariable FIdUse.lookupFV(FQName fqn);
	inh AbstractFVariable FIdUse.lookupDerFV(FQName fqn);
	inh AbstractFVariable FExp.lookupFV(FQName fqn);
	inh AbstractFVariable AbstractFVariable.lookupFV(FQName fqn);
	inh AbstractFVariable FRecordDecl.lookupFV(FQName fqn);
	
	syn AbstractFVariable FIdUse.myFV() = lookupFV(getFQName());
	syn lazy AbstractFVariable FIdUseExp.myFV() = getFIdUse().myFV();
	eq FDerExp.myFV() = getFIdUse().lookupDerFV(getFIdUse().getFQName());
    
	eq Root.getChild().lookupFV(FQName fqn)    = null;
	eq Root.getChild().lookupDerFV(FQName fqn) = null;

	eq FClass.getChild(int index).lookupFV(FQName fqn)    = lookupFV(fqn, false);
	eq FClass.getChild(int index).lookupDerFV(FQName fqn) = lookupFV(fqn, true);
	
	public AbstractFVariable FClass.lookupFV(FQName fqn, boolean der) {
		boolean scalar = fqn.scalarized;
		fqn.scalarized = scalarized;
		AbstractFVariable variable = lookupFVFromMap(fullyQualifiedVariablesMap(), fqn, der);
		fqn.scalarized = scalar;
		return (variable == null) ? unknownFVariable() : variable;
	}

	eq FFunctionDecl.getChild(int index).lookupFV(FQName fqn) {
		AbstractFVariable variable = lookupFVFromMap(fullyQualifiedVariablesMap(), fqn, false);
		return (variable == null) ? unknownFVariable() : variable;
	}

	eq FRecordDecl.getChild(int index).lookupFV(FQName fqn) {
		AbstractFVariable variable = lookupFVInRecord(fqn, 0);
		return (variable == null) ? lookupFV(fqn) : variable;
	}
	
	public static AbstractFVariable ASTNode.lookupFVFromMap(
			HashMap<String, ? extends AbstractFVariable> map, FQName fqn, boolean der) {
		AbstractFVariable variable = map.get(der ? fqn.derName() : fqn.name());
		if (variable == null) {
			int i = fqn.getNumFQNamePart();
			do {
				i--;
				String part = fqn.prefixName(i);
				AbstractFVariable tmp = map.get(part);
				if (tmp != null) 
					variable = tmp.lookupFVInRecord(fqn, i);
			} while (variable == null && i > 1);
		}
		return variable;
	}
	
	public AbstractFVariable FRecordDecl.lookupFVInRecord(FQName fqn, int part) {
		AbstractFVariable res = null;
		for (FVariable fv : getFVariables())
			if (fv.name().equals(fqn.getFQNamePart(part).getName()))
				res = fv;
		return res;
	}
	
	public AbstractFVariable AbstractFVariable.lookupFVInRecord(FQName fqn, int part) {
		FRecordDecl rec = myFRecordDecl();
		AbstractFVariable res = (rec != null) ? rec.lookupFVInRecord(fqn, part) : null;
		if (part < fqn.getNumFQNamePart() - 1 && res != null)
			res = res.lookupFVInRecord(fqn, part + 1);
		return res;
	}
	
	inh AbstractFVariable FForClauseE.lookupFV(FQName fqn);
	eq FForClauseE.getChild().lookupFV(FQName fqn) {
		String name = fqn.name();
		for (FForIndex fi : getFForIndexs()) {
			if (fi.getFVariable().name().equals(name)) {
				return fi.getFVariable();
			}
		}
		return lookupFV(fqn);
	}
	
	inh AbstractFVariable FForStmt.lookupFV(FQName fqn);
	eq FForStmt.getChild().lookupFV(FQName fqn) {
		String name = fqn.name();
		FVariable index = getIndex().getFVariable();
		if (index.name().equals(name)) 
			return index;
		return lookupFV(fqn);
	}
	
	inh AbstractFVariable FIterExp.lookupFV(FQName fqn);
	eq FIterExp.getChild().lookupFV(FQName fqn) {
		String name = fqn.name();
		for (CommonForIndex fi : getForIndexs()) {
			if (fi instanceof FForIndex) {
				FForIndex ffi = (FForIndex) fi;
				if (ffi.getFVariable().name().equals(name)) 
					return ffi.getFVariable();
			}
		}
		return lookupFV(fqn);
	}
	
	/*
	inh AbstractFVariable FDerExp.lookupFDV(String name);
	eq Root.getChild().lookupFDV(String name) = null;
	eq FClass.getChild(int index).lookupFDV(String name) {
		FVariable variable = (FDerivativeVariable) 
		   fullyQualifiedVariablesMap().get(name);
		if (variable == null) {
			return unknownFVariable();
		} else {
			return variable;
		}		
	}
	syn lazy FDerivativeVariable FDerExp.myFDV() = 
		(FDerivativeVariable)lookupFDV(getFIdUseExp().derName());	
	*/
	
	syn lazy HashMap<String,FVariable> FClass.fullyQualifiedVariablesMap() {
		//log.debug("FClass.fullyQualifiedVariablesMap()");
		HashMap<String,FVariable> map = new HashMap<String,FVariable>();
		for (FVariable v : getFVariables()) 
			map.put(v.name(), v);
		for (FVariable v : getAliasVariables()) 
			map.put(v.name(), v);
		return map;
	}
	
	syn lazy HashMap<String, FFunctionVariable> FFunctionDecl.fullyQualifiedVariablesMap() {
		HashMap<String, FFunctionVariable> map = new HashMap<String, FFunctionVariable>();
		for (FFunctionVariable v : getFFunctionVariables()) 
			map.put(v.name(), v);
		return map;
	}
	
	syn boolean FIdUse.isForIndex()            = myFV().isForIndex();
	syn boolean FIdUseInstAccess.isForIndex()  = getInstAccess().isForIndex();
	syn boolean FInstAccessExp.isForIndex()    = getInstAccess().isForIndex();
	syn boolean InstAccess.isForIndex()        = myInstComponentDecl().isForIndex();
	syn boolean AbstractFVariable.isForIndex() = false;
	inh boolean FVariable.isForIndex();
	inh boolean InstComponentDecl.isForIndex();
	eq FForIndex.getFVariable().isForIndex()        = true;
	eq InstForIndex.getInstPrimitive().isForIndex() = true;
	eq InstNode.getChild().isForIndex()             = false;
	eq FClass.getChild().isForIndex()               = false;
	
}

aspect ParameterEquations {

	syn boolean AbstractFVariable.hasBindingExp() = false;
	
	syn boolean AbstractFVariable.hasParameterEquation() = 
		!hasBindingExp() && parameterEquation() != null;
	
	inh lazy FAbstractEquation AbstractFVariable.parameterEquation();
	eq FClass.getFVariable(int i).parameterEquation()     = 
		parameterEquationsMap().get(getFVariable(i));
	eq FClass.getAliasVariable(int i).parameterEquation() = 
		parameterEquationsMap().get(getAliasVariable(i));
	eq FForIndex.getChild().parameterEquation()           = null;
	eq FFunctionDecl.getChild().parameterEquation()       = null;
	eq FRecordDecl.getChild().parameterEquation()         = null;
	
	syn lazy HashMap<AbstractFVariable,FAbstractEquation> FClass.parameterEquationsMap() {
		HashMap<AbstractFVariable,FAbstractEquation> map = new HashMap<AbstractFVariable,FAbstractEquation>();
		for (FAbstractEquation equ : getFParameterEquations())
			equ.addToParameterEquationsMap(map);
		return map;
	}
	
	public void FAbstractEquation.addToParameterEquationsMap(
			HashMap<AbstractFVariable,FAbstractEquation> map) {}
	
	public void FEquation.addToParameterEquationsMap(
			HashMap<AbstractFVariable,FAbstractEquation> map) {
		getLeft().addToParameterEquationsMap(map, this);
	}
	
	public void FFunctionCallEquation.addToParameterEquationsMap(
			HashMap<AbstractFVariable,FAbstractEquation> map) {
		for (FFunctionCallLeft left : getLefts())
			if (left.hasFExp())
				left.getFExp().addToParameterEquationsMap(map, this);
	}
	
	public void FExp.addToParameterEquationsMap(
			HashMap<AbstractFVariable,FAbstractEquation> map, FAbstractEquation equ) {
		for (FExp e : childFExps())
			e.addToParameterEquationsMap(map, equ);
	}
	
	public void FIdUseExp.addToParameterEquationsMap(
			HashMap<AbstractFVariable,FAbstractEquation> map, FAbstractEquation equ) {
		AbstractFVariable fv = getFIdUse().myFV();
		if (!fv.isUnknown())
			map.put(fv, equ);
	}
	
	/**
	 * \brief Is this expression in a parameter equation?
	 */
	inh boolean FExp.inParameterEquation();
	eq FClass.getFParameterEquation().inParameterEquation() = true;
	eq FClass.getChild().inParameterEquation()              = false;
	eq FFunctionDecl.getChild().inParameterEquation()       = false;
	eq Root.getChild().inParameterEquation()                = false;
	
	/**
	 * \brief Is this expression in the value of an attribute?
	 */
	inh boolean FExp.inAttributeValue();
	eq FAttribute.getValue().inAttributeValue()    = true;
	eq FClass.getChild().inAttributeValue()        = false;
	eq FFunctionDecl.getChild().inAttributeValue() = false;
	eq Root.getChild().inAttributeValue()          = false;
	
}

aspect FlatFunctionBinding {
	
	syn FFunctionDecl FFunctionCall.myFFunctionDecl() = lookupFFunctionDecl(getName().name());
	
	inh FFunctionDecl FFunctionCall.lookupFFunctionDecl(String name);
	eq Root.getChild().lookupFFunctionDecl(String name)   = null;
	eq FClass.getChild().lookupFFunctionDecl(String name) = lookupFunc(name);
	
	syn FFunctionDecl FClass.lookupFunc(String name) {
		for (FFunctionDecl f : getFFunctionDecls())
			if (f.name().equals(name))
				return f;
		return null;
	}
	
}

aspect FlatRecordBinding {
	
	/**
	 * \brief Get the outermost record variable of this access.
	 * 
	 * Access is assumed to be refering to a member of a record and be in a function.
	 */
	syn AbstractFVariable FIdUse.myRecordFV() = lookupFV(getFQName().copyPrefix());
	
	syn lazy FRecordDecl AbstractFVariable.myFRecordDecl() = null;
	eq FRecordVariable.myFRecordDecl()   = lookupFRec(getRecord().name());
	eq FFunctionVariable.myFRecordDecl() = type().myFRecordDecl();
	
	syn lazy FRecordDecl FIdUse.myFRecordDecl() = lookupFRec(name());
	
	syn lazy FRecordDecl FType.myFRecordDecl() = null;
	eq FRecordType.myFRecordDecl() = lookupFRec(getName());
	
	inh FRecordDecl FRecordVariable.lookupFRec(String name);
	inh FRecordDecl FRecordType.lookupFRec(String name);
	inh FRecordDecl FIdUse.lookupFRec(String name);
	eq Root.getChild().lookupFRec(String name) = null;
	eq FClass.getChild().lookupFRec(String name) = lookupFRec(name);
	syn FRecordDecl FClass.lookupFRec(String name) {
		for (FRecordDecl r : getFRecordDecls())
			if (r.name().equals(name))
				return r;
		return null;
	}
	
	syn AbstractFVariable FRecordDecl.findComponent(FQName name) {
		UnknownFVariable unknown = unknownFVariable();
		FRecordDecl rec = this;
		AbstractFVariable res = unknown;
		for (FQNamePart p : name.getFQNameParts()) {
			if (rec == null)
				return unknown;
			res = unknown;
			for (FVariable v : rec.getFVariables()) {
				if (v.getFQName().getFQNamePart(0).getName().equals(p.getName())) {
					res = v;
					rec = v.myFRecordDecl();
					break;
				}
			}
			if (res == unknown)
				return unknown;
		}
		return res;
	}
	
}

aspect FlatEnumBinding {
	
	syn AbstractFVariable FIdUse.myEnumFV() = lookupFV(getFQName().copyPrefix());
	
	syn lazy FEnumDecl AbstractFVariable.myFEnumDecl() = null;
	eq FEnumVariable.myFEnumDecl()     = lookupFEnum(getEnum().name());
	eq FFunctionVariable.myFEnumDecl() = type().myFEnumDecl();
	eq FEnumLiteral.myFEnumDecl()      = retrieveFEnumDecl();
	
	inh FEnumDecl FEnumLiteral.retrieveFEnumDecl();
	eq FEnumDecl.getChild().retrieveFEnumDecl() = this;
	eq FlatRoot.getChild().retrieveFEnumDecl()  = null;
	
	syn lazy FEnumDecl FIdUse.myFEnumDecl()      = lookupFEnum(name());
	syn lazy FEnumDecl FEnumLitExp.myFEnumDecl() = lookupFEnum(getEnum());
	syn lazy FEnumDecl FType.myFEnumDecl()       = null;
	eq FEnumType.myFEnumDecl()                   = lookupFEnum(getName());
	
	inh FEnumDecl FEnumVariable.lookupFEnum(String name);
	inh FEnumDecl FEnumType.lookupFEnum(String name);
	inh FEnumDecl FEnumLitExp.lookupFEnum(String name);
	inh FEnumDecl FIdUse.lookupFEnum(String name);
	eq Root.getChild().lookupFEnum(String name) = null;
	eq FClass.getChild().lookupFEnum(String name) = lookupFEnum(name);
	syn FEnumDecl FClass.lookupFEnum(String name) {
		for (FEnumDecl r : getFEnumDecls()) {
			if (r.name().equals(name))
				return r;
		}
		return null;
	}
	/*
	syn AbstractFVariable FEnumDecl.findComponent(FQName name) {
		UnknownFVariable unknown = unknownFVariable();
		FEnumDecl rec = this;
		AbstractFVariable res = unknown;
		for (FQNamePart p : name.getFQNameParts()) {
			if (rec == null)
				return unknown;
			res = unknown;
			for (FVariable v : rec.getFVariables()) {
				if (v.getFQName().getFQNamePart(0).getName().equals(p.getName())) {
					res = v;
					rec = v.myFEnumDecl();
					break;
				}
			}
			if (res == unknown)
				return unknown;
		}
		return res;
	}
	*/
}

aspect FlatPreBinding {

	inh AbstractFVariable FIdUse.lookupPreFV(FQName fqn);	
	inh AbstractFVariable FExp.lookupPreFV(FQName fqn);	
	syn AbstractFVariable FPreExp.myFV() {
		if (getFExp() instanceof FIdUseExp) {
		    //System.out.println("FPreExp.myFV(): " + name());
			return ((FIdUseExp)getFExp()).lookupPreFV(((FIdUseExp)getFExp()).getFIdUse().getFQName());
    	} else {
    		return unknownFVariable();
    	}
    }
    
	eq Root.getChild().lookupPreFV(FQName fqn) = null;

	eq FClass.getChild(int index).lookupPreFV(FQName fqn) = lookupPreFV(fqn);
	
	public AbstractFVariable FClass.lookupPreFV(FQName fqn) {
		boolean scalar = fqn.scalarized;
		fqn.scalarized = scalarized;
		AbstractFVariable variable = lookupPreFVFromMap(fullyQualifiedVariablesMap(), fqn);
		//System.out.println("FClass.lookupPreFV(): " + fqn.name());
		fqn.scalarized = scalar;
		return (variable == null) ? unknownFVariable() : variable;
	}

	public static AbstractFVariable ASTNode.lookupPreFVFromMap(
		HashMap<String, ? extends AbstractFVariable> map, FQName fqn) {
		AbstractFVariable variable = map.get(fqn.preName());
		//System.out.println(map.toString());
		return variable;
	}

}

aspect FlatHDerBinding {

	inh AbstractFVariable FIdUse.lookupHDerFV(FQName fqn, int order);	
	inh AbstractFVariable FExp.lookupHDerFV(FQName fqn, int order);	
	syn AbstractFVariable FHDerExp.myFV() {
		return getFIdUse().lookupHDerFV(getFIdUse().getFQName(),getOrder());
    }
    
	eq Root.getChild().lookupHDerFV(FQName fqn, int order) = null;

	eq FClass.getChild(int index).lookupHDerFV(FQName fqn, int order) = lookupHDerFV(fqn, order);
	
	public AbstractFVariable FClass.lookupHDerFV(FQName fqn, int order) {
		boolean scalar = fqn.scalarized;
		fqn.scalarized = scalarized;
		AbstractFVariable variable = lookupHDerFVFromMap(fullyQualifiedVariablesMap(), fqn, order);
		//System.out.println("FClass.lookupPreFV(): " + fqn.name());
		fqn.scalarized = scalar;
		return (variable == null) ? unknownFVariable() : variable;
	}

	public static AbstractFVariable ASTNode.lookupHDerFVFromMap(
		HashMap<String, ? extends AbstractFVariable> map, FQName fqn, int order) {
		String[] str = fqn.derName().split("\\)");
		AbstractFVariable variable = map.get(str[0] + "," + order + ")");
		//System.out.println(map.toString());
		return variable;
	}

}

aspect UnknownFVariables {

	syn UnknownFVariable FClass.getUnknownFVariable() = new UnknownFVariable();

	syn UnknownFVariable ASTNode.unknownFVariable() = root().unknownFVariable();
	eq FlatRoot.unknownFVariable() = getFClass().getUnknownFVariable();

	syn boolean AbstractFVariable.isUnknown() = false;
	eq UnknownFVariable.isUnknown() = true;
	   
}