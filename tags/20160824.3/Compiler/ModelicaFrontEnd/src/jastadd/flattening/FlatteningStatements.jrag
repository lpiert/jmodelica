/*
    Copyright (C) 2016 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect FlatStatements {
    
    public static boolean FStatement.flattenFStatementList(List<FStatement> sl, FClass fc, FQName prefix) {
        return FStatement.flattenFStatementList(sl, new List<FStatement>(), fc, prefix);
    }
    
    public static boolean FStatement.flattenFStatementList(List<FStatement> fromList, List<FStatement> toList, 
            FClass fc, FQName prefix) {
        boolean hasBroken = false;
        boolean res = false;
        for (FStatement stmt : fromList) {
            if (hasBroken) {
                toList = FStatement.breakBlock(toList, new FIdUseExp(stmt.breakCondName()));
            }
            res = (hasBroken = stmt.flatten(toList, fc, prefix)) || res;
        }
        return res;
    }
    
    public static List<FStatement> FStatement.breakBlock(List<FStatement> stmts, FExp breakCond) {
        FIfStmt ifStmt = new FIfStmt();
        FIfClause clause = new FIfClause();
        clause.setTest(breakCond.fullCopy());
        ifStmt.addFIfWhenClause(clause);
        stmts.add(ifStmt);
        return clause.getFStatements();
    }
    
    syn boolean ASTNode.containsBreakStmt() {
        for (ASTNode n : this)
            if (n.containsBreakStmt())
                return true;
        return false;
    }
    eq FForStmt.containsBreakStmt() {
        return false;
    }
    eq FWhileStmt.containsBreakStmt() {
        return false;
    }
    eq FBreakStmt.containsBreakStmt() {
        return true;
    }
    
    public abstract boolean FStatement.flatten(List<FStatement> sl, FClass fc, FQName prefix);
    
    public boolean FAssignStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        sl.add(new FAssignStmt((FAssignableExp)getLeft().flatten(prefix), getRight().flatten(prefix)));
        return false;
    }
    
    public boolean FInitArrayStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix){
        throw new UnsupportedOperationException();
    }
    
    public boolean FFunctionCallStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        List<FFunctionCallLeft> l = new List<FFunctionCallLeft>();
        for (FFunctionCallLeft a : getLefts())
            l.add(a.flatten(prefix));
        sl.add(new FFunctionCallStmt(l, (FAbstractFunctionCall) getCall().flatten(prefix)));
        return false;
    }
    
    public boolean FWhenStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        boolean res = false;
        boolean hasBroken = false;
        List cl = new List();
        for (FIfWhenClause c : getFIfWhenClauses()) {
            hasBroken = c.flatten(cl, fc, prefix) || hasBroken;
        }
        sl.add(new FWhenStmt(cl));
        return hasBroken;
    }
    
    public boolean FIfStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        boolean res = false;
        boolean hasBroken = false;
        List cl = new List();
        boolean first = true;
        for (FIfWhenClause c : getFIfWhenClauses()) {
            if (c.getTest().variability().indexParameterOrLess()) {
                try {
                    CValue val = c.getTest().ceval();
                    if (!val.booleanValue()) {
                        continue;
                    } else if (first) {
                        return FStatement.flattenFStatementList(c.getFStatements(), sl, fc, prefix) || hasBroken;
                    } else {
                        List l = new List();
                        hasBroken = FStatement.flattenFStatementList(c.getFStatements(), l, fc, prefix) || hasBroken;
                        sl.add(new FIfStmt(cl, l));
                        return hasBroken;
                    }
                } catch (ConstantEvaluationException e) {
                    
                }
            }
            first = false;
            hasBroken = c.flatten(cl, fc, prefix) || hasBroken;
        }
        if (first) {
            return FStatement.flattenFStatementList(getElseStmts(), sl, fc, prefix) || hasBroken;
        }
        List l = new List();
        hasBroken = FStatement.flattenFStatementList(getElseStmts(), l, fc, prefix) || hasBroken;
        sl.add(new FIfStmt(cl, l));
        return hasBroken;
    }
    
    public boolean FIfWhenClause.flatten(List cl, FClass fc, FQName prefix) {
        List l = new List();
        FExp t = getTest().flatten(prefix);
        boolean res = FStatement.flattenFStatementList(getFStatements(), l, fc, prefix);
        cl.add(flattenFIfWhenClause(t, l));
        return res;
    }
    
    protected abstract FIfWhenClause FIfWhenClause.flattenFIfWhenClause(FExp t, List l);
    
    protected FIfWhenClause FIfClause.flattenFIfWhenClause(FExp t, List l) {
        return new FIfClause(t, l);
    }
    
    protected FIfWhenClause FWhenClause.flattenFIfWhenClause(FExp t, List l) {
        return new FWhenClause(t, l);
    }
    
    public boolean FForStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        sl.add((FForStmt)(this.fullCopy()));
        return false;
    }
        
    public boolean FWhileStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        List l = new List();
        boolean res = FStatement.flattenFStatementList(getWhileStmts(), l, fc, prefix);
        sl.add(new FWhileStmt(getTest().flatten(prefix), l));
        return res;
    }
    
    public boolean InstForStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        if (inFunction()) {
            List fsl = new List();
            FStatement.flattenFStatementList(getForStmts(), fsl, fc, prefix);
            int i = getNumInstForIndex() - 1;
            FForStmt res = new FForStmt(getInstForIndex(i).flatten(prefix), fsl);
            for (i--; i >= 0; i--) {
                fsl = new List().add(res);
                res = new FForStmt(getInstForIndex(i).flatten(prefix), fsl);
            }
            sl.add(res);
        } else {
            ArrayList<CommonForIndex> forIndices = new ArrayList<CommonForIndex>();
            for (InstForIndex ifi : getInstForIndexs()) {
                forIndices.add(ifi);
            }
            Indices ind = Indices.create(forIndices);
            if (!ind.size().isZero()) {
                FVariable tempVar = null;
                boolean hasBreak = getForStmts().containsBreakStmt();
                if (hasBreak) {
                    tempVar = new FBooleanVariable(new FTemporaryVisibilityType(), fDiscrete(),
                            prefix.copyAndAppend(myTempVarName()));
                    fc.addFVariableNoTransform(tempVar);
                    new FAssignStmt(tempVar.createUseExp(), new FBooleanLitExpTrue()).flatten(sl, fc, prefix);
                }
                
                List<FStatement> inner = sl;
                for (Index i : ind) {
                    if (hasBreak) {
                        inner = FStatement.breakBlock(sl, tempVar.createUseExp());
                    }
                    ind.translate(i).setValues(forIndices);
                    getForStmts().flushAllRecursive();
                    FStatement.flattenFStatementList(getForStmts(), inner, fc, prefix);
                }
                for (InstForIndex ifi : getInstForIndexs()) {
                    ifi.clearEvaluationValue();
                }
            }
        }
        return false;
    }
    
    private static final String InstClassDecl.TEMP_VAR_PREFIX = "temp_";
    private int InstClassDecl.incr = 0;
    
    syn String InstClassDecl.nextTempVarName() = TEMP_VAR_PREFIX + ++incr;
    
    inh lazy String InstForStmt.myTempVarName();
    eq Root.getChild().myTempVarName() = null;
    eq InstClassDecl.getChild().myTempVarName() {
        String sqn = "";
        while (componentExists(sqn = nextTempVarName())) {}
        return name() + "." + sqn;
    }
    
    inh String FStatement.breakCondName();
    eq InstForStmt.getChild().breakCondName() = myTempVarName();
    eq Root.getChild().breakCondName() = null;
    
    public boolean InstClassDecl.componentExists(String name) {
        for (InstComponentDecl decl : allInstComponentDecls()) {
            if (decl.name().equals(name)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean FBreakStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix){
        if (!inFunction() && enclosingLoop() instanceof InstForStmt) {
            new FAssignStmt(prefix.copyAndAppend(breakCondName()).createFIdUseExp(),
                    new FBooleanLitExpFalse()).flatten(sl, fc, prefix);
            return true;
        }
        sl.add(new FBreakStmt());
        return false;
    }
    public boolean FReturnStmt.flatten(List<FStatement> sl, FClass fc, FQName prefix) {
        sl.add(new FReturnStmt());
        return false;
    }
    
}

