<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>FMI Interface</title>

  <para>FMI (Functional Mock-up Interface) is a standard for exchanging models
  between different modeling and simulation environments. FMI defines a model
  execution interface consisting of a set of C-function signatures for
  handling the communication between the model and a simulation environment.
  Models are presented as ODEs with time, state and step events. FMI also
  specifies that all information related to a model, except the equations,
  should be stored in an XML formated text-file. The format is specified in
  the standard and specifically contains information about the variables,
  names, identifiers, types and start attributes.</para>

  <para>A model is distributed in a zip-file with the extension '.fmu',
  containing several files. These zip-files containing the models are called
  FMUs (Functional Mock-up Units). The important files in an FMU are mainly
  the XML-file, which contains the definitions of all variables and then files
  containing the C-functions which can be provided in source and/or binary
  form. FMI standard also supports providing documentation and resources
  together with the FMU. For more information regarding the FMI standard,
  please visit http://www.functional-mockup-interface.org/.</para>

  <section>
    <title>Overview of JModelica.org FMI Python package</title>

    <para>The JModelica.org interface to FMI is written in Python and is
    intended to be a close copy of the defined C-interface for an FMU and
    provides classes and functions for interacting with FMUs.</para>

    <para>The JModelica.org platform offers a Pythonic and convenient
    interface for FMUs which can be used to connect other simulation software.
    JModelica.org also offers a connection to Assimulo, the default simulation
    package included in JModelica.org so that FMUs can easily be
    simulated.</para>

    <para>The interface is located in <literal>jmodelica.fmi</literal> and
    consist of the class <literal>FMIModel</literal> together with methods for
    unzipping the FMU and for writing the simulation results. Connected to
    this interface is a wrapper for JModelica.org's simulation package to
    enable an easy simulation of the FMUs. The simulation wrapper is located
    in <literal>jmodelica.simulation.assimulo</literal>,
    <literal>FMIODE</literal>.</para>

    <para>In the table below is a list of the FMI C-interface and its
    counterpart in the JModelica.org Python package. We have adapted the name
    convention of lowercase letters and underscores separating words. For
    methods with no calculations, as for example
    <literal>fmi(Get/Set)ContinuousStates</literal> they are instead of
    different methods, connected with a property. In the table, a lack of
    parenthesis indicates that the method is instead a property.</para>

    <table>
      <title>Conversion table.</title>

      <tgroup align="left" cols="2">
        <thead>
          <row>
            <entry align="center">FMI C-Interface</entry>

            <entry align="center">JModelica.org FMI Python Interface</entry>
          </row>
        </thead>

        <tbody>
          <row>
            <entry>const char* fmiGetModelTypesPlatform()</entry>

            <entry>string FMIModel.model_types_platform</entry>
          </row>

          <row>
            <entry>const char* fmiGetVersion()</entry>

            <entry>string FMIModel.version</entry>
          </row>

          <row>
            <entry>fmiComponent fmiInstantiateModel(...)</entry>

            <entry>FMIModel.__init__()</entry>
          </row>

          <row>
            <entry>void fmiFreeModelInstance(fmiComponent c)</entry>

            <entry>FMIModel.__del__()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetDebugLogging(...)</entry>

            <entry>none FMIModel.set_debug_logging(flag)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetTime(...)</entry>

            <entry>FMIModel.time</entry>
          </row>

          <row>
            <entry>fmiStatus fmi(Get/Set)ContinuousStates(...)</entry>

            <entry>FMIModel.continuous_states</entry>
          </row>

          <row>
            <entry>fmiStatus fmiCompletedIntegratorStep(...)</entry>

            <entry>boolean FMIModel.completed_integrator_step()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetReal/Integer/Boolean/String(...)</entry>

            <entry>none
            FMIModel.set_real/integer/boolean/string(valueref,values)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiInitialize(...)</entry>

            <entry>none FMIModel.initialize() (also sets the start
            attributes)</entry>
          </row>

          <row>
            <entry>struct fmiEventInfo</entry>

            <entry>FMIModel.get_event_info()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetDerivatives(...)</entry>

            <entry>numpy.array FMIModel.get_derivatives()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetEventIndicators(...)</entry>

            <entry>numpy.array FMIModel.get_event_indicators()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetReal/Integer/Boolean/String(...)</entry>

            <entry>numpy.array
            FMIModel.get_real/integer/boolean/string(valueref)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiEventUpdate(...)</entry>

            <entry>none FMIModel.event_update()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetNominalContinuousStates(...)</entry>

            <entry>FMIModel.nominal_continuous_states</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetStateValueReferences(...)</entry>

            <entry>numpy.array FMIModel.get_state_value_references()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiTerminate(...)</entry>

            <entry>FMIModel.__del__()</entry>
          </row>
        </tbody>
      </tgroup>
    </table>

    <para>If logging is set to <literal>True</literal> the log can be
    retrieved with the method,</para>

    <programlisting language="Python">FMIModel.get_log()</programlisting>

    <para>Documentation of the functions can also be accessed interactively
    from IPython by using for instance,</para>

    <programlisting language="Python">FMIModel.get_real?</programlisting>

    <para>There is also a one-to-one map to the C-functions, meaning that
    there is an option to use the low-level C-functions as they are specified
    in the standard instead of using our wrapping of the functions. These
    functions are also located in <literal>FMIModel</literal> and is named
    with a leading underscore together with the same name as specified in the
    standard.</para>
  </section>

  <section>
    <title>Examples</title>

    <para>In the next two sections, it will be shown how to use the
    JModelica.org platform both for simulation of an FMU using the native
    Python interface and how to simulate an FMU using JModelica.org's
    simulation package, Assimulo.</para>

    <para>The Python commands in these examples may be copied and pasted
    directly into a Python shell, in some cases with minor modifications.
    Alternatively, they may be copied into a text file, which also is the
    recommended way.</para>

    <section>
      <title>Simulation using the native FMI interface</title>

      <para>This example shows how to use the native JModelica.org FMI
      interface for simulation of an FMU. The FMU that is to be simulated is
      the bouncing ball example from Qtronics FMU SDK
      (http://www.qtronic.de/en/fmusdk.html). This example is written similar
      to the example in the documentation of the 'Functional Mock-up Interface
      for Model Exchange' version 1.0
      (http://www.functional-mockup-interface.org/). The bouncing ball model
      is to be simulated using the explicit Euler method with event
      detection.</para>

      <para>The example can also be found in the Python examples catalog in
      the JModelica.org platform.</para>

      <para>The bouncing ball consists of two equations,</para>

      <para><inlinemediaobject>
          <imageobject>
            <imagedata fileref="images/bounc_eq.png" scale="100"></imagedata>
          </imageobject>
        </inlinemediaobject></para>

      <para>and one event function (also commonly called root
      function),</para>

      <para><inlinemediaobject>
          <imageobject>
            <imagedata fileref="images/event_eq.png"></imagedata>
          </imageobject>
        </inlinemediaobject></para>

      <para>Where the ball bounces and lose some of its energy according
      to,</para>

      <para><inlinemediaobject>
          <imageobject>
            <imagedata fileref="images/event_handle.png"></imagedata>
          </imageobject>
        </inlinemediaobject></para>

      <para>Here, h is the height, g the gravity, v the velocity and e a
      dimensionless parameter. The starting values are, h=1 and v=0 and for
      the parameters, e=0.7 and g = 9.81.</para>

      <section>
        <title>Implementation</title>

        <para>Start by importing the necessary modules,</para>

        <programlisting language="Python">        import numpy as N 
        import pylab as P #Used for plotting
        from jmodelica.fmi import FMIModel #The FMI Interface      </programlisting>

        <para>Next, the FMU is to be loaded and initialized,</para>

        <programlisting language="Python">        #Load the FMU by specifying the fmu together with the path.
        bouncing_fmu = FMIModel('/path/to/FMU/bouncingBall.fmu')

        Tstart = 0.5 #The start time.
        Tend   = 3.0 #The final simulation time.
        
        bouncing_fmu.time = Tstart #Set the start time before the initialization.
                                   #(Defaults to 0.0)
        
        bouncing_fmu.initialize() #Initialize the model. Also sets all the start 
                                  #attributes defined in the XML file.     </programlisting>

        <para>The first line loads the FMU and connects the C-functions of the
        model to Python together with loading the information from the
        XML-file. The start time also needs to be specified by setting the
        property <literal>time</literal>. The model is also initialized, which
        must be done before the simulation is started.</para>

        <para>Note that if the start time is not specified,
        <literal>FMIModel</literal> tries to find the starting time in the
        XML-file structure 'default experiment' and if successful starts the
        simulation from that time. Also if the XML-file does not contain any
        information about the default experiment the simulation is started
        from time zero.</para>

        <para>Then information about the first step is retrieved and stored
        for later use.</para>

        <programlisting language="Python">        #Get Continuous States
        x = bouncing_fmu.continuous_states
        #Get the Nominal Values
        x_nominal = bouncing_fmu.nominal_continuous_states
        #Get the Event Indicators
        event_ind = bouncing_fmu.get_event_indicators()
        
        #Values for the solution
        vref  = [bouncing_fmu.get_valueref('h')] + \
                [bouncing_fmu.get_valueref('v')] #Retrieve the valureferences for the
                                                 #values 'h' and 'v't_sol = [Tstart]
        sol = [bouncing_fmu.get_real(vref)]        </programlisting>

        <para>Here the continuous states together with the nominal values and
        the event indicators are stored to be used in the integration loop. In
        our case the nominal values are all equal to one. This information is
        available in the XML-file. We also create lists which are used for
        storing the result. The final step before the integration is started
        is to define the step-size.</para>

        <programlisting language="Python">        time = Tstart
        Tnext = Tend #Used for time events
        dt = 0.01 #Step-size      </programlisting>

        <para>We are now ready to create our main integration loop where the
        solution is advanced using the explicit Euler method.</para>

        <programlisting language="Python">        #Main integration loop.
        while time &lt; Tend and not bouncing_fmu.get_event_info().terminateSimulation:
            #Compute the derivative of the previous step f(x(n), t(n))
            dx = bouncing_fmu.get_derivatives()
            
            #Advance
            h = min(dt, Tnext-time)
            time = time + h
            
            #Set the time
            bouncing_fmu.time = time
            
            #Set the inputs at the current time (if any)
            #bouncing_fmu.set_real,set_integer,set_boolean,set_string (valueref, values)
            
            #Set the states at t = time (Perform the step using x(n+1)=x(n)+hf(x(n), t(n))
            x = x + h*dx 
            bouncing_fmu.continuous_states = x    </programlisting>

        <para>This is the integration loop for advancing the solution one
        step. The loop continues until the final time have been reached or if
        the FMU reported that the simulation is to be terminated. At the start
        of the loop the derivatives of the continuous states are retrieved and
        then the simulation time is incremented by the step-size and set to
        the model. It could also be the case that the model is depended on
        inputs which can be set using the <literal>set_(real/...)</literal>
        methods.</para>

        <para>Note that only variables defined in the XML-file to be inputs
        can be set using the <literal>set_(real/...)</literal> methods
        according to the FMI specification.</para>

        <para>The step is performed by calculating the new states (x+h*dx) and
        setting the values into the model. As our model, the bouncing ball
        also consist of event functions which needs to be monitored during the
        simulation, we have to check the indicators which is done
        below.</para>

        <programlisting language="Python">            #Get the event indicators at t = time
            event_ind_new = bouncing_fmu.get_event_indicators()
            
            #Inform the model about an accepted step and check for step events
            step_event = bouncing_fmu.completed_integrator_step()
            
            #Check for time and state events
            time_event  = abs(time-Tnext) &lt;= 1.e-10
            state_event = True if True in ((event_ind_new&gt;0.0) != (event_ind&gt;0.0))\
                          else False   </programlisting>

        <para>Events can be, time, state or step events. The time events are
        checked by continuously monitor the current time and the next time
        event (Tnext). State events are checked against sign changes of the
        event functions. Step events are monitored in the FMU, in the method
        <literal>completed_integrator_step()</literal> and return True if any
        event handling is necessary. If an event have occurred, it needs to be
        handled, see below.</para>

        <programlisting language="Python">            #Event handling
            if step_event or time_event or state_event:
                
                eInfo = bouncing_fmu.get_event_info()
                eInfo.iterationConverged = False
                
                #Event iteration
                while eInfo.iterationConverged == False:
                    bouncing_fmu.event_update('0') #Stops at each event iteration
                    eInfo = bouncing_fmu.get_event_info()

                    #Retrieve solutions (if needed)
                    if eInfo.iterationConverged == False:
                        #bouncing_fmu.get_real,get_integer,get_boolean,get_string(valueref)
                        pass
                
                #Check if the event affected the state values and if so sets them
                if eInfo.stateValuesChanged:
                    x = bouncing_fmu.continuous_states
            
                #Get new nominal values.
                if eInfo.stateValueReferencesChanged:
                    atol = 0.01*rtol*bouncing_fmu.nominal_continuous_states
                    
                #Check for new time event
                if eInfo.upcomingTimeEvent:
                    Tnext = min(eInfo.nextEventTime, Tend)
                else:
                    Tnext = Tend  </programlisting>

        <para>If an event occurred, we enter the iteration loop where we loop
        until the solution of the new states have converged. During this
        iteration we can also retrieve the intermediate values with the normal
        <literal>get</literal> methods. At this point <literal>eInfo</literal>
        contains information about the changes made in the iteration. If the
        state values have changed, they are retrieved. If the state references
        have changed, meaning that the state variables no longer have the same
        meaning as before by pointing to another set of continuous variables
        in the model, for example in the case with dynamic state selection,
        new absolute tolerances are calculated with the new nominal values.
        Finally the model is checked for a new time event.</para>

        <programlisting language="Python">            event_ind = event_ind_new
        
            #Retrieve solutions at t=time for outputs
            #bouncing_fmu.get_real,get_integer,get_boolean,get_string (valueref)
            
            t_sol += [time]
            sol += [bouncing_fmu.get_real(vref)]   </programlisting>

        <para>In the end of the loop, the solution is stored and the old event
        indicators are stored for use in the next loop.</para>

        <para>After the loop have finished, by reaching the final time, we
        plot the simulation results</para>

        <programlisting language="Python">        #Plot the height
        P.figure(1)
        P.plot(t_sol,N.array(sol)[:,0])
        P.title(bouncing_fmu.get_name())
        P.ylabel('Height (m)')
        P.xlabel('Time (s)')
        #Plot the velocity
        P.figure(2)
        P.plot(t_sol,N.array(sol)[:,1])
        P.title(bouncing_fmu.get_name())
        P.ylabel('Velocity (m/s)')
        P.xlabel('Time (s)')
        P.show()     </programlisting>

        <para>and the figure below shows the results.</para>

        <figure>
          <title>Simulation result</title>

          <mediaobject>
            <imageobject>
              <imagedata fileref="images/bouncing_h.svg" scalefit="1"
                         width="50%"></imagedata>
            </imageobject>
          </mediaobject>

          <mediaobject>
            <imageobject>
              <imagedata fileref="images/bouncing_v.svg" scalefit="1"
                         width="50%"></imagedata>
            </imageobject>
          </mediaobject>
        </figure>
      </section>
    </section>

    <section>
      <title>Example using a compiled FMU</title>

      <para>This example will show how to use the JModelica.org's
      FMI-interface together with its simulation package, Assimulo. The FMU to
      be simulated is the full Robot from the Modelica standard library (3.1)
      where it is located in
      <literal>Mechanics.MultiBody.Examples.Systems.RobotR3</literal>. It
      consists of brakes, motors, gears and path planning. The model consists
      of 36 continuous states and around 700 algebraic variables together with
      98 event functions and also a few thousand constants/parameters. The FMU
      was generated using Dymola 7.4.</para>

      <figure>
        <title>Full Robot</title>

        <mediaobject>
          <imageobject>
            <imagedata fileref="images/fullRobot.png" scalefit="1" width="60%"></imagedata>
          </imageobject>
        </mediaobject>
      </figure>

      <section>
        <title>Implementation</title>

        <para>We start by importing the necessary method and module,</para>

        <programlisting language="Python">    from jmodelica import simulate
    import pylab as P </programlisting>

        <para>The <literal>simulate</literal> method is a high-level method
        for simulation where the input are the model of interest together with
        options for the algorithm and options for the solver. We are
        interested in simulating the Robot from time 0.0 to 1.8 using 1000
        communication points. This information is specified in a dictionary
        called <literal>alg_args</literal> where all the algorithms options
        are specified. For the solver options there is another dictionary
        called <literal>solver_args</literal>, where for example the
        tolerances can be specified. Information regarding the solver
        arguments can be found here, http://www.jmodelica.org/assimulo and a
        selection of solver arguments can be found in the table below.
        Currently only the solver CVode is supported.</para>

        <table>
          <title>Selection of solver arguments for CVode</title>

          <tgroup align="left" cols="2">
            <thead>
              <row>
                <entry align="center">Argument</entry>

                <entry align="center">Option</entry>
              </row>
            </thead>

            <tbody>
              <row>
                <entry>discr (Discretization method)</entry>

                <entry>Adams / BDF (string), (default BDF)</entry>
              </row>

              <row>
                <entry>iter (Iteration method)</entry>

                <entry>Newton / FixedPoint (string), (default Newton)</entry>
              </row>

              <row>
                <entry>maxord (The maximum order)</entry>

                <entry>Positive float (max 5 for BDF and 12 for Adams)</entry>
              </row>

              <row>
                <entry>write_cont (Turn on continuous writing of
                result)</entry>

                <entry>Boolean (default True)</entry>
              </row>

              <row>
                <entry>rtol (Relative Tolerance)</entry>

                <entry>Float (default value in XML-file or 1.0e-4)</entry>
              </row>

              <row>
                <entry>atol (Absolute Tolerance(s))</entry>

                <entry>Float or Array of Floats (default
                0.01*rtol*nominal_continuous_states)</entry>
              </row>
            </tbody>
          </tgroup>
        </table>

        <para>In the code below, the simulation options are passed on to
        <literal>simulate</literal></para>

        <programlisting language="Python">    simRobot = simulate('Robot.fmu', alg_args={'final_time':1.8, 
                       'num_communication_points':1000}) </programlisting>

        <para>This preforms the simulation and the statistics will be printed
        in the prompt.</para>

        <para>The simulation result can then be retrieved from the result
        object <literal>simRobot</literal> by calling the method
        <literal>get_result_data()</literal>.</para>

        <programlisting language="Python">    res = sim_res.get_result_data() </programlisting>

        <para>To retrieve data about a variable from the result data (res),
        use the method <literal>get_variable_data</literal> together with the
        name of the variable.</para>

        <programlisting language="Python">    dq1 = res.get_variable_data('der(mechanics.q[1])')
    dq6 = res.get_variable_data('der(mechanics.q[6])')  </programlisting>

        <para>Now we have loaded and retrieved the variables of interest. So
        lets plot them.</para>

        <programlisting language="Python">    P.plot(dq1.t,dq1.x,dq6.t,dq6.x)
    P.legend(['der(mechanics.q[1])','der(mechanics.q[6])'])
    P.xlabel('Time (s)')
    P.ylabel('Joint Velocity (rad/s)')
    P.title('Full Robot')
    P.show()</programlisting>

        <para>Below is the resulting figure together with a comparison from
        the simulation result generated by Dymola.</para>

        <figure>
          <title>Full Robot Results</title>

          <mediaobject>
            <imageobject>
              <imagedata fileref="images/robot_result.svg" scalefit="1"
                         width="60%"></imagedata>
            </imageobject>
          </mediaobject>
        </figure>

        <figure>
          <title>Comparison with Dymola</title>

          <mediaobject>
            <imageobject>
              <imagedata fileref="images/DymolaFullRobotComparison.png"
                         scalefit="1" width="60%"></imagedata>
            </imageobject>
          </mediaobject>
        </figure>
      </section>
    </section>
  </section>
</chapter>
