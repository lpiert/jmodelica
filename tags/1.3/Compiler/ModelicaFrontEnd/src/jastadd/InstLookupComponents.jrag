/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.util.HashSet;

aspect InstLookupComponents {

	inh HashSet InstAccess.lookupInstComponent(String name);	
	inh lazy HashSet InstNode.lookupInstComponent(String name);	
	inh lazy HashSet InstModification.lookupInstComponent(String name);	
	eq InstBaseClassDecl.getChild().lookupInstComponent(String name) = genericLookupInstComponent(name); 	
	eq InstNode.getChild().lookupInstComponent(String name) = genericLookupInstComponent(name); 
	
	eq InstComponentDecl.getInstModification().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstComponentDecl.getInstConstraining().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstComponentDecl.getConditionalAttribute().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstReplacingComposite.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstReplacingPrimitive.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstExtends.getInstClassModification().lookupInstComponent(String name) = lookupInstComponent(name);
	
	eq InstShortClassDecl.getChild().lookupInstComponent(String name) = lookupInstComponent(name);

	eq InstComponentDecl.getFArraySubscripts().lookupInstComponent(String name) = lookupInstComponent(name);
	
	// TODO: Make sure all of these applies to records as well as primitives
	eq InstAssignable.getChild().lookupInstComponent(String name) = memberInstComponent(name); 
	eq InstAssignable.getBindingFExp().lookupInstComponent(String name) = myInstValueMod().lookupInstComponent(name);
	eq InstAssignable.getInstModification().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstAssignable.getConditionalAttribute().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstAssignable.getFArraySubscripts().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstAssignable.getDynamicFExp().lookupInstComponent(String name) = lookupInstComponent(name);
	
	eq InstRoot.getChild().lookupInstComponent(String name) = emptyHashSet();
	
	eq InstDot.getRight().lookupInstComponent(String name) = getLeft().qualifiedLookupInstComponent(name);
	eq InstArrayAccess.getFArraySubscripts().lookupInstComponent(String name) = getTopInstAccess().lookupInstComponent(name);
	eq InstAccess.getExpandedSubscripts().lookupInstComponent(String name) = getTopInstAccess().lookupInstComponent(name);
		
	syn HashSet InstAccess.qualifiedLookupInstComponent(String name) = emptyHashSet();
	eq InstClassAccess.qualifiedLookupInstComponent(String name) = myInstClassDecl().memberInstComponent(name);
	eq InstComponentAccess.qualifiedLookupInstComponent(String name) {
		InstComponentDecl icd = myInstComponentDecl();
		if (hasFArraySubscripts() && !isArray()) {
			// If we can, try to get the correct InstArrayComponentDecl to do lookup from
			try {
				boolean ok = true;
				Index i = getFArraySubscripts().asIndex();
				InstComponentDecl icd2 = icd;
				for (int dim = 0; ok && dim < i.ndims(); dim++) {
					int j = i.get(dim) - 1;
					if (j < 0 || j >= icd.getNumInstComponentDecl()) {
						ok = false;
					} else {
						icd2 = icd2.getInstComponentDecl(j);
						if (!(icd2 instanceof InstArrayComponentDecl))
							ok = false;
					}
				}
				if (ok)
					icd = icd2;
			} catch (ConstantEvaluationException e) {
			} 
		}
		return icd.memberInstComponent(name);	
	}

	eq SourceRoot.getChild().lookupInstComponent(String name) = emptyHashSet();
	// This equation is necessary since InstAccesses may be present in FExps.	
	eq FlatRoot.getChild().lookupInstComponent(String name) = emptyHashSet();

	inh lazy HashSet InstForClauseE.lookupInstComponent(String name);
	eq InstForClauseE.getChild().lookupInstComponent(String name) {
		HashSet set = new HashSet(4);
		for (InstForIndex ifi : getInstForIndexs()) 
			ifi.matchInstComponent(name, set);
		return (set.size() > 0) ? set : lookupInstComponent(name);
	}
	
	inh lazy HashSet InstForStmt.lookupInstComponent(String name);
	eq InstForStmt.getChild().lookupInstComponent(String name) {
		HashSet set = new HashSet(4);
		for (InstForIndex ifi : getInstForIndexs()) 
			ifi.matchInstComponent(name, set);
		return (set.size() > 0) ? set : lookupInstComponent(name);
	}
	
	inh lazy HashSet FIterExp.lookupInstComponent(String name);
	eq FIterExp.getChild().lookupInstComponent(String name) {
		HashSet set = new HashSet(4);
		for (CommonForIndex fi : getForIndexs()) 
			fi.matchInstComponent(name, set);
		return (set.size() > 0) ? set : lookupInstComponent(name);
	}

	public void CommonForIndex.matchInstComponent(String name, HashSet set) {}
	public void InstForIndex.matchInstComponent(String name, HashSet set) {
		if (getInstPrimitive().name().equals(name)) 
			set.add(getInstPrimitive());
	}
	
	syn lazy HashSet InstNode.genericLookupInstComponent(String name) {
		HashSet set = new HashSet(4);
		set.addAll(memberInstComponent(name));
		for (InstImport ii : instImports()) 
			set.addAll(ii.lookupInstConstantInImport(name));		
		if (set.isEmpty()) 
			set.addAll(genericLookupInstConstant(name));
		return set.isEmpty() ? emptyHashSet() : set;
	 }
	
	syn HashSet InstNode.genericLookupInstConstant(String name) {
		HashSet set = new HashSet(4);
		set.addAll(lookupInstConstant(name));
		for (InstExtends ie : instExtends())
			set.addAll(ie.lookupInstConstant(name));
		return set;
	}
	
	eq InstComponentDecl.genericLookupInstConstant(String name) {
		HashSet set = super.genericLookupInstConstant(name);
		set.addAll(myInstClass().lookupInstConstant(name));
		return set;
	}
	
	/**
	 * \brief Check if any constraining class/component has the given component.
	 */
	syn boolean InstNode.constrainMemberInstComponent(String name) {
		if (!hasInstConstraining())
			return true;
		return getInstConstraining().getInstNode().memberInstComponent(name).size() > 0;
	}
	
	syn lazy HashSet InstNode.memberInstComponent(String name)  {

		if (isArray()) {
			if (getNumInstComponentDecl() == 0)
				return emptyHashSet();
			
			int ndims = ndims();
			InstNode n = getInstComponentDecl(0);
			for (int i=0;i<ndims-1;i++) {
				n = n.getInstComponentDecl(0);
			}
			return n.memberInstComponent(name);
			
		} else {	
			HashSet set = new HashSet(4);
		 	
			for (InstComponentDecl ic : instComponentDecls()) 
				if (ic.matchInstComponentDecl(name))
					set.add(ic);

			for (InstExtends ie : instExtends()) 
				set.addAll(ie.memberInstComponent(name));
		
			if (set.size() > 0 && constrainMemberInstComponent(name)) 
				return set;
			return emptyHashSet();
		}
	}
	
	inh lazy HashSet InstNode.lookupInstConstant(String name);
	eq Root.getChild().lookupInstConstant(String name) = emptyHashSet();
	eq InstNode.getChild().lookupInstConstant(String name) {
		HashSet set = new HashSet(4);
		set.addAll(memberInstConstant(name));
		if (set.isEmpty())
			set.addAll(lookupInstConstant(name));
		return set.isEmpty() ? emptyHashSet() : set;
	}
	
	syn lazy HashSet InstNode.memberInstConstant(String name) {
		HashSet set = new HashSet(4);
		for (InstComponentDecl ic : instComponentDecls()) 
			if (ic.getComponentDecl().isConstant() && ic.matchInstComponentDecl(name))
				set.add(ic);

		if (set.size() > 0 && constrainMemberInstComponent(name)) 
			return set;
		return emptyHashSet();
	}

	syn HashSet InstImport.lookupInstConstantInImport(String name) {
		// Assume import points to a single (constant) component
	    if (name.equals(name())) {
	    	String className = getPackageName().enclosingName();
	    	if (!className.equals("")) {
		    	InstClassDecl icd = ((SourceRoot)root()).getProgram().getInstProgramRoot().
		           simpleLookupInstClassDecl(className);
		    	return icd.memberInstConstant(getPackageName().getLastInstAccess().name());
	    	}
		}
		return emptyHashSet();
	}
	
	eq InstImportUnqualified.lookupInstConstantInImport(String name)  {
        return getImportedClass().memberInstConstant(name);
	}

	// This is needed since the member components of InstPrimitive:s (which are attributes)
	// are not instantiated
	eq InstPrimitive.memberInstComponent(String name) = myInstClass().memberInstComponent(name);
	eq InstExtends.memberInstComponent(String name) = extendsPrimitive()? myInstClass().memberInstComponent(name) : super.memberInstComponent(name);

	/**
	 * Simple matching of component names.
	 */
	syn boolean InstComponentDecl.matchInstComponentDecl(String name) = name().equals(name);
	
	syn lazy InstComponentDecl InstAccess.myInstComponentDecl() = unknownInstComponentDecl();
	eq InstComponentAccess.myInstComponentDecl() {
		HashSet set = lookupInstComponent(name());
		if (set.size() > 0) {
			return (InstComponentDecl)set.iterator().next();
		} else
			return unknownInstComponentDecl();
	}
	
	eq InstDot.myInstComponentDecl() {
		return getRight().myInstComponentDecl();
	}
	
}

aspect LookupInstComponentsInModifications {
      
    /**
     * The inherited attribute lookupInstComponentInInstElement defines 
     * the lookup mechanism for left hand component references in modifications.
     * InstComponents are looked up in InstComponentDecl:s sometimes and in InstClassDecl:s
     * sometimes. TODO: this should probably be fixed.
     * 
     */
	inh HashSet InstElementModification.lookupInstComponentInInstElement(String name);
	inh HashSet InstNamedModification.lookupInstComponentInInstElement(String name);
	
	eq InstNamedModification.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstConstrainingClass.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	eq InstConstrainingComponent.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	     
	eq InstComponentDecl.getInstModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name); 
	
	eq InstElementModification.getInstModification().lookupInstComponentInInstElement(String name) = getName().myInstComponentDecl().memberInstComponent(name);
    
	eq InstExtends.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	
//	eq InstShortClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	
	inh HashSet InstComponentRedeclare.lookupInstComponentInInstElement(String name);
	eq InstComponentRedeclare.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstRoot.getChild().lookupInstComponentInInstElement(String name) {return emptyHashSet();}
	eq FlatRoot.getChild().lookupInstComponentInInstElement(String name) {return emptyHashSet();}
	  
}

  