/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect PredefinedTypes {

	inh List FullClassDecl.getPredefinedType();
	eq SourceRoot.getProgram().getPredefinedType() = getProgram().getPredefinedTypeList();

	/* The predefined types Real, Integer etc of Modelica are
	 * much like user-defined classes, in that they have components 
	 * (called attributes). However, the types of attributes are restricted
	 * to RealType, IntegerType, StringType and BooleanType, which corresponds
	 * to machine representations (in the following referred to as built-in types). 
	 * It is therefore convenient to implement the predefined types
	 * as regular classes, in order to reuse the name and type lookup mechanisms.
	 * Some differences apply however. The types of predefined types must
	 * be looked up only amongst the built-in types. Also, there are no 
	 * component accesses that needs to be looked up predefined types.
	 * The predefined types are defined in the (list) non terminal attribute
	 * PredefinedType.
	 */

	/**
	 * This attribute defines the NTA for predefined types
	 * which contains a list of predefined PrimitiveClassDef:s.
	 */
	syn lazy List Program.getPredefinedTypeList() {
		//debugPrint("Program.getPredefinedTypeList()");
		List l = new List();

	    // Build a string with a Modelica class corresponding to Real
		String builtInDef= "type Real\n"; 
		builtInDef += "RealType value=0;\n";
    	builtInDef += "parameter StringType quantity=\"\";\n";
    	builtInDef += "parameter StringType unit=\"\";\n";
    	builtInDef += "parameter StringType displayUnit=\"\";\n";
    	builtInDef += "parameter RealType min=-1e20, max=1e20;\n";
    	builtInDef += "parameter RealType start=0;\n";
    	builtInDef += "parameter BooleanType fixed=false;\n";
    	builtInDef += "parameter RealType nominal=0;\n";
    	builtInDef += "end Real;\n";
    	
    	builtInDef += "type Integer\n"; 
		builtInDef += "IntegerType value=0;\n";
    	builtInDef += "parameter StringType quantity=\"\";\n";
    	builtInDef += "parameter IntegerType min=-1e20, max=1e20;\n";
    	builtInDef += "parameter IntegerType start=0;\n";
    	builtInDef += "parameter BooleanType fixed=\"false\";\n";
    	builtInDef += "end Integer;\n";
    	
		builtInDef += "type Boolean\n"; 
		builtInDef += "BooleanType value=0;\n";
    	builtInDef += "parameter StringType quantity=\"\";\n";
       	builtInDef += "parameter BooleanType start=false;\n";
    	builtInDef += "parameter BooleanType fixed=true;\n";
    	builtInDef += "end Boolean;\n";
    	
    	builtInDef += "type String\n"; 
		builtInDef += "StringType value=0;\n";
    	builtInDef += "parameter StringType quantity=\"\";\n";
       	builtInDef += "parameter StringType start=\"\";\n";
    	builtInDef += "end String;\n"; 	
   
//   		java.io.StringReader builtInDefReader = new java.io.StringReader(builtInDef);
// 	  	ModelicaParser parser = new ModelicaParser();
 		PrimitiveClassDecl pcd=null;
  
   		try {
   		
 //  			ModelicaScanner scanner = new ModelicaScanner(builtInDefReader);
 //  			SourceRoot sr = (SourceRoot)parser.parse(scanner);
   			ParserHandler ph = new ParserHandler();
  			SourceRoot sr = ph.parseString(builtInDef,"");
 			Program p = sr.getProgram();
 //  			builtInDefReader.close();
   			
   			for (int i=0;i<p.getUnstructuredEntity(0).getNumElement();i++) {
   			//debugPrint("Program.getPredefinedTypeList(): Hepp!: " + i);
  			FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(i));	

  			pcd = new PrimitiveClassDecl(new PublicVisibilityType(),
  			                       new Opt(),
  			                       new Opt(),
  			                       cd.getRestriction(),
  			                       cd.getName(),
  			                       new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   cd.getEquationList(),
                                   cd.getAlgorithmList(),
                                   cd.getSuperList(),
                                   cd.getImportList(),
                                   cd.getClassDeclList(),
                                   cd.getComponentDeclList(),
                                   cd.getAnnotationList(),
                                   new Opt(),
                                   cd.getEndName());
 		
 			l.add(pcd);	
		}	
 		
 		} catch(Exception e){e.printStackTrace();}
			
		//debugPrint("Program.getPredefinedTypeList(): "+l.getNumChild());
		
		return l;
		
	}
	
	/**
	 * This attribute defines the NTA for predefined types
	 * which contains a list of predefined PrimitiveClassDecl:s.
	 */
	syn lazy List Program.getBuiltInFunctionList() {

		List l = new List();

   		
 	  	ModelicaParser parser = new ModelicaParser();
  		PrimitiveClassDecl pcd=null;
  
   		try {
   		
   			String builtInFunc = "function der end der;\n"  
                                  +"//function initial end initial;\n"
                                  +"function terminal end terminal;\n"
   		   							+"function smooth end smooth;\n"
+"function noEvent end noEvent;\n"
+"function sample end sample;\n"
+"function pre end pre;\n"
+"function edge end edge;\n"
+"function change end change;\n"
+"function reinit end reinit;\n"
+"function assert end assert;\n"
+"function terminate end terminate;\n"
+"function abs end abs;\n"
+"function sign end sign;\n"
+"function sqrt end sqrt;\n"
+"function div end div;\n"
+"function mod end mod;\n"
+"function rem end rem;\n"
+"function ceil end ceil;\n"
+"function floor end floor;\n"
+"function integer end integer;\n"
+"//function Integer end Integer;\n"
+"//function String end String;\n"
+"function delay end delay;\n"
+"function cardinality end cardinality;\n"
+"function isPresent end isPresent;\n"
+"function semiLinear end semiLinear;\n"
+"function promote end promote;\n"
+"function ndims end ndims;\n"
+"function size end size;\n"
+"function scalar end scalar;\n"
+"function vector end vector;\n"
+"function matrix end matrix;\n"
+"function transpose end transpose;\n"
+"function outerProduct end outerProduct;\n"
+"function identity end identity;\n"
+"function diagonal end diagonal;\n"
+"function zeros end zeros;\n"
+"function ones end ones;\n"
+"function fill end fill;\n"
+"function linspace end linspace;\n"
+"function min end min;\n"
+"function max end max;\n"
+"function sum end sum;\n"
+"function product end product;\n"
+"function symmetric end symmetric;\n"
+"function cross end cross;\n"
+"function skew end skew;\n"
+"function sin \"sine\" end sin;\n"
+"function cos \"cosine\" end cos;\n"
+"function tan \"tangent (u shall not be -pi/2, pi/2, 3*pi/2, ...)\" end tan;\n"
+"function asin \"inverse sine (-1 <= u <= 1)\" end asin;\n"
+"function acos \"inverse cosine (-1 <= u <= 1)\" end acos;\n"
+"function atan \"inverse tangent\" end atan;\n"
+"function atan2 \"four quadrant inverse tangent\" end atan2;\n"
+"function sinh \"hyperbolic sine\" end sinh;\n"
+"function cosh \"hyperbolic cosine\" end cosh;\n"
+"function tanh \"hyperbolic tangent\" end tanh;\n"
+"function exp \"exponential, base e\" end exp;\n"
+"function log \"natural (base e) logarithm (u shall be > 0)\" end log;\n"
+"function log10 \"base 10 logarithm (u shall be > 0)\" end log10;\n";

 //  		   java.io.StringReader reader = new java.io.StringReader(builtInFunc);
   			//Reader reader = new FileReader("builtin/builtin_functions.mo");
//   			ModelicaScanner scanner = new ModelicaScanner(reader);

   			ParserHandler ph = new ParserHandler();
  			SourceRoot sr = ph.parseString(builtInFunc,"");

//   			SourceRoot sr = (SourceRoot)parser.parse(scanner);
   			Program p = sr.getProgram();
 //  			reader.close();
   			
   			for (int i=0;i<p.getUnstructuredEntity(0).getNumElement();i++) {
   			
  			FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(i));	

  			pcd = new PrimitiveClassDecl(new PublicVisibilityType(),
  			                       new Opt(),
  			                       new Opt(),
  			                       cd.getRestriction(),
  			                       cd.getName(),
  			                       new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   cd.getEquationList(),
                                   cd.getAlgorithmList(),
                                   cd.getSuperList(),
                                   cd.getImportList(),
                                   cd.getClassDeclList(),
                                   cd.getComponentDeclList(),
                                   cd.getAnnotationList(),
                                   new Opt(),
                                   cd.getEndName());
 		
 			l.add(pcd);	
		}	
 		
 		} catch(Exception e){}
			
		
		
		return l;
		
	}


	/**
	 * This attribute defines the NTA for predefined types
	 * which contains a list of predefined BuiltInType:s.
	 */
	syn lazy List Program.getBuiltInTypeList() {
		List l = new List();		
		l.add(new BuiltInClassDecl(new IdDecl("RealType")));	
		l.add(new BuiltInClassDecl(new IdDecl("IntegerType")));			
		l.add(new BuiltInClassDecl(new IdDecl("BooleanType")));			
		l.add(new BuiltInClassDecl(new IdDecl("StringType")));				
		return l;
	}
	
	inh ClassDecl Exp.lookupBuiltInFunction(String name);
	eq SourceRoot.getProgram().lookupBuiltInFunction(String name) = unknownClassDecl();
	/*
	rewrite FunctionCall {
   		when (getName().myClassDecl() == lookupBuiltInFunction("size") && 
   		      hasFunctionArguments() &&
   		      getFunctionArguments().getNumExp()==2)
   			to SizeExp {
				return new SizeExp((Access)getFunctionArguments().getExp(0),
				                   getFunctionArguments().getExp(1));
   			}		
   	}
	*/
  
 	
 	boolean PrimitiveClassDecl.rewritten = false;
	rewrite PrimitiveClassDecl {
		when (getName().getID().equals("Real") && !rewritten)
 			to RealClassDecl {
 				RealClassDecl rcd = new RealClassDecl(getVisibilityType(),
 				                                    getEncapsulatedOpt(),
 				                                    getPartialOpt(),
 				                                    getRestriction(),
 				                                    getName(),
 				                                    getRedeclareOpt(),
 				                                    getFinalOpt(),
 				                                    getInnerOpt(),
 				                                    getOuterOpt(),
 				                                    getReplaceableOpt(),
 				                                    getConstrainingClauseOpt(),
 				                                    getConstrainingClauseCommentOpt(),
 				                                    getStringCommentOpt(),
 				                                    getEquationList(),
 				                                    getAlgorithmList(),
 				                                    getSuperList(),
 				                                    getImportList(),
 				                                    getClassDeclList(),
 				                                    getComponentDeclList(),
 				                                    getAnnotationList(),
 				                                    getExternalClauseOpt(),
 				                                    getEndName());
 				rcd.rewritten = true;
 				return rcd;
 			}
 	}
 	
 	rewrite PrimitiveClassDecl {
		when (getName().getID().equals("Integer") && !rewritten)
 			to IntegerClassDecl {
 				IntegerClassDecl rcd = new IntegerClassDecl(getVisibilityType(),
 				                                    getEncapsulatedOpt(),
 				                                    getPartialOpt(),
 				                                    getRestriction(),
 				                                    getName(),
 				                                    getRedeclareOpt(),
 				                                    getFinalOpt(),
 				                                    getInnerOpt(),
 				                                    getOuterOpt(),
 				                                    getReplaceableOpt(),
 				                                    getConstrainingClauseOpt(),
 				                                    getConstrainingClauseCommentOpt(),
 				                                    getStringCommentOpt(),
 				                                    getEquationList(),
 				                                    getAlgorithmList(),
 				                                    getSuperList(),
 				                                    getImportList(),
 				                                    getClassDeclList(),
 				                                    getComponentDeclList(),
 				                                    getAnnotationList(),
 				                                    getExternalClauseOpt(),
 				                                    getEndName());
 				rcd.rewritten = true;
 				return rcd;
 			}
 	}
 	
 	rewrite PrimitiveClassDecl {
		when (getName().getID().equals("Boolean") && !rewritten)
 			to BooleanClassDecl {
 				BooleanClassDecl rcd = new BooleanClassDecl(getVisibilityType(),
 				                                    getEncapsulatedOpt(),
 				                                    getPartialOpt(),
 				                                    getRestriction(),
 				                                    getName(),
 				                                    getRedeclareOpt(),
 				                                    getFinalOpt(),
 				                                    getInnerOpt(),
 				                                    getOuterOpt(),
 				                                    getReplaceableOpt(),
 				                                    getConstrainingClauseOpt(),
 				                                    getConstrainingClauseCommentOpt(),
 				                                    getStringCommentOpt(),
 				                                    getEquationList(),
 				                                    getAlgorithmList(),
 				                                    getSuperList(),
 				                                    getImportList(),
 				                                    getClassDeclList(),
 				                                    getComponentDeclList(),
 				                                    getAnnotationList(),
 				                                    getExternalClauseOpt(),
 				                                    getEndName());
 				rcd.rewritten = true;
 				return rcd;
 			}
 	}
 	
 	 	rewrite PrimitiveClassDecl {
		when (getName().getID().equals("String") && !rewritten)
 			to StringClassDecl {
 				StringClassDecl rcd = new StringClassDecl(getVisibilityType(),
 				                                    getEncapsulatedOpt(),
 				                                    getPartialOpt(),
 				                                    getRestriction(),
 				                                    getName(),
 				                                    getRedeclareOpt(),
 				                                    getFinalOpt(),
 				                                    getInnerOpt(),
 				                                    getOuterOpt(),
 				                                    getReplaceableOpt(),
 				                                    getConstrainingClauseOpt(),
 				                                    getConstrainingClauseCommentOpt(),
 				                                    getStringCommentOpt(),
 				                                    getEquationList(),
 				                                    getAlgorithmList(),
 				                                    getSuperList(),
 				                                    getImportList(),
 				                                    getClassDeclList(),
 				                                    getComponentDeclList(),
 				                                    getAnnotationList(),
 				                                    getExternalClauseOpt(),
 				                                    getEndName());
 				rcd.rewritten = true;
 				return rcd;
 			}
 	}
}

aspect InstPredefinedTypes {

	syn lazy List InstProgramRoot.getInstPredefinedTypeList() {
		List l = new List();
		for (ClassDecl pcd : getProgram().getPredefinedTypes()) {
			l.add(new InstPrimitiveClassDecl((FullClassDecl)pcd,new Opt()));
		}
		return l;
	}

	syn lazy List InstProgramRoot.getInstBuiltInTypeList() {
		List l = new List();
		for (ClassDecl pcd : getProgram().getBuiltInTypes()) {
			l.add(new InstBuiltInClassDecl((BuiltInClassDecl)pcd));
		}
		return l;
	}

	syn lazy List InstProgramRoot.getInstBuiltInFunctionList() {
		List l = new List();
		for (ClassDecl bcd : getProgram().getBuiltInFunctions()) {
			l.add(new InstFullClassDecl((FullClassDecl)bcd, new Opt()));
		}
		return l;
	}
}

aspect AnnotationTypes {
	
}
