<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Check Simple Vector Concept</title>
<meta name="description" id="description" content="Check Simple Vector Concept"/>
<meta name="keywords" id="keywords" content=" simple vector check concept "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_checksimplevector_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="simplevector.cpp.xml" target="_top">Prev</a>
</td><td><a href="checksimplevector.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>CheckSimpleVector</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>CheckSimpleVector-&gt;</option>
<option>CheckSimpleVector.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Restrictions</option>
<option>Include</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Check Simple Vector Concept</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<code><font color="blue"><br/>
# include &lt;cppad/check_simple_vector.hpp&gt;</font></code>
<code><span style='white-space: nowrap'><br/>
</span></code><code><font color="blue"><span style='white-space: nowrap'>CheckSimpleVector&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;()</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
The syntax 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;CheckSimpleVector&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;()<br/>
</span></font></code>preforms compile and run time checks that the type specified
by <i>Vector</i> satisfies all the requirements for 
a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with 
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;Scalar</span></a>
.
If a requirement is not satisfied,
a an error message makes it clear what condition is not satisfied.

<br/>
<br/>
<b><big><a name="Restrictions" id="Restrictions">Restrictions</a></big></b>
<br/>
The following extra assumption is made by <code><font color="blue">CheckSimpleVector</font></code>:
If <i>x</i> is a <i>Scalar</i> object
and <i>i</i> is an <code><font color="blue">int</font></code>,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>assigns the object <i>x</i> the value of the
value of <i>i</i>.  
If <i>y</i> is another <i>Scalar</i> object,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>assigns the object <i>x</i> the value of <i>y</i>.


<br/>
<br/>
<b><big><a name="Include" id="Include">Include</a></big></b>
<br/>
The file <code><font color="blue">cppad/check_simple_vector.hpp</font></code> is included by <code><font color="blue">cppad/cppad.hpp</font></code>
but it can also be included separately with out the rest
if the CppAD include files.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file <a href="checksimplevector.cpp.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector.cpp</span></a>

contains an example and test of this function where <i>S</i>
is the same as <i>T</i>.
It returns true, if it succeeds an false otherwise.
The comments in this example suggest a way to change the example
so <i>S</i> is not the same as <i>T</i>.


<hr/>Input File: cppad/check_simple_vector.hpp

</body>
</html>
