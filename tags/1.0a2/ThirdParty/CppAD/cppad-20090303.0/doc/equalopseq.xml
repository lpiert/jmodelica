<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Check if Equal and Correspond to Same Operation Sequence</title>
<meta name="description" id="description" content="Check if Equal and Correspond to Same Operation Sequence"/>
<meta name="keywords" id="keywords" content=" Equalopseq operation equal sequence "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_equalopseq_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="parvar.cpp.xml" target="_top">Prev</a>
</td><td><a href="equalopseq.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>BoolValued</option>
<option>EqualOpSeq</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>BoolValued-&gt;</option>
<option>Compare</option>
<option>NearEqualExt</option>
<option>BoolFun</option>
<option>ParVar</option>
<option>EqualOpSeq</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>EqualOpSeq-&gt;</option>
<option>EqualOpSeq.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Motivation</option>
<option>x</option>
<option>y</option>
<option>b</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
 







<center><b><big><big>Check if Equal and Correspond to Same Operation Sequence</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>b</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;EqualOpSeq(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Determine if two <i>x</i> and <i>y</i> are equal, and
if they are <a href="glossary.xml#Variable" target="_top"><span style='white-space: nowrap'>variables</span></a>
,
determine if they correspond to the same
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.

<br/>
<br/>
<b><big><a name="Motivation" id="Motivation">Motivation</a></big></b>
<br/>
Sometimes it is useful to cache information
and only recalculate when a function's arguments change.
In the case of AD variables, 
it may be important not only when the argument values are equal,
but when they are related to the
<a href="glossary.xml#Tape.Independent Variable" target="_top"><span style='white-space: nowrap'>independent&#xA0;variables</span></a>
 
by the same operation sequence.
After the assignment
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>these two AD objects would not only have equal values,
but would also correspond to the same operation sequence.

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="b" id="b">b</a></big></b>
<br/>
The result <i>b</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;</span></font></code><i><span style='white-space: nowrap'>b</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The result is true if and only if one of the following cases holds:

<ol type="1"><li>
Both <i>x</i> and <i>y</i> are variables 
and correspond to the same operation sequence.
</li><li>

Both <i>x</i> and <i>y</i> are parameters,
<i>Base</i> is an AD type,
and <code><font color="blue"><span style='white-space: nowrap'>EqualOpSeq(&#xA0;Value(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)&#xA0;,&#xA0;Value(</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)&#xA0;)</span></font></code> is true.
</li><li>

Both <i>x</i> and <i>y</i> are parameters,
<i>Base</i> is not an AD type,
and <code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;==&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"></font></code> is true.
</li></ol>



<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="equalopseq.cpp.xml" target="_top"><span style='white-space: nowrap'>EqualOpSeq.cpp</span></a>

contains an example and test of <code><font color="blue">EqualOpSeq</font></code>.
It returns true if it succeeds and false otherwise.



<hr/>Input File: cppad/local/equal_op_seq.hpp

</body>
</html>
