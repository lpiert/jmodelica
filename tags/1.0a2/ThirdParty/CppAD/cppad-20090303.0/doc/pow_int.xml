<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>The Integer Power Function</title>
<meta name="description" id="description" content="The Integer Power Function"/>
<meta name="keywords" id="keywords" content=" pow integer exponent "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_pow_int_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="nan.cpp.xml" target="_top">Prev</a>
</td><td><a href="poly.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>pow_int</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>pow_int</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Include</option>
<option>x</option>
<option>y</option>
<option>z</option>
<option>Type</option>
<option>Operation Sequence</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>The Integer Power Function</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<code><font color="blue"><br/>
# include &lt;cppad/pow_int.h&gt;</font></code>
<code><span style='white-space: nowrap'><br/>
</span></code><code><font color="blue"></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;pow(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Determines the value of the power function 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>pow</mi>
</mstyle></mrow>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>y</mi>
</msup>
</mrow></math>

for integer exponents <i>n</i> 
using multiplication and possibly division to compute the value.
The other CppAD <a href="pow.xml" target="_top"><span style='white-space: nowrap'>pow</span></a>
 function may use logarithms and exponentiation 
to compute derivatives of the same value
(which will not work if <i>x</i> is less than or equal zero).

<br/>
<br/>
<b><big><a name="Include" id="Include">Include</a></big></b>
<br/>
The file <code><font color="blue">cppad/pow_int.h</font></code> is included by <code><font color="blue">cppad/cppad.hpp</font></code>
but it can also be included separately with out the rest of 
the <code><font color="blue">CppAD</font></code> routines.
Including this file defines
this version of the <code><font color="blue">pow</font></code> within the <code><font color="blue">CppAD</font></code> namespace.

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;int&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="z" id="z">z</a></big></b>
<br/>
The result <i>z</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>z</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="Type" id="Type">Type</a></big></b>
<br/>
The type <i>Type</i> must support the following operations
where <i>a</i> and <i>b</i> are <i>Type</i> objects
and <i>i</i> is an <code><font color="blue">int</font></code>:
<table><tr><td align='left'  valign='top'>

<b>Operation</b>  <code><span style='white-space: nowrap'>&#xA0;&#xA0;</span></code>
	</td><td align='left'  valign='top'>
 <b>Description</b> 
	</td><td align='left'  valign='top'>
 <b>Result Type</b> 
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>a</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> 
	</td><td align='left'  valign='top'>
 construction of a <i>Type</i> object from an <code><font color="blue">int</font></code>
	</td><td align='left'  valign='top'>
 <i>Type</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>a</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;*&#xA0;</span></font></code><i><span style='white-space: nowrap'>b</span></i> 
	</td><td align='left'  valign='top'>
 binary multiplication of <i>Type</i> objects
	</td><td align='left'  valign='top'>
 <i>Type</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>a</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;/&#xA0;</span></font></code><i><span style='white-space: nowrap'>b</span></i> 
	</td><td align='left'  valign='top'>
 binary division of <i>Type</i> objects
	</td><td align='left'  valign='top'>
 <i>Type</i>
</td></tr>
</table>
<br/>
<b><big><a name="Operation Sequence" id="Operation Sequence">Operation Sequence</a></big></b>
<br/>
The <i>Type</i> operation sequence used to calculate <i>z</i> is 
<a href="glossary.xml#Operation.Independent" target="_top"><span style='white-space: nowrap'>independent</span></a>

of <i>x</i>.



<hr/>Input File: cppad/pow_int.hpp

</body>
</html>
