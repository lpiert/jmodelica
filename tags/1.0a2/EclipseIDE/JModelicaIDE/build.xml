
<project name="jmodelica_ide" default="gen">

	<condition property="jmodelica_root" value=".." else="../..">
		<available file="../Compiler/ModelicaFrontEnd/src" type="dir" property=""/>
	</condition>

	<property name="lib_dir" value="lib" />
	<property name="beaver_dir" value="${jmodelica_root}/ThirdParty/Beaver/beaver-0.9.6.1/lib" />
	<property name="jflex_dir" value="${jmodelica_root}/ThirdParty/JFlex/jflex-1.4.3/lib" />
	<property name="jastadd_dir" value="${jmodelica_root}/ThirdParty/JastAdd" />

	<property name="java_dir" value="java" />
	<property name="bin_dir" value="bin" />
	<property name="output_dir" value="site" />

	<property name="jmodelica_dir" value="${jmodelica_root}/Compiler/ModelicaFrontEnd/src" />
	<condition property="core_dir" value="../org.jastadd.plugin" else="${jastadd_dir}/org.jastadd.plugin">
		<available file="../org.jastadd.plugin" type="dir" property=""/>
	</condition>

	<property name="package" value="org.jmodelica" />
	<property name="ast_package" value="${package}.modelica.compiler" />
	<property name="package_dir" value="${java_dir}/org/jmodelica" />
	<!--	
	<property name="jmodelica_test_framework" value="${jmodelica_dir}/test/test-framework" />
	-->
	<property name="jmodelica_java" value="${jmodelica_dir}/java" />
	<property name="scanner_output" value="${package_dir}/generated/scanners" />
	<property name="scanner_dir" value="flex" />

	<property name="parser_output" value="${package_dir}/modelica/parser" />
	<property name="parser_dir" value="${jmodelica_dir}/parser" />

	<property name="ide_aspects" value="aspects" />
	<property name="ide_core_aspects" value="${core_dir}/src" />
	<property name="compiler_aspects" value="${jmodelica_dir}/jastadd" />

	<!-- "jflex" is an ant task class for the scanner generator in JFlex.jar -->
	<!-- "beaver" is an ant task class for the parser generator in beaver.jar -->
	<!-- "jastadd" is an ant task class in jastadd2.jar -->
	<taskdef name="jflex" classname="JFlex.anttask.JFlexTask" classpath="${jflex_dir}/JFlex.jar" />
	<taskdef name="beaver" classname="beaver.comp.run.AntTask" classpath="${beaver_dir}/beaver.jar" />
	<taskdef name="jastadd" classname="jastadd.JastAddTask" classpath="${jastadd_dir}/jastadd2.jar" />

	<!-- for ant-contrib (foreach, etc) -->
	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="${lib_dir}/ant-contrib.jar"/>
		</classpath>
	</taskdef>

	<!-- meta-target for generating scanners -->
	<target name="genscanner" depends="copy,scanner"/>

	<!-- meta-target for generating all java files -->
	<target name="gen" depends="copy,scanner,parser,ast"/>

	<!-- generate compiler ast files -->
	<!-- create AST node types and weave aspect modules -->
	<target name="ast">
		<jastadd package="${ast_package}" license="${ide_aspects}/licence.blurb" beaver="true" rewrite="true" outdir="${java_dir}" NoCacheCycle="false" ComponentCheck="true" visitcheck="false" LazyMaps="true" Deterministic="true" NoStatic="false" Debug="false">
			<fileset dir="${compiler_aspects}">
				<include name="**/*.ast" />
				<include name="**/*.jrag" />
				<include name="**/*.jadd" />
			</fileset>
			<fileset dir="${ide_aspects}">
				<include name="**/*.ast" />
				<include name="**/*.jrag" />
				<include name="**/*.jadd" />
			</fileset>
			<fileset dir="${ide_core_aspects}">
				<include name="**/*.ast" />
				<include name="**/*.jrag" />
				<include name="**/*.jadd" />
			</fileset>
		</jastadd>
	</target>

	<!-- Copy required files from JModelica -->
	<target name="copy">
		<!--
		<copy todir="${java_dir}">
			<fileset dir="${jmodelica_test_framework}">
				<include name="**/*.java" />
			</fileset>
		</copy>
-->
		<copy todir="${java_dir}">
			<fileset dir="${jmodelica_java}">
				<include name="**/*.java" />
			</fileset>
		</copy>
		<copy todir="${lib_dir}">
			<fileset dir="${beaver_dir}">
				<include name="beaver-rt*" />
			</fileset>
		</copy>
	</target>

	<!-- generate the scanners -->
	<target name="scanner">
		<mkdir dir="${parser_output}" />
		<mkdir dir="${scanner_output}" />
		<foreach target="jflex" param="flex_file" inheritall="true" parallel="true">
			<path id="scanners">
				<fileset dir="${scanner_dir}">
					<include name="*.flex"/>
				</fileset>
				<fileset dir="${parser_dir}">
					<include name="*.flex"/>
				</fileset>
			</path>
		</foreach>
	</target>

	<target name="jflex">
		<jflex file="${flex_file}" destdir="${java_dir}" nobak="yes" />
	</target>

	<target name="parser">
		<mkdir dir="${parser_output}" />

		<!-- Modelica parser -->
		<!-- generate the parser phase 1, translating .lalr to .beaver -->
		<concat destfile="${parser_output}/Modelica_all.parser" force="no">
			<filelist dir="." files="${parser_dir}/Modelica_header.parser" />
			<filelist dir="${parser_dir}" files="Modelica.parser" />
		</concat>
		<java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
			<arg line="${parser_output}/Modelica_all.parser ${parser_output}/ModelicaParser_raw.beaver" />
		</java>
		<concat destfile="${parser_output}/ModelicaParser.beaver" force="no">
			<filelist dir="." files="${parser_dir}/beaver.input" />
			<filelist dir="${parser_output}" files="ModelicaParser_raw.beaver" />
		</concat>
		<!-- generate the parser phase 2, translating .beaver to .java -->
		<beaver file="${parser_output}/ModelicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes" />

		<!-- Flat Modelica parser -->
		<jflex file="${parser_dir}/FlatModelica.flex" outdir="${parser_output}" nobak="yes" />
		<!-- generate the parser phase 1, translating .lalr to .beaver -->
		<java classpath="${jastadd_dir}/JastAddParser.jar:${beaver_dir}/lib/beaver-rt.jar" classname="Main">
			<arg line="${parser_dir}/FlatModelica.parser ${parser_output}/FlatModelicaParser_raw.beaver" />
		</java>
		<concat destfile="${parser_output}/FlatModelicaParser.beaver" force="no">
			<filelist dir="." files="${parser_dir}/beaver.flat.input" />
			<filelist dir="${parser_output}" files="FlatModelicaParser_raw.beaver" />
		</concat>
		<!-- generate the parser phase 2, translating .beaver to .java -->
		<beaver file="${parser_output}/FlatModelicaParser.beaver" terminalNames="yes" compress="no" useSwitch="yes" />

		<!-- clean up intermediate files -->
		<delete>
			<fileset dir="${parser_output}">
				<include name="*.parser" />
				<include name="*.beaver" />
			</fileset>
		</delete>
	</target>

	<!-- remove generated and copied java files -->
	<target name="cleanGen" depends="clean">
		<delete dir="${package_dir}/modelica" />
		<delete dir="${scanner_output}" />
		<delete file="${lib_dir}/beaver-rt.jar" />
		<delete file="${lib_dir}/beaver-rt-src.jar" />
	</target>

	<!-- remove compiled files -->
	<target name="clean">
	</target>
</project>


