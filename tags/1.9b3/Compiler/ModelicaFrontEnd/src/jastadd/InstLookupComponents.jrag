/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


aspect InstLookupComponents {

	inh InstComponentDecl InstAccess.lookupInstComponent(String name);	
	inh InstComponentDecl InstResidual.lookupInstComponent(String name);	
	inh lazy InstComponentDecl InstNode.lookupInstComponent(String name);	
	inh lazy InstComponentDecl InstModification.lookupInstComponent(String name);	
	eq InstNode.getChild().lookupInstComponent(String name) = genericLookupInstComponent(name); 
	eq InstRoot.getChild().lookupInstComponent(String name) = null;
	
	eq InstComponentDecl.getInstModification().lookupInstComponent(String name)      = lookupInstComponent(name);
	eq InstComponentDecl.getInstConstraining().lookupInstComponent(String name)      = lookupInstComponent(name);
	eq InstComponentDecl.getConditionalAttribute().lookupInstComponent(String name)  = lookupInstComponent(name);
	eq InstComponentDecl.getFArraySubscripts().lookupInstComponent(String name)      = lookupInstComponent(name);
	eq InstComponentDecl.getLocalFArraySubscripts().lookupInstComponent(String name) = lookupInstComponent(name);

	eq InstAssignable.getBindingFExp().lookupInstComponent(String name) = myInstValueMod().lookupInstComponent(name);
	
	eq InstReplacingComposite.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstReplacingPrimitive.getOriginalInstComponent().lookupInstComponent(String name) = lookupInstComponent(name);
	eq InstNormalExtends.getInstClassModification().lookupInstComponent(String name)      = lookupInstComponent(name);
	eq InstShortClassDecl.getChild().lookupInstComponent(String name)                     = lookupInstComponent(name);
	eq InstSimpleShortClassDecl.getChild().lookupInstComponent(String name)               = lookupInstComponent(name);
    eq InstExternalObject.getDestructorCall().lookupInstComponent(String name)            = lookupInstComponent(name);
	
	eq InstDot.getInstAccess(int i).lookupInstComponent(String name)            = 
			(i == 0) ? lookupInstComponent(name) : getInstAccessNoListTrans(i - 1).qualifiedLookupInstComponent(name);
	eq InstScalarAccess.getExpandedSubscripts().lookupInstComponent(String name) = getTopInstAccess().lookupInstComponent(name);
	eq InstArrayAccess.getFArraySubscripts().lookupInstComponent(String name)   = getTopInstAccess().lookupInstComponent(name);
	eq InstArrayAccess.getExpandedSubscripts().lookupInstComponent(String name) = getTopInstAccess().lookupInstComponent(name);
	eq InstGlobalAccess.getInstAccess().lookupInstComponent(String name)        = null;
	eq InstResidual.getIterationVariable().lookupInstComponent(String name)    = lookupInstComponent(name);
	
	syn InstComponentDecl InstAccess.qualifiedLookupInstComponent(String name) = null;
	eq InstClassAccess.qualifiedLookupInstComponent(String name)               = myInstClassDecl().memberInstComponentConstrained(name);
	eq InstComponentAccess.qualifiedLookupInstComponent(String name)           = 
		lookupArrayElement(myInstComponentDecl()).memberInstComponentConstrained(name);
	eq InstComponentArrayAccess.qualifiedLookupInstComponent(String name)           = 
			lookupArrayElement(myInstComponentDecl()).memberInstComponentConstrained(name);

	inh InstComponentDecl InstAccess.localLookupInstComponent(String name);	
	eq InstRoot.getChild().localLookupInstComponent(String name)          = null;
	eq FlatRoot.getChild().localLookupInstComponent(String name)          = null;
	eq InstClassDecl.getChild().localLookupInstComponent(String name)     = memberInstComponent(name);
	eq InstComponentDecl.getChild().localLookupInstComponent(String name) = memberInstComponent(name);

	/**
	 * Get a specific child access, triggering transformations on that child, but *not* on the entire list.
	 */
	syn InstAccess InstDot.getInstAccessNoListTrans(int i) = (InstAccess) getInstAccessListNoTransform().getChild(i);

	/**
	 * Lookup the specific array component corresponding to this access, using current ceval() 
	 * value for indices. If no specific component can be found or this access is not to a specific 
	 * element, the component for the array is returned.
	 * 
	 * @param array  the component node for the array
	 */
	public InstComponentDecl InstAccess.lookupArrayElement(InstComponentDecl array) {
		return array;
	}
	
	public InstComponentDecl InstArrayAccess.lookupArrayElement(InstComponentDecl array) {
		if (!isArray()) {
			// If we can, try to get the correct InstArrayComponentDecl to do lookup from
			try {
				Index i = getFArraySubscripts().createIndex();
				InstComponentDecl icd = array;
				for (int dim = 0; dim < i.ndims(); dim++) {
					int j = i.get(dim) - 1;
					if (j < 0 || j >= icd.getNumInstComponentDecl()) {
						return array;
					} else {
						icd = icd.getInstComponentDecl(j);
						if (!(icd instanceof InstArrayComponentDecl))
							return array;
					}
				}
				return icd;
			} catch (ConstantEvaluationException e) {
			}
		}
		return array;
	}
	
	/**
	 * Lookup component, re-evaluating any array accesses except in last component.
	 */
	public InstComponentDecl InstAccess.lookupEvaluatingIndices() {
		InstAccess cur = getFirstInstAccess();
		InstComponentDecl icd = cur.myInstComponentDecl();
		
		InstAccess next = cur.getNextInstAccess();
		while (next != null) {
			icd = cur.lookupArrayElement(icd);
			icd = icd.memberInstComponent(next.name());
			cur = next;
			next = next.getNextInstAccess();
		}
		icd = cur.lookupArrayElement(icd);
		
		return icd;
	}
	
	eq SourceRoot.getChild().lookupInstComponent(String name) = null;
	// This equation is necessary since InstAccesses may be present in FExps.	
	eq FlatRoot.getChild().lookupInstComponent(String name) = null;

	inh lazy InstComponentDecl InstForClauseE.lookupInstComponent(String name);
	eq InstForClauseE.getChild().lookupInstComponent(String name) {
		for (InstForIndex ifi : getInstForIndexs()) 
			if (ifi.matches(name))
				return ifi.getInstPrimitive();
		return lookupInstComponent(name);
	}
	
	inh lazy InstComponentDecl InstForStmt.lookupInstComponent(String name);
	eq InstForStmt.getChild().lookupInstComponent(String name) {
		for (InstForIndex ifi : getInstForIndexs()) 
			if (ifi.matches(name))
				return ifi.getInstPrimitive();
		return lookupInstComponent(name);
	}
	
	inh lazy InstComponentDecl FIterExp.lookupInstComponent(String name);
	eq FIterExp.getChild().lookupInstComponent(String name) {
		for (CommonForIndex fi : getForIndexs()) 
			if (fi.matches(name) && fi instanceof InstForIndex)
				return ((InstForIndex) fi).getInstPrimitive();
		return lookupInstComponent(name);
	}
	
	syn lazy InstComponentDecl InstNode.genericLookupInstComponent(String name) {
		InstComponentDecl icd = memberInstComponent(name);
		if (icd != null)
			return icd;
		for (InstImport ii : instImports()) {
			icd = ii.lookupInstConstantInImport(name);
			if (icd != null)
				return icd;
		}
		return genericLookupInstConstant(name);
	}
	eq InstSimpleShortClassDecl.genericLookupInstComponent(String name) = actualInstClass().genericLookupInstComponent(name);
	
	syn InstComponentDecl InstNode.genericLookupInstConstant(String name) = lookupInstConstant(name);
	eq InstComponentDecl.genericLookupInstConstant(String name)           = myInstClass().lookupInstConstant(name);
	eq InstExtends.genericLookupInstConstant(String name)                 = myInstClass().lookupInstConstant(name);
	
	/**
	 * \brief Check if any constraining class/component has the given component.
	 */
	syn boolean InstNode.constrainMemberInstComponent(String name) {
		if (!hasInstConstraining())
			return true;
		return getInstConstraining().getInstNode().memberInstComponent(name) != null;
	}
	
	syn InstComponentDecl InstNode.arrayMemberInstComponent(String name, int ndims) {
		if (ndims == 0) 
			return memberInstComponent(name);
		if (getNumInstComponentDecl() == 0)
			return emptyArrayMemberInstComponent(name);
		return getInstComponentDecl(0).arrayMemberInstComponent(name, ndims - 1);
	}
	eq InstSimpleShortClassDecl.arrayMemberInstComponent(String name, int ndims) = actualInstClass().arrayMemberInstComponent(name, ndims);
	
	syn InstComponentDecl InstNode.emptyArrayMemberInstComponent(String name) = null;
	eq InstComponentDecl.emptyArrayMemberInstComponent(String name)           = 
		size().isUnknown() ? myInstClass().memberInstComponent(name) : null;
	
	syn lazy InstComponentDecl InstNode.memberInstComponent(String name)  {
		if (isArray()) {
			return arrayMemberInstComponent(name, ndims());
		} else {	
			for (InstComponentDecl ic : instComponentDecls()) 
				if (ic.matches(name))
					return ic;

			for (InstExtends ie : instExtends()) {
				InstComponentDecl icd = ie.memberInstComponent(name);
				if (icd != null)
					return icd;
			}
			
			return null;
		}
	}
	eq InstSimpleShortClassDecl.memberInstComponent(String name) = actualInstClass().memberInstComponent(name);
	
	syn InstComponentDecl InstNode.memberInstComponentConstrained(String name)  {
		if (isArray() || constrainMemberInstComponent(name)) 
			return memberInstComponent(name);
		else
			return null;
	}
	
	inh lazy InstComponentDecl InstNode.lookupInstConstant(String name);
	eq Root.getChild().lookupInstConstant(String name)     = null;
	eq InstNode.getChild().lookupInstConstant(String name) = memberInstConstantFirst(name);
	
	syn InstComponentDecl InstNode.memberInstConstantFirst(String name) {
		InstComponentDecl icd = memberInstConstantWithExtends(name);
		if (icd != null)
			return icd;
		
		for (InstImport ii : instImports()) {
			icd = ii.lookupInstConstantInImport(name);
			if (icd != null)
				return icd;

		}

		return lookupInstConstant(name);
	}
	eq InstSimpleShortClassDecl.memberInstConstantFirst(String name) = actualInstClass().memberInstConstantFirst(name);
	
	syn lazy InstComponentDecl InstNode.memberInstConstantWithExtends(String name) {
		InstComponentDecl icd = memberInstConstant(name);
		if (icd != null)
			return icd;
		
		for (InstExtends ie : instExtends()) {
			icd = ie.memberInstConstantWithExtends(name);
			if (icd != null)
				return icd;
		}

		return null;
	}
	eq InstSimpleShortClassDecl.memberInstConstantWithExtends(String name) = actualInstClass().memberInstConstantWithExtends(name);
	
	syn lazy InstComponentDecl InstNode.memberInstConstant(String name) {
		if (!constrainMemberInstComponent(name)) 
			return null;
		
		for (InstComponentDecl ic : instComponentDecls()) 
			if (ic.getComponentDecl().isConstant() && ic.matches(name))
				return ic;

		return null;
	}
	eq InstSimpleShortClassDecl.memberInstConstant(String name) = 
		constrainMemberInstComponent(name) ? actualInstClass().memberInstConstant(name) : null;

	syn InstComponentDecl InstImport.lookupInstConstantInImport(String name) {
		// Assume import points to a single (constant) component
	    if (name.equals(name())) {
	    	String className = getPackageName().enclosingName();
	    	if (!className.equals("")) {
		    	InstClassDecl icd = ((SourceRoot)root()).getProgram().getInstProgramRoot().
		           simpleLookupInstClassDecl(className);
		    	return icd.memberInstConstant(getPackageName().getLastInstAccess().name());
	    	}
		}
		return null;
	}
	
	eq InstImportUnqualified.lookupInstConstantInImport(String name)  {
        return getImportedClass().memberInstConstant(name);
	}

	// This is needed since the member components of InstPrimitive:s (which are attributes)
	// are not instantiated
	eq InstPrimitive.memberInstComponent(String name) = myInstClass().memberInstComponent(name);
	eq InstExtends.memberInstComponent(String name)   = 
			extendsPrimitive() ? myInstClass().memberInstComponent(name) : super.memberInstComponent(name);

	eq InstComponentDecl.matches(String str) = name().equals(str);
	eq InstForIndex.matches(String str)      = getInstPrimitive().matches(str);
	
	syn InstComponentDecl InstAccess.myInstComponentDecl() = unknownInstComponentDecl();
	eq InstDot.myInstComponentDecl()                       = getLastInstAccess().myInstComponentDecl();
	eq InstGlobalAccess.myInstComponentDecl()              = getInstAccess().myInstComponentDecl();
	syn lazy InstComponentDecl InstComponentAccess.myInstComponentDecl() {
		InstComponentDecl icd = lookupInstComponent(name());
		return (icd != null) ? icd : unknownInstComponentDecl();
	}
	
	syn lazy InstComponentDecl InstComponentArrayAccess.myInstComponentDecl() {
		InstComponentDecl icd = lookupInstComponent(name());
		return (icd != null) ? icd : unknownInstComponentDecl();
	}
	
}

aspect LookupInstComponentsInModifications {
      
    /**
     * The inherited attribute lookupInstComponentInInstElement defines 
     * the lookup mechanism for left hand component references in modifications.
     * InstComponents are looked up in InstComponentDecl:s sometimes and in InstClassDecl:s
     * sometimes. TODO: this should probably be fixed.
     * 
     */
	inh InstComponentDecl InstElementModification.lookupInstComponentInInstElement(String name);
	inh InstComponentDecl InstNamedModification.lookupInstComponentInInstElement(String name);
	
	eq InstNamedModification.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstConstrainingClass.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponentConstrained(name);
	eq InstConstrainingComponent.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponentConstrained(name);
	     
	eq InstComponentDecl.getInstModification().lookupInstComponentInInstElement(String name) = myInstClass().memberInstComponentConstrained(name); 
	
	eq InstElementModification.getInstModification().lookupInstComponentInInstElement(String name) = getName().qualifiedLookupInstComponent(name);
    
	eq InstNormalExtends.getInstClassModification().lookupInstComponentInInstElement(String name) = myInstClass().memberInstComponentConstrained(name);
	
//	eq InstShortClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) = getClassName().myInstClassDecl().memberInstComponent(name);
	
	inh InstComponentDecl InstComponentRedeclare.lookupInstComponentInInstElement(String name);
	eq InstComponentRedeclare.getName().lookupInstComponent(String name) = lookupInstComponentInInstElement(name);
	
	eq InstRoot.getChild().lookupInstComponentInInstElement(String name) = null;
	eq FlatRoot.getChild().lookupInstComponentInInstElement(String name) = null;
	  
}

aspect SimpleLookupInstComponentDecl {
	syn InstComponentDecl InstNode.simpleLookupInstComponentDecl(String name) {
		for (InstComponentDecl icd : getInstComponentDecls()) {
			if (icd.name().matches(name))
				return icd;
		}
		for (InstExtends ie : getInstExtendss()) {
			InstComponentDecl match = ie.simpleLookupInstComponentDecl(name);
			if (match != null)
				return match;
		}
		return null;
	}
}

  