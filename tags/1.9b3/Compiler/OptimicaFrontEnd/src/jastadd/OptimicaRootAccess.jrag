/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect OptimicaRootAccess {
	
	refine RootAccess eq SourceRoot.language() = "Optimica";
    
	refine RootAccess public static beaver.Parser ParserHandler.createModelicaParser(AbstractModelicaScanner scanner) {
    	org.jmodelica.optimica.parser.ModelicaParser parser = 
    		new org.jmodelica.optimica.parser.ModelicaParser();
        parser.setLineBreakMap(scanner.getLineBreakMap());
		return parser;
    }
    
	refine RootAccess public static AbstractModelicaScanner ParserHandler.createModelicaScanner(Reader in) {
    	return new org.jmodelica.optimica.parser.ModelicaScanner(in);
    }
    
	refine RootAccess public static AbstractFlatModelicaScanner ParserHandler.createModelicaFlatScanner(Reader in) {
    	return new org.jmodelica.optimica.parser.FlatModelicaScanner(in);
    }
    
	refine RootAccess public static short ParserHandler.expModelicaGoal() {
    	return org.jmodelica.optimica.parser.ModelicaParser.AltGoals.exp;
    }

    public static beaver.Parser ParserHandler.createOptimicaParser(AbstractModelicaScanner scanner) {
    	org.jmodelica.optimica.parser.OptimicaParser parser = 
    		new org.jmodelica.optimica.parser.OptimicaParser();
        parser.setLineBreakMap(scanner.getLineBreakMap());
        parser.setFormattingInfo(scanner.getFormattingInfo());
		return parser;
    }
    
    public static AbstractModelicaScanner ParserHandler.createOptimicaScanner(Reader in) {
    	return new org.jmodelica.optimica.parser.OptimicaScanner(in);
    }
    
	public static short ParserHandler.expOptimicaGoal() {
    	return org.jmodelica.optimica.parser.OptimicaParser.AltGoals.exp;
    }
	
	refine RootAccess public SourceRoot ParserHandler.parseFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		return parseOptimicaFile(fileName);
	}

	refine RootAccess public SourceRoot ParserHandler.parseString(String str, String fileName)  
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		return parseOptimicaString(str, fileName);
	}
	
	refine RootAccess public Exp ParserHandler.parseExpString(String str) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		return parseOptimicaExpString(str);
	}

	public SourceRoot ParserHandler.parseOptimicaFile(String fileName) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		Reader reader = fileReader(fileName);
		AbstractModelicaScanner scanner = createOptimicaScanner(new BufferedReader(reader));
		beaver.Parser parser = createOptimicaParser(scanner);
		SourceRoot sr = (SourceRoot) parser.parse(scanner);
		sr.setFileName(fileName);
		reader.close();
		return sr;
	}

	public SourceRoot ParserHandler.parseOptimicaString(String str, String fileName)  
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		SourceRoot sr = null;
		AbstractModelicaScanner scanner = createOptimicaScanner(new StringReader(str));
		beaver.Parser parser = createOptimicaParser(scanner);
		sr = (SourceRoot) parser.parse(scanner);
		sr.setFileName(fileName);
		return sr;
	}
	
	public Exp ParserHandler.parseOptimicaExpString(String str) 
	   throws ParserException, beaver.Parser.Exception, FileNotFoundException, IOException {
		Exp exp = null;
		AbstractModelicaScanner scanner = createOptimicaScanner(new StringReader(str));
		beaver.Parser parser = createOptimicaParser(scanner);
		exp = (Exp) parser.parse(scanner, expOptimicaGoal());
		//log.debug("OptimicaParser.parseExpString: "+str);
		//exp.dumpTreeBasic("");		
		return exp;
	}
	
	
}