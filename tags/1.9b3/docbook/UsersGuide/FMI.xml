<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xml:id="ch_fmi" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title xml:id="sec_fmi">FMI Interface</title>

  <para>FMI (Functional Mock-up Interface) is a standard for exchanging models
  between different modeling and simulation environments. FMI defines a model
  execution interface consisting of a set of C-function signatures for
  handling the communication between the model and a simulation environment.
  Models are presented as ODEs with time, state and step events. FMI also
  specifies that all information related to a model, except the equations,
  should be stored in an XML formated text-file. The format is specified in
  the standard and specifically contains information about the variables,
  names, identifiers, types and start attributes.</para>

  <para>A model is distributed in a zip-file with the extension '.fmu',
  containing several files. These zip-files containing the models are called
  FMUs (Functional Mock-up Units). The important files in an FMU are mainly
  the XML-file, which contains the definitions of all variables and then files
  containing the C-functions which can be provided in source and/or binary
  form. FMI standard also supports providing documentation and resources
  together with the FMU. For more information regarding the FMI standard,
  please visit http://www.functional-mockup-interface.org/.</para>

  <section>
    <title>Overview of JModelica.org FMI Python package</title>

    <para>The JModelica.org interface to FMI is written in Python and is
    intended to be a close copy of the defined C-interface for an FMU and
    provides classes and functions for interacting with FMUs.</para>

    <para>The JModelica.org platform offers a Pythonic and convenient
    interface for FMUs which can be used to connect other simulation software.
    JModelica.org also offers a connection to Assimulo, the default simulation
    package included in JModelica.org so that FMUs can easily be
    simulated.</para>

    <para>The interface is located in <literal>pyfmi.fmi</literal> and consist
    of the class <literal>FMUModel</literal> together with methods for
    unzipping the FMU and for writing the simulation results. Connected to
    this interface is a wrapper for JModelica.org's simulation package to
    enable an easy simulation of the FMUs. The simulation wrapper is located
    in <literal>pyfmi.simulation.assimulo</literal>,
    <literal>FMIODE</literal>.</para>

    <para>In the table below is a list of the FMI C-interface and its
    counterpart in the JModelica.org Python package. We have adapted the name
    convention of lowercase letters and underscores separating words. For
    methods with no calculations, as for example
    <literal>fmi(Get/Set)ContinuousStates</literal> they are instead of
    different methods, connected with a property. In the table, a lack of
    parenthesis indicates that the method is instead a property.</para>

    <table>
      <title>Conversion table.</title>

      <tgroup align="left" cols="2">
        <thead>
          <row>
            <entry align="center">FMI C-Interface</entry>

            <entry align="center">JModelica.org FMI Python Interface</entry>
          </row>
        </thead>

        <tbody>
          <row>
            <entry>const char* fmiGetModelTypesPlatform()</entry>

            <entry>string FMUModel.model_types_platform</entry>
          </row>

          <row>
            <entry>const char* fmiGetVersion()</entry>

            <entry>string FMUModel.version</entry>
          </row>

          <row>
            <entry>fmiComponent fmiInstantiateModel(...)</entry>

            <entry>FMUModel.__init__()</entry>
          </row>

          <row>
            <entry>void fmiFreeModelInstance(fmiComponent c)</entry>

            <entry>FMUModel.__del__()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetDebugLogging(...)</entry>

            <entry>none FMUModel.set_debug_logging(flag)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetTime(...)</entry>

            <entry>FMUModel.time</entry>
          </row>

          <row>
            <entry>fmiStatus fmi(Get/Set)ContinuousStates(...)</entry>

            <entry>FMUModel.continuous_states</entry>
          </row>

          <row>
            <entry>fmiStatus fmiCompletedIntegratorStep(...)</entry>

            <entry>boolean FMUModel.completed_integrator_step()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiSetReal/Integer/Boolean/String(...)</entry>

            <entry>none
            FMUModel.set_real/integer/boolean/string(valueref,values)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiInitialize(...)</entry>

            <entry>none FMUModel.initialize() (also sets the start
            attributes)</entry>
          </row>

          <row>
            <entry>struct fmiEventInfo</entry>

            <entry>FMUModel.get_event_info()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetDerivatives(...)</entry>

            <entry>numpy.array FMUModel.get_derivatives()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetEventIndicators(...)</entry>

            <entry>numpy.array FMUModel.get_event_indicators()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetReal/Integer/Boolean/String(...)</entry>

            <entry>numpy.array
            FMUModel.get_real/integer/boolean/string(valueref)</entry>
          </row>

          <row>
            <entry>fmiStatus fmiEventUpdate(...)</entry>

            <entry>none FMUModel.event_update()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetNominalContinuousStates(...)</entry>

            <entry>FMUModel.nominal_continuous_states</entry>
          </row>

          <row>
            <entry>fmiStatus fmiGetStateValueReferences(...)</entry>

            <entry>numpy.array FMUModel.get_state_value_references()</entry>
          </row>

          <row>
            <entry>fmiStatus fmiTerminate(...)</entry>

            <entry>FMUModel.__del__()</entry>
          </row>
        </tbody>
      </tgroup>
    </table>

    <para>If logging is set to <literal>True</literal> the log can be
    retrieved with the method,</para>

    <programlisting language="python">FMUModel.get_log()
</programlisting>

    <para>Documentation of the functions can also be accessed interactively
    from IPython by using for instance,</para>

    <programlisting language="python">FMUModel.get_real?
</programlisting>

    <para>There is also a one-to-one map to the C-functions, meaning that
    there is an option to use the low-level C-functions as they are specified
    in the standard instead of using our wrapping of the functions. These
    functions are also located in <literal>FMUModel</literal> and is named
    with a leading underscore together with the same name as specified in the
    standard.</para>
  </section>
</chapter>
