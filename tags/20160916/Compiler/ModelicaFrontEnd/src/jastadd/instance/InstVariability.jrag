/*
    Copyright (C) 2014-2015 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

aspect InstVariability {

    syn lazy FTypePrefixVariability InstComponentDecl.definedVariability() = 
        overrideVariability(localDefinedVariability(), bExpVariability());

    syn lazy FTypePrefixVariability InstComponentDecl.localDefinedVariability() {
        ComponentDecl cd = getComponentDecl();
        return cd.hasTypePrefixVariability() ? cd.getTypePrefixVariability().flatten() : noDefinedVariability();
    }

    syn FTypePrefixVariability InstComponentDecl.noDefinedVariability() = fContinuous();
    eq InstReplacingComposite.noDefinedVariability() = getOriginalInstComponent().localDefinedVariability();
    eq InstReplacingRecord.noDefinedVariability()    = getOriginalInstComponent().localDefinedVariability();
    eq InstReplacingPrimitive.noDefinedVariability() = getOriginalInstComponent().localDefinedVariability();

    inh FTypePrefixVariability InstNode.overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar);
    eq InstBaseNode.getChild().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var;
    eq Root.getChild().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var;
    eq InstPrimitive.getChild().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var;
    eq InstComponentDecl.getInstComponentDecl().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var.combineDown(definedVariability());
    eq InstComponentDecl.getInstExtends().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var.combineDown(definedVariability());
    eq InstRecordConstructor.getInstComponentDecl().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var.combineDown(bexpVar);
    eq InstRecordConstructor.getInstExtends().overrideVariability(
            FTypePrefixVariability var, FTypePrefixVariability bexpVar) = var.combineDown(bexpVar);

    syn boolean InstComponentDecl.isConstant()   = variability().constantVariability();
    syn boolean InstComponentDecl.isParameter()  = variability().parameterVariability();
    syn boolean InstComponentDecl.isDiscrete()   = variability().discreteVariability();
    syn boolean InstComponentDecl.isContinuous() = variability().continuousVariability();
    
    // This attribute should only have one equation, sometimes we flush it manually.
    syn lazy FTypePrefixVariability InstComponentDecl.variability() = calcVariability();
    
    syn FTypePrefixVariability InstComponentDecl.calcVariability() = definedVariability().combineDown(defaultVariability());
    
    eq InstEnumLiteral.calcVariability()                           = fConstant();
    eq UnknownInstComponentDecl.calcVariability()                  = fConstant();
    eq InstExternalObject.calcVariability()                        = fParameter();
    
    eq InstAssignable.calcVariability() {
        FTypePrefixVariability v =  super.calcVariability();
        if (v.parameterVariability()) {
            
            if (isSetAsInitial()) {
                v = fInitialParameter();
            } else if (!isRecord() && isEvalAnnotated()) {
                v = fEvalParameter();
            } else if (isFinalIndependent()) {
                v = fFinalParameter();
            }
            
            v = v.combineDown(forcedVariability);
            v = bExpVariability().combinePropagate(v);
        }
        return v;
    }
    
    eq InstRecord.calcVariability() {
        InstComponentDecl rec = this;
        for (int i = 0; i < ndims(); i++) {
            if (rec.getNumInstComponentDecl() == 0)
                return fContinuous();
            rec = rec.getInstComponentDecl(0);
        }
        List<FComponentVariability> l = new List<FComponentVariability>();
        for (InstComponentDecl icd : rec.allInstComponentDecls()) {
            l.add(new FComponentVariability(icd.name(), icd.variability()));
        }
        return new FCompositeVariability(l).combineInherit(super.calcVariability());
    }
    
    syn boolean InstAssignable.isFinalIndependent() {
        InstValueModification mod = myInstValueMod();
        return mod != null && mod.isFinal() && mod.getFExp().isIndependentParameterExp();
    }
    syn boolean InstAssignable.isEvalAnnotated() = annotation().forPath("Evaluate").bool() || inEvalAnnotated();
    inh boolean InstComponentDecl.inEvalAnnotated();
    eq InstClassDecl.getChild().inEvalAnnotated()          = false;
    eq InstComponentDecl.getChild().inEvalAnnotated()      = false;
    eq InstAssignable.getChild().inEvalAnnotated()         = isEvalAnnotated();
    eq InstArrayComponentDecl.getChild().inEvalAnnotated() = inEvalAnnotated();
    
    syn boolean InstAssignable.isSetAsInitial() = isPrimitive() && 
            attributeCValueSet(defaultVariableEvaluator(), FAttribute.FIXED) != null && 
            !fixedAttributeCValue().reduceBooleanAnd();

    syn FTypePrefixVariability InstComponentDecl.defaultVariability() = fContinuous();
    eq InstArrayComponentDecl.defaultVariability()                    = parentDefaultVariability();
    eq InstPrimitive.defaultVariability() {
        if (isReal())
            return fContinuous();
        else if (isExternalObject())
            return fParameter();
        else
            return fDiscrete();
    }
    eq InstRecord.defaultVariability() = fContinuous();
    
    // Parameters variabilities also depend on binding expressions since its nice
    // to propagate known values, these include: 
    // * Structural parameters, marked during error checking.
    // * Evaluate=true parameters
    // * Final independent parameters
    //
    // Since this is used before/during error checks and depend on expressions
    // we have to guard agains circularity.
    private boolean InstAssignable.circularVariability = false;
    
    syn FTypePrefixVariability InstComponentDecl.bExpVariability() = fContinuous();
    eq InstAssignable.bExpVariability() {
        FTypePrefixVariability v = super.bExpVariability();
        if (!circularVariability) {
            InstValueModification ivm = myInstValueMod();
            if (ivm != null && !ivm.getFExp().type().isUnknown()) {
                circularVariability = true;
                v = ivm.getFExp().variability();
                circularVariability = false;
            }
        }
        return v;
    }
    
    inh FTypePrefixVariability InstComponentDecl.parentBExpVariability();
    inh FTypePrefixVariability InstExtends.parentBExpVariability();
    eq Root.getChild().parentBExpVariability()  = fContinuous();
    eq InstBaseNode.getChild().parentBExpVariability()  = fContinuous();
    eq InstComponentDecl.getInstComponentDecl().parentBExpVariability() = bExpVariability();
    eq InstComponentDecl.getInstExtends().parentBExpVariability() = bExpVariability();
    
    /**
     * Combines variabilities with rules required for propagating variabilities.
     */
    syn FTypePrefixVariability FTypePrefixVariability.combinePropagate(FTypePrefixVariability other) = other;
    eq FCompositeVariability.combinePropagate(FTypePrefixVariability other) = combine(other, propagateCombiner);
    eq FConstant.combinePropagate(FTypePrefixVariability other)             = other.combineDown(fFixedParameter());
    eq FParameter.combinePropagate(FTypePrefixVariability other)            = other.combineDown(this);
    eq FInitialParameter.combinePropagate(FTypePrefixVariability other)     = this;
    eq FEvalParameter.combinePropagate(FTypePrefixVariability other)        =
            fStructParameter().combinePropagate(other);
    
    class FCompositeVariability {
        protected static Combiner propagateCombiner = new Combiner() {
            @Override
            public FTypePrefixVariability combine(FTypePrefixVariability v1, FTypePrefixVariability v2) {
                return v1.combinePropagate(v2);
            }
        };
    }
    
    syn FTypePrefixVariability FTypePrefixVariability.combineInherit(FTypePrefixVariability other) =
            other.combineDown(this);
    eq FCompositeVariability.combineInherit(FTypePrefixVariability other) = combine(other, inheritCombiner);
    eq FInitialParameter.combineInherit(FTypePrefixVariability other)     =
            other.parameterVariability() ? this : super.combineInherit(other);
    
    class FCompositeVariability {
        protected static Combiner inheritCombiner = new Combiner() {
            @Override
            public FTypePrefixVariability combine(FTypePrefixVariability v1, FTypePrefixVariability v2) {
                return v1.combineInherit(v2);
            }
        };
    }
    
    /**
     * The variability of the surrounding component, if any (null otherwise).
     */
    inh FTypePrefixVariability InstComponentDecl.parentDefaultVariability();
    eq InstComponentDecl.getChild().parentDefaultVariability() = variability();
    eq InstClassDecl.getChild().parentDefaultVariability()     = null;
    eq Root.getChild().parentDefaultVariability()              = null;
    
    
    protected static Set<InstComponentDecl> FAbstractEquation.assignedSetFromEqns(List<FAbstractEquation> eqns) {
        Set<InstComponentDecl> res = new HashSet<InstComponentDecl>();
        for (FAbstractEquation eqn : eqns)
            res.addAll(eqn.assignedSet());
        return res;
    }

    /**
     * Gives the set of components assigned in this equation.
     * 
     * Only works in instance tree.
     * For if and when equations, only the first branch is considered.
     */
    syn lazy Set<InstComponentDecl> FAbstractEquation.assignedSet() = Collections.emptySet();
    eq InstForClauseE.assignedSet()      = assignedSetFromEqns(getFAbstractEquations());
    eq FIfWhenElseEquation.assignedSet() = assignedSetFromEqns(getFAbstractEquations());
    eq FEquation.assignedSet()           = getLeft().accessedVarSet();
    eq FFunctionCallEquation.assignedSet() {
        LinkedHashSet<InstComponentDecl> s = new LinkedHashSet<InstComponentDecl>();
        for (FFunctionCallLeft left : getLefts()) {
            if (left.hasFExp()) {
                s.addAll(left.getFExp().accessedVarSet());
            }
        }
        return s;
    }

    /**
     * If this is an instance tree access, return set containing accessed var, otherwise empty set.
     */
    syn Set<InstComponentDecl> FExp.accessedVarSet() = Collections.emptySet();
    eq FIdUseExp.accessedVarSet()                    = getFIdUse().accessedVarSet();

    /**
     * If this is an instance tree access, return set containing accessed var, otherwise empty set.
     */
    syn Set<InstComponentDecl> FIdUse.accessedVarSet() = Collections.emptySet();
    eq FIdUseInstAccess.accessedVarSet()               = getInstAccess().accessedVarSet();

    /**
     * Get set containing accessed var.
     */
    syn Set<InstComponentDecl> InstAccess.accessedVarSet() = 
        Collections.singleton(myInstComponentDecl());


    eq FIdUseInstAccess.variability() = getInstAccess().myInstComponentDecl().variability().combine(indexVariability());

    eq FIdUseInstAccess.indexVariability() = getInstAccess().indexVariability();

    syn FTypePrefixVariability InstAccess.indexVariability() = fConstant();
    eq InstArrayAccess.indexVariability()                    = getFArraySubscripts().variability();
    eq InstDot.indexVariability() {
        FTypePrefixVariability variability = fConstant();
        for (InstAccess part : getInstAccesss())
            variability = variability.combine(part.indexVariability());
        return variability;
    }

}


aspect SourceVariability {

    syn boolean TypePrefixVariability.constantVariability() = false;
    eq Constant.constantVariability() = true;   
    syn boolean TypePrefixVariability.parameterVariability() = false;
    eq Parameter.parameterVariability() = true; 
    syn boolean TypePrefixVariability.discreteVariability() = false;
    eq Discrete.discreteVariability() = true;   
    syn boolean TypePrefixVariability.continuousVariability() = false;
    eq Continuous.continuousVariability() = true;

}
