import java.util.ArrayList;

/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/** \file DAETag.java
 *  DAETag class.
 */


/**
 * A tag class intended for use as base class for tags using the DAE
 * interface.
 *
 */
public abstract class DAETag extends AbstractTag {
	/**
	 * FClass object.
	 */
	protected FClass fclass;
    
    private String[] options;
    private boolean optionsChecked;
    private boolean active;
	
	/**
	 * Default constructor.
	 * 
	 * @param name Tag name.
	 * @param myGenerator The tag's generator.
	 * @param fclass An FClass object used as a basis in the code generation.
	 */
	public DAETag(String name, AbstractGenerator myGenerator, FClass fclass) {
		super(name, myGenerator);
		this.fclass = fclass;
        options = new String[0];
        optionsChecked = false;
	}
    
    /**
     * Set list of options that must be true for tag to be active.
     * 
     * If an option is prefixed with '!', then it must be false instead.
     */
    protected void setOptions(String... opts) {
        options = opts;
    }
	
	/**
	 * Add options to list of options that must be true for tag to be active.
     * 
     * If an option is prefixed with '!', then it must be false instead.
	 */
	protected void addOptions(String... opts) {
	    if (options.length == 0) {
	        options = opts;
	    } else {
	        String[] old = options;
	        options = new String[old.length + opts.length];
            System.arraycopy(old, 0, options, 0, old.length);
            System.arraycopy(opts, 0, options, old.length, opts.length);
	    }
	}
    
    /**
     * Check if this tag should be generated.
     */
    public boolean isActive() {
        if (!optionsChecked) 
            active = checkOptions();
        return active;
    }
    
    /**
     * Check if all options needed are active.
     */
    private boolean checkOptions() {
        optionsChecked = true;
        for (String opt : options) {
            boolean flip = false;
            if (opt.charAt(0) == '!') {
                flip = true;
                opt = opt.substring(1);
            }
            if (fclass.myOptions().getBooleanOption(opt) == flip)
                return false;
        }
        return true;
    }
	
	/**
	 * Get the FClass object.
	 * @return The FClass object.
	 */
	public FClass getFClass() {
		return fclass;
	}
	
	/**
	 * Instantiates all inner classes, of this tag, that inherits AbstractTag and adds them to the tag map.
	 */
	protected void instantiateTags(Map<String,AbstractTag> map) {
		instantiateTags(getClass(), map, this, myGenerator, fclass);
	}

    public abstract class CodeSplitter<T> {
        private int itemIndex = 0;
        private int itemLimit = 1000;
        private int splitCount = 0;
        private String funcName;
        protected CodeStream str;
        protected CodePrinter p;
        protected String indent;
        protected boolean allowDirect;
        
        private ArrayList<T> elements = new ArrayList<T>();
        
        public CodeSplitter(CodePrinter p, CodeStream str, String indent, boolean allowDirect, String funcName) {
            this(p, str, indent, allowDirect, funcName, new ArrayList<T>());
        }
        
        public CodeSplitter(CodePrinter p, CodeStream str, String indent,
                boolean allowDirect, String funcName, ArrayList<T> elements) {
            this.funcName = funcName;
            this.str = str;
            this.p = p;
            this.indent = indent;
            this.elements = elements;
            this.allowDirect = allowDirect;
        }
        
        public Collection<T> getElements() {
            return elements;
        }

        public void add(Collection<T> coll) {
            elements.addAll(coll);
        }
        
        public void add(T fv) {
            elements.add(fv);
        }
        
        private int numSplits() {
            int s = elements.size();
            if (allowDirect && s < itemLimit) {
                s = 0;
            } else if (s > 0) {
                s = 1 + ((s-1) / itemLimit);
            }
            return s;
        }
        
        public void printStatusDecl() {
            str.print(indent + "int ef = 0;\n");
        }
        
        public void printStatusReturn() {
            str.print(indent + "return ef;\n");
        }
        
        public void genFuncs() {
            for (int split = 0; split < numSplits(); split++) {
                str.print("int ");
                str.print(funcName);
                str.print(split);
                str.print("(jmi_t* jmi) {\n");
                printStatusDecl();
                int n = Math.min(itemLimit, elements.size()-split*itemLimit);
                gen(split*itemLimit, n);
                printStatusReturn();
                str.print("}\n\n");
            }
        }
        
        public void genCalls() {
            int n = numSplits();
            if (n == 0) {
                gen(0, elements.size());
            }
            for (int split = 0; split < n; split++) {
                str.print(indent);
                str.print("ef |= ");
                str.print(funcName);
                str.print(split);
                str.print("(jmi);\n");
            }
        }
        
        protected void gen(int start, int n) {
            for (int i = start; i < start + n; i++) {
                genDecl(elements.get(i));
            }
            for (int i = start; i < start + n; i++) {
                genPre(elements.get(i));
            }
            for (int i = start; i < start + n; i++) {
                gen(elements.get(i));
            }
            for (int i = start; i < start + n; i++) {
                genPost(elements.get(i));
            }
        }
        
        public abstract void genDecl(T element);
        public void genPre(T element) {}
        public abstract void gen(T element);
        public void genPost(T element) {}
    }

}

