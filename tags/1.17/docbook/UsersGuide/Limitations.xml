<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Limitations</title>

  <para>This page lists the current limitations of the JModelica.org platform.
  The development of the platform can be followed at the <link
  xlink:href="http://trac.jmodelica.org">Trac</link> site, where future
  releases and associated features are planned. The <link
  xlink:href="http://www.jmodelica.org/binary">JModelica.org platform download
  page</link> has links to compliance reports detailing the current MSL
  compliance.</para>

  <itemizedlist>
    <listitem>
      <para>The Modelica compliance of the front-end is limited; the following
      features are currently not supported:</para>

      <itemizedlist>
        <listitem>
          <para>The support for String variables and parameters is
          limited.</para>
        </listitem>

        <listitem>
          <para>Partial support for external functions; records are not
          supported as arguments or return values.</para>
        </listitem>

        <listitem>
          <para>The following built-in functions are not
          supported:<informaltable border="1" frame="void" width="25%">
              <tr>
                <td><code>terminal()</code></td>
              </tr>
            </informaltable></para>
        </listitem>

        <listitem>
          <para>The following built-in functions are only supported in
          FMUs:<informaltable border="1" frame="void" width="100%">
              <tr>
                <td><code>ceil(x)</code></td>

                <td><code>integer(x)</code></td>

                <td><code>reinit(x, expr)</code></td>
              </tr>

              <tr>
                <td><code>div(x,y)</code></td>

                <td><code>mod(x,y)</code></td>

                <td><code>sample(start,interval)</code></td>
              </tr>

              <tr>
                <td><code>edge(b)</code></td>

                <td><code>pre(y)</code></td>

                <td><code>semiLinear(...)</code></td>
              </tr>

              <tr>
                <td><code>floor(x)</code></td>

                <td><code>rem(x,y)</code></td>

                <td><code>sign(v)</code></td>
              </tr>

              <tr>
                <td><code>initial()</code></td>

                <td><code>delay(...)</code></td>

                <td><code>spatialDistribution(...)</code></td>
              </tr>
            </informaltable></para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>In the Optimica front-end the following constructs are not
      supported:</para>

      <itemizedlist>
        <listitem>
          <para>Annotations for transcription information.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The following limitations apply to FMUs compiled with
      JModelica.org:</para>

      <itemizedlist>
        <listitem>
          <para>Source code FMUs can not be generated, only binary
          FMUs.</para>
        </listitem>

        <listitem>
          <para>Functions for setting and getting string variables do not
          work.</para>
        </listitem>

        <listitem>
          <para>The dependenciesKind attribute in the XML file for FMU 2.0 is
          not generated.</para>
        </listitem>

        <listitem>
          <para>Directional derivatives are known to have limitations in some
          cases.</para>
        </listitem>

        <listitem>
          <para>Asynchronous simulation is not supported.</para>
        </listitem>

        <listitem>
          <para>FMU states (set, get and serialize) are not supported.</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The following limitations apply to optimization using CasADi-based
      collocation with JModelica.org:<itemizedlist>
          <listitem>
            <para>Incomplete support for the <literal>Integer</literal> and
            <literal>Boolean</literal> types: To the extent that they are
            supported, they are treated more or less like reals.</para>
          </listitem>

          <listitem>
            <para>No support for <literal>String</literal> and
            <literal>enumeration</literal> types.</para>
          </listitem>

          <listitem>
            <para>Attributes with any name can be set on any type of
            variable.</para>
          </listitem>

          <listitem>
            <para>The property of whether an optimization problem has free or
            fixed time horizon cannot be changed after compilation.</para>
          </listitem>
        </itemizedlist></para>
    </listitem>

    <listitem>
      <para>The following limitations apply to JMUs compiled with
      JModelica.org (note that JMUs are deprecated in JModelica.org
      1.15):</para>

      <itemizedlist>
        <listitem>
          <para>The ODE interface requires the Modelica model to be written on
          explicit ODE form in order to work.</para>
        </listitem>

        <listitem>
          <para>Second order derivatives (Hessians) are not provided.</para>
        </listitem>

        <listitem>
          <para>The interface for interacting with JMUs does not comply with
          FMI specification.</para>
        </listitem>

        <listitem>
          <para>Discrete variables are not supported in JMUs.</para>
        </listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</chapter>
