<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Float and Double Standard Math Unary Functions</title>
<meta name="description" id="description" content="Float and Double Standard Math Unary Functions"/>
<meta name="keywords" id="keywords" content=" standard math unary abs float and double acos asin atan cos cosh exp log log10 sin sinh sqrt tan tanh "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_std_math_unary_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="luratio.cpp.xml" target="_top">Prev</a>
</td><td><a href="cppad_vector.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>std_math_unary</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>opt_val_hes</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>std_math_unary</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Type</option>
<option>x</option>
<option>y</option>
<option>fun</option>
</select>
</td>
</tr></table><br/>






















<center><b><big><big>Float and Double Standard Math Unary Functions</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>#&#xA0;include&#xA0;&lt;cppad/std_math_unary.hpp&gt;</span></font></code>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Places a copy of the standard math unary functions in the CppAD namespace.
This is included with <code><font color="blue">&lt;cppad/cppad.hpp&gt;</font></code> and can
be included separately.

<br/>
<br/>
<b><big><a name="Type" id="Type">Type</a></big></b>
<br/>
The <i>Type</i> is determined by the argument <i>x</i>
and is either <code><font color="blue">float</font></code> or <code><font color="blue">double</font></code>.

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has the following prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The result <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="fun" id="fun">fun</a></big></b>
<br/>
A definition of <i>fun</i> is included for each of the 
following functions:
<code><font color="blue">abs</font></code>, 
<code><font color="blue">acos</font></code>, 
<code><font color="blue">asin</font></code>, 
<code><font color="blue">atan</font></code>, 
<code><font color="blue">cos</font></code>,  
<code><font color="blue">cosh</font></code>  
<code><font color="blue">exp</font></code>, 
<code><font color="blue">log</font></code>, 
<code><font color="blue">log10</font></code>, 
<code><font color="blue">sin</font></code>, 
<code><font color="blue">sinh</font></code>, 
<code><font color="blue">sqrt</font></code>, 
<code><font color="blue">tan</font></code>, 
<code><font color="blue">tanh</font></code>, 


<hr/>Input File: cppad/std_math_unary.hpp

</body>
</html>
