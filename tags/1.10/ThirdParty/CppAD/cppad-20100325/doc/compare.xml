<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>AD Binary Comparison Operators</title>
<meta name="description" id="description" content="AD Binary Comparison Operators"/>
<meta name="keywords" id="keywords" content=" binary Ad compare operator &lt; &lt;= &gt; &gt;= == != "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_compare_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="boolvalued.xml" target="_top">Prev</a>
</td><td><a href="compare.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>BoolValued</option>
<option>Compare</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>BoolValued-&gt;</option>
<option>Compare</option>
<option>NearEqualExt</option>
<option>BoolFun</option>
<option>ParVar</option>
<option>EqualOpSeq</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>Compare-&gt;</option>
<option>Compare.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Op</option>
<option>x</option>
<option>y</option>
<option>b</option>
<option>Operation Sequence</option>
<option>Assumptions</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>AD Binary Comparison Operators</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>


<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>b</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>Op</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Compares two operands where one of the operands is an
<code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object.
The comparison has the same interpretation as for 
the <i>Base</i> type.


<br/>
<br/>
<b><big><a name="Op" id="Op">Op</a></big></b>
<br/>
The operator <i>Op</i> is one of the following:
<table><tr><td align='left'  valign='top'>

<b>Op</b> <code><span style='white-space: nowrap'>&#xA0;</span></code>  </td><td align='left'  valign='top'>
 <b>Meaning</b>                           </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">&lt;</font></code>   </td><td align='left'  valign='top'>
 is <i>x</i> less than <i>y</i>              </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">&lt;=</font></code>  </td><td align='left'  valign='top'>
 is <i>x</i> less than or equal <i>y</i>     </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">&gt;</font></code>   </td><td align='left'  valign='top'>
 is <i>x</i> greater than <i>y</i>           </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">&gt;=</font></code>  </td><td align='left'  valign='top'>
 is <i>x</i> greater than or equal <i>y</i>  </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">==</font></code>  </td><td align='left'  valign='top'>
 is <i>x</i> equal to <i>y</i>               </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue">!=</font></code>  </td><td align='left'  valign='top'>
 is <i>x</i> not equal to <i>y</i>
</td></tr>
</table>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The operand <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>Type</i> is <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>, <i>Base</i>, or <code><font color="blue">int</font></code>.

<br/>
<br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The operand <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>Type</i> is <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>, <i>Base</i>, or <code><font color="blue">int</font></code>.

<br/>
<br/>
<b><big><a name="b" id="b">b</a></big></b>
<br/>
The result <i>b</i> has type
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;</span></font></code><i><span style='white-space: nowrap'>b</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="Operation Sequence" id="Operation Sequence">Operation Sequence</a></big></b>
<br/>
The result of this operation is a <code><font color="blue">bool</font></code> value
(not an <a href="glossary.xml#AD of Base" target="_top"><span style='white-space: nowrap'>AD&#xA0;of&#xA0;Base</span></a>
 object).
Thus it will not be recorded as part of an
AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>For example, suppose 
<i>x</i> and <i>y</i> are <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> objects,
the tape corresponding to <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> is recording,
<i>b</i> is true,
and the subsequent code is
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;if(&#xA0;</span></font></code><i><span style='white-space: nowrap'>b</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;)<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;cos(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;else&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;sin(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);&#xA0;<br/>
</span></font></code>only the assignment <code><font color="blue"></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;cos(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> is recorded on the tape
(if <i>x</i> is a <a href="glossary.xml#Parameter" target="_top"><span style='white-space: nowrap'>parameter</span></a>
, 
nothing is recorded).
The <a href="comparechange.xml" target="_top"><span style='white-space: nowrap'>CompareChange</span></a>
 function can yield
some information about changes in comparison operation results.
You can use <a href="condexp.xml" target="_top"><span style='white-space: nowrap'>CondExp</span></a>
 to obtain comparison operations
that depends on the 
<a href="glossary.xml#Tape.Independent Variable" target="_top"><span style='white-space: nowrap'>independent&#xA0;variable</span></a>
 
values with out re-taping the AD sequence of operations.

<br/>
<br/>
<b><big><a name="Assumptions" id="Assumptions">Assumptions</a></big></b>
<br/>
If one of the <i>Op</i> operators listed above
is used with an <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object,
it is assumed that the same operator is supported by the base type 
<i>Base</i>.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="compare.cpp.xml" target="_top"><span style='white-space: nowrap'>Compare.cpp</span></a>

contains an example and test of these operations.
It returns true if it succeeds and false otherwise.


<hr/>Input File: cppad/local/compare.hpp

</body>
</html>
