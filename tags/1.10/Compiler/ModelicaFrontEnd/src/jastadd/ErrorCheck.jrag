/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Collection;
import java.util.LinkedList;
import java.util.ArrayList;

import org.jmodelica.util.XMLUtil;

class Problem implements Comparable {
    public int compareTo(Object o) {
        if(o instanceof Problem) {
            Problem other = (Problem)o;
            if(!fileName.equals(other.fileName))
                return fileName.compareTo(other.fileName);
            if(!(beginLine == other.beginLine))
                return beginLine > other.beginLine? 1 : -1;
            if(!(beginColumn == other.beginColumn))
                return beginColumn > other.beginColumn? 1 : -1;
            return message.compareTo(other.message);
        }
        return 0;
    }
    public enum Severity { ERROR, WARNING }
    public enum Kind { OTHER, LEXICAL, SYNTACTIC, SEMANTIC, COMPLIANCE }
    protected int beginLine = 0;
    protected int beginColumn = 0;
    public int beginLine() { return beginLine; }
    public void setBeginLine(int beginLine) { this.beginLine = beginLine; }
    public int beginColumn() { return beginColumn; }
    public void setBeginColumn(int beginColumn) { this.beginColumn = beginColumn; }
  
    protected String fileName;
    public String fileName() { return fileName; }
    public void setFileName(String fileName) { this.fileName = fileName; }
    protected String message;
    public String message() { return message; }
    protected Severity severity = Severity.ERROR;
    public Severity severity() { return severity; }
    protected Kind kind = Kind.OTHER;
    public Kind kind() { return kind; }
    
    public Problem(String fileName, String message) {
        this.fileName = fileName;
        this.message = message;
    }
    public Problem(String fileName, String message, Severity severity) {
        this(fileName, message);
        this.severity = severity;
    }
    public Problem(String fileName, String message, Severity severity, Kind kind) {
        this(fileName, message, severity);
        this.kind = kind;
    }
    
    public Problem(String fileName, String message, Severity severity, Kind kind, int beginLine, int beginColumn) {
        this(fileName, message, severity);
        this.kind = kind;
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
    }
    
    public boolean isTestError(boolean checkAll) {
        return checkAll || (severity == Severity.ERROR && kind != Kind.COMPLIANCE);
    }
    
    public boolean equals(Object o) {
        return (o instanceof Problem) && (compareTo(o) == 0);
    }
    
    private static String name(Object o) {
        String name = o.toString();
        return name.charAt(0) + name.substring(1).toLowerCase();
    }
    
    public String toString() {
        String kindStr = (kind == Kind.OTHER) ? "At " : name(kind) + " error at ";
        return name(severity) + ": in file '" + fileName + "':\n" + 
                kindStr + "line " + beginLine + ", column " + beginColumn + ":\n" + 
                "  " + message;
    }
    
    public String toXML() {
        return String.format(
                "<%s>\n" +
                "    <value name=\"kind\">%s</value>\n" +
                "    <value name=\"file\">%s</value>\n" +
                "    <value name=\"line\">%s</value>\n" +
                "    <value name=\"column\">%s</value>\n" +
                "    <value name=\"message\">%s</value>\n" +
                "</%1$s>",
                XMLUtil.escape(name(severity)),
                XMLUtil.escape(kind.toString().toLowerCase()),
                XMLUtil.escape(fileName),
                beginLine, beginColumn,
                XMLUtil.escape(message)
        );
    }
}


/**
 * \brief Common super class for all JModelica exceptions.
 */
public class ModelicaException extends Exception {
    
    public ModelicaException() {
    }
    
    public ModelicaException(String message) {
        super(message);
    }
    
    public ModelicaException(String message, Throwable cause) {
        super(message, cause);
    }
}
  
  /**
   * Exception containing a list of compiler errors/warnings. 
   */
  public class CompilerException extends ModelicaException {
	  private Collection<Problem> errors;
	  private Collection<Problem> warnings;
	  
	  /**
	   * Default constructor.
	   */
	  public CompilerException() {
		  errors = new ArrayList<Problem>();
		  warnings = new ArrayList<Problem>();
	  }
	  
	  /**
	   * Construct from a list of problems.
	   */
	  public CompilerException(Collection<Problem> problems) {
		  this();
		  for (Problem p : problems) 
			  addProblem(p);
	  }
	  
	  /**
	   * Add a new problem.
	   */
	  public void addProblem(Problem p) {
		  if (p.severity() == Problem.Severity.ERROR) 
			  errors.add(p);
		  else
			  warnings.add(p);
	  }
	  
	  /**
	   * Get the list of problems.
	   */
	  public Collection<Problem> getProblems() {
		  Collection<Problem> problems = new ArrayList<Problem>();
		  problems.addAll(errors);
		  problems.addAll(warnings);
		  return problems;
	  }
	  
	  /**
	   * Get the list of errors.
	   */
	  public Collection<Problem> getErrors() {
		  return errors;
	  }
	  
	  /**
	   * Get the list of warnings.
	   */
	  public Collection<Problem> getWarnings() {
		  return warnings;
	  }
	  
	  /**
	   * Should these problems cause compilation to stop?
	   * 
	   * @param warnings  value to return if there are warnings but not errors 
	   */
	  public boolean shouldStop(boolean warnings) {
		  return !errors.isEmpty() || (warnings && !this.warnings.isEmpty());
	  }

	  /**
	   * Convert to error message.
	   */
	  public String getMessage() {
		  StringBuilder str = new StringBuilder();
		  if (!errors.isEmpty()) {
			  str.append(errors.size());
			  str.append(" errors ");
			  str.append(warnings.isEmpty() ? "found:\n\n" : "and ");
		  }
		  if (!warnings.isEmpty()) {
			  str.append(warnings.size());
			  str.append(" warnings found:\n\n");
		  }
		  for (Problem p : errors) {
			  str.append(p);
			  str.append("\n\n");
		  }
		  for (Problem p : warnings) {
			  str.append(p);
			  str.append("\n\n");
		  }
		  str.deleteCharAt(str.length() - 1);
		  return str.toString();
	  }
	  
	  /**
	   * Convert to xml error message.
	   */
	  public String getXMLMessage() {
		  StringBuilder str = new StringBuilder();
		  for (Problem p : errors) {
			  str.append(p.toXML());
			  str.append("\n");
		  }
		  for (Problem p : warnings) {
			  str.append(p.toXML());
			  str.append("\n");
		  }
		  str.deleteCharAt(str.length() - 1);
		  return str.toString();
	  }
  }
  
  /**
   * Exception to be thrown when the Modelica class to instantiate is not
   * found.
   */
  public class ModelicaClassNotFoundException extends ModelicaException {
	  private String className;
	  
	  public ModelicaClassNotFoundException(String className) {
		  super("Class "+ className + " not found");
		  this.className = className;
	  }
	  
	  public String getClassName() {
		  return className;
	  }
	  
  }
  
  /**
   * Interface for handling semantic errors.
   * $see Root#setErrorHandler(IErrorHandler)
   */
  public interface IErrorHandler {
	  /**
	   * \brief Called when a semantic error is found. 
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   * @see ASTNode#error(String)
	   */
	  public void error(String s, ASTNode n);
	  
	  /**
	   * \brief Called when a compiler compliance error is found.
	   * 
	   * These errors are generated when compiling code that is legal Modelica, 
	   * but uses features that aren't implemented. Compliance errors are ignored 
	   * by test cases (except ComplianceErrorTestCase).
	   *  
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   * @see ASTNode#compliance(String)
	   */
	  public void compliance(String s, ASTNode n);
	  
	  /**
	   * \brief Called when a warning is issued during semantic error checking.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   * @see ASTNode#warning(String)
	   */
	  public void warning(String s, ASTNode n);
	  
	  /**
	   * Connect error handler to a Root.
	   * 
	   * Might return another equivalent error handler that is connected to the root instead.
	   * 
	   * @param root  the Root to connect to
	   * @return  an error handler connected to <code>root</code>
	   */
	  public IErrorHandler connectTo(Root root);
  }
  
  /**
   * \brief Default implementation of {@link IErrorHandler}.
   *  
   * Collects a list of {@link Problem} for all found errors.
   */
  public class DefaultErrorHandler implements IErrorHandler {
	  protected Root root;
	  
	  public DefaultErrorHandler(Root root) {
		  this.root = root;
	  }

	  /**
	   * \brief Creates a new {@link Problem} and adds it to root.errors, ignoring duplicates.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void error(String s, ASTNode n) {
		  problem(s, n, root.errors, Problem.Severity.ERROR, Problem.Kind.SEMANTIC);
	  }

	  /**
	   * \brief Creates a new {@link Problem} with kind COMPLIANCE 
	   *        and adds it to root.errors, ignoring duplicates.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void compliance(String s, ASTNode n) {
		  problem(s, n, root.errors, Problem.Severity.ERROR, Problem.Kind.COMPLIANCE);
	  }

	  /**
	   * \brief Creates a new {@link Problem} and adds it to root.warnings, ignoring duplicates.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   */
	  public void warning(String s, ASTNode n) {
		  problem(s, n, root.warnings, Problem.Severity.WARNING, Problem.Kind.OTHER);
	  }
	  
	  protected void problem(String s, ASTNode n, ArrayList<Problem> list, Problem.Severity sev, Problem.Kind kind) {
		  Problem p = new Problem(n.fileName(), s, sev, kind, n.lineNumber(), n.columnNumber());
		  if (!list.contains(p))
			  list.add(p);
	  }
	  
	  public IErrorHandler connectTo(Root root) {
		  IErrorHandler eh = new DefaultErrorHandler(root);
		  root.setErrorHandler(eh);
		  return eh;
	  }
  }
  
  /**
   * \brief Error handler that generates warnings for compliance errors, 
   *        delegating to another error handler.
   */
  public class ComplianceWarnErrorHandler implements IErrorHandler {
  	
  	  private IErrorHandler delegate;

	  public ComplianceWarnErrorHandler(IErrorHandler delegate) {
	  	  if (delegate instanceof ComplianceWarnErrorHandler)
	  		  delegate = ((ComplianceWarnErrorHandler) delegate).delegate;
		  this.delegate = delegate;
	  }

	  /**
	   * \brief Delegates to wrapped error handler.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void error(String s, ASTNode n) {
		  delegate.error(s, n);
	  }

	  /**
	   * \brief Delegates to warning() in wrapped error handler.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void compliance(String s, ASTNode n) {
		  delegate.warning(s, n);
	  }

	  /**
	   * \brief Delegates to wrapped error handler.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   */
	  public void warning(String s, ASTNode n) {
		  delegate.warning(s, n);
	  }
	  
	  public IErrorHandler connectTo(Root root) {
		  delegate = delegate.connectTo(root);
		  return this;
	  }
	  
  }
  

aspect ErrorCheck {

  public ArrayList<Problem> Root.errors = new ArrayList<Problem>();
  public ArrayList<Problem> Root.warnings = new ArrayList<Problem>();

  private IErrorHandler Root.errorHandler = new DefaultErrorHandler(this);
  
  /**
   * Set the handler for semantic errors.
   * @see IErrorHandler 
   */
  public void Root.setErrorHandler(IErrorHandler handler) {
	  errorHandler = handler;
  }
  
  /**
   * Get the handler for semantic errors.
   * @see IErrorHandler 
   */
  public IErrorHandler Root.getErrorHandler() {
	  return errorHandler;
  }

  syn int ASTNode.lineNumber() = (start != 0 || getParent()==null)? beginLine() : getParent().lineNumber();
  syn int ASTNode.columnNumber() = (start != 0 || getParent()==null)? beginColumn() : getParent().columnNumber();
  
  /**
   * Register an error. Delegates to an {@link IErrorHandler}.
   * @param s	the error message.
   */
  void ASTNode.error(String s) {
	  root().getErrorHandler().error(s, this);
  }
  
  /**
   * Register an error. Delegates to an {@link IErrorHandler}.
   * 
   * Builds error message using <code>format</code> as format string.
   */
  void ASTNode.error(String format, Object... args) {
	  error(String.format(format, args));
  }

  /**
   * Register a compliance error. Delegates to an {@link IErrorHandler}.
   * @param s	the error message.
   */
  void ASTNode.compliance(String s) {
	  root().getErrorHandler().compliance(s, this);
  }
  
  /**
   * Register a compliance error. Delegates to an {@link IErrorHandler}.
   * 
   * Builds error message using <code>format</code> as format string.
   */
  void ASTNode.compliance(String format, Object... args) {
	  compliance(String.format(format, args));
  }

  /**
   * Register a warning. Delegates to an {@link IErrorHandler}.
   * @param s	the warning message.
   */
  void ASTNode.warning(String s) {
	  root().getErrorHandler().warning(s, this);
  }
  
  /**
   * Register a warning. Delegates to an {@link IErrorHandler}.
   * 
   * Builds warning message using <code>format</code> as format string.
   */
  void ASTNode.warning(String format, Object... args) {
	  warning(String.format(format, args));
  }
  
  /**
   * Lock the closest surrounding if-equation or if-expression that have only 
   * parameter-expression tests and evaluates to another branch. Returns true 
   * if any such if exists. 
   * 
   * Any parameters used in the test will be marked as structural.
   */
  inh boolean FExp.lockBranch();
  inh boolean FExpSubscript.lockBranch();
  inh boolean FIfEquation.lockBranch();
  eq InstNode.getChild().lockBranch()                = false;
  eq FClass.getChild().lockBranch()                  = false;
  eq FIfExp.getThenExp().lockBranch()                = lockMyBranch(true);
  eq FIfExp.getElseExp().lockBranch()                = lockMyBranch(false);
  eq FIfEquation.getFAbstractEquation().lockBranch() = lockMyBranch(true);
  eq FIfEquation.getElse().lockBranch()              = lockMyBranch(false);
  
  /**
   * Lock this if-expression and return true if it only has only parameter-expression 
   * tests and evaluates to the branch not indicated by <code>then</code>. Otherwise 
   * delegates to {@link #lockBranch()}.
   * 
   * Any parameters used in the test will be marked as structural.
   * 
   * @param this  if true, we are trying to remove the then-branch, otherwise the else-branch
   */
  public boolean FIfExp.lockMyBranch(boolean then) {
	  if (getIfExp().variability().parameterOrLess() && getIfExp().ceval().booleanValue() != then) {
		  getIfExp().markAsStructuralParameter();
		  return true;
	  } else {
		  return lockBranch();
	  }
  }
  
  /**
   * Lock this if-expression and return true if it only has only parameter-expression 
   * tests and evaluates to the branch not indicated by <code>then</code>. Otherwise 
   * delegates to {@link #lockBranch()}.
   * 
   * Any parameters used in the test will be marked as structural.
   * 
   * @param this  if true, we are trying to remove the then-branch, otherwise the else-branch
   */
  public boolean FIfEquation.lockMyBranch(boolean then) {
	  if (getTest().variability().parameterOrLess() && getTest().ceval().booleanValue() != then) {
		  getTest().markAsStructuralParameter();
		  return true;
	  } else {
		  return lockBranch();
	  }
  }

  /**
   * \brief Call all *Check() methods for this node.
   * 
   * Helper method to make it easier to add new check methods 
   * for all nodes.
   */
  protected void ASTNode.allChecks() {
	  nameCheck();
	  typeCheck();
	  contentCheck();	  
	  complianceCheck();	  
  }
  
  public void ASTNode.collectErrors() {
	  allChecks();
	  for(int i = 0; i < getNumChild(); i++) {
		  getChild(i).collectErrors();
	  }
  }


  public Collection<Problem> ASTNode.errorCheck(){
    ArrayList allErrors = root().errors;
    ArrayList allWarnings = root().warnings;
    collectErrors();
    java.util.Collections.sort(allErrors);
    java.util.Collections.sort(allWarnings);
    
    ArrayList<Problem> problems = new ArrayList<Problem>();
    problems.addAll(allWarnings);
    problems.addAll(allErrors);    
    
    /*
    if (!allWarnings.isEmpty()) {
    	str.append("\n");        	
    	str.append("Warnings:\n");    
    	for(Iterator iter = allWarnings.iterator(); iter.hasNext(); ) {
    		Problem problem = (Problem) iter.next();
    		str.append(problem+"\n");
    	}
    }
   if(allErrors.isEmpty())
      return false;
    str.append("\n");        	
    str.append(allErrors.size() + " error(s) found...\n");
    for(Iterator iter = allErrors.iterator(); iter.hasNext(); ) {
      Problem problem = (Problem)iter.next();
      str.append(problem+"\n");
    }
    */
    
    return problems;
  }

/*
    syn lazy FullClassDecl ASTNode.retrieveFullClassDecl(String className) {
      for(int i = 0; i < getNumChild(); i++) {
		  FullClassDecl fcd = getChild(i).retrieveFullClassDecl(className);
	  	  if (fcd != null)
	  	  	return fcd;
	  }
	  return null;
    }   
  	
  	eq FullClassDecl.retrieveFullClassDecl(String className) {
	   	if (className.equals(qualifiedName())) {
	   		return this;
		} else
			return getClassDeclList().retrieveFullClassDecl(className);
	}
*/
   
}

aspect InstanceErrorCheck {

 // Error checking in instance tree

	// We don't want to error check an entire model, just the classes
	//   that are used. 
	public Collection<Problem> InstProgramRoot.checkErrorsInInstClass(String className) throws ModelicaClassNotFoundException{
		InstClassDecl icd = lookupInstClassQualified(className);
		if (icd.isUnknown()) 
			throw new ModelicaClassNotFoundException(className);
		else
			return icd.errorCheck();
	}

	protected boolean BaseNode.errorChecked = false;

	public void ASTNode.resetCollectErrors() {
		for (ASTNode n : noTransform())
			n.resetCollectErrors();
	}
	
	public static void ASTNode.resetCollectErrorsOn(ASTNode n) {
		if (n != null)
			n.resetCollectErrors();
	}
	
	public void BaseNode.resetCollectErrors() {
		errorChecked = false;
		super.resetCollectErrors();
	}
	
	public void InstNode.resetCollectErrors() {
		super.resetCollectErrors();
		resetCollectErrorsOn(getInstComponentDeclListNoTransform());
		resetCollectErrorsOn(getInstClassDeclListNoTransform());
		resetCollectErrorsOn(getInstExtendsListNoTransform());
		resetCollectErrorsOn(getInstImportListNoTransform());
		resetCollectErrorsOn(getRedeclaredInstClassDeclListNoTransform());
		resetCollectErrorsOn(getFAbstractEquationListNoTransform());
	}

  public void InstNode.collectErrors() {
    if (!errorChecked) {
      errorChecked = true;
	  allChecks();
	  for (InstNode n : getInstComponentDecls()) 
		  n.collectErrors();
	  for (InstNode n : getInstExtendss()) 
		  n.collectErrors();
	  for (FAbstractEquation e : getFAbstractEquations()) 
		  e.collectErrors();
	}
  }
    
    /**
     * \brief Check if this node is in an InstComponentDecl.
     */
    inh boolean InstExtends.inInstComponent();
    inh boolean InstClassRedeclare.inInstComponent();
    eq InstComponentDecl.getChild().inInstComponent() = true;
    eq InstClassDecl.getChild().inInstComponent()     = false;
    eq InstRoot.getChild().inInstComponent()          = false;
    eq FlatRoot.getChild().inInstComponent()          = false;

  	public void InstBaseClassDecl.collectErrors() {
    	if (!errorChecked) {
	        super.collectErrors();
	    	errorChecked = true;
 //       for (InstClassDecl icd : instClassDecls())
 //       	icd.collectErrors();
 /*
        for (InstComponentDecl icd : instComponentDecls())
        	icd.collectErrors();
        for (InstExtends ie : instExtends())
        	ie.collectErrors();	
*/        	
	        for (InstImport ii : getInstImports())
	        	ii.collectErrors();		
			if (hasInstConstraining())
				getInstConstraining().collectErrors();
//        for (FAbstractEquation e : getFAbstractEquations())
//        	e.collectErrors();
//    	getEquationList().collectErrors();
//    	getAlgorithmList().collectErrors();
//    	getSuperList().collectErrors();
//    	getImportList().collectErrors();
//    	getComponentDeclList().collectErrors();
			if (getBaseClassDecl() instanceof FullClassDecl) { 	
				FullClassDecl fcd = (FullClassDecl)getBaseClassDecl();
				//log.debug(fcd.getName().getID() +  " " + fcd.getEndName());
				if (!(fcd.getName().getID().equals(fcd.getEndDecl().getEndID()))) {
					error("The declaration and end names of a class should be the same");
				}
			}
  		}
  	}
  	
  	public void InstFullClassDecl.collectErrors() {
    	if (!errorChecked) {
            super.collectErrors();
    		errorChecked = true;
            getInstExternalOpt().collectErrors();
    	}
  	}
    
    public void InstSimpleShortClassDecl.collectErrors() {
        if (!errorChecked) {
        	errorChecked = true;
        	getTarget().collectErrors();
        	actualInstClass().collectErrors();
        }    	
    }

//  	public void InstEnumClassDecl.collectErrors() {  		
//  		// TODO: Error checking of enumeration declarations
//  	}
  	
    public void InstImport.collectErrors() {
    	if (!errorChecked) {
    		errorChecked = true;
			getPackageName().collectErrors();
		}
	}
	
	public void InstComponentDecl.collectErrors() {
	    if (!errorChecked) {
	    	errorChecked = true;
	    	if (isRecursed()) {
	    		error("Recursive class structure");
	    	} else if (isOuter()) {
	    		if (myInnerInstComponentDecl().isUnknown())
	    			error("Cannot find inner declaration for outer " + name());
	    		else
	    			myInnerInstComponentDecl().collectErrors();
	    	} else if (inOuter()) {
	    		surroundingOuterComponentDecl().collectErrors();
	    	} else {
  		  		errorChecked = false;
	    		super.collectErrors();
  		  		collectErrorsInClassName();
	    		if (hasConditionalAttribute()) 
	    			getConditionalAttribute().collectErrors();
	    		if (isActive() && hasInstModification())
	    			getInstModification().collectErrors();
	    		if (hasInstConstraining())
	    			getInstConstraining().collectErrors();
	    	}
		}
	}
	
	// TODO: move to better place (InstanceTree.jrag?)
	syn boolean InstComponentDecl.isActive() {
		try {
			return !hasConditionalAttribute() || getConditionalAttribute().ceval().booleanValue();
		} catch (ConstantEvaluationException e) {
			return false;
		}
	}
	
	public void InstComponentDecl.collectErrorsInClassName() {
		getClassName().collectErrors();
	}	
	
	public void InstArrayComponentDecl.collectErrorsInClassName() {
		// TODO: use correct class name instead of "ArrayDecl" so that name lookup suceeds instead?
		//       need that for other things as well, but is there problems with it?
	}	
	
	public void FExp.checkConstantExpression(String varKind, String varName) {
		String exp = "'" + prettyPrint("") + "'";
		boolean failed = false;
		try {
			if (isCircular()) {
				error("Could not evaluate binding expression for %s '%s' due to circularity: %s", 
						varKind, varName, exp);
			} else {
				CValue val = ceval();
				if (val.isUnknown()) {
					if (val.isUnsupported()) {
						compliance("Constant evaluation not supported for expression(s) directly or indirectly " + 
								"used by the binding expression for %s '%s': %s", varKind, varName, exp);
					} else {
						failed = true;
					}
				}
			}
		} catch (ConstantEvaluationNotReadyException e) {
			// Will be evaluatable later, ignore for now
		} catch (ConstantEvaluationException e) {
			failed = true;
		}
		if (failed)
			error("Could not evaluate binding expression for %s '%s': %s", varKind, varName, exp);
	}
	
	public void InstAssignable.collectErrors() {
		//log.debug(toString());
        if (!errorChecked) {
        	super.collectErrors();
    		errorChecked = true;
	
			// Check binding expression
    		FExp bexp = myBindingInstExp();
    		if (bexp != null) {
    			String type = isParameter() ? "parameter" : "constant";
	    		// TODO: Check structural parameters as well
				// Check if the binding expression of constants can be evaluated
				if (isConstant()) 
					bexp.checkConstantExpression("constant", qualifiedName());
				else if (isParameter() && bexp.isCircular())
					bexp.error("Circularity in binding expression of parameter: %s = %s", 
							qualifiedName(), bexp);
				bexp.collectErrors();
    		} else {
				// Warn if constant or parameter does not have a binding expression (start is used)
				if ((isConstant() || isParameter()) && !isForIndex()) {
					String type = isParameter() ? "parameter" : "constant";
					warning("The %s %s does not have a binding expression", type, qualifiedName());
				}
    		}
    		
    		// Mark parameters with Evaluate=true as structural
    		if (isParameter() && annotation().forPath("Evaluate").bool()) 
    			markAsStructuralParameter();
			
			// Check array indices
			getClassName().collectErrors();
			getLocalFArraySubscriptsOpt().collectErrors();
			
			// Check attributes for primitive variables
			checkAttributes();
		}
	}
	
	public void InstAssignable.checkAttributes() {}
	
	public void InstPrimitive.checkAttributes() {
		// Check if the expressions of the attributes can be evaluated
		// Note that this check has to be done locally in the
		// context of an InstAssignable node in order to avoid
		// evaluation of all value modifications also for non
		// parameters.
		for (InstModification im : totalMergedEnvironment()) {
			// Only check attributes, value modifications are checked above
			if (im instanceof InstComponentModification) {
				InstComponentModification icm = (InstComponentModification)im;
				if (icm.hasInstModification() && icm.getInstModification().hasInstValueMod()) {
					FExp val_mod = icm.getInstModification().instValueMod();
					if (val_mod.variability().lessOrEqual(fConstant())) 
						val_mod.checkConstantExpression("attribute", icm.getName().name());
					else if (!val_mod.variability().lessOrEqual(fParameter())) 
						val_mod.error("Variability of binding expression for attribute '%s' is not less than or equal to parameter variability: %s", 
								icm.getName().name(), val_mod);
					if (val_mod.isCircular())
						error("Could not evaluate binding expression for attribute '%s' due to circularity: %s", 
								icm.getName().name(), val_mod);
				}
			}
		}
	}
	
	public void InstExtends.collectErrors() {
	    if (!errorChecked) {
	    	if (isRecursed()) {
	    		error("Recursive class structure");
	    		errorChecked = true;
	    	} else {
				super.collectErrors();
    			errorChecked = true;
				getClassName().collectErrors();
				if (hasInstClassModification() && shouldCheckModification())
					getInstClassModification().collectErrors();
			}
	    }
	}
	
	// Normally the class modifications in an InstExtendsShortClass
	// does not need to be checked, since they are checked in InstShortClassDecl.
	// This is not the case if the short class decl references
	// an primitive variable, however, and in this case the
	// class modification needs to be checked for errors.
	syn boolean InstExtends.shouldCheckModification()           = true;
	eq InstExtendsShortClass.shouldCheckModification()          = extendsPrimitive();
	eq InstReplacingExtendsShortClass.shouldCheckModification() = extendsPrimitive();
	
	public void InstShortClassDecl.collectErrors() {
	    if (!errorChecked) {
		  super.collectErrors();
    	  errorChecked = true;
		  // The localInstModifications should only be checked if
		  // the node is not a InstReplacingShortClassDecl. This
		  // is accomplished by the method collectInstModificationErrors.
		  collectInstModificationErrors();
		  if (hasInstConstraining())
				getInstConstraining().collectErrors();		  
		}
	}

	public void InstShortClassDecl.collectInstModificationErrors() {
		for (InstModification mod : localInstModifications())
			mod.collectErrors();
    }
    public void InstReplacingShortClassDecl.collectInstModificationErrors() { }

	public void InstReplacingShortClassDecl.collectErrors() {
	    if (!errorChecked) {
		  super.collectErrors();
    	  errorChecked = true;
		  getOriginalInstClass().collectErrors();
		}
		
	}

	public void InstReplacingFullClassDecl.collectErrors() {
	    if (!errorChecked) {
		  super.collectErrors();
    	  errorChecked = true;
		  getOriginalInstClass().collectErrors();
		}
		
	}

	public void InstBuiltIn.collectErrors() {}

	public void InstComponentRedeclare.collectErrors() {
		super.collectErrors();
		getInstComponentDecl().collectErrors();
	}

	public void InstClassRedeclare.collectErrors() {
		super.collectErrors();
		if (!inInstComponent())
			getInstClassDecl().collectErrors();	
	}

	public void InstValueModification.collectErrors() {
		getFExp().collectErrors();
	}

	public void InstDot.collectErrors() {
	    if (!errorChecked) {	
			errorChecked = true;
			for (InstAccess ia : getInstAccesss()) {
				ia.collectErrors();
				if (ia.isUnknown())
					break;
			}
			allChecks();
	    }
	}

	public void InstClassAccess.collectErrors() {
	    if (!errorChecked) {
		    //super.collectErrors();
		    errorChecked = true;
		    nameCheck();
		}
	}

	public void InstComponentAccess.collectErrors() {
	    if (!errorChecked) {
		    errorChecked = true;
		    super.collectErrors();
		    if (!myInstComponentDecl().isUnknown() && !isModificationName())
		    	myInstComponentDecl().collectErrors();
		}
	}
	
	public void InstComponentArrayAccess.collectErrors() {
	    if (!errorChecked) {
		    errorChecked = true;
		    super.collectErrors();
		    if (!myInstComponentDecl().isUnknown() && !isModificationName())
		    	myInstComponentDecl().collectErrors();
		}
	}
	
	inh boolean InstAccess.isModificationName();
	eq InstNamedModification.getName().isModificationName() = true;
	eq BaseNode.getChild().isModificationName()             = false;

	inh boolean FArraySubscripts.myAccessExists();
	eq Root.getChild().myAccessExists()       = false;
	eq InstAccess.getChild().myAccessExists() = !myInstComponentDecl().isUnknown();
	
	public void FArraySubscripts.collectErrors() {
		// Should this check be in the access instead?
		int ndims = mySize().ndims();
		if (getNumFSubscript() > ndims && !isInstComponentSize() && myAccessExists()) {
			// TODO: shouldn't this check for to few as well? (no [] or all dimensions given)
			error("Too many array subscripts for access: " + getNumFSubscript() + 
					" subscripts given, component has " + mySize().ndims() + " dimensions");
			allChecks();
			for (int i = 0; i < ndims; i++)
				getFSubscript(i).collectErrors();
		} else {
    		super.collectErrors();
		}
	}
	
	/**
	 * \brief Check if class has exactly one algorithm section or external function declaration.
	 */
	public boolean InstClassDecl.isCompleteFunction() {
		return (numFAlgorithm() == 1) != (numInstExternal() == 1);
	}
	
	syn boolean InstClassDecl.hasInstExternal() = false;
	
	syn int InstClassDecl.numInstExternal() {
		int n = hasInstExternal() ? 1 : 0;
		for (InstExtends ie : getInstExtendss())
			n += ie.myInstClass().numInstExternal();
		return n;
	}
	eq InstSimpleShortClassDecl.numInstExternal() = actualInstClass().numInstExternal();
	
	syn int InstClassDecl.numFAlgorithm() {
		int n = 0;
		for (FAbstractEquation e : getFAbstractEquations())
			if (e instanceof FAlgorithm)
				n++;
		for (InstExtends ie : getInstExtendss())
			n += ie.myInstClass().numFAlgorithm();
		return n;
	}
	eq InstSimpleShortClassDecl.numFAlgorithm() = actualInstClass().numFAlgorithm();
	
	public void InstExternalObject.collectErrors() {
	    if (!errorChecked) {
			super.collectErrors();
	    	errorChecked = true;
	    	if (!inFunction()) {
	    		getDestructorCall().collectErrors();
	    		myInstClass().collectErrors();
	    	}
	    }
	}
	
	public void InstFunctionCall.collectErrors() {
	    if (!errorChecked) {
	    	errorChecked = true;
	    	// Check that the function exists
	    	InstClassDecl func = getName().myInstClassDecl();
	    	if (!func.isCallable()) {
	    		// Report that function does not exist
		    	String name = getName().name();
		    	if (func.isExternalObject()) {
		    		name += ".constructor";
		    		func = func.myConstructor();
		    	}
		    	if (func.isUnknown()) 
	    			error("Cannot find function declaration for " + name + "()");
	    		else
	    			error("The class " + name + " is not a function");
	    	} else if (!func.isRecord() && !func.isCompleteFunction()) {
	    		// TODO: add check if function is partial?
	    		error("Calling function " + getName().name() + 
	    				"(): can only call functions that have one algorithm section or external function specification");
	    	} else {
	    		// Function exists, check everything
	    		super.collectErrors();
	    		
	    		// We need to check the function definition as well.
	    	    func.collectErrors();
	    	    
	    	    // Check if there were any unbindable args
			    boolean pos = true;
			    String desc = functionCallDecription();
			    for (InstFunctionArgument arg : unbindableArgs) 
			    	pos = arg.generateUnbindableError(desc, pos);
	    	}
	    }
	}
	
	public void FInfArgsFunctionCall.collectErrors() {
		super.collectErrors();
		if (unbindableArgs != null) {
		    boolean pos = true;
		    String desc = functionCallDecription();
		    for (InstFunctionArgument arg : unbindableArgs) 
		    	pos = arg.generateUnbindableError(desc, pos);
		}
	}
	
	syn String FAbstractFunctionCall.functionCallDecription() = "Calling function " + name() + "()";
	eq FRecordConstructor.functionCallDecription() = "Record constructor for " + name();
	eq InstFunctionCall.functionCallDecription()   = getName().myInstClassDecl().isRecord() ? 
			"Record constructor for " + name() : super.functionCallDecription();
	  
	public boolean InstFunctionArgument.generateUnbindableError(String desc, boolean genForPos) {
		return genForPos;
	}
	  
	public boolean InstPositionalArgument.generateUnbindableError(String desc, boolean genForPos) {
		if (genForPos)
			error(desc + ": too many positional arguments");
		return false;
	}
	  
	public boolean InstNamedArgument.generateUnbindableError(String desc, boolean genForPos) {
		error(desc + ": no input matching named argument " + getName().name() + " found");
		return genForPos;
	}
	
	public void FBuiltInFunctionCall.collectErrors() {
	    if (!errorChecked) {
	    	super.collectErrors();
	    	errorChecked = true;
	    	getOriginalArgs().collectErrors();
	    }
	}
	
	public void FUnsupportedBuiltIn.collectErrors() {
		// Don't check arguments
		allChecks();
	}
 
	public void InstNamedArgument.collectErrors() {
		// TODO: This way, the FExp for each argument to a built-in function is checked twice - fix that
	    if (!errorChecked) {
			allChecks();
			getFExp().collectErrors();
	    }
	}
	
	/**
	 * \brief Check if this node is in a recursive structure.
	 */
	syn boolean InstNode.isRecursed()      = false;
	eq InstComponentDecl.isRecursed()      = isWithin(myInstClass());
	eq InstExtends.isRecursed()            = isWithin(myInstClass());
	eq InstArrayComponentDecl.isRecursed() = instComponentDecl().isRecursed();
	
	// TODO: check if we realy need this in addition to isRecursed()
	/**
	 * \brief Check if extends tree is recursive.
	 */
	public boolean InstExtends.isRecursive() {
		if (recursiveCache == RECURSIVE_UNKNOWN)
			calcIsRecursive(new HashSet<InstNode>());
		return recursiveCache == RECURSIVE_YES;
	}
	
	/**
	 * \brief Check if extends tree is recursive.
	 */
	public boolean InstSimpleShortClassDecl.isRecursive() {
		if (recursiveCache == RECURSIVE_UNKNOWN)
			calcIsRecursive(new HashSet<InstNode>());
		return recursiveCache == RECURSIVE_YES;
	}
	
	/**
	 * \brief Examine extends tree to find recursive extends nodes.
	 */
	public void InstExtends.calcIsRecursive(HashSet<InstNode> visited) {
		recursiveCache = visited.contains(this) ? RECURSIVE_YES : RECURSIVE_NO;
		visited.add(this);
		if (recursiveCache == RECURSIVE_NO) 
			myInstClass().calcIsRecursive(visited);
	}
	
	/**
	 * \brief Examine extends tree to find recursive extends nodes.
	 */
	public void InstClassDecl.calcIsRecursive(HashSet<InstNode> visited) {
		for (InstExtends ie : getInstExtendss())
			ie.calcIsRecursive(visited);
	}
	
	public void InstSimpleShortClassDecl.calcIsRecursive(HashSet<InstNode> visited) {
		recursiveCache = visited.contains(this) ? RECURSIVE_YES : RECURSIVE_NO;
		visited.add(this);
		if (recursiveCache == RECURSIVE_NO) 
			getTarget().myInstClassDecl().calcIsRecursive(visited);
	}
	
	private byte InstExtends.recursiveCache              = RECURSIVE_UNKNOWN;
	private byte InstSimpleShortClassDecl.recursiveCache = RECURSIVE_UNKNOWN;
	protected static final byte InstNode.RECURSIVE_UNKNOWN = 0;
	protected static final byte InstNode.RECURSIVE_YES     = 1;
	protected static final byte InstNode.RECURSIVE_NO      = 2;
	
	/**
	 * \brief Check if <code>icd</code> is an ancestor of this node or any ancestor is an 
	 *        instance of <code>icd</code>.
	 */
	inh boolean InstComponentDecl.isWithin(InstClassDecl icd);
	inh boolean InstExtends.isWithin(InstClassDecl icd);
	eq InstNode.getChild().isWithin(InstClassDecl icd)          = isOfInstClassDecl(icd);
	eq InstComponentDecl.getChild().isWithin(InstClassDecl icd) = isOfInstClassDecl(icd) || isWithin(icd);
	eq InstExtends.getChild().isWithin(InstClassDecl icd)       = isOfInstClassDecl(icd) || isWithin(icd);
	
	/**
	 * \brief Check if this node is equal to or an instance of <code>icd</code>.
	 */
	syn boolean InstNode.isOfInstClassDecl(InstClassDecl icd) = false;
	eq InstClassDecl.isOfInstClassDecl(InstClassDecl icd)     = icd == this;
	eq InstComponentDecl.isOfInstClassDecl(InstClassDecl icd) = icd == myInstClass() && !icd.isUnknown();
	eq InstExtends.isOfInstClassDecl(InstClassDecl icd)       = icd == myInstClass() && !icd.isUnknown();
	
}

