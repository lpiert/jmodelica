<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>LuFactor: Example and Test</title>
<meta name="description" id="description" content="LuFactor: Example and Test"/>
<meta name="keywords" id="keywords" content=" example Lufactor test "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_lufactor.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="lufactor.xml" target="_top">Prev</a>
</td><td><a href="lu_factor.hpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>LuDetAndSolve</option>
<option>LuFactor</option>
<option>LuFactor.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>opt_val_hes</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>LuDetAndSolve-&gt;</option>
<option>LuSolve</option>
<option>LuFactor</option>
<option>LuInvert</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>LuFactor-&gt;</option>
<option>LuFactor.cpp</option>
<option>lu_factor.hpp</option>
</select>
</td>
<td>LuFactor.cpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>LuFactor: Example and Test</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 
# include &lt;cstdlib&gt;               // for rand function
# include &lt;cppad/lu_factor.hpp&gt;      // for CppAD::LuFactor
# include &lt;cppad/near_equal.hpp&gt;     // for CppAD::NearEqual
# include &lt;cppad/vector.hpp&gt;  // for CppAD::vector

bool LuFactor(void)
{	bool  ok = true;

# ifndef _MSC_VER
	using std::rand;
	using std::srand;
# endif

	size_t  n = 5;                        // number rows in A 
	double  rand_max = double(RAND_MAX);  // maximum rand value
	double  sum;                          // element of L * U
	double  pij;                          // element of permuted A
	size_t  i, j, k;                      // temporary indices

	// A is an n by n matrix
	CppAD::vector&lt;double&gt; A(n*n), LU(n*n), L(n*n), U(n*n);

	// set A equal to an n by n random matrix
	for(i = 0; i &lt; n; i++)
		for(j = 0; j &lt; n; j++)
			A[i * n + j] = rand() / rand_max;

	// pivot vectors
	CppAD::vector&lt;size_t&gt; ip(n);
	CppAD::vector&lt;size_t&gt; jp(n);

	// factor the matrix A
	LU       = A;
	CppAD::LuFactor(ip, jp, LU);

	// check that ip and jp are permutations of the indices 0, ... , n-1
	for(i = 0; i &lt; n; i++)
	{	ok &amp;= (ip[i] &lt; n);
		ok &amp;= (jp[i] &lt; n);
		for(j = 0; j &lt; n; j++)
		{	if( i != j )
			{	ok &amp;= (ip[i] != ip[j]);
				ok &amp;= (jp[i] != jp[j]);
			}
		}
	}
	
	// Extract L from LU
	for(i = 0; i &lt; n; i++)
	{	// elements along and below the diagonal
		for(j = 0; j &lt;= i; j++)
			L[i * n + j] = LU[ ip[i] * n + jp[j] ];
		// elements above the diagonal
		for(j = i+1; j &lt; n; j++)
			L[i * n + j] = 0.;
	}
	
	// Extract U from LU
	for(i = 0; i &lt; n; i++)
	{	// elements below the diagonal
		for(j = 0; j &lt; i; j++)
			U[i * n + j] = 0.;
		// elements along the diagonal
		U[i * n + i] = 1.;
		// elements above the diagonal
		for(j = i+1; j &lt; n; j++)
			U[i * n + j] = LU[ ip[i] * n + jp[j] ];
	}

	// Compute L * U 
	for(i = 0; i &lt; n; i++)
	{	for(j = 0; j &lt; n; j++)
		{	// compute element (i,j) entry in L * U
			sum = 0.;
			for(k = 0; k &lt; n; k++)
				sum += L[i * n + k] * U[k * n + j];
			// element (i,j) in permuted version of A
			pij  = A[ ip[i] * n + jp[j] ];
			// compare
			ok  &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>(pij, sum, 1e-10, 1e-10);
		}
	}

	return ok;
}
</pre>
</font></code>


<hr/>Input File: example/lu_factor.cpp

</body>
</html>
