<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Computing Jacobian and Hessian of Bender's Reduced Objective</title>
<meta name="description" id="description" content="Computing Jacobian and Hessian of Bender's Reduced Objective"/>
<meta name="keywords" id="keywords" content=" Hessian Bender Jacobian Benderquad "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_benderquad_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="odegearcontrol.cpp.xml" target="_top">Prev</a>
</td><td><a href="benderquad.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>BenderQuad</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>opt_val_hes</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>BenderQuad-&gt;</option>
<option>BenderQuad.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>See Also</option>
<option>Problem</option>
<option>Purpose</option>
<option>x</option>
<option>y</option>
<option>fun</option>
<option>---..fun.f</option>
<option>---..fun.h</option>
<option>---..fun.dy</option>
<option>g</option>
<option>gx</option>
<option>gxx</option>
<option>BAvector</option>
<option>ADvector</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>Computing Jacobian and Hessian of Bender's Reduced Objective</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i></i><code><font color="blue"><span style='white-space: nowrap'>#&#xA0;include&#xA0;&lt;cppad/cppad.hpp&gt;<br/>
BenderQuad(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>g</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>gx</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>gxx</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>  

<br/>
<br/>
<b><big><a name="See Also" id="See Also">See Also</a></big></b>

<br/>
<a href="opt_val_hes.xml" target="_top"><span style='white-space: nowrap'>opt_val_hes</span></a>


<br/>
<br/>
<b><big><a name="Problem" id="Problem">Problem</a></big></b>
<br/>
The type <a href="benderquad.xml#ADvector" target="_top"><span style='white-space: nowrap'>ADvector</span></a>
 cannot be determined
form the arguments above 
(currently the type <i>ADvector</i> must be 
<code><font color="blue"><span style='white-space: nowrap'>CPPAD_TEST_VECTOR&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>.)
This will be corrected in the future by requiring <i>Fun</i>
to define <code><font color="blue"></font></code><i><span style='white-space: nowrap'>Fun</span></i><code><font color="blue"><span style='white-space: nowrap'>::vector_type</span></font></code> which will specify the
type <i>ADvector</i>.

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We are given the optimization problem

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>minimize</mi>
</mstyle></mrow>
</mtd><mtd columnalign="center" >
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>w</mi>
<mo stretchy="false">.</mo>
<mi mathvariant='normal'>r</mi>
<mo stretchy="false">.</mo>
<mi mathvariant='normal'>t</mi>
<mo stretchy="false">.</mo>
</mstyle></mrow>
<mspace width='.3em'/>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x000D7;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>m</mi>
</msup>
</mtd></mtr></mtable>
</mrow></math>

that is convex with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>y</mi>
</mrow></math>

.
In addition, we are given a set of equations 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>


such that 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mspace width='.3em'/>
<mspace width='.3em'/>
<mo stretchy="false">&#x021D2;</mo>
<mspace width='.3em'/>
<mspace width='.3em'/>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>y</mi>
</msub>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

(In fact, it is often the case that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>y</mi>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.)
Furthermore, it is easy to calculate a Newton step for these equations; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>dy</mi>
<mo stretchy="false">=</mo>
<mo stretchy="false">-</mo>
<mo stretchy="false">[</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>y</mi>
</msub>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<msup><mo stretchy="false">]</mo>
<mrow><mn>-1</mn>
</mrow>
</msup>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

The purpose of this routine is to compute the 
value, Jacobian, and Hessian of the reduced objective function

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mrow></math>

Note that if only the value and Jacobian are needed, they can be
computed more quickly using the relations

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msup><mi mathvariant='italic'>G</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>x</mi>
</msub>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mrow></math>

<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="benderquad.xml#BAvector" target="_top"><span style='white-space: nowrap'>BAvector</span></a>
 below)
and its size must be equal to <i>n</i>.
It specifies the point at which we evaluating 
the reduced objective function and its derivatives.


<br/>
<br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
It must be equal to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

; i.e., 
it must solve the problem in 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>y</mi>
</mrow></math>

 for this given value of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>



<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>minimize</mi>
</mstyle></mrow>
</mtd><mtd columnalign="center" >
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>w</mi>
<mo stretchy="false">.</mo>
<mi mathvariant='normal'>r</mi>
<mo stretchy="false">.</mo>
<mi mathvariant='normal'>t</mi>
<mo stretchy="false">.</mo>
</mstyle></mrow>
<mspace width='.3em'/>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mi mathvariant='italic'>m</mi>
</msup>
</mtd></mtr></mtable>
</mrow></math>

<br/>
<b><big><a name="fun" id="fun">fun</a></big></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> object <i>fun</i> 
must support the member functions listed below.
The <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> arguments will be variables for
a tape created by a call to <a href="independent.xml" target="_top"><span style='white-space: nowrap'>Independent</span></a>
 from <code><font color="blue">BenderQuad</font></code>
(hence they can not be combined with variables corresponding to a 
different tape). 

<br/>
<br/>
<b><a name="fun.fun.f" id="fun.fun.f">fun.f</a></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> argument <i>fun</i> supports the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.f(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.f</span></font></code> argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="benderquad.xml#ADvector" target="_top"><span style='white-space: nowrap'>ADvector</span></a>
 below)
and its size must be equal to <i>n</i>.
The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.f</span></font></code> argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.f</span></font></code> result <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to one.
The value of <i>f</i> is

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><a name="fun.fun.h" id="fun.fun.h">fun.h</a></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> argument <i>fun</i> supports the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>h</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.h(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.h</span></font></code> argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>n</i>.
The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.h</span></font></code> argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.h</span></font></code> result <i>h</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>h</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
The value of <i>h</i> is

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>h</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

.

<br/>
<br/>
<b><a name="fun.fun.dy" id="fun.fun.dy">fun.dy</a></b>
<br/>
The <code><font color="blue">BenderQuad</font></code> argument <i>fun</i> supports the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>dy</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.dy(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>h</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
<br/>
</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.dy</span></font></code> argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>n</i>.
Its value will be exactly equal to the <code><font color="blue">BenderQuad</font></code> argument 
<i>x</i> and values depending on it can be stored as private objects
in <i>f</i> and need not be recalculated.
<code><font color="blue"><span style='white-space: nowrap'><br/>
<br/>
</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.dy</span></font></code> argument <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
Its value will be exactly equal to the <code><font color="blue">BenderQuad</font></code> argument 
<i>y</i> and values depending on it can be stored as private objects
in <i>f</i> and need not be recalculated.
<code><font color="blue"><span style='white-space: nowrap'><br/>
<br/>
</span></font></code><i><span style='white-space: nowrap'>h</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.dy</span></font></code> argument <i>h</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>h</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
<code><font color="blue"><span style='white-space: nowrap'><br/>
<br/>
</span></font></code><i><span style='white-space: nowrap'>dy</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The <code><font color="blue"></font></code><i><span style='white-space: nowrap'>fun</span></i><code><font color="blue"><span style='white-space: nowrap'>.dy</span></font></code> result <i>dy</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>dy</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and its size must be equal to <i>m</i>.
The return value <i>dy</i> is given by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>dy</mi>
<mo stretchy="false">=</mo>
<mo stretchy="false">-</mo>
<mo stretchy="false">[</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mi mathvariant='italic'>y</mi>
</msub>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<msup><mo stretchy="false">]</mo>
<mrow><mn>-1</mn>
</mrow>
</msup>
<mi mathvariant='italic'>h</mi>
</mrow></math>

Note that if <i>h</i> is equal to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>dy</mi>
</mrow></math>

 is the Newton step for finding a zero
of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 with respect to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>y</mi>
</mrow></math>

;
i.e., 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>dy</mi>
</mrow></math>

 is an approximate solution for the equation

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>dy</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

. 

<br/>
<br/>
<b><big><a name="g" id="g">g</a></big></b>
<br/>
The argument <i>g</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>g</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and has size one.
The input value of its element does not matter.
On output,
it contains the value of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>g</mi>
<mo stretchy="false">[</mo>
<mn>0</mn>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="gx" id="gx">gx</a></big></b>
<br/>
The argument <i>gx</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>gx</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and has size 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.
The input values of its elements do not matter.
On output,
it contains the Jacobian of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

, 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>gx</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>G</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<msub><mo stretchy="false">)</mo>
<mi mathvariant='italic'>j</mi>
</msub>
</mrow></math>

<br/>
<b><big><a name="gxx" id="gxx">gxx</a></big></b>
<br/>
The argument <i>gx</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>gxx</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and has size 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

.
The input values of its elements do not matter.
On output,
it contains the Hessian of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

; i.e.,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

, and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

, 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>gxx</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>G</mi>
<mrow><mo stretchy="false">(</mo>
<mn>2</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<msub><mo stretchy="false">)</mo>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
</mrow></math>

<br/>
<b><big><a name="BAvector" id="BAvector">BAvector</a></big></b>
<br/>
The type <i>BAvector</i> must be a 
<a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class. 
We use <i>Base</i> to refer to the type of the elements of 
<i>BAvector</i>; i.e.,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>::value_type<br/>
</span></font></code><br/>
<b><big><a name="ADvector" id="ADvector">ADvector</a></big></b>
<br/>
The type <i>ADvector</i> must be a 
<a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with elements of type 
<code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>; i.e.,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>::value_type<br/>
</span></font></code>must be the same type as
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>BAvector</span></i><code><font color="blue"><span style='white-space: nowrap'>::value_type&#xA0;&gt;<br/>
</span></font></code>.


<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="benderquad.cpp.xml" target="_top"><span style='white-space: nowrap'>BenderQuad.cpp</span></a>

contains an example and test of this operation.   
It returns true if it succeeds and false otherwise.



<hr/>Input File: cppad/local/bender_quad.hpp

</body>
</html>
