<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<chapter>
  <title>Simulation</title>

  <section>
    <title>Simulation problems</title>

    <para>Simulation is the process of executing experiments on a
    parameterized model over time. The model represents thereby the behaviour
    of a real physical process. The purpose of creating physical models and
    running simulations is to fit the parameter values in that way that the
    process can be improved (e.g. less energy wastage, higher production rate,
    less time).</para>

    <para>The models are based on mathematical equations, ordinary
    differential equations (ODEs) as well as differential algebraic equations
    (DAEs). They contain states (variables) with related initial conditions,
    parameters and inputs, which can be set external.</para>
  </section>

  <section>
    <title>Getting started<action/></title>

    <para>As a first example we consider a <emphasis>Van der Po</emphasis>l
    (VDP) oscillator, which is an oscillatory system with non-linear damping.
    The model contains two states <literal>x1</literal> and
    <literal>x2</literal> and is represented by a second order differential
    equation. For small amplitudes the damping is negative. When the amplitude
    is increased up to a certain limit, the damping becomes positive. The
    system stabilises and a limit cycle develops. In Modelica all parameters,
    inputs and states with initial values have to be listed. Introducing an
    additional state <literal>x2</literal> the second order equation can be
    transformed into two first order equations.</para>

    <programlisting>model VDP "Van der Pol model"

   // State start values
   parameter Real x1_0 = 0;
   parameter Real x2_0 = 1;

   // The states
   Real x1(start = x1_0);
   Real x2(start = x2_0);

   // Parameters
   parameter Real d=1;

   // The control signal
   Modelica.Blocks.Interfaces.RealInput u

equation 
   der(x1) = d*(1 - x2^2) * x1 - x2 + u;
   der(x2) = x1;
end VDP;</programlisting>

    <para>The following plots are the results of the simulation. When x2 is
    plotted against x1the occurring limit cycle bespeaks an stable
    system.</para>

    <figure>
      <title>VDP- x1 and x2 depended on time</title>

      <mediaobject>
        <imageobject>
          <imagedata align="left" fileref="Doc%20pictures/vdp_doc.png"
                     scale="50" width="600"/>
        </imageobject>
      </mediaobject>
    </figure>

    <figure>
      <title>VDP- x1 depended on x2 (limit cycle)</title>

      <mediaobject>
        <imageobject>
          <imagedata align="left" fileref="Doc%20pictures/vdp_doc2.png"
                     scale="50" width="600"/>
        </imageobject>
      </mediaobject>
    </figure>
  </section>

  <section>
    <title>Distillation column</title>

    <section>
      <title>Model description</title>

      <para>Distillation is the physical process of separating mixtures based
      on the different volatilities of their components. In this binary
      distillation column model the two components cyclohexane and n-heptane
      are separated over 30 and 40 trays respectively. The model is set up in
      four different ways. The first two models only contain ODEs of the mole
      fraction <literal>x</literal> of cyclohexane at each tray. From the
      equilibrium assumption and mole balances the vapor mole fraction of the
      first component as well as both values for the second component can be
      calculated:</para>

      <itemizedlist>
        <listitem>
          <para>vol = (yA/xA) / (yB/xB)</para>
        </listitem>
      </itemizedlist>

      <itemizedlist>
        <listitem>
          <para>xA + xB = 1</para>
        </listitem>
      </itemizedlist>

      <itemizedlist>
        <listitem>
          <para>yA + yB = 1</para>
        </listitem>
      </itemizedlist>

      <para>In the third version additional to the 30 states of the mole
      fraction DAEs for the temperature at each tray are adjoined. This
      version should be represented more in detail later.</para>

      <para>The fourth version represents the most complex model. The number
      of trays is raised to 40 and it is described through the following
      equations:</para>

      <itemizedlist>
        <listitem>
          <para>42 ODEs for the mole fraction of cyclohexane
          <literal>xA</literal> at each tray and for the reboiler and
          condenser</para>
        </listitem>
      </itemizedlist>

      <itemizedlist>
        <listitem>
          <para>42 DAEs for the temperature at each tray and for the reboiler
          and condenser</para>
        </listitem>
      </itemizedlist>

      <itemizedlist>
        <listitem>
          <para>41 DAEs for the vapor molar flux between the trays, the
          reboiler and the condenser</para>
        </listitem>
      </itemizedlist>

      <para>The model depends on two inputs: The heat input to the reboiler
      provided by an electric heater and the reflux flow rate of the recycled
      distillate <literal>Vdot_L1</literal>. The reflux ratio displays the
      amount of condensed overhead liquid product that is returned back to the
      upper part of the system. The down flowing reflux ratio provides cooling
      and condensation of the up flowing vapors and so the efficiency of
      distillation column can be increased. To introduce an input Dymola
      provides a special component Modelica.Blocks.Interfaces.RealInput. The
      use of this component makes it possible to experiment with different
      values an even time depended functions of the input.</para>

      <figure>
        <title>Picture of a distillation column (from Wikipedia: Fractional
        distillation)</title>

        <mediaobject>
          <imageobject>
            <imagedata align="left"
                       fileref="Doc%20pictures/Distillation_Column.png"
                       scale="120"/>
          </imageobject>
        </mediaobject>
      </figure>
    </section>

    <section>
      <title>Set up and simulate the model in Modelica</title>

      <para>We consider the third version of the distillation column more in
      detail now. As before all parameter and variable declarations as well as
      start values, ODEs and DAEs have to be defined. To ensure the physical
      correctness of a model it is very helpful to work with SIunits. Thus the
      units of all equations are automatically adjusted and in the case of a
      mismatch an error occurs.</para>

      <programlisting>model Distillation3

  import SI = Modelica.SIunits;

  parameter Real rr=2.7 "reflux ratio";
  parameter SI.MolarFlowRate Feed =  24.0/3600 "Feed Flowrate";
  parameter Real x_Feed = 0.5 "Mole Fraction of Feed";
  parameter SI.MolarFlowRate D=0.5*Feed "Distillate Flowrate";
  parameter SI.MolarFlowRate L=rr*D 
    "Flowrate of the Liquid in the Rectification Section";
  parameter SI.MolarFlowRate V=L+D "Vapor Flowrate in the Column";
  parameter SI.MolarFlowRate FL=Feed+L 
    "Flowrate of the Liquid in the Stripping Section";
  parameter Real atray=0.25 "Total Molar Holdup in the Condenser";
  parameter Real acond=0.5 "Total Molar Holdup on each Tray";
  parameter Real areb=1.0 "Total Molar Holdup in the Reboiler";
  parameter SI.Pressure P=101000 "total pressure in column (Pa)";
  parameter Real[5,2] DIPPR= {{5.1087E1,    8.7829E1},
         {-5.2264E3,   -6.9964E3},
         {-4.2278E0,   -9.8802E0},
          {9.7554E-18,  7.2099E-6},
          {6.0000E0,    2.0000E0}} "Saturated Vapor Pressures
                                    Data from the DIPPR Database (empirical fit";
  parameter Real L12 = 1.618147731 "Activity Coefficients of Liquid Mixture";
  parameter Real L21 = 0.50253532 
    "Wilson Activity Coefficient Model Parameters";

  parameter Real x_init[32]={0.97287970129754,
   0.95636038934316,
   .
   .
   .
   0.02712029870246};
  parameter SI.Temperature T_init[32]=100 *{3.54170894061095,
   3.54384309151657,
   .
   .
   .
   3.70819628750959};

  Real x[32](start=x_init) "mole fraction of A at each state, column vector";
  SI.Temperature T[32](start=T_init) "Temperature at each state";
  Real y[32] "vapor Mole Fractions of Component A";
  Real PsatA[32];
  Real PsatB[32];
  Real gammaA[32];
  Real gammaB[32];

equation 
  for i in 1:32 loop
   PsatA[i] = exp(DIPPR[1,1] + DIPPR[2,1]/T[i] + DIPPR[3,1] * log(T[i]) +
      DIPPR[4,1] * (T[i]^DIPPR[5,1]));
   PsatB[i] = exp(DIPPR[1,2] + DIPPR[2,2]/T[i] + DIPPR[3,2] * log(T[i]) +
      DIPPR[4,2] * (T[i]^DIPPR[5,2]));
  end for;

  for i in 1:32 loop
     gammaA[i] = exp(-log(x[i] + L12 * (1 - x[i])) + (1 - x[i]) * 
                 (L12 / (x[i] + L12 * (1 - x[i])) - (L21 / (L21 * x[i] + (1 - x[i])))));
     gammaB[i] = exp(-log((1 - x[i]) + L21 * x[i]) + x[i] * 
                 (L21 / ((1 - x[i]) + L21 * x[i]) - (L12 / (L12 * (1 - x[i]) + x[i]))));
  end for "Wilson Equations";

  for i in 1:32 loop
     y[i] = x[i]*gammaA[i]*(PsatA[i] / P) "Vapor Mole Fractions of Component A";
  end for;

//ODE
   der(x[1]) = 1/acond *(V*(y[2]-x[1])) "condenser";
   der(x[2:16])  = 1/atray *(L*(x[1:15]-x[2:16]) - V*(y[2:16]-y[3:17])) 
    "15 column stages";
   der(x[17]) = 1/atray * (Feed*x_Feed + L*x[16] - FL*x[17] - V*(y[17]-y[18])) 
    "feed tray";
   der(x[18:31]) = 1/atray * (FL*(x[17:30]-x[18:31]) - V*(y[18:31]-y[19:32])) 
    "14 column stages";
   der(x[32]) = 1/areb  * (FL*x[31] - (Feed-D)*x[32] - V*y[32]) "reboiler";

//DAE
   for i in 1:32 loop
      0= ((x[i]*gammaA[i]*PsatA[i]) + ((1-x[i])*gammaB[i]*PsatB[i])-P)/P 
      "der( T[i])=0";
   end for;
end Distillation3;
</programlisting>

      <para>In contrast to the extensive DAE handling in Matlab, it is
      sufficient to set the state derivatives 0 in Modelica.</para>

      <para>Experimenting with the inputs the purity of the components can be
      raised at the bottom and the top of the distillation column by raising
      the heat reflux.</para>
    </section>

    <section>
      <title>Compile, load and simulate Modelica models in
      JModelica.org</title>

      <para>To run simulations in JModelica the Python language is used. Some
      imports are necessary to be able to compile and load the models.</para>

      <programlisting># Import library for path manipulations
import os.path

# Import numerical libraries
import numpy as N
import matplotlib.pyplot as plt

# Import the JModelica.org Python packages
from pymodelica import compile_fmu
from pyfmi import FMUModel</programlisting>

      <para>The distillation3 model should be simulated for 7200 s.</para>

      <programlisting>curr_dir = os.path.dirname(os.path.abspath(__file__));

fmu_name1 = compile_fmu("JMExamples.Distillation.Distillation1", 
curr_dir+"/files/JMExamples.mo")
dist3 = FMUModel(fmu_name1)
    
res = dist3.simulate(final_time=7200)</programlisting>

      <para>At the end all variables are extracted, the results can be printed
      and plotted. More details considering this topic can be found in the
      JModelica User's Guide.</para>
    </section>

    <section>
      <title>Simulation results and interpretation</title>

      <para>The plots of the mole fractions <literal>x</literal> and the vapor
      mole fractions <literal>y</literal> of component A at tray 1,8,16 and 32
      are shown. The reflux flow rate was reduced from 3 to 2.7. Therefore the
      purity of the components at the top and the bottom decreases.</para>

      <figure>
        <title>Distillation column (reflux ratio = 2.7)</title>

        <mediaobject>
          <imageobject>
            <imagedata align="left" fileref="Doc%20pictures/dist1_2.7_doc.png"
                       scale="67"/>
          </imageobject>
        </mediaobject>
      </figure>

      <para>Though a high purity at the trays is desirable since the purpose
      of distillation is the separation of the components. From theoretical
      considerations the conclusion can be drawn that a high reflux ratio
      implies a high purity, which means that x is getting closer to 1 at the
      top and closer to 0 at the bottom of the distillation column. This
      statement can be verified by setting the reflux ratio = 3.7.</para>

      <figure>
        <title>Distillation column (reflux ratio = 3.7)</title>

        <mediaobject>
          <imageobject>
            <imagedata align="left"
                       fileref="../../../../Documents%20and%20Settings/sabine/My%20Documents/Dymola/Doc%20pictures/dist1_3.7_doc.png"
                       scale="70"/>
          </imageobject>
        </mediaobject>
      </figure>

      <para>It can be figured out that the purity of the components is
      directly depended on the reflux flow rate as an input. Only experiments
      certainly seem not to be convincing when searching for the optimal input
      to get the highest purity considering all circumstances. In the next
      chapter I will go one step further using optimization which leads to the
      aim of finding the optimal solution.</para>

      <para>To verify the existence of a steady state condition for rr=3, a
      step function is created as an input, where rr=3 is kept constant for
      1000s and is then increased to 3.7. In case of a steady state condition,
      the derivative is 0, the states stay constant. </para>

      <figure>
        <title>Distillation column (reflux ratio = 3.0 for 1000s, then
        3.7)</title>

        <mediaobject>
          <imageobject>
            <imagedata align="left" fileref="Doc%20pictures/dist1_step.png"
                       scale="72"/>
          </imageobject>
        </mediaobject>
      </figure>
    </section>
  </section>
</chapter>
