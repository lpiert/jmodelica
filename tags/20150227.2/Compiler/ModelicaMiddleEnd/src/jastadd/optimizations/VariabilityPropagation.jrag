/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


aspect VariabilityPropagation {
	
    public boolean FFunctionCallEquation.taggedForCleanup = false;
	public boolean FAbstractEquation.taggedForRemoval = false;
	public boolean FAbstractVariable.taggedForRemoval = false;
	public boolean FFunctionCallLeft.taggedForRemoval = false;
	public boolean FExp.taggedForRemoval              = false;
	
    public void FAbstractEquation.cleanEquations(ArrayList<FAbstractEquation> l) {
        if (!taggedForRemoval) {
            l.add(this);
        }
        taggedForRemoval = false;
    }
    public void FFunctionCallEquation.cleanEquations(ArrayList<FAbstractEquation> l) {
        super.cleanEquations(l);
        if (taggedForCleanup) {
            for (FFunctionCallLeft left : getLefts()) {
                left.cleanEquations();
            }
            taggedForCleanup = false;
        }
    }
    public void FFunctionCallLeft.cleanEquations() {
        if (taggedForRemoval) {
            setFExpOpt(new Opt());
            taggedForRemoval = false;
        } else if (hasFExp()){
            setFExpOpt(new Opt(getFExp().cleanEquations()));
        }
    }
    
    public FExp FExp.cleanEquations() {
        if (taggedForRemoval) {
            return new FNoExp();
        } else {
            return this;
        }
    }
    
    @Override
    public FExp FArray.cleanEquations() {
        FArray res = new FArray();
        for (FExp exp : getFExps()) {
            res.addFExpNoTransform(exp.cleanEquations());
        }
        return res;
    }
    
    @Override
    public FExp FRecordConstructor.cleanEquations() {
        FRecordConstructor res = new FRecordConstructor(getRecord().name());
        for (FExp arg : getArgs()) {
            res.addArgNoTransform(arg.cleanEquations());
        }
        return res;
    }
    
    
	
	/**
	 * Checks if equation is eligible for var. prop.
	 */
    syn boolean FAbstractEquation.canPropagate() =
            canPropagateContext() && !containsActiveAnnotations();
    eq FEquation.canPropagate() = super.canPropagate() && variables().size() == 1;
    eq FFunctionCallEquation.canPropagate() {
        getCall().flushAllRecursiveClearFinal();
        return super.canPropagate() && variables().size() > 0 && numScalarEquations() > 0 && 
                ((variables().size() == numScalarEquations() && getCall().variability().parameterOrLess()) 
                        || potentialPartialVariability().knownParameterOrLess());
    }
    
    inh boolean FAbstractEquation.canPropagateContext();
    eq Root.getChild().canPropagateContext() = false;
    eq FClass.getFAbstractEquation().canPropagateContext() = true;
    eq FIfWhenElseEquation.getChild().canPropagateContext() = false;
    
    /**
     * If any arg is constant return constant. Otherwise returns combined variability.
     */
    syn FTypePrefixVariability FFunctionCallEquation.potentialPartialVariability()
        = getCall().potentialPartialVariability();
    
    syn FTypePrefixVariability FAbstractFunctionCall.potentialPartialVariability() = variability();
    eq FFunctionCall.potentialPartialVariability() {
        for (FExp exp : getArgs()) {
            if (exp.variability().knownParameterOrLess()) {
                return fConstant();
            }
        }
        return super.potentialPartialVariability();
    }
    
	/**
	 * Checks if left hand side variable(s) is eligible for var. prop.
	 */
	syn boolean FExp.canPropagate() {
		throw new UnsupportedOperationException();
	}
	eq FRecordConstructor.canPropagate() {
		for (FExp exp : getArgs()) {
			if (!exp.canPropagate()) {
				return false;
			}
		}
		return true;
	}
	eq FArray.canPropagate() {
		for (FExp exp : getFExps()) {
			if (!exp.canPropagate()) {
				return false;
			}
		}
		return true;
	}
	eq FIdUseExp.canPropagate() {
		return myFV().canPropagate();
	}
	syn boolean FAbstractVariable.canPropagate() = false;
	eq FVariable.canPropagate() = !isOutput() && !containsActiveAnnotations();
	eq FDerivativeVariable.canPropagate() = false;
	
	// Hook for checks that are needed in optimica
	syn boolean FEquation.canPropagate(FVariable var, FExp solution) = true;

    public class FClass {
        /**
         * Runs variability propagation if option is set.
         */
        public class variabilityPropagationIfSet extends Transformation {
            public variabilityPropagationIfSet() {
                super("variability_propagation");
            }
            
            public void perform() {
                variabilityPropagation();
                enableConstantFolding();
                forceRewrites();
                root().flushAllRecursiveClearFinal(); // Enable expression rewrites
            }
        }
    }
	
	/**
	 * Runs variability propagation.
	 */
	public void FClass.variabilityPropagation() {

		List<FAbstractEquation> parameterEquations = getFParameterEquations();
		LinkedHashSet<FAbstractEquation> worklist = new LinkedHashSet<FAbstractEquation>();
		List<FAbstractEquation> equations = getFAbstractEquations();

		// Build worklist
		for (FAbstractEquation equation: equations) {
			if (equation.canPropagate()) {
				worklist.add(equation);
			}
		}

		// Work
		while (!worklist.isEmpty()) {
			FAbstractEquation equation = worklist.iterator().next();
			equation.variabilityPropagation(worklist, parameterEquations);
			worklist.remove(equation);
		}

		// Clean variables
        ArrayList<FVariable> newVariables = new ArrayList<FVariable>();
        ArrayList<FVariable> newAliases   = new ArrayList<FVariable>();
        AliasManager am = getAliasManager();
        for (FVariable fVariable: getFVariables()) {
            if (!fVariable.taggedForRemoval) {
                newVariables.add(fVariable);
            }
        }
        for (FVariable fVariable: getAliasVariables()) {
            if (am.getAliasSet(fVariable) != null) {
                newAliases.add(fVariable);
            } else if (!fVariable.isTemporary()) {
                newVariables.add(fVariable);
            }
        }
        setFVariableList(new List<FVariable>(newVariables));
        setAliasVariableList(new List<FVariable>(newAliases));

		// Clean equations
		ArrayList<FAbstractEquation> newEquations = new ArrayList<FAbstractEquation>();
		for (FAbstractEquation equation: equations) {
			equation.cleanEquations(newEquations);
		}
		setFAbstractEquationList(new List<FAbstractEquation>(newEquations));
		
		// Clean initial equations
		newEquations = new ArrayList<FAbstractEquation>();
		for (FAbstractEquation equation: initialEquations()) {
			if (!equation.taggedForRemoval) {
				newEquations.add(equation);
			}
		}
		setFInitialEquationList(new List<FAbstractEquation>(newEquations));
	}
	
	public void FAbstractEquation.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
			List<FAbstractEquation> parameterEquations) {
		
	}
		
	public void FEquation.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
			List<FAbstractEquation> parameterEquations) {
		
		FVariable fVariable;
		FExp solution;
		FTypePrefixVariability solutionVariability;

		if (!canPropagate()) {
			return; // Unbalanced equation
		}

		fVariable = variables().iterator().next();

		if (!fVariable.canPropagate()) {
			return;
		}

		solution = solution(fVariable);
		solution.resetOriginalReferences();
		
		if (!solution.isValidExp()) {
			return;
		}
		
		if (!canPropagate(fVariable, solution)) {
			return;
		}
		
		solutionVariability = solution.variability();

		if (!solutionVariability.parameterOrLess()) {
			// Sometimes combinations of parameters and constants yield 
			// discrete variability. IfExpr, sample() etc.
			return;
		}

		if (solutionVariability.knownParameterOrLess()) {
			try {
				solution = solution.ceval().buildLiteral();
			} catch (ConstantEvaluationException e) {
				// If we can't evaluate right now, set it as parameter
				solutionVariability = fParameter();
			}
		} else {
			parameterEquations.add(new FEquation(fVariable.createUseExp(), solution));
		}

		this.taggedForRemoval = true;
		fVariable.variabilityPropagation(worklist, solutionVariability, solution, parameterEquations);

	}
	
	public void FFunctionCallEquation.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
			List<FAbstractEquation> parameterEquations) {
        
        if (!canPropagate()) {
            return;
        }
        
        boolean param = getCall().variability().parameterOrLess();
        for (FFunctionCallLeft left : getLefts()) {
            if (left.hasFExp() && !left.getFExp().canPropagate()) {
                param = false;
            }
        }
        FTypePrefixVariability variability = potentialPartialVariability();
        boolean eval = variability.knownParameterOrLess();
        
        CValue[] val = null;
        if (eval) {
            try {
                VariableEvaluator evaluator;
                boolean fullEval = getCall().variability().knownParameterOrLess();
                if (fullEval) {
                    evaluator = defaultVariableEvaluator();
                } else {
                    evaluator = new PartialVariableEvaluator();
                }
                val = getCall().evaluate(evaluator);
            } catch (ConstantEvaluationException e) {
                eval = false;
            }
        }
        
        boolean allRemoved = true;
        int i = 0;
        for (FFunctionCallLeft left : getLefts()) {
            if (left.hasFExp()) {
                if (left.getFExp().canPropagate()) {
                    left.taggedForRemoval = left.getFExp().variabilityPropagation(worklist,
                            eval ? val[i] : CValue.UNKNOWN, param ? parameterEquations : null);
                }
                allRemoved &= left.taggedForRemoval;
            }
            i++;
        }
        
        if (allRemoved) {
            this.taggedForRemoval = true;
        } else {
            if (param) {
                parameterEquations.add(this);
                this.taggedForRemoval = true;
            }
        }
        this.taggedForCleanup = true;
    }
	
	public boolean FExp.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
			CValue value,
			List<FAbstractEquation> parameterEquations) {
		
		throw new UnsupportedOperationException();
	}
	
	public boolean FRecordConstructor.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
            CValue value,
            List<FAbstractEquation> parameterEquations) {
	    boolean res = true;
		CValueRecord frc = null;
		if (!value.isUnknown()) {
			frc = (CValueRecord) value;
		}

		for (int i = 0; i < getNumArg(); i++) {
			res &= getArg(i).variabilityPropagation(worklist, 
					frc != null ? frc.getMember(i) : CValue.UNKNOWN, parameterEquations);
		}
		taggedForRemoval = res;
		return res;
	}
	
	public boolean FArray.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
            CValue value,
            List<FAbstractEquation> parameterEquations) {
		
		CValueArray a = null;
		if (value != CValue.UNKNOWN) {
			a = (CValueArray) value;
		}
		taggedForRemoval = this.variabilityPropagationArray(worklist, a, parameterEquations, new Enumerator());
		return taggedForRemoval;
	}
	
    public boolean FExp.variabilityPropagationArray(
            LinkedHashSet<FAbstractEquation> worklist,
            CValueArray value,
            List<FAbstractEquation> parameterEquations,
            Enumerator enumerator) {
        return this.variabilityPropagation(worklist, 
                    value == null ? CValue.UNKNOWN : value.values[enumerator.next()], parameterEquations);
    }
    
    public boolean FArray.variabilityPropagationArray(
            LinkedHashSet<FAbstractEquation> worklist,
            CValueArray value,
            List<FAbstractEquation> parameterEquations,
            Enumerator enumerator) {
        boolean res = true;
        for (FExp exp : getFExps()) {
            res &= exp.variabilityPropagationArray(worklist, value, parameterEquations, enumerator);
        }
        return res;
    }
	
	
    public boolean FIdUseExp.variabilityPropagation(
            LinkedHashSet<FAbstractEquation> worklist,
            CValue value,
            List<FAbstractEquation> parameterEquations) {
        
        FExp bexp = null;
        FTypePrefixVariability variability = null;
        if (!value.isUnknown()) {
            try {
                bexp = value.buildLiteral();
                variability = fConstant();
            } catch (ConstantEvaluationException e) {
                variability = fParameter();
            }
        } else {
            variability = fParameter();
        }
        
        if (variability.knownParameterOrLess() || parameterEquations != null) {
            myFV().variabilityPropagation(worklist, variability, bexp, parameterEquations);
        }
        
        taggedForRemoval = variability.knownParameterOrLess();
        return taggedForRemoval;
    }
    
    
	public void FAbstractVariable.variabilityPropagation(
			LinkedHashSet<FAbstractEquation> worklist,
			FTypePrefixVariability variability,
            FExp bindingExp,
            List<FAbstractEquation> parameterEquations) {
		
		throw new UnsupportedOperationException();
	}

    public void FVariable.variabilityPropagation(
            LinkedHashSet<FAbstractEquation> worklist,
            FTypePrefixVariability variability,
            FExp bindingExp,
            List<FAbstractEquation> parameterEquations) {
        
        FAbstractEquation equation;
        boolean wasDiscrete = isDiscrete();
        
        // Declaration
        // TODO: after we have added "dependent parameter that is evaluated" variability, this exception should use that instead
        if (variability.evalParameterVariability())
            variability = fStructParameter();
        setFTypePrefixVariability(variability);
        if (variability.knownParameterOrLess()) {
            setBindingExp(bindingExp.fullCopy());
        }
        if (variability.parameterVariability()) {
            if (fixedAttributeSet()) {
                setFixedAttribute(true);
            }
        }
        
        // Uses
        for (FIdUseExp use : uses()) {
            if (use.inFEquation()) {
                equation = use.myFEquation();
                equation.variables().remove(this);
                if (equation.canPropagate()) {
                    worklist.add(equation);
                } else if (equation.inInitialEquationSection() && equation.variables().size() == 0
                        && equation.nonFixedParameters().size() == 0) {
                    equation.taggedForRemoval = true; // Initial equation
                }
            }
        }
        
        // Pre var
        if (wasDiscrete) {
            FAbstractVariable pre = myPreVariable();
            pre.taggedForRemoval = true;
            pre.variabilityPropagation(worklist, variability, bindingExp, parameterEquations);
        }
        
        // Derivative
        if (isDifferentiatedVariable()) {
            FDerivativeVariable fDerivativeVariable = myDerivativeVariable();
            fDerivativeVariable.taggedForRemoval = true;
            fDerivativeVariable.variabilityPropagation(worklist, FTypePrefixVariability.fConstant(), new FIntegerLitExp(0), parameterEquations);
        }
        
        // Alias variables
        if (aliasSet() != null) {
            AliasManager.AliasVariable thisAlias = aliasSet().getIterationVariable();
            for (AliasManager.AliasVariable av : aliasSet()) {
                FVariable fv = av.getFVariable();
                boolean neg = av.isNegated() ^ thisAlias.isNegated();
                if (fv != this) {
                    fv.setFTypePrefixVariability(variability);
                    if (variability.knownParameterOrLess())
                        fv.setBindingExp(bindingExp.negated(neg));
                    if (variability.parameterVariability() && !fv.isTemporary())
                        parameterEquations.add(new FEquation(fv.createUseExp(), createUseExp().negated(neg)));
                }
            }
            myFClass().getAliasManager().unAlias(this);
        }
    }
}

aspect ConstantFolding {
	public boolean FAbstractExp.constantFoldingEnabled = false;
	
	/**
	 * Enables constant folding
	 */
	public void FClass.enableConstantFolding() {
		for (FAbstractEquation equation : allEquations()) {
			equation.enableConstantFolding();
		}
	}
	
	public void ASTNode.enableConstantFolding() {
	    for (ASTNode n : this)
	        n.enableConstantFolding();
	}
	
	public void FFunctionCallEquation.enableConstantFolding() {
		for (FExp childExp : getCall().childFExps()) {
			childExp.enableConstantFolding();
		}
	}
	
    public void FAlgorithm.enableConstantFolding() {
       
    }
    
    public void FPartialFunctionCall.enableConstantFolding() {
        for (ASTNode n : this)
            n.enableConstantFolding();
    }
	
	public void FIfExp.enableConstantFolding() {
		super.enableConstantFolding();
		if (!constantFoldingEnabled && getIfExp().isConstantExp()) {
			eliminateIfEquation = true;
			is$Final = false;
		}
	}
	
	public void FAbstractExp.enableConstantFolding() {
		if (isConstantExp()) {
			constantFoldingEnabled = true;
			is$Final = false;
			return;
		}
		super.enableConstantFolding();
	}
	public void FIdUseExp.enableConstantFolding() {
		if (type().isString()) {
			constantFoldingEnabled = true;
			is$Final = false;
			return;
		}
		super.enableConstantFolding();
	}
	public void FPreExp.enableConstantFolding() {
		if (variability().parameterOrLess()) {
			constantFoldingEnabled = true;
			is$Final = false;
			return;
		}
	}

	rewrite FAbstractExp {
		when (constantFoldingEnabled && isConstantExp()) to FExp {
			try {
				CValue value = ceval();
				if (!value.size().isEmpty()) 
					return value.buildLiteral();
			} catch (ConstantEvaluationException e) {
			}
			constantFoldingEnabled = false;
			return this;
		}
	}

	rewrite FIdUseExp {
		when (constantFoldingEnabled && type().isString()) to FExp {
			try {
				CValue value = ceval();
				if (!value.size().isEmpty()) 
					return value.buildLiteral();
			} catch (ConstantEvaluationException e) {
				compliance("Could not evaluate string variable " + name());
			}
			constantFoldingEnabled = false;
			return this;
		}
	}
	
	rewrite FPreExp {
		when (constantFoldingEnabled) to FExp {
			return new FIdUseExp(getFIdUse().fullCopy());
		}
	}
	
}
