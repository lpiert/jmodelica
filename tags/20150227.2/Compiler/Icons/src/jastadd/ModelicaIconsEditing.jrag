import org.jmodelica.icons.Observable;
import org.jmodelica.icons.primitives.Line;
import org.jmodelica.icons.Observer;

aspect ModelicaIconsEditing {
	
	/*-------------------------------------------------------------------------
	 *                          Connection related
	 ------------------------------------------------------------------------*/
	
	/**
	 * Thread safe method that retreives the connection line for this connection.
	 */
	public Line FConnectClause.syncGetConnectionLine() {
		synchronized (state()) {
			return getConnectClause().syncGetConnectionLine();
		}
	}
	
	private Line ConnectClause.connectionLine;
	
	/**
	 * Thread safe method that retreives the connection line for this connection.
	 */
	public Line ConnectClause.syncGetConnectionLine() {
		synchronized (state()) {
			if (connectionLine != null)
				return connectionLine;
			try {
				connectionLine = annotation().createConnectionLine();
			} catch (FailedConstructionException e) {
				connectionLine = new Line();
			}
			connectionLine.addObserver(this);
			return connectionLine;
		}
	}
	
	public class ConnectClause implements Observer{}
	
	@Override
	public void ConnectClause.update(Observable o, Object flag, Object additionalInfo) {
		synchronized (state()) {
			if (o == syncGetConnectionLine())
				annotation().saveConnectionLine(syncGetConnectionLine());
		}
	}
	
	/*-------------------------------------------------------------------------
	 *                          InstNode related
	 ------------------------------------------------------------------------*/
	
	/**
	 * Thread safe method that lookup the parameter value for <code>paramter</code>
	 */
	public String InstNode.syncLookupParameterValue(String parameter) {
		synchronized (state()) {
			for (Object o : memberInstComponent(parameter)) {
				if (o instanceof InstPrimitive) {
					InstPrimitive ip = (InstPrimitive) o;
					return ip.ceval().toString();
				}
			}
			return null;
		}
	}

	
	/*-------------------------------------------------------------------------
	 *                          Component related
	 ------------------------------------------------------------------------*/
	/**
	 * Thread safe method that retreives the placement for this component.
	 */
	public Placement InstComponentDecl.syncGetPlacement() {
		synchronized (state()) {
			return getComponentDecl().syncGetPlacement();
		}
	}
	
	private Placement ComponentDecl.placement;
	
	/**
	 * Thread safe method that retreives the placement for this component.
	 */
	public Placement ComponentDecl.syncGetPlacement() {
		synchronized (state()) {
			if (placement != null)
				return placement;
			
			try {
				placement = annotation().createPlacement(Context.DIAGRAM);
			}
			catch(FailedConstructionException fe) {
				placement = new Placement(new Transformation(new Extent(new Point(-10, -10), new Point(10, 10))));
			}
			placement.addObserver(this);
			return placement;
		}
	}
	
	public class ComponentDecl implements Observer{}
	
	@Override
	public void ComponentDecl.update(Observable o, Object flag, Object additionalInfo) {
		synchronized (state()) {
			if (o == syncGetPlacement())
				annotation().savePlacement(syncGetPlacement());
		}
	}
	
	/**
	 * Thread safe method that checks if a component should be rendered.
	 */
	public boolean InstComponentDecl.syncIsIconRenderable() {
		if (myInstClass().isUnknown())
			return false;
		if (!(myInstClass() instanceof InstBaseClassDecl))
			return false;
		if (hasConditionalAttribute()) {
			CValue val = getConditionalAttribute().ceval();
			if (val instanceof CValueBoolean && !val.booleanValue()) {
				return false;
			}
		}
		if (isConnector() && getComponentDecl().isProtected())
			return false;
		return true;
	}
	
	
	syn ClassDecl InstNode.iconClassDecl() = null;
	eq InstClassDecl.iconClassDecl() = getClassDecl();
	eq InstComponentDecl.iconClassDecl() = myInstClass().iconClassDecl();
	eq InstExtends.iconClassDecl() = myInstClass().iconClassDecl();
	
	/**
	 * Thread safe method that retreives the icon layer for this component.
	 */
	public Layer InstNode.syncGetIconLayer() {
		synchronized (state()) {
			ClassDecl decl = iconClassDecl();
			if (decl == null)
				return Layer.NO_LAYER;
			else
				return decl.iconLayer();
		}
	}
	
	/**
	 * Thread safe method that retreives the diagram layer for this component.
	 */
	public Layer InstNode.syncGetDiagramLayer() {
		synchronized (state()) {
			ClassDecl decl = iconClassDecl();
			if (decl == null)
				return Layer.NO_LAYER;
			else
				return decl.diagramLayer();
		}
	}
	
	
	private Layer ClassDecl.iconLayer;
	
	public Layer ClassDecl.iconLayer() {
		if (iconLayer != null)
			return iconLayer;
		iconLayer = annotation().createIconLayer();
		iconLayer.addObserver(this);
		return iconLayer;
	}
	
	private Layer ClassDecl.diagramLayer;
	
	public Layer ClassDecl.diagramLayer() {
		if (diagramLayer != null)
			return diagramLayer;
		diagramLayer = annotation().createDiagramLayer();
		diagramLayer.addObserver(this);
		return diagramLayer;
	}
	
	
	public class ClassDecl implements Observer{}
	
	@Override
	public void ClassDecl.update(Observable o, Object flag, Object additionalInfo) {
		synchronized (state()) {
			if (o == iconLayer) {
				if (flag == Layer.COORDINATE_SYSTEM_UPDATED)
					annotation().saveIconCoordinateSystem(iconLayer.getCoordinateSystem());
//				if (flag == Layer.GRAPHICS_SWAPPED) //TODO:add support for graphics change.
//					annotation().save(iconLayer.getGraphics().toArray(new GraphicItem[iconLayer.getGraphics().size()]));
			}
			if (o == diagramLayer) {
				if (flag == Layer.COORDINATE_SYSTEM_UPDATED)
					annotation().saveDiagramCoordinateSystem(diagramLayer.getCoordinateSystem());
//				if (flag == Layer.GRAPHICS_SWAPPED) //TODO:add support for graphics change.
//					annotation().save(iconLayer.getGraphics().toArray(new GraphicItem[iconLayer.getGraphics().size()]));
			}
		}
	}
	
	/*-------------------------------------------------------------------------
	 *                          Class related
	 ------------------------------------------------------------------------*/
	/**
	 * Thread safe method that adds a new component to this class.
	 */
	public InstComponentDecl InstClassDecl.syncAddComponent(String className, String componentName, Placement placement) {
		synchronized (state()) {
			ComponentDecl componentDecl = new ComponentDecl();
			componentDecl.setClassName(Access.fromClassName(className));
			componentDecl.setVisibilityType(new PublicVisibilityType());
			componentDecl.setName(new IdDecl(componentName));
			componentDecl.setComment(new Comment());
			componentDecl.setCCComment(new Comment());
			componentDecl.annotation().savePlacement(placement);
			
			FullClassDecl classDecl = (FullClassDecl)getClassDecl();
			classDecl.addNewComponentDecl(componentDecl);
			InstComponentDecl icd = (InstComponentDecl)createInstComponentDecl(componentDecl);
			addInstComponentDecl(icd);
			return icd;
		}
	}
	
	/**
	 * Thread safe method that removes a component from this class.
	 */
	public void InstClassDecl.syncRemoveComponent(InstComponentDecl icd) {
		synchronized (state()) {
			removeInstComponentDecl(icd);
			((FullClassDecl)getClassDecl()).removeComponentDecl(icd.getComponentDecl());
		}
	}
	
	/**
	 * Thread safe method that adds a new connection to this class.
	 */
	public ConnectClause InstClassDecl.syncAddConnection(String sourceID, String targetID) {
		synchronized (state()) {
			ConnectClause connectClause = new ConnectClause();
			connectClause.setComment(new Comment());
			connectClause.setConnector1(Access.fromClassName(sourceID));
			connectClause.setConnector2(Access.fromClassName(targetID));
			syncAddConnection(connectClause);
			return connectClause;
		}
	}
	
	/**
	 * Thread safe method that adds an existing connection to this class.
	 * TODO:Propper Javadoc
	 */
	public void InstClassDecl.syncAddConnection(ConnectClause connectClause) {
		synchronized (state()) {
			((FullClassDecl)getClassDecl()).addNewEquation(connectClause);
			FConnectClause fcc = (FConnectClause)connectClause.instantiate();
			addFAbstractEquation(fcc);
		}
	}
	
	/**
	 * Thread safe method that removes a connection from this class.
	 * TODO:Propper Javadoc
	 */
	public boolean InstClassDecl.syncRemoveConnection(FConnectClause fcc) {
		synchronized (state()) {
			if (!removeFAbstractEquation(fcc))
				return false;
			((FullClassDecl) getClassDecl()).removeEquation(fcc.getConnectClause());
			return true;
		}
	}
	
	
	public void InstClassDecl.syncSetParameterValue(Stack<String> path, String value) {
		synchronized (state()) {
			setParameterValue(path, value);
		}
	}
	
	protected void InstClassDecl.setParameterValue(Stack<String> path, String value) {
		String componentName = path.peek();
		for (InstComponentDecl component : getInstComponentDecls()) {
			if (componentName.equals(component.name())) {
				path.pop();
				component.getComponentDecl().setParameterValue(path, value);
				return;
			}
		}
		
		for (InstExtends ie : getInstExtendss()) {
			if (ie.containsComponent(componentName)) {
				ie.getExtendsClause().setParameterValue(path, value);
				return;
			}
		}
	}
	
	protected boolean InstNode.containsComponent(String componentName) {
		for (InstComponentDecl icd : getInstComponentDecls()) {
			if (componentName.equals(icd.name()))
				return true;
		}
		for (InstExtends ie : getInstExtendss()) {
			boolean val = containsComponent(componentName);
			if (val)
				return true;
		}
		return false;
	}


    protected void Element.setParameterValue(Stack<String> path, String value) {
        StringBuilder sb = new StringBuilder(path.pop());
        for (String part : path) {
            sb.append('/');
            sb.append(part);
        }
        try {
            AnnotationNode node = modificationAnnotation();
            Exp e = ParserHandler.parseExpString(value);
            node.forPath(sb.toString()).setValue(e);
        } catch (ParserException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (beaver.Parser.Exception e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	/*-------------------------------------------------------------------------
	 *                          Misc Sync methods
	 ------------------------------------------------------------------------*/
	
	/**
	 * Thread safe wrapper method for InstComponentDecl.isConnector()
	 */
	public boolean InstComponentDecl.syncIsConnector() {
		synchronized (state()) {
			return isConnector();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstComponentDecl.qualifiedName()
	 */
	public String InstComponentDecl.syncQualifiedName() {
		synchronized (state()) {
			return qualifiedName();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstClassDecl.qualifiedName()
	 */
	public String InstClassDecl.syncQualifiedName() {
		synchronized (state()) {
			return qualifiedName();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstClassDecl.getClassIconName()
	 */
	public String InstClassDecl.syncGetClassIconName() {
		synchronized (state()) {
			return getClassIconName();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.name()
	 */
	public String InstNode.syncName() {
		synchronized (state()) {
			return name();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.getInstExtendss()
	 */
	public List<InstExtends> InstNode.syncGetInstExtendss() {
		synchronized (state()) {
			return getInstExtendss();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.getInstComponentDecls()
	 */
	public List<InstComponentDecl> InstNode.syncGetInstComponentDecls() {
		synchronized (state()) {
			return getInstComponentDecls();
		}
	}
	
	/**
	 * Thread safe wrapper method for BaseNode.iconAnnotation()
	 */
	public AnnotationNode BaseNode.syncIconAnnotation() {
		synchronized (state()) {
			return iconAnnotation();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.myInstClass()
	 */
	public InstClassDecl InstNode.syncMyInstClass() {
		synchronized (state()) {
			return myInstClass();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.getFAbstractEquations()
	 */
	public List<FAbstractEquation> InstNode.syncGetFAbstractEquations() {
		synchronized (state()) {
			return getFAbstractEquations();
		}
	}
	
	/**
	 * Thread safe wrapper method for FConnectClause.getConnectClause()
	 */
	public ConnectClause FConnectClause.syncGetConnectClause() {
		synchronized (state()) {
			return getConnectClause();
		}
	}
	
	/**
	 * Thread safe wrapper method for FConnectClause.getConnector1()
	 */
	public FIdUseInstAccess FConnectClause.syncGetConnector1() {
		synchronized (state()) {
			return getConnector1();
		}
	}
	
	/**
	 * Thread safe wrapper method for FConnectClause.getConnector2()
	 */
	public FIdUseInstAccess FConnectClause.syncGetConnector2() {
		synchronized (state()) {
			return getConnector2();
		}
	}
	
	/**
	 * Thread safe wrapper method for FIdUseInstAccess.getInstAccess()
	 */
	public InstAccess FIdUseInstAccess.syncGetInstAccess() {
		synchronized (state()) {
			return getInstAccess();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstAccess.myInstComponentDecl()
	 */
	public InstComponentDecl InstAccess.syncMyInstComponentDecl() {
		synchronized (state()) {
			return myInstComponentDecl();
		}
	}
	
	/**
	 * Thread safe wrapper method for InstNode.simpleLookupInstComponentDecl
	 */
	public InstComponentDecl InstNode.syncSimpleLookupInstComponentDecl(String name) {
		synchronized (state()) {
			return simpleLookupInstComponentDecl(name);
		}
	}
	
	/**
	 * Thread safe wrapper method for InstProgramRoot.simpleLookupInstClassDecl
	 */
	public InstClassDecl InstProgramRoot.syncSimpleLookupInstClassDecl(String name) {
		synchronized (state()) {
			return simpleLookupInstClassDecl(name);
		}
	}
	
	/**
	 * Thread safe wrapper method for InstComponentDecl.getComponentDecl()
	 */
	public ComponentDecl InstComponentDecl.syncGetComponentDecl() {
		synchronized (state()) {
			return getComponentDecl();
		}
	}

	/**
	 * Thread safe wrapper method for InstComponentDecl.isPrimitive()
	 */
	public boolean InstComponentDecl.syncIsPrimitive() {
		synchronized (state()) {
			return isPrimitive();
		}
	}

	/**
	 * Thread safe wrapper method for InstComponentDecl.isParameter()
	 */
	public boolean InstComponentDecl.syncIsParameter() {
		synchronized (state()) {
			return isParameter();
		}
	}

}