/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.LinkedHashSet;
//import org.jmodelica.modelica.compiler.AliasManager.AliasVariable;

/**
 * Computation of alias sets.
 */
aspect AliasVariables {
	
	/**
	 * \brief Check if an FVariable is an alias.
	 * 
	 * Returns true if alias, else false.
	 * 
	 * @return True if alias, else false.
	 */
	syn lazy boolean FAbstractVariable.isAlias() = false; 
	eq FVariable.isAlias() = aliasVariable() != null; 
	
	/**
	 * \brief Get the alias variable of an FVariable. 
	 * 
	 * If the FVariable is not an alias, null is returned.
	 * 
	 * @return An AliasVariable object if the variable is an alias, otherwise
	 * null.
	 */
	syn lazy AliasManager.AliasVariable FVariable.aliasVariable() {
		AliasManager aliasManager = myFClass().getAliasManager();
		AliasManager.AliasSet aliasSet = aliasManager.getAliasSet(this);
		if (aliasSet == null || inRecord()) {
			return null;
		}
		AliasManager.AliasVariable av = aliasSet.getIterationVariable();
		return av.getFVariable()==this? null : av;
	}
	
	/**
	 * \brief Get the iteration variable corresponding to the alias.
	 * 
	 * If the FVariable is an alias, the alias() return corresponding iteration
	 * FVariable, else null.
	 * 
	 * @return The corresponding iteration variable if alias, otherwise null.
	 */
	syn FVariable FAbstractVariable.alias() = null;
	
	eq FVariable.alias() {
		if (aliasVariable() == null) {
			return null;
		} else {
			return aliasVariable().getFVariable();			
		}
	}

	/**
	 * \brief Returns true if the alias is negated otherwise false.
	 * 
	 * @return True if negated alias, otherwise false.
	 */
	syn boolean FAbstractVariable.isNegated() = false;
	
	eq FVariable.isNegated(){
		AliasManager aliasManager = myFClass().getAliasManager();
		AliasManager.AliasVariable alias = aliasManager.getAliasVariable(this);
		AliasManager.AliasSet aliasSet = alias.getSet();
		if (alias == null) {
			return false;
		} else {
			AliasManager.AliasVariable iav = aliasSet.getIterationVariable();
			return (alias.isNegated() != iav.isNegated());
		}
	}
		
	/**
	 * \brief An FClass has an alias manager.
	 */
	private AliasManager FClass.aliasManager = new AliasManager();

	/**
	 * \brief Get the alias manager.
	 * 
	 * @return the alias manager.
	 */
	public AliasManager FClass.getAliasManager() {
		return aliasManager;
	}
	
	/**
	 * \brief AliasManager contains the connection sets of an FClass.
	 * 
	 * The alias sets are build by adding pairs of FVariables, in addition
	 * to information about whether the alias pair is negated.
	 */
	public class AliasManager {
	
		// Alias error message.
		private String aliasErrorMessage = "";

		// Indicate alias error
		private boolean aliasErrorFlag = false;
		
		// A set of alias sets
		private Set<AliasSet> aliasSets = new LinkedHashSet<AliasSet>();
		
		// A map that maps FVariable to its alias set.
		private Map<FVariable, AliasVariable> variableMap = new HashMap<FVariable, AliasVariable>();
		
		/**
		 * \brief Getter for alias error message.
		 * 
		 * @return The error message.
		 */
		public String getAliasErrorMessage() {
			return aliasErrorMessage;
		}
		
		/**
		 * \brief Check if there was an alias error.
		 * 
		 * The error message can be retrieved by calling the method
		 * getAliasManagerError.
		 * 
		 * @return True if there was an alias error, otherwise false.
		 */
		public boolean aliasError() {
			return aliasErrorFlag;
		}
		
		/**
		 * \brief Get the set of alias sets.
		 * 
		 * @return A set containing the alias sets.
		 */
		public Set<AliasSet> getAliasSets() {
			return aliasSets;
		}
		
		/**
		 * \brief Get the AliasVariable object corresponding to an FVariable
		 * 
		 * @param fv An FVariable object
		 * @return The alias variable that corresponds to an FVariable
		 */
		public AliasVariable getAliasVariable(FVariable fv) {
			return variableMap.get(fv);
		}
		

		/**
		 * \brief Retrieve the alias set corresponding to an FVariable.
		 * 
		 * Returns null if the FVariable is not present in an alias set.
		 * 
		 * @param alias An FVariable.
		 * @return The alias set corresponding to the alias variable.
		 */
		public AliasSet getAliasSet(FVariable fv) {
			AliasVariable alias = variableMap.get(fv);
			if (alias == null)
				return null;
			return alias.getSet();
		}

		/**
		 * \brief Add a pair of alias variables to the alias manager.
		 * 
		 * @param fvA An FVariable.
		 * @param fvB Another FVariable.
		 * @param neg True if the alias pair is negated, otherwise false.
		 * 
		 */
		public void addAliasVariables(FVariable fvA, FVariable fvB, boolean neg) {
			/*
			 * Rules for alias sets:
			 * 
			 *  1. An alias variable can only occur in one alias set
			 *  2. An alias variable can occur either negated or
			 *     non-negated in an alias set, not both.
			 *  
			 *  These rules are enforced by the algorithm below: if a
			 *  rule is about to be broken an error message is generated.
			 *  			
			 *  Notice that LinkedHashSets are expected to be persistent, 
			 *  therfore alias sets need to be temporarily removed from the
			 *  set, then modified, then reinserted.
			 */
			
			AliasVariable aliasA = getAliasVariable(fvA);
			AliasVariable aliasB = getAliasVariable(fvB);
			
			if (aliasA != null && aliasB != null) {
				merge(aliasA, aliasB, neg);
			} else if (aliasA != null) {
				add(aliasA.getSet(), fvB, aliasA.isNegated() ? !neg : neg);
			} else if (aliasB != null) {
				add(aliasB.getSet(), fvA, aliasB.isNegated() ? !neg : neg);
			} else {
				join(fvA, fvB, neg);
			}
		}
		
		/**
		 * Merges two alias variables <code>aliasA</code> and <code>aliasB</code>.
		 * The implementation also check the rules for a alias set is maintained.
		 * 
		 * @param aliasA First alias variable
		 * @param aliasB Second alias variable
		 * @param neg True if the alias pair is negated, otherwise false.
		 */
		private void merge(AliasVariable aliasA, AliasVariable aliasB, boolean neg) {
			AliasSet mergeSet = aliasB.getSet();
			AliasSet resultSet = aliasA.getSet();
			if (resultSet == mergeSet) {
				if (aliasA.isNegated() == aliasB.isNegated() && neg) {
					aliasErrorFlag = true;
					aliasErrorMessage = "Alias error: trying to add the " +
					"negated alias pair (" + aliasA.getFVariable().name() + ",-" + 
					aliasB.getFVariable().name() + ") to the alias set " + resultSet;
				} else if (aliasA.isNegated() != aliasB.isNegated() && !neg) {
					aliasErrorFlag = true;
					aliasErrorMessage = "Alias error: trying to add the " +
					"alias pair (" + aliasA.getFVariable().name() + "," + 
					aliasB.getFVariable().name() + ") to the alias set " + resultSet;
				}
				return;
			}
			boolean shouldNegate = aliasA.isNegated() == aliasB.isNegated() ? neg : !neg;
			for (AliasVariable a : mergeSet) {
				if (shouldNegate)
					a.negated = !a.negated;
				resultSet.add(a);
			}
			aliasSets.remove(mergeSet);
		}
		
		/**
		 * Add a new variable to an exisiting alias set.
		 * 
		 * @param set The existing alias set
		 * @param fv The new alias variable
		 * @param neg True if the alias variable is negated, otherwise false.
		 */
		private void add(AliasSet set, FVariable fv, boolean neg) {
			set.add(new AliasVariable(fv, neg));
		}
		
		/**
		 * Creates a new alias set based on the two variables.
		 * 
		 * @param fvA First variable
		 * @param fvB First variable
		 * @param neg True if the alias pair is negated, otherwise false.
		 */
		private void join(FVariable fvA, FVariable fvB, boolean negate) {
			AliasSet set = new AliasSet();
			aliasSets.add(set);
			set.add(new AliasVariable(fvA, false));
			set.add(new AliasVariable(fvB, negate));
		}
		
		/**
		 * \brief Print the alias sets.
		 * 
		 * @return A string containing the alias sets.
		 */
		public String printAliasSets() {
			StringBuilder str = new StringBuilder();
			for (AliasSet aliasSet : aliasSets) {
				str.append(aliasSet +"\n");
			}
			return str.toString();		
		}
		
		/**
		 * A class representing a set of alias variables.
		 */
		public class AliasSet implements Iterable<AliasVariable> {
			
			// List of aliases
			private java.util.List<AliasVariable> aliases = new ArrayList<AliasVariable>();
			
			// If this set is marked as manual tearing
			private boolean isManualIteration = false;
			
			/**
			 * Private constructor
			 */
			private AliasSet() {
			}
			
			/**
			 * An iterator over all the alias variables in this set.
			 * 
			 * @return An iterator for this set
			 */
			@Override
			public Iterator<AliasVariable> iterator() {
				return aliases.iterator();
			}
			
			/**
			 * Adds a new alias variable to this set.
			 * Manual tearing is also propagated if necessary.
			 * 
			 * @param newAlias Alias variable to add
			 */
			private void add(AliasVariable newAlias) {
				boolean newAliasIsManualIteration = newAlias.getFVariable().getHGTType() == FClass.HGTVariableType.COMPONENT;
				if (isManualIteration && !newAliasIsManualIteration) {
					newAlias.getFVariable().setHGTType(FClass.HGTVariableType.COMPONENT);
				} else if (!isManualIteration && newAliasIsManualIteration) {
					isManualIteration = true;
					for (AliasVariable alias : aliases)
						alias.getFVariable().setHGTType(FClass.HGTVariableType.COMPONENT);
				}
				newAlias.setSet(this);
				aliases.add(newAlias);
			}
			
			/**
			 * Get the iteration variable of this alias set.
			 * 
			 * @return The iteration variable of this alias set.
			 */
			public AliasVariable getIterationVariable() {
				// Find the variable with highest heuristic score
				AliasVariable best = null;
				int max = 0;
				for (AliasVariable av : aliases) {
					int score = av.getFVariable().aliasHeuristicScore();
					if (score > max) {
						max = score;
						best = av;
					}
				}
				return best;
			}
			
			/**
			 * Method for retreiving the number of aliases in this set.
			 * 
			 * @return The number of aliases in this set
			 */
			public int numAliases() {
				return aliases.size();
			}
			
			@Override
			public String toString() {
				StringBuilder str = new StringBuilder();
				str.append("{");
				
				AliasVariable iterationAlias = getIterationVariable();
				str.append(iterationAlias);
				
				for (AliasVariable alias : aliases) {
					if (alias == iterationAlias)
						continue;
					str.append(',');
					str.append(alias);
				}
				str.append("}");
				return str.toString();
			}
			
			/**
			 * A more verbose version of toString().
			 * 
			 * @return More info about this set
			 */
			public String toStringVerbose() {
				StringBuilder str = new StringBuilder();
				str.append("Alias set: (" + getIterationVariable().getFVariable().name() + ")\n");
				for (AliasVariable alias : aliases) {
					if (alias.isNegated()) {
						str.append("-");
					}
					str.append(alias.getFVariable().name());
					str.append(" " + alias.getFVariable().aliasHeuristicScore() + " ");
					str.append(" " + alias.getFVariable().varKind() + "\n");
				}
				return str.toString();
			}
			
		}
		
		/**
		 * \brief AliasVariable is used to encapsulate an FVariable and whether
		 * the alias is negated. 
		 * 
		 * AliasVariable implements the Comparable interface and objects of the 
		 * class are used in the alias sets.
		 */
		public class AliasVariable {
			
			// The FVariable
			private FVariable fv;
			// Negated attribute
			private boolean negated = false;
			
			// The set that this alias variable belong to
			private AliasSet set;
			
			/**
			 * \brief Constructor.
			 * 
			 * @param fv An FVariable.
			 * @param negated True if the alias is negated, otherwise false.
			 */
			private AliasVariable(FVariable fv,boolean negated) {
				this.fv = fv;
				this.negated = negated;
				variableMap.put(fv, this);
			}
		
			/**
			 * \brief Returns true if the alias is negated.
			 * 
			 * @return True if the alias is negated, otherwise false.
			 */
			public boolean isNegated() {
				return negated;
			}
		
			/**
			 * \brief Getter for the FVariable.
			 * 
			 * @return The FVariable.
			 */
			public FVariable getFVariable() {
				return fv;
			}	
			
			/**
			 * Returns the alias set that this alias variable belong to.
			 * 
			 * @return Alias set that this alias belong to
			 */
			public AliasSet getSet() {
				return set;
			}
			
			/**
			 * Private method for changing the set that this alias belong to.
			 * This method should only be used when the alias initially is added
			 * of when two alias sets are merged.
			 * 
			 * @param set The new set that this alias should belong to
			 */
			private void setSet(AliasSet set) {
				this.set = set;
			}
			
			@Override
			public String toString() {
				if (isNegated())
					return '-' + getFVariable().name();
				else
					return getFVariable().name();
			}
			
		}
	
	}
	
	/**
	 * \brief Get the heuristic score for keeping this variable in an alias set.
	 * 
	 * The variable with the highest score should be kept. Score is always > 0.
	 */
	syn int FVariable.aliasHeuristicScore() {
		int score = 1000;
		
		// Temporary variables added during transformations should get lower priority
		if (isTemporary())
			score -= 100;
		
		// 0. Derivatives
		if (isDerivativeVariable())
			return score;
		score--;
		
		// 1. Input
		if (isInput())
			return score;
		score--;
		
		// 2. parameter
		if (isParameter())
			return score;
		score--;
		
		// 3. non-parameter with fixed attribute set to true
		if (!isParameter() && fixedAttributeSet() && fixedAttributeExp().ceval().booleanValue())
			return score;
		score--;
				
		// 4. Variables with StateSelect-information
		if (isReal() && ((FRealVariable)this).stateSelectAttributeSet()) 
			return score;
		score--;
		
		if (getHGTType() == FClass.HGTVariableType.PAIR || getHGTType() == FClass.HGTVariableType.COMPONENT)
			return score;
		score--;
		
		// 5. start attribute set
		if (startAttributeSet())
			return score;
		score--;
		
		return score;
	}

}