<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Adolc Speed: Second Derivative of a Polynomial</title>
<meta name="description" id="description" content="Adolc Speed: Second Derivative of a Polynomial"/>
<meta name="keywords" id="keywords" content=" adolc speed polynomial link_poly "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_adolc_poly.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="adolc_ode.cpp.xml" target="_top">Prev</a>
</td><td><a href="adolc_sparse_hessian.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_adolc</option>
<option>adolc_poly.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed_adolc-&gt;</option>
<option>adolc_det_minor.cpp</option>
<option>adolc_det_lu.cpp</option>
<option>adolc_ode.cpp</option>
<option>adolc_poly.cpp</option>
<option>adolc_sparse_hessian.cpp</option>
<option>adolc_sparse_jacobian.cpp</option>
</select>
</td>
<td>adolc_poly.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>link_poly</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Adolc Speed: Second Derivative of a Polynomial</big></big></b></center>
<br/>
<b><big><a name="link_poly" id="link_poly">link_poly</a></big></b>


<code><font color='blue'><pre style='display:inline'> 
# include &lt;vector&gt;

# include &lt;cppad/speed/uniform_01.hpp&gt;
# include &lt;cppad/poly.hpp&gt;
# include &lt;cppad/vector.hpp&gt;

# include &lt;adolc/adouble.h&gt;
# include &lt;adolc/taping.h&gt;
# include &lt;adolc/interfaces.h&gt;

bool link_poly(
	size_t                     size     , 
	size_t                     repeat   , 
	CppAD::vector&lt;double&gt;     &amp;a        ,  // coefficients of polynomial
	CppAD::vector&lt;double&gt;     &amp;z        ,  // polynomial argument value
	CppAD::vector&lt;double&gt;     &amp;ddp      )  // second derivative w.r.t z  
{
	// -----------------------------------------------------
	// setup
	int tag  = 0;  // tape identifier
	int keep = 1;  // keep forward mode results in buffer
	int m    = 1;  // number of dependent variables
	int n    = 1;  // number of independent variables
	int d    = 2;  // order of the derivative
	double f;      // function value
	int i;         // temporary index

	// choose a vector of polynomial coefficients
	CppAD::uniform_01(size, a);

	// AD copy of the polynomial coefficients
	std::vector&lt;adouble&gt; A(size);
	for(i = 0; i &lt; int(size); i++)
		A[i] = a[i];

	// domain and range space AD values
	adouble Z, P;

	// allocate arguments to hos_forward
	double *x0 = 0;
	x0         = CPPAD_TRACK_NEW_VEC(n, x0);
	double *y0 = 0;
	y0         = CPPAD_TRACK_NEW_VEC(m, y0);
	double **x = 0;
	x          = CPPAD_TRACK_NEW_VEC(n, x);
	double **y = 0;
	y          = CPPAD_TRACK_NEW_VEC(m, y);
	for(i = 0; i &lt; n; i++)
	{	x[i] = 0;
		x[i] = CPPAD_TRACK_NEW_VEC(d, x[i]);
	}
	for(i = 0; i &lt; m; i++)
	{	y[i] = 0;
		y[i] = CPPAD_TRACK_NEW_VEC(d, y[i]);
	}
	// Taylor coefficient for argument
	x[0][0] = 1.;  // first order
	x[0][1] = 0.;  // second order
	

	extern bool global_retape;
	if( global_retape ) while(repeat--)
	{	// choose an argument value
		CppAD::uniform_01(1, z);

		// declare independent variables
		trace_on(tag, keep);
		Z &lt;&lt;= z[0]; 

		// AD computation of the function value 
		P = CppAD::Poly(0, A, Z);

		// create function object f : Z -&gt; P
		P &gt;&gt;= f;
		trace_off();

		// get the next argument value
		CppAD::uniform_01(1, z);
		x0[0] = z[0];

		// evaluate the polynomial at the new argument value
		hos_forward(tag, m, n, d, keep, x0, x, y0, y);

		// second derivative is twice second order Taylor coef
		ddp[0] = 2. * y[0][1];
	}
	else
	{
		// choose an argument value
		CppAD::uniform_01(1, z);

		// declare independent variables
		trace_on(tag, keep);
		Z &lt;&lt;= z[0]; 

		// AD computation of the function value 
		P = CppAD::Poly(0, A, Z);

		// create function object f : Z -&gt; P
		P &gt;&gt;= f;
		trace_off();
		while(repeat--)
		{	// get the next argument value
			CppAD::uniform_01(1, z);
			x0[0] = z[0];

			// evaluate the polynomial at the new argument value
			hos_forward(tag, m, n, d, keep, x0, x, y0, y);

			// second derivative is twice second order Taylor coef
			ddp[0] = 2. * y[0][1];
		}
	}
	// ------------------------------------------------------
	// tear down
	CPPAD_TRACK_DEL_VEC(x0);
	CPPAD_TRACK_DEL_VEC(y0);
	for(i = 0; i &lt; n; i++)
		CPPAD_TRACK_DEL_VEC(x[i]);
	for(i = 0; i &lt; m; i++)
		CPPAD_TRACK_DEL_VEC(y[i]);
	CPPAD_TRACK_DEL_VEC(x);
	CPPAD_TRACK_DEL_VEC(y);

	return true;
}
</pre></font></code>


<hr/>Input File: speed/adolc/poly.cpp

</body>
</html>
