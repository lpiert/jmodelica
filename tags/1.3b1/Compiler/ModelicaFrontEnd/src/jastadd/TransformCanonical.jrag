/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.HashSet;
import java.util.Collections;

/* This causes conflicts
import org.jmodelica.graphs.EquationSystem;
import org.jmodelica.graphs.Equation;
import org.jmodelica.graphs.Variable;
*/

/**
 * \brief Contains transformations on the flattened model which converts
 * it into a canonical form.
 */
aspect TransformCanonical {

	/**
	 * \brief Transform the raw flattened model into a structured form.
	 * 
	 * Currently, the following operations are performed:
	 *  <ul>
	 *    <li> Binding equations for variables are converted into regular
	 *         equations by calling genBindingEquations().
	 *    <li> For each non-function algorithm:
	 *         <ul>
	 *           <li> A function is generated with the algorithm as body, 
	 *                any assigned variables as outputs and any used 
	 *                variables as inputs.
	 *           <li> The original algorithm is replaced with a call to 
	 *                the generated function.
	 *         </ul>
	 *    <li> The model is scalarized, i.e., all array declarations and
	 *         equations are replaced by scalar declarations and equations
	 *    <li> Alias variables are eliminated from the model.
	 *    <li> Derivative variables are generated and inserted in the
	 *         list of FVariables by calling addFDerivativeVariables().
	 *    <li> Generate initial equations based on start attribute.
	 *    <li> Sort dependent parameters.
	 *  </ul>
	 */
	public void FClass.transformCanonical() {
		genBindingEquations();
		genAlgorithmFunctions();
		scalarize();
		
		// This must be done after scalarization
		enableStreamsRewrite();
		root().flushAllRecursive();
		
		if (root().options.getBooleanOption("eliminate_alias_variables")) {
			genAlias();
			eliminateAliasVariables();
		}
//		log.debug(prettyPrint(""));
		addFDerivativeVariables();
		genInitialEquations();
		sortDependentParameters();

		if (root().options.getBooleanOption("index_reduction")) {
			indexReduction();
		}

		if (root().options.getBooleanOption("equation_sorting")) {
			indexReduction();
			blt();
		}
		
        root().flushAllRecursive();
		
	}
		
	
	private org.jmodelica.graphs.EquationSystem FClass.eqSys = null;
	
	public void FClass.indexReduction() {
		
		eqSys = new org.jmodelica.graphs.EquationSystem(name());
		org.jmodelica.graphs.Equation eqn;
		org.jmodelica.graphs.Variable v = null;
		
		int k = 1;
		for (FAbstractEquation e : equations()) {
			eqn = eqSys.addEquation("eq_"+k);
			k++;
			
			for (FVariable y : e.algebraicVariables()) { 
				log.debug("  Algebraic: " + y.name()); 
					 	 
				v = eqSys.addVariable(y.name());     
				eqn.addVariable(v); 
			} 
					 
			for (FVariable x : e.differentiatedVariables()) { 
				log.debug("  State: " + x.name()); 
					 	 
				v = eqSys.addVariable(x.name());     
				eqn.addVariable(v); 
			} 
				 	 
			for (FDerivativeVariable dx : e.derivativeVariables()) { 
				FVariable x = dx.myDifferentiatedVariable(); 
				log.debug("  Diffed state: " + x.name()); 
 
				v = eqSys.addVariable(x.name(), 1);     
				eqn.addVariable(v); 
			} 
			
		}
		
		try {
			int index = eqSys.pantelides();
		} catch (org.jmodelica.graphs.EquationSystem.PantelidesMaxDepthException e) {
            System.out.println("Pantelides reached max depth");
        } catch (org.jmodelica.graphs.EquationSystem.PantelidesEmptyEquationException e) {
            System.out.println("Pantelides encountered empty equation");
        }
        
        eqSys.dumpGraph();
			
	}

	public void FClass.blt() {
		
		LinkedList<Stack<org.jmodelica.graphs.Equation>> blt_result = eqSys.blt();

		StringBuffer str = new StringBuffer();
		str.append("BLT:\n");
		LinkedList<Stack<org.jmodelica.graphs.Equation>> blt = eqSys.blt();
		int i = 0;
		for (Stack<org.jmodelica.graphs.Equation> s : blt_result) {
			str.append("Block "+i+":\n");
			for (org.jmodelica.graphs.Equation e : s) {
				str.append(e + " (" + e.getMatch()+")\n");
			}
			i++;
		}
		System.out.println(str.toString());
	}
	
	/**
	 * \brief Convert variable binding equations into regular equations.
	 */
	public void FClass.genBindingEquations() {
		for (FVariable fv : getFVariables()) {
			if (!fv.isIndependentParameter() && !fv.isConstant()) {
				boolean depPar = fv.isDependentParameter();
				FQName var_name = fv.getFQName().fullCopy();
				// Do not copy array indices
				var_name.setLastFArraySubscripts(null);
				if (fv.hasBindingExp()) {
					FExp bexp = fv.getBindingExp();
					fv.setBindingExpOpt(new Opt());
					FEquation feq = new FEquation(new FIdUseExp(var_name), bexp);
					if (depPar)
						addFParameterEquation(feq);
					else
						addFEquation(feq);
				} else if (fv.isRecord()) {
					FRecordDecl rec = fv.myFRecordDecl();
					for (FAttribute a : fv.getFAttributes()) 
						a.genBindingEquations(this, rec, var_name, depPar);
				}
			}
		}
//		flush();
		flushAllRecursive();
	}
	
	/**
	 * \brief Generate equations from an attribute of a record variable.
	 */
	public void FAttribute.genBindingEquations(FClass fc, FRecordDecl rec, FQName prefix, boolean depPar) {
		AbstractFVariable comp = rec.findComponent(getName().getFQName());
		if (hasValue() && !comp.isUnknown()) {
			// TODO: handle 'each'
			FQName fqn = prefix.copyAndAppend(getName().getFQName());
			FEquation feq = new FEquation(new FIdUseExp(fqn), getValue());
			if (depPar)
				fc.addFParameterEquation(feq);
			else
				fc.addFEquation(feq);
			setValueOpt(new Opt());
			if (comp.isRecord()) {
				FRecordDecl next = comp.myFRecordDecl();
				for (FAttribute a : getFAttributes())
					a.genBindingEquations(fc, next, fqn, depPar);
			}
		}
	}
	
	/**
	 * \brief Generate functions from algorithms.
	 */
	public void FClass.genAlgorithmFunctions() {
		for (FAlgorithmBlock alg : myAlgorithms()) {
			// Create function declaration
			List<FFunctionVariable> vars = new List<FFunctionVariable>();
			FQName funcName = new FQName(alg.generateFunctionName());
			
			// - Find inputs
			ArrayList<FVariable> inVars = new ArrayList<FVariable>();
			ArrayList<FFunctionVariable> bothInVars = new ArrayList<FFunctionVariable>();
			for (FIdUse use : alg.usedFIdUses()) {
				AbstractFVariable fv = use.myFV();
				if (!fv.isUnknown() && !inVars.contains(fv) && !fv.isForIndex()) {
					inVars.add((FVariable) fv);
					bothInVars.add(null);
				}
			}
			
			// - Create outputs, add to vars and locate those that are inputs as well
			ArrayList<FVariable> outVars = new ArrayList<FVariable>();
			for (FIdUse use : alg.assignedFIdUses()) {
				if (!use.myFV().isUnknown() && !outVars.contains(use.myFV())) {
					FVariable fv = (FVariable) use.myFV();
					FFunctionVariable ffv = fv.createFFunctionOutput();
					vars.add(ffv);
					outVars.add(fv);
					int i = inVars.indexOf(fv);
					if (i >= 0) 
						bothInVars.set(i, ffv);
				}
			}
			
			// - Create inputs, add to vars and update inputs and outputs that are the same
			ArrayList<FVariable> allVars = new ArrayList<FVariable>();
			allVars.addAll(inVars);
			allVars.addAll(outVars);
			
			for (int i = 0; i < inVars.size(); i++) {
				FVariable fv = inVars.get(i);
				FFunctionVariable ffv = fv.createFFunctionInput();
				vars.add(ffv);
				if (bothInVars.get(i) != null) {
					
					// Generate a new (unique) name for variable
					boolean nameExists = true;
					String name = null;
					for (int j = 0; nameExists; j++) {
						name = fv.name() + "_" + j;
						nameExists = false;
						for (FVariable fv2 : allVars)
							if (fv2.name().equals(name))
								nameExists = true;
					}
					
					// Rename input
					FQName fqn = new FQName(name);
					ffv.setFQName(fqn);
					
					// Create and add binding expression for output
					bothInVars.get(i).setBindingExp(fqn.createFIdUseExp());
				}
			}
			
			// Create function call equation and replace algorithm
			List<FExp> args = new List<FExp>();
			for (int i = 0; i < inVars.size(); i++) 
				args.add(inVars.get(i).createAlgorithmArgument(bothInVars.get(i) != null));
			Size[] sizes = new Size[outVars.size()];
			for (int i = 0; i < outVars.size(); i++)
				sizes[i] = outVars.get(i).size();
			FFunctionCall call = new FFunctionCall(funcName.createFIdUse(), args, sizes);
			List<FFunctionCallLeft> lefts = new List<FFunctionCallLeft>();
			for (FVariable fv : outVars)
				lefts.add(fv.getFQName().createFFunctionCallLeft());
			replaceEquation(alg, new FFunctionCallEquation(lefts, call));
			
			// Add return statement, create function declaration and add it to FClass 
			alg.addFStatement(new FReturnStmt());
			addFFunctionDecl(new FFunctionDecl(funcName, vars, alg));
		}
	}
	
	/**
	 * \brief Replace one equation with another.
	 */
	public void FClass.replaceEquation(FAbstractEquation oldEq, FAbstractEquation newEq) {
		for (FEquationBlock b : getFEquationBlocks())
			if (b.getFAbstractEquations().replaceChild(oldEq, newEq))
				return;
	}
	
	/**
	 * \brief Create an FFunctionCallLeft using a copy of this name.
	 */
	public FFunctionCallLeft FQName.createFFunctionCallLeft() {
		FQName fqn = fullCopy();
		fqn.setLastFArraySubscripts(null);
		return new FFunctionCallLeft(new Opt(new FIdUseExp(fqn)));
	}

	/**
	 * \brief Create an expression to serve as argument to a generated algorithm function.
	 * 
	 * @param init  <code>true</code> if the argument is to initialize an assigned variable
	 */
	public FExp FVariable.createAlgorithmArgument(boolean init) {
		if (init) {
			if (isDiscrete())
				return null; // TODO: Return pre() value.
			if (isContinuous())
				return startAttributeExp();
		}
		return getFQName().createFIdUseExp();
	}
	
	/**
	 * \brief Create an input representing this variable in a generated algorithm function.
	 */
	public FFunctionVariable FVariable.createFFunctionOutput() {
		return createFFunctionVariable(new FOutput());
	}

	/**
	 * \brief Create an output representing this variable in a generated algorithm function.
	 */
	public FFunctionVariable FVariable.createFFunctionInput() {
		return createFFunctionVariable(new FInput());
	}

	/**
	 * \brief Create an function variable representing this variable in a generated algorithm function.
	 */
	public FFunctionVariable FVariable.createFFunctionVariable(FTypePrefixInputOutput io) {
		FType type = (FType) type().fullCopy();
		return new FFunctionVariable(new Opt(io), type, new Opt(), getFQName().fullCopy());
	}

	/**
	 * \brief Add derivative variables to the list of FVariables, one for each
	 * differentiate variable.
	 */
	public void FClass.addFDerivativeVariables() {
		ArrayList<FDerivativeVariable> l = new ArrayList<FDerivativeVariable>();
		for (FVariable fv : differentiatedRealVariables()) {
			l.add(new FDerivativeVariable((FVisibilityType)fv.getFVisibilityType().fullCopy(),
					(FTypePrefixVariability)fv.getFTypePrefixVariability().fullCopy(),
					new Opt(),
					new List(),
					new Opt(),
					new Opt(),
					fv.getFQName().fullCopy()));
		}
		for (FVariable fv : l) {
			addFVariable(fv);
		}
//		flush();
				root().flushAllRecursive();
	}

	/**
	 * \brief Generate initial equations from variables with fixed start
	 * attributes.
	 * 
	 * Intitial equations corresponding to explicitly set start attributes of 
	 * differentiated variables are also generated, without taking the fixed
	 * attribute into account.
	 */
	public void FClass.genInitialEquations() {
		// TODO: Isn't there always a value for fv.startAttributeExp() now?
		for (FRealVariable fv : realVariables()) {
			if (fv.fixedAttribute() ||
					(root().options.getBooleanOption("state_start_values_fixed") 
							&&  fv.isDifferentiatedVariable() && fv.startAttributeSet())) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeSet()? fv.startAttributeExp(): new FRealLitExp("0.0")));
			}	
		}
//		log.debug(prettyPrint(""));
		for (FIntegerVariable fv : integerVariables()) {
			if (fv.fixedAttribute()) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeSet()? fv.startAttributeExp(): new FIntegerLitExp("0")));
			}	
		}
		for (FBooleanVariable fv : booleanVariables()) {
			if (fv.fixedAttribute()) {
				addFInitialEquation(new FEquation(new FIdUseExp(
						new FIdUse(fv.getFQName().fullCopy())),
						fv.startAttributeSet()? fv.startAttributeExp(): new FBooleanLitExpFalse()));
			}	
		}		
		//flush();
				root().flushAllRecursive();
	}
	
	ArrayList<String> FClass.aliasErrors = new ArrayList<String>();
	
	
	/**
	 * \brief A helper class containing information about an alias pair.
	 */
	public class AliasPair {
		public FVariable fv1;
		public FVariable fv2;
		public boolean negated;
		public AliasPair(FVariable fv1, FVariable fv2, boolean negated) {
			this.fv1 = fv1;
			this.fv2 = fv2;
			this.negated = negated;
		}
	}
	
	public AliasPair FAbstractEquation.aliasVariables() {
		return null;
	}
	
	/**
	 * \brief Detection of alias variables in equations
	 * 
	 * Equations of the type 'x=y', 'x=-y', '-x=y' and '-x=-y' are detected
	 * as alias equations and an AliasPair object is returned.
	 */
	public AliasPair FEquation.aliasVariables() {

		AbstractFVariable afv1 = null;
		AbstractFVariable afv2 = null;
		
		FVariable fv1 = null;
		FVariable fv2 = null;
		boolean negated = false;
		
		if (getLeft() instanceof FIdUseExp && // x = y
					getRight() instanceof FIdUseExp) {
			afv1 = ((FIdUseExp)getLeft()).myFV();
			afv2 = ((FIdUseExp)getRight()).myFV();
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = false;
			}
		} else if (getLeft() instanceof FNegExp && // -x = y
				((FNegExp)getLeft()).getFExp() instanceof FIdUseExp &&
				getRight() instanceof FIdUseExp) {
			afv1 = ((FIdUseExp)((FNegExp)getLeft()).getFExp()).myFV();
			afv2 = ((FIdUseExp)getRight()).myFV();
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = true;
			}
		} else if (getLeft() instanceof FIdUseExp && // x = -y
					getRight() instanceof FNegExp &&
				((FNegExp)getRight()).getFExp() instanceof FIdUseExp) {
			afv1 = ((FIdUseExp)getLeft()).myFV();
			afv2 = ((FIdUseExp)((FNegExp)getRight()).getFExp()).myFV();	
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = true;
			}
		} else if (getLeft() instanceof FNegExp && // -x = -y
				((FNegExp)getLeft()).getFExp() instanceof FIdUseExp &&
				getRight() instanceof FNegExp &&
				((FNegExp)getRight()).getFExp() instanceof FIdUseExp
				) {
			afv1 = ((FIdUseExp)((FNegExp)getLeft()).getFExp()).myFV();
			afv2 = ((FIdUseExp)((FNegExp)getRight()).getFExp()).myFV();	
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = false;
			}
		} else if (getLeft() instanceof FLitExp && getLeft().ceval().realValue() == 0 && // 0 = x + y
				getRight() instanceof FDotAddExp &&
				((FDotAddExp)getRight()).getLeft() instanceof FIdUseExp &&
				((FDotAddExp)getRight()).getRight() instanceof FIdUseExp) {
			afv1 = ((FIdUseExp)((FDotAddExp)getRight()).getLeft()).myFV();
			afv2 = ((FIdUseExp)((FDotAddExp)getRight()).getRight()).myFV();
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = true;
			}
		} else if ((getLeft() instanceof FDotAddExp &&  //  x + y = 0
				((FDotAddExp)getLeft()).getLeft() instanceof FIdUseExp &&
				((FDotAddExp)getLeft()).getRight() instanceof FIdUseExp) &&
				(getRight() instanceof FLitExp && getRight().ceval().realValue() == 0)) {
			afv1 = ((FIdUseExp)((FDotAddExp)getLeft()).getLeft()).myFV();
			afv2 = ((FIdUseExp)((FDotAddExp)getLeft()).getRight()).myFV();
			if (!afv1.isUnknown() && !afv2.isUnknown()) {
				fv1 = (FVariable)afv1;
				fv2 = (FVariable)afv2;
				negated = true;
			}
		}
		
		if ((fv1!=null) && 
				(fv2!=null)) {
			return new AliasPair(fv1,fv2,negated);
		} else {
			return null;
		}
		
	}
	
	syn lazy boolean FAbstractEquation.isAliasEquation() = false;
	eq FEquation.isAliasEquation() = aliasVariables()!=null;
	
	/**
	 * \brief Generate alias information and remove alias equations.
	 */
	public void FClass.genAlias() {
		aliasManager = new AliasManager();
		
		for (FAbstractEquation equation : 
			getFEquationBlock(0).getFAbstractEquations()) {
			// Iterate over all equations
			// If the equation is an alias equation, then register
			// the alias variables in the alias manager.
//			System.out.println(equation.prettyPrint(" -- "));
			if (equation.isAliasEquation()) {
				AliasPair aliasPair = equation.aliasVariables();
				aliasManager.addAliasVariables(aliasPair.fv1,
						aliasPair.fv2, aliasPair.negated);
//				System.out.println(aliasManager.printAliasSets());
			}			
		}	
		//flush();
		root().flushAllRecursive(); // TODO: is this really needed? the tree isn't changed
	}

	/**
	 * \brief Flag indicating if alias variables have been eliminated from the
	 * model.
	 */
	public boolean FClass.aliasVariablesEliminated = false;
	
	/**
	 * \brief Eliminate alias variables from the flat model.
	 * 
	 * The non-alias variables are kept in the list FClass.FVariableList but the
	 * alias variables are moved to FClass.AliasVariablesList.
	 */
	public void FClass.eliminateAliasVariables() {
		ArrayList<FVariable> nonAliasVars = new ArrayList<FVariable>();
		ArrayList<FVariable> aliasVars    = new ArrayList<FVariable>();
		ArrayList<FAbstractEquation> eqns = new ArrayList<FAbstractEquation>();
		
		//log.debug("FClass.eliminateAliasVariables(): l1 " + nonAliasVars + " l2 " + aliasVars);
		
		// Go through all variables and insert in correct list
		for (FVariable fv : getFVariables()) {
			if (fv.isAlias()) 
				aliasVars.add(fv);
			else 
				nonAliasVars.add(fv);
		}
		
		// Remove alias equations.
		for (FAbstractEquation equation : getFEquationBlock(0).getFAbstractEquations()) 
			if (!equation.isAliasEquation()) 
				eqns.add(equation);

		setFVariableList(new List(nonAliasVars));
		setAliasVariableList(new List(aliasVars));
		getFEquationBlock(0).setFAbstractEquationList(new List(eqns));
		
		aliasVariablesEliminated = true;
		enableAliasRewrite();
		
		flush();
	}
	
	public void ASTNode.enableAliasRewrite() {
		for (ASTNode n : this)
			n.enableAliasRewrite();
	}
	
	public void FIdUseExp.enableAliasRewrite() {
		super.enableAliasRewrite();
		rewriteAlias = true;
		is$Final = false;
	}
	
	boolean FIdUseExp.rewriteAlias = false;
	
	/**
	 * \brief FIdUses referring to alias variables need to be changed to 
	 * FIdUses referring to their alias targets. 
	 * 
	 * This is done by rewrites which are activated once FClass.aliasVariablesEliminated
	 * is true. Notice that in order for the rewrite to be enable, the is$final
	 * field of FIdUseExp needs to be set to false: this is done by the recursive
	 * method ASTNode.flushAllRecursiveClearFinal. 
	 */
	rewrite FIdUseExp {
		when (rewriteAlias && !myFV().isUnknown() && myFV().isAlias()) to FExp {
			FVariable fv = (FVariable) myFV();
			FExp new_exp = fv.alias().getFQName().createFIdUseExp();
			if (fv.isNegated()) 
				new_exp = new FNegExp(new_exp);
			return new_exp;
		}
	}
	
	/**
	 * \brief FDerExps referring to alias variables need to be changed to 
	 * FDerExps referring to their alias targets. 
	 * 
	 * This is done by rewrites which are activated once FClass.aliasVariablesEliminated
	 * is true. Notice that in order for the rewrite to be enable, the is$final
	 * field of FIdUseExp needs to be set to false: this is done by the recursive
	 * method ASTNode.flushAllRecursiveClearFinal. 
	 */
	rewrite FDerExp {
		when (rewriteAlias && !getFIdUse().myFV().isUnknown() && getFIdUse().myFV().isAlias()) to FExp {
			FQName new_name = (FQName)((FVariable)getFIdUse().myFV()).alias().getFQName().fullCopy();
			FExp new_exp = new FDerExp(new FIdUse(new_name));
			if (getFIdUse().myFV().isNegated()) 
				new_exp = new FNegExp(new_exp);
			return new_exp;
		}
	}
}

aspect ParameterSorting {

	/**
	 * \brief Indicate if there exist cyclic parameter dependencies.
	 */
	public boolean FClass.cyclicParameters = false;
	
	/**
	 * \brief Index of equation during equation sorting.
	 */
	public int FAbstractEquation.sortingIndex = -1;
	
	public class FAbstractEquation {
		public static class SortingIndexComparator implements Comparator<FAbstractEquation> {
			public int compare(FAbstractEquation o1, FAbstractEquation o2) {
				return o1.sortingIndex - o2.sortingIndex;
			}
		}
	}
	
	public class FVariable {
		public static class SortingIndexComparator implements Comparator<FVariable> {
			public int compare(FVariable o1, FVariable o2) {
				return o1.parameterEquation().sortingIndex - o2.parameterEquation().sortingIndex;
			}
		}
	}
	
	/**
	 * \brief Creates a TreeSet that can hold FAbstractEquations, sorted by their sortingIndex.
	 */
	public static TreeSet<FAbstractEquation> FAbstractEquation.sortingSet() {
		return new TreeSet<FAbstractEquation>(new SortingIndexComparator());
	}
	
	/** 
	 * \brief Sorts a list of FVariables according to the sortingIndex of their equation.
	 */
	public static void FVariable.sortParameters(java.util.List<FVariable> vars) {
		Collections.sort(vars, new SortingIndexComparator());
	}
	
	/**
	 * \brief Sort dependent parameter equations.
	 * 
	 * This is a simple implementation of Kahn's topological sorting algorithm.
	 * This implementation will most likely be replaced by other graph 
	 * algorithms later on.
	 */
	public void FClass.sortDependentParameters() {
		// Get the list of parameter equations and set up preliminary data.
		ArrayList<FAbstractEquation> eqns = getFParameterEquations().toArrayList();
		int n_eqns = eqns.size();
		List<FVariable> vars = getFVariables();
		int n_vars = getNumFVariable();
		for (int i = 0; i < n_eqns; i++)
			eqns.get(i).sortingIndex = i;
		for (FVariable fv : vars)
			fv.parameterEquation();  // Calculate these befor changing anything
		
		// If there is nothing to do, return.
		if (n_eqns == 0)
			return;
		
		// Set up data structures for the adjacency graph.
		ArrayList<HashSet<FAbstractEquation>> toNodes = new ArrayList<HashSet<FAbstractEquation>>(n_eqns);
		ArrayList<HashSet<FAbstractEquation>> fromNodes = new ArrayList<HashSet<FAbstractEquation>>(n_eqns);
		for (int i = 0; i < n_eqns; i++) {
			toNodes.add(new HashSet<FAbstractEquation>());
			fromNodes.add(new HashSet<FAbstractEquation>());
		}
		
		// For each equation
		for (FAbstractEquation eqn : eqns) {
			// Retreive all variables referenced in right hand side
			Set<AbstractFVariable> deps = eqn.referencedFVariablesInRHS();
			// Build the actual adjacency graph.
			for (AbstractFVariable fv : deps) {
				FAbstractEquation depEqn = fv.parameterEquation();
				if (depEqn != null && depEqn.sortingIndex >= 0) {
					toNodes.get(depEqn.sortingIndex).add(eqn);		
					fromNodes.get(eqn.sortingIndex).add(depEqn);
				}
			}
		}	
				
		// Sort using algorithm described at
		// http://en.wikipedia.org/wiki/Topological_sorting
		List<FAbstractEquation> L = new List<FAbstractEquation>();
		Queue<FAbstractEquation> S = 
			new java.util.concurrent.ArrayBlockingQueue<FAbstractEquation>(n_eqns);
		
		// Add all nodes without incoming edges to S
		for (int i = 0; i < n_eqns; i++) 
			if (fromNodes.get(i).isEmpty()) 
				S.add(eqns.get(i));
		
		// Repeat while S is not empty
		int si = 0;
		while (S.size() > 0) {
			// Remove a node n from S
			FAbstractEquation n = S.remove();
			// Insert n into L
			L.add(n);
			// For each node m with an edge e from n to m (in original order)
			Set<FAbstractEquation> nodes = FAbstractEquation.sortingSet();
			nodes.addAll(toNodes.get(n.sortingIndex));
			for (FAbstractEquation m : nodes) {
				// Remove edge e from fromNodes
				fromNodes.get(m.sortingIndex).remove(n);
				// If m has no incoming edges add m to S
				if (fromNodes.get(m.sortingIndex).isEmpty()) 
					S.add(m);
			}
			// Remove e from toNodes
			toNodes.get(n.sortingIndex).clear();
			// Done with n - update sortingIndex
			n.sortingIndex = si++;
		}
		
		// Not possible to find an ordering without cycles?
		if (L.getNumChild() != eqns.size()) {
			cyclicParameters = true;
			return;
		}
		
		// Replace old parameter equation list
		setFParameterEquationList(L);
		
		// Rearrange parameters in same order as parameter equations
		ArrayList<FVariable> depParams = new ArrayList<FVariable>(n_eqns);
		ArrayList<Integer> depParamIndices = new ArrayList<Integer>(n_eqns);
		for (int i = 0; i < n_vars; i++) {
			FVariable fv = vars.getChild(i);
			if (fv.hasParameterEquation()) {
				depParams.add(fv);
				depParamIndices.add(new Integer(i));
			}
		}
		FVariable.sortParameters(depParams);
		for (int i = 0; i < depParams.size(); i++)
			vars.setChild(depParams.get(i), depParamIndices.get(i).intValue());

		// Flush AST since the structure has changed.
		flush();
	}	
		
}

aspect TransformCanonicalErrorCheck {
	
	public void FClass.checkFClassDimensions() {
		// Check dimensions of DAE
		int n_eq_F = numScalarEquations();
		int n_vars_F = numAlgebraicRealVariables() + 
		               numDifferentiatedRealVariables() + 
		               numDiscreteVariables();
		if (n_eq_F!=n_vars_F) {
			error("The DAE system has " + n_eq_F + " equations and " + n_vars_F + " free variables.");
		}
		
		// Check dimensions of DAE initialization system
		int n_eq_F0 = numInitialEquations() + numScalarEquations();
		int n_vars_F0 = numAlgebraicRealVariables() + 
		                2*numDifferentiatedRealVariables() + 
		                numDiscreteVariables();
		if (n_eq_F0>n_vars_F0) {
			error("The DAE initialization system has " + n_eq_F0 + " equations and " + n_vars_F0 + " free variables.");
		}
		
	}
	
	public void FClass.collectErrors() {
		if (cyclicParameters) {
			error("The model "+ name() +" contains cyclic parameter dependencies.");
		}
		if (getAliasManager().aliasError()) {
			error(getAliasManager().getAliasErrorMessage());
		}
		for (String str : aliasErrors) {
			error(str);
		}
		checkDuplicateVariables();
		checkFClassDimensions();
	}
}

aspect FlushFClass {
	/**
	 * \brief Flush all caches, including collection attributes.
	 */
	public void ASTNode.flushAll() {
		flushCache();
		flushCollectionCache();
	}

	/**
	 * \brief Flush all caches, including collection attributes, and also
	 * flush all children. In addition, the is$final attribute is set to false
	 * for FIdUseExps and FDerExps in order to enable rewrites of such nodes
	 * after elimination of alias variables.
	 */
	public void ASTNode.flushAllRecursiveClearFinal() {
		flushAll();
		//is$Final = false;
		for (int i=0;i<getNumChild();i++) {
			getChild(i).flushAllRecursiveClearFinal();
		}
	}
	
	public void FIdUseExp.flushAllRecursiveClearFinal() {
		flushAll();
		is$Final = false;
		for (int i=0;i<getNumChild();i++) {
			getChild(i).flushAllRecursiveClearFinal();
		}
	}
	
	public void FDerExp.flushAllRecursiveClearFinal() {
		flushAll();
		is$Final = false;
		for (int i=0;i<getNumChild();i++) {
			getChild(i).flushAllRecursiveClearFinal();
		}
	}

	/**
	 * \brief Flush all caches, including collection attributes, and also
	 * flush all children. 
	 */
	public void ASTNode.flushAllRecursive() {
		flushAll();
		for (int i=0;i<getNumChild();i++) {
			getChild(i).flushAllRecursive();
		}
	}
	
	/**
	 * \brief Flush model AST and enable rewrites of FIdUseExps and FDerExps
	 * after alias elimination.
	 */
	public void FClass.flush() {
		flushAllRecursiveClearFinal();
		//is$Final = true;
	}
	
}
