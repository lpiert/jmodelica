import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/*
    Copyright (C) 2016 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect eliminateLinearEquations {

    public class FClass {
        private static final int ALIAS_SIZE = 1;

        /**
         *  <h1>Eliminate Linear Equations</h1>
         *  <p>
         *  Eliminates <i>simple</i> linear equations from a set of equations.
         */
        public class eliminateLinearEquations extends Transformation {

            public eliminateLinearEquations() {
                super("eliminate_linear_equations");
            }

            @Override
            public void perform() {
                ArrayList<FAbstractEquation> equations = new ArrayList<FAbstractEquation>();
                for (FAbstractEquation faq : getFAbstractEquations()) {
                    equations.add(faq);
                }

                Set<FAbstractEquation> modified = new HashSet<FAbstractEquation>();
                Map<FAbstractEquation, Map<String, Integer>> equationMap =
                        new LinkedHashMap<FAbstractEquation, Map<String, Integer>>();
                for (int i = 0; i < equations.size(); ++i) {
                    FAbstractEquation src = equations.get(i);

                    if (!src.isSimpleLinear()) {
                        continue;
                    }

                    equationMap.put(src, src.coefficientMap());

                    for (int j = i + 1; j < equations.size(); ++j) {
                        FAbstractEquation dest = equations.get(j);

                        if (modified.contains(dest) || !dest.isSimpleLinear()) {
                            continue;
                        }

                        equationMap.put(dest, dest.coefficientMap());
                        FAbstractEquation newEquation = null;
                        if ((newEquation = removeAliasesFromEquations(equationMap, src, dest)) != null) {
                            // ASTNode.log.debug("Rewriting " + dest + ", using\n\t" + src + "\n\tto\n" +
                            //         newEquation + ".");
                            equations.set(j, newEquation);
                            modified.add(newEquation);
                        }
                    }
                }
            }

            /**
             *  Removes an alias expression mutual for two equations.
             *  
             *  @param  equationMap
             *          An equation map; each equation key maps to another map which maps the terms used in the
             *          equation to their respective coefficients.
             *  @param  source
             *          The equation to compare against {@code dest}.
             *  @param  dest
             *          The equation to compare against {@code source}. This equation will be overwritten in case a
             *          sub-expression is eliminated.
             *  @return
             *          The modified equation, {@code null} otherwise.
             */
            protected FAbstractEquation removeAliasesFromEquations(
                    Map<FAbstractEquation, Map<String, Integer>> equationMap,
                    FAbstractEquation source, FAbstractEquation dest) {

                Map<String, Integer> coeffSource = equationMap.get(source);
                Map<String, Integer> coeffDest   = equationMap.get(dest);

                Set<String> mutual              = new LinkedHashSet<String>();
                Set<String> uniqueSource        = new LinkedHashSet<String>();
                Set<String> uniqueDest          = new LinkedHashSet<String>();

                if (!coefficientSort(mutual, uniqueSource, uniqueDest, coeffSource.keySet(), coeffDest.keySet())) {
                    return null;
                }

                String pivot = null;
                for (String key : mutual) {
                    pivot = key;
                    break;
                }

                if (pivot == null) {
                    return null;
                }
                mutual.remove(pivot);

                ArrayList<FExp> leftExps = new ArrayList<FExp>();
                ArrayList<FExp> rightExps = new ArrayList<FExp>();

                int c1 = coeffSource.get(pivot);
                int c2 = coeffDest.get(pivot);
                FExp coeff = newDivision(c2, c1);

                /*
                 * Put all constants on the same side.
                 */

                if (uniqueDest.isEmpty()) {
                    leftExps.add(new FIntegerLitExp(0));
                } else {
                    for (String key : uniqueDest) {
                        leftExps.add(newTerm(coeffDest.get(key), key));
                    }
                }

                if (uniqueSource.isEmpty()) {
                    rightExps.add(new FIntegerLitExp(0));
                } else {
                    for (String key : uniqueSource) {
                        rightExps.add(newTerm(coeffSource.get(key), key));
                    }
                }

                FExp lhs = FExp.createBalancedBinaryTree(new FAddExp(), leftExps);
                FExp rhs = new FMulExp(coeff, addTree(rightExps));
                rightExps.clear();

                for (String key : mutual) {
                    rightExps.add(new FMulExp(newDivision(c2 * coeffSource.get(key) - c1 * coeffDest.get(key),
                            c1), newTerm(key)));
                }

                rhs = addTree(rhs, rightExps);
                FAbstractEquation newEquation = new FEquation(new FNormalEquation(), lhs, rhs);
                dest.replaceMe(newEquation);
                return newEquation;
            }

            /**
             * Sorts coefficient keys according to whether or not they exist in both equations' key sets or not.
             * 
             * @param   mutual
             *          The set in which to put keys present in both equations.
             * @param   sourceUnique
             *          The set in which to put keys only present in the source equation.
             * @param   destUnique
             *          The set in which to put keys only present in the destination equation.
             * @param   sourceKeys
             *          The coefficient keys of the source equation.
             * @param   destKeys
             *          The coefficient keys of the destination equation.
             */
            private boolean coefficientSort(Set<String> mutual, Set<String> sourceUnique, Set<String> destUnique,
                    Set<String> sourceKeys, Set<String> destKeys) {

                destUnique.addAll(destKeys);

                for (String key : sourceKeys) {
                    if (destKeys.contains(key)) {
                        mutual.add(key);
                    } else {
                        sourceUnique.add(key);
                    }
                }
                destUnique.removeAll(mutual);

                if (mutual.size() <= 0) {
                    return false;
                }

                return !(sourceUnique.isEmpty() && destUnique.isEmpty() || sourceUnique.size() > ALIAS_SIZE ||
                        destUnique.size() > ALIAS_SIZE);
            }

            /* ========================= *
             *  Create new expressions.  *
             * ========================= */

            private FExp addTree(ArrayList<FExp> exps) {
                if (exps.size() == 0) {
                    return new FIntegerLitExp(0);
                }
                if (exps.size() == 1) {
                    return exps.get(0);
                }
                if (exps.size() % 2 != 0) {
                    exps.add(new FIntegerLitExp(0));
                }
                return FExp.createBalancedBinaryTree(new FAddExp(), exps);
            }

            private FExp addTree(FExp exp, ArrayList<FExp> exps) {
                return new FAddExp(exp, addTree(exps));
            }

            private FExp newDivision(int nominator, int denominator) {
                if (nominator % denominator == 0) {
                    return new FIntegerLitExp(nominator / denominator);
                }
                return new FDivExp(new FIntegerLitExp(nominator), new FIntegerLitExp(denominator));
            }

            private FExp newTerm(String expKey) {
                if (expKey.equals(FLitExp.coeffKey)) {
                    return new FIntegerLitExp(1);
                } else if (expKey.equals(FTimeExp.coeffKey)) {
                    return new FTimeExp();
                }
                return new FIdUseExp(expKey);
            }

            private FExp newTerm(int coeff, String expKey) {
                return new FMulExp(new FIntegerLitExp(coeff), newTerm(expKey));
            }
        }

    }

    /* ============================ *
     *  Collect term coefficients.  *
     * ============================ */

    syn Map<String, Integer> FAbstractEquation.coefficientMap() { return new LinkedHashMap<String, Integer>(); }
    eq FEquation.coefficientMap() {
        Map<String, Integer> coefficientMap = new LinkedHashMap<String, Integer>();

        for (FExp exp : linearTerms()) {
            String coeffKey = exp.myCoeffKey();
            int newValue = exp.coeff() + (coefficientMap.containsKey(coeffKey) ? coefficientMap.get(coeffKey) : 0);
            if (newValue == 0) {
                coefficientMap.remove(coeffKey);
            } else {
                coefficientMap.put(coeffKey, newValue);
            }
        }
        return coefficientMap;
    }

    syn java.util.List<FExp> FAbstractEquation.linearTerms() = Collections.<FExp> emptyList();
    eq FEquation.linearTerms() {
        ArrayList<FExp> linearTerms = new ArrayList<FExp>();
        linearTerms.addAll(collectUses());
        linearTerms.addAll(timeExps());
        for (FExp exp : terms()) {
            if (exp.isLiteralExp()) {
                linearTerms.add(exp);
            }
        }
        return linearTerms;
    }

    /* ======================== *
     *  Calculate coefficient.  *
     * ======================== */

    syn int FExp.coeff() = myCoeff() * outerCoeff();

    syn int FExp.myCoeff()                          = 1;
    eq FIntegerLitExp.myCoeff()                     = getValue();
    eq FRealLitExp.myCoeff()                        = (int) getValue();

    inh int FExp.outerCoeff();
    eq FAbstractEquation.getChild().outerCoeff()    = 1;
    eq FEquation.getRight().outerCoeff()            = -1;

    eq FLitExp.getChild().outerCoeff()              = outerCoeff() * myCoeff();
    eq FDotSubExp.getRight().outerCoeff()           = - coeff();
    eq FNegExp.getChild().outerCoeff()              = - coeff();

    eq FDotMulExp.getChild(int index).outerCoeff()  {
        FExp literal = getLiteral();
        if (literal == getChild(index)) {
            return coeff();
        }
        return literal.coeff();
    }

    syn FExp FDotMulExp.getLiteral() {
        FExp exp = null;
        if ((exp = getLeft()).isLiteralExp()) {
            return exp;
        }
        if ((exp = getRight()).isLiteralExp()) {
            return exp;
        }
        return null;
    }

    syn FExp FDotMulExp.getTerm() {
        FExp exp = null;
        if ((exp = getLeft()).isLiteralExp()) {
            return getRight();
        }
        return exp;
    }

    /* ============================ *
     *  Keys for coefficient maps.  *
     * ============================ */

    public static final String FLitExp.coeffKey     = "#const";
    public static final String FTimeExp.coeffKey    = "time";
    syn String FExp.myCoeffKey()                    = toString();

    eq FIdUseExp.myCoeffKey()                       = name();
    eq FLitExp.myCoeffKey()                         = coeffKey;
    eq FNegExp.myCoeffKey()                         = getFExp().myCoeffKey();
    eq FTimeExp.myCoeffKey()                        = coeffKey;

    /* ==================================================== *
     *  Determine whether or not to eliminate an equation.  *
     * ==================================================== */

    syn boolean FAbstractEquation.isSimpleLinear() = false;
    eq FEquation.isSimpleLinear() = !containsActiveAnnotations() && !isSimpleAlias() &&
            getLeft().isSimpleLinearExp() && getRight().isSimpleLinearExp();

    syn boolean FExp.isSimpleLinearExp()    = false;
    eq FBinExp.isSimpleLinearExp()          = getLeft().isSimpleLinearExp() && getRight().isSimpleLinearExp();
    eq FBoolBinExp.isSimpleLinearExp()      = false;
    eq FDotDivExp.isSimpleLinearExp()       = false;
    eq FDotMulExp.isSimpleLinearExp()       = getLiteral() != null && super.isSimpleLinearExp();
    eq FDotPowExp.isSimpleLinearExp()       = false;

    eq FDerExp.isSimpleLinearExp()          = false;
    eq FNegExp.isSimpleLinearExp()          = getFExp().isSimpleLinearExp();
    eq FRelExp.isSimpleLinearExp()          = false;

    eq FIdUseExp.isSimpleLinearExp()        = true;
    eq FTimeExp.isSimpleLinearExp()         = true;
    eq FIntegerLitExp.isSimpleLinearExp()   = true;
    eq FRealLitExp.isSimpleLinearExp()      = getValue() == ((int) getValue()) && !isLiteralZero();

    syn boolean FAbstractEquation.isSimpleAlias() = false;
    eq FEquation.isSimpleAlias() = getLeft().isSimpleAlias() && getRight().isSimpleAlias();
    syn boolean FExp.isSimpleAlias() = false;
    eq FIdUseExp.isSimpleAlias() = true;
    eq FLitExp.isSimpleAlias() = true;
    eq FNegExp.isSimpleAlias() = getFExp().isSimpleAlias();
}