
/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file CGenerator.java
*  \brief CGenerator class.
*/

import java.io.*;

public class CGenerator extends GenericGenerator {
	
	protected static final String INDENT = "    ";
	
	class DAETag_C_externalFuncIncludes extends DAETag {
		
		public DAETag_C_externalFuncIncludes(AbstractGenerator myGenerator,
				FClass fclass) {
			super("external_func_includes","C: external function includes",
					myGenerator,fclass);
		}
		
		public void generate(PrintStream genPrinter) {
			for (String incl : fclass.externalIncludes()) 
				genPrinter.println(incl);
		}
	}

	class DAETag_C_scalingMethod extends DAETag {
		
		public DAETag_C_scalingMethod(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_scaling_method","C: scaling_method",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("enable_variable_scaling")) {
				genPrinter.print("JMI_SCALING_VARIABLES");
			} else {
				genPrinter.print("JMI_SCALING_NONE");
			}
		}
	
	}
	
	class DAETag_C_equationResiduals extends DAETag {
		
		public DAETag_C_equationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_equation_residuals","C: equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_dae")) {
				for (FAbstractEquation e : fclass.equations()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				int i=0;
				for (FAbstractEquation e : fclass.equations()) {
					e.genResidual_C(i, INDENT, genPrinter);				
					i += e.numScalarEquations();
				}
			}
		}
	
	}

	class DAETag_C_eventIndicatorResiduals extends DAETag {
		
		public DAETag_C_eventIndicatorResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_event_indicator_residuals","C: event indicator residuals in equations",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FRelExp e : fclass.relExpInEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FRelExp e : fclass.relExpInEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}
		}
	
	}

	class DAETag_C_initialEquationResiduals extends DAETag {
		
		public DAETag_C_initialEquationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_equation_residuals","C: initial equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_dae")) {
				for (FAbstractEquation e : fclass.equations()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				for (FAbstractEquation e : fclass.initialEquations()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				int i=0;
				for (FAbstractEquation e : fclass.equations()) {
					e.genResidual_C(i,INDENT,genPrinter);				
					i += e.numScalarEquations();
				}
				for (FAbstractEquation e : fclass.initialEquations()) {
					e.genResidual_C(i,INDENT,genPrinter);				
					i += e.numScalarEquations();
				}
			}
		}	
	}

	class DAETag_C_initialEventIndicatorResiduals extends DAETag {
		
		public DAETag_C_initialEventIndicatorResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_event_indicator_residuals","C: event indicator residuals in initial equations",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FRelExp e : fclass.relExpInEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FRelExp e : fclass.relExpInInitialEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			int i=0;
			for (FRelExp e : fclass.relExpInEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}
			for (FRelExp e : fclass.relExpInInitialEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}			
		}
	}
	
	class DAETag_C_initialGuessEquationResiduals extends DAETag {
		
		public DAETag_C_initialGuessEquationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_guess_equation_residuals","C: initial guess equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FRealVariable fv : fclass.realVariables()) {
				if (!(fv.fixedAttribute() )) {
					fv.genStartAttributeResidual_C(i,"   ",genPrinter);
					i++;
				}
			}
		}
	
	}

	class DAETag_C_initialDependentParameterResiduals extends DAETag {
		
		public DAETag_C_initialDependentParameterResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_dependent_parameter_residuals","C: dependent parameter residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_dae")) {			
				int i=0;
				for (FAbstractEquation e : fclass.getFParameterEquations()) {
					e.genResidual_C(i, INDENT, genPrinter);				
					i += e.numScalarEquations();
				}
			}
		}
	
	}

	class DAETag_C_initialDependentParameterAssignments extends DAETag {
		
		public DAETag_C_initialDependentParameterAssignments(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_dependent_parameter_assignments","C: dependent parameter assignments",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FAbstractEquation e : fclass.getFParameterEquations()) 
				e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
			for (FAbstractEquation e : fclass.getFParameterEquations()) 
				e.genAssignment_C(INDENT, genPrinter);
		}
	
	}

	class DAETag_C_variableAliases extends DAETag {
		
		public DAETag_C_variableAliases(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_variable_aliases","C: macros for C variable aliases",
			  myGenerator,fclass);
		}
		
		private void generateVar(PrintStream genPrinter, FVariable fv, String offset, int index) {
			genPrinter.print("#define ");
			genPrinter.print(fv.name_C());
			genPrinter.print(" ((*(jmi->z))[jmi->offs_");
			genPrinter.print(offset);
			genPrinter.print("+");
			genPrinter.print(index);
			genPrinter.print("])\n");
		}
		
		private void generateVarList(PrintStream genPrinter, Iterable<? extends FVariable> list, String offset) {
			generateVarList(genPrinter, list, offset, 0);
		}

		private void generateVarList(PrintStream genPrinter, Iterable<? extends FVariable> list, String offset, int index) {
			for (FVariable fv : list)
				generateVar(genPrinter, fv, offset, index++);
		}

		private void generatePreVar(PrintStream genPrinter, FVariable fv, String offset, int index) {
			genPrinter.print("#define ");
			genPrinter.print("pre" + fv.name_C());
			genPrinter.print(" ((*(jmi->z))[jmi->offs_pre_");
			genPrinter.print(offset);
			genPrinter.print("+");
			genPrinter.print(index);
			genPrinter.print("])\n");
		}
		
		private void generatePreVarList(PrintStream genPrinter, Iterable<? extends FVariable> list, String offset) {
			int index = 0;
			for (FVariable fv : list)
				generatePreVar(genPrinter, fv, offset, index++);
		}
	
		public void generate(PrintStream genPrinter) {
			generateVarList(genPrinter, fclass.independentRealConstants(),     "real_ci");
			generateVarList(genPrinter, fclass.dependentRealConstants(),       "real_cd");
			generateVarList(genPrinter, fclass.independentRealParameters(),    "real_pi");
			generateVarList(genPrinter, fclass.dependentRealParameters(),      "real_pd");
			// Handle enums as Integers
			generateVarList(genPrinter, fclass.independentIntegerConstants(),  "integer_ci");
			generateVarList(genPrinter, fclass.independentEnumConstants(),     "integer_ci", fclass.numIndependentIntegerConstants());
			generateVarList(genPrinter, fclass.dependentIntegerConstants(),    "integer_cd");
			generateVarList(genPrinter, fclass.dependentEnumConstants(),       "integer_cd", fclass.numDependentIntegerConstants());
			generateVarList(genPrinter, fclass.independentIntegerParameters(), "integer_pi");
			generateVarList(genPrinter, fclass.independentEnumParameters(),    "integer_pi", fclass.numIndependentIntegerParameters());
			generateVarList(genPrinter, fclass.dependentIntegerParameters(),   "integer_pd");
			generateVarList(genPrinter, fclass.dependentEnumParameters(),      "integer_pd", fclass.numDependentIntegerParameters());
			generateVarList(genPrinter, fclass.independentBooleanConstants(),  "boolean_ci");
			generateVarList(genPrinter, fclass.dependentBooleanConstants(),    "boolean_cd");
			generateVarList(genPrinter, fclass.independentBooleanParameters(), "boolean_pi");
			generateVarList(genPrinter, fclass.dependentBooleanParameters(),   "boolean_pd");
			generateVarList(genPrinter, fclass.derivativeVariables(),          "real_dx");
			generateVarList(genPrinter, fclass.differentiatedRealVariables(),  "real_x");
			generateVarList(genPrinter, fclass.realInputs(),                   "real_u");
			generateVarList(genPrinter, fclass.algebraicRealVariables(),       "real_w");
			genPrinter.print("#define _time ((*(jmi->z))[jmi->offs_t])\n"); 
			generateVarList(genPrinter, fclass.discreteRealVariables(),        "real_d");
			generateVarList(genPrinter, fclass.discreteIntegerVariables(),     "integer_d");
			generateVarList(genPrinter, fclass.discreteEnumVariables(),        "integer_d");
			generateVarList(genPrinter, fclass.integerInputs(),                "integer_u");
			generateVarList(genPrinter, fclass.enumInputs(),                   "integer_u");
			generateVarList(genPrinter, fclass.discreteBooleanVariables(),     "boolean_d");
			generateVarList(genPrinter, fclass.booleanInputs(),                "boolean_u");
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				generatePreVarList(genPrinter, fclass.derivativeVariables(),          "real_dx");
				generatePreVarList(genPrinter, fclass.differentiatedRealVariables(),  "real_x");
				generatePreVarList(genPrinter, fclass.realInputs(),                   "real_u");
				generatePreVarList(genPrinter, fclass.algebraicRealVariables(),       "real_w");
				generatePreVarList(genPrinter, fclass.discreteRealVariables(),        "real_d");
				generatePreVarList(genPrinter, fclass.discreteIntegerVariables(),     "integer_d");
				generatePreVarList(genPrinter, fclass.discreteEnumVariables(),        "integer_d");
				generatePreVarList(genPrinter, fclass.integerInputs(),                "integer_u");
				generatePreVarList(genPrinter, fclass.enumInputs(),                   "integer_u");
				generatePreVarList(genPrinter, fclass.discreteBooleanVariables(),     "boolean_d");
				generatePreVarList(genPrinter, fclass.booleanInputs(),                "boolean_u");
			}

		}
	}

	/**
	 * Generates code for BLT block residuals
	 */
	class DAETag_C_dae_blocks_residual_functions extends DAETag {
		
		public DAETag_C_dae_blocks_residual_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_dae_blocks_residual_functions","C: C functions for the DAE BLT block residuals",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				int block_counter = 0;

				for (AbstractEquationBlock block : fclass.getDAEStructuredBLT().getAllBlocks()) {
				 	block.genBlockResidualFunction_C(block_counter,false,"  ",genPrinter); 
				  	if (!block.isSolvable()) {
				  		block_counter++;
				  	}				  
				}
			}
		}
	}

	/**
	 * Generates code for adding BLT blocks 
	 */
	class DAETag_C_dae_add_blocks_residual_functions extends DAETag {
		
		public DAETag_C_dae_add_blocks_residual_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_dae_add_blocks_residual_functions","C: Add the DAE block functions to the JMI struct",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode") && !fclass.root().options.getBooleanOption("generate_ode_jacobian")) {
			
				int block_counter = 0;

				for (AbstractEquationBlock block : fclass.getDAEStructuredBLT().getAllBlocks()) {
					if (!block.isSolvable()) {
						block.genBlockAddCall_C(block_counter,false," ",genPrinter);
						block_counter++;
					}
				}
			}
		}
	}

	/**
	 * Number of DAE blocks
	 */
	class DAETag_C_dae_n_blocks extends DAETag {
		
		public DAETag_C_dae_n_blocks(AbstractGenerator myGenerator, FClass fclass) {
			super("n_dae_blocks","C: Number of DAE blocks",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
		
				int block_counter = 0;	
				for (AbstractEquationBlock block : fclass.getDAEBLT()) {
				    if (!block.isSolvable()) {
						block_counter++;
					}
				}
				genPrinter.print(block_counter);
			} else {
				genPrinter.print("0");
			}
		}
	}

	/**
	 * Generates code for BLT block residuals (DAE initialization system)
	 */
	class DAETag_C_dae_init_blocks_residual_functions extends DAETag {
		
		public DAETag_C_dae_init_blocks_residual_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_dae_init_blocks_residual_functions","C: C functions for the DAE BLT block residuals",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				int block_counter = 0;
				for (AbstractEquationBlock block : fclass.getDAEInitBLT()) {
					block.genBlockResidualFunction_C(block_counter,true,"  ",genPrinter); 
				  	if (!block.isSolvable()) {
				  		block_counter++;
				  	}
				}
				
			}
		}
	}

	/**
	 * Generates code for adding BLT blocks 
	 */
	class DAETag_C_dae_init_add_blocks_residual_functions extends DAETag {
		
		public DAETag_C_dae_init_add_blocks_residual_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_dae_init_add_blocks_residual_functions","C: Add the DAE initialization block functions to the JMI struct",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode") && !fclass.root().options.getBooleanOption("generate_ode_jacobian")) {
				int block_counter = 0;
				for (AbstractEquationBlock block : fclass.getDAEInitBLT()) {
				    if (!block.isSolvable()) {
				    	block.genBlockAddCall_C(block_counter,true," ",genPrinter);
						block_counter++;
					}
				}
			}
		}
	}

	/**
	 * Number of DAE initialization blocks
	 */
	class DAETag_C_dae_init_n_blocks extends DAETag {
		
		public DAETag_C_dae_init_n_blocks(AbstractGenerator myGenerator, FClass fclass) {
			super("n_dae_init_blocks","C: Number of DAE initialization blocks",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				int block_counter = 0;	
				for (AbstractEquationBlock block : fclass.getDAEInitBLT()) {
				    if (!block.isSolvable()) {
						block_counter++;
					}
				}
				genPrinter.print(block_counter);
			} else {
				genPrinter.print("0");
			}
		}
	}

	/**
	 * Generates code for computing the guard expressions
	 */
	class DAETag_C_ode_guards extends DAETag {
		
		public DAETag_C_ode_guards(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_guards","C: Compute guard expressions ODE",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				for (FExp e : fclass.guardExpInEquations()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				int i=0;
				for (FExp e : fclass.guardExpInEquations()) {
				    genPrinter.print("  _guards(" + i + ") = ");
					e.prettyPrint_C(genPrinter,"");		
					genPrinter.print(";\n");		
					i++;
				}
			}
		}
	}

	/**
	 * Generates code for computing the guard expressions in the initial equations
	 */
	class DAETag_C_ode_guards_init extends DAETag {
		
		public DAETag_C_ode_guards_init(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_guards_init","C: Compute guard expressions ODE initialization system",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				for (FExp e : fclass.guardExpInInitialEquations()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				int i=0;
				for (FExp e : fclass.guardExpInInitialEquations()) {
				    genPrinter.print("  _guards_init(" + i + ") = ");
					e.prettyPrint_C(genPrinter,"");		
					genPrinter.print(";\n");		
					i++;
				}
			}
		}
	}

	/**
	 * Generates code for computation of the nect time event.
	 */
	class DAETag_C_ode_time_events extends DAETag {
		
		public DAETag_C_ode_time_events(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_time_events","C: Compute the next time event.",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				for (FSampleExp e : fclass.samplers()) 
					e.genVarDecls_C(ASTNode.printer_C, genPrinter, INDENT);
				genPrinter.print("  jmi_real_t nextTimeEvent;\n");
				genPrinter.print("  jmi_real_t nextTimeEventTmp;\n");				
				genPrinter.print("  jmi_real_t nSamp;\n");	
				genPrinter.print("  nextTimeEvent = JMI_INF;\n");
				
				
				/*
					There are three cases to consider for each sampler:
					 1) The current time is (surely) before the offset of 
					    the sampler. In this case, the next time event occurs
					    at time offset.
					 2) The current time is a sample instant of the sampler
					    in which case next time event is the next sample
					    instant.
					 3) If neither of the above holds true, then the current
					    time is in between two sample instants of the sampler.
					    In this case, the next time event is the next sample
					    instant.
				
				*/
				
	//			genPrinter.print("  printf(\"sample function called at time %f\\n\",_t);\n");	
				for (FSampleExp s : fclass.samplers()) {
					genPrinter.print("  nextTimeEventTmp = JMI_INF;\n");
					// Case 1: time is before offset 
					genPrinter.print("  if (SURELY_LT_ZERO(_t - ");
					s.getOffset().prettyPrint_C(genPrinter,"");
					genPrinter.print(")) {\n");
					genPrinter.print("    nextTimeEventTmp = ");
					s.getOffset().prettyPrint_C(genPrinter,"");
					genPrinter.print(";\n");
					//genPrinter.print("printf(\"Hepp: %f %f\\n\",_t,nextTimeEventTmp);\n");
				    genPrinter.print("  } ");	
				    // Case 2: we are at a sample			    					
					genPrinter.print(" else if (ALMOST_ZERO(jmi_dremainder(_t - ");
					s.getOffset().prettyPrint_C(genPrinter,"");
					genPrinter.print(",");
					s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("))) {\n");
					// nSamp should be almost an integer - round to get it exact
					genPrinter.print("    nSamp = jmi_dround((_t-");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(")/(");
				    s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("));\n");
					genPrinter.print("    nextTimeEventTmp = (nSamp + 1.0)*");
					s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print(" + ");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(";\n");
/*----*//*
					genPrinter.print("printf(\"Hopp: %12.12f %12.12f %12.12f %12.12f\\n\",_t,nextTimeEventTmp,nSamp,");
					genPrinter.print("(_t-");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(")/(");
				    s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("));\n");

*//*---*/
				    genPrinter.print("  } ");

					// Case 3: Neither of the above: in between samples
					genPrinter.print(" else if (SURELY_GT_ZERO(jmi_dremainder(_t - ");
					s.getOffset().prettyPrint_C(genPrinter,"");
					genPrinter.print(",");
					s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("))) {\n");
					// User the floor function to get the number of previous
					// samples
					genPrinter.print("    nSamp = floor((_t-");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(")/(");
				    s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("));\n");
					genPrinter.print("    nextTimeEventTmp = (nSamp + 1.0)*");
					s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print(" + ");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(";\n");
/*----*//*
					genPrinter.print("printf(\"Hopp2: %12.12f %12.12f %12.12f %12.12f\\n\",_t,nextTimeEventTmp,nSamp,");
					genPrinter.print("(_t-");
				    s.getOffset().prettyPrint_C(genPrinter,"");
				    genPrinter.print(")/(");
				    s.getInterval().prettyPrint_C(genPrinter,"");
					genPrinter.print("));\n");

*//*---*/
				    genPrinter.print("  }\n ");


				    genPrinter.print("  if (nextTimeEventTmp<nextTimeEvent) {\n");
				    genPrinter.print("    nextTimeEvent = nextTimeEventTmp;\n");
				    genPrinter.print("  }\n");				    						
				}
	//			genPrinter.print("printf(\"Happ: %12.12f\\n\",nextTimeEvent);\n");

				genPrinter.print("  *nextTime = nextTimeEvent;\n");
			}
		}
	}

	/**
	 * Generates code for solving the BLT blocks
	 */
	class DAETag_C_ode_derivatives extends DAETag {
		
		public DAETag_C_ode_derivatives(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_derivatives","C: Compute derivatives of the ODE",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				fclass.getDAEStructuredBLT().genVarDecls_C(genPrinter, INDENT);
				genPrinter.print(INDENT + "model_ode_guards(jmi);\n");						
				fclass.getDAEStructuredBLT().genOdeDerivativeBlocks(genPrinter);
			}
		}
	}

	/**
	 * Generates code for solving the BLT blocks in the initialization system
	 */
	class DAETag_C_ode_initialization extends DAETag {
		
		public DAETag_C_ode_initialization(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_initialization","C: Solve the initialization system",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("generate_ode")) {
				fclass.getDAEInitBLT().genVarDecls_C(genPrinter, INDENT);
				genPrinter.print(INDENT + "model_ode_guards(jmi);\n");						
				int block_counter = 0;
				for (AbstractEquationBlock block : fclass.getDAEInitBLT()) {
					block.genBlock_C(block_counter,true,"  ",genPrinter);                                        
                    if (!block.isSolvable()) {
						block_counter++;
                    }
				}
			}
		}
	}

	/**
	 * Generates code for solving the BLT blocks for computing the outputs
	 */
	class DAETag_C_ode_outputs extends DAETag {
		
		public DAETag_C_ode_outputs(AbstractGenerator myGenerator, FClass fclass) {
			super("C_ode_outputs","C: Compute the ODE outputs",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
		}
	}
	
	/**
	 * Generates headers for Modelica functions.
	 */
	class DAETag_C_function_headers extends DAETag {
		
		public DAETag_C_function_headers(AbstractGenerator myGenerator, FClass fclass) {
			super("C_function_headers","C: C function headers representing Modelica functions",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FFunctionDecl func : fclass.getFFunctionDecls())
				func.genHeader_C(genPrinter, "");
		}
	}
	
	/**
	 * Generates definitions for Modelica functions.
	 */
	class DAETag_C_functions extends DAETag {
		
		public DAETag_C_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_functions","C: C functions representing Modelica functions",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FFunctionDecl func : fclass.getFFunctionDecls())
				func.prettyPrint_C(genPrinter, "");
		}
	}
	
	/**
	 * Generates structs for Modelica records.
	 */
	class DAETag_C_records extends DAETag {
		
		public DAETag_C_records(AbstractGenerator myGenerator, FClass fclass) {
			super("C_records","C: C structs representing Modelica records",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FRecordDecl rec : fclass.getFRecordDecls())
				rec.prettyPrint_C(genPrinter, "");
		}
	}
	
	/**
	 * Generates wrappers for Modelica functions for exporting in a shared library.
	 */
	class DAETag_C_export_functions extends DAETag {
		
		public DAETag_C_export_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_export_functions","C: C functions wrapping internal representation of Modelica functions",
			  myGenerator,fclass);
		}

		public void generate(PrintStream out) {
			if (fclass.root().options.getBooleanOption("export_functions"))
				for (FFunctionDecl func : fclass.getFFunctionDecls())
					if (func.hasExportWrapper_C())
						func.exportWrapper_C(out, "");
		}
	}
	
	/**
	 * Generates export wrappers for groups of Modelica functions with the same signature.
	 * 
	 * Allows less functions to be imported from resulting shared library. 
	 * Requires $C_export_functions$, and must be after it.
	 */
	class DAETag_C_export_wrappers extends DAETag {
		
		public DAETag_C_export_wrappers(AbstractGenerator myGenerator, FClass fclass) {
			super("C_export_wrappers","C: C functions wrapping groups of export function with same signature",
			  myGenerator,fclass);
		}

		public void generate(PrintStream out) {
			if (fclass.root().options.getBooleanOption("export_functions_vba")) {
				// TODO: refactor out parts not specific to VBA to make it 
				//       easier to support other platforms with special needs
				int i = 0;
				String ind = ASTNode.printer_C.indent("");
				for (ArrayList<FFunctionDecl> grp : fclass.exportWrapperGroups()) {
					FFunctionDecl first = grp.get(0);
					String type = first.exportWrapperType_C();
					String name = "select_vba_" + (++i);
					
					out.format("char* %s_names[] = { ", name);
					String fmt = "\"%s\"";
					for (FFunctionDecl f : grp) {
						out.format(fmt, f.getFQName().nameUnderscore());
						fmt = ", \"%s\"";
					}
					out.print(" };\n");
					
					out.format("int %s_lengths[] = { ", name);
					fmt = "%d";
					for (FFunctionDecl f : grp) {
						out.format(fmt, f.getFQName().nameUnderscore().length());
						fmt = ", %d";
					}
					out.print(" };\n");
					
					out.format("%s (*%s_funcs[])(", type, name);
					first.exportWrapperArgumentTypeDecl_C(out);
					out.print(") = { ");
					fmt = "*%s";
					for (FFunctionDecl f : grp) {
						out.format(fmt, f.funcNameExportWrapper());
						fmt = ", *%s";
					}
					out.print(" };\n");
					
					String sep = first.myInputs().isEmpty() ? "" : ", "; 
					out.format("DllExport %s __stdcall %s(char* name%s", type, name, sep);
					first.exportWrapperArgumentDecl_C(out);
					out.print(") {\n");
					out.format("%sint i, j;\n", ind);
					out.format("%sfor (i = 0, j = 0; name[i] != 0; i++) \n", ind);
					out.format("%s%swhile (j < %d && i <= %s_lengths[j] && name[i] > %s_names[j][i]) j++;\n", ind, ind, grp.size(), name, name);
					out.format("%sif (j >= %d || strcmp(%s_names[j], name)) return 0;\n", ind, grp.size(), name);
					out.format("%sreturn %s_funcs[j](", ind, name);
					first.exportWrapperArgumentCall_C(out);
					out.print(");\n");
					out.print("}\n\n");
				}
			}
		}
	}

	class DAETag_C_outputVrefs extends DAETag {
		
		public DAETag_C_outputVrefs(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_output_vrefs","C: DAE output value references",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {

			if (fclass.numOutputs()>0) {
				genPrinter.print("static const int Output_vrefs[" + 
					fclass.numOutputs() + "] = {");		

				int ind = 0;
				for (FVariable fv : fclass.outputs()) {
					genPrinter.print(fv.valueReference());
					if (ind < fclass.numOutputs()-1) {
						genPrinter.print(",");
					}	
					ind++;
				}		
				genPrinter.print("};\n");
			} else {
				genPrinter.print("static const int Output_vrefs[1] = {-1};");
			} 
		}	
	}

	/**
	 * Generates MODEL_IDENTIFIER.
	 */
	class DAETag_C_model_id extends DAETag {
		
		public DAETag_C_model_id(AbstractGenerator myGenerator, FClass fclass) {
			super("C_model_id","C: Model identifier",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			genPrinter.print(fclass.nameUnderscore());
		}
	}

	/**
	 * Generates GUID.
	 */
	class DAETag_C_guid extends DAETag {
		
		public DAETag_C_guid(AbstractGenerator myGenerator, FClass fclass) {
			super("C_guid","C: GUID",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			genPrinter.print("\""+fclass.guid()+"\"");
		}
	}

	/**
	 * Generates start values.
	 */
	class DAETag_C_set_start_values extends DAETag {
		
		public DAETag_C_set_start_values(AbstractGenerator myGenerator, FClass fclass) {
			super("C_set_start_values","C: start values",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for(FVariable fv : fclass.independentConstants()) {
				fv.genStartValue_C(INDENT, genPrinter);
			}
			for(FVariable fv : fclass.dependentConstants()) {
				fv.genStartValue_C(INDENT, genPrinter);
			}
			for(FVariable fv : fclass.independentParameters()) {
				fv.genStartValue_C(INDENT, genPrinter);
			}
			genPrinter.print("   model_init_eval_parameters(jmi);\n");
			for(FVariable fv : fclass.variables()) {
				fv.genStartValue_C(INDENT, genPrinter);
			}
			for(FVariable fv : fclass.discretePreVariables()) {
				fv.genStartValue_C(INDENT, genPrinter);
			}
		}
	}
	
	/**
	 * \brief Returns the string denoting the beginning of the copyright blurb.
	 */
	protected String startOfBlurb() { return "/*"; }
	
	/**
	 * Constructor.
	 * 
	 * @param expPrinter Printer object used to generate code for expressions.
	 * @param escapeCharacter Escape characters used to decode tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public CGenerator(Printer expPrinter, char escapeCharacter,
			FClass fclass) {
		super(expPrinter, escapeCharacter, fclass);
	}

}

