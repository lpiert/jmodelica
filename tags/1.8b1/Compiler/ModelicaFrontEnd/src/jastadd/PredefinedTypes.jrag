/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect PredefinedTypes {

    public List SourceRoot.computePredefinedType() {
        return getProgram().getPredefinedTypeList();
    }
    
    inh List FullClassDecl.getPredefinedType();
    eq SourceRoot.getProgram().getPredefinedType() = computePredefinedType();
    eq Root.getChild().getPredefinedType() = null;

    
    /* The predefined types Real, Integer etc of Modelica are
     * much like user-defined classes, in that they have components 
     * (called attributes). However, the types of attributes are restricted
     * to RealType, IntegerType, StringType and BooleanType, which corresponds
     * to machine representations (in the following referred to as built-in types). 
     * It is therefore convenient to implement the predefined types
     * as regular classes, in order to reuse the name and type lookup mechanisms.
     * Some differences apply however. The types of predefined types must
     * be looked up only amongst the built-in types. Also, there are no 
     * component accesses that needs to be looked up predefined types.
     * The predefined types are defined in the (list) non terminal attribute
     * PredefinedType.
     */

    /**
     * This attribute defines the NTA for predefined types
     * which contains a list of predefined PrimitiveClassDef:s.
     */
    syn lazy List Program.getPredefinedTypeList() {
        //log.debug("Program.getPredefinedTypeList()");
        List l = new List();

        // Build a string with a Modelica class corresponding to Real
        String builtInDef= "type Real\n"; 
        builtInDef += "RealType value=0;\n";
        builtInDef += "parameter StringType quantity=\"\";\n";
        builtInDef += "parameter StringType unit=\"\";\n";
        builtInDef += "parameter StringType displayUnit=\"\";\n";
        builtInDef += "parameter RealType min=-1e20, max=1e20;\n";
        builtInDef += "parameter RealType start=0;\n";
        builtInDef += "parameter BooleanType fixed=false;\n";
        builtInDef += "parameter RealType nominal=0;\n";
        builtInDef += "parameter EnumType stateSelect=StateSelect.default;\n";
        builtInDef += "end Real;\n";
        
        builtInDef += "type Integer\n"; 
        builtInDef += "IntegerType value=0;\n";
        builtInDef += "parameter StringType quantity=\"\";\n";
        builtInDef += "parameter IntegerType min=-1e20, max=1e20;\n";
        builtInDef += "parameter IntegerType start=0;\n";
        builtInDef += "parameter BooleanType fixed=false;\n";
        builtInDef += "end Integer;\n";
        
        builtInDef += "type Boolean\n"; 
        builtInDef += "BooleanType value=0;\n";
        builtInDef += "parameter StringType quantity=\"\";\n";
        builtInDef += "parameter BooleanType start=false;\n";
        builtInDef += "parameter BooleanType fixed=true;\n";
        builtInDef += "end Boolean;\n";
        
        builtInDef += "type String\n"; 
        builtInDef += "StringType value=0;\n";
        builtInDef += "parameter StringType quantity=\"\";\n";
        builtInDef += "parameter StringType start=\"\";\n";
        builtInDef += "end String;\n";  

        builtInDef += "type StateSelect = enumeration("; 
        builtInDef += "never \"Do not use as state at all.\","; 
        builtInDef += " avoid \"Use as state, if it cannot be avoided (but only if variable appears"; 
        builtInDef += " differentiated and no other potential state with attribute";  
        builtInDef += " default, prefer, or always can be selected).\","; 
        builtInDef += " default \"Use as state if appropriate, but only if variable appears"; 
        builtInDef += " differentiated.\","; 
        builtInDef += "	prefer \"Prefer it as state over those having the default value";  
        builtInDef += " (also variables can be selected, which do not appear"; 
        builtInDef += " differentiated). \","; 
        builtInDef += " always \"Do use it as a state.\""; 
        builtInDef += ");\n"; 

        PrimitiveClassDecl pcd=null;
  
        try {
        
            ParserHandler ph = new ParserHandler();
            SourceRoot sr = ph.parseString(builtInDef,"");
            Program p = sr.getProgram();
            
            for (int i=0;i<p.getUnstructuredEntity(0).getNumElement();i++) {
            //log.debug("Program.getPredefinedTypeList(): Hepp!: " + i);
            FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(i));   
       
            if (!cd.name().equals("StateSelect")) {
            	pcd = new PrimitiveClassDecl(new PublicVisibilityType(),
                        new Opt(),
                        new Opt(),
                        cd.getRestriction(),
                        cd.getName(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        cd.getEquationList(),
                        cd.getAlgorithmList(),
                        cd.getSuperList(),
                        cd.getImportList(),
                        cd.getClassDeclList(),
                        cd.getComponentDeclList(),
                        cd.getAnnotationList(),
                        new Opt(),
                        cd.getEndDecl());
            	l.add(pcd); 
            } else {
            	pcd = new EnumClassDecl(new PublicVisibilityType(),
                        new Opt(),
                        new Opt(),
                        cd.getRestriction(),
                        cd.getName(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        new Opt(),
                        cd.getEquationList(),
                        cd.getAlgorithmList(),
                        cd.getSuperList(),
                        cd.getImportList(),
                        cd.getClassDeclList(),
                        cd.getComponentDeclList(),
                        cd.getAnnotationList(),
                        new Opt(),
                        cd.getEndDecl());
            	l.add(pcd); 	
            }
        }   
        
        } catch(Exception e){e.printStackTrace();}
            
        //log.debug("Program.getPredefinedTypeList(): "+l.getNumChild());
        
        return l;
        
    }
    
    /**
     * This attribute defines the NTA for obtaining a base enumeration
     * declaration, which is used to construct user defined enumeration
     * types.
     */
    syn lazy EnumClassDecl Program.getEnumBaseDecl() {

        // Build a string with a Modelica class corresponding to a basic
    	// enumeration
        String builtInDef= "type BaseEnum\n"; 
        builtInDef += "EnumType value;\n";
        builtInDef += "parameter StringType quantity=\"\";\n";
        builtInDef += "parameter EnumType min, max;\n";
        builtInDef += "parameter EnumType start;\n";
        builtInDef += "parameter BooleanType fixed=false;\n";
        builtInDef += "end BaseEnum;\n";
        
        EnumClassDecl ecd = null;
  
        try {
        
            ParserHandler ph = new ParserHandler();
            SourceRoot sr = ph.parseString(builtInDef,"");
            Program p = sr.getProgram();

            FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(0));   
            
            ecd = new EnumClassDecl(new PublicVisibilityType(),
                                   new Opt(),
                                   new Opt(),
                                   cd.getRestriction(),
                                   cd.getName(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   new Opt(),
                                   cd.getEquationList(),
                                   cd.getAlgorithmList(),
                                   cd.getSuperList(),
                                   cd.getImportList(),
                                   cd.getClassDeclList(),
                                   cd.getComponentDeclList(),
                                   cd.getAnnotationList(),
                                   new Opt(),
                                   cd.getEndDecl());
        
        
        } catch(Exception e){e.printStackTrace();}
            
        //log.debug("Program.getPredefinedTypeList(): "+l.getNumChild());
        
        return ecd;
        
    }
    
    /**
     * This attribute defines the NTA for predefined types
     * which contains a list of predefined PrimitiveClassDecl:s.
     */
    syn lazy List Program.getBuiltInFunctionList() {

        List l = new List();

        
        PrimitiveClassDecl pcd=null;
  
        try {
        
            String builtInFunc = 
"function initial end initial;\n"
+"function terminal end terminal;\n"
+"function smooth\n"
+"  input Integer p;\n"
+"  input Real expr;\n"   // Type not used
+"  output Real y;\n"     // Type not used
+"end smooth;\n"
+"function noEvent\n"
+"  input Real expr;\n"   // Type not used
+"  output Real y;\n"     // Type not used
+"end noEvent;\n"
+"function sample\n"
+"  input Real start;\n"
+"  input Real interval;\n"
+"  output Real y;\n"     // Type not used
+"end sample;\n"
+"function pre\n"
+"  input Real x;\n"      // Type not used
+"  output Real y;\n"     // Type not used
+"end pre;\n"
+"function edge end edge;\n"
+"function change end change;\n"
+"function reinit end reinit;\n"
+"function assert end assert;\n"
+"function terminate end terminate;\n"
+"function abs\n"
+"  input Real v;\n"      // Type not used
+"  output Real y;\n"     // Type not used
+"end abs;\n"
+"function sign end sign;\n"
+"function sqrt\n"
+"  input Real x;\n"
+"  output Real y;\n"
+"end sqrt;\n"
+"function div end div;\n"
+"function mod end mod;\n"
+"function rem end rem;\n"
+"function ceil end ceil;\n"
+"function floor end floor;\n"
+"function integer"
+"  input Real x;\n"
+"  output Integer y;\n"
+"end integer;\n"
+"function Integer"
+"  input Real x;\n"      // Type not used
+"  output Integer y;\n"
+"end Integer;\n"
+"//function String end String;\n"
+"function delay end delay;\n"
+"function cardinality end cardinality;\n"
+"function isPresent end isPresent;\n"
+"function semiLinear end semiLinear;\n"
+"function promote end promote;\n"
+"function ndims\n"
+"  input Real A;\n"      // Type not used
+"  output Real n;\n"
+"end ndims;\n"
+"function size\n"
+"  input Real A;\n"      // Type not used
+"  input Real d = 0;\n"  // Default value not used
+"  output Integer s;\n"
+"end size;\n"
+"function scalar end scalar;\n"
+"function vector end vector;\n"
+"function matrix end matrix;\n"
+"function transpose\n"
+"  input Real A;\n"    // Type not used
+"  output Real B;\n"   // Type not used
+"end transpose;\n"
+"function cross\n"
+"  input Real x[3];\n"
+"  input Real y[3];\n"
+"  output Real z[3];\n"   // Type not used
+"end cross;\n"
+"function outerProduct end outerProduct;\n"
+"function diagonal end diagonal;\n"
+"function identity\n"
+"  input Integer n;\n"
+"  output Integer a[n, n];\n"
+"end identity;\n"
+"function array end array;\n"
+"function zeros end zeros;\n"
+"function ones end ones;\n"
+"function fill\n"
+"  input Real s;\n"      // Type not used
+"  output Real o;\n"     // Type not used
+"end fill;\n"
+"function cat\n"
+"  input Real k;\n"      // Type not used
+"  output Real o;\n"     // Type not used
+"end cat;\n"
+"function linspace\n" 
+"  input Real x1;\n"
+"  input Real x2;\n"
+"  input Integer n;\n"
+"  output Real z[1];\n"   // Size not used
+"end linspace;\n"
+"function min\n"
+"  input Real x;\n"      // Type not used
+"  input Real y = 0;\n"  // Type and default value not used
+"  output Real z;\n"     // Type not used
+"end min;\n"
+"function max\n"
+"  input Real x;\n"      // Type not used
+"  input Real y = 0;\n"  // Type and default value not used
+"  output Real z;\n"     // Type not used
+"end max;\n"
+"function sum\n"
+"  input Real A;\n"      // Type not used
+"  output Real o;\n"     // Type not used
+"end sum;\n"
+"function product end product;\n"
+"function symmetric end symmetric;\n"
+"function skew end skew;\n"
+"function inStream\n"
+"  input Real v;\n"      // Type not used
+"  output Real o;\n"     // Type not used
+"end inStream;\n"
+"function actualStream\n"
+"  input Real n;\n"      // Type not used
+"  output Real o;\n"     // Type not used
+"end actualStream;\n"
+"function sin \"sine\"\n"
+"  input Modelica.SIunits.Angle u;\n"
+"  output Real y;\n"
+"end sin;\n"
+"function cos \"cosine\"" 
+"  input Modelica.SIunits.Angle u;\n"
+"  output Real y;\n"
+"end cos;\n"
+"function tan \"tangent (u shall not be -pi/2, pi/2, 3*pi/2, ...)\"\n"
+"  input Modelica.SIunits.Angle u;\n"
+"  output Real y;\n"
+"end tan;\n"
+"function asin \"inverse sine (-1 <= u <= 1)\"\n"
+"  input Real u;\n"
+"  output Modelica.SIunits.Angle y;\n"
+"end asin;\n"
+"function acos \"inverse cosine (-1 <= u <= 1)\"\n"
+"  input Real u;\n"
+"  output Modelica.SIunits.Angle y;\n"
+"end acos;\n"
+"function atan \"inverse tangent\"\n"
+"  input Real u;\n"
+"  output Modelica.SIunits.Angle y;\n"
+"end atan;\n"
+"function atan2 \"four quadrant inverse tangent\"\n"
+"  input Real u1;\n"
+"  input Real u2;\n"
+"  output Modelica.SIunits.Angle y;\n"
+"end atan2;\n"
+"function sinh \"hyperbolic sine\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end sinh;\n"
+"function cosh \"hyperbolic cosine\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end cosh;\n"
+"function tanh \"hyperbolic tangent\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end tanh;\n"
+"function exp \"exponential, base e\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end exp;\n"
+"function log \"natural (base e) logarithm (u shall be > 0)\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end log;\n"
+"function log10 \"base 10 logarithm (u shall be > 0)\"\n"
+"  input Real u;\n"
+"  output Real y;\n"
+"end log10;\n";

            ParserHandler ph = new ParserHandler();
            SourceRoot sr = ph.parseString(builtInFunc,"");

            Program p = sr.getProgram();
            
            ExternalClause builtin = new ExternalClause(new Opt(new ExternalLanguage("builtin")), new Opt(), new Opt(), new Opt());
            for (Element e : p.getUnstructuredEntity(0).getElements()) {
            	FullClassDecl fcd = (FullClassDecl) e;
            	String name = fcd.getName().getID();
            	if (name.equals("Integer") || name.equals("String")) {
            		// Add character that isn't allowed in identifiers to prevent collision with user class or the primitive class
            		name = "!" + name;
            		fcd.getName().setID(name);
            		fcd.setEndDecl(fcd.getEndDecl());
            	}
                fcd.setExternalClause(builtin.fullCopy());
                l.add(fcd);
            }
        
        } catch(Exception e){}
            
        
        
        return l;
        
    }


    /**
     * This attribute defines the NTA for predefined types
     * which contains a list of predefined BuiltInType:s.
     */
    syn lazy List Program.getBuiltInTypeList() {
        List l = new List();        
        l.add(new BuiltInClassDecl(new IdDecl("RealType")));    
        l.add(new BuiltInClassDecl(new IdDecl("IntegerType")));         
        l.add(new BuiltInClassDecl(new IdDecl("BooleanType")));         
        l.add(new BuiltInClassDecl(new IdDecl("StringType")));              
        l.add(new BuiltInClassDecl(new IdDecl("EnumType")));              
        return l;
    }
    
    public ClassDecl SourceRoot.doLookupBuiltInFunction(String name) {
        return unknownClassDecl();
    }
    
        
    inh ClassDecl Exp.lookupBuiltInFunction(String name);
    eq SourceRoot.getProgram().lookupBuiltInFunction(String name) = doLookupBuiltInFunction(name);
    eq Root.getChild().lookupBuiltInFunction(String name) = null;
    

    boolean PrimitiveClassDecl.rewritten = false;
    rewrite PrimitiveClassDecl {
        when (getName().getID().equals("Real") && !rewritten)
            to RealClassDecl {
                RealClassDecl rcd = new RealClassDecl(getVisibilityType(),
                                                    getEncapsulatedOpt(),
                                                    getPartialOpt(),
                                                    getRestriction(),
                                                    getName(),
                                                    getRedeclareOpt(),
                                                    getFinalOpt(),
                                                    getInnerOpt(),
                                                    getOuterOpt(),
                                                    getReplaceableOpt(),
                                                    getConstrainingClauseOpt(),
                                                    getConstrainingClauseCommentOpt(),
                                                    getStringCommentOpt(),
                                                    getEquationList(),
                                                    getAlgorithmList(),
                                                    getSuperList(),
                                                    getImportList(),
                                                    getClassDeclList(),
                                                    getComponentDeclList(),
                                                    getAnnotationList(),
                                                    getExternalClauseOpt(),
                                                    getEndDecl());
                rcd.rewritten = true;
                return rcd;
            }
    }
    
    rewrite PrimitiveClassDecl {
        when (getName().getID().equals("Integer") && !rewritten)
            to IntegerClassDecl {
                IntegerClassDecl rcd = new IntegerClassDecl(getVisibilityType(),
                                                    getEncapsulatedOpt(),
                                                    getPartialOpt(),
                                                    getRestriction(),
                                                    getName(),
                                                    getRedeclareOpt(),
                                                    getFinalOpt(),
                                                    getInnerOpt(),
                                                    getOuterOpt(),
                                                    getReplaceableOpt(),
                                                    getConstrainingClauseOpt(),
                                                    getConstrainingClauseCommentOpt(),
                                                    getStringCommentOpt(),
                                                    getEquationList(),
                                                    getAlgorithmList(),
                                                    getSuperList(),
                                                    getImportList(),
                                                    getClassDeclList(),
                                                    getComponentDeclList(),
                                                    getAnnotationList(),
                                                    getExternalClauseOpt(),
                                                    getEndDecl());
                rcd.rewritten = true;
                return rcd;
            }
    }
    
    rewrite PrimitiveClassDecl {
        when (getName().getID().equals("Boolean") && !rewritten)
            to BooleanClassDecl {
                BooleanClassDecl rcd = new BooleanClassDecl(getVisibilityType(),
                                                    getEncapsulatedOpt(),
                                                    getPartialOpt(),
                                                    getRestriction(),
                                                    getName(),
                                                    getRedeclareOpt(),
                                                    getFinalOpt(),
                                                    getInnerOpt(),
                                                    getOuterOpt(),
                                                    getReplaceableOpt(),
                                                    getConstrainingClauseOpt(),
                                                    getConstrainingClauseCommentOpt(),
                                                    getStringCommentOpt(),
                                                    getEquationList(),
                                                    getAlgorithmList(),
                                                    getSuperList(),
                                                    getImportList(),
                                                    getClassDeclList(),
                                                    getComponentDeclList(),
                                                    getAnnotationList(),
                                                    getExternalClauseOpt(),
                                                    getEndDecl());
                rcd.rewritten = true;
                return rcd;
            }
    }
    
        rewrite PrimitiveClassDecl {
        when (getName().getID().equals("String") && !rewritten)
            to StringClassDecl {
                StringClassDecl rcd = new StringClassDecl(getVisibilityType(),
                                                    getEncapsulatedOpt(),
                                                    getPartialOpt(),
                                                    getRestriction(),
                                                    getName(),
                                                    getRedeclareOpt(),
                                                    getFinalOpt(),
                                                    getInnerOpt(),
                                                    getOuterOpt(),
                                                    getReplaceableOpt(),
                                                    getConstrainingClauseOpt(),
                                                    getConstrainingClauseCommentOpt(),
                                                    getStringCommentOpt(),
                                                    getEquationList(),
                                                    getAlgorithmList(),
                                                    getSuperList(),
                                                    getImportList(),
                                                    getClassDeclList(),
                                                    getComponentDeclList(),
                                                    getAnnotationList(),
                                                    getExternalClauseOpt(),
                                                    getEndDecl());
                rcd.rewritten = true;
                return rcd;
            }
    }
}

aspect InstPredefinedTypes {

    syn lazy List InstProgramRoot.getInstPredefinedTypeList() {
        List l = new List();
        for (BaseClassDecl pcd : getProgram().getPredefinedTypes()) {
            l.add(pcd.newInstClassDecl());
        }
        return l;
    }

    syn lazy List InstProgramRoot.getInstBuiltInTypeList() {
        List l = new List();
        for (ClassDecl pcd : getProgram().getBuiltInTypes()) {
            l.add(pcd.newInstClassDecl());
        }
        return l;
    }

    syn lazy List InstProgramRoot.getInstBuiltInFunctionList() {
        List l = new List();
        for (BaseClassDecl bcd : getProgram().getBuiltInFunctions()) {
            l.add(bcd.newInstClassDecl());
        }
        return l;
    }
}

aspect AnnotationTypes {
    
}
