aspect IfEquationElimination {

    /*
     *  TODO: We shouldn't need this now that scalarization can do this anyway. 
     *        Just removing it and making obviously necessary changes causes lots of tests to fail. 
     */

    public class FClass {

        public class enableIfEquationElimination extends Transformation {

            /**
             * Eliminates unnecessary if equations; if a test can be determinedly evaluated to false or true
             * the functions in the if or else branche's block can be discarded or inlined respectively.
             * <p>
             * Depends on MakeReinitedVarsStates when invoked after scalarization.
             */
            @Override
            public void perform() {
                List<FAbstractEquation> newEquations = new List<FAbstractEquation>();
                List<FAbstractEquation> newInitialEquations = new List<FAbstractEquation>();
                List<FAbstractEquation> newParameterEquations = new List<FAbstractEquation>();

                for (FAbstractEquation equation : getFAbstractEquations()) {
                    equation.eliminateIfEquations(newEquations);
                }

                for (FAbstractEquation equation : getFInitialEquations()) {
                    equation.eliminateIfEquations(newInitialEquations);
                }

                for (FAbstractEquation equation : getFParameterEquations()) {
                    equation.eliminateIfEquations(newParameterEquations);
                }

                setFAbstractEquationList(newEquations);
                setFInitialEquationList(newInitialEquations);
                setFParameterEquationList(newParameterEquations);

                /*
                 * Remove orphaned temporaries.
                 */
                flushAllRecursive();
                removeUnusedTemporaries();
            }

        }

    }

    syn List<FAbstractEquation> FIfWhenElseEquation.myIfEliminatedEquations() {
        List<FAbstractEquation> equations = new List<FAbstractEquation>();
        for (FAbstractEquation equation : getFAbstractEquations()) {
            equation.eliminateIfEquations(equations);
        }
        return equations;
    }

    public void ASTNode.eliminateIfEquations(List<FAbstractEquation> equations) {}
    public void FAbstractEquation.eliminateIfEquations(List<FAbstractEquation> equations) { equations.add(this); }
    public FIfWhenElseEquation FIfWhenElseEquation.eliminateIfEquationsLocal(List<FAbstractEquation> equations,
            boolean falseBranchesOnly) { return this; }

    /**
     * Eliminates unnecessary if equations.
     * 
     * @param equations
     *          The list in which to put equations that are extracted from live branches.
     */
    public void FIfWhenEquation.eliminateIfEquations(List<FAbstractEquation> equations) {
        FIfWhenElseEquation eqn = eliminateIfEquationsLocal(equations, false);
        if (eqn != null) {
            equations.add(eqn);
        }
    }

    /**
     * Eliminates unnecessary if equations.
     * <p>
     * If an equation's test can be evaluated to {@code true}, it's block equations are added to the list and
     * this method finishes. If it is evaluated to {@code false}, the block equations are disregarded.
     * <p>
     * If the test can not be evaluated, the branch is kept and remaining branches are kept, unless they can be
     * evaluated to false, in which case they are disregarded. Remaining live branches are kept but their
     * block equations are not added yet, since <i>the first</i> {@code true} test's equations should be added,
     * and there are preceding branches that could not be evaluated.
     * This is specified with the {@code falseBranchesOnly} flag.
     * 
     * @param equations
     *          The list in which to put equations that are extracted from live branches.
     * @param falseBranchesOnly
     *          A flag specifying whether or not to only eliminate dead branches and ignore live branches.
     *          The flag is set to true as soon as a test could not be evaluated.
     * @return
     *          this branch if it could not be evaluated, {@code null} otherwise. If non-{@code null} this equation is
     *          added back to the {@code equations} list by {@link #eliminateIfEquations(List<FAbstractEquation>)}.
     */
    public FIfWhenElseEquation FIfEquation.eliminateIfEquationsLocal(List<FAbstractEquation> equations,
            boolean falseBranchesOnly) {

        if (!getTest().isConstantExp()) {
            if (hasElse()) {
                FIfWhenElseEquation elseEquation = getElse().eliminateIfEquationsLocal(equations, true);
                if (elseEquation == null) {
                    setElseOpt(new Opt<FIfWhenElseEquation>());
                } else {
                    setElse(elseEquation);
                }
            }
            return this;
        }

        try {
            if (getTest().ceval().booleanValue() && !falseBranchesOnly) {
                equations.addAll(myIfEliminatedEquations());
                return null;
            } else if (hasElse()) {
                return getElse().eliminateIfEquationsLocal(equations, falseBranchesOnly);
            }
        } catch (ConstantEvaluationException e) {
            /*
             * Test failed to evaluate to boolean constant. In this case we do nothing.
             */
            return this;
        }
        return null;
    }

    /**
     * Eliminates the else branch, if it was determined to be the live branch.
     * <p>
     * If-equation-elimination only reaches this method in two cases: either all previous branches were determinedly
     * false (meaning that {@code falseBranchesOnly} is still false) in which case the block equations are added
     * to {@code equations}. In the other case not all branches could be evaluated, and this branch needs to be kept.
     * 
     * @param equations
     *          The list in which to put equations that are extracted from true branches.
     * @param falseBranchesOnly
     *          A flag specifying whether or not to only eliminate dead branches and ignore
     *          live branches. The flag is set to true as soon as a test could not be evaluated.
     */
    public FIfWhenElseEquation FElseEquation.eliminateIfEquationsLocal(List<FAbstractEquation> equations,
            boolean falseBranchesOnly) {

        if (falseBranchesOnly) {
            return this;
        }

        equations.addAll(myIfEliminatedEquations());
        return null;
    }

    /**
     * Replaces an {@link FIfExp} with either branch if its test can be evaluated.
     * 
     * @param equations
     *          The equation list where to put equations that are extracted from {@code true} branches.
     *          Note that this is not used for this method.
     */
    public void FIfExp.eliminateIfEquations(List<FAbstractEquation> equations) {
        try {
            if (getIfExp().isConstantExp()) {
                replaceMe(getIfExp().ceval().booleanValue() ? getThenExp() : getElseExp());
            }
        } catch (ConstantEvaluationException e) {
            /*
             * Test failed to evaluate to boolean constant. In this case we do nothing.
             */
        }
    }

}