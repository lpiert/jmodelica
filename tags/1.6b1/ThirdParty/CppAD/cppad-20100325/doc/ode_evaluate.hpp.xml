<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Source: ode_evaluate</title>
<meta name="description" id="description" content="Source: ode_evaluate"/>
<meta name="keywords" id="keywords" content=" ode_evaluate source "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_ode_evaluate.hpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="ode_evaluate.cpp.xml" target="_top">Prev</a>
</td><td><a href="sparse_evaluate.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_utility</option>
<option>ode_evaluate</option>
<option>ode_evaluate.hpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed_utility-&gt;</option>
<option>uniform_01</option>
<option>det_of_minor</option>
<option>det_by_minor</option>
<option>det_by_lu</option>
<option>det_33</option>
<option>det_grad_33</option>
<option>ode_evaluate</option>
<option>sparse_evaluate</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>ode_evaluate-&gt;</option>
<option>ode_evaluate.cpp</option>
<option>ode_evaluate.hpp</option>
</select>
</td>
<td>ode_evaluate.hpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>Source: ode_evaluate</big></big></b></center>
<code><font color="blue"># ifndef CPPAD_ODE_EVALUATE_INCLUDED 
<code><span style='white-space: nowrap'><br/>
</span></code># define CPPAD_ODE_EVALUATE_INCLUDED 

<pre style='display:inline'> 
# include &lt;cppad/vector.hpp&gt;
# include &lt;cppad/ode_err_control.hpp&gt;
# include &lt;cppad/runge_45.hpp&gt;

namespace CppAD {  // BEGIN CppAD namespace

template &lt;class Float&gt;
class ode_evaluate_fun {
private:
	const size_t m_;
	const CppAD::vector&lt;Float&gt; x_;
public:
	ode_evaluate_fun(size_t m, const CppAD::vector&lt;Float&gt; &amp;x) 
	: m_(m), x_(x)
	{ }
	void Ode(
		const Float                  &amp;t, 
		const CppAD::vector&lt;Float&gt;   &amp;z, 
		CppAD::vector&lt;Float&gt;         &amp;h) 
	{
		if( m_ == 0 )
			ode_y(t, z, h);
		if( m_ == 1 )
			ode_z(t, z, h);
	}
	void ode_y(
		const Float                  &amp;t, 
		const CppAD::vector&lt;Float&gt;   &amp;y, 
		CppAD::vector&lt;Float&gt;         &amp;g) 
	{	// y_t = g(t, x, y)
		CPPAD_ASSERT_UNKNOWN( y.size() == x_.size() );

		size_t i;
		size_t n = x_.size();
		for(i = 0; i &lt; n; i++)
			g[i]  = x_[i] * y[i];
		// because y_i(0) = 1, solution for this equation is
		// y_0 (t) = t
		// y_1 (t) = exp(x_1 * t)
		// y_2 (t) = exp(2 * x_2 * t)
		// ...
	}
	void ode_z(
		const Float                  &amp;t , 
		const CppAD::vector&lt;Float&gt;   &amp;z , 
		CppAD::vector&lt;Float&gt;         &amp;h ) 
	{	// z    = [ y ; y_x ]
		// z_t  = h(t, x, z) = [ y_t , y_x_t ]
		size_t i, j;
		size_t n = x_.size();
		CPPAD_ASSERT_UNKNOWN( z.size() == n + n * n );

		// y_t
		for(i = 0; i &lt; n; i++)
		{	h[i] = x_[i] * z[i];

			// initialize y_x_t as zero
			for(j = 0; j &lt; n; j++)
				h[n + i * n + j] = 0.;
		}
		for(i = 0; i &lt; n; i++)
		{	// partial of g_i w.r.t y_i
			Float gi_yi = x_[i]; 
			// partial of g_i w.r.t x_i
			Float gi_xi = z[i];
			// partial of y_i w.r.t x_i
			Float yi_xi = z[n + i * n + i];
			// derivative of yi_xi with respect to t 
			h[n + i * n + i] = gi_xi + gi_yi * yi_xi;
		}
	}
};

template &lt;class Float&gt;
class ode_evaluate_method {
private:
	ode_evaluate_fun&lt;Float&gt; F;
public:
	// constructor
	ode_evaluate_method(size_t m, const CppAD::vector&lt;Float&gt; &amp;x) 
	: F(m, x)
	{ }
	void step(
		Float                 ta ,
		Float                 tb ,
		CppAD::vector&lt;Float&gt; &amp;xa ,
		CppAD::vector&lt;Float&gt; &amp;xb ,
		CppAD::vector&lt;Float&gt; &amp;eb )
	{	xb = CppAD::Runge45(F, 1, ta, tb, xa, eb);
	}
	size_t order(void)
	{	return 4; }
};


template &lt;class Float&gt;
void ode_evaluate(
	CppAD::vector&lt;Float&gt; &amp;x  , 
	size_t m                 , 
	CppAD::vector&lt;Float&gt; &amp;fm )
{
	typedef CppAD::vector&lt;Float&gt; Vector;

	size_t n = x.size();
	size_t ell;
	CPPAD_ASSERT_KNOWN( m == 0 || m == 1,
		&quot;ode_evaluate: m is not zero or one&quot;
	);
	CPPAD_ASSERT_KNOWN( 
		((m==0) &amp; (fm.size()==n)) || ((m==1) &amp; (fm.size()==n*n)),
		&quot;ode_evaluate: the size of fm is not correct&quot;
	);
	if( m == 0 )
		ell = n;
	else	ell = n + n * n;

	// set up the case we are integrating
	Float  ti   = 0.;
	Float  tf   = 1.;
	Float  smin = 1e-5;
	Float smax  = 1.;
	Float scur  = 1.;
	Float erel  = 0.;
	vector&lt;Float&gt; yi(ell), eabs(ell);
	size_t i, j;
	for(i = 0; i &lt; ell; i++)
	{	eabs[i] = 1e-10;
		if( i &lt; n )
			yi[i] = 1.;
		else	yi[i]  = 0.;
	}

	// return values
	Vector yf(ell), ef(ell), maxabs(ell);
	size_t nstep;

	// construct ode method for taking one step
	ode_evaluate_method&lt;Float&gt; method(m, x);

	// solve differential equation
	yf = OdeErrControl(method, 
		ti, tf, yi, smin, smax, scur, eabs, erel, ef, maxabs, nstep);

	if( m == 0 )
	{	for(i = 0; i &lt; n; i++)
			fm[i] = yf[i];
	}
	else
	{	for(i = 0; i &lt; n; i++)
			for(j = 0; j &lt; n; j++)
				fm[i * n + j] = yf[n + i * n + j];
	}
	return;
}

} // END CppAD namespace</pre>

# endif
</font></code>


<hr/>Input File: omh/ode_evaluate.omh

</body>
</html>
