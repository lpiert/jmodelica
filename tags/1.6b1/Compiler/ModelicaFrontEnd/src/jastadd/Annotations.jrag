/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.NoSuchElementException;

import org.jmodelica.util.ChainedIterator;

aspect AnnotationAPI {
	
	/**
	 * \brief Get the annotation node for this AST node's annotation, if any.
	 * 
	 * This should be overridden for all nodes that can have annotations.
	 */
	syn AnnotationNode ASTNode.annotation() = AnnotationNode.NO_ANNOTATION;
	
	/**
	 * \brief Get the annotation node for a sub-node of this AST node's annotation, if any.
	 * 
	 * Path is interpreted as a "/"-separated list of names of nested annotations.
	 * 
	 * Example:
	 * <code>annotation(A(B(C = "foo")));</code>
	 * Here the annotation given by the path <code>"A/B/C"</code> has the value <code>"foo"</code>.
	 */
	syn AnnotationNode ASTNode.annotation(String path) = annotation().forPath(path);
	
	eq Opt.annotation()  = (getNumChild() > 0) ? getChild(0).annotation() : AnnotationNode.NO_ANNOTATION;
	eq List.annotation() = (getNumChild() > 0) ? getChild(0).annotation() : AnnotationNode.NO_ANNOTATION;
	
	eq Comment.annotation()         = getAnnotationOpt().annotation();
	eq ParseAnnotation.annotation() = getClassModification().annotationNode();
	
	eq ExternalClause.annotation() = getAnnotation1Opt().annotation();
	eq InstExternal.annotation()   = getExternalClause().annotation();
	
	eq FullClassDecl.annotation()           = getAnnotations().annotation();
	eq InstClassDecl.annotation()           = getClassDecl().annotation();
	eq ShortClassDecl.annotation()          = getExtendsClauseShortClass().annotation();
	eq ExtendsClauseShortClass.annotation() = getComment().annotation();
	eq InstShortClassDecl.annotation()      = getClassDecl().annotation();
	
	eq ComponentDecl.annotation()     = getComment().annotation();
	eq InstComponentDecl.annotation() = getComponentDecl().annotation();
	
	
	/**
	 * Get the annotation node that represents this node when used as an annotation, if 
	 * applicable.
	 */
	syn AnnotationNode ASTNode.annotationNode()            = AnnotationNode.NO_ANNOTATION;
	syn lazy AnnotationNode Modification.annotationNode()  = AnnotationNode.NO_ANNOTATION;
	eq ClassModification.annotationNode()                  = new AnnotationNode.CMAnnotationNode(this);
	eq ValueModification.annotationNode()                  = new AnnotationNode.VMAnnotationNode(this);
	eq CompleteModification.annotationNode()               = getClassModification().annotationNode();
	eq ComponentModification.annotationNode()              = 
		hasModification() ? getModification().annotationNode() : AnnotationNode.NO_ANNOTATION;
	syn lazy AnnotationNode Exp.annotationNode()           = new AnnotationNode.EAnnotationNode(this);
	eq FunctionCall.annotationNode()                       = new AnnotationNode.FCAnnotationNode(this);
	syn lazy AnnotationNode NamedArgument.annotationNode() = new AnnotationNode.NAAnnotationNode(this);
	
	/**
	 * \brief Describes a node in the tree formed by an annotation.
	 */
	public abstract class AnnotationNode implements Iterable<AnnotationNode> {
		
		/**
		 * \brief Represents an annotation that does not exist.
		 */
		public static final AnnotationNode NO_ANNOTATION = new NullAnnotationNode();
		
		/**
		 * \brief Finds an annotation node at the given path below this one.
		 */
		public AnnotationNode forPath(String path) {
			return forPath(path.split("/"), 0);
		}
		
		/**
		 * \brief Internal definition of {@link #forPath(String)}.
		 * 
		 * @param path  the path elements to find
		 * @param i     the first index in <code>path</code> to use
		 */
		protected AnnotationNode forPath(String[] path, int i) {
			if (i >= path.length)
				return this;
			return NO_ANNOTATION;
		}
		
		/**
		 * Check if this annotation node represents a value node.
		 */
		public boolean isValue() {
			return false;
		}
		
		/**
		 * \brief Checks if this annotation node represents an existing annotation.
		 */
		public boolean exists() {
			return true;
		}
		
		/**
		 * Get the name associated with this annotation node, if any.
		 * 
		 * In general, any node reachable with a call to forPath() will have a name.
		 * 
		 * @return the name or <code>null</code>, if no name is available
		 */
		public String name() {
			return null;
		}
		
		/**
		 * Iterate over this node's child annotation nodes.
		 */
		public Iterator<AnnotationNode> iterator() {
			return NO_ANNOTATION.iterator();
		}
		
		/**
		 * \brief Returns the value for a node that represents a string value.
		 */
		public String string() {
			return null;
		}
		
		/**
		 * Returns the value for a node that represents a string value, interpreted as 
		 * a path of an URI (with protocol file or modelica). A simple path is also 
		 * supported, and is interpreted relative to the top containing package, or if 
		 * that does not exist, relative to the directory containing the current file.
		 */
		public String path() {
			return ast().uri2path(string());
		}
		
		/**
		 * \brief Returns the value for a node that represents a list of strings.
		 */
		public ArrayList<String> stringList() {
			return null;
		}
		
		/**
		 * \brief Returns the value for a node that represents a list of strings, 
		 *        or a single string value.
		 */
		public ArrayList<String> asStringList() {
			ArrayList<String> res = stringList();
			if (res == null) {
				String str = string();
				if (str != null) {
					res = new ArrayList(1);
					res.add(str);
				}
			}
			return res;
		}
		
		/**
		 * \brief Returns the value for a node that represents a real value.
		 */
		public double real() {
			return 0.0;
		}
		
		/**
		 * \brief Returns the value for a node that represents a vector of real values.
		 */
		public double[] realVector() {
			return null;
		}
		
		/**
		 * \brief Returns the value for a node that represents a matrix of real values.
		 */
		public double[][] realMatrix() {
			return null;
		}
		
		/**
		 * Returns the value for a node that represents a boolean value.
		 */
		public boolean bool() {
			return false;
		}
		
		/**
		 * Returns the ast node that this annotation node is connected to.
		 */
		protected abstract ASTNode ast();
		
		/**
		 * Iterates over the annotation nodes representing the nodes in the list.
		 */
		protected static class AnnotationIterator implements Iterator<AnnotationNode> {
			private Iterator<? extends ASTNode> it;
			private AnnotationNode next;
			
			public AnnotationIterator(Iterator<? extends ASTNode> it) {
				this.it = it;
				update();
			}
			
			public AnnotationIterator(Iterable<? extends ASTNode> list) {
				this(list.iterator());
			}
			
			public boolean hasNext() {
				return next.exists();
			}
			
			public AnnotationNode next() {
				if (!hasNext())
					throw new NoSuchElementException();
				AnnotationNode res = next;
				update();
				return res;
			}
			
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
			private void update() {
				next = NO_ANNOTATION;
				while (it.hasNext() && !next.exists()) 
					next = it.next().annotationNode();
			}
		}
		
		/**
		 * \brief Represents a non-existing annotation.
		 */
		private static class NullAnnotationNode extends AnnotationNode {
			private static final Iterator<AnnotationNode> EMPTY_ITERATOR = 
				new ArrayList<AnnotationNode>().iterator();
			
			public Iterator<AnnotationNode> iterator() {
				return EMPTY_ITERATOR;
			}
			
			public boolean exists() {
				return false;
			}
			
			public String path() {
				return null;
			}

			protected ASTNode ast() {
				return null;
			}
			
		}
		
		/**
		 * \brief Represents an annotation that is represented in the source tree by a ClassModification.
		 */
		public static class CMAnnotationNode extends AnnotationNode {
			
			private ClassModification mod;
			
			public CMAnnotationNode(ClassModification cm) {
				mod = cm;
			}

			public Iterator<AnnotationNode> iterator() {
				return new AnnotationIterator(mod.getArguments());
			}
			
			public String name() {
				return mod.annotationName();
			}
			
			protected AnnotationNode forPath(String[] path, int i) {
				if (i >= path.length)
					return this;
				for (Argument arg : mod.getArguments()) 
					if (arg.matches(path[i])) 
						return arg.annotationNode().forPath(path, i+1);
				return NO_ANNOTATION;
			}

			protected ASTNode ast() {
				return mod;
			}

		}
		
		/**
		 * \brief Represents an annotation that is represented in the source tree by a FunctionCall.
		 */
		public static class FCAnnotationNode extends AnnotationNode {
			
			private FunctionCall call;
			
			public FCAnnotationNode(FunctionCall fc) {
				call = fc;
			}

			public Iterator<AnnotationNode> iterator() {
				return new AnnotationIterator(new ChainedIterator(
						call.getFunctionArguments().getExps().iterator(), 
						call.getFunctionArguments().getNamedArguments().iterator()));
			}
			
			public String name() {
				return call.getName().name();
			}
			
			protected AnnotationNode forPath(String[] path, int i) {
				if (i >= path.length)
					return this;
				for (NamedArgument arg : call.getFunctionArguments().getNamedArguments()) 
					if (arg.matches(path[i])) 
						return arg.annotationNode().forPath(path, i+1);
				return NO_ANNOTATION;
			}
			
			protected ASTNode ast() {
				return call;
			}
			
		}
		
		protected static abstract class ExpAnnotationNode extends AnnotationNode {
			
			protected abstract Exp exp();
			
			public String string() {
				return exp().avalueString();
			}

			public boolean isValue() {
				return !exp().isAnnotationExp();
			}
			
			public ArrayList<String> stringList() {
				return exp().avalueStringList();
			}

			public Iterator<AnnotationNode> iterator() {
				return isValue() ? super.iterator() : new AnnotationIterator(exp().annotationList());
			}
			
			public double real() {
				return exp().avalueReal();
			}
			
			public double[] realVector() {
				return exp().avalueRealVector();
			}
			
			public double[][] realMatrix() {
				return exp().avalueRealMatrix();
			}

			public boolean bool() {
				return exp().avalueBool();
			}
			
			protected ASTNode ast() {
				return exp();
			}

		}
		
		/**
		 * \brief Represents an annotation that is represented in the source tree by a ValueModification.
		 */
		public static class VMAnnotationNode extends ExpAnnotationNode {
			
			private ValueModification mod;
			
			public VMAnnotationNode(ValueModification vm) {
				mod = vm;
			}
			
			protected Exp exp() {
				return mod.getExp();
			}
			
			public String name() {
				return mod.annotationName();
			}
			
		}
		
		/**
		 * \brief Represents an annotation that is represented in the source tree by a NamedArgument.
		 */
		public static class NAAnnotationNode extends ExpAnnotationNode {
			
			private NamedArgument arg;
			
			public NAAnnotationNode(NamedArgument na) {
				arg = na;
			}
			
			protected Exp exp() {
				return arg.getExp();
			}
			
			public String name() {
				return arg.getName().name();
			}
			
		}
		
		/**
		 * \brief Represents an annotation that is represented in the source tree by an Exp.
		 */
		public static class EAnnotationNode extends ExpAnnotationNode {
			
			private Exp e;
			
			public EAnnotationNode(Exp exp) {
				e = exp;
			}
			
			protected Exp exp() {
				return e;
			}
			
		}
		
	}
	
	syn boolean Argument.matches(String str) = false;
	eq NamedModification.matches(String str) = getName().name().equals(str);
	
	syn boolean NamedArgument.matches(String str) = getName().name().equals(str);
	
	inh String Modification.annotationName();
	eq NamedModification.getChild().annotationName()    = getName().name();
	eq CompleteModification.getChild().annotationName() = annotationName();
	eq BaseNode.getChild().annotationName()             = null;
	
	syn String Exp.avalueString()  = null;
	eq StringLitExp.avalueString() = unEscape();
	eq AccessExp.avalueString()    = getAccess().name();
	
	syn ArrayList<String> Exp.avalueStringList() = null;
	eq ArrayConstructor.avalueStringList() {
		ArrayList<String> l = new ArrayList<String>(getFunctionArguments().getNumExp());
		for (Exp e : getFunctionArguments().getExps())
			l.add(e.avalueString());
		return l.contains(null) ? null : l;
	}
	
	syn boolean Exp.avalueBool()      = false;
	eq BooleanLitExpTrue.avalueBool() = true;
	
	syn double Exp.avalueReal()   = 0.0;
	eq IntegerLitExp.avalueReal() = (double) Integer.parseInt(getUNSIGNED_INTEGER());
	eq RealLitExp.avalueReal()    = Double.parseDouble(getUNSIGNED_NUMBER());
	eq NegExp.avalueReal()        = -getExp().avalueReal();
	
	syn double[] Exp.avalueRealVector() = null;
	eq ArrayConstructor.avalueRealVector() {
		if (adim() != 1)
			return null;
		double[] res = new double[getFunctionArguments().getNumExp()];
		int i = 0;
		for (Exp e : getFunctionArguments().getExps())
			res[i++] = e.avalueReal();
		return res;
	}
	
	syn double[][] Exp.avalueRealMatrix() = null;
	eq ArrayConstructor.avalueRealMatrix() {
		if (adim() != 2)
			return null;
		double[][] res = new double[getFunctionArguments().getNumExp()][];
		int i = 0;
		for (Exp e : getFunctionArguments().getExps())
			res[i++] = e.avalueRealVector();
		return res;
	}
	
	syn int Exp.adim()         = 0;
	eq ArrayConstructor.adim() = 
		getFunctionArguments().getNumExp() > 0 ? getFunctionArguments().getExp(0).adim() + 1 : 1;
		
	syn boolean Exp.isFunctionCall() = false;
	eq FunctionCall.isFunctionCall() = true;
	
	syn boolean Exp.isAnnotationExp()     = false;
	eq FunctionCall.isAnnotationExp()     = true;
	eq ArrayConstructor.isAnnotationExp() = 
		getFunctionArguments().getNumExp() > 0 && getFunctionArguments().getExp(0).isFunctionCall();
		
	syn Iterable<? extends ASTNode> Exp.annotationList() = new ArrayList<ASTNode>(0);
	eq ArrayConstructor.annotationList()                 = getFunctionArguments().getExps();
	eq FunctionCall.annotationList()                     = Collections.singletonList(this);
	
}

aspect TestAnnotations {
/*
	rewrite ParseAnnotation {
		when (testCases().size() > 0)
			to AUnitTesting {
				Collection<FunctionCall> h = testCases();
				AUnitTesting ut = new AUnitTesting();
				for (Iterator<FunctionCall> iter = h.iterator(); iter.hasNext();) {
				    FunctionCall f = iter.next();
					FunctionArguments fa = f.getFunctionArguments();
				    if (f.name().equals("FlatteningTestCase")) {
						AFlatteningTestCase tc = new AFlatteningTestCase();
						//for (int i=0;i<fa.getNumExp();i++) {
						//	tc.setChild(((StringLitExp)fa.getExp(i)).getSTRING(),i);
						//}
						for (int i=0;i<fa.getNumNamedArgument();i++) {
							if (fa.getNamedArgument(i).getName().name().equals("name")) {
								log.debug("### rewrite ParseAnnotation: name found");
								//tc.setName(((StringLitExp)fa.getNamedArgument(i).getExp()).getSTRING());
								tc.setName((StringLitExp)((StringLitExp)fa.getNamedArgument(i).getExp()).fullCopy());
							} else if (fa.getNamedArgument(i).getName().name().equals("description")) {
								log.debug("### rewrite ParseAnnotation: description found");
								//tc.setDescription(((StringLitExp)fa.getNamedArgument(i).getExp()).getSTRING());
								tc.setDescription((StringLitExp)((StringLitExp)fa.getNamedArgument(i).getExp()).fullCopy());
							} else if (fa.getNamedArgument(i).getName().name().equals("flatModel")) {
								log.debug("### rewrite ParseAnnotation: flatModel found");
								//tc.setFlatModel(((StringLitExp)fa.getNamedArgument(i).getExp()).getSTRING());	
								tc.setFlatModel((StringLitExp)((StringLitExp)fa.getNamedArgument(i).getExp()).fullCopy());	
							}
						}
						//log.debug("### rewrite ParseAnnotation: check name: "+tc.getName());
						//log.debug("### rewrite ParseAnnotation: check description: "+tc.getDescription());
						//log.debug("### rewrite ParseAnnotation: check flatModel: "+tc.getFlatModel());

						//tc.dumpTreeBasic("");
						ut.addATestCase(tc);	
					}
					
				}
			return ut;
			}
	
	}

	coll Collection<FunctionCall> Annotation.testCases() [new LinkedHashSet<FunctionCall>()] with add root ParseAnnotation;
	 
	FunctionCall contributes
 	   this when (hasTestCaseName())
	to Annotation.testCases() for myAnnotation();

	inh Annotation ASTNode.myAnnotation();
	eq Annotation.getChild().myAnnotation() = this;
	eq FClass.getChild().myAnnotation() = null;
	eq Root.getChild().myAnnotation() = null;

	syn boolean FunctionCall.hasTestCaseName() = 
		(name().equals("FlatteningTestCase") || name().equals("ErrorTestCase"));
	
	syn String AFlatteningTestCase.name() = getName().getSTRING();
	syn String AFlatteningTestCase.description() = getDescription().getSTRING();
	syn String AFlatteningTestCase.flatModel() = getFlatModel().getSTRING();
	
    public FlatteningTestCase AFlatteningTestCase.createFlatteningTestCase() {
       return new FlatteningTestCase(name(),description(),root().fileName(),enclosingClassDecl().qualifiedName(),flatModel(),false);
    }

*/
}
