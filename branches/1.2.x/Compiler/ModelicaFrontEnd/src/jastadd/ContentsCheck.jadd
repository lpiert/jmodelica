/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ContentCheck {

	/**
	 * \brief Check for code that is not allowed in its current context.
	 * 
	 * Examples would be checking that classes follow the requirements of 
	 * their restriction. 
	 */
	public void ASTNode.contentCheck() {}
	
	public void FAbstractEquation.contentCheck() {
		if (inFunction()) 
			error("Equations are not allowed in functions");
	}
	
	public void FAlgorithmBlock.contentCheck() {}
	
	public void InstDerExp.contentCheck() {
		if (!(getFExp() instanceof FInstAccessExp))
			error("Expressions within der() not supported");
	}
	
	public void InstExternal.contentCheck() {
		if (!inFunction())
			error("External function declarations are only allowed in functions");
	}
	
	public void FReturnStmt.contentCheck() {
		if (!inFunction())
			error("Return statements are only allowed in functions");
	}
	
	public void FWhenStmt.contentCheck() {
		if (inFunction())
			error("When statements are not allowed in functions");
		else if (insideBlockStmt())
			error("When statements are not allowed inside if, for, while and when clauses");
	}
	
	public void InstForIndex.contentCheck() {
		if (!hasFExp())
			error("For index without in expression isn't supported");
	}
	
	public void FParseArray.contentCheck() {
		// If not already rewritten to FLongArray, this is an error.
		error("The array() operator may not be used in function call equations or function call statements");
	}
	
	public void FEndExp.contentCheck() {
		if (!inArraySubscripts())
			error("The end operator may only be used in array subscripts");
	}
	
	// TODO: check if this is a builtin function (with bad arguments), error otherwise
//	public void FBuiltinExternalLanguage.contentCheck() {
//		error("The \"builtin\" external language specitication may only be used for functions that are a part of the Modelica specification");
//	}
	
	public void FUnknownExternalLanguage.contentCheck() {
		error("The external language specification \"" + getLanguage() + "\" is not supported");
	}
	
	inh boolean FIterExp.iterExpUseOK();
	eq FExp.getChild().iterExpUseOK()       = false;
	eq Root.getChild().iterExpUseOK()       = false;
	eq InstNode.getChild().iterExpUseOK()   = false;
	eq FArray.getChild().iterExpUseOK()     = true;
	eq FMinMaxExp.getChild().iterExpUseOK() = true;
	eq FSumExp.getChild().iterExpUseOK()    = true;
	// TODO: Add product() when it is implemented
	
	public void FIterExp.contentCheck() {
		if (!iterExpUseOK())
			error("Reduction-expressions are only allowed with sum(), min(), max() and product()");
	}
	
}