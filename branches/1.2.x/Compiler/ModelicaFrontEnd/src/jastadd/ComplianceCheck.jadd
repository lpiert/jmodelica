/*
    Copyright (C) 2010 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ComplianceCheck {

	/**
	 * \brief Check for code that is not allowed in its current context.
	 * 
	 * Examples would be checking that classes follow the requirements of 
	 * their restriction. 
	 */
	public void ASTNode.complianceCheck() {}
	
	public void FIfWhenClause.complianceCheck() {
		super.complianceCheck();
		if (!getTest().variability().lessOrEqual(fParameter()))
			warning("Using if statements is currently not supported when compiling with CppAD, and can give erroneous results");
	}
	
	public void FWhileStmt.complianceCheck() {
		super.complianceCheck();
		if (!getTest().variability().lessOrEqual(fParameter()))
			warning("Using while statements is currently not supported when compiling with CppAD, and can give erroneous results");
	}
	
	public void InstAccess.complianceCheck() {
		super.complianceCheck();
		if (myInstComponentDecl().isRecord() && hasFArraySubscripts() && inFunction() && 
				!getFArraySubscripts().variability().lessOrEqual(fParameter())) {
			warning("Using arrays of records with indices of higher than parameter variability is currently not supported when compiling with CppAD");
		}
	}
	
	public void InstExternal.complianceCheck() {
		super.complianceCheck();
		if (!hasFExternalLanguage() || !getFExternalLanguage().supportedLanguage())
			compliance("External functions are not supported");
	}
	
	syn boolean FExternalLanguage.supportedLanguage() = false;
	eq FBuiltinExternalLanguage.supportedLanguage()   = true;
	eq FUnknownExternalLanguage.supportedLanguage()   = true;  // Avoid duplicate errors

	public void FUnsupportedEquation.collectErrors() {
		compliance("Unsupported equation type");
	}

	public void FUnsupportedExp.collectErrors() {
		compliance("Unsupported expression type");
	}
	
	public void FWhenStmt.complianceCheck() {
		super.complianceCheck();
		compliance("When statements are not supported");
	}
	
	public void FUnsupportedBuiltIn.complianceCheck() {
		super.complianceCheck();
		compliance("The " + getName() + "() function-like operator is not supported");
	}
	
	public void FDotAddExp.complianceCheck() {
		super.complianceCheck();
		if (type().isString())
			compliance("String concatenation is not supported");
	}
	
	public void InstPrimitive.complianceCheck() {
		super.complianceCheck();
		if (isString())
			compliance("String variables are not supported");
		if (isInteger() && !variability().lessOrEqual(fParameter()))
			compliance("Integer variables are not supported, only constants and parameters");
		if (isBoolean() && !variability().lessOrEqual(fParameter()))
			compliance("Boolean variables are not supported, only constants and parameters");
		if (size().isEmpty())
			compliance("Array variables with dimensions of length 0 are not supported");
	}

	public void InstComponentDecl.complianceCheck() {
		super.complianceCheck();
		if (hasConditionalAttribute())
			compliance("Conditional components are not supported");
	}
	
	public void FExp.complianceCheck() {
		super.complianceCheck();
		if (size().isEmpty()) {
			for (FExp ch : childFExps())
				if (ch.size().isEmpty())
					return;
			compliance("Empty arrays are not supported");
		}
	}
	
	public void InstNamedModification.complianceCheck() {
		if (getName().hasFArraySubscripts())
			compliance("Modifiers of specific array elements are not supported");
	}
	
}