/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Arrays;
import java.util.TreeSet;

aspect FlatTypeAnalysis {

	syn lazy FType AbstractFVariable.type();
	eq FRealVariable.type()       = fRealType(size());
	eq FIntegerVariable.type()    = fIntegerType(size());
	eq FBooleanVariable.type()    = fBooleanType(size());
	eq FStringVariable.type()     = fStringType(size());
	eq FRecordVariable.type()     = getType();
	eq FDerivativeVariable.type() = fRealScalarType();
	eq FFunctionVariable.type()   = getType();
	eq UnknownFVariable.type()    = fUnknownType();
	
	syn FType FRecordVariable.getType() = myFRecordDecl().type().sizedType(size());
	
	syn FType InstComponentModification.type() {
		if (getName().myInstComponentDecl().isAssignable()) {
			InstAssignable ip = ((InstAssignable)getName().myInstComponentDecl());
			return ip.type();
		} else {
			return fUnknownType();
		}
	}
	
	syn lazy FType InstAssignable.type();
	
	eq InstPrimitive.type() {
		FType scalar = fUnknownType();
		if (isReal()) 
			scalar = fRealScalarType();
		else if (isInteger()) 
			scalar = fIntegerScalarType();
		else if (isBoolean()) 
			scalar = fBooleanScalarType();
		else if (isString()) 
			scalar = fStringScalarType();
		return isArray() ? scalar.arrayType(size()) : scalar;
	}
	
	// TODO: Reduce code duplication here
	syn lazy FRecordType FRecordDecl.type() {
		TreeSet<FRecordComponentType> comps = new TreeSet<FRecordComponentType>();
		for (FVariable fv : getFVariables()) 
			comps.add(new FRecordComponentType(fv.name(), (FType) fv.type().fullCopy()));
		FRecordType type = new FRecordType(Size.SCALAR, name(), new List());
		for (FRecordComponentType comp : comps)
			type.addComponent(comp);
		return type;
	}
	
	eq InstRecord.type() {
		TreeSet<FRecordComponentType> comps = new TreeSet<FRecordComponentType>();
		InstComponentDecl root = isArray() ? getInstComponentDecl(0) : this;
		for (InstComponentDecl icd : root.getInstComponentDecls()) 
			comps.add(new FRecordComponentType(icd.name(), (FType) icd.type().fullCopy()));
		FRecordType type = new FRecordType(size(), myInstClassDecl().qualifiedName(), new List());
		for (FRecordComponentType comp : comps)
			type.addComponent(comp);
		return type;
	}
	
	syn lazy FRecordType InstClassDecl.recordType() {
		TreeSet<FRecordComponentType> comps = new TreeSet<FRecordComponentType>();
		for (InstComponentDecl icd : getInstComponentDecls()) 
			comps.add(new FRecordComponentType(icd.name(), (FType) icd.type().fullCopy()));
		FRecordType type = new FRecordType(Size.SCALAR, qualifiedName(), new List());
		for (FRecordComponentType comp : comps)
			type.addComponent(comp);
		return type;
	}
	
	public class FRecordComponentType implements Comparable<FRecordComponentType> {
		public int compareTo(FRecordComponentType other) {
			return getName().compareTo(other.getName());
		}
	}
	
	syn lazy FType FExp.type() = fUnknownType();
	
	eq FUnaryExp.type()   = getFExp().type();
	eq FArtmBinExp.type() = getLeft().type().looseNumericPromotion(getRight().type());
	eq FSubExp.type()     = getLeft().type().numericPromotion(getRight().type());
	eq FNegExp.type()     = getFExp().type().isNumeric() ? getFExp().type() : fUnknownType();
	
	eq FDotAddExp.type() {
		FType t = rawType();
		return t.hasAdd() ? t : fUnknownType();
	}
	
	syn FType FDotAddExp.rawType() = getLeft().type().looseTypePromotion(getRight().type());
	eq FAddExp.rawType()           = getLeft().type().typePromotion(getRight().type());
	
	eq FNoEventExp.type() = getFExp().type();
	
	eq FDivExp.type() {
		if (getRight().type().isScalar())
			return super.type();
		else
			return fUnknownType();
	}
	
	eq FMulExp.type() {
		if (ndims() < 0)
			return fUnknownType();
		if (isElementWise())
			return super.type();
		Size left = getLeft().size();
		Size right = getRight().size();
		if (!left.equivalentDim(right, false, left.ndims() - 1, 0))
			return fUnknownType();
		if (left.ndims() > 2 || right.ndims() > 2)
			return fUnknownType();
		FType res = getLeft().type().scalarNumericPromotion(getRight().type());
		if (left.ndims() == 1 && right.ndims() == 1) 
			return res;
		else
			return res.arrayType(size());
	}
	
	eq FPowExp.type() {
		if (getRight().type().isUnknown() || getRight().ndims() != 0)
			return fUnknownType();
		if (getLeft().ndims() == 0)
			return super.type();
		// Left operand is not scalar, must be matrix exponentiation
		if (ndims() != 2 || size().get(0) != size().get(1))
			return fUnknownType();
		if (!getRight().type().isInteger() || !getRight().isConstantExp())
			return fUnknownType();
		if (getRight().ceval().intValue() < 0)
			return fUnknownType();
		return getLeft().type();
	}
	
	eq FIntegerExp.type() = fIntegerScalarType();
	
	syn FType FRecordConstructor.type() = getType();
	syn FType FRecordConstructor.getType() = getRecord().recordType();
	
	syn FRecordType FIdUse.recordType() = myFRecordDecl().type();
	eq FIdUseInstAccess.recordType()    = getInstAccess().myInstClassDecl().recordType();
	
	eq FTranspose.type() = 
		getFExp().ndims() < 2 ? 
			fUnknownType() : 
			getFExp().type().arrayType(size());
	
	eq FCross.type() = getX().type().numericPromotion(getY().type());
	
	eq FAbsExp.type() = getFExp().type();
	
	eq FNdimsExp.type() = fIntegerScalarType();
	
	eq FSizeExp.type() {
		if (hasDim()) {
			if (!getDim().variability().lessOrEqual(fParameter()))
				return fUnknownType();
			if (!getDim().type().isScalar() || !getDim().type().isInteger())
				return fUnknownType();
			int dim = dimension();
			if (dim < 0 || dim > getFExp().ndims() - 1)
				return fUnknownType();
			return fIntegerScalarType();
		} else {
			return fIntegerArrayType(size());
		}
	}
	
	eq FMinMaxExp.type() = 
		hasY() ? 
			getX().type().scalarType().typePromotion(getY().type().scalarType()) : 
			getX().type().scalarType();
	
	eq FRangeExp.type() {
		FType tot = fIntegerScalarType();
		for (FExp e : getFExps()) {
			if (!e.type().isScalar() || !e.type().isNumeric())
				return fUnknownType();
			tot = tot.scalarNumericPromotion(e.type());
		}
		return tot.arrayType(size());
	}
	
	eq FLinspace.type() {
		if (!getN().variability().lessOrEqual(fParameter()) || getN().ceval().intValue() < 2)
			return fUnknownType();
		return fRealArrayType(size());
	}
	
	eq FRelExp.type() {
		FType left  = getLeft().type();
		if (left.equivalentTo(getRight().type()) && left.isScalar())
			return fBooleanScalarType();
		else
			return fUnknownType();
	}
	eq FEqRelExp.type() {
		if ((getLeft().type().isReal() || getRight().type().isReal()) && !inFunction())
			return fUnknownType();
		return super.type();
	}
	eq FLogBinExp.type() {
		FType left = getLeft().type();
		if (left.typeCompatible(getRight().type()) && left.isBoolean())
			return left;
		else
			return fUnknownType();
	}
	eq FNotExp.type() = getFExp().type().isBoolean() ? getFExp().type() : fUnknownType();
	
	eq FIfExp.type() {
		if (!getIfExp().type().isBoolean() || !getIfExp().type().isScalar())
			return fUnknownType();
		FType tot = getThenExp().type();
		boolean func = inFunction();
		for (FElseIfExp e : getFElseIfExps())
			tot = tot.typePromotion(e.type(), func);
		return tot.typePromotion(getElseExp().type(), func);
	}
	
	eq FElseIfExp.type() {
		if (!getIfExp().type().isBoolean() || !getIfExp().type().isScalar())
			return fUnknownType();
		return getThenExp().type();
	}

	eq FRealLitExp.type() = fRealScalarType();
	eq FIntegerLitExp.type() = fIntegerScalarType();
	eq FBooleanLitExp.type() = fBooleanScalarType();
	eq FStringLitExp.type() = fStringScalarType();
	eq FTimeExp.type() = fRealScalarType();
	eq FDerExp.type() = fRealScalarType();

	// TODO: expand to handle boolean and enum index
	eq FEndExp.type() = fIntegerScalarType();
	
	/**
	 * \brief Check if this FArray is on the form "{exp for i in exp}".
	 */
	syn boolean FArray.isIterArray() = getNumFExp() == 1 && getFExp(0).isIterExp();
	
	/**
	 * \brief Check if this FExp is on the form "exp for i in exp".
	 */
	syn boolean FExp.isIterExp() = false;
	eq FIterExp.isIterExp()      = true;
	
	/**
	 * \brief Returns the expression being iterated over in an FIterExp child. 
	 *        Only valid if {@link #isIterExp()} returns <code>true</code>.
	 */
	public FExp FSumExp.iterExp() { return ((FIterExp) getFExp()).getFExp(); }
	
	eq FArray.type() {
		if (isIterArray())
			return getFExp(0).type();
		
		// Check that the types of the elements are consistent
		FType t = getFExp(0).type();
		boolean func = inFunction();
		for (int i = 1; i < getNumFExp(); i++) {
			if (!t.equivalentTo(getFExp(i).type(), func)) 
				return fUnknownType();
			if (t.isNumeric()) 
				t = t.numericPromotion(getFExp(i).type(), func);
		}
		return t.arrayType(size());
	}
	
	eq FAbstractCat.type() {
		if (!dimensionIsOk())
			return fUnknownType();
		int dim = dimension();
		
		FType t = getFExp(0).type().scalarType();
		Size s = getFExp(0).size().promote(ndimsForArg(0));
		boolean func = inFunction();
		for (int i = 1; i < getNumFExp(); i++) {
			// Check that the types of the elements are consistent
			FType t2 = getFExp(i).type().scalarType();
			if (!t.equivalentTo(t2, func)) 
				return fUnknownType();
			if (t.isNumeric()) 
				t = t.numericPromotion(t2, func);
			
			// Check that the sizes are consistent
			if (!s.equivalentExcept(getFExp(i).size().promote(ndimsForArg(i)), func, dim))
				return fUnknownType();
		}
		return t.arrayType(size());
	}
	
	syn int FAbstractCat.ndimsForArg(int i) = ndims();
	eq FCatExp.ndimsForArg(int i)           = getFExp(i).ndims();
	
	eq FSumExp.type() {
		if (getFExp().isIterExp())
			return iterExp().type();
		return getFExp().type().scalarType();
	}
	
	eq FIterExp.type() {
		if (size().numElements() == 0)
			return fUnknownType();
		return getFExp().type().arrayType(size());
	}
	
	eq FIdentity.type() {
		if (!getFExp().variability().lessOrEqual(fParameter()))
			return fUnknownType();
		return fIntegerArrayType(size());
	}

	eq FInfArgsFunctionCall.type() = (size().ndims() > 0) ? fIntegerArrayType(size()) : fUnknownType();
	eq FFillExp.type()             = (size().ndims() > 0) ? getFillExp().type().arrayType(size()) : fUnknownType();
	
	eq FIdUseExp.type() = getFIdUse().type();
	eq FInstAccessExp.type() = getInstAccess().type();
	
	syn lazy FType FIdUse.type() = myFV().type();
	eq FIdUseInstAccess.type() = getInstAccess().type();

	syn lazy FType InstAccess.type() {
		if (myInstComponentDecl().isAssignable()) {
			InstAssignable ip = (InstAssignable) myInstComponentDecl();
			FType t = ip.type().scalarType();
			return isArray() ? t.arrayType(size()) : t.scalarType();
		} else {
			return fUnknownType();
		}
	}
	
	inh FType FFunctionCallLeft.type();
	eq FFunctionCallEquation.getLeft(int i).type() = getCall().typeOfOutput(i);
	eq FFunctionCallStmt.getLeft(int i).type()     = getCall().typeOfOutput(i);
	
	// TODO: Can't anything be inferred here?
	syn lazy FType InstComponentDecl.type() = fUnknownType();
	
	syn FType FAbstractFunctionCall.typeOfOutput(int i) = (i == 0) ? type() : fUnknownType();
	syn lazy FType InstFunctionCall.typeOfOutput(int i) = myOutputs().get(i).type().sizedType(sizeOfOutput(i));
	syn lazy FType FFunctionCall.typeOfOutput(int i)    = myOutputs().get(i).type().sizedType(sizeOfOutput(i));

	eq FFunctionCall.type()    = hasOutputs() ? typeOfOutput(0) : fUnknownType();
	eq InstFunctionCall.type() = hasOutputs() ? typeOfOutput(0) : fUnknownType();
	eq FMathematicalFunctionCall.type() = fRealScalarType();		
	
	
	// TODO: expand to handle boolean end enum index
	syn lazy FType FSubscript.type();
	eq FColonSubscript.type() = fIntegerArrayType(size());
	eq FExpSubscript.type()   = getFExp().type();
}

aspect InstBindingType {
	
	/**
	 * \brief Returns the type the binding expression should have.
	 */
	syn FType InstAssignable.bindingType() {
		InstNode decl = myInstValueMod().myInstNode();
		if (myBindingExpHasEach() || decl == this) 
			return type();
		else
			return expandBindingType(type(), decl);
	}
		
	/**
	 * \brief Constructs the type a specific attribute to this instance tree node should 
	 *        have, given the scalar type of the attribute. 
	 *        
	 * Default is <code>null</code>, so override is necessary for all InstNode 
	 * subclasses that needs this check.
	 */
	syn FType InstNode.attributeType(InstComponentModification icm) = null;
		
	/**
	 * \brief Constructs the type a specific attribute to this primitive should 
	 *        have, given the scalar type of the attribute.
	 */
	eq InstAssignable.attributeType(InstComponentModification icm) {
		// TODO: This is only used to get the size of the type - perhaps it should return Size instead
		FType res = icm.type();
		InstNode decl = icm.myInstNode();
		if (decl == this)
			return res.arrayType(size());  // TODO: Take care of scalars as well
		if (icm.getEach() || !enclosedBy(decl)) 
			return res;
		else
			return expandBindingType(res.arrayType(size()), decl);
	}
	
	/**
	 * \brief Add array dimensions for surrounding InstArrayComponentDecls to 
	 *        type for binding expression.
	 */
	inh FType InstAssignable.expandBindingType(FType type, InstNode node);
	inh FType InstArrayComponentDecl.expandBindingType(FType type, InstNode node);
	inh FType InstComposite.expandBindingType(FType type, InstNode node);
	
	eq InstClassDecl.getChild().expandBindingType(FType type, InstNode node) = type;
	eq InstRoot.getChild().expandBindingType(FType type, InstNode node)      = type;
	eq FlatRoot.getChild().expandBindingType(FType type, InstNode node)      = type;
	eq InstArrayComponentDecl.getChild().expandBindingType(FType type, InstNode node) = 
		expandBindingType(type.enclosingArrayType(childFSubscript(0).numIndices()), node);
	eq InstComposite.getChild().expandBindingType(FType type, InstNode node) = 
		(node == this) ? type : expandBindingType(type, node);
	eq InstRecord.getChild().expandBindingType(FType type, InstNode node) = 
		(node == this) ? type : expandBindingType(type, node);
	
	inh boolean InstAssignable.inArrayComponent();
	eq InstClassDecl.getChild().inArrayComponent()          = false;
	eq InstRoot.getChild().inArrayComponent()               = false;
	eq FlatRoot.getChild().inArrayComponent()               = false;
	eq InstArrayComponentDecl.getChild().inArrayComponent() = true;
	
	/**
	 * \brief Add an enclosing dimension to type. 
	 * 
	 * Returns a FType that is a copy of this one, but with one 
	 * more dimension, added first. If this type is scalar, an array type is 
	 * created instead.
	 */
	syn FType FType.enclosingArrayType(int size) = arrayType(size().expand(size));
	eq FUnknownType.enclosingArrayType(int size) = this;
	
}

aspect TypePromotion {
	
	/**
	 * \brief Combine two types to the broadest common denominator.
	 * 
	 * If types are not compatible, the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 */
	syn FType FType.typePromotion(FType type) = typePromotion(type, false);

	/**
	 * \brief Combine two types to the broadest common denominator.
	 * 
	 * If scalar types are not compatible or if both are array and sizes 
	 * doesn't match, the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 */
	syn FType FType.looseTypePromotion(FType type) = looseTypePromotion(type, false);
	
	/**
	 * \brief Combine two types to the broadest common numeric denominator.
	 * 
	 * If either is non-numeric or if sizes doesn't match, 
	 * the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 */
	syn FType FType.numericPromotion(FType type) = numericPromotion(type, false);
	
	/**
	 * \brief Combine two types to the broadest common numeric denominator.
	 * 
	 * If either is non-numeric or if both are array and sizes doesn't match, 
	 * the unknown type is returned. 
	 * An integer type combined with a real type results in a real type.
	 */
	syn FType FType.looseNumericPromotion(FType type) = looseNumericPromotion(type, false);
	
	/**
	 * \brief Combine two types to the broadest common scalar numeric denominator.
	 * 
	 * If either is non-numeric, the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 */
	syn FType FType.scalarNumericPromotion(FType type) = fUnknownType();
	
	/**
	 * \brief Combine two types to the broadest common denominator.
	 * 
	 * If types are not compatible, the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 * If <code>allowUnknown</code> is <code>true</code>, then unknown 
	 * lengths are considered equal to any length.
	 */
	syn FType FType.typePromotion(FType type, boolean allowUnknown) {
		if (!equivalentTo(type, allowUnknown))
			return fUnknownType();
		return isNumeric() ? numericPromotion(type, allowUnknown) : this;
	}

	/**
	 * \brief Combine two types to the broadest common denominator.
	 * 
	 * If scalar types are not compatible or if both are array and sizes 
	 * doesn't match, the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 * If <code>allowUnknown</code> is <code>true</code>, then unknown 
	 * lengths are considered equal to any length.
	 */
	syn FType FType.looseTypePromotion(FType type, boolean allowUnknown) {
		if (isScalar() || type.isScalar() || dimensionCompatible(type, allowUnknown)) {
			FType scalar = scalarType().typePromotion(type.scalarType());
			return scalar.sizedType(isScalar() ? type.size() : size());
		} else {
			return fUnknownType();
		}
	}
	
	/**
	 * \brief Combine two types to the broadest common numeric denominator.
	 * 
	 * If either is non-numeric or if sizes doesn't match, 
	 * the unknown type is returned.
	 * An integer type combined with a real type results in a real type.
	 * If <code>allowUnknown</code> is <code>true</code>, then unknown 
	 * lengths are considered equal to any length.
	 */
	syn FType FType.numericPromotion(FType type, boolean allowUnknown) = 
		fUnknownType();
	
	/**
	 * \brief Combine two types to the broadest common numeric denominator.
	 * 
	 * If either is non-numeric or if both are array and sizes doesn't match, 
	 * the unknown type is returned. 
	 * An integer type combined with a real type results in a real type.
	 * If <code>allowUnknown</code> is <code>true</code>, then unknown 
	 * lengths are considered equal to any length.
	 */
	syn FType FType.looseNumericPromotion(FType type, boolean allowUnknown) = 
		fUnknownType();
	

	eq FPrimitiveNumericType.numericPromotion(FType type, boolean allowUnknown) {
		if (dimensionCompatible(type, allowUnknown)) {
			return scalarNumericPromotion(type).arrayType(size());
		} else {
			return fUnknownType();
		}
	}

	eq FPrimitiveNumericType.looseNumericPromotion(FType type, boolean allowUnknown) {
		if (isScalar() || type.isScalar() || dimensionCompatible(type, allowUnknown)) {
			return scalarNumericPromotion(type).sizedType(isScalar() ? type.size() : size());
		} else {
			return fUnknownType();
		}
	}

	eq FPrimitiveNumericType.scalarNumericPromotion(FType type) {
		if (type.isNumeric()) 
			return isReal() ? scalarType() : type.scalarType(); 
		else 
			return fUnknownType();
	}

}

aspect FTypeCompatibility {
	
	syn boolean FType.typeCompatible(FType type) = typeCompatible(type, false);

	syn boolean FType.typeCompatible(FType type, boolean allowUnknown) = false;
	eq FRealType.typeCompatible(FType type, boolean allowUnknown) = 
		(type.isReal() || type.isInteger()) && dimensionCompatible(type, allowUnknown);
	eq FIntegerType.typeCompatible(FType type, boolean allowUnknown) = 
		type.isInteger() && dimensionCompatible(type, allowUnknown);
	eq FBooleanType.typeCompatible(FType type, boolean allowUnknown) = 
		type.isBoolean() && dimensionCompatible(type, allowUnknown);
	eq FStringType.typeCompatible(FType type, boolean allowUnknown) = 
		type.isString() && dimensionCompatible(type, allowUnknown);
	eq FRecordType.typeCompatible(FType type, boolean allowUnknown) = 
		typeMatches(type, true, allowUnknown);
	
	syn boolean FType.typeMatches(FType type, boolean compatible, boolean allowUnknown) =
		compatible ? typeCompatible(type, allowUnknown) : equivalentTo(type, allowUnknown);
	eq FRecordType.typeMatches(FType type, boolean compatible, boolean allowUnknown) {
		if (!dimensionCompatible(type, allowUnknown) || !type.isRecord())
			return false;
		FRecordType rec = (FRecordType) type;
		if (getName().equals(rec.getName())) 
			return true;
		if (getNumComponent() != rec.getNumComponent())
			return false;
		for (int i = 0; i < getNumComponent(); i++) {
			FRecordComponentType c1 = getComponent(i);
			FRecordComponentType c2 = rec.getComponent(i);
			if (!c1.getName().equals(c2.getName()) || 
					!c1.getFType().typeMatches(c2.getFType(), compatible, allowUnknown))
				return false;
		}
		return true;
	}
	
	syn boolean FType.dimensionCompatible(FType type) = dimensionCompatible(type, false);
	syn boolean FType.dimensionCompatible(FType type, boolean allowUnknown) = 
		size().equivalent(type.size(), allowUnknown);
	
}

aspect FTypeEquivalent {
	/* The function equivalentTo is used in equation type checking where equations
	 * like
	 * x=0
	 * where x is declared as Real is ok.
	 */
	syn boolean FType.equivalentTo(FType type) = equivalentTo(type, false);

	syn boolean FType.equivalentTo(FType type, boolean allowUnknown) = false;
	eq FPrimitiveNumericType.equivalentTo(FType type, boolean allowUnknown) = 
		type.isNumeric() && dimensionCompatible(type, allowUnknown);
	eq FBooleanType.equivalentTo(FType type, boolean allowUnknown) = 
		type.isBoolean() && dimensionCompatible(type, allowUnknown);
	eq FStringType.equivalentTo(FType type, boolean allowUnknown) = 
		type.isString() && dimensionCompatible(type, allowUnknown);
	eq FRecordType.equivalentTo(FType type, boolean allowUnknown) = 
		typeMatches(type, false, allowUnknown);
 
}

aspect BuiltInFlatTypes {
 	
 	syn boolean InstPrimitive.isReal() = 
 		myInstClass().finalClass().primitiveName().equals("Real");
 	syn boolean InstPrimitive.isInteger() = 
 		myInstClass().finalClass().primitiveName().equals("Integer");
 	syn boolean InstPrimitive.isBoolean() = 
 		myInstClass().finalClass().primitiveName().equals("Boolean");
 	syn boolean InstPrimitive.isString() = 
 		myInstClass().finalClass().primitiveName().equals("String");

 	syn boolean InstBuiltIn.isReal() = 
 		myInstClass().finalClass().primitiveName().equals("RealType");
 	syn boolean InstBuiltIn.isInteger() = 
 		myInstClass().finalClass().primitiveName().equals("IntegerType");
 	syn boolean InstBuiltIn.isBoolean() = 
 		myInstClass().finalClass().primitiveName().equals("BooleanType");
 	syn boolean InstBuiltIn.isString() = 
 		myInstClass().finalClass().primitiveName().equals("StringType");

	 
	public static final FRealType    FRealType.SCALAR    = new FRealType(Size.SCALAR);
	public static final FIntegerType FIntegerType.SCALAR = new FIntegerType(Size.SCALAR);
	public static final FBooleanType FBooleanType.SCALAR = new FBooleanType(Size.SCALAR);
	public static final FStringType  FStringType.SCALAR  = new FStringType(Size.SCALAR);
	public static final FUnknownType FUnknownType.SCALAR = new FUnknownType(Size.SCALAR);

	syn boolean FType.isReal()    = false;
	eq FRealType.isReal()         = true;
	syn boolean FType.isInteger() = false;
	eq FIntegerType.isInteger()   = true;
	syn boolean FType.isBoolean() = false;
	eq FBooleanType.isBoolean()   = true;
	syn boolean FType.isString()  = false;
	eq FStringType.isString()     = true;
	
	syn boolean FType.isArray()  = getSize() != Size.SCALAR;
	syn boolean FType.isScalar() = getSize() == Size.SCALAR;
	
	syn boolean FType.isNumeric()        = false;
	eq FPrimitiveNumericType.isNumeric() = true;
	
	syn boolean FType.isPrimitive() = false;
	eq FPrimitiveType.isPrimitive() = true;
	syn boolean FType.isRecord()    = false;
	eq FRecordType.isRecord()       = true;
	
	syn boolean FType.hasAdd()        = false;
	eq FPrimitiveNumericType.hasAdd() = true;
	eq FStringType.hasAdd()           = true;

	syn int FType.ndims() = getSize().ndims();
 	
	syn Size FType.size() = getSize();
 	
	syn FType FType.sizedType(Size s) = 
		(s == Size.SCALAR) ? scalarType() : arrayType(s);
 	
	syn FType FType.scalarType() = FUnknownType.SCALAR;
	eq FRealType.scalarType()    = FRealType.SCALAR;
	eq FIntegerType.scalarType() = FIntegerType.SCALAR;
	eq FBooleanType.scalarType() = FBooleanType.SCALAR;
	eq FStringType.scalarType()  = FStringType.SCALAR;
  	eq FRecordType.scalarType() {
  		if (isScalar())
  			return this;
  		FRecordType copy = (FRecordType) fullCopy();
  		copy.setSize(Size.SCALAR);
  		return copy;
  	}
 	
	syn boolean FType.isUnknown() = false;
 	eq FUnknownType.isUnknown() = true;
 	
	syn FPrimitiveType ASTNode.fRealScalarType()    = FRealType.SCALAR;
	syn FPrimitiveType ASTNode.fIntegerScalarType() = FIntegerType.SCALAR;
	syn FPrimitiveType ASTNode.fBooleanScalarType() = FBooleanType.SCALAR;
	syn FPrimitiveType ASTNode.fStringScalarType()  = FStringType.SCALAR;
	syn FPrimitiveType ASTNode.fUnknownType()       = FUnknownType.SCALAR;
 	
	syn FPrimitiveType ASTNode.fRealArrayType(Size size)    = new FRealType(size);
	syn FPrimitiveType ASTNode.fIntegerArrayType(Size size) = new FIntegerType(size);
	syn FPrimitiveType ASTNode.fBooleanArrayType(Size size) = new FBooleanType(size);
	syn FPrimitiveType ASTNode.fStringArrayType(Size size)  = new FStringType(size);
 	
	syn FPrimitiveType ASTNode.fRealType(Size size) = 
 		(size == Size.SCALAR) ? fRealScalarType() : fRealArrayType(size);
	syn FPrimitiveType ASTNode.fIntegerType(Size size) = 
 		(size == Size.SCALAR) ? fIntegerScalarType() : fIntegerArrayType(size);
	syn FPrimitiveType ASTNode.fBooleanType(Size size) = 
 		(size == Size.SCALAR) ? fBooleanScalarType() : fBooleanArrayType(size);
	syn FPrimitiveType ASTNode.fStringType(Size size) = 
 		(size == Size.SCALAR) ? fStringScalarType() : fStringArrayType(size);
 	
 	syn FType FType.arrayType(Size size);
 	eq FUnknownType.arrayType(Size size)  = this;
	eq FRealType.arrayType(Size size)     = fRealArrayType(size);
 	eq FIntegerType.arrayType(Size size)  = fIntegerArrayType(size);
 	eq FStringType.arrayType(Size size)   = fStringArrayType(size);
 	eq FBooleanType.arrayType(Size size)  = fBooleanArrayType(size);
  	eq FRecordType.arrayType(Size size) {
  		FRecordType copy = (FRecordType) fullCopy();
  		copy.setSize(size);
  		return copy;
  	}
 	
 	/**
 	 * \brief Create a literal with the zero value for the given type.
 	 */
 	public FExp FType.zeroLiteral()        { return null; }
 	public FExp FRealType.zeroLiteral()    { return new FRealLitExp(0.0); }
 	public FExp FIntegerType.zeroLiteral() { return new FIntegerLitExp(0); }
 	public FExp FStringType.zeroLiteral()  { return new FStringLitExp(""); }
 	public FExp FBooleanType.zeroLiteral() { return new FBooleanLitExpFalse(); }

}
 
aspect InstClassRestriction {
	
	/**
	 * \brief Is this class a function?
	 */
	syn boolean InstClassDecl.isFunction() = false;
	eq InstBaseClassDecl.isFunction()      = getInstRestriction().isFunction();
	eq UnknownInstClassDecl.isFunction()   = false;
	
	/**
	 * \brief Is this restriction "function"?
	 */
	syn boolean InstRestriction.isFunction() = false;
	eq InstFunction.isFunction() = true;
	
	/**
	 * \brief Is this class a record?
	 */
	syn boolean InstClassDecl.isRecord() = false;
	eq InstBaseClassDecl.isRecord()      = getInstRestriction().isRecord();
	eq UnknownInstClassDecl.isRecord()   = false;
	
	/**
	 * \brief Is this restriction "record"?
	 */
	syn boolean InstRestriction.isRecord() = false;
	eq InstMRecord.isRecord() = true;
	
	/**
	 * \brief Is this class a function or record?
	 */
	syn boolean InstClassDecl.isCallable() = false;
	eq InstBaseClassDecl.isCallable()      = getInstRestriction().isCallable();
	eq UnknownInstClassDecl.isCallable()   = false;
	
	/**
	 * \brief Is this restriction "function" or "record"?
	 */
	syn boolean InstRestriction.isCallable() = false;
	eq InstFunction.isCallable() = true;
	eq InstMRecord.isCallable()  = true;
	
}
