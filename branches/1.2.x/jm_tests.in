#!/bin/bash
# Script for running JModelica.org test suites.
# Copy this file to somewhere in your PATH.
# You may need to change variables below (in the copy) to match your system.
# Type "tests -h" for usage.
# Requres Unix-like system with bash, grep, dc, mktemp, uname and sed.

# ====== Variables to set ======

# Memory allocation for the ant process
ANT_OPTS="-Xmx512m"

# Default tests - run if no tests are chosen
# Possible values: modelica, optimica, python or combination (e.g. "modelica optimica")
DEFAULT_TESTS="optimica python"

# Default arguments - always parse these arguments before command line
DEFAULT_ARGS=""

# You may need to change this - only needed on some systems
PYTHON_HOME=/opt/python/python2.5/

# Variables set by configure script
JMODELICA_HOME=@prefix@
JMODELICA_SRC=@abs_top_srcdir@
IPOPT_HOME=@IPOPT_HOME@
SUNDIALS_HOME=@SUNDIALS_HOME@
ECLIPSE_HOME=@ECLIPSE_HOME@
BUILD=@abs_builddir@
if [[ "${JAVA_HOME}" == "" ]]; then
  JAVA_HOME=@_JAVA_HOME_@
fi

# Variables that are relative to others
CPPAD_HOME=${JMODELICA_HOME}/ThirdParty/CppAD/
PYTHONPATH=${PYTHON_HOME}:${JMODELICA_HOME}/Python/

# ====== End variables to set ======

# Update library path
if [[ "${IPOPT_HOME}" != "" ]]; then
  LIB_PATH=${IPOPT_HOME}/lib
  if [[ "${SUNDIALS_HOME}" != "" ]]; then
    LIB_PATH=${LIB_PATH}:${SUNDIALS_HOME}/lib
  fi
  if [[ "${LD_LIBRARY_PATH}" == "" ]]; then
    LD_LIBRARY_PATH=/lib:${LIB_PATH}
  else
    LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LIB_PATH}
  fi
fi

# Test sets
tests="modelica optimica python"
test_set_a="modelica optimica python"
test_set_j="modelica optimica"
test_set_m="modelica"
test_set_o="optimica"
test_set_p="python"

# Functions for running specific tests
function modelica_tests() {
  junit_test Modelica
}

function optimica_tests() {
  junit_test Optimica
}

function python_tests() {
  TEST_DIR=$(mktemp -dqt)
  TESTS="$(find ${JMODELICA_HOME}/Python/jmodelica/tests -name \*.py | grep test_)"
  PATTERN=----------------------------------------------------------------------
  TAGS="-a stddist"
  if [[ "${IPOPT_HOME}" != "" ]]; then
    TAGS="${TAGS} -a ipopt"
    if [[ "${SUNDIALS_HOME}" != "" ]]; then
      TAGS="${TAGS} -a assimulo"
    fi
  fi
  print_name Python
  case ${OUTPUT} in
    v)
      if build_jmodelica; then
        cd ${TEST_DIR}
        for TEST in ${TESTS}; do
	  echo ${TEST} | sed -e 's!^.*jmodelica.tests.!Running !'
          nosetests ${TAGS} -v ${TEST}
        done
      fi
      ;;
    q)
      if build_jmodelica 2> /dev/null > /dev/null; then
        cd ${TEST_DIR}
        for TEST in ${TESTS}; do
          nosetests ${TAGS} -v ${TEST} 2>&1 \
	    | sed -n -e 's!^OK$!PASSED!ip' -e 's!^FAILED !TEST FAILED!p'
        done | filter_python_separate
      else
        echo BUILD FAILED
      fi
      ;;
    n)
      echo Building...
      if build_jmodelica > /dev/null; then
        cd ${TEST_DIR}
        for TEST in ${TESTS}; do
          echo ${TEST} | sed -e 's!^.*jmodelica.tests.!Running !'
          nosetests ${TAGS} -v ${TEST} 2>&1 \
	    | egrep -v '^Exception .* ignored$' \
	    | grep -B1 -A500 -e${PATTERN} \
	    | sed -e 's!^OK$!PASSED!' -e 's!^FAILED !TEST FAILED!'
        done | filter_python_separate
      fi
      ;;
  esac
  cd ..
  rm -rf ${TEST_DIR}
}

# Helper functions
function print_name() {
  if [[ "${OUTPUT}" == q ]]; then
    echo -n "$1... "
  else
    echo ======= $1 =======
  fi
}

function filter_python_separate() {
  TMP=$(mktemp -qt) 
  tee ${TMP} | egrep -v "(PASSED|TEST FAILED)"
  cat ${TMP} \
  | sed -n \
        -e '1 i\
            0 0' \
        -e 's!^TEST FAILED(errors=\([0-9]*\))!\1+!p' \
        -e 's!^TEST FAILED(failures=\([0-9]*\))!r\1+r!p' \
        -e 's!^TEST FAILED(errors=\([0-9]*\), failures=\([0-9]*\))!\1+r\2+r!p' \
        -e '$ a\
            [errors=]nn[, failures=]np' \
  | dc \
  | sed -e 's!errors=0, failures=0!PASSED!' \
	-e 's!\(errors=.*, failures=.*\)!TEST FAILED (\1)!'
  rm -f ${TMP}
}
  
function junit_test() {
  print_name $1
  cd ${JMODELICA_SRC}/Compiler/$1Compiler
  case ${OUTPUT} in
    v)
      ant_test
      ;;
    q)
      ant_test 2>&1 \
        | egrep "(\[(jastadd|junit)\]|BUILD FAILED|\[java\] Error when parsing file:)" \
	| sed -n \
	    -e '1 i\
                0 0' \
            -e 's!^.*\[jastadd\] \(Semantic errors\|Syntax error\).*$!BUILD FAILED!' \
	    -e 's!^.*\[java\] Error when parsing file:.*$!BUILD FAILED!' \
	    -e 's!^.*Failures: \([0-9]*\), Errors: \([0-9]*\),.*$!r\1+r\2+!p' \
	    -e 's!^.*\(BUILD FAILED\).*$![\1]pq!p' \
	    -e '$ a\
                [errors=]nn[, failures=]np' \
	| dc \
	| sed -e 's!errors=0, failures=0!PASSED!' \
	    -e 's!\(errors=.*, failures=.*\)!TEST FAILED (\1)!'
      ;;
    n)
      ant_test | egrep "(\[junit\]|\[(java|jastadd)\].*([Ee]rror|[A-Za-z.]*:[1-9][0-9]*))"
      ;;
  esac
}

function ant_test() {
  ant ${CLEAN_JUNIT} test
}

function build_jmodelica() {
  if [[ "${NO_BUILD_JMODELICA}" != "1" ]]; then
    if [[ -d ${BUILD} ]]; then
      cd ${BUILD}
      ${CLEAN_BUILD}
      make && make install
    else
      echo Cannot build, build dir does not exist!
      exit 1
    fi
  fi
}

function switch_test_flags() {
  STATE=0
  for a in $*; do
    if [[ "${test[${!a}]}" != 1 ]]; then 
      STATE=1
    fi
  done
  for a in $*; do
    test[${!a}]=${STATE}
  done
}

# === Main ===
OUTPUT=n
i=0
for a in ${tests}; do
    test[${i}]=0
    (( ${a}=i++ ))
done
export JAVA_HOME JMODELICA_HOME IPOPT_HOME CPPAD_HOME PYTHONPATH LD_LIBRARY_PATH ANT_OPTS
TIME=$(date +%s)

for a in ${DEFAULT_ARGS} $*; do
  for b in $(echo "" ${a}|sed 's!\(.\)!\1 !g'); do
    case ${b} in
      -)
        ;;
      v|q|n)
        OUTPUT=${b}
        ;;
      a|j|m|o|p)
        test_set="test_set_${b}"
        switch_test_flags ${!test_set}
        ;;
      c|C)
        CLEAN_JUNIT=clean
        if [[ "${b}" == C ]]; then
          CLEAN_BUILD="make clean"
        fi
        ;;
      i)
        NO_BUILD_JMODELICA=1
        ;;
      h)
        echo "usage: tests [-ajmopvnqcCh]"
        echo "The -ajmop options each control a set of tests, and select all"
        echo "tests in set for running, or, if all in set are already selected,"
        echo "deselects them. Thus, \"tests -am\" runs all tests except"
        echo "modelica. If no tests are chosen, a default set is run (${DEFAULT_TESTS})."
        echo "  -a  Select/deselect all tests"
        echo "  -j  Select/deselect junit tests"
        echo "  -m  Select/deselect modelica tests"
        echo "  -o  Select/deselect optimca tests"
        echo "  -p  Select/deselect python tests"
        echo "  -v  Verbose output, show everything"
        echo "  -n  Normal output, only show results and compilation errors"
        echo "  -q  Quiet output, only show brief resuls and compilation errors"
        echo "  -c  Do a clean before junit tests"
        echo "  -C  Do a clean before each test suite"
        echo "  -i  Do not re-build before python tests"
        echo "  -h  Print this help and exit"
	exit
        ;;
      *)
        echo "Unknown flag -${b}."
	exit
    esac
  done
done

ANY_ON=0
for a in ${tests}; do
  if [[ "${test[${!a}]}" == 1 ]]; then
    ANY_ON=1
  fi
done
if [[ "${ANY_ON}" == 0 ]]; then
  switch_test_flags ${DEFAULT_TESTS}
fi

# If on Windows, inform user about bug in ticket #766
if [[ "$(uname | sed -n -e 's!^MINGW!1!p')" != "" ]]; then
  echo Note: If modelica or optimica tests seem to hang, press enter a couple of times.
fi

for a in ${tests}; do
  if [[ "${test[${!a}]}" == 1 ]]; then
    ${a}_tests
  fi
done

if [[ "${OUTPUT}" != q ]]; then
  echo ========================
fi
echo $(date +%s) ${TIME}"-[Total time: ]nd60/n[:]n60%[0n]sxd10>xp" | dc
