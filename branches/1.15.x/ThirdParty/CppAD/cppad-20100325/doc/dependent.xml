<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Stop Recording and Store Operation Sequence</title>
<meta name="description" id="description" content="Stop Recording and Store Operation Sequence"/>
<meta name="keywords" id="keywords" content=" Adfun operation sequence store recording stop tape Dependent "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_dependent_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="fun_assign.cpp.xml" target="_top">Prev</a>
</td><td><a href="abort_recording.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Dependent</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>seq_property</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>optimize</option>
<option>FunDeprecated</option>
</select>
</td>
<td>Dependent</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>y</option>
<option>ADvector</option>
<option>Taping</option>
<option>Forward</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>





<center><b><big><big>Stop Recording and Store Operation Sequence</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Dependent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Stop recording and the AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>

that started with the call
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>and store the operation sequence in <i>f</i>.
The operation sequence defines an 
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>


<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>B</mi>
</mrow></math>

 is the space corresponding to objects of type <i>Base</i>.
The value 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 is the dimension of the 
<a href="seq_property.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for the operation sequence.
The value 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 is the dimension of the 
<a href="seq_property.xml#Range" target="_top"><span style='white-space: nowrap'>range</span></a>
 space for the operation sequence
(which is determined by the size of <i>y</i>).

<br/>
<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The AD of <i>Base</i> operation sequence is stored in <i>f</i>; i.e.,
it becomes the operation sequence corresponding to <i>f</i>.
If a previous operation sequence was stored in <i>f</i>,
it is deleted. 

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> 
must be the vector argument in a previous call to
<a href="independent.xml" target="_top"><span style='white-space: nowrap'>Independent</span></a>
.
Neither its size, or any of its values, are allowed to change
between calling
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>and 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Dependent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>.

<br/>
<br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The vector <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>ADvector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="funconstruct.xml" target="_top"><span style='white-space: nowrap'>ADvector</span></a>
 below).
The length of <i>y</i> must be greater than zero
and is the dimension of the range space for <i>f</i>.

<br/>
<br/>
<b><big><a name="ADvector" id="ADvector">ADvector</a></big></b>
<br/>
The type <i>ADvector</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type</span></a>

<code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code>.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="Taping" id="Taping">Taping</a></big></b>
<br/>
The tape,
that was created when <code><font color="blue"><span style='white-space: nowrap'>Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> was called, 
will stop recording.
The AD operation sequence will be transferred from
the tape to the object <i>f</i> and the tape will then be deleted.

<br/>
<br/>
<b><big><a name="Forward" id="Forward">Forward</a></big></b>
<br/>
No <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 calculation is preformed during this operation.
Thus, directly after this operation,
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.size_taylor()<br/>
</span></font></code>is zero (see <a href="size_taylor.xml" target="_top"><span style='white-space: nowrap'>size_taylor</span></a>
).

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>
<br/>
The file
<a href="funcheck.cpp.xml" target="_top"><span style='white-space: nowrap'>FunCheck.cpp</span></a>
 
contains an example and test of this operation.
It returns true if it succeeds and false otherwise.


<hr/>Input File: cppad/local/dependent.hpp

</body>
</html>
