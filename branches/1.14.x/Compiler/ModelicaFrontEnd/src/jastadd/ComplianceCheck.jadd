/*
    Copyright (C) 2010 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.jmodelica.util.ErrorCheckType;

aspect ComplianceCheck {

    /**
     * \brief Check for code that is not allowed in its current context.
     * 
     * Examples would be checking that classes follow the requirements of 
     * their restriction. 
     */
    public void ASTNode.complianceCheck(ErrorCheckType checkType) {}

    public abstract class ErrorChecker {
        public static class ComplianceChecker extends ErrorChecker {
            public ComplianceChecker() {
                super("ComplianceCheck");
            }

            @Override
            public void check(ASTNode node, ErrorCheckType checkType) {
                node.complianceCheck(checkType);
            }
        }
    }

    private static ErrorChecker ASTNode.COMPLIANCE_CHECKER = addErrorChecker(new ErrorChecker.ComplianceChecker());
	
	public void FIfWhenClause.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (!getTest().variability().lessOrEqual(fParameter()))
			complianceOnlyFMU("Using if statements is");
	}
	
	public void FWhileStmt.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (!getTest().variability().lessOrEqual(fParameter()))
			complianceOnlyFMU("Using while statements is");
	}
	
	public void FRelExp.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (!inFunction() && generatesEvent() && inWhile())
			compliance("Event generating expressions are not supported in while statements: " + prettyPrint(""));
	}
	
	public void FEventGenExp.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		complianceOnlyFMU("The " + name() + "() function-like operator is");
		if (!inFunction() && generatesEvent() && inWhile())
			compliance("Event generating expressions are not supported in while statements: " + prettyPrint(""));
	}
	
    inh boolean FRelExp.inWhile();
    inh boolean FEventGenExp.inWhile();
    eq FWhileStmt.getChild().inWhile() = true;
    eq Root.getChild().inWhile() = false;
    eq FAlgorithm.getChild().inWhile() = false;
	
	public void InstAccess.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (myInstComponentDecl().isRecord() && hasFArraySubscripts() && inFunction() && 
				!getFArraySubscripts().variability().lessOrEqual(fParameter())) {
			complianceOnlyFMU("Using arrays of records with indices of higher than parameter variability is");
		}
	}

	public void FWhenEquation.complianceCheck(ErrorCheckType checkType) {
		complianceOnlyFMU("When equations are");
	}
	
	public void ASTNode.complianceOnlyFMU(String message) {
		if (!root().options.getBooleanOption("generate_ode"))
			compliance(message + " currently only supported when compiling FMUs");
	}

	public void FUnsupportedEquation.collectErrors(ErrorCheckType checkType) {
		compliance("Unsupported equation type");
	}

	public void FUnsupportedExp.collectErrors(ErrorCheckType checkType) {
		compliance("Unsupported expression type");
	}

	public void FUnsupportedBuiltIn.complianceCheck(ErrorCheckType checkType) {
		compliance("The " + getName() + "() function-like operator is not supported");
	}
	
	public void FSampleExp.complianceCheck(ErrorCheckType checkType) {
	    complianceOnlyFMU("The sample() function-like operator is");
	}

	public void InstPreExp.complianceCheck(ErrorCheckType checkType) {
	    complianceOnlyFMU("The pre() function-like operator is");
	}
	
	public void FEdgeExp.complianceCheck(ErrorCheckType checkType) {
	    complianceOnlyFMU("The edge() function-like operator is");
	}
	
	public void FChangeExp.complianceCheck(ErrorCheckType checkType) {
	    complianceOnlyFMU("The change() function-like operator is");
	}
	
	public void FInitialExp.complianceCheck(ErrorCheckType checkType) {
		complianceOnlyFMU("The initial() function-like operator is");
	}
	
	public void FSignExp.complianceCheck(ErrorCheckType checkType) {
		complianceOnlyFMU("The sign() function-like operator is");
	}
	
	public void FHomotopyExp.complianceCheck(ErrorCheckType checkType) {
		if (root().options.getStringOption("homotopy_type") == OptionRegistry.Homotopy.HOMOTOPY) {
			warning("The 'homotopy' setting of the homotopy_type option is not supported. Setting to 'actual'.");
			root().options.setStringOption("homotopy_type", OptionRegistry.Homotopy.ACTUAL);
			
		}
	}
	
	public void FSemiLinearExp.complianceCheck(ErrorCheckType checkType) {
		complianceOnlyFMU("The semiLinear() function-like operator is");
	}
	
	public void FCardinality.complianceCheck(ErrorCheckType checkType) {
		warning("The cardinality() function-like operator is deprecated, and will be removed in a future version of Modelica");
		if (!inAssert() && !inIfTestWithoutConnect()) 
			compliance("The cardinality() function-like operator is only supported in asserts and in the tests of if clauses that do not contain connect()");
    }
	
	syn boolean FExp.isAccessToInnerOrOuter()  = false;
	eq FInstAccessExp.isAccessToInnerOrOuter() = 
        getInstAccess().myInstComponentDecl().isInner() || getInstAccess().myInstComponentDecl().isOuter();
	
	inh boolean FCardinality.inAssert();
	eq FAssert.getChild().inAssert()  = true;
	eq Root.getChild().inAssert()     = false;
	eq InstNode.getChild().inAssert() = false;
	
	inh boolean FCardinality.inIfTestWithoutConnect();
	eq FIfEquation.getTest().inIfTestWithoutConnect() = !containsConnect();
	eq Root.getChild().inIfTestWithoutConnect()       = false;
	eq InstNode.getChild().inIfTestWithoutConnect()   = false;
	
	syn boolean FAbstractEquation.containsConnect() = false;
	eq FConnectClause.containsConnect()             = true;
	eq FForClauseE.containsConnect()                = containsConnect(getFAbstractEquations());
	eq FIfWhenElseEquation.containsConnect()        = containsConnect(getFAbstractEquations());
	eq FIfWhenEquation.containsConnect()            = 
		super.containsConnect() || (hasElse() && getElse().containsConnect());
	
	public static boolean FAbstractEquation.containsConnect(List<FAbstractEquation> eqns) {
		for (FAbstractEquation eqn : eqns)
			if (eqn.containsConnect())
				return true;
		return false;
	}
	
	public void FUnaryBuiltIn.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (!unknownSizeSupported() && inFunction() && getFExp().size().isUnknown()) {
			compliance("Unknown sizes in operator " + name() + "() is not supported in functions");
		}
	}
	
	syn boolean FUnaryBuiltIn.unknownSizeSupported() = true;
	eq FSymmetric.unknownSizeSupported()             = false;
	eq FScalarExp.unknownSizeSupported()             = false;
	eq FVectorExp.unknownSizeSupported()             = false;
	eq FMatrixExp.unknownSizeSupported()             = false;
	
	public void InstForIndex.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (hasFExp() && getFExp().size().isUnknown() && getFExp().isAccess() && inFunction()) {
			compliance("Unknown size array as a for index is not supported in functions");
		}
		if (!hasFExp()) {
			compliance("For index without in expression isn't supported");
		}
		if (hasFExp() && !getFExp().variability().parameterOrLess() && !inFunction()) {
			compliance("For index with higher than parameter variability is not supported in equations and algorithms.");
		}
	}
	
	public void InstForIndex.contentCheck(ErrorCheckType checkType) {
	}
	
	public void FIdentity.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (inFunction() && !getFExp().canCeval()) {
			compliance("Unknown size arg in operator " + name() + "() is not supported in functions");
		}
	}
	
	public void FPowExp.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (inFunction() && getLeft().size().isUnknown()) {
			compliance("Unknown sizes in operator ^ is not supported in functions");
		}
	}
	
	public void FIgnoredBuiltIn.complianceCheck(ErrorCheckType checkType) {
		warning("The " + getName() + 
				"() function-like operator is not supported, and is currently ignored");
	}
	
	public void FTerminalExp.complianceCheck(ErrorCheckType checkType) {
		warning("The terminal() function-like operator is not supported, and is currently evaluated to false");
	}
    
    public void FTerminate.complianceCheck(ErrorCheckType checkType) {
        complianceOnlyFMU("The terminate() function-like operator is");
    }
    
    public void FReinit.complianceCheck(ErrorCheckType checkType) {
        complianceOnlyFMU("The reinit() function-like operator is");
    }
	
	public void FStringExp.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (hasMinimumLength()) {
			getMinimumLength().markAsStructuralParameter(checkType);
			if (!getMinimumLength().variability().parameterOrLess())
				getMinimumLength().compliance("minimumLength with higher than parameter variability is not supported");
		}
		if (hasLeftJustified()) {
			getLeftJustified().markAsStructuralParameter(checkType);
			if (!getLeftJustified().variability().parameterOrLess())
				getLeftJustified().compliance("leftJustified with higher than parameter variability is not supported");
		}
		if (hasSignificantDigits()) {
			getSignificantDigits().markAsStructuralParameter(checkType);
			if (!getSignificantDigits().variability().parameterOrLess())
				getSignificantDigits().compliance("significantDigits with higher than parameter variability is not supported");
		}
		if (hasFormat()) {
			getFormat().compliance("format argument is not supported");
//			getFormat().markAsStructuralParameter(checkType);
//			if (!getSignificantDigits().variability().parameterOrLess())
//				getFormat().compliance("format with higher than parameter variability is not supported");
		}
	}
		
	public void InstPrimitive.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (isString()) {
			if (inFunction()) {
				warning("String arguments in functions are only partially supported");
			} else if (variability().lessOrEqual(fParameter())) {
				warning("String parameters are only partially supported");
				if (isParameter())
					markAsStructuralParameter(checkType);
			} else {
				compliance("String variables are not supported");
			}
		}
		if (!root().options.getBooleanOption("generate_ode")) {
			if (isInteger() && !variability().lessOrEqual(fParameter()) && !inFunction())
				compliance("Integer variables are supported only when compiling FMUs (constants and parameters are always supported)");
			if (isBoolean() && !variability().lessOrEqual(fParameter()))
				compliance("Boolean variables are supported only when compiling FMUs (constants and parameters are always supported)");
		}
	}
	
	public void InstEnum.complianceCheck(ErrorCheckType checkType) {
		super.complianceCheck(checkType);
		if (!variability().lessOrEqual(fParameter()) && !root().options.getBooleanOption("generate_ode"))
			compliance("Enumeration variables are supported only when compiling FMUs (constants and parameters are always supported)");
	}
	
	// This can be removed when a real name collision check is added
	public void FClass.checkDuplicateVariables() {
		for (String name : nonUniqueVars) {
			warning("The variable " + name + " is declared multiple times and can not " +
			        "be verified to be identical to other declaration(s) with the same name.");
		}

	}
	
	public void FClass.checkUnsupportedStreamConnections() {
		for (ConnectionSet cse : getConnectionSetManager().getConnectionSetList()) {
			if (cse.numStreamVariables() > 2) {
				compliance("Stream connections with more than two connectors are not supported: " + cse);
			}
		}
	}
	
    public void InstComposite.complianceCheck(ErrorCheckType checkType) {
        if (isFunctionalArg())
            compliance("Using functional input arguments is currently not supported");
    }
}