<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>CppAD Speed: Second Derivative of a Polynomial</title>
<meta name="description" id="description" content="CppAD Speed: Second Derivative of a Polynomial"/>
<meta name="keywords" id="keywords" content=" cppad speed polynomial link_poly "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_cppad_poly.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="cppad_ode.cpp.xml" target="_top">Prev</a>
</td><td><a href="cppad_sparse_hessian.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_cppad</option>
<option>cppad_poly.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed_cppad-&gt;</option>
<option>cppad_det_minor.cpp</option>
<option>cppad_det_lu.cpp</option>
<option>cppad_ode.cpp</option>
<option>cppad_poly.cpp</option>
<option>cppad_sparse_hessian.cpp</option>
<option>cppad_sparse_jacobian.cpp</option>
</select>
</td>
<td>cppad_poly.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>link_poly</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>CppAD Speed: Second Derivative of a Polynomial</big></big></b></center>
<br/>
<b><big><a name="link_poly" id="link_poly">link_poly</a></big></b>


<code><font color='blue'><pre style='display:inline'> 
# include &lt;cppad/cppad.hpp&gt;
# include &lt;cppad/speed/uniform_01.hpp&gt;

bool link_poly(
	size_t                     size     , 
	size_t                     repeat   , 
	CppAD::vector&lt;double&gt;     &amp;a        ,  // coefficients of polynomial
	CppAD::vector&lt;double&gt;     &amp;z        ,  // polynomial argument value
	CppAD::vector&lt;double&gt;     &amp;ddp      )  // second derivative w.r.t z  
{
	// -----------------------------------------------------
	// setup
	typedef CppAD::<a href="ad.xml" target="_top">AD</a>&lt;double&gt;     ADScalar; 
	typedef CppAD::vector&lt;ADScalar&gt; ADVector; 

	size_t i;      // temporary index
	size_t m = 1;  // number of dependent variables
	size_t n = 1;  // number of independent variables
	ADVector Z(n); // AD domain space vector
	ADVector P(m); // AD range space vector

	// choose the polynomial coefficients
	CppAD::uniform_01(size, a);

	// AD copy of the polynomial coefficients
	ADVector A(size);
	for(i = 0; i &lt; size; i++)
		A[i] = a[i];

	// forward mode first and second differentials
	CppAD::vector&lt;double&gt; p(1), dp(1), dz(1), ddz(1);
	dz[0]  = 1.;
	ddz[0] = 0.;

	// AD function object
	CppAD::<a href="funconstruct.xml" target="_top">ADFun</a>&lt;double&gt; f;

	static bool printed = false;
	bool print_this_time = (! printed) &amp; (repeat &gt; 1) &amp; (size &gt;= 10);

	extern bool global_retape;
	if( global_retape ) while(repeat--)
	{
		// choose an argument value
		CppAD::uniform_01(1, z);
		Z[0] = z[0];

		// declare independent variables
		<a href="independent.xml" target="_top">Independent</a>(Z);

		// AD computation of the function value 
		P[0] = CppAD::Poly(0, A, Z[0]);

		// create function object f : A -&gt; detA
		f.Dependent(Z, P);

		extern bool global_optimize;
		if( global_optimize )
		{	size_t before, after;
			before = f.size_var();
			f.optimize();
			if( print_this_time ) 
			{	after = f.size_var();
				std::cout &lt;&lt; &quot;cppad_poly_optimize_size_&quot; 
				          &lt;&lt; int(size) &lt;&lt; &quot; = [ &quot; &lt;&lt; int(before) 
				          &lt;&lt; &quot;, &quot; &lt;&lt; int(after) &lt;&lt; &quot;]&quot; &lt;&lt; std::endl;
				printed         = true;
				print_this_time = false;
			}
		}

		// pre-allocate memory for three forward mode calculations
		f.capacity_taylor(3);

		// get the next argument value
		CppAD::uniform_01(1, z);

		// evaluate the polynomial at the new argument value
		p = f.<a href="forward.xml" target="_top">Forward</a>(0, z);

		// evaluate first order Taylor coefficient
		dp = f.<a href="forward.xml" target="_top">Forward</a>(1, dz);

		// second derivative is twice second order Taylor coef
		ddp     = f.<a href="forward.xml" target="_top">Forward</a>(2, ddz);
		ddp[0] *= 2.;
	}
	else
	{
		// choose an argument value
		CppAD::uniform_01(1, z);
		Z[0] = z[0];

		// declare independent variables
		<a href="independent.xml" target="_top">Independent</a>(Z);

		// AD computation of the function value 
		P[0] = CppAD::Poly(0, A, Z[0]);

		// create function object f : A -&gt; detA
		f.Dependent(Z, P);

		extern bool global_optimize;
		if( global_optimize )
		{	size_t before, after;
			before = f.size_var();
			f.optimize();
			if( print_this_time ) 
			{	after = f.size_var();
				std::cout &lt;&lt; &quot;optimize: size = &quot; &lt;&lt; size
				          &lt;&lt; &quot;: size_var() = &quot;
				          &lt;&lt; before &lt;&lt; &quot;(before) &quot; 
				          &lt;&lt; after &lt;&lt; &quot;(after) &quot; 
				          &lt;&lt; std::endl;
				printed         = true;
				print_this_time = false;
			}
		}

		while(repeat--)
		{	// sufficient memory is allocated by second repetition

			// get the next argument value
			CppAD::uniform_01(1, z);

			// evaluate the polynomial at the new argument value
			p = f.<a href="forward.xml" target="_top">Forward</a>(0, z);

			// evaluate first order Taylor coefficient
			dp = f.<a href="forward.xml" target="_top">Forward</a>(1, dz);

			// second derivative is twice second order Taylor coef
			ddp     = f.<a href="forward.xml" target="_top">Forward</a>(2, ddz);
			ddp[0] *= 2.;
		}
	}
	return true;
}
</pre></font></code>


<hr/>Input File: speed/cppad/poly.cpp

</body>
</html>
