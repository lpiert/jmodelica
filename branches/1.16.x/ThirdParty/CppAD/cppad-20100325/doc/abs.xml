<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>AD Absolute Value Function</title>
<meta name="description" id="description" content="AD Absolute Value Function"/>
<meta name="keywords" id="keywords" content=" abs Ad absolute value value_ directional derivative "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_abs_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="mathother.xml" target="_top">Prev</a>
</td><td><a href="abs.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>ADValued</option>
<option>MathOther</option>
<option>abs</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADValued-&gt;</option>
<option>Arithmetic</option>
<option>std_math_ad</option>
<option>MathOther</option>
<option>CondExp</option>
<option>Discrete</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>MathOther-&gt;</option>
<option>abs</option>
<option>atan2</option>
<option>erf</option>
<option>pow</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>abs-&gt;</option>
<option>Abs.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>x</option>
<option>y</option>
<option>Operation Sequence</option>
<option>Complex Types</option>
<option>Directional Derivative</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>AD Absolute Value Function</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;abs(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Evaluates the absolute value function.


<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has one of the following prototypes
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The result <i>y</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="Operation Sequence" id="Operation Sequence">Operation Sequence</a></big></b>
<br/>
This is an AD of <i>Base</i>
<a href="glossary.xml#Operation.Atomic" target="_top"><span style='white-space: nowrap'>atomic&#xA0;operation</span></a>

and hence is part of the current
AD of <i>Base</i>
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.

<br/>
<br/>
<b><big><a name="Complex Types" id="Complex Types">Complex Types</a></big></b>
<br/>
The function <code><font color="blue">abs</font></code> is not defined for the AD type sequences
above <code><font color="blue">std::complex&lt;float&gt;</font></code> or <code><font color="blue">std::complex&lt;double&gt;</font></code>
because the complex <code><font color="blue">abs</font></code> function is not complex differentiable
(see <a href="faq.xml#Complex Types" target="_top"><span style='white-space: nowrap'>complex&#xA0;types&#xA0;faq</span></a>
).

<br/>
<br/>
<b><big><a name="Directional Derivative" id="Directional Derivative">Directional Derivative</a></big></b>


<br/>
The derivative of the absolute value function is one for 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&gt;</mo>
<mn>0</mn>
</mrow></math>

 and minus one for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&lt;</mo>
<mn>0</mn>
</mrow></math>

.
The subtitle issue is 
how to compute its directional derivative
what 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>The function corresponding to the argument <i>x</i> 
and the result <i>y</i> are represented
by their Taylor coefficients; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>p</mi>
</msup>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>p</mi>
</msup>
</mtd></mtr></mtable>
</mrow></math>

Note that 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow></math>

 is the value of <i>x</i> and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow></math>

 is the value of <i>y</i>.
In the equations above, the order 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
</mrow></math>

 is specified
by a call to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 or <a href="reverse.xml" target="_top"><span style='white-space: nowrap'>Reverse</span></a>
 as follows:
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Forward(</span></font></code><i><span style='white-space: nowrap'>p</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>dx</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Reverse(</span></font></code><i><span style='white-space: nowrap'>p</span></i><code><font color="blue"><span style='white-space: nowrap'>+1,&#xA0;</span></font></code><i><span style='white-space: nowrap'>w</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>If all of the Taylor coefficients of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 are zero,
we define 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>p</mi>
</mrow></math>

.
Otherwise, we define 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
</mrow></math>

 to be the minimal index such that 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
</mrow></math>

.
Note that if 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

.
The Taylor coefficient representation of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>


(and hence it's derivatives) are computed as

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">{</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="left" >
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>if</mi>
</mstyle></mrow>
<mspace width='.3em'/>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&gt;</mo>
<mn>0</mn>
</mtd></mtr><mtr><mtd columnalign="left" >
<mn>0</mn>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>if</mi>
</mstyle></mrow>
<mspace width='.3em'/>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mtd></mtr><mtr><mtd columnalign="left" >
<mo stretchy="false">-</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mo stretchy="false">&#x02113;</mo>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>if</mi>
</mstyle></mrow>
<mspace width='.3em'/>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&lt;</mo>
<mn>0</mn>
</mtd></mtr></mtable>
</mrow><mo stretchy="true"> </mo></mrow>
</mrow></math>

<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="abs.cpp.xml" target="_top"><span style='white-space: nowrap'>Abs.cpp</span></a>

contains an example and test of this function.   
It returns true if it succeeds and false otherwise.


<hr/>Input File: cppad/local/abs.hpp

</body>
</html>
