<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Installation</title>

  <section xml:id="inst_sec_supported_platforms">
    <title>Supported platforms</title>

    <para>JModelica.org can be installed on Linux, Mac OS X, and Windows (XP,
    Vista, 7) with 32-bit or 64-bit architectures.</para>
  </section>

  <section xml:id="inst_sec_win">
    <title>Installation on Windows</title>

    <para>Pre-built binary distributions for Windows are available in the
    Download section of <link
    xlink:href="www.jmodelica.org">www.jmodelica.org</link>.</para>

    <para>The Windows installer contains a binary distribution of
    JModelica.org, bundled with all required third-party software components.
    A list of the third-party dependencies can be found in <xref
    linkend="inst_sec_win_deps"/>. The installer sets up a pre-configured
    complete environment with convenient start menu shortcuts. Installation
    instructions are found in <xref linkend="inst_sec_win_inst"/>.</para>

    <section xml:id="inst_sec_win_deps">
      <title>Dependencies</title>

      <para>As of JModelica.org version 1.9, all dependencies are bundled in
      the installer. They are listed below, each with version number (where
      applicable) and link to corresponding web site.<itemizedlist>
          <listitem>
            <para><emphasis role="bold">Applications</emphasis><itemizedlist>
                <listitem>
                  <para><link
                  xlink:href="http://www.oracle.com/technetwork/java/javase/downloads/index.html">Java
                  1.7</link> (JRE)</para>
                </listitem>

                <listitem>
                  <para><link xlink:href="http://www.mingw.org">MinGW</link>
                  (gcc 4.7.2)</para>
                </listitem>

                <listitem>
                  <para><link xlink:href="http://www.python.org/">Python
                  2.7</link></para>
                </listitem>
              </itemizedlist></para>
          </listitem>

          <listitem>
            <para><emphasis role="bold">Libraries</emphasis></para>

            <itemizedlist>
              <listitem>
                <para><link
                xlink:href="https://projects.coin-or.org/Ipopt">Ipopt
                3.10.3</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://crd.lbl.gov/~xiaoye/SuperLU/">SuperLU
                4.1</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://beaver.sourceforge.net/">Beaver
                0.9.6.1</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://www.coin-or.org/CppAD/">CppAD</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://expat.sourceforge.net/">eXpat
                2.1.0</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://www.winimage.com/zLibDll/minizip.html">Minizip</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="https://www.modelica.org/libraries/Modelica">MSL
                (Modelica Standard Library)</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="https://computation.llnl.gov/casc/sundials">SUNDIALS
                2.4.0</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://www.zlib.net/">Zlib
                1.2.6</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://casadi.org">CasADi</link></para>
              </listitem>
            </itemizedlist>
          </listitem>

          <listitem>
            <para><emphasis role="bold">Python packages</emphasis></para>

            <itemizedlist>
              <listitem>
                <para><link xlink:href="http://www.cython.org/">Cython
                0.18</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://packages.python.org/distribute/">Distribute
                0.6.35</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://ipython.org/">IPython
                0.13.1</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://jpype.sourceforge.net">JPype
                0.5.4.2</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://lxml.de/">lxml
                3.1.0</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://matplotlib.org/">matplotlib
                1.2.0</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="https://nose.readthedocs.org/en/latest/">nose
                1.2.1</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://www.numpy.org/">NumPy
                1.6.2</link></para>
              </listitem>

              <listitem>
                <para><link
                xlink:href="http://packages.python.org/pyreadline/">Pyreadline
                1.7.1</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://www.scipy.org/">SciPy
                0.11.0</link></para>
              </listitem>

              <listitem>
                <para><link xlink:href="http://www.wxpython.org">wxPython
                2.8</link></para>
              </listitem>
            </itemizedlist>
          </listitem>
        </itemizedlist></para>
    </section>

    <section xml:id="inst_sec_win_inst">
      <title>Installation</title>

      <para>The following step-by-step instructions describe how to install
      JModelica.org using the Windows binary distribution.</para>

      <procedure>
        <step>
          <para>Download a <link
          xlink:href="http://www.jmodelica.org/page/12">JModelica.org Windows
          binary installer</link> and save the executable file somewhere on
          your computer.</para>
        </step>

        <step>
          <para>Run the file by double-clicking and selecting "Run" if
          prompted with a security warning. This will launch an installer
          which should be self-explanatory.</para>

          <itemizedlist>
            <listitem>
              <para>In the <emphasis>Choose Components</emphasis> window,
              select which of the bundled Python packages that should be
              installed. Make sure that any package not already installed on
              your computer is checked.<figure>
                  <title>Selecting Python packages in the <emphasis>Choose
                  components</emphasis> window.</title>

                  <mediaobject>
                    <imageobject>
                      <imagedata align="center"
                                 fileref="images/Setup_components.png"
                                 scalefit="1" width="50%"/>
                    </imageobject>
                  </mediaobject>
                </figure></para>
            </listitem>
          </itemizedlist>
        </step>
      </procedure>
    </section>

    <section>
      <title>Verifying the installation</title>

      <para>Test the installation by starting a IPython or pylab shell from
      the JModelica.org start menu and run a few examples. Starting the Python
      session from the start menu will set all the environment variables
      required to run the JModelica.org Python interface.<programlisting
      language="python"># Run the VDP example and plot results
from pyjmi.examples import vdp
vdp.run_demo()

# Run the CSTR example and plot results
from pyjmi.examples import cstr
cstr.run_demo()

# Run the CSTR example using CasADi and plot results
from pyjmi.examples import cstr_casadi
cstr_casadi.run_demo()</programlisting></para>
    </section>

    <section>
      <title>Compilation from sources</title>

      <para>For compiling JModelica.org from sources on Windows there is a
      <link xlink:href="http://www.jmodelica.org/sdk">Software Development
      Kit</link> (SDK) available for download. The SDK is a bundle of tools
      used to build JModelica.org from source code on Windows, please see the
      SDK User's guide, which can be reached from the download site, for more
      information.</para>
    </section>
  </section>

  <section xml:id="inst_sec_sdk">
    <title>Installation on Linux systems</title>

    <para>This section describes a procedure for compiling JModelica.org from
    sources on Linux. The instructions have been verified to work on Ubuntu
    Linux release 12.04, 64bit.</para>

    <section xml:id="inst_sec_sdk_prereq">
      <title>Prerequisites</title>

      <section>
        <title>Installing pre-compiled packages</title>

        <para>It is convenient to use a package management system, if
        available, of the Linux distribution to install the prerequisites. On
        Ubuntu systems, the apt-get command line program may be used:</para>

        <programlisting>sudo apt-get -y install g++
sudo apt-get -y install subversion
sudo apt-get -y install gfortran
sudo apt-get -y install ipython
sudo apt-get -y install cmake
sudo apt-get -y install swig
sudo apt-get -y install ant
sudo apt-get -y install openjdk-6-jdk
sudo apt-get -y install python-dev
sudo apt-get -y install python-numpy
sudo apt-get -y install python-scipy
sudo apt-get -y install python-matplotlib
sudo apt-get -y install cython
sudo apt-get -y install python-lxml
sudo apt-get -y install python-nose
sudo apt-get -y install python-jpype
</programlisting>

        <para>The following versions of each package have been tested and
        verified to work. Please note that in some cases, a minimum version is
        required.</para>

        <table xml:id="inst_tab_versions_ubuntu">
          <title>Package versions for Ubuntu</title>

          <tgroup cols="3">
            <colspec align="left" colname="col–package" colwidth="3*"/>

            <colspec align="left" colname="col–version" colwidth="1*"/>

            <colspec align="left" colname="col–note" colwidth="2*"/>

            <thead>
              <row>
                <entry align="center">Package</entry>

                <entry align="center">Version</entry>

                <entry align="center">Note</entry>
              </row>
            </thead>

            <tbody>
              <row>
                <entry><literal>g++</literal></entry>

                <entry><literal>4.6.3</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>subversion</literal></entry>

                <entry><literal>1.6.17</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>gfortran</literal></entry>

                <entry><literal>4.6.3</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>ipython</literal></entry>

                <entry><literal>0.12.1</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>cmake</literal></entry>

                <entry><literal>2.8.6</literal></entry>

                <entry>Minimum version</entry>
              </row>

              <row>
                <entry><literal>swig</literal></entry>

                <entry><literal>2.0.4</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>ant</literal></entry>

                <entry><literal>1.8.2</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-dev</literal></entry>

                <entry><literal>2.7.3</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-numpy</literal></entry>

                <entry><literal>1.6.1</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-scipy</literal></entry>

                <entry><literal>0.9.0</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-matplotlib</literal></entry>

                <entry><literal>1.1.1</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>cython</literal></entry>

                <entry><literal>0.15</literal></entry>

                <entry>Minimum version</entry>
              </row>

              <row>
                <entry><literal>python-lxml</literal></entry>

                <entry><literal>2.3.2</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-nose</literal></entry>

                <entry><literal>1.1.2</literal></entry>

                <entry>Tested version</entry>
              </row>

              <row>
                <entry><literal>python-jpype</literal></entry>

                <entry><literal>0.5.4.2</literal></entry>

                <entry>Tested version</entry>
              </row>
            </tbody>
          </tgroup>
        </table>
      </section>

      <section>
        <title>Compiling Ipopt</title>

        <para>While Ipopt is available as a pre-compiled package for Ubuntu,
        it is recommended to build Ipopt from sources. The Ipopt packages
        provided for Ubuntu have had flaws (including the version provided for
        Ubuntu 12.04) that prevented usage with JModelica.org. Also, compiling
        Ipopt from sources is required when using the linear solvers MA27 or
        MA57 from the HSL library, since these are not available as open
        source software.</para>

        <para>First, download the Ipopt sources from <link
        xlink:href="https://projects.coin-or.org/Ipopt">https://projects.coin-or.org/Ipopt</link>
        and unpack the content:</para>

        <programlisting>tar xvf Ipopt-3.10.2.tgz</programlisting>

        <para>Then, retrieve the third party dependencies:</para>

        <programlisting>cd Ipopt-3.10.2/ThirdParty/Blas
./get.Blas
cd ../Lapack
./get.Lapack
cd ../Mumps
./get.Mumps
cd ../Metis
./get.Metis
cd ../../
</programlisting>

        <para>If you have access to the HSL codes MA57 or MA27, copy their
        sources into the directory <literal>ThirdParty/HSL</literal>. In the
        next step, configure and compile Ipopt:</para>

        <programlisting>mkdir build
cd build
../configure --prefix=/home/&lt;user_name&gt;/&lt;ipopt_installation_location&gt;
make install
</programlisting>

        <para>where <literal>&lt;user_name&gt;</literal> and
        <literal>&lt;ipopt_installation_location&gt;</literal> are replaced by
        the user directory and the installation directory of choice for
        Ipopt.</para>
      </section>
    </section>

    <section>
      <title>Compiling</title>

      <para>Make sure that all prerequisites are installed before compiling
      the JModelica.org platform. First, check out the JModelica.org
      sources:</para>

      <programlisting>svn co https://svn.jmodelica.org/trunk JModelica.org
</programlisting>

      <para>Then configure and build JModelica.org:</para>

      <programlisting>cd JModelica.org
mkdir build
cd build
../configure --prefix=/home/&lt;user_name&gt;/&lt;jmodelica_install_location&gt; --with-ipopt=/home/&lt;user_name&gt;/&lt;ipopt_install_location&gt; 
make install
make install_casadi
</programlisting>

      <para>where <literal>&lt;user_name&gt;</literal> and
      <literal>&lt;jmodelica_installation_location&gt;</literal> are replaced
      by the user directory and the installation directory of choice for
      JModelica.org.</para>
    </section>

    <section>
      <title>Testing JModelica.org</title>

      <para>In order to verify that JModelica.org has been installed
      correctly, start an IPython shell using the command
      <literal>/home/&lt;user_name&gt;/&lt;jmodelica_install_location&gt;/bin/jm_ipython</literal>
      and enter the the commands:</para>

      <programlisting language="python">from pyjmi.examples import *
cstr.run_demo()
cstr_casadi.run_demo()
distillation_fmu.run_demo()
</programlisting>
    </section>
  </section>
</chapter>
