<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Reverse Mode Hessian Sparsity: Example and Test</title>
<meta name="description" id="description" content="Reverse Mode Hessian Sparsity: Example and Test"/>
<meta name="keywords" id="keywords" content=" Revsparsehes example sparsity Hessian test "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_revsparsehes.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="revsparsehes.xml" target="_top">Prev</a>
</td><td><a href="drivers.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>FunEval</option>
<option>Sparse</option>
<option>RevSparseHes</option>
<option>RevSparseHes.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>FunEval-&gt;</option>
<option>Forward</option>
<option>Reverse</option>
<option>Sparse</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Sparse-&gt;</option>
<option>ForSparseJac</option>
<option>RevSparseJac</option>
<option>RevSparseHes</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>RevSparseHes-&gt;</option>
<option>RevSparseHes.cpp</option>
</select>
</td>
<td>RevSparseHes.cpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>Reverse Mode Hessian Sparsity: Example and Test</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 

# include &lt;cppad/cppad.hpp&gt;
namespace { // -------------------------------------------------------------

// expected sparsity pattern
bool check_f0[] = {
	false, false, false,  // partials w.r.t x0 and (x0, x1, x2)
	false, false, false,  // partials w.r.t x1 and (x0, x1, x2)
	false, false, true    // partials w.r.t x2 and (x0, x1, x2)
};
bool check_f1[] = {
	false,  true, false,  // partials w.r.t x0 and (x0, x1, x2)
	true,  false, false,  // partials w.r.t x1 and (x0, x1, x2)
	false, false, false   // partials w.r.t x2 and (x0, x1, x2)
};

// define the template function BoolCases&lt;Vector&gt; in empty namespace
template &lt;typename Vector&gt; // vector class, elements of type bool
bool BoolCases(void)
{	bool ok = true;
	using CppAD::AD;

	// domain space vector
	size_t n = 3; 
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; X(n);
	X[0] = 0.; 
	X[1] = 1.;
	X[2] = 2.;

	// declare independent variables and start recording
	CppAD::<a href="independent.xml" target="_top">Independent</a>(X);

	// range space vector
	size_t m = 2;
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; Y(m);
	Y[0] = sin( X[2] );
	Y[1] = X[0] * X[1];

	// create f: X -&gt; Y and stop tape recording
	CppAD::<a href="funconstruct.xml" target="_top">ADFun</a>&lt;double&gt; f(X, Y);

	// sparsity pattern for the identity matrix
	Vector r(n * n);
	size_t i, j;
	for(i = 0; i &lt; n; i++)
	{	for(j = 0; j &lt; n; j++)
			r[ i * n + j ] = (i == j);
	}

	// compute sparsity pattern for J(x) = F^{(1)} (x)
	f.ForSparseJac(n, r);

	// compute sparsity pattern for H(x) = F_0^{(2)} (x)
	Vector s(m);
	for(i = 0; i &lt; m; i++)
		s[i] = false;
	s[0] = true;
	Vector h(n * n);
	h    = f.RevSparseHes(n, s);

	// check values
	for(i = 0; i &lt; n; i++)
		for(j = 0; j &lt; n; j++) 
			ok &amp;= (h[ i * n + j ] == check_f0[ i * n + j ] );

	// compute sparsity pattern for H(x) = F_1^{(2)} (x)
	for(i = 0; i &lt; m; i++)
		s[i] = false;
	s[1] = true;
	h    = f.RevSparseHes(n, s);

	// check values
	for(i = 0; i &lt; n; i++)
		for(j = 0; j &lt; n; j++) 
			ok &amp;= (h[ i * n + j ] == check_f1[ i * n + j ] );

	return ok;
}
// define the template function SetCases&lt;Vector&gt; in empty namespace
template &lt;typename Vector&gt; // vector class, elements of type std::set&lt;size_t&gt;
bool SetCases(void)
{	bool ok = true;
	using CppAD::AD;

	// domain space vector
	size_t n = 3; 
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; X(n);
	X[0] = 0.; 
	X[1] = 1.;
	X[2] = 2.;

	// declare independent variables and start recording
	CppAD::<a href="independent.xml" target="_top">Independent</a>(X);

	// range space vector
	size_t m = 2;
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; Y(m);
	Y[0] = sin( X[2] );
	Y[1] = X[0] * X[1];

	// create f: X -&gt; Y and stop tape recording
	CppAD::<a href="funconstruct.xml" target="_top">ADFun</a>&lt;double&gt; f(X, Y);

	// sparsity pattern for the identity matrix
	Vector r(n);
	size_t i;
	for(i = 0; i &lt; n; i++)
	{	assert( r[i].empty() );
		r[i].insert(i);
	}

	// compute sparsity pattern for J(x) = F^{(1)} (x)
	f.ForSparseJac(n, r);

	// compute sparsity pattern for H(x) = F_0^{(2)} (x)
	Vector s(1);
	assert( s[0].empty() );
	s[0].insert(0);
	Vector h(n);
	h    = f.RevSparseHes(n, s);

	// check values
	std::set&lt;size_t&gt;::iterator itr;
	size_t j;
	for(i = 0; i &lt; n; i++)
	{	for(j = 0; j &lt; n; j++)
		{	bool found = h[i].find(j) != h[i].end();
			ok        &amp;= (found == check_f0[i * n + j]);
		}
	}

	// compute sparsity pattern for H(x) = F_1^{(2)} (x)
	s[0].clear();
	assert( s[0].empty() );
	s[0].insert(1);
	h    = f.RevSparseHes(n, s);

	// check values
	for(i = 0; i &lt; n; i++)
	{	for(j = 0; j &lt; n; j++)
		{	bool found = h[i].find(j) != h[i].end();
			ok        &amp;= (found == check_f1[i * n + j]);
		}
	}

	return ok;
}
} // End empty namespace

# include &lt;vector&gt;
# include &lt;valarray&gt;
bool RevSparseHes(void)
{	bool ok = true;
	// Run with Vector equal to four different cases
	// all of which are Simple Vectors with elements of type bool.
	ok &amp;= BoolCases&lt; CppAD::vector  &lt;bool&gt; &gt;();
	ok &amp;= BoolCases&lt; CppAD::vectorBool     &gt;();
	ok &amp;= BoolCases&lt; std::vector    &lt;bool&gt; &gt;(); 
	ok &amp;= BoolCases&lt; std::valarray  &lt;bool&gt; &gt;(); 

	// Run with Vector equal to two different cases both of which are 
	// Simple Vectors with elements of type std::set&lt;size_t&gt;
	typedef std::set&lt;size_t&gt; set;
	ok &amp;= SetCases&lt; CppAD::vector  &lt;set&gt; &gt;();
	ok &amp;= SetCases&lt; std::vector    &lt;set&gt; &gt;(); 

	// Do not use valarray because its element access in the const case
	// returns a copy instead of a reference
	// ok &amp;= SetCases&lt; std::valarray  &lt;set&gt; &gt;(); 

	return ok;
}

</pre>
</font></code>


<hr/>Input File: example/rev_sparse_hes.cpp

</body>
</html>
