<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Simulate a [0,1] Uniform Random Variate</title>
<meta name="description" id="description" content="Simulate a [0,1] Uniform Random Variate"/>
<meta name="keywords" id="keywords" content=" uniform_01 random uniform vector "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_uniform_01_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="speed_utility.xml" target="_top">Prev</a>
</td><td><a href="uniform_01.hpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_utility</option>
<option>uniform_01</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed_utility-&gt;</option>
<option>uniform_01</option>
<option>det_of_minor</option>
<option>det_by_minor</option>
<option>det_by_lu</option>
<option>det_33</option>
<option>det_grad_33</option>
<option>ode_evaluate</option>
<option>sparse_evaluate</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>uniform_01-&gt;</option>
<option>uniform_01.hpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Inclusion</option>
<option>seed</option>
<option>n</option>
<option>x</option>
<option>Vector</option>
<option>Source Code</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>Simulate a [0,1] Uniform Random Variate</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>#&#xA0;include&#xA0;&lt;cppad/speed/uniform_01.hpp&gt;<br/>
</span></font></code><code><font color="blue"><span style='white-space: nowrap'>uniform_01(</span></font></code><i><span style='white-space: nowrap'>seed</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code><code><font color="blue"><span style='white-space: nowrap'>uniform_01(</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
This routine is used to create random values for speed testing purposes.

<br/>
<br/>
<b><big><a name="Inclusion" id="Inclusion">Inclusion</a></big></b>
<br/>
The template function <code><font color="blue">uniform_01</font></code> is defined in the <code><font color="blue">CppAD</font></code>
namespace by including 
the file <code><font color="blue">cppad/speed/uniform_01.hpp</font></code> 
(relative to the CppAD distribution directory).
It is only intended for example and testing purposes, 
so it is not automatically included by
<a href="cppad.xml" target="_top"><span style='white-space: nowrap'>cppad.hpp</span></a>
.

<br/>
<br/>
<b><big><a name="seed" id="seed">seed</a></big></b>
<br/>
The argument <i>seed</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>seed</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>It specifies a seed
for the uniform random number generator.

<br/>
<br/>
<b><big><a name="n" id="n">n</a></big></b>
<br/>
The argument <i>n</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>It specifies the number of elements in the random vector <i>x</i>.

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>.
The input value of the elements of <i>x</i> does not matter.
Upon return, the elements of <i>x</i> are set to values
randomly sampled over the interval [0,1].

<br/>
<br/>
<b><big><a name="Vector" id="Vector">Vector</a></big></b>
<br/>
If <i>y</i> is a <code><font color="blue">double</font></code> value,
the object <i>x</i> must support the syntax
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <i>i</i> has type <code><font color="blue">size_t</font></code> with value less than
or equal 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

.
This is the only requirement of the type <i>Vector</i>.



<br/>
<br/>
<b><big><a name="Source Code" id="Source Code">Source Code</a></big></b>
<br/>
The file 
<a href="uniform_01.hpp.xml" target="_top"><span style='white-space: nowrap'>uniform_01.hpp</span></a>

constraints the source code for this template function.


<hr/>Input File: cppad/speed/uniform_01.hpp

</body>
</html>
