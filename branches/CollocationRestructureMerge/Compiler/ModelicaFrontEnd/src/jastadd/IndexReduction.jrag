/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.jmodelica.util.exceptions.MunkresException;
import org.jmodelica.util.exceptions.IndexReductionException;
import org.jmodelica.util.munkres.MunkresProblem;
import org.jmodelica.util.munkres.MunkresCost;

aspect IndexReduction {
    
    syn boolean FClass.indexReductionErrorsAsWarnings() = false;
    
    public class FClass {
        /**
         * Generate array initialization statements for unknown function arrays.
         */
        public class indexReduction extends Transformation {
            public indexReduction() {
                super("index_reduction");
            }
            
            public void perform() {
                try {
                    // Make sure that we don't do this if any errors have been reported previously
                    breakOnErrors();
                    
                    DAEBiPGraph = computeMatchedDAEBiPGraph();
                    if (shouldReduceIndex(DAEBiPGraph))
                        reduceIndex();
                } catch (IndexReductionException e) {
                    if (indexReductionErrorsAsWarnings())
                        warning(e.getMessage());
                    else
                        error(e.getMessage());
                    // We need to check structure and give warning since a bad system can affect index reduction
                    checkStructure();
                } catch (EquationDifferentiationException e) {
                    error(e.getMessage());
                }
                breakOnErrors();
            }
        }
    }
    
    private boolean FClass.shouldReduceIndex(BiPGraph g) {
        return !g.isComplete() && continuousEquations().size() > 0 && derivativeVariables().size() > 0;
    }
    
    
    /**
     * Perform index reduction on model.
     * 
     * @return  the new graph
     */
    private void FClass.reduceIndex() {
        // Only do index reduction based on real variables and equations
        BiPGraph g = new BiPGraph();
        g.addVariables(this, VAR_TYPES_REAL);
        g.addEquations(continuousEquations(), VAR_TYPES_REAL, "eq_", 1);
        log.info("Index reduction starts");
        log.info(g);
        g.maximumMatching(true);    
        log.info("DAE system real variables only");
        log.info(g.printMatchingObj());
        
        IndexReductionResult result = null;
        result = g.reduceIndex(false);
        
        // Add the new equations
        // Replace selected dummy derivatives with algebraics
        // Enable rewrite of identifiers? (not necessary?)
        for (FAbstractEquation fe : result.getEquations()) {
            addFEquation((FAbstractEquation)fe.fullCopy());
        }
        
        Collection<FVariable> l = new ArrayList<FVariable>();
        
        for (FVariable fv : getFVariables()) {
            if (result.getDummyDerivatives().contains(fv)) {
                if (fv instanceof FHDerivativeVariable) {
                    FHDummyDerivativeVariable fdv = new FHDummyDerivativeVariable(fv.getFVisibilityType().fullCopy(),
                        fv.getFTypePrefixVariability().fullCopy(), fv.getFQName().fullCopy(),
                        ((FHDerivativeVariable)fv).getOrder());
                    l.add(fdv);
                } else if (fv instanceof FDerivativeVariable) {
                    FDummyDerivativeVariable fdv = new FDummyDerivativeVariable(fv.getFVisibilityType().fullCopy(),
                        fv.getFTypePrefixVariability().fullCopy(), fv.getFQName().fullCopy());
                    l.add(fdv);
                } else {
                    l.add(fv);
                }
            } else if (fv instanceof FHDerivativeVariable) {
                // Convert higher order derivative variables into
                // first order derivatives, generating variables for each step
                FHDerivativeVariable fhdv = (FHDerivativeVariable) fv;
                FQName fqnPrev = fv.getFQName().fullCopy();
                for (int i = 1; i < fhdv.getOrder(); i++) {
                    FQName fqn = fv.getFQName().createDerPrefixedName(i);
                    l.add(new FRealVariable(new FPublicVisibilityType(),
                        fv.getFTypePrefixVariability().fullCopy(), fqn.fullCopy()));
                    // Derivative variable is added later
                    addFEquation(new FEquation(new FIdUseExp(fqn), new FDerExp(fqnPrev.fullCopy())));
                    fqnPrev = fqn;
                }
            } else if (!fv.isDerivativeVariable()) {
                // Derivative variables are added later
                l.add(fv);
            }
        }

        setFVariableList(new List<FVariable>(l));

        
        log.info(this);
        
        enableFHDerRewrite();

        // flushAllRecursiveClearFinal must be used here in order to
        // trigger rewrites of FDerExp -> FDummyDerExp
        root().flushAllRecursiveClearFinal();

        aliasElimination.apply();
        // Add derivative variables
        setFDerivativeVariables.apply();

        root().flushAllRecursive();

        log.info(this);
        log.info(aliasManager.printAliasSets());

        log.info("States:");
        Set<FVariable> stateSelectAlwaysVars = new HashSet<FVariable>();
        for (FVariable fv : variables()) {
            if (fv.aliasSet() != null) {
                for (AliasManager.AliasVariable av : fv.aliasSet()) {
                    if (av.getFVariable().stateSelectAttribute() == FRealVariable.StateSelect.ALWAYS)
                        stateSelectAlwaysVars.add(av.getFVariable());
                }
            } else if (fv.stateSelectAttribute() == FRealVariable.StateSelect.ALWAYS) {
                stateSelectAlwaysVars.add(fv);
            }
        }
        for (FVariable fv : differentiatedRealVariables()) {
            if (fv.stateSelectAttribute() == FRealVariable.StateSelect.NEVER)
                fv.warning(fv.name() + " has stateSelect=never, but must be selected as state");
            stateSelectAlwaysVars.remove(fv);
            log.info(new PrettyPrintDelegate(fv, "  "));
        }
        for (FVariable fv : stateSelectAlwaysVars) 
            if (fv.isSetInReinit())
                fv.error(fv.name() + " is assigned in reinit(), but could not be selected as state");
            else
                fv.warning(fv.name() + " has stateSelect=always, but could not be selected as state");

        DAEBiPGraph = computeMatchedDAEBiPGraph();
        log.info("BiPGraph after index reduction");
        log.info(DAEBiPGraph);
        log.info("BiPGraph matching after index reduction");
        log.info(DAEBiPGraph.printMatchingObj());

        if (!DAEBiPGraph.isComplete()) {
            // If we get here, index reduction failed
            throw new IndexReductionException();
        }
    }

    /**
     * Check if this variable is marked as being set by a reinit().
     */
    syn boolean FAbstractVariable.isSetInReinit() = false;
    eq FRealVariable.isSetInReinit() {
        FAttribute a = findAttribute(FAttribute.STATE_SELECT);
        return a != null && a.isInternal();
    }

    
    /**
     * Create a new FQName that is a copy of this one, prefixed with "_der" <code>order</code> times in the last name part.
     */
    public FQName FQName.createDerPrefixedName(int order) {
        FQName pref = copyPrefix();
        String name = lastActualPartName();
        if (name.startsWith("_"))
            name = name.substring(1);
        name = createDerPrefixString(order) + name;
        FQName der;
        if (hasFArraySubscripts())
            der = pref.copyAndAppend(name, getFArraySubscripts().fullCopy());
        else
            der = pref.copyAndAppend(name);
        der.scalarized = true;
        return der;
    }
    
    /**
     * Create a new FQName that is a copy of this one, prefixed with "_der" <code>order</code> times in the first name
     * part. This function is used in functions, instead of createDerPrefixedName(int).
     */
    public FQName FQName.createDerPrefixedFunctionName(int order) {
        FQName suf = copySuffix();
        String name = firstActualPartName();
        if (name.startsWith("_"))
            name = name.substring(1);
        name = createDerPrefixString(order) + name;
        FQName der = suf.copyAndPrepend(name);
        if (hasFArraySubscripts())
            der = der.copyAndAddFas(getFArraySubscripts().fullCopy());
        der = der.asFQNameFull();
        der.scalarized = true;
        return der;
    }
    
    public static final int BiPGraph.MAX_N_EQ_DIFFS = 5;
    
    private static Map<Eq, FAbstractEquation> BiPGraph.differentiateEquations(Collection<Eq> eqns) {
        Map<Eq, FAbstractEquation> diffedEqnsMap = new HashMap<Eq, FAbstractEquation>();
        for (Eq eqn : eqns) {
            FAbstractEquation fEqn = eqn.getEquation();
            if (eqn.numDifferentiations() > MAX_N_EQ_DIFFS)
                throw new IndexReductionException("Maximum number of differentiations reached");
            FAbstractEquation diffedFEqn = null;
            // Look through member equations and check if the differentiated equation already exists
            for (Eq groupMember : eqn.getGroupMembers())
                if (groupMember.getMeDifferentiated() != null)
                    diffedFEqn = groupMember.getMeDifferentiated().getEquation();
            if (diffedFEqn == null) {
                // It does not exsit, create it.
                Collection<FAbstractEquation> diffedFEqns = fEqn.diff("time");
                if (diffedFEqns.size() != 1)
                    throw new UnsupportedOperationException("Differentiation of equations should result" +
                            " in a single equation when differentiating on time");
                else
                    diffedFEqn = diffedFEqns.iterator().next();
            } else {
                // It already exists, we need to flush cached value later, indicate with null.
                diffedFEqn = null;
            }
            diffedEqnsMap.put(eqn, diffedFEqn);
        }
        return diffedEqnsMap;
    }
    
    private boolean BiPGraph.removePreferDerivatives(Collection<Eq> eqns, Stack<Eq> workStack) {
        boolean skip = false;
        for (Eq eqn : eqns) {
            for (FVariable fv : eqn.getEquation().variables()) {
                if (fv.stateSelectAttribute() != FAbstractVariable.StateSelect.PREFER)
                    continue;
                if (fv.getMeDifferentiated() == null)
                    continue;
                Var derVar = getVariable(fv.getMeDifferentiated().name());
                if (derVar == null)
                    continue;
                if (derVar.getMatching() != null)
                    continue;
                ASTNode.log.info("Removing derivative variable that was introduced due to StateSelect.Prefer, \"%s\"", derVar);
                removeVariable(derVar);
                Var var = addVariable(fv.name(), fv);
                ASTNode.log.info("Introducing the original variable, \"%s\"", var);
                for (FIdUseExp use : fv.uses()) {
                    FAbstractEquation fae = use.myFEquation();
                    if (fae == null)
                        continue;
                    for (Eq useEqn : getEquations(fae))
                        useEqn.addVariable(var);
                }
                skip = true;
                workStack.push(eqn);
            }
        }
        return skip;
    }
    
    public IndexReductionResult BiPGraph.reduceIndex(boolean resetMatching) {
        IndexReductionResult result = new IndexReductionResult();
        if (resetMatching) {
            reset();
        }
        FClass fclass = null;
        Stack<Eq> eqToMatchStack = new Stack<Eq>();
        for (Eq e : getEquations()) {
            if (e.getEquation().isContinuous()) {
                eqToMatchStack.push(e);
                fclass = e.getEquation().myFClass();
            }
        }
        Collections.reverse(eqToMatchStack);
        
        // If there are no equations, we cannot do index reduction.
        if (eqToMatchStack.isEmpty())
            throw new IndexReductionException("No continuous equations was given");
        
        java.util.List<FVariable> newDiffedVars = new ArrayList<FVariable>();
        // Algoritm step numbering from the original Pantelides paper
        // Loop over all equations (step 3)
        while (!eqToMatchStack.empty() ){
            Eq eqToMatch = eqToMatchStack.pop(); // Step 3a
            if (eqToMatch.getMatching() != null || eqToMatch.getMeDifferentiated() != null)
                continue;
            
            // If the equation is not matched
            
            // Since the states are not added to the graph,
            // Step 3b-1 is skipped and is done at the end of 
            // the iteration 
            lightReset(); // Step 3b-2
            // Find an augmenting path, step 3b-4
            //n_ind = 0;
            boolean pathFound = augmentPath(eqToMatch);
            Collection<Eq> visitedEquations = getVisitedEquations();
            Collection<Var> visitedVariables = getVisitedVariables();
            ASTNode.log.info("*************");
            ASTNode.log.info("Looking for augmented path starting in equation: %s", eqToMatch);
            if (pathFound) {
                ASTNode.log.info("Path found!");
            } else {
                ASTNode.log.info("Path not found!");
            }
            ASTNode.log.info("Visisted equations:");
            for (Eq ee : visitedEquations) {
                ASTNode.log.info("  %s", ee.getEquation());
            }
            ASTNode.log.info("Visisted variables:");
            for (Var vv : visitedVariables) {
                ASTNode.log.info("  %s", vv.toString());
            }
            
            if (!pathFound) { // Step 3b-5
                // First, differentiate all equations to see if it is possible.
                // Remove derivative if we find StateSelect.Prefer variables that wasn't possible to select due to
                // equation that can't be differentiated
                Map<Eq, FAbstractEquation> diffedEqnsMap;
                try {
                    diffedEqnsMap = differentiateEquations(visitedEquations);
                } catch (EquationDifferentiationException e) {
                    ASTNode.log.info("Unable to differentiate equation:\n%s", e.equation);
                    if (removePreferDerivatives(visitedEquations, eqToMatchStack))
                        continue;
                    else
                        throw e;
                }
                for (Var var : visitedVariables) { // Loop over all visited variables, Step 3b-5i
                    // Create a new differentiated variable
                    FVariable diffedFVar = var.getVariable().createFDerivativeVariable();
                    newDiffedVars.add(diffedFVar);
                    // Add the differentiated variable to the graph 
                    Var diffedVar = addVariable(diffedFVar.name(), diffedFVar);
                    // Set the "A vector" in the paper: 
                    // a reference from the visited variable to its 
                    // differentiated counterpart
                    var.setMeDifferentiated(diffedVar); 
                    diffedVar.setMeIntegrated(var);
                }
                for (Eq eqn : visitedEquations) { // Step 3b-5ii
                    FAbstractEquation diffedFEqn = diffedEqnsMap.get(eqn);
                    if (diffedFEqn != null) {
                        result.addEquation(diffedFEqn);
                    } else {
                        // Already differentiated, look through member equations
                        for (Eq groupMember : eqn.getGroupMembers())
                            if (groupMember.getMeDifferentiated() != null)
                                diffedFEqn = groupMember.getMeDifferentiated().getEquation();
                        // It already exists we need to flush cached values since we might have added new variables
                        diffedFEqn.flushAllRecursive();
                    }
                    ASTNode.log.info("About to add equation: %s", diffedFEqn);
                    // Sets that indicates the variables in the lhs and rhs.
                    Set<Var> rhsVars = Collections.emptySet();
                    Map<Var, Integer> lhsVars = Collections.emptyMap();
                    if (diffedFEqn.numScalarEquations() > 1) {
                        rhsVars = lookupVarsInSet(diffedFEqn.FIdUseExpsInRHS());
                        lhsVars = new HashMap<Var, Integer>();
                        int i = 0;
                        for (Var var : lookupVarsInSet(diffedFEqn.FIdUseExpsInLHS()))
                            lhsVars.put(var, i++);
                        if (diffedFEqn.lhsVarsAreSolved())
                            rhsVars.removeAll(lhsVars.keySet());
                    }
                    // Add the differentiated equation
                    int groupNumber = eqn.groupNumber();
                    Eq diffedEqn = addEquation("eq_" + (getEquations().size() + 1), diffedFEqn, groupNumber, diffedFEqn.variability());
                    
                    // Add edges
                    Collection<Var> variables = lookupVarsInSet(diffedFEqn.findFIdUseExpsInTree());
                    for (Var var : variables) {
                        Integer pos = lhsVars.get(var);
                        if (var != null && (pos == null || pos == groupNumber || rhsVars.contains(var))) {
                            ASTNode.log.info("*** %s", var);
                            addEdge(diffedEqn, var);
                        }
                    }
                    
                    // Set a reference from the visited equation to
                    // its differentiated counterpart
                    eqn.setMeDifferentiated(diffedEqn);
                    diffedEqn.setMeIntegrated(eqn);
                    
                    // TODO: We might need to add this equation to its existing groupmembers?
                    ASTNode.log.info("Equation added");
                }
                for (Var var : visitedVariables) {
                    // Set matchings, Step 3b-5iii
                    Eq eqn = var.getMatching();
                    match(eqn.getMeDifferentiated(), var.getMeDifferentiated());
                    // Remove variables that have been differentiated, Step 3b-1
                    removeVariable(var);
                }
                // Set the next equation to start matching from, 
                // Step 3b-5iv
                // Push all differentiated equations in the group to the stack
                // of equations to be matched
                for (Eq groupMemberEquation : eqToMatch.getGroupMembers())
                    eqToMatchStack.push(groupMemberEquation.getMeDifferentiated());
                
                ASTNode.log.info("Graph after addition of equations and variables:");
                ASTNode.log.info(this);
            }
            ASTNode.log.info("Matching:");
            ASTNode.log.info(printMatchingObj());
            ASTNode.log.info("*************");
        }
        fclass.addFDerivativeVariables(newDiffedVars);
        for (FAbstractEquation eqn : result.getEquations())
            eqn.flushAllRecursive();

        // Remove equations that are not connected
        java.util.List<Eq> eqToRemove = new ArrayList<Eq>();
        for (Eq e : getEquations())
            if (e.getVariables().size() == 0)
                eqToRemove.add(e);
        for (Eq e : eqToRemove)
            removeEquation(e);
        
        ASTNode.log.info("Index reduction done!");
        ASTNode.log.info("Matching:");
        ASTNode.log.info(printMatchingObj());
        
        if (result.getEquations().isEmpty()) {
            ASTNode.log.info("Index reduction did not change the system. Munkres is skipped.");
            return result;
        }
        // Selection of dummy derivatives
        /*
            The algorithm is implemented differently than in the original paper
            by Mattsson and S\ufffdderlind. In the paper, each block is treated
            individually by selecting all dummy derivatives of one block
            before proceeding to the next block. This approach, however, makes
            it hard to handle user preferences for state selection as defined
            by the stateSelect attribute. This is because selection of
            dummy derivatives in one block may restrict the choices in 
            following blocks, even though a global analysis would yield that
            it would indeed be possible to satisfy user selection.
            
            Therefore, all equations in blocks containing differentiated
            equations are treated in one block.
            
            NOTE: This algorithm is still a prototype and it needs to be refactored.
            This will be done when the final algorithm have been settled.
        */
        
        BLT blt = computeBLT(false, false, false);
    
        ASTNode.log.info(blt);

        // Lists of equations and variables, notation is the same as in the 
        // paper
        java.util.List<FAbstractEquation> g = new ArrayList<FAbstractEquation>();
        
        java.util.List<FAbstractEquation> h = new ArrayList<FAbstractEquation>();

        java.util.List<FVariable> z = new ArrayList<FVariable>();

        java.util.List<FVariable> zHat = new ArrayList<FVariable>();

        // Collect all blocks that contain differentiated equations
        for (AbstractEquationBlock eb : blt) {
            boolean add = false;
            if (!eb.differentiatedEquations().isEmpty()) {
                add = true;
            } else {
                for (FVariable var : eb.allVariables()) {
                    if (var.isDerivativeVariable()) {
                        add = true;
                        break;
                    }
                }
            }
            if (add) {
                ASTNode.log.info(" ********* Found block containing diffed equations ***********");
                ASTNode.log.info(eb);
                
                // Step 1: Initialize, add all equations in blocks
                // having differentiated equations
                g.addAll(eb.allEquations());
                
                // Step 2: Extract all active variables, i.e., highest order 
                // derivatives for each variable
                for (FVariable fv : eb.allVariables())
                    z.add(fv);
            }
        }
        
        // Initialize variable to keep track of there are still 
        // differentiated equations
        boolean remainingDifferentiatedEquations = true;
        
        // Iterate
        while (remainingDifferentiatedEquations) {
            ASTNode.log.info("----------------");
            // Step 3: Extract differentiated equations 
            h = new ArrayList<FAbstractEquation>();
            for (FAbstractEquation fe : g) {
                if (fe.getMeIntegrated()!=null) {
                    h.add(fe);
                }
            }
            
            // Step 4: Select independent columns.
            // In a first attempt we don't add variables with StateSeclect.prefer
            // If it is possible to find a matching, then we proceed. If not, then
            // We start over and all all variables, also the ones with StateSelect.prefer
            // This algorithm is a first approximation of what we need, since we must also
            // have to support avoid and never.
            BiPGraph gd = new BiPGraph(); 
                        
            // Add variables to the graph
            for (FVariable fv : z) {
                gd.addVariable(fv.name(),fv);
            }
                
            int k = 1;
            Eq eqn = null;
            Var v = null;
            // Add the equations to the graph
            for (FAbstractEquation e : h) {
            
                int n_eq = e.numScalarEquations();
                
                for (int i=0;i<n_eq;i++) {
                    ASTNode.log.info("*** %s", e);
                    eqn = gd.addEquation("eq_" + k, e, i, e.variability());
                    k++;
                
                    for (FVariable y : e.variables()) { 
                        //FRealVariable frv = (FRealVariable)((FRealVariable)y).getMeIntegrated();
                        ASTNode.log.info(" ** %s %s", y, z.contains(y));
                        if (z.contains(y)) {
                            v = gd.getVariable(y.name()); 
                            gd.addEdge(eqn,v);
                        } 
                    } 
                }
            }
            
            ASTNode.log.info(gd);
            
            // Run matching
            // Compute weights for variables based on linearity
            Map<Var, BiPGraphCost> weights = new HashMap<Var, BiPGraphCost>();
            for (Var var : gd.getVariables()) {
                FVariable fv = var.getVariable();
                int linearity = calculateLinearityWeight(g, fv);
                if (fv.getMeIntegrated() != null)
                    linearity += calculateLinearityWeight(g, fv.getMeIntegrated());
                boolean fixed = fv.getTopIntegrated().fixedAttribute();
                BiPGraphCost weight = new BiPGraphCost(fv.stateSelection(), fixed, linearity);
                ASTNode.log.info("Weight: %s %s", var, weight);
                weights.put(var, weight);
            }
            
            gd.matchMunkres(weights);
            ASTNode.log.info(gd.printMatchingObj());
            
            // The matched variables are selected as dummy derivatives
            for (Var vv : gd.getMatchedVariables()) {
                zHat.add(vv.getVariable());
            }
            
            // Step 5: prepare for next iteration
            // Get the "integrated" equations
            g = new ArrayList<FAbstractEquation>();
            for (FAbstractEquation ee : h) {
                g.add(ee.getMeIntegrated());
            }
            
            // Get the "integrated" variables 
            z = new ArrayList<FVariable>();
            for (FVariable fv : zHat)
                z.add(fv.getMeIntegrated());
            
            // Check if there are remaining differentiated equations
            remainingDifferentiatedEquations = false;
            for (FAbstractEquation fe : g) {
                if (fe.getMeIntegrated()!=null) {
                    remainingDifferentiatedEquations = true;
                }
            }
            
            ASTNode.log.info(" --- Equations in iteration: --- ");
            ASTNode.log.info(" Equations: ");
            for (FAbstractEquation fe : g) {
                ASTNode.log.info("   %s", fe);
            }

            ASTNode.log.info(" Dummy derivatives selected in iteration: ");
            for (FVariable fv : zHat) {
                result.addDummyDerivative(fv);
                ASTNode.log.info("   %s", fv);
            }
            ASTNode.log.info("");
        }
        
        ASTNode.log.info(" Dummy derivatives: ");
        for (FVariable fv : zHat) {
            result.addDummyDerivative(fv);
            ASTNode.log.info("   %s", fv);
        }
        ASTNode.log.info("");
        
        return result;
    }
    
    private int BiPGraph.calculateLinearityWeight(Collection<FAbstractEquation> equations, FVariable variable) {
        int weight = 0;
        for (FAbstractEquation feq : equations) {
            try {
                if (!feq.referencedFVariables().contains(variable))
                    continue;
                Collection<FAbstractEquation> feqds = feq.diff(variable.name());
                FTypePrefixVariability variability = ASTNode.fConstant();
                for (FAbstractEquation feqd : feqds)
                    variability = variability.combine(feqd.variability());
                if (!variability.lessOrEqual(ASTNode.fParameter()))
                    weight += 2;
                else if (!variability.lessOrEqual(ASTNode.fConstant()))
                    weight += 1;
            } catch(EquationDifferentiationException e) {}
        }
        return weight;
    }
    
    
    syn FRealVariable.StateSelect FVariable.stateSelectAttribute() = FRealVariable.StateSelect.DEFAULT;
    eq FRealVariable.stateSelectAttribute() = isTemporary() ? FRealVariable.StateSelect.AVOID : StateSelect.values()[stateSelectAttributeInt() - 1];
    
    public class FAbstractVariable {
        public enum StateSelect {
            NEVER   (BiPGraph.BiPGraphCost.STATE_SELECT_NEVER_WEIGHT_POSITION), 
            AVOID   (BiPGraph.BiPGraphCost.STATE_SELECT_AVOID_WEIGHT_POSITION), 
            DEFAULT (BiPGraph.BiPGraphCost.STATE_SELECT_DEFAULT_WEIGHT_POSITION), 
            PREFER  (BiPGraph.BiPGraphCost.STATE_SELECT_PREFER_WEIGHT_POSITION), 
            ALWAYS  (BiPGraph.BiPGraphCost.STATE_SELECT_ALWAYS_WEIGHT_POSITION);
            
            private int weightPos;
            
            private StateSelect(int weightPos) {
                this.weightPos = weightPos;
            }
            
            public int weightPos() {
                return weightPos;
            }
        }
    }
    
    syn StateSelect FAbstractVariable.stateSelection() {
        throw new UnsupportedOperationException("Unable to get state select from variable type " + getClass().getSimpleName());
    }
    eq FVariable.stateSelection() {
        FRealVariable frv = (FRealVariable) getMeIntegrated();
        return (frv == null) ? StateSelect.DEFAULT : frv.stateSelectAttribute();
    }
    
    public class BiPGraph{
        
        public static class BiPGraphCost implements MunkresCost<BiPGraphCost> {
            
            public static BiPGraphCost UNMATCHED_WEIGHT = new BiPGraphCost();
            
            private static final int NUM_WEIGHTS = 7;
            public static final int STATE_SELECT_ALWAYS_WEIGHT_POSITION  = 0;
            public static final int STATE_SELECT_PREFER_WEIGHT_POSITION  = 1;
            public static final int STATE_SELECT_DEFAULT_WEIGHT_POSITION = 2;
            public static final int STATE_SELECT_AVOID_WEIGHT_POSITION   = 3;
            public static final int STATE_SELECT_NEVER_WEIGHT_POSITION   = 4;
            private static final int FIXED_WEIGHT_POSITION               = 5;
            private static final int LINEARITY_WEIGHT_POSITION           = 6;
            
            private boolean unmatched = false;
            private int[] weights = new int[NUM_WEIGHTS];
            
            private BiPGraphCost() {
                unmatched = true;
            }
            
            public BiPGraphCost(FAbstractVariable.StateSelect stateSelect, boolean fixed, int linearity) {
                weights[stateSelect.weightPos()] += 1;
                weights[FIXED_WEIGHT_POSITION] += fixed ? 1 : 0;
                weights[LINEARITY_WEIGHT_POSITION] += linearity;
            }
            
            @Override
            public int compareTo(BiPGraphCost other) {
                int diff = new Boolean(unmatched).compareTo(other.unmatched);
                if (diff != 0)
                    return diff;
                for (int i = 0; i < weights.length; i++) {
                    diff = new Integer(weights[i]).compareTo(other.weights[i]);
                    if (diff != 0)
                        return diff;
                }
                return 0;
            }
            
            @Override
            public void subtract(BiPGraphCost other) {
                unmatched |= other.unmatched;
                for (int i = 0; i < weights.length; i++)
                    weights[i] -= other.weights[i];
            }
            
            @Override
            public void add(BiPGraphCost other) {
                unmatched |= other.unmatched;
                for (int i = 0; i < weights.length; i++)
                    weights[i] += other.weights[i];
            }
            
            @Override
            public boolean isZero() {
                if (unmatched)
                    return false;
                for (int i = 0; i < weights.length; i++)
                    if (weights[i] != 0)
                        return false;
                return true;
            }
            
            @Override
            public BiPGraphCost copy() {
                BiPGraphCost copy = new BiPGraphCost();
                copy.unmatched = unmatched;
                for (int i = 0; i < weights.length; i++)
                    copy.weights[i] = weights[i];
                return copy;
            }
            
            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("(");
                sb.append(unmatched);
                sb.append(" ");
                for (int weight : weights) {
                    sb.append(", ");
                    sb.append(weight);
                }
                sb.append(")");
                return sb.toString();
            }
            
            public boolean isUnmatched() {
                return unmatched;
            }
            
            public static BiPGraphCost zeroCost() {
                BiPGraphCost cost = new BiPGraphCost();
                cost.unmatched = false;
                return cost;
            }
        }
    }

    public void BiPGraph.matchMunkres(Map<Var, BiPGraphCost> weights) {

        if (equationMap.size()==0 || variableMap.size()==0) {
            throw new MunkresException("Empty set of equations or variables was given to the munkres algorithm");
        }

        BiPGraphCost cost[][] = new BiPGraphCost[equationMap.size()][variableMap.size()];

        int i = 0;
        
        for (Eq e : getEquations()) {
            int j = 0;
            for (Var var : variableMap.values())
                cost[i][j++] = e.getVariables().contains(var) ? weights.get(var) : BiPGraphCost.UNMATCHED_WEIGHT;
            i++;
        }
        
        MunkresProblem<BiPGraphCost> munk = new MunkresProblem<BiPGraphCost>(cost);
        int[][] result = munk.solve();
        BiPGraphCost opt_cost = BiPGraphCost.zeroCost();
        ASTNode.log.info("Munkres result:");
        Eq[] eqs = equationMap.values().toArray(new Eq[equationMap.size()]);
        Var[] vars = variableMap.values().toArray(new Var[variableMap.size()]);
        boolean failed = false;
        for (i=0;i<result.length;i++) {
            opt_cost.add(cost[result[i][0]][result[i][1]]);
            Eq eqn = eqs[result[i][0]];
            Var var = vars[result[i][1]];
            match(eqn, var);
            ASTNode.log.info("%s: %s %s", eqn, var, cost[result[i][0]][result[i][1]]);
        }
        ASTNode.log.info("Optimal cost: %s", opt_cost);
        if (opt_cost.isUnmatched()) {
            throw new MunkresException("Munkres algorithm was unable to find a matching");
        }
    }
    
    public class IndexReductionResult {

        private java.util.List<FAbstractEquation> addedEquations;
        private java.util.List<FVariable> dummyDerivatives;

        public IndexReductionResult() {
            addedEquations = new ArrayList<FAbstractEquation>();
            dummyDerivatives = new ArrayList<FVariable>();
        }
        
        public void addEquation(FAbstractEquation fe) {
            addedEquations.add(fe); 
        }

        public void addDummyDerivative(FVariable fv) {
            dummyDerivatives.add(fv);   
        }

        public java.util.List<FAbstractEquation> getEquations() {
            return addedEquations;
        }

        public java.util.List<FVariable> getDummyDerivatives() {
            return dummyDerivatives;
        }
    }
    
}