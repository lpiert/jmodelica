/*
    Copyright (C) 2009-2013 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect InteractiveFMU{
	
    public class FClass {
        /**
         * Run matchings and BLT.
         */
        public class interactiveFMU extends Transformation {
            public interactiveFMU() {
                super("interactive_fmu");
            }
            
            public void perform() {
                computeInteractiveFMU();
                daeBLT.complianceCheck(ErrorCheckType.COMPILE);
                breakOnErrors();
            }
        }
    }
    
    public void FClass.computeInteractiveFMU() {
		// Convert equations
		Collection<FAbstractEquation> localIterations = new HashSet<FAbstractEquation>();
		getDAEInitBLT().computeInteractiveFMU(this, new Enumerator(), localIterations);
		
		// Flush cache
		root().flushAllRecursive();
		
		// Print the model
		log.info("FClass after interactiveFMU:");
		log.info(this);
		
		// Eliminate aliases
		aliasElimination.apply();
		
		// Redo BLT
		computeMatchingsAndBLT(true);
		
		// Check for unsolved blocks
		boolean allSolved = true;
		for (AbstractEquationBlock aeb : daeBLT) {
			if (aeb.isSolvable())
				continue;
			for (FAbstractEquation fae : aeb.allEquations())
				if (!localIterations.contains(fae))
					allSolved = false;
		}
		for (AbstractEquationBlock aeb : daeInitBLT) {
			if (aeb.isSolvable())
				continue;
			for (FAbstractEquation fae : aeb.allEquations())
				if (!localIterations.contains(fae))
					allSolved = false;
		}
		if (!allSolved)
			error("Generation of interactive FMU failed, system still contains unsolved blocks, contact JModelica.org developer!");
	}
	
	public void BLT.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
		for (AbstractEquationBlock subBlock : this)
			subBlock.computeInteractiveFMU(fclass, indexEnumerator, localIterations);
	}
	
	public abstract void AbstractEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations);
	
	public void EquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
		for (SimpleEquationBlock subBlock : unsolvedBlocks)
			subBlock.computeInteractiveFMU(fclass, indexEnumerator, localIterations);
	}
	
	public void SimpleEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
		// Default empty implementation
	}

    public void UnsolvedScalarEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
        int index = indexEnumerator.next();
        String iterName = "iter_" + index;
        String resName = "res_" + index;
        FEquation equation = equation();

        // Make iteration variable into input
        fv.setFTypePrefixInputOutput(new FInput());

        // Add iter var
        FVariable iterVar = (FVariable) fv.createEmptyNode();
        iterVar.setFVisibilityType(new FPublicVisibilityType());
        iterVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
        iterVar.setFQName(new FQNameString(iterName));
        iterVar.setFStringComment(new FStringComment(fv.name()));
        fclass.addFVariable(iterVar);

        // Add residual var
        FVariable resVar = (FVariable) fv.createEmptyNode();
        resVar.setFVisibilityType(new FPublicVisibilityType());
        resVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
        resVar.setFQName(new FQNameString(resName));
        resVar.setFTypePrefixInputOutput(new FOutput());
        fclass.addFVariable(resVar);

        // Add alias equation
        FEquation aliasEquation = new FEquation(equation.getType(), new FIdUseExp(iterName), new FIdUseExp(fv.name()));
        fclass.addFEquation(aliasEquation);

        // Convert old equation into residual equation
        FExp oldLhs = equation.getLeft();
        FExp oldRhs = equation.getRight();
        equation.setLeft(new FIdUseExp(resName));
        equation.setRight(new FSubExp(oldLhs, oldRhs));
        if (equation.name() != null) {
            if (!fclass.lookupFV(equation.name()).isUnknown()) {
                equation.error("Unable to introduce equation name alias for equation with name '" + equation.name() + "'");
            } else {
                String nameName = equation.name();
                // Alias variable for the residual variable
                FVariable nameVar = (FVariable) fv.createEmptyNode();
                nameVar.setFVisibilityType(new FPublicVisibilityType());
                nameVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
                nameVar.setFQName(new FQNameString(nameName));
                fclass.addFVariable(nameVar);
                
                // Alias equation for the residual variable
                FEquation nameAliasEquation = new FEquation(equation.getType(), new FIdUseExp(resName), new FIdUseExp(nameName));
                fclass.addFEquation(nameAliasEquation);
            }
            
        }
    }

    public void UnsolvedFunctionCallEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
        Iterator<FVariable> activeVarIt = activeVars.iterator();
        Iterator<Integer> assignedIndicesIt = assignedIndices.iterator();
        FFunctionCallEquation equation = equation();
        java.util.List<FIdUseExp> assignedFIdUses = new ArrayList<FIdUseExp>(equation.FIdUseExpsInLHS());
        while (activeVarIt.hasNext() && assignedIndicesIt.hasNext()) {
            FVariable fv = activeVarIt.next();
            int assignedIndex = assignedIndicesIt.next();
            FIdUseExp oldLhs = assignedFIdUses.get(assignedIndex);
            int index = indexEnumerator.next();
            String iterName = "iter_" + index;
            String resName = "res_" + index;
            String tmpName = fclass.nextTempVarName();

            // Make iteration variable into input
            fv.setFTypePrefixInputOutput(new FInput());

            // Add iter var
            FVariable iterVar = (FVariable) fv.createEmptyNode();
            iterVar.setFVisibilityType(new FPublicVisibilityType());
            iterVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
            iterVar.setFQName(new FQNameString(iterName));
            iterVar.setFStringComment(new FStringComment(fv.name()));
            fclass.addFVariable(iterVar);

            // Add residual var
            FVariable resVar = (FVariable) fv.createEmptyNode();
            resVar.setFVisibilityType(new FPublicVisibilityType());
            resVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
            resVar.setFQName(new FQNameString(resName));
            resVar.setFTypePrefixInputOutput(new FOutput());
            fclass.addFVariable(resVar);

            // Add alias equation
            FEquation aliasEquation = new FEquation(equation.getType(), new FIdUseExp(iterName), new FIdUseExp(fv.name()));
            fclass.addFEquation(aliasEquation);

            // Add tmp variable
            FVariable tmpVar = (FVariable) fv.createEmptyNode();
            tmpVar.setFVisibilityType(new FPublicVisibilityType());
            tmpVar.setFTypePrefixVariability(fv.getFTypePrefixVariability().fullCopy());
            tmpVar.setFQName(new FQNameString(tmpName));
            fclass.addFVariable(tmpVar);

            // Replace old equation
            oldLhs.replaceMe(new FIdUseExp(tmpName));

            // Create residual equation
            FEquation resEquation = new FEquation(equation.getType(), new FIdUseExp(resName), new FSubExp(new FIdUseExp(tmpName),oldLhs));
            fclass.addFEquation(resEquation);
        }
    }
	
	public void NumericallySolvedScalarEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
		localIterations.add(equation());
	}
	
	public void TornEquationBlock.computeInteractiveFMU(FClass fclass, Enumerator indexEnumerator, Collection<FAbstractEquation> localIterations) {
		for (SimpleEquationBlock solvedBlock : solvedBlocks())
			solvedBlock.computeInteractiveFMU(fclass, indexEnumerator, localIterations);
		for (SimpleEquationBlock unsolvedBlock : unsolvedBlocks())
			unsolvedBlock.computeInteractiveFMU(fclass, indexEnumerator, localIterations);
	}
	
}