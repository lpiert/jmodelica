import java.io.PrintStream;


/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect CCodeGen {
	
	class CPrettyPrinter extends Printer {
 		public void toString(ASTNode node, PrintStream str, String indent, Object o) { 
 			node.prettyPrint_C(this, str, indent, o); 
 		}
	}
	
	public String ASTNode.prettyPrint_C(String indent) {
		StringOutputStream os = new StringOutputStream();
		PrintStream str = new PrintStream(os);
		prettyPrint_C(str,indent,null);
		return os.toString();
	}

	public String ASTNode.prettyPrint_C(String indent,Object o) {
		StringOutputStream os = new StringOutputStream();	
		PrintStream str = new PrintStream(os);
		prettyPrint_C(str,indent,o);
		return os.toString();
	}

	public void ASTNode.prettyPrint_C(PrintStream str,String indent) {
 		prettyPrint_C(new CPrettyPrinter(),str,indent,null);
	}

	public void ASTNode.prettyPrint_C(PrintStream str,String indent, Object o) {
 		prettyPrint_C(new CPrettyPrinter(),str,indent,o);
	}
	
	public void ASTNode.prettyPrint_C(Printer p, PrintStream str, String indent, Object o) {
		prettyPrint(p,str,indent,o);
		/*for(int i = 0; i < getNumChild(); i++)
   			p.toString(getChild(i),str,indent,o); // distpatch through Printer
	    */
	}
	
	public void FPowExp.prettyPrint_C(Printer p, PrintStream str, String indent, Object o) {
		str.print("pow(");
		p.toString(getLeft(),str,indent,o); 
		str.print(",");
		p.toString(getRight(),str,indent,o);
		str.print(")");
	}
		
	public void FIdUseExp.prettyPrint_C(Printer p, PrintStream str, String indent, Object o) {
		str.print("_"+nameUnderscore()+"_");
	}

	public void FDivExp.prettyPrint_C(Printer p, PrintStream str, String indent, Object o) {
		str.print("jmi_divide(");
		p.toString(getLeft(),str,indent,o);
		str.print(","); 
		p.toString(getRight(),str,indent,o);
		str.print(","); 
		str.print("\"Divide by zero: "+prettyPrint("")+"\")");
	}
	
	syn String FLogBinExp.macroC() = "ERROR_IN_CONDITIONAL_EXPRESSION";
	eq FEqExp.macroC() = "COND_EXP_EQ";
	eq FLtExp.macroC() = "COND_EXP_LT";
	eq FLeqExp.macroC() = "COND_EXP_LE";
	eq FGtExp.macroC() = "COND_EXP_GT";
	eq FGeqExp.macroC() = "COND_EXP_GE";
		
	public void FIfExp.prettyPrint_C(Printer p, PrintStream str, String indent, Object o) {

		FLogBinExp if_exp = (FLogBinExp)getIfExp();
		FExp op1 = if_exp.getLeft();
		FExp op2 = if_exp.getRight();
		
		int end_parenthesis = 1;
		str.print("(" + if_exp.macroC() + "(");
		if (op1 instanceof FLitExp) {
			str.print("AD_WRAP_LITERAL(");
			p.toString(op1,str,indent,o);
			str.print(")");
		} else {
			p.toString(op1,str,indent,o);
		} 
		str.print(",");
		if (op2 instanceof FLitExp) {
			str.print("AD_WRAP_LITERAL(");
			p.toString(op2,str,indent,o);
			str.print(")");
		} else {
			p.toString(op2,str,indent,o);
		} 
		str.print(",");
		if (getThenExp() instanceof FLitExp) {
			str.print("AD_WRAP_LITERAL(");
			p.toString(getThenExp(),str,indent,o);
			str.print(")");
		} else {
			p.toString(getThenExp(),str,indent,o);
		} 
		str.print(",");
		for (FElseIfExp el_if : getFElseIfExps()) {
			end_parenthesis++;
			if_exp = (FLogBinExp)el_if.getIfExp();
			op1 = if_exp.getLeft();
			op2 = if_exp.getRight();
			str.print("(" + if_exp.macroC() + "(");
			if (op1 instanceof FLitExp) {
				str.print("AD_WRAP_LITERAL(");
				p.toString(op1,str,indent,o);
				str.print(")");
			} else {
				p.toString(op1,str,indent,o);
			} 
			str.print(",");
			if (op2 instanceof FLitExp) {
				str.print("AD_WRAP_LITERAL(");
				p.toString(op2,str,indent,o);
				str.print(")");
			} else {
				p.toString(op2,str,indent,o);
			} 
			str.print(",");
			if (el_if.getThenExp() instanceof FLitExp) {
				str.print("AD_WRAP_LITERAL(");
				p.toString(el_if.getThenExp(),str,indent,o);
				str.print(")");
			} else {
				p.toString(el_if.getThenExp(),str,indent,o);
			} 
			str.print(",");
		}
		if (getElseExp() instanceof FLitExp) {
			str.print("AD_WRAP_LITERAL(");
			p.toString(getElseExp(),str,indent,o);
			str.print(")");
		} else {
			p.toString(getElseExp(),str,indent,o);
		} 
		for (int i=0;i<end_parenthesis*2;i++) {
			str.print(")");
		}
	}	
		
	public void FAbstractEquation.genResidual_C(int i, String indent, PrintStream str) {}
	
	public void FEquation.genResidual_C(int i, String indent, PrintStream str) {
		str.print(indent + "(*res)[" + i + "] = ");
		getRight().prettyPrint_C(str,"");
		str.print(" - (");
		getLeft().prettyPrint_C(str,"");
		str.print(");\n");
	}

	public void FLogBinExp.genResidual_C(int i, String indent, PrintStream str) {
		str.print(indent + "(*res)[" + i + "] = ");
		getRight().prettyPrint_C(str,"");
		str.print(" - (");
		getLeft().prettyPrint_C(str,"");
		str.print(");\n");
	}	
	
	public void FRealVariable.genStartAttributeResidual_C(int i, String indent, PrintStream str) {
		str.print(indent + "(*res)[" + i + "] = ");
		if (startAttributeSet()) {
			startAttributeExp().prettyPrint_C(str,"");
		} else {
			str.print("0.0");
		}
		str.print(" - ");
		str.print("_"+nameUnderscore()+"_");
		str.print(";\n");
	}

	
	
}


