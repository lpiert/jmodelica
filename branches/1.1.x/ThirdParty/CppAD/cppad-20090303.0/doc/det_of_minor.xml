<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Determinant of a Minor</title>
<meta name="description" id="description" content="Determinant of a Minor"/>
<meta name="keywords" id="keywords" content=" det_of_minor determinant matrix minor "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_det_of_minor_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="uniform_01.hpp.xml" target="_top">Prev</a>
</td><td><a href="det_of_minor.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>speed</option>
<option>speed_utility</option>
<option>det_of_minor</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>speed-&gt;</option>
<option>speed_main</option>
<option>speed_utility</option>
<option>speed_double</option>
<option>speed_adolc</option>
<option>speed_cppad</option>
<option>speed_fadbad</option>
<option>speed_sacado</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>speed_utility-&gt;</option>
<option>uniform_01</option>
<option>det_of_minor</option>
<option>det_by_minor</option>
<option>det_by_lu</option>
<option>det_33</option>
<option>det_grad_33</option>
<option>ode_evaluate</option>
<option>sparse_evaluate</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>det_of_minor-&gt;</option>
<option>det_of_minor.cpp</option>
<option>det_of_minor.hpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Inclusion</option>
<option>Purpose</option>
<option>Determinant of A</option>
<option>a</option>
<option>m</option>
<option>n</option>
<option>r</option>
<option>c</option>
<option>d</option>
<option>Scalar</option>
<option>Example</option>
<option>Source Code</option>
</select>
</td>
</tr></table><br/>








<center><b><big><big>Determinant of a Minor</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>#&#xA0;include&#xA0;&lt;cppad/speed/det_of_minor.hpp&gt;<br/>
</span></font></code><code><font color="blue"></font></code><i><span style='white-space: nowrap'>d</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;det_of_minor(</span></font></code><i><span style='white-space: nowrap'>a</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>m</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>r</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>c</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> 


<br/>
<br/>
<b><big><a name="Inclusion" id="Inclusion">Inclusion</a></big></b>
<br/>
The template function <code><font color="blue">det_of_minor</font></code> is defined in the <code><font color="blue">CppAD</font></code>
namespace by including 
the file <code><font color="blue">cppad/speed/det_of_minor.hpp</font></code> 
(relative to the CppAD distribution directory).
It is only intended for example and testing purposes, 
so it is not automatically included by
<a href="cppad.xml" target="_top"><span style='white-space: nowrap'>cppad.hpp</span></a>
.

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
This template function
returns the determinant of a minor of the matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>


using expansion by minors.
The elements of the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 minor 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
</mrow></math>

 
of the matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 are defined,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

, by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>M</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">=</mo>
<msub><mi mathvariant='italic'>A</mi>
<mrow><mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow>
</msub>
</mrow></math>

where the functions 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 is defined by the <a href="det_of_minor.xml#r" target="_top"><span style='white-space: nowrap'>argument&#xA0;r</span></a>
 and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 is defined by the <a href="det_of_minor.xml#c" target="_top"><span style='white-space: nowrap'>argument&#xA0;c</span></a>
.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>This template function
is for example and testing purposes only.
Expansion by minors is chosen as an example because it uses
a lot of floating point operations yet does not require much source code
(on the order of <i>m</i> factorial floating point operations and 
about 70 lines of source code including comments).
This is not an efficient method for computing a determinant;
for example, using an LU factorization would be better.

<br/>
<br/>
<b><big><a name="Determinant of A" id="Determinant of A">Determinant of A</a></big></b>
<br/>
If the following conditions hold, the minor is the
entire matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 and hence <code><font color="blue">det_of_minor</font></code>
will return the determinant of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

:

<ol type="1"><li>

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

.
</li><li>

for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow></math>

, 
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

.
</li><li>

for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>c</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow></math>

,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>c</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mn>0</mn>
</mrow></math>

.
</li></ol>


<br/>
<br/>
<b><big><a name="a" id="a">a</a></big></b>
<br/>
The argument <i>a</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;std::vector&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&amp;&#xA0;</span></font></code><i><span style='white-space: nowrap'>a</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is a vector with size 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>


(see description of <a href="det_of_minor.xml#Scalar" target="_top"><span style='white-space: nowrap'>Scalar</span></a>
 below).
The elements of the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 are defined,
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

, by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>A</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>a</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
</mrow></math>

<br/>
<b><big><a name="m" id="m">m</a></big></b>
<br/>
The argument <i>m</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>m</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is the size of the square matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="n" id="n">n</a></big></b>
<br/>
The argument <i>n</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is the size of the square minor 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="r" id="r">r</a></big></b>
<br/>
The argument <i>r</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;std::vector&lt;size_t&gt;&amp;&#xA0;</span></font></code><i><span style='white-space: nowrap'>r</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is a vector with 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow></math>

 elements.
This vector defines the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 
which specifies the rows of the minor 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
</mrow></math>

.
To be specific, the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

 is defined by 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>r</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mtd></mtr></mtable>
</mrow></math>

All the elements of <i>r</i> must have value
less than or equal <i>m</i>.
The elements of vector <i>r</i> are modified during the computation, 
and restored to their original value before the return from
<code><font color="blue">det_of_minor</font></code>.

<br/>
<br/>
<b><big><a name="c" id="c">c</a></big></b>
<br/>
The argument <i>c</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;std::vector&lt;size_t&gt;&amp;&#xA0;</span></font></code><i><span style='white-space: nowrap'>c</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is a vector with 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
</mrow></math>

 elements
This vector defines the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 
which specifies the rows of the minor 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
</mrow></math>

.
To be specific, the function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

 is defined by 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>c</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">+</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>c</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>C</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mtd></mtr></mtable>
</mrow></math>

All the elements of <i>c</i> must have value
less than or equal <i>m</i>.
The elements of vector <i>c</i> are modified during the computation, 
and restored to their original value before the return from
<code><font color="blue">det_of_minor</font></code>.

<br/>
<br/>
<b><big><a name="d" id="d">d</a></big></b>
<br/>
The result <i>d</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>d</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and is equal to the determinant of the minor 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>M</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="Scalar" id="Scalar">Scalar</a></big></b>
<br/>
If <i>x</i> and <i>y</i> are objects of type <i>Scalar</i>
and <i>i</i> is an object of type <code><font color="blue">int</font></code>,
the <i>Scalar</i> must support the following operations:
<table><tr><td align='left'  valign='top'>

<b>Syntax</b> 
	</td><td align='left'  valign='top'>
 <b>Description</b>
	</td><td align='left'  valign='top'>
 <b>Result Type</b>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i>
	</td><td align='left'  valign='top'>
 default constructor for <i>Scalar</i> object.
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i>
	</td><td align='left'  valign='top'>
 set value of <i>x</i> to current value of <i>i</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>
	</td><td align='left'  valign='top'>
 set value of <i>x</i> to current value of <i>y</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;+&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>
	</td><td align='left'  valign='top'>
 value of <i>x</i> plus <i>y</i>
	</td><td align='left'  valign='top'>
 <i>Scalar</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;-&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>
	</td><td align='left'  valign='top'>
 value of <i>x</i> minus <i>y</i>
	</td><td align='left'  valign='top'>
 <i>Scalar</i>
</td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;*&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i>
	</td><td align='left'  valign='top'>
 value of <i>x</i> times value of <i>y</i>
	</td><td align='left'  valign='top'>
 <i>Scalar</i>
</td></tr>
</table>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>
<br/>
The file
<a href="det_of_minor.cpp.xml" target="_top"><span style='white-space: nowrap'>det_of_minor.cpp</span></a>
 
contains an example and test of <code><font color="blue">det_of_minor.hpp</font></code>.
It returns true if it succeeds and false otherwise.

<br/>
<br/>
<b><big><a name="Source Code" id="Source Code">Source Code</a></big></b>
<br/>
The file
<a href="det_of_minor.hpp.xml" target="_top"><span style='white-space: nowrap'>det_of_minor.hpp</span></a>
 
contains the source for this template function.



<hr/>Input File: cppad/speed/det_of_minor.hpp

</body>
</html>
