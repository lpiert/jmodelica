<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Sparse Jacobian: Example and Test</title>
<meta name="description" id="description" content="Sparse Jacobian: Example and Test"/>
<meta name="keywords" id="keywords" content=" Jacobian sparse example test spare "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_sparse_jacobian.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="sparse_jacobian.xml" target="_top">Prev</a>
</td><td><a href="sparse_hessian.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Drivers</option>
<option>sparse_jacobian</option>
<option>sparse_jacobian.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Drivers-&gt;</option>
<option>Jacobian</option>
<option>ForOne</option>
<option>RevOne</option>
<option>Hessian</option>
<option>ForTwo</option>
<option>RevTwo</option>
<option>sparse_jacobian</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>sparse_jacobian-&gt;</option>
<option>sparse_jacobian.cpp</option>
</select>
</td>
<td>sparse_jacobian.cpp</td>
<td>Headings</td>
</tr></table><br/>



<center><b><big><big>Sparse Jacobian: Example and Test</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 

# include &lt;cppad/cppad.hpp&gt;
namespace { // ---------------------------------------------------------
// define the template function in empty namespace
template &lt;class BaseVector, class BoolVector&gt; 
bool reverse_case()
{	bool ok = true;
	using CppAD::AD;
	using CppAD::NearEqual;
	size_t i, j, k;

	// domain space vector
	size_t n = 4;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt;  X(n);
	for(j = 0; j &lt; n; j++)
		X[j] = AD&lt;double&gt; (0);

	// declare independent variables and starting recording
	CppAD::Independent(X);

	size_t m = 3;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt;  Y(m);
	Y[0] = X[0] + X[1];
	Y[1] = X[2] + X[3];
	Y[2] = X[0] + X[1] + X[2] + X[3] * X[3] / 2.;

	// create f: X -&gt; Y and stop tape recording
	CppAD::ADFun&lt;double&gt; f(X, Y);

	// new value for the independent variable vector
	BaseVector x(n);
	for(j = 0; j &lt; n; j++)
		x[j] = double(j);

	// Jacobian of y without sparsity pattern
	BaseVector jac(m * n);
	jac = f.SparseJacobian(x);
	/*
	      [ 1 1 0 0  ]
	jac = [ 0 0 1 1  ]
	      [ 1 1 1 x_3]
	*/
	BaseVector check(m * n);
	check[0] = 1.; check[1] = 1.; check[2]  = 0.; check[3]  = 0.;
	check[4] = 0.; check[5] = 0.; check[6]  = 1.; check[7]  = 1.;
	check[8] = 1.; check[9] = 1.; check[10] = 1.; check[11] = x[3];
	for(k = 0; k &lt; 12; k++)
		ok &amp;=  NearEqual(check[k], jac[k], 1e-10, 1e-10 );

	// test passing sparsity pattern
	BoolVector s(m * m);
	BoolVector p(m * n);
	for(i = 0; i &lt; m; i++)
	{	for(k = 0; k &lt; m; k++)
			s[i * m + k] = false;
		s[i * m + i] = true;
	}
	p   = f.RevSparseJac(m, s);
	jac = f.SparseJacobian(x);
	for(k = 0; k &lt; 12; k++)
		ok &amp;=  NearEqual(check[k], jac[k], 1e-10, 1e-10 );

	return ok;
}

template &lt;class BaseVector, class BoolVector&gt; 
bool forward_case()
{	bool ok = true;
	using CppAD::AD;
	using CppAD::NearEqual;
	size_t j, k;

	// domain space vector
	size_t n = 3;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt;  X(n);
	for(j = 0; j &lt; n; j++)
		X[j] = AD&lt;double&gt; (0);

	// declare independent variables and starting recording
	CppAD::Independent(X);

	size_t m = 4;
	CPPAD_TEST_VECTOR&lt; AD&lt;double&gt; &gt;  Y(m);
	Y[0] = X[0] + X[2];
	Y[1] = X[0] + X[2];
	Y[2] = X[1] + X[2];
	Y[3] = X[1] + X[2] * X[2] / 2.;

	// create f: X -&gt; Y and stop tape recording
	CppAD::ADFun&lt;double&gt; f(X, Y);

	// new value for the independent variable vector
	BaseVector x(n);
	for(j = 0; j &lt; n; j++)
		x[j] = double(j);

	// Jacobian of y without sparsity pattern
	BaseVector jac(m * n);
	jac = f.SparseJacobian(x);
	/*
	      [ 1 0 1   ]
	jac = [ 1 0 1   ]
	      [ 0 1 1   ]
	      [ 0 1 x_2 ]
	*/
	BaseVector check(m * n);
	check[0] = 1.; check[1]  = 0.; check[2]  = 1.; 
	check[3] = 1.; check[4]  = 0.; check[5]  = 1.;
	check[6] = 0.; check[7]  = 1.; check[8]  = 1.; 
	check[9] = 0.; check[10] = 1.; check[11] = x[2];
	for(k = 0; k &lt; 12; k++)
		ok &amp;=  NearEqual(check[k], jac[k], 1e-10, 1e-10 );

	// test passing sparsity pattern
	BoolVector r(n * n);
	BoolVector p(m * n);
	for(j = 0; j &lt; n; j++)
	{	for(k = 0; k &lt; n; k++)
			r[j * n + k] = false;
		r[j * n + j] = true;
	}
	p   = f.ForSparseJac(n, r);
	jac = f.SparseJacobian(x);
	for(k = 0; k &lt; 12; k++)
		ok &amp;=  NearEqual(check[k], jac[k], 1e-10, 1e-10 );

	return ok;
}
} // End empty namespace 
# include &lt;vector&gt;
# include &lt;valarray&gt;
bool sparse_jacobian(void)
{	bool ok = true;
	// Run with BaseVector equal to three different cases
	// all of which are Simple Vectors with elements of type double.
	// Also vary the type of vector for BoolVector.
	ok &amp;= forward_case&lt; CppAD::vector&lt;double&gt;, CppAD::vectorBool   &gt;();
	ok &amp;= reverse_case&lt; CppAD::vector&lt;double&gt;, CppAD::vector&lt;bool&gt; &gt;();
	//
	ok &amp;= forward_case&lt; std::vector&lt;double&gt;,   std::vector&lt;bool&gt;   &gt;();
	ok &amp;= reverse_case&lt; std::vector&lt;double&gt;,   std::valarray&lt;bool&gt; &gt;();
	//
	ok &amp;= forward_case&lt; std::valarray&lt;double&gt;, CppAD::vectorBool   &gt;();
	ok &amp;= reverse_case&lt; std::valarray&lt;double&gt;, CppAD::vector&lt;bool&gt; &gt;();
	//
	return ok;
}</pre>
</font></code>


<hr/>Input File: example/sparse_jacobian.cpp

</body>
</html>
