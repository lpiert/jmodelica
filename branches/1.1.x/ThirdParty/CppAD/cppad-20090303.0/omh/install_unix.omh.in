/* --------------------------------------------------------------------------
CppAD: C++ Algorithmic Differentiation: Copyright (C) 2003-08 Bradley M. Bell

CppAD is distributed under multiple licenses. This distribution is under
the terms of the 
                    Common Public License Version 1.0.

A copy of this license is included in the COPYING file of this distribution.
Please visit http://www.coin-or.org/CppAD/ for information on other licenses.
-------------------------------------------------------------------------- */

$begin InstallUnix$$
$dollar ^$$
$spell
	err_retu
	const
	printf
	avector.h
	Ipopt
	Ip
	gzip
	Linux
	fi
	exp_apx
	tgz
	gpl
	Dev
	sed
	cppad
	gprof
	config
	stdvector
	std
	ublas
	hpp
	adouble
	badiff
	usr
	cygdrive
	htm
	xml
	Dir
	xvzf
	gz
	workspace
	Makefile
	exe
	Cygwin
	Microsoft
	dsw
	CppAD
	Adolc
	Fadbad
	Sacado
	ls
	aclocal
	yum
	devel
$$

$index CppAD, unix install$$
$index unix, CppAD install$$
$index free, unix install$$
$index install, unix CppAD$$

$section Unix Download, Test and Installation$$

$head Fedora$$
$index install, Fedora$$
$index Fedora, install$$
CppAD is available through yum on the Fedora operating system starting
Fedora version 7. You can download and install CppAD with the instruction
$code
	yum install cppad-devel
$$
(In Fedora, $code devel$$ is used for program development tools.)
You can download and install the corresponding version of the
documentation using the command
$code
	yum install cppad-doc
$$ 

$head RPM$$
$index rpm, cppad.spec$$
$index cppad.spec$$
If you want to use the Fedora $code cppad.spec$$ file
to build an RPM for some other operating system,
it can be found at
$pre
$$
$href%https://projects.coin-or.org/CppAD/browser/trunk/cppad.spec%$$


$head Download$$
$index download, unix$$
$index unix, download$$

$children%
	omh/subversion.omh
%$$
$subhead Subversion$$
If you are familiar with subversion, you may want to follow
the more complicated CppAD download instructions;
see the following $cref/subversion instructions/subversion/$$.

$subhead Web Link$$
If you are not using the subversion download instructions,
make sure you are reading the web version of this documentation by
following the link 
$href%http://www.coin-or.org/CppAD/Doc/installunix.htm%web version%$$.
Then proceed with the instruction that appear below this point.

$subhead Unix Tar Files$$
$index CppAD, tar file$$
$index tar, CppAD file$$
The download files below were first archived with $code tar$$
and then compressed with $code gzip$$:
The ascii files for these downloads are in 
Unix format; i.e., each line ends with just a line feed.

$table
CPL License $pre  $$ $cnext $href%
http://www.coin-or.org/download/source/CppAD/cppad-@VERSION@.cpl.tgz%
cppad-@VERSION@.cpl.tgz%$$ 
$rnext
GPL License $pre  $$ $cnext $href%
http://www.coin-or.org/download/source/CppAD/cppad-@VERSION@.gpl.tgz%
cppad-@VERSION@.gpl.tgz%$$
$tend

$subhead Tar File Extraction$$
Use the unix command
$syntax%
	tar -xvzf cppad-@VERSION@.%license%.tgz
%$$
(where $italic license$$ is $code cpl$$ or $code gpl$$)
to decompress and extract the unix format version
into the distribution directory
$syntax%
	cppad-@VERSION@
%$$
To see if this has been done correctly, check for the following file:
$syntax%
	cppad-@VERSION@/cppad/cppad.hpp
%$$

$head Configure$$
$index configure$$
Enter the directory created by the extraction and execute the command:
$syntax%
	./configure                            \
	--prefix=%PrefixDir%                     \
	--with-Documentation                   \
	--with-Introduction                    \
	--with-Example                         \
	--with-TestMore                        \
	--with-Speed                           \
	--with-PrintFor                        \
	--with-stdvector                       \  
	POSTFIX_DIR=%PostfixDir%                 \
	ADOLC_DIR=%AdolcDir%                     \
	FADBAD_DIR=%FadbadDir%                   \
	SADADO_DIR=%SacadoDir%                   \
	BOOST_DIR=%BoostDir%                     \
	IPOPT_DIR=%IpoptDir%                     \
	CXX_FLAGS=%CompilerFlags% 
%$$
where only the $code configure$$ command need appear.
The entries one each of the other lines are optional
and the text in italic is replaced values that you choose.

$head Testing Return Status$$
$index status, test return$$
$index test, return status$$
All of the test programs mentioned below
(with the exception of $cref/print_for/InstallUnix/--with-PrintFor/$$)
return a status of zero, if all correctness tests pass, and one for an error.
For example, 
if $cref/--with-Example/InstallUnix/--with-Example/$$ 
is included in the $code configure$$ command, 
$codep
	if ! example/example
	then
		echo "example failed its test"
		echo exit 1
	fi
$$
could be used abort the a bash shell script when the test failed.

$head PrefixDir$$
$index configure, prefix directory$$
$index prefix, configure directory$$
$index directory, configure prefix$$
The default value for prefix directory is $code ^HOME$$
i.e., by default the CppAD include files 
will $cref/install/InstallUnix/make install/$$ below $code ^HOME$$.
If you want to install elsewhere, you will have to use this option.
As an example of using the $syntax%--prefix=%PrefixDir%$$ option,
if you specify
$codep
	./configure --prefix=/usr/local
$$ 
the CppAD include files will be installed in the directory
$syntax%
	/usr/local/include/cppad
%$$
If $cref/--with-Documentation/InstallUnix/--with-Documentation/$$
is specified, the CppAD documentation files will be installed in the directory
$syntax%
	/usr/local/share/doc/cppad-@VERSION@
%$$

$head --with-Documentation$$
$index documentation, install$$
$index install, documentation$$
If the command line argument $code --with-Documentation$$ is specified,
the CppAD documentation HTML and XML files are copied to the directory
$syntax%
	%PrefixDir%/share/doc/%PostfixDir%/cppad-@VERSION@
%$$
(see $cref/PostfixDir/InstallUnix/PostfixDir/$$).
The top of the CppAD HTML documentation tree 
(with mathematics displayed as LaTex command) will be located at
$syntax%
	%PrefixDir%/share/doc/%PostfixDir%/cppad-@VERSION@/cppad.htm
%$$
and the top of the XML documentation tree
(with mathematics displayed as MathML) will be located at
$syntax%
	%PrefixDir%/share/doc/%PostfixDir%/cppad-@VERSION@/cppad.xml
%$$

$head --with-Introduction$$
$index introduction, unix$$
$index unix, introduction$$

$subhead get_started$$
$index get_started, unix$$
$index unix, get_started$$
If the command line argument $code --with-Introduction$$ is specified,
the $xref/get_started.cpp/$$ example will be built.
Once the $code make$$ command 
has been executed, you can run this example by executing the command 
$codep
	introduction/get_started/get_started
$$

$subhead exp_apx$$
$index exp_apx, unix$$
$index unix, exp_apx$$
If the command line argument $code --with-Introduction$$ is specified,
the $cref/exp_apx_main.cpp/$$ program
(verifies calculations in the $cref/Introduction/$$ $code exp_apx$$ example) 
will be built.
Once the $code make$$ command
has been executed, you can run these examples by executing the command
$codep
	introduction/exp_apx/exp_apx
$$

$head --with-Example$$
$index example, unix$$
$index test, unix$$
If the command line argument $code --with-Example$$ is specified,
the $xref/Example.cpp/$$ program 
(an extensive set of examples and correctness tests) will be built.
Once the $code make$$ command 
has been executed, you can run these examples by executing the command 
$codep
	example/example
$$

$head --with-TestMore$$
$index test more, unix$$
$index unix, test more$$
If the command line argument $code --with-TestMore$$ is specified,
another extensive set of correctness tests will be compiled by the
$xref/InstallUnix/make/make/$$ command. Once the $code make$$ command 
has been executed, you can run these tests by executing the command 
$codep
	test_more/test_more
$$

$head --with-Speed$$
$index speed, test unix$$
$index test, unix speed$$
$index unix, speed test$$
If the command line argument $code --with-Speed$$ is specified,
a set of speed tests will built.

$subhead cppad$$
$index speed, cppad test$$
$index cppad, test speed$$
$index test, cppad speed$$
After you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/cppad/cppad %option% %seed%
%$$

$subhead double$$
$index speed, double test$$
$index double, test speed$$
$index test, double speed$$
After you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/double/double %option% %seed%
%$$

$subhead profile$$
$index speed, profile cppad$$
$index profile, cppad speed$$
$index cppad, profile speed$$
The C++ compiler flags used to build the profile speed tests are
$code
$verbatim%speed/profile/makefile.am%5%# BEGIN OPTIMIZE%# END OPTIMIZE%$$
$$
After you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/profile/profile %option% %seed%
%$$
You can then obtain the profiling results with
$syntax%
	gprof -b speed/profile/profile
%$$ 
If you are using a windows operating system with Cygwin or MinGW, 
you may have to replace $code profile$$ by $code profile.exe$$
in the $code gprof$$ command above; i.e.,
$codep
	gprof -b speed/profile/profile.exe
$$
In C++, template parameters and argument types become part of a 
routines's name.
This can make the $code gprof$$ output hard to read 
(the routine names can be very long).
You can remove the template parameters and argument types from the 
routine names by executing the following command
$codep
	gprof -b speed/profile/profile | sed -f speed/profile/gprof.sed
$$
If you are using a windows operating system with Cygwin or MinGW, 
you would need to use
$codep
	gprof -b speed/profile/profile.exe | sed -f speed/profile/gprof.sed
$$

$subhead example$$
$index speed, utility example$$
$index example, speed utility$$
$index utility, speed example$$
After you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_utility/$$ examples with the command
$syntax%
	speed/example/example
%$$

$head --with-PrintFor$$
$index print, forward mode$$
$index forward, print$$
If the command line argument $code --with-PrintFor$$ is specified,
the $xref/PrintFor.cpp/$$ example will be built.
Once the $code make$$ command 
has been executed, you can run this example by executing the command 
$codep
	print_for/print_for
$$
Unlike the other programs listed in this section,
$code print_for$$ does not automatically check for correctness 
and return a corresponding
$cref/status/InstallUnix/Testing Return Status/$$.
Instead, it displays what it's output should be.

$head --with-stdvector$$
$index std::vector, unix$$
The 
$small $cref/CPPAD_TEST_VECTOR/test_vector/$$ $$
template class is used for extensive examples and testing of CppAD.
If the command line argument $code --with-stdvector$$ is specified,
the default definition this template class is replaced by
$codep
	std::vector
$$
(In this case $italic BoostDir$$ must not also be specified.)

$head PostfixDir$$
$index configure, postfix directory$$
$index postfix, configure directory$$
$index directory, configure postfix$$
By default, the postfix directory is empty; i.e., there
is no postfix directory.
As an example of using the $syntax%POSTFIX_DIR=%PostfixDir%$$ option,
if you specify
$codep
	./configure --prefix=/usr/local POSTFIX_DIR=coin
$$ 
the CppAD include files will be 
$cref/installed/InstallUnix/make install/$$ in the directory
$syntax%
	/usr/local/include/coin/cppad
%$$
If $cref/--with-Documentation/InstallUnix/--with-Documentation/$$
is specified, the CppAD documentation files will be installed in the directory
$syntax%
	/usr/local/share/doc/coin/cppad-@VERSION@
%$$


$head AdolcDir$$
$index Adolc, unix$$
If you have 
$href%
	http://www.math.tu-dresden.de/~adol-c/%
	Adolc 1.10.2
%$$
installed on your system, you can 
specify a value for $italic AdolcDir$$ in the 
$xref/InstallUnix/Configure/configure/$$ command line.
The value of $italic AdolcDir$$ must be such that
$syntax%
	%AdolcDir%/include/adolc/adouble.h
%$$
is a valid way to reference $code adouble.h$$.
If $cref/--with-Speed/InstallUnix/--with-Speed/$$ is also specified,
after you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/%package%/%package% %option% %seed%
%$$
where $italic package$$ is equal to $code adolc$$.

$subhead Fix Adolc$$
Some compilers will complain about Adolc use for a $code const char*$$
as a $code char*$$. This can be fixed by changing the following
routine in the file $code adolc/avector.h$$:
$codep
	class ADOLC_DLL_EXPORT err_retu {
	char* message;
	public:
		err_retu(char* x) {
		printf("%s \n",x);
		};
	};
$$
to be as follows
$codep
	class ADOLC_DLL_EXPORT err_retu {
	char* message;
	public:
		err_retu(const char* x) {
		printf("%s \n",x);
		};
	};
$$


$subhead Linux$$
If you are using Linux, 
you will have to add to following lines to the file
$code .bash_profile$$ in your home directory:
$syntax%
	LD_LIBRARY_PATH=%AdolcDir%/lib:^{LD_LIBRARY_PATH}
	export LD_LIBRARY_PATH
%$$
in order for Adolc to run properly.

$subhead Cygwin$$
If you are using Cygwin, 
you will have to add to following lines to the file
$code .bash_profile$$ in your home directory:
$syntax%
	PATH=%AdolcDir%/bin:^{PATH}
	export PATH
%$$
in order for Adolc to run properly.
If $italic AdolcDir$$ begins with a disk specification,
you must use the Cygwin format for the disk specification.
For example, 
if $code d:/adolc_base$$ is the proper directory,
$code /cygdrive/d/adolc_base$$ should be used for $italic AdolcDir$$.

$head FadbadDir$$
$index Fadbad, unix$$
If you have
$href%
	http://www.fadbad.com/%
	Fadbad 2.1
%$$
installed on your system, you can 
specify a value for $italic FadbadDir$$.
It must be such that
$syntax%
	%FadbadDir%/FADBAD++/badiff.h
%$$
is a valid reference to $code badiff.h$$.
If $cref/--with-Speed/InstallUnix/--with-Speed/$$ is also specified,
after you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/%package%/%package% %option% %seed%
%$$
where $italic package$$ is equal to $code fadbad$$.

$head SacadoDir$$
$index Sacado, unix$$
If you have
$href%
	http://trilinos.sandia.gov/packages/sacado/%
	Sacado
%$$
installed on your system, you can 
specify a value for $italic SacadoDir$$.
It must be such that
$syntax%
	%SacadoDir%/include/Sacado.hpp
%$$
is a valid reference to $code Sacado.hpp$$.
If $cref/--with-Speed/InstallUnix/--with-Speed/$$ is also specified,
after you execute the $xref/InstallUnix/make/make/$$ command,
you can run the $cref/speed_main/$$ program with the command
$syntax%
	speed/%package%/%package% %option% %seed%
%$$
where $italic package$$ is equal to $code sacado$$.

$head BoostDir$$
$index boost, unix$$
The
$small $cref/CPPAD_TEST_VECTOR/test_vector/$$ $$
template class is used for extensive examples and testing of CppAD.
The default definition for $code CPPAD_TEST_VECTOR$$ is
$xref/CppAD_vector//CppAD::vector/$$.
If the command line argument
$syntax%
	BOOST_DIR=%BoostDir%
%$$
is present, it must be such that
$syntax%
	%BoostDir%/boost/numeric/ublas/vector.hpp
%$$
is a valid reference to the file $code vector.hpp$$.
In this case, the default definition of $code CPPAD_TEST_VECTOR$$ 
is replaced by
$codep
	boost::numeric::ublas::vector
$$
(see $href%http://www.boost.org%boost%$$).
If $italic BoostDir$$ is present, the argument $code --with-stdvector$$
must not be present.

$head IpoptDir$$
$index Ipopt, unix$$
If you have
$href%
	http://www.coin-or.org/projects/Ipopt.xml%
	Ipopt
%$$
installed on your system, you can 
specify a value for $italic IpoptDir$$.
It must be such that
$syntax%
	%IpoptDir%/include/IpIpoptApplication.hpp
%$$
is a valid reference to $code IpIpoptApplication.hpp$$.
In this case, 
$cref/ipopt_cppad_simple.cpp/$$ will be included in the example testing.

$head CompilerFlags$$
$index compile, unix flags$$
$index flags, unix compile$$
If the command line argument $italic CompilerFlags$$ is present,
it specifies compiler flags.
For example,
$syntax%
	CXX_FLAGS="-Wall -ansi"
%$$
would specify that warning flags $code -Wall$$
and $code -ansi$$ should be included
in all the C++ compile commands.
The error and warning flags chosen must be valid options
for the C++ compiler.
The default value for $italic CompilerFlags$$ is the
empty string.


$head make$$
The command
$codep
	make
$$
will compile all of the examples and tests.  
An extensive set of examples and tests can be run as described under
the headings
$xref/InstallUnix/--with-Introduction/--with-Introduction/$$,
$xref/InstallUnix/--with-Example/--with-Example/$$,
$xref/InstallUnix/--with-TestMore/--with-TestMore/$$,
$xref/InstallUnix/--with-Speed/--with-Speed/$$,
$xref/InstallUnix/--with-PrintFor/--with-PrintFor/$$,
$xref/InstallUnix/AdolcDir/AdolcDir/$$, 
$xref/InstallUnix/FadbadDir/FadbadDir/$$,
$xref/InstallUnix/SacadoDir/SacadoDir/$$,
and $xref/InstallUnix/IpoptDir/IpoptDir/$$ above.

$head make install$$
Once you are satisfied that the tests are giving correct results,
you can install CppAD into easy to use directories by executing the command
$codep
	make install
$$
This will install CppAD in the location specified by 
$cref/PrefixDir/InstallUnix/PrefixDir/$$.
You must have permission to write in the $italic PrefixDir$$
directory to execute this command. 
You may optionally specify a destination directory for the install; i.e.,
$syntax%
	make install DESTDIR=%DestinationDirectory%
%$$

$end
