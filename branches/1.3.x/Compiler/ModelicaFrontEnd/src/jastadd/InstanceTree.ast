/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

abstract InstNode : BaseNode ::= /InstComponentDecl*/ 
                                 /InstClassDecl*/ 
                                 /InstExtends*/ 
                                 /InstImport*/ 
                                 /RedeclaredInstClassDecl:InstClassDecl*/
                                 /FAbstractEquation*/ 
                                 /FQName/ 
                                 /DynamicClassName:InstAccess*/ 
                                 /DynamicComponentName:InstAccess*/ 
                                 /DynamicFExp:FExp*/;
abstract InstRoot : InstNode;
InstClassRoot : InstRoot ::= <ClassDecl:ClassDecl>;
//InstFullClassDeclRoot :InstRoot ::= InstFullClassDecl;
InstProgramRoot : InstRoot ::= <Program:Program>
					           /UnknownInstClassDecl/
                               /UnknownInstComponentDecl/
                               /InstPredefinedType:InstClassDecl*/ 
                               /InstBuiltInType:InstClassDecl*/   
                               /InstBuiltInFunction:InstClassDecl*/
                               /InstLibClassDecl:InstClassDecl*/;

abstract InstClassDecl: InstNode ::= <ClassDecl:ClassDecl>;
abstract InstBaseClassDecl : InstClassDecl ::= [InstConstraining] InstRestriction;
InstFullClassDecl      : InstBaseClassDecl ::= [InstExternal];
InstExtendClassDecl    : InstFullClassDecl ::= ;
InstShortClassDecl     : InstBaseClassDecl ::= [FArraySubscripts];
InstPrimitiveClassDecl : InstFullClassDecl;
InstBuiltInClassDecl   : InstClassDecl;
UnknownInstClassDecl   : InstFullClassDecl;

InstReplacingFullClassDecl      : InstFullClassDecl  ::= 
	                                 <OriginalClassDecl:ClassDecl> 
                                     /OriginalInstClass:InstClassDecl/ 
                                     <InstClassRedeclare:InstRedeclareClassNode>;
InstReplacingShortClassDecl     : InstShortClassDecl ::= 
	                                 <OriginalClassDecl:ClassDecl> 
                                     /OriginalInstClass:InstClassDecl/ 
                                     <InstClassRedeclare:InstRedeclareClassNode>;
InstReplacingPrimitiveClassDecl : InstReplacingFullClassDecl;

abstract InstRestriction : BaseNode;
InstModel     : InstRestriction;
InstBlock     : InstRestriction;
InstMClass    : InstRestriction;
InstConnector : InstRestriction;
InstExpandableConnector : InstConnector;
InstMType     : InstRestriction;
InstMPackage  : InstRestriction;
InstFunction  : InstRestriction;
InstMRecord   : InstRestriction;

InstExternal     : BaseNode ::= <ExternalClause:ExternalClause> [FExternalLanguage] [InstExternalCall];
InstExternalCall : BaseNode ::= [ReturnVar:InstAccess] <Name> Arg:FExp*;

abstract InstImport : BaseNode ::= PackageName:InstAccess <ImportClause:ImportClause>;
InstImportQualified   : InstImport;
InstImportUnqualified : InstImport;
InstImportRename      : InstImport;

abstract InstConstraining : BaseNode         ::=  ClassName:InstAccess [InstClassModification] /InstNode/;
InstConstrainingClass     : InstConstraining ::= <BaseClassDecl:BaseClassDecl>;
InstConstrainingComponent : InstConstraining ::= <ComponentDecl:ComponentDecl> /DynamicClassName:InstAccess*/;

// The class to expand may differ from the declared class of the component due to 
// redeclaration of classes.
abstract InstComponentDecl : InstNode ::= ClassName:InstAccess 
                                          [LocalFArraySubscripts:FArraySubscripts] 
                                          <ComponentDecl:ComponentDecl> 
                                          [InstModification] 
                                          [InstConstraining] 
                                          /FArraySubscripts/ 
                                          [ConditionalAttribute:FExp];
InstComposite            : InstComponentDecl;
abstract InstAssignable  : InstComponentDecl ::= /BindingFExp:FExp/;
InstPrimitive            : InstAssignable;
InstRecord               : InstAssignable;
InstBuiltIn              : InstPrimitive;
UnknownInstComponentDecl : InstComponentDecl;
InstArrayComponentDecl   : InstComponentDecl ::= <Index:int>;
                         
InstReplacingComposite   : InstComposite ::= <OriginalDecl:ComponentDecl> 
                                             /OriginalInstComponent:InstComponentDecl/ 
                                             <InstComponentRedeclare:InstComponentRedeclare>;
// TODO: InstReplacingAssignable/Record?
InstReplacingPrimitive   : InstPrimitive ::= <OriginalDecl:ComponentDecl>
                                             /OriginalInstComponent:InstComponentDecl/ 
                                             <InstComponentRedeclare:InstComponentRedeclare>;

InstExtends : InstNode ::= ClassName:InstAccess <ExtendsClause:ExtendsClause> [InstClassModification];
InstExtendsShortClass : InstExtends;
InstInlineExtends     : InstExtends;
//InstReplacingExtendsShortClass : InstReplacingExtendsShortClass ::= <InstExtendsShortClass:InstExtendsShortClass>

// Accesses in the instance tree
abstract InstAccess : BaseNode ::= <ID> /ExpandedSubscripts:FArraySubscripts/;
InstDot : InstAccess ::= Left:InstAccess Right:InstAccess;

InstArrayAccess     : InstAccess ::= [FArraySubscripts];
InstParseAccess     : InstArrayAccess;
InstClassAccess     : InstAccess;
InstAmbiguousAccess : InstArrayAccess;
InstComponentAccess : InstArrayAccess;

InstForIndex : CommonForIndex ::= InstPrimitive /DynamicClassName:InstAccess*/;

InstForClauseE : FAbstractEquation ::= InstForIndex* FAbstractEquation*;
InstForStmt    : FStatement        ::= InstForIndex* ForStmt:FStatement*;

// These nodes are used to create instance copies of modification trees intended for
// error checking.	
abstract InstModification : BaseNode ::= <Modification:Modification>;
InstCompleteModification : InstModification ::= InstClassModification [InstValueModification];
InstValueModification    : InstModification ::= /FExp/ 
                                                /InstRecordConstructorModification*/ 
                                                /DynamicInstMod:InstModification*/;
InstClassModification    : InstModification ::= InstArgument*;
//Virtual modification for argument of record constructor value modification.
InstRecordConstructorModification : InstValueModification ::= <Input:InstComponentDecl> <Arg:FExp>;
//Virtual modification for cell of array constant value modification.
InstArrayModification : InstValueModification ::= <Cell:FExp>;

abstract InstArgument            : InstModification      ::= <Each:boolean> <Final:boolean>;
abstract InstNamedModification   : InstArgument          ::= Name:InstAccess;
abstract InstElementModification : InstNamedModification ::= [InstModification];
abstract InstElementRedeclare    : InstNamedModification;
InstComponentModification : InstElementModification; 
InstClassRedeclare        : InstElementRedeclare ::= /InstClassDecl/;
InstComponentRedeclare    : InstElementRedeclare ::= /InstComponentDecl/ /DynamicClassName:InstAccess*/;