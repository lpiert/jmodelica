/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect TestFramework {
	
/*	
	public boolean ASTNode.collectTestCases(TestSuite ts, String className) {
		log.debug("ASTNode.collectTestCases");
		for (int i=0;i<getNumChild();i++)
			if (getChild(i).collectTestCases(ts,className))
				return true;
		return false;
	}

	public boolean FullClassDecl.collectTestCasesActive = false;
	
	public boolean FullClassDecl.collectTestCases(TestSuite ts, String className) {
		log.debug("FullClassDecl.collectTestCases: "+ qualifiedName() + " looking for " + className);
		if (qualifiedName().equals(className)) {
		    log.debug("FullClassDecl.collectTestCases: found "+className);
		    collectTestCasesActive = true;
			Collection<TestCase> tcs = testCases();
			collectTestCasesActive = false;
			for (Iterator<TestCase> iter=tcs.iterator();iter.hasNext();)
				ts.add(iter.next());
			return true;
		}
		return getClassDeclList().collectTestCases(ts,className);
	}

	coll Collection<TestCase> FullClassDecl.testCases() [new LinkedHashSet<TestCase>()] with add root FullClassDecl;
	
	AFlatteningTestCase contributes
 	   createFlatteningTestCase()
	to FullClassDecl.testCases() for callingFullClassDecl();


	inh FullClassDecl ASTNode.callingFullClassDecl();
	eq Root.getChild().callingFullClassDecl() = null;
	eq FullClassDecl.getChild().callingFullClassDecl() {
		log.debug("FullClassDecl.getChild().callingFullClassDecl(): " + collectTestCasesActive);
		return collectTestCasesActive? this: callingFullClassDecl();
	}
*/


	public boolean ASTNode.collectTestCases(TestSuite ts, String className) {
		for (int i=0;i<getNumChild();i++)
			if (getChild(i).collectTestCases(ts,className))
				return true;
		return false;
	}

	public boolean FullClassDecl.collectTestCases(TestSuite ts, String className) {
		if (qualifiedName().equals(className)) {
			collectTestCases(ts);
			return true;
		}
		return getClassDeclList().collectTestCases(ts,className);
	}

	public void ASTNode.collectTestCases(TestSuite ts) {
		collectTestCasesEM("JModelica",ts);
	}

	public void ASTNode.collectTestCasesEM(String name, TestSuite ts) {
		for (int i=0;i<getNumChild();i++)
			getChild(i).collectTestCasesEM(name,ts);
	}
	
	public void ASTNode.collectTestCasesFC(String name, TestSuite ts) {
		for (int i=0;i<getNumChild();i++)
			getChild(i).collectTestCasesFC(name,ts);
	}
	
	public void ASTNode.collectTestCasesNA(String name, TestSuite ts) {
		for (int i=0;i<getNumChild();i++)
			getChild(i).collectTestCasesNA(name,ts);
	}

	public void ElementModification.collectTestCasesEM(String name, TestSuite ts) {
		if (name.equals(getName().qualifiedName())) {
			if (name.equals("JModelica"))  
				getModification().collectTestCasesEM("unitTesting",ts);
			else if (name.equals("unitTesting"))
				getModification().collectTestCasesFC("JModelica.UnitTesting",ts);
		}
	}

	public void FunctionCall.collectTestCasesFC(String name, TestSuite ts) {
		if (name.equals(getName().qualifiedName()) && name.equals("JModelica.UnitTesting")) {
			getFunctionArguments().collectTestCasesNA("testCase", ts);
		} else if (getName().qualifiedName().startsWith(name) && name.equals("JModelica.UnitTesting.")) {
			// Find the name of the test case to create
			String className = getName().qualifiedName().substring(name.length());
			String fullName = TestCase.class.getName().replace("TestCase", className);
			String attrName = null;
			try {
				// Get a class object for the test case class
				Class cl = Class.forName(fullName);
				
				// Check if it is a test case
				if (TestCase.class.isAssignableFrom(cl)) {
					// Create an object of the new test case class
					TestCase tc = (TestCase) cl.newInstance();
					
					// Add all attributes
					for (NamedArgument na : getFunctionArguments().getNamedArguments()) {
						attrName = na.getName().name();
						boolean isOpt = attrName.indexOf('_') >= 0;
						String methodName = isOpt ? null : 
							"set" + Character.toUpperCase(attrName.charAt(0)) + attrName.substring(1);
						
						// TODO: Use type() and ceval() instead.
						if (na.getExp() instanceof StringLitExp) {
							String val = ((StringLitExp) na.getExp()).getSTRING().replace("\\\"", "\"");
							if (isOpt) {
								tc.setStringOption(attrName, val);
							} else {
								Method m = cl.getMethod(methodName, String.class);
								m.invoke(tc, val);
							}
						} else if (na.getExp() instanceof BooleanLitExp) {
							boolean val = na.getExp() instanceof BooleanLitExpTrue;
							if (isOpt) {
								tc.setBooleanOption(attrName, val);
							} else {
								Method m = cl.getMethod(methodName, Boolean.TYPE);
								m.invoke(tc, val);
							}
						} else {
							log.error("Creating test case '" + className + "' - attribute '" + 
									attrName + "' is assigned from an unsupported expression type");
						}
					}
					
					// Set generic information of the test case and add to list
					tc.setSourceFileName(root().fileName());
					tc.setClassName(enclosingClassDecl().qualifiedName());
					ts.add(tc);
					log.debug("Created test case '" + className + "' for class '" + tc.getClassName() + "'");
				}
			} catch (ClassNotFoundException e) {
				// TODO: Maybe we should throw exceptions instead of log?
				// No such class was found - log that
				log.error("Creating test case '" + className + "' - class not found");
			} catch (NoSuchMethodException e) {
				// No such method was found - log that
				log.error("Creating test case '" + className + "' - attribute '" + attrName + 
						"' not found or used with wrong type of expression");
			} catch (Exception e) {
				// The test case class did not behave as expected - log that
				log.error("Creating test case '" + className + "' - malformed test case class");
			}
		}
	}

	public void NamedArgument.collectTestCasesNA(String name, TestSuite ts) {
		if (name.equals(getName().qualifiedName()) && name.equals("testCase")) {
			getExp().collectTestCasesFC("JModelica.UnitTesting.", ts);
		}
	}

}