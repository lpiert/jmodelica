/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


aspect InstNameClassification {

	/**
	 * \brief Helper class for definition of kinds.
	 */
    abstract public class Kind {
    	static Kind CLASS_ACCESS          = new ClassAccess();
    	static Kind COMPONENT_ACCESS      = new ComponentAccess();
    	static Kind AMBIGUOUS_ACCESS      = new AmbigousAccess();
    	static Kind LAST_AMBIGUOUS_ACCESS = new LastAmbigousAccess();
    	
    	public boolean isClassAccess()     { return false; }
    	public boolean isComponentAccess() { return false; }
    	public boolean isAmbigousAccess()  { return false; }
    	
    	public abstract Kind predKind();
    	
    	static protected class ClassAccess extends Kind {
    		public Kind    predKind()      { return CLASS_ACCESS; }
    		public boolean isClassAccess() { return true; }
    	}
    	
    	static protected class ComponentAccess extends Kind {
    		public Kind    predKind()          { return AMBIGUOUS_ACCESS; }
    		public boolean isComponentAccess() { return true; }
    	}
    	
    	static protected class AmbigousAccess extends Kind {
    		public Kind    predKind()         { return AMBIGUOUS_ACCESS; }
    		public boolean isAmbigousAccess() { return true; }
    	}
    	
    	static protected class LastAmbigousAccess extends AmbigousAccess {
    		public Kind predKind() { return CLASS_ACCESS; }
    	}
    }
	
	
	rewrite InstParseAccess {
		to InstAccess {
			InstAccess a;
			if (kind().isComponentAccess()) 
			    a = new InstComponentAccess(getID(), getFArraySubscriptsOpt());
			else if (kind().isClassAccess()) 
				a = new InstClassAccess(getID());
			else 
				a = new InstAmbiguousAccess(getID(), getFArraySubscriptsOpt());
			a.setLocation(this);
			return a;
  		}
	}

	
	// TODO: Check this section so that all inh calls are caught at the right level
	/**
	 * Here a few cases are classified based on their context.
	 */
	inh Kind InstAccess.kind();
	eq InstRoot.getChild().kind() = Kind.AMBIGUOUS_ACCESS;
	eq FlatRoot.getChild().kind() = Kind.AMBIGUOUS_ACCESS;

	eq InstDot.getLeft().kind() {
	 	return getRightNoTransform().predKind();
	}

	eq InstExtends.getClassName().kind() = Kind.CLASS_ACCESS;
	eq InstImport.getPackageName().kind() = Kind.LAST_AMBIGUOUS_ACCESS;
//	eq InstShortClassDecl.getClassName().kind() = Kind.CLASS_ACCESS;
	eq InstComponentDecl.getClassName().kind() = Kind.CLASS_ACCESS;	
	eq InstComponentModification.getName().kind() = Kind.COMPONENT_ACCESS;
	eq InstClassRedeclare.getName().kind() = Kind.CLASS_ACCESS;
	eq InstComponentRedeclare.getName().kind() = Kind.COMPONENT_ACCESS;
	eq InstFunctionCall.getName().kind() = Kind.CLASS_ACCESS;
	
	eq InstConstraining.getClassName().kind() = Kind.CLASS_ACCESS;
	eq InstNode.getDynamicComponentName().kind() = Kind.COMPONENT_ACCESS;
	eq InstNode.getDynamicClassName().kind() = Kind.CLASS_ACCESS;
    eq InstComponentRedeclare.getDynamicClassName().kind() = Kind.CLASS_ACCESS;
	eq InstConstrainingComponent.getDynamicClassName().kind() = Kind.CLASS_ACCESS;
	eq InstComponentDecl.getConditionalAttribute().kind() = Kind.AMBIGUOUS_ACCESS;
	eq InstBaseClassDecl.getFAbstractEquation().kind() = Kind.AMBIGUOUS_ACCESS;	

	/**
	 * The attribute predKind defines the kind for the previous part of a 
	 * qualified name.
	 */
	syn Kind InstAccess.predKind() {
		return kind().predKind();
	}
	eq InstDot.predKind() {
		return getLeftNoTransform().predKind();
	}

}
aspect InstResolveAmbiguousNames {
	/**
	 * This rewrite determines whether an InstAmbiguousAccess is a InstTypeAcces or a
	 * InstComponentAccess by attempting type and component lookups respectively.
	 */
	boolean InstAmbiguousAccess.rewritten = false;
	rewrite InstAmbiguousAccess {
	    when(!rewritten) 
	    to InstAccess {
	    	//log.debug("rewrite InstAmbiguousAccess begin " + name());
	  		if (!lookupInstComponent(name()).isEmpty()) {
		    	//log.debug("rewrite InstAmbiguousAccess " + name() + " component");
     		    InstComponentAccess c = new InstComponentAccess(name(), getFArraySubscriptsOpt());
  	 			c.setLocation(this);
  	 			return c;
  		    } else if (!lookupInstClass(name()).isEmpty()) {		
    	    	//log.debug("rewrite InstAmbiguousAccess " + name() + " class");
            	InstClassAccess t = new InstClassAccess(name()); 
    			t.setLocation(this);
  	 			return t;
  			}
  			rewritten = true;
	    	//log.debug("rewrite InstAmbiguousAccess end " + name());
  			return this;
  		}
  	}
}


