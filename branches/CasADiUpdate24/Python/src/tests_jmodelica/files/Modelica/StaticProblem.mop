model StaticProblem
    Real x(start=2, min=0.1);
    Real y(start=0.5, min=0.1);
    input Real u(start=1);
    parameter Real p(start=1.5) = 1.5;
equation
    der(x) = x^p - y*u;
    x*y = 1;
initial equation
    der(x) = 0;
end StaticProblem;

optimization StaticProblemControl(objectiveIntegrand=(y-0.5)^2)
    extends StaticProblem;
end StaticProblemControl;

optimization StaticProblemBound(objectiveIntegrand=(y-0.5)^2)
    extends StaticProblem(p(free=true), y(min=0.6));
end StaticProblemBound;

optimization StaticProblemConstraint(objectiveIntegrand=(y-0.5)^2)
    extends StaticProblem(p(free=true));
constraint
    y >= 0.6;
end StaticProblemConstraint;

optimization StaticProblemEst
    extends StaticProblem(p(free=true));
end StaticProblemEst;

model AlgebraicProblem
    Real x(min=0.1, start=2);
    Real y(min=0.1, start=4);
    Real z(min=0.1, start=6);
    input Real u(start=0.5);
    parameter Real p(min=0.1, start=0.5, max=0.9) = 0.5;
equation
    cos(x*u) = p;
    y = x^2;
    z = x + y;
end AlgebraicProblem;

optimization AlgebraicProblemEst
    extends AlgebraicProblem(p(free=true));
end AlgebraicProblemEst;
