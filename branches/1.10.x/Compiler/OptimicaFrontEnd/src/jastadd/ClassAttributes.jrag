/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ClassAttributes {

	syn lazy List OptClassDecl.getClassAttributeList() {

		//log.debug("Program.getPredefinedTypeList()");
		List l = new List();

	    // Build a string with a Modelica class corresponding to Real
		String builtInDef = "model ClassAttributes\n";
		builtInDef += "Real objective = 1;\n"; 
		builtInDef += "Real objectiveIntegrand = 1;\n"; 
		builtInDef += "parameter Real startTime=0;\n";
		builtInDef += "parameter Real finalTime(initialGuess=1)=1;\n";
		builtInDef += "parameter Boolean static=false;\n";
		builtInDef += "end ClassAttributes;\n";
   
   		//java.io.StringReader builtInDefReader = new java.io.StringReader(builtInDef);
 	  	//ModelicaParser parser = new ModelicaParser();
  		PrimitiveClassDecl pcd=null;
  
   		try {
   		
   			ParserHandler ph = new ParserHandler();
   			SourceRoot sr = ph.parseString(builtInDef,"");
 			Program p = sr.getProgram();
   			   			
  			FullClassDecl cd = (FullClassDecl)(p.getUnstructuredEntity(0).getElement(0));	

			for (int i=0;i<cd.getNumComponentDecl();i++) { 		
 				l.add(cd.getComponentDecl(i));	
			}	
 		
 		} catch(Exception e){e.printStackTrace(); System.exit(0);}
			
		//log.debug("Program.getPredefinedTypeList(): "+l.getNumChild());
		
		return l;
	}

	eq InstTimedVariable.getName().kind() = Kind.COMPONENT_ACCESS;
	
	syn lazy List InstOptClassDecl.getInstClassAttributeList() {
		List l = new List();
		for (ComponentDecl cd : ((OptClassDecl)getClassDecl()).getClassAttributes()) {
    		InstAccess name = cd.getClassName().newInstAccess();
			l.add(new InstCreateComponentDecl(cd, this));
		}
		return l;
	}

 	eq InstOptClassDecl.localInstModifications() {
		ArrayList<InstModification> l = new ArrayList<InstModification>();
		// Add modifications from the class modification list
		if (hasInstClassModification())
			l.add(getInstClassModification());			
		return l;
	}
	
	eq InstOptClassDecl.getInstClassModification().lookupInstComponentInInstElement(String name) {
		for (InstComponentDecl ica : getInstClassAttributes()) {
			if (ica.matches(name))
				return ica;
		}
		return null;
	}
    
    rewrite InstPrimitive {
    	when (!(this instanceof InstClassAttribute) && isClassAttribute()) to InstClassAttribute 
    	copyLocationTo(new InstClassAttribute(
    			getClassName(), getLocalFArraySubscriptsOpt(), getComponentDecl(), 
    			getInstModificationOpt(), getInstConstrainingOpt(), getConditionalAttributeOpt()));    	
    }
    
    inh boolean InstPrimitive.isClassAttribute();
    eq BaseNode.getChild().isClassAttribute()                      = false;
    eq InstOptClassDecl.getInstClassAttribute().isClassAttribute() = true;

	// This equation is needed in order to ensure correct lookup of accesses
	// in the class attribute construct.
	eq InstOptClassDecl.getInstClassModification().myInstNode() =
		getInstClassAttribute(0);
	
	eq InstOptClassDecl.getInstClassModification().lookupInstComponent(String name) =
		memberInstComponent(name);
	
	eq InstOptClassDecl.memberInstComponent(String name) {
		InstComponentDecl icd = super.memberInstComponent(name);
		if (icd != null)
			return icd;
		
		for (InstComponentDecl ica : getInstClassAttributes()) {
			if (ica.matches(name))
				return ica;
		}
		
		return null;
	}

	
}
