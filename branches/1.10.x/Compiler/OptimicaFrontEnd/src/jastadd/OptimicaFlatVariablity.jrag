
/**
 * This aspect introduces a new type of variability: timed. An expression has 
 * timed variability if it is composed of parameters, literals and timed
 * variables but no continuous or discrete variables.
 */
aspect OptimicaFlatVariability {

	syn boolean FTypePrefixVariability.timedVariability() = false;
	eq FTimed.timedVariability() = true;	
	syn boolean FExp.isTimedExp() = variability().timedVariability();
	
	eq FTimedVariable.variability() = fTimed();
	eq InstTimedVariable.variability() = fTimed();
	eq FStartTimeExp.variability() = fParameter();
	eq FFinalTimeExp.variability() = fParameter();
	
	public static final FTimed FTimed.instance = new FTimed();	

	public static FTimed ASTNode.fTimed() {
		return FTimed.instance;
	}
		
	public static final FOptParameter FOptParameter.instance = new FOptParameter();	
	
	public static FOptParameter ASTNode.fOptParameter() {
		return FOptParameter.instance;
	}
	
	syn boolean FTypePrefixVariability.optParameterVariability() = false;
	eq FOptParameter.optParameterVariability() = true;	
	eq FOptParameter.parameterVariability()    = true;	
	syn boolean FExp.isOptParameterExp() = variability().optParameterVariability();
	
	protected int FTimed.variabilityLevel()        { return VARIABILITY_LEVEL; }
	protected int FOptParameter.variabilityLevel() { return VARIABILITY_LEVEL; }
	protected static final int FTimed.VARIABILITY_LEVEL        = FDiscrete.VARIABILITY_LEVEL - 5;
	protected static final int FOptParameter.VARIABILITY_LEVEL = FParameter.VARIABILITY_LEVEL;
	
	syn int FOptParameter.combineLevel() = variabilityLevel() * 10 + 5;
	
	// Notice that the attribute FVariable.isParameter is not redefined in order
	// not to render a lot of attributes in FlatAPI to be redefined as a
	// consequence.
    syn boolean FVariable.isOptParameter() = variability().optParameterVariability();
	
	// TODO: Change to be more in line with FlatVariability and more extensible
	refine FlatVariability eq FVariable.variability() {
		FTypePrefixVariability var = FlatVariability.FVariable.variability();
		return (var.parameterVariability() && freeAttribute()) ? fOptParameter() : var;
	}
	
	refine FlatVariability eq FExp.isParameterExp() = 
		variability().parameterVariability() || variability().optParameterVariability(); 
	
   	eq FTimed.toString() = "timed";
   	eq FOptParameter.toString() = "opt_parameter";
   	eq FTimed.toStringLiteral() = "timed";
   	eq FOptParameter.toStringLiteral() = "opt_parameter";
	
   	// TODO: Perhaps FTypePrefixVariability.lessOrEqual() should be used here?
	syn boolean FConstraint.isTimed();
	eq FRelationConstraint.isTimed() = 
		(getRight().isTimedExp() || getRight().isParameterExp() || getRight().isConstantExp()) & 
		(getLeft().isTimedExp() || getLeft().isParameterExp() || getLeft().isConstantExp());
	eq FForClauseC.isTimed() {
		for (FConstraint c : getFConstraints())
			if (!c.isTimed())
				return false;
		return true;
	}
	eq InstForClauseC.isTimed() {
		for (FConstraint c : getFConstraints())
			if (!c.isTimed())
				return false;
		return true;
	}

	syn boolean FConstraint.isParameter();
	eq FRelationConstraint.isParameter() = 
		(getRight().isParameterExp() || getRight().isConstantExp()) & 
		(getLeft().isParameterExp() || getLeft().isConstantExp());
	eq InstForClauseC.isParameter() {
		for (FConstraint c : getFConstraints())
			if (!c.isParameter())
				return false;
		return true;
	}
	eq FForClauseC.isParameter() {
		for (FConstraint c : getFConstraints())
			if (!c.isParameter())
				return false;
		return true;
	}

	
}