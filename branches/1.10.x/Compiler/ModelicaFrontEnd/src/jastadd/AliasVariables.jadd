/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.LinkedHashSet;
import java.util.BitSet;
//import org.jmodelica.modelica.compiler.AliasManager.AliasVariable;

/**
 * Computation of alias sets.
 */
aspect AliasVariables {
	
	/**
	 * \brief Check if an FVariable is an alias.
	 * 
	 * Returns true if alias, else false.
	 * 
	 * @return True if alias, else false.
	 */
	syn boolean FAbstractVariable.isAlias() = false; 
	eq FVariable.isAlias() = aliasVariable() != null; 
	
	/**
	 * Retreives the alias set for this variable or null if not an alias
	 * 
	 *  @return AliasSet that this variable belongs to
	 */
	syn AliasManager.AliasSet FVariable.aliasSet() {
		AliasManager aliasManager = myFClass().getAliasManager();
		return aliasManager.getAliasSet(this);
	}
	
	/**
	 * \brief Get the alias variable of an FVariable. 
	 * 
	 * If the FVariable is not an alias, null is returned.
	 * 
	 * @return An AliasVariable object if the variable is an alias, otherwise
	 * null.
	 */
	syn lazy AliasManager.AliasVariable FVariable.aliasVariable() {
		AliasManager.AliasSet aliasSet = aliasSet();
		if (aliasSet == null || inRecord()) {
			return null;
		}
		AliasManager.AliasVariable av = aliasSet.getIterationVariable();
		return av.getFVariable()==this? null : av;
	}
	
	/**
	 * \brief Get the iteration variable corresponding to the alias.
	 * 
	 * If the FVariable is an alias, the alias() return corresponding iteration
	 * FVariable, else null.
	 * 
	 * @return The corresponding iteration variable if alias, otherwise null.
	 */
	syn FVariable FAbstractVariable.alias() = null;
	
	eq FVariable.alias() {
		if (aliasVariable() == null) {
			return null;
		} else {
			return aliasVariable().getFVariable();			
		}
	}

	/**
	 * \brief Returns true if the alias is negated otherwise false.
	 * 
	 * @return True if negated alias, otherwise false.
	 */
	syn boolean FAbstractVariable.isNegated() = false;
	
	eq FVariable.isNegated() {
		AliasManager aliasManager = myFClass().getAliasManager();
		AliasManager.AliasVariable alias = aliasManager.getAliasVariable(this);
		AliasManager.AliasSet aliasSet = alias.getSet();
		if (alias == null) {
			return false;
		} else {
			AliasManager.AliasVariable iav = aliasSet.getIterationVariable();
			return (alias.isNegated() != iav.isNegated());
		}
	}
	
	/**
	 * Set each attribute of this variable to the most relevant version from the 
	 * variables of the alias set.
	 * 
	 * What is most relevant depends on the attribute, see respective subclass of 
	 * AliasManager.AttributePropagator.
	 */
	public void FVariable.propagateAttributesFromAlias() {
		AliasManager.AliasSet aliasSet = aliasSet();
		if (aliasSet != null) {
			AliasManager.AliasVariable myAv = aliasSet.getIterationVariable();
			if (myAv.getFVariable() == this) {
				AliasManager.AttributePropagatorMap map = new AliasManager.AttributePropagatorMap(myAv);
				for (AliasManager.AliasVariable av : aliasSet) 
					if (av.getFVariable() != this) 
						map.addAlias(av);
				map.propagate();
			}
		}
	}
		
	/**
	 * \brief An FClass has an alias manager.
	 */
	private AliasManager FClass.aliasManager = new AliasManager();

	/**
	 * \brief Get the alias manager.
	 * 
	 * @return the alias manager.
	 */
	public AliasManager FClass.getAliasManager() {
		return aliasManager;
	}
	
	/**
	 * \brief AliasManager contains the connection sets of an FClass.
	 * 
	 * The alias sets are build by adding pairs of FVariables, in addition
	 * to information about whether the alias pair is negated.
	 */
	public class AliasManager {
	
		// Alias error message.
		private String aliasErrorMessage = "";

		// Indicate alias error
		private boolean aliasErrorFlag = false;
		
		// A set of alias sets
		private Set<AliasSet> aliasSets = new LinkedHashSet<AliasSet>();
		
		// A map that maps FVariable to its alias set.
		private Map<FVariable, AliasVariable> variableMap = new HashMap<FVariable, AliasVariable>();
		
		/**
		 * \brief Getter for alias error message.
		 * 
		 * @return The error message.
		 */
		public String getAliasErrorMessage() {
			return aliasErrorMessage;
		}
		
		/**
		 * \brief Check if there was an alias error.
		 * 
		 * The error message can be retrieved by calling the method
		 * getAliasManagerError.
		 * 
		 * @return True if there was an alias error, otherwise false.
		 */
		public boolean aliasError() {
			return aliasErrorFlag;
		}
		
		/**
		 * \brief Get the set of alias sets.
		 * 
		 * @return A set containing the alias sets.
		 */
		public Set<AliasSet> getAliasSets() {
			return aliasSets;
		}
		
		/**
		 * \brief Get the AliasVariable object corresponding to an FVariable
		 * 
		 * @param fv An FVariable object
		 * @return The alias variable that corresponds to an FVariable
		 */
		public AliasVariable getAliasVariable(FVariable fv) {
			return variableMap.get(fv);
		}
		

		/**
		 * \brief Retrieve the alias set corresponding to an FVariable.
		 * 
		 * Returns null if the FVariable is not present in an alias set.
		 * 
		 * @param alias An FVariable.
		 * @return The alias set corresponding to the alias variable.
		 */
		public AliasSet getAliasSet(FVariable fv) {
			AliasVariable alias = variableMap.get(fv);
			if (alias == null)
				return null;
			return alias.getSet();
		}

		/**
		 * \brief Add a pair of alias variables to the alias manager.
		 * 
		 * @param fvA An FVariable.
		 * @param fvB Another FVariable.
		 * @param neg True if the alias pair is negated, otherwise false.
		 * 
		 */
		public void addAliasVariables(FVariable fvA, FVariable fvB, boolean neg) {
			/*
			 * Rules for alias sets:
			 * 
			 *  1. An alias variable can only occur in one alias set
			 *  2. An alias variable can occur either negated or
			 *     non-negated in an alias set, not both.
			 *  
			 *  These rules are enforced by the algorithm below: if a
			 *  rule is about to be broken an error message is generated.
			 *  			
			 *  Notice that LinkedHashSets are expected to be persistent, 
			 *  therfore alias sets need to be temporarily removed from the
			 *  set, then modified, then reinserted.
			 */
			
			AliasVariable aliasA = getAliasVariable(fvA);
			AliasVariable aliasB = getAliasVariable(fvB);
			
			if (aliasA != null && aliasB != null) {
				merge(aliasA, aliasB, neg);
			} else if (aliasA != null) {
				add(aliasA.getSet(), fvB, aliasA.isNegated() ? !neg : neg);
			} else if (aliasB != null) {
				add(aliasB.getSet(), fvA, aliasB.isNegated() ? !neg : neg);
			} else {
				join(fvA, fvB, neg);
			}
		}
		
		/**
		 * Merges two alias variables <code>aliasA</code> and <code>aliasB</code>.
		 * The implementation also check the rules for a alias set is maintained.
		 * 
		 * @param aliasA First alias variable
		 * @param aliasB Second alias variable
		 * @param neg True if the alias pair is negated, otherwise false.
		 */
		private void merge(AliasVariable aliasA, AliasVariable aliasB, boolean neg) {
			AliasSet mergeSet = aliasB.getSet();
			AliasSet resultSet = aliasA.getSet();
			if (resultSet == mergeSet) {
				if (aliasA.isNegated() == aliasB.isNegated() && neg) {
					aliasErrorFlag = true;
					aliasErrorMessage = "Alias error: trying to add the " +
					"negated alias pair (" + aliasA.getFVariable().name() + ",-" + 
					aliasB.getFVariable().name() + ") to the alias set " + resultSet;
				} else if (aliasA.isNegated() != aliasB.isNegated() && !neg) {
					aliasErrorFlag = true;
					aliasErrorMessage = "Alias error: trying to add the " +
					"alias pair (" + aliasA.getFVariable().name() + "," + 
					aliasB.getFVariable().name() + ") to the alias set " + resultSet;
				}
				return;
			}
			boolean shouldNegate = aliasA.isNegated() == aliasB.isNegated() ? neg : !neg;
			for (AliasVariable a : mergeSet) {
				if (shouldNegate)
					a.negated = !a.negated;
				resultSet.add(a);
			}
			aliasSets.remove(mergeSet);
		}
		
		/**
		 * Add a new variable to an exisiting alias set.
		 * 
		 * @param set The existing alias set
		 * @param fv The new alias variable
		 * @param neg True if the alias variable is negated, otherwise false.
		 */
		private void add(AliasSet set, FVariable fv, boolean neg) {
			set.add(new AliasVariable(fv, neg, variableMap));
		}
		
		/**
		 * Creates a new alias set based on the two variables.
		 * 
		 * @param fvA First variable
		 * @param fvB First variable
		 * @param neg True if the alias pair is negated, otherwise false.
		 */
		private void join(FVariable fvA, FVariable fvB, boolean negate) {
			AliasSet set = new AliasSet();
			aliasSets.add(set);
			set.add(new AliasVariable(fvA, false, variableMap));
			set.add(new AliasVariable(fvB, negate, variableMap));
		}
		
		/**
		 * \brief Print the alias sets, with start attributes.
		 * 
		 * @return A string containing the alias sets.
		 */
		public String printAliasSets() {
			StringBuilder str = new StringBuilder();
			for (AliasSet aliasSet : aliasSets) {
				str.append(aliasSet.toStringWithStart() +"\n");
			}
			return str.toString();		
		}
		
		/**
		 * A class representing a set of alias variables.
		 */
		public class AliasSet implements Iterable<AliasVariable> {
			
			// List of aliases
			private java.util.List<AliasVariable> aliases = new ArrayList<AliasVariable>();
			
			/**
			 * Private constructor
			 */
			private AliasSet() {
			}
			
			/**
			 * An iterator over all the alias variables in this set.
			 * 
			 * @return An iterator for this set
			 */
			@Override
			public Iterator<AliasVariable> iterator() {
				return aliases.iterator();
			}
			
			/**
			 * Adds a new alias variable to this set.
			 * 
			 * @param newAlias Alias variable to add
			 */
			private void add(AliasVariable newAlias) {
				newAlias.setSet(this);
				aliases.add(newAlias);
			}
			
			/**
			 * Get the iteration variable of this alias set.
			 * 
			 * @return The iteration variable of this alias set.
			 */
			public AliasVariable getIterationVariable() {
				// Find the variable with highest heuristic score
				AliasVariable best = null;
				int max = 0;
				for (AliasVariable av : aliases) {
					int score = av.getFVariable().aliasHeuristicScore();
					if (score > max) {
						max = score;
						best = av;
					}
				}
				return best;
			}
			
			/**
			 * Method for retreiving the number of aliases in this set.
			 * 
			 * @return The number of aliases in this set
			 */
			public int numAliases() {
				return aliases.size();
			}
			
			@Override
			public String toString() {
				return toString(false);
			}
			
			public String toStringWithStart() {
				return toString(true);
			}
			
			private String toString(boolean withStart) {
				String sep = withStart ? ", " : ",";
				StringBuilder str = new StringBuilder();
				str.append("{");
				
				AliasVariable iteration = getIterationVariable();
				str.append(iteration.toString(withStart));
				
				for (AliasVariable alias : aliases) {
					if (alias == iteration)
						continue;
					str.append(sep);
					str.append(alias.toString(withStart));
				}
				str.append("}");
				return str.toString();
			}
			
			/**
			 * A more verbose version of toString().
			 * 
			 * @return More info about this set
			 */
			public String toStringVerbose() {
				StringBuilder str = new StringBuilder();
				str.append("Alias set: (" + getIterationVariable().getFVariable().name() + ")\n");
				for (AliasVariable alias : aliases) {
					if (alias.isNegated()) {
						str.append("-");
					}
					str.append(alias.getFVariable().name());
					str.append(" " + alias.getFVariable().aliasHeuristicScore() + " ");
					str.append(" " + alias.getFVariable().varKind() + "\n");
				}
				return str.toString();
			}
			
		}
		
		/**
		 * \brief AliasVariable is used to encapsulate an FVariable and whether
		 * the alias is negated. 
		 * 
		 * AliasVariable implements the Comparable interface and objects of the 
		 * class are used in the alias sets.
		 */
		public static class AliasVariable {
			
			// The FVariable
			private FVariable fv;
			// Negated attribute
			private boolean negated = false;
			
			// The set that this alias variable belong to
			private AliasSet set;
			
			/**
			 * \brief Constructor.
			 * 
			 * @param fv           An FVariable.
			 * @param negated      True if the alias is negated, otherwise false.
			 * @param variableMap  Map to add the new AliasVariable to.
			 */
			private AliasVariable(FVariable fv, boolean negated, Map<FVariable, AliasVariable> variableMap) {
				this.fv = fv;
				this.negated = negated;
				variableMap.put(fv, this);
			}
		
			/**
			 * \brief Returns true if the alias is negated.
			 * 
			 * @return True if the alias is negated, otherwise false.
			 */
			public boolean isNegated() {
				return negated;
			}
		
			/**
			 * \brief Getter for the FVariable.
			 * 
			 * @return The FVariable.
			 */
			public FVariable getFVariable() {
				return fv;
			}	
			
			/**
			 * Returns the alias set that this alias variable belong to.
			 * 
			 * @return Alias set that this alias belong to
			 */
			public AliasSet getSet() {
				return set;
			}
			
			/**
			 * Private method for changing the set that this alias belong to.
			 * This method should only be used when the alias initially is added
			 * of when two alias sets are merged.
			 * 
			 * @param set The new set that this alias should belong to
			 */
			private void setSet(AliasSet set) {
				this.set = set;
			}
			
			@Override
			public String toString() {
				return toString(false);
			}
			
			protected String toString(boolean withStart) {
				StringBuilder buf = new StringBuilder();
				if (isNegated())
					buf.append('-');
				FVariable fv = getFVariable();
				buf.append(fv.name());
				if (withStart && fv.startAttributeSet()) {
					buf.append("(start=");
					buf.append(fv.startAttributeCValue());
					buf.append(")");
				}
				return buf.toString();
			}
			
		}
		
		
		public static class AttributePropagatorMap {

			private Map<String, AttributePropagator> map;
			private AliasVariable target;

			public AttributePropagatorMap(AliasVariable av) {
				map = new LinkedHashMap<String,AttributePropagator>();
				target = av;
				addAlias(av);
			}

			public void propagate() {
				List<FAttribute> l = new List<FAttribute>();
				for (AttributePropagator ap : map.values())
					ap.propagate(l);
				target.getFVariable().setFAttributeList(l);
			}
			
			public void addAlias(AliasVariable av) {
				FVariable fv = av.getFVariable();
				boolean neg = av.isNegated();
				for (FAttribute attr : fv.getFAttributes())
					addAttribute(attr, fv, neg);
				if (fv.myFDerivedType() != null)
					for (FAttribute attr : fv.myFDerivedType().getFAttributes())
						addTypeAttribute(attr, fv, neg);
			}
			
			private void addAttribute(FAttribute attr, FVariable fv, boolean neg) {
				get(attr.name()).addAttribute(attr, neg);
			}

			private void addTypeAttribute(FAttribute attr, FVariable fv, boolean neg) {
				AttributePropagator ap = get(attr.name());
				if (!ap.lastIsInFV(fv))
					ap.addAttribute(attr, neg);
			}

			private AttributePropagator get(String name) {
				AttributePropagator ap = map.get(name);
				if (ap == null) {
					ap = createAttributePropagator(name);
					map.put(name, ap);
				}
				return ap;
			}

			private AttributePropagator createAttributePropagator(String name) {
				// TODO: add support for more attributes
				if (name.equals(FAttribute.START))
					return new StartAttributePropagator();
				else
					return new OtherAttributePropagator(name);
			}

			private abstract class AttributePropagator {

				protected ArrayList<FAttribute> list = new ArrayList<FAttribute>();
				protected BitSet neg = new BitSet();

				public void addAttribute(FAttribute attr, boolean neg) {
					this.neg.set(list.size(), neg);
					list.add(attr);
				}

				public boolean lastIsInFV(FVariable fv) {
					int n = list.size();
					return n > 0 && list.get(n - 1).isInFV(fv);
				}
				
				public abstract void propagate(List<FAttribute> l);

			}
			
			/**
			 * Used for attributes that are not specifically handled.
			 * 
			 * Only adds back the attribute from the target variable, if any.
			 */
			private class OtherAttributePropagator extends AttributePropagator {

				private String name;

				public OtherAttributePropagator(String name) {
					this.name = name;
				}

				public void propagate(List<FAttribute> l) {
					if (list.size() > 0 && list.get(0).isInFV(target.getFVariable())) 
						l.add(list.get(0));
				}
				
			}
			
			/**
			 * Propagates start attribute. Selects the attribute that was set 
			 * "closest to the user".
			 * 
			 * Selects the value with lowest level among attributes set on variables, 
			 * or if no such are found the first found from a type. 
			 */
			private class StartAttributePropagator extends AttributePropagator {
				
				private final static int NO_KIND   = 0;
				private final static int TYPE_KIND = 1;
				private final static int FV_KIND   = 2;

				public void propagate(List<FAttribute> l) {
					FAttribute sel = null;
					int selKind = NO_KIND;
					int selLevel = 0;
					int selIndex = 0;
					int curIndex = 0;
					for (FAttribute attr : list) {
						int curKind = attr.isInFV() ? FV_KIND : TYPE_KIND;
						int curLevel = attr.getLevel();
						if (curKind > selKind || (curKind == FV_KIND && curLevel < selLevel)) {
							sel = attr;
							selKind = curKind;
							selLevel = curLevel;
							selIndex = curIndex;
						}
						curIndex++;
					}
					// Don't add attributes from own type
					FVariable targetFV = target.getFVariable();
					if (sel == null || (selKind == TYPE_KIND && targetFV.findAttribute(FAttribute.START) == sel))
						return;
					if (!sel.isInFV(targetFV))
						sel = sel.fullCopy();
					if (neg.get(selIndex) != target.isNegated() && sel.hasValue())
						sel.setValue(sel.getValue().createNegated());
					l.add(sel);
				}
				
			}
			
		}
	
	}
	
	/**
	 * \brief Get the heuristic score for keeping this variable in an alias set.
	 * 
	 * The variable with the highest score should be kept. Score is always > 0.
	 */
	syn int FVariable.aliasHeuristicScore() {
		int score = 1000;
		
		// Temporary variables added during transformations should get lower priority
		if (isTemporary())
			score -= 100;
		
		// 0. Derivatives
		if (isDerivativeVariable())
			return score;
		score--;
		
		// 1. Input
		if (isInput())
			return score;
		score--;
		
		// 2. parameter
		if (isParameter())
			return score;
		score--;
		
		// 3. non-parameter with fixed attribute set to true
		if (!isParameter() && fixedAttributeSet() && fixedAttributeExp().ceval().booleanValue())
			return score;
		score--;
				
		// 4. Variables with StateSelect-information
		if (isReal() && ((FRealVariable)this).stateSelectAttributeSet()) 
			return score;
		score--;
		
		if (getHGTType() == FClass.HGTVariableType.PAIR || getHGTType() == FClass.HGTVariableType.COMPONENT)
			return score;
		score--;
		
		// 5. start attribute set
		if (startAttributeSet())
			return score;
		score--;
		
		return score;
	}

}