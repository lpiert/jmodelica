/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import java.util.ArrayList;
import org.eclipse.jface.text.Position;
import org.jmodelica.ide.folding.FilePosition;
import org.jmodelica.ide.folding.FileCharacterPosition;

aspect ModelicaFolding {
	
	protected Position ASTNode.makeFoldingPosition(IDocument document) {
        try {
            int lineStart = ASTNode.getLine(getStart());
            int lineEnd = ASTNode.getLine(getEnd());
            int nbrOfLines = document.getNumberOfLines();
            int startOffset = document.getLineOffset(lineStart > 0 ? lineStart - 1 : 0);
            int endOffset = document.getLineOffset(lineEnd < nbrOfLines ? lineEnd : lineEnd - 1);
            int foldLength = endOffset - startOffset;
            if ((lineEnd - lineStart) > 0) 
                return createFoldingPosition(startOffset, foldLength, fileName());
        } catch (BadLocationException e) {
        }
    	return null;
    }
	
	// For when we have support for folds that does not span entire lines
//	protected Position Annotation.makeFoldingPosition(IDocument document) {
//		int start = getBeginOffset() + 1;
//		int end = getEndOffset();
//		int length = end - start + 1;
//    	if (start > 0 && length > 0) 
//	    	return createFoldingPosition(start, length, fileName());
//    	return null;
//    }
    
    protected ArrayList<Position> ASTNode.makeFoldingList(IDocument document) {
    	ArrayList<Position> res = new ArrayList<Position>(1);
    	Position pos = makeFoldingPosition(document);
    	if (pos != null)
	    	res.add(pos);
    	return res;
    }
    
    protected Position ASTNode.createFoldingPosition(int start, int length, String fileName) {
    	return new FilePosition(start, length, fileName);
    }
    
    protected Position Annotation.createFoldingPosition(int start, int length, String fileName) {
    	return new FileCharacterPosition(start, length, fileName);
    }

//    eq FullClassDecl.hasFolding() = true;
    eq Annotation.hasFolding() = true;
    eq Annotation.foldingPositions(IDocument document) = makeFoldingList(document);

    /*
     * We need to replace foldingPositions(), since we don't want any rewrites
     * (copied and altered from folding aspect in JastAdd IDE plugin).
     */
    refine Folding eq ASTNode.foldingPositions(IDocument document) {
        ArrayList list = new ArrayList();
        if (hasFolding()) {
        	Position pos = makeFoldingPosition(document);
        	if (pos != null)
        		list.add(pos);
        }
        for (ASTNode n : noTransform()) {
            list.addAll(n.foldingPositions(document));
        }
        return list;
    }

    /*
     * Since all children isn't always returned by getChild(), 
     * ClassDecl needs new foldingPositions().
     */
	eq ClassDecl.foldingPositions(IDocument document) {
		ArrayList list = new ArrayList();
        if (hasFolding()) {
        	Position pos = makeFoldingPosition(document);
        	if (pos != null)
        		list.add(pos);
        }
		for (ASTNode n : noTransform()) {
			if (!(n instanceof ClassDecl))
				list.addAll(n.foldingPositions(document));
		}
		for (ClassDecl decl : classes())
			list.addAll(decl.foldingPositions(document));
		return list;
	}

	/* 
	 * Don't traverse entire tree. We should avoid error nodes and nodes that can't contain folds anyway.
	 */
	protected static ArrayList ASTNode.EMPTY_LIST = new ArrayList();
	eq BadDefinition.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadClassDecl.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadElement.foldingPositions(IDocument document) = EMPTY_LIST;
	eq BadArgument.foldingPositions(IDocument document) = EMPTY_LIST;
	eq AbstractExp.foldingPositions(IDocument document) = EMPTY_LIST;
	eq Access.foldingPositions(IDocument document) = EMPTY_LIST;
	eq Equation.foldingPositions(IDocument document) = EMPTY_LIST;
    
}