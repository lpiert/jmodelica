<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<chapter>
  <title>Introduction</title>

  <section>
    <title>Theme of the internship</title>

    <para>The theme of the internship is to create and implement case studies
    for solutions of simulation and optimization problems based on technical
    models in Modelica. The models are developed from mathematical
    descriptions and Matlab source code and collected in the JModelica.org
    example library. These case studies can be very useful for customers to
    get an impression how their physical problem could possibly be realized
    and solved using JModelica.org. Therefore one of the created case studies
    is supposed to be added to the .<emphasis>JModelica Users
    Guide</emphasis></para>
  </section>

  <section>
    <title>Abstract</title>

    <para>In a first step the models should be set up in Modelica. To get
    started with Dymola, a software that uses the language Modelica, I
    implemented several examples from an already prepared workshop. For the
    examples of the "Van der Pol oscillator" (VDP) and the "Continuous Stirred
    Tank Reactor" (CSTR) the source code was already given and my task was to
    analyze and understand the problem, simulate and interpret the results,
    experiment with different inputs and finally write some documentation and
    create an icon in Dymola.</para>

    <para>The first example I implemented was the model of a distillation
    column, which demonstrates a typical simulation problem. The models are
    taken from the Hedengren repository
    (http://www.hedengren.net/research/models.htm). Based on four different
    versions written in Matlab, I translated the problem into the Modelica
    language, which is applied in Dymola. The models consist of ordinary
    differential equations (ODE) as well as algebraic differential equations
    (DAE). Comparing the plots I could verify the solution of the models
    written in Matlab.</para>

    <para>Thereafter the Dymola examples should be run in JModelica.org. I
    went through some examples to get started and learned how to compile and
    load the Dymola models in JModelica. I acquired a basic knowledge of the
    Python language, which is applied in addition to Modelica in JModelica.
    Due to the fact that I was developing new code I was introduced to the
    subversion system which offers the possibility to change and edit code by
    different persons at the same time. On the JModelica project planing
    platform the different projects are made public to provide an open base
    for team work.</para>

    <para>To solve not only simulation but also optimization problems the
    Modelica language involves the extension Optimica. In the next step I used
    this approach to set up various examples of the wide range of optimization
    problems such as optimal control, minimal time and parameter estimation
    problems. Therefore the PROPT page - a Matlab Optimal Control Software for
    DAEs and ODEs- was very useful since it provides source code based on
    Matlab for a huge number of models applied to optimization. In addition
    the references of the models are given, which turned out to be very useful
    to gain background information about the models. For all models I could
    verify my results by comparing them with the displayed plots and
    solutions.</para>

    <para>The most complex and interesting model was thereby the model of a
    Penicillin Plant, where the objective was to produce a maximal amount of
    Penicillin. The complexity arose from the fact that the model consists of
    two different phases and a method to provide initial guesses is
    integrated.</para>

    <para>Based on simulation models I also created self-made optimization
    problems and experimented with different inputs, objectives and
    constraints based on the background information of the references in the
    PROPT example library.</para>

    <para>The last chapter includes a personal review of the internship and
    introduces my approaches related to the content.</para>

    <para>In summary I could set up a huge facility of examples which motivate
    to create a wide range of various models. In this documentation only a few
    selected models are presented.</para>
  </section>
</chapter>
