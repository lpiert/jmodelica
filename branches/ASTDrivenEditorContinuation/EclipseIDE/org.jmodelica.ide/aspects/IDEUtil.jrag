/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.jmodelica.ide.helpers.Util;

aspect IDEUtil {
	
	syn StoredDefinition ASTNode.getDefinition()          = null;
	syn StoredDefinition StoredDefinition.getDefinition() = this;
	inh lazy StoredDefinition BaseNode.getDefinition();
	eq StoredDefinition.getChild(int index).getDefinition() = this;
	eq InstNode.getInstClassDecl(int index).getDefinition() = 
			getInstClassDecl(index).getClassDecl().getDefinition();
	eq InstNode.getInstComponentDecl(int index).getDefinition() = 
			getInstComponentDecl(index).getComponentDecl().getDefinition();
	eq FlatRoot.getFClass().getDefinition() = getFClass().storedDefinition();
	eq InstProgramRoot.getInstLibClassDecl(int index).getDefinition() = 
		getInstLibClassDecl(index).getClassDecl().getDefinition();
	
	private StoredDefinition FClass.definition;
	public void FClass.setDefinition(StoredDefinition sd) {
		definition = sd;
	}
	public StoredDefinition FClass.storedDefinition() {
		return definition;
	}

   public Object List.getLast() {
       if (getNumChild() > 0)
           return getChild(getNumChild() - 1);
       return null; 
   }
   
   inh boolean ClassDecl.isTopLevel();
   eq ClassDecl.getChild().isTopLevel()  = false;
   eq SourceRoot.getChild().isTopLevel() = true;
   eq Proxy.getChild().isTopLevel()      = true;
   
	//I hate double negating things :p
	public boolean InstClassDecl.isKnown() {
	    return !isUnknown();
	}
	
	public boolean InstComponentDecl.isKnown() {
	    return !isUnknown();
	}
	
	public List.List(T... ts) {
	    super();
	    for (T t : ts) {
	        add(t);
	    }
	}
	
}