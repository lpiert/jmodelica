<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Sum of 1/i Main Program</title>
<meta name="description" id="description" content="Sum of 1/i Main Program"/>
<meta name="keywords" id="keywords" content=" Openmp example program "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_sum_i_inv.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="multi_newton.hpp.xml" target="_top">Prev</a>
</td><td><a href="optimize.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>omp_max_thread</option>
<option>openmp_run.sh</option>
<option>sum_i_inv.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>seq_property</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>optimize</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>omp_max_thread-&gt;</option>
<option>openmp_run.sh</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>openmp_run.sh-&gt;</option>
<option>example_a11c.cpp</option>
<option>multi_newton.cpp</option>
<option>sum_i_inv.cpp</option>
</select>
</td>
<td>sum_i_inv.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>n_thread</option>
<option>repeat</option>
<option>mega_sum</option>
<option>Example Source</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>Sum of 1/i Main Program</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>sum_i_inv&#xA0;</span></font></code><i><span style='white-space: nowrap'>n_thread</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>repeat</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>mega_sum</span></i>

<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Runs a timing test of computing
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;1&#xA0;+&#xA0;1/2&#xA0;+&#xA0;1/3&#xA0;+&#xA0;...&#xA0;+&#xA0;1/</span></font></code><i><span style='white-space: nowrap'>n_sum</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>where <code><font color="blue"></font></code><i><span style='white-space: nowrap'>n_sum</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;1,000,000&#xA0;*&#xA0;</span></font></code><i><span style='white-space: nowrap'>mega_sum</span></i>

<br/>
<br/>
<b><big><a name="n_thread" id="n_thread">n_thread</a></big></b>
<br/>
If the argument <i>n_thread</i> is equal to <code><font color="blue">automatic</font></code>, 
dynamic thread adjustment is used.
Otherwise, <i>n_thread</i> must be a positive number
specifying the number of OpenMP threads to use.

<br/>
<br/>
<b><big><a name="repeat" id="repeat">repeat</a></big></b>
<br/>
If the argument <i>repeat</i> is equal to <code><font color="blue">automatic</font></code>,
the number of times to repeat the calculation of the number of zeros
in total interval is automatically determined.
In this case, the rate of execution of the total solution is reported.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>If the argument <i>repeat</i> is not equal to <i>automatic</i>,
it must be a positive integer.
In this case <i>repeat</i> determination of the number of times 
the calculation of the summation above.
The rate of execution is not reported (it is assumed that the
program execution time is being calculated some other way).

<br/>
<br/>
<b><big><a name="mega_sum" id="mega_sum">mega_sum</a></big></b>
<br/>
Is the value of <i>mega_sum</i> in the summation
(it must be greater than or equal to the number of threads).

<br/>
<br/>
<b><big><a name="Example Source" id="Example Source">Example Source</a></big></b>

<code><font color="blue">
<br/>
<pre style='display:inline'> 
# include &lt;cppad/cppad.hpp&gt;
# ifdef _OPENMP
# include &lt;omp.h&gt;
# endif

# include &lt;cassert&gt;
# ifdef _OPENMP
# include &lt;omp.h&gt;
# endif

# include &lt;cstring&gt;

namespace { // empty namespace
	int n_thread;
}

double sum_using_one_thread(int start, int stop)
{	// compute 1./start + 1./(start+1) + ... + 1./(stop-1)
	double sum = 0.;
	int i = stop;
	while( i &gt; start )
	{	i--;
		sum += 1. / double(i);	
	}
	return sum;
}
double sum_using_multiple_threads(int n_sum)
{	// compute 1. + 1./2 + ... + 1./n_sum
	assert( n_sum &gt;= n_thread );   // assume n_sum / n_thread &gt; 1

	// limit holds start and stop values for each thread
	int    limit[n_thread + 1];
	int i;
	for(i = 1; i &lt; n_thread; i++)
		limit[i] = (n_sum * i ) / n_thread;
	limit[0]         = 1;
	limit[n_thread]  = n_sum + 1;

	// compute sum_one[i] = 1/limit[i] + ... + 1/(limit[i+1} - 1)
	double sum_one[n_thread];
//--------------------------------------------------------------------------
# ifdef _OPENMP
# pragma omp parallel for 
# endif
	for(i = 0; i &lt; n_thread; i++)
		sum_one[i] = sum_using_one_thread(limit[i], limit[i+1]);
// -------------------------------------------------------------------------

	// compute sum_all = sum_one[0] + ... + sum_one[n_thread-1]
	double sum_all = 0.;
	for(i = 0; i &lt; n_thread; i++)
		sum_all += sum_one[i];

	return sum_all;
}

void test_once(double &amp;sum, size_t mega_sum)
{	assert( mega_sum &gt;= 1 );
	int n_sum = int(mega_sum * 1000000);
	sum = sum_using_multiple_threads(n_sum); 
	return;
}

void test_repeat(size_t size, size_t repeat)
{	size_t i;
	double sum;
	for(i = 0; i &lt; repeat; i++)
		test_once(sum, size);
	return;
}

int main(int argc, char *argv[])
{
	using std::cout;
	using std::endl;
	using CppAD::vector;

	const char *usage = &quot;sum_i_inv n_thread repeat mega_sum&quot;;
	if( argc != 4 )
	{	std::cerr &lt;&lt; usage &lt;&lt; endl;
		exit(1);
	}
	argv++;

	// n_thread command line argument
	if( std::strcmp(*argv, &quot;automatic&quot;) == 0 )
		n_thread = 0;
	else	n_thread = std::atoi(*argv);
	argv++;

	// repeat command line argument
	size_t repeat;
	if( std::strcmp(*argv, &quot;automatic&quot;) == 0 )
		repeat = 0;
	else
	{	assert( std::atoi(*argv) &gt; 0 );
		repeat = std::atoi(*argv);
	}
	argv++;

	// mega_sum command line argument 
	size_t mega_sum;
	assert( std::atoi(*argv) &gt; 0 );
	mega_sum = size_t( std::atoi(*argv++) );

	// minimum time for test (repeat until this much time)
	double time_min = 1.;

# ifdef _OPENMP
	if( n_thread &gt; 0 )
	{	omp_set_dynamic(0);            // off dynamic thread adjust
		omp_set_num_threads(n_thread); // set the number of threads 
	}
	// now determine the maximum number of threads
	n_thread = omp_get_max_threads();
	assert( n_thread &gt; 0 );
	
	// No tapes are currently active,
	// so we can inform CppAD of the maximum number of threads
	CppAD::<a href="ad.xml" target="_top">AD</a>&lt;double&gt;::omp_max_thread(size_t(n_thread));

	// inform the user of the maximum number of threads
	cout &lt;&lt; &quot;OpenMP: version = &quot;         &lt;&lt; _OPENMP;
	cout &lt;&lt; &quot;, max number of threads = &quot; &lt;&lt; n_thread &lt;&lt; endl;
# else
	cout &lt;&lt; &quot;_OPENMP is not defined, &quot;;
	cout &lt;&lt; &quot;running in single tread mode&quot; &lt;&lt; endl;
	n_thread = 1;
# endif
	// initialize flag
	bool ok = true;

	// Correctness check
	double sum;
	test_once(sum, mega_sum);
	double epsilon = 1e-6;
	size_t i = 0;
	size_t n_sum = mega_sum * 1000000;
	while(i &lt; n_sum)
		sum -= 1. / double(++i); 
	ok &amp;= std::fabs(sum) &lt;= epsilon;

	if( repeat &gt; 0 )
	{	// run the calculation the requested number of time
		test_repeat(mega_sum, repeat);
	}
	else
	{	// actually time the calculation	 

		// size of the one test case
		vector&lt;size_t&gt; size_vec(1);
		size_vec[0] = mega_sum;

		// run the test case
		vector&lt;size_t&gt; rate_vec =
		CppAD::speed_test(test_repeat, size_vec, time_min);

		// report results
		cout &lt;&lt; &quot;mega_sum         = &quot; &lt;&lt; size_vec[0] &lt;&lt; endl;
		cout &lt;&lt; &quot;repeats per sec  = &quot; &lt;&lt; rate_vec[0] &lt;&lt; endl;
	}
	// check all the threads for a CppAD memory leak
	if( CPPAD_TRACK_COUNT() != 0 )
	{	ok = false;
		cout &lt;&lt; &quot;Error: memory leak detected&quot; &lt;&lt; endl;
	}
	if( ok )
		cout &lt;&lt; &quot;Correctness Test Passed&quot; &lt;&lt; endl;
	else	cout &lt;&lt; &quot;Correctness Test Failed&quot; &lt;&lt; endl;

	return static_cast&lt;int&gt;( ! ok );
}
</pre>
</font></code>


<hr/>Input File: openmp/sum_i_inv.cpp

</body>
</html>
