/*
    Copyright (C) 2016 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


aspect AlgorithmScalarization {
    
    public class Scalarizer {
        public static class Algorithm {
            List<FAbstractVariable> vars; List<FStatement> stmts; Map<String,FExp> indexMap;
            public Algorithm(List<FAbstractVariable> vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
                this.vars = vars; this.stmts = stmts; this.indexMap = indexMap;
            }
        }
    }
    
    public static boolean FStatement.scalarizeStmtList(
            List<FStatement> fromList, List<FStatement> toList, 
            List vars, Map<String,FExp> indexMap) {
        boolean res = false;
        boolean breaker = false;
        for (FStatement stmt : fromList) {
            if (breaker)
                toList = FStatement.breakBlock(toList, stmt.createBreakCond());
            stmt.addSizeAsserts(toList);
            breaker = stmt.scalarize(vars, toList, indexMap);
            res = res || breaker;
        }
        return res;
    }
    
    public boolean FStatement.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        stmts.add((FStatement) fullCopy());
        return false;
    }
    
    public boolean FAssignStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        indexMap = new HashMap<String, FExp>(indexMap);
        
        FAssignableExp left = getLeft();
        FExp right = getRight();
        
        if (scalarizedAsFFunctionCallStmt()) {
            // Special case, Function call stmt
            FFunctionCall call = (FFunctionCall) right;
            call.getArgs().createArrayTemporaries(stmts, vars, indexMap);
            call.createFunctionCallClause(stmts, indexMap, left);
        } else {
            FType t = left.size().isUnknown() ? left.type() : right.type();
            if (extractTemp()) {
                addTempVar(stmts, vars, indexMap, t);
                FIdUseExp temp = getRight().tempExp(tempVarName());
                t.scalarizeAssignment(vars, stmts, indexMap, temp, right);
                right = temp;
            }
            t.scalarizeAssignment(vars, stmts, indexMap, left, right);
        }
        return false;
    }
    
    
    syn boolean FAssignStmt.scalarizedAsFFunctionCallStmt() = 
            getRight().isNonVectorizedFunctionCall() && getLeft().type().isComposite() && 
            inFunction() && !getLeft().isSlice();
    
    syn boolean FExp.isNonVectorizedFunctionCall()          = false;
    eq InstFunctionCall.isNonVectorizedFunctionCall()       = true;
    eq InstVectorFunctionCall.isNonVectorizedFunctionCall() = false;
    eq FFunctionCall.isNonVectorizedFunctionCall()          = true;
    eq FVectorFunctionCall.isNonVectorizedFunctionCall()    = false;
    
    public boolean FFunctionCallStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        indexMap = new HashMap<String, FExp>(indexMap);
        FFunctionCallStmt stmt = new FFunctionCallStmt();
        getCall().createArrayTemporaries(stmts, vars, indexMap);
        getLefts().createArrayTemporaries(stmts, vars, indexMap);
        stmts.add(stmt);
        getLefts().createArrayTemporaries(stmts, null, indexMap);
        for (FFunctionCallLeft left : getLefts())
            stmt.addLeft(left.scalarize(indexMap));
        stmt.setCall((FAbstractFunctionCall) getCall().scalarizeExp(indexMap));
        return false;
    }
    
    public boolean FIfWhenStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        boolean breaker = false;
        boolean res = false;
        indexMap = new HashMap<String, FExp>(indexMap);
        for (FIfWhenClause cl : getFIfWhenClauses())
            cl.getTest().createArrayTemporaries(stmts, vars, indexMap);
        FIfWhenStmt stmt = createEmptyNode();
        for (FIfWhenClause cl : getFIfWhenClauses()) {
            breaker = cl.scalarize(vars, stmt.getFIfWhenClauses(), indexMap);
            res = res || breaker;
        }
        breaker = scalarizeElse(vars, stmt, indexMap);
        res = res || breaker;
        stmts.add(stmt);
        return res;
    }
    
    /**
     * Scalarize the else part of an if statement. Does nothing for when statements.
     */
    public boolean FIfWhenStmt.scalarizeElse(List vars, FIfWhenStmt stmt, Map<String,FExp> indexMap) { 
        return false; 
    }
    
    public boolean FIfStmt.scalarizeElse(List vars, FIfWhenStmt stmt, Map<String,FExp> indexMap) {
        FIfStmt ifstmt = (FIfStmt) stmt;
        return FStatement.scalarizeStmtList(getElseStmts(), ifstmt.getElseStmtList(), vars, indexMap);
    }
    
    /**
     * Scalarize the if or when clause.
     */
    public boolean FIfWhenClause.scalarize(List vars, List<FIfWhenClause> clauses, Map<String,FExp> indexMap) {
        FExp test = getTest().scalarize(indexMap);
        List<FStatement> stmts = new List<FStatement>();
        clauses.add(createNode(test, stmts));
        return FStatement.scalarizeStmtList(getFStatements(), stmts, vars, indexMap);
    }
    
    public boolean FForStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        if (inFunction()) {
            indexMap = new HashMap<String, FExp>(indexMap);
            getIndex().createArrayTemporaries(stmts, vars, indexMap);
            FForStmt stmt = new FForStmt();
            stmt.setIndex(getIndex().scalarize(indexMap));
            FStatement.scalarizeStmtList(getForStmts(), stmt.getForStmtList(), vars, indexMap);
            stmts.add(stmt);
        } else {
            List<FStatement> inner = stmts;
            boolean hasBreak = getForStmts().containsBreakStmt();
            if (hasBreak) {
                vars.add(myBreakVar());
                stmts.add(new FAssignStmt(myBreakVar().createUseExp(), new FBooleanLitExpTrue()));
            }
            ArrayList<FForIndex> indexList = new ArrayList();
            indexList.add(getIndex());
            Indices indices = Indices.create(indexList);
            Map<String,FExp> myIndexMap = new HashMap<String,FExp>();
            myIndexMap.putAll(indexMap);
            for (Index i : indices) {
                if (hasBreak)
                    inner = FStatement.breakBlock(stmts, myBreakVar().createUseExp());
                indices.fillIndexMap(myIndexMap, i, indexList);
                FStatement.scalarizeStmtList(getForStmts(), inner, vars, myIndexMap);
                getForStmts().flushForScalarization(true);
            }
            for (FForIndex fi : indexList)
                fi.clearEvaluationValue();
        }
        return false;
    }
    
    public boolean FWhileStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        indexMap = new HashMap<String, FExp>(indexMap);
        getTest().createArrayTemporaries(stmts, vars, indexMap);
        FWhileStmt stmt = new FWhileStmt();
        stmt.setTest(getTest().scalarize(indexMap));
        FStatement.scalarizeStmtList(getWhileStmts(), stmt.getWhileStmtList(), vars, indexMap);
        getTest().createArrayTemporaries(stmt.getWhileStmtList(), null, indexMap);
        stmts.add(stmt);
        return false;
    }
    
    public boolean FBreakStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        FStatement loop = enclosingLoop();
        if (!inFunction() && loop instanceof FForStmt) {
            stmts.add(new FAssignStmt(createBreakCond(), new FBooleanLitExpFalse()));
            return true;
        } else {
            return super.scalarize(vars, stmts, indexMap);
        }
    }
    
    public static List<FStatement> FStatement.breakBlock(List<FStatement> stmts, FExp breakCond) {
        FIfStmt ifStmt = new FIfStmt();
        FIfClause clause = new FIfClause();
        clause.setTest(breakCond.fullCopy());
        ifStmt.addFIfWhenClause(clause);
        stmts.add(ifStmt);
        return clause.getFStatements();
    }
    
    private FVariable FForStmt.breakVar = null;
    private FVariable FForStmt.myBreakVar(){
        if (breakVar == null) {
            FQName fqn = new FQNameString(getIndex().getFExp().calcTempVarName());
            FVariable fv = new FBooleanVariable(new FTemporaryVisibilityType(), fDiscrete(), fqn);
            breakVar = fv;
        }
        return breakVar;
    }
    
    inh FIdUseExp FStatement.createBreakCond();
    eq Root.getChild().createBreakCond() = null;
    eq FForStmt.getChild().createBreakCond() = myBreakVar().createUseExp();
    
    syn boolean ASTNode.containsBreakStmt() {
        for (ASTNode n : this)
            if (n.containsBreakStmt())
                return true;
        return false;
    }
    eq FForStmt.containsBreakStmt() {
        return false;
    }
    eq FWhileStmt.containsBreakStmt() {
        return false;
    }
    eq FBreakStmt.containsBreakStmt() {
        return true;
    }
    
    public boolean FInitArrayStmt.scalarize(List vars, List<FStatement> stmts, Map<String,FExp> indexMap) {
        type().scalarizeInitArray(new Scalarizer.Algorithm(vars, stmts, indexMap), null);
        return super.scalarize(vars, stmts, indexMap);
    }
    
    public void FType.scalarizeInitArray(Scalarizer.Algorithm scalarizer, ASTNode context) {
        setSize(getSize().scalarizeInitArray(scalarizer, context == null ? this : context));
    }
    
    public Size Size.scalarizeInitArray(Scalarizer.Algorithm scalarizer, ASTNode context) {
        return this;
    }
    
    @Override
    public Size MutableSize.scalarizeInitArray(Scalarizer.Algorithm scalarizer, ASTNode context) {
        MutableSize res = new MutableSize(ndims());
        for (int i = 0; i < exps.length; i++) {
            res.size[i] = size[i];
            if (exps[i] != null) {
                exps[i].createArrayTemporaries(scalarizer.stmts, scalarizer.vars, scalarizer.indexMap, new ForNames());
                res.exps[i] = exps[i].scalarize(scalarizer.indexMap);
                res.exps[i].parent = context;
            }
        }
        return res;
    }
}

aspect AssignmentScalarization {
    /**
     * Scalarize an assignment and put the resulting statements in the list of statements. 
     */
    public void FType.scalarizeAssignment(
            List vars, List<FStatement> stmts, 
            Map<String,FExp> indexMap, FAssignableExp left, FExp right) {
        
        FExp expInAst = right.getParent() != null ? right : left;
        
        ForNames names = new ForNames();
        if (size().isUnknown()) {
            names.addLayer(ndims());
            names.fillLayer(expInAst);
        }
        
        left.createArrayTemporaries(stmts, vars, indexMap, names);
        right.createArrayTemporaries(stmts, vars, indexMap, names);
        
        if (size().isUnknown()) {
            left.addArrayUsesToIndexMap(indexMap, names);
            right.addArrayUsesToIndexMap(indexMap, names);
            stmts = names.createForLoops(stmts, expInAst);
            names.removeLayer();
        }
        
        scalarizeAssignment_sub(vars, stmts, indexMap, left, right);
    }
    
    public void FType.scalarizeAssignment_sub(
            List vars, List<FStatement> stmts, 
            Map<String,FExp> indexMap, FAssignableExp left, FExp right) {
        if (size().isUnknown() || !isArray()) {
            scalarizeScalarAssignment(vars, stmts, indexMap, left, right);
        } else {
            scalarizeArrayAssignment(vars, stmts, indexMap, left, right);
        }
    }
    
    /**
     * Scalarize an array assignment of known size.
     */
    public void FType.scalarizeArrayAssignment(
        List vars, List<FStatement> stmts, 
        Map<String,FExp> indexMap, FAssignableExp left, FExp right) {
        
        Indices ind = size().isUnknown() ? right.indices() : indices();
        for (Index i : ind) {
            scalarizeScalarAssignment(vars, stmts, indexMap,
                    (FAssignableExp) left.extractArrayCell(indexMap, i),
                    right.extractArrayCell(indexMap, i));
        }
    }
    
    /**
     * Scalarize an assignment of record type.
     */
    protected void FRecordType.scalarizeScalarAssignment(
            List vars, List<FStatement> stmts, 
            Map<String,FExp> indexMap, FAssignableExp left, FExp right) {
        scalarRecordClauses(stmts, false, new FQNameEmpty(), indexMap, left, right);
    }
    
    /**
     * Scalarize a scalar assignment
     */
    protected void FType.scalarizeScalarAssignment(
        List vars, List<FStatement> stmts, 
        Map<String,FExp> indexMap, FAssignableExp left, FExp right) {
    
        left = (FAssignableExp) left.scalarize(indexMap);
        right = right.scalarize(indexMap);
        
        stmts.add(new FAssignStmt(left,right));
    }
}
