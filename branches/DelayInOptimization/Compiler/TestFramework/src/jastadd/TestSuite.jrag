/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.IOException;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class TestSuite {
	String name;
	private ArrayList<TestCase> l;

	public TestSuite() {
		l = new ArrayList<TestCase>();
	}

	public TestSuite(String fileName, String className, TestCase.Assert asserter) {
		name = className;
		l = new ArrayList<TestCase>();
		ParserHandler ph = new ParserHandler();
		SourceRoot sr = null;
		try {
			sr = ph.parseFile(fileName);
		} catch (Exception e) {
			asserter.fail("Error when parsing file: " + fileName + "\n" + e);
		}
		
		OptionRegistry or = ModelicaCompiler.createOptions();
		String modelica_path = System.getenv("JMODELICA_HOME") + File.separator + 
				"ThirdParty" + File.separator + "MSL";
		or.addStringOption("MODELICAPATH", modelica_path);
		sr.options = or;
		ASTNode.log = new SysErrLogger();

		sr.collectTestCases(this,className);
	}
	
	public File dumpJunit(String testFile, String dir) {
		SourceRoot sr = new SourceRoot();
		String lang = sr.language().toLowerCase();
		
		StringBuilder str = new StringBuilder();
		str.append("package org.jmodelica.test." + lang + ".junitgenerated;\n\n");
		str.append("import org.junit.*;\n");
		str.append("import org.jmodelica." + lang + ".compiler.*;\n\n");

		str.append("public class " + name + " {\n\n");
		str.append("  static TestSuite ts;\n\n");
		
		str.append("  private static class Assert implements TestCase.Assert {\n");
		str.append("    public void fail(String msg) {\n");
		str.append("      org.junit.Assert.fail(msg);\n");
		str.append("    }\n\n");
		str.append("    public void assertEquals(String msg, String expected, String actual) {\n");
		str.append("      org.junit.Assert.assertEquals(msg, expected, actual);\n");
		str.append("    }\n");
		str.append("  }\n");
		str.append("  static Assert asserter = new Assert();\n\n");
		
		String escaped = testFile.replaceAll("\\\\", "\\\\\\\\");  // Replaces \ with \\
		str.append("  @BeforeClass public static void setUp() {\n");
		str.append("    ts = new TestSuite(\"" + escaped + "\", \"" + getName() + "\", asserter);\n");
		str.append("  }\n\n");
		
		for (int i=0; i < l.size(); i++) 
			get(i).dumpJunit(str, i);

		str.append("  @AfterClass public static void tearDown() {\n");
		str.append("    ts = null;\n");
		str.append("  }\n\n");
		str.append("}\n\n");
		
		File file = new File(dir, getName() + ".java");
		try {
			FileWriter writer = new FileWriter(file);
			writer.append(str.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return file;
	}
	
	public void add(TestCase tc) {
		l.add(tc);
	}

	public TestCase get(int i) {
		return l.get(i);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	private static Document parseAndGetDOM(String xmlfile) throws ParserConfigurationException, IOException, SAXException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(true);
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(new File(xmlfile));
		return doc;
	}
	
	private static class SysErrLogger extends ModelicaLogger {
		public SysErrLogger() {
			super(Level.WARNING);
		}
		
		@Override
		public void close() {}

		@Override
		protected void write(Level level, String logMessage) {
			System.err.println(logMessage);
		}
		
		@Override
		protected void write(Level level, Throwable throwable) {
			write(level, throwable.toString());
		}

		@Override
		protected void write(Level level, Problem problem) {
			write(level, problem.toString());
		}
	}
	
}
