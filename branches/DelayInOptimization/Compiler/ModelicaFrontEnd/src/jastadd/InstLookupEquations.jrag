aspect InstLookupEquations{
	
	syn InstAccess InstAccess.convertToEquationAccess();
	eq InstGlobalAccess.convertToEquationAccess() = copyLocationTo(new InstGlobalAccess(getInstAccess().convertToEquationAccess()));
	eq InstDot.convertToEquationAccess() {
		List<InstAccess> l = new List<InstAccess>();
		List<InstAccess> instAccessList = getInstAccessListNoTransform();
		int numInstAccess = instAccessList.getNumChildNoTransform();
		for (int i = 0; i < numInstAccess - 1; i++)
			l.add(instAccessList.getChildNoTransform(i).fullCopy());
		l.add(instAccessList.getChildNoTransform(numInstAccess - 1).convertToEquationAccess());
		return copyLocationTo(new InstDot(l));
	}
	eq InstArrayAccess.convertToEquationAccess() = copyLocationTo(new InstEquationArrayAccess(getID(), getFArraySubscripts()));
	eq InstScalarAccess.convertToEquationAccess() = copyLocationTo(new InstEquationAccess(getID()));
	eq InstClassAccess.convertToEquationAccess() = copyLocationTo(new InstEquationAccess(getID()));
	
	
	inh InstNode InstAccess.equationLookupScope();
	eq InstDot.getInstAccess(int i).equationLookupScope() = 
		(i == 0) ? equationLookupScope() : getInstAccessNoListTrans(i - 1).myInstComponentDecl();
	eq InstNode.getChild().equationLookupScope() = this;
	eq InstRoot.getChild().equationLookupScope() = null;
	eq FlatRoot.getChild().equationLookupScope() = null;
	
	syn FAbstractEquation InstAccess.myEquation() {
		throw new UnsupportedOperationException();
	}
	eq InstDot.myEquation() = getLastInstAccess().myEquation();
	eq InstEquationAccess.myEquation() = equationLookupScope().memberEquation(getID());
	eq InstEquationArrayAccess.myEquation() = equationLookupScope().memberEquation(getID());
	eq InstGlobalAccess.myEquation() = getInstAccess().myEquation();
	
	syn FAbstractEquation InstNode.memberEquation(String name) {
		for (FAbstractEquation eqn : getFAbstractEquations()) {
			FAbstractEquation match = eqn.memberEquation(name);
			if (match != null)
				return match;
		}
		for (InstExtends node : getInstExtendss()) {
			FAbstractEquation match = node.memberEquation(name);
			if (match != null)
				return match;
		}
		return null;
	}
	
	syn FAbstractEquation FAbstractEquation.memberEquation(String name) = name != null && name.equals(name()) ? this : null;
	eq InstForClauseE.memberEquation(String name) {
		FAbstractEquation match = super.memberEquation(name);
		if (match != null)
			return match;
		for (FAbstractEquation eqn : getFAbstractEquations()) {
			match = eqn.memberEquation(name);
			if (match != null)
				return match;
		}
		return null;
	}
	
	syn FAbstractEquation FExp.referencedEquation() = null;
	eq FInstAccessExp.referencedEquation() = getInstAccess().myEquation();
	
	syn String FAbstractEquation.name() {
	    FAttribute attr = findAttribute("name");
	    if (attr == null || !attr.isDecl())
	        return null;
	    return attr.getFIdDecl().name();
	}
	
}