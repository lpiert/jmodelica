/*
Copyright (C) 2014 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jmodelica.util.CodeStream;
import org.jmodelica.util.NotNullCodeStream;

/**
 * Aspect that adds XML codegeneration for flat models to the compiler
 */
aspect XMLCodeGenerator {
	
	/**
	 * Printer class that is used for printing nodes in the AST to a stream
	 */
	public class XMLPrettyPrint extends Printer {
		public XMLPrettyPrint() {
			super("\t");
		}
		
		public void print(ASTNode node, CodeStream str, String indent) {
			node.prettyPrintXML(this, str, indent);
		}
	}
	
	public void ASTNode.prettyPrintXML(CodeStream str, String indent) {
		prettyPrintXML(new XMLPrettyPrint(), str, indent);
	}
	
	public void ASTNode.prettyPrintXML(Printer p, CodeStream str, String indent) {
		prettyPrint(p, str, indent);
	}
	
	//=========================================================================
	// Helpers for XML generation
	//=========================================================================
	private static final String ASTNode.OPEN_TAG = "%s<%s%s>\n";
	private static final String ASTNode.CLOSED_TAG = "%s<%s%s/>\n";
	private static final String ASTNode.CLOSE_TAG = "%s</%s>\n";
	
	public static void ASTNode.openTagXML(CodeStream str, String tag, String indent) {
		openTagXML(str, tag, indent, null);
	}
	
	public static void ASTNode.openTagXML(CodeStream str, String tag, String indent,
			String attributeName, String attributeValue) {
		String attribute = " " + attributeName + "=\"" + attributeValue + "\"";
		str.format(OPEN_TAG, indent, tag, attribute);
	}

	public static void ASTNode.openTagXML(CodeStream str, String tag, String indent, 
			Map<String, String> attributes) {
		generateXMLTag(str, tag, indent, attributes, OPEN_TAG);
	}

	public static void ASTNode.closedTagXML(CodeStream str, String tag, String indent) {
		closedTagXML(str, tag, indent, null);
	}
	
	public static void ASTNode.closedTagXML(CodeStream str, String tag, String indent,
			String attributeName, String attributeValue) {
		String attribute = " " + attributeName + "=\"" + attributeValue + "\"";
		str.format(CLOSED_TAG, indent, tag, attribute);
	}
	
	public static void ASTNode.closedTagXML(CodeStream str, String tag, String indent, 
			Map<String, String> attributes) {
		generateXMLTag(str, tag, indent, attributes, CLOSED_TAG);
	}
	
	public static void ASTNode.closeTagXML(CodeStream str, String tag, String indent) {
		str.format(CLOSE_TAG, indent, tag);
	}
	
	public static void ASTNode.generateXMLTag(CodeStream str, String tag, String indent, Map<String, String> attributes, String format) {
		StringBuilder allAttributes = new StringBuilder();
		if (attributes != null) {
			for (Map.Entry<String, String> attribute : attributes.entrySet()) {	
				allAttributes.append(" ").append(attribute.getKey()).append("=\"")
					.append(attribute.getValue()).append("\"");
			}
		}
		str.format(format, indent, tag, allAttributes.toString());
	}
	
	syn boolean ASTNode.isAlgorithm() = false;
	eq FAlgorithm.isAlgorithm() = true;
	
	/**
	 * Entry point for XML generation, generates XML for child nodes by calling
	 * their print method
	 */
	public void FClass.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		String indentThreeSteps = p.indent(indentTwoSteps);
		openTagXML(str, "class", indent, "kind", "model");
		
		for (FRecordDecl record : getFRecordDecls()) {
			p.print(record, str, indentOneStep);
			str.print("\n");
		}
		
		for (FDerivedType ftype : getFDerivedTypes()) {
			p.print(ftype, str, indentOneStep);
			str.print("\n");
		}
		
		for (FEnumDecl fenum : getFEnumDecls()) {
			p.print(fenum, str, indentOneStep);
			str.print("\n");
		}
		
		for (FFunctionDecl function : getFFunctionDeclList()) {
			p.print(function, str, indentOneStep);
			str.print("\n");
		}
		
		for (FVariable fv : getFVariables()) {
			p.print(fv, str, indentOneStep);
		}

		// not exported for now
		/*for (FVariable fv : getAliasVariables()) {
			p.print(fv, str, indentOneStep);
		}*/
		
		// add a newline between sections for clearer XML
		str.print("\n");
		
		// generate xml for initial equations
		if (getNumFInitialEquation() > 0) {
			ArrayList<FAbstractEquation> initAlgorithms = new ArrayList<FAbstractEquation>();
			ArrayList<FAbstractEquation> initEquations = new ArrayList<FAbstractEquation>();
			for (FAbstractEquation initEqu : getFInitialEquations()) {
				if (initEqu.isAlgorithm()) {
					initAlgorithms.add(initEqu);
				} else {
					initEquations.add(initEqu);
				}
			}
			
			if (initEquations.size() > 0) {
				openTagXML(str, "equation", indentOneStep, "kind", "initial");
				for (FAbstractEquation initEqu : initEquations) {
					p.print(initEqu, str, indentTwoSteps);
				}
				closeTagXML(str, "equation", indentOneStep);
			}
			
			if (initAlgorithms.size() > 0) {
				openTagXML(str, "algorithm", indentOneStep, "kind", "initial");
				for (FAbstractEquation initAlg : initAlgorithms) {
					p.print(initAlg, str, indentTwoSteps);
				}
				closeTagXML(str, "algorithm", indentOneStep);
			}
			
			// add a newline between sections for clearer XML
			str.print("\n");
		}
		
		// generate xml for parameter equations
		if (getNumFParameterEquation() > 0) {
			openTagXML(str, "equation", indentOneStep, "kind", "parameter");
			for (FAbstractEquation paramEqu : getFParameterEquations()) {
				p.print(paramEqu, str, indentTwoSteps);
			}
			closeTagXML(str, "equation", indentOneStep);
			str.print("\n");
		}
		
		// generate xml for equations
		if (hasFAbstractEquation()) {
			ArrayList<FAbstractEquation> algorithms = new ArrayList<FAbstractEquation>();
			ArrayList<FAbstractEquation> equations = new ArrayList<FAbstractEquation>();
			for (FAbstractEquation equ : getFAbstractEquations()) {
				if (equ.isAlgorithm()) {
					algorithms.add(equ);
				} else {
					equations.add(equ);
				}
			}
			
			if (equations.size() > 0) {
				openTagXML(str, "equation", indentOneStep);
				for (FAbstractEquation equ : equations) {
					p.print(equ, str, indentTwoSteps);
				}
				closeTagXML(str, "equation", indentOneStep);	
			}
			
			// add a newline between sections for clearer XML
			str.print("\n");
			
			if (algorithms.size() > 0) {
				openTagXML(str, "algorithm", indentOneStep);
				for (FAbstractEquation alg : algorithms) {
					p.print(alg, str, indentTwoSteps);
				}
				closeTagXML(str, "algorithm", indentOneStep);
			}
		}
		closeTagXML(str, "class", indent);
	}
	
	//=========================================================================
	// Code generation for declarations
	//=========================================================================
	public void FVariable.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		if (getFVisibilityType().isRuntimeOptionVisibility()) {
			// do not generate runtime variables
			return;
		}
		
		FTypePrefixInputOutput causalityPrefix = getFTypePrefixInputOutput();
		FTypePrefixVariability variabilityPrefix = getFTypePrefixVariability();		
		Map<String, String> attributes = new LinkedHashMap<String, String>();
		attributes.put("name", displayName());
		if (!variabilityPrefix.toString().equals("")) {
			attributes.put("variability", variabilityPrefix.toString().trim());			
		}
		if (causalityPrefix != null) {
			attributes.put("causality", causalityPrefix.toString());
		}
		if (hasFStringComment()) {
			String comment = getFStringComment().toString();
			comment = comment.replace("\"", "");
			attributes.put("comment", comment.trim());
		}
		
		openTagXML(str, "component", indent, attributes);
		String variableType = prettyPrintType();
		if (variableType.equals("Real") || variableType.equals("Integer") || 
				variableType.equals("Boolean") || variableType.equals("String")) {
			closedTagXML(str, "builtin", indentOneStep, "name", variableType);
		} else {
			closedTagXML(str, "local",  indentOneStep, "name", variableType);
		}
		
		// loop through attributes and generate modifiers
		if (hasFAttribute()) {
			if (isAlias()) {
				openTagXML(str, "bindingExpression", indentOneStep);
				openTagXML(str, "local", indentTwoSteps, "name", alias().name());
				closeTagXML(str, "bindingExpression", indentOneStep);
			} else {
				openTagXML(str, "modifier", indentOneStep);
				for (FAttribute attr : getFAttributes()) {
					p.print(attr, str, indentTwoSteps);
				}
				closeTagXML(str, "modifier", indentOneStep);
			}
		}
		
		// generate bindingexpression if it exists
		if (hasBindingExp()) {
			openTagXML(str, "bindingExpression", indentOneStep);
			p.print(getBindingExp(), str, indentTwoSteps);
			closeTagXML(str, "bindingExpression", indentOneStep);
		}
		closeTagXML(str, "component", indent);
	}
	
	public void FDerivativeVariable.prettyPrintXML(Printer p, CodeStream str, String indent) {
	}
	
	public void FEnumDecl.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "classDefinition", indent, "name", getName().name());
		openTagXML(str, "enumeration", indentOneStep);
		for (FEnumLiteral fenum : getFEnumSpecification().enumLiterals()) {
			p.print(fenum, str, indentTwoSteps);
		}
		closeTagXML(str, "enumeration", indentOneStep);
		closeTagXML(str, "classDefinition", indent);
	}
	
	public void FEnumLiteral.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "item", indent, "name", getName().name());
	}
	
	public void FAttribute.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "item", indent, "name", getName().name());
		if (hasValue()) {
			p.print(getValue(), str, p.indent(indent));
		}
		closeTagXML(str, "item", indent);
	}
	
	public void FDerivedType.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		if (!getBaseType().isEnum()) {
			openTagXML(str, "classDefinition", indent, "name", getName());
			closedTagXML(str, "builtin", indentOneStep, "name", getBaseType().toString());
			if (hasFAttribute()) {
				openTagXML(str, "modifier", indentOneStep);
				for (FAttribute attr : getFAttributeList()) {
					p.print(attr, str, indentTwoSteps);
				}
				closeTagXML(str, "modifier", indentOneStep);
			}
			closeTagXML(str, "classDefinition", indent);
		}
	}
	
	public void FFunctionVariable.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		Map<String, String> attributes = new LinkedHashMap<String, String>();
		attributes.put("name", name());
		if (hasFTypePrefixInputOutput()) {
			attributes.put("causality", getFTypePrefixInputOutput().toString());
		}
		openTagXML(str, "component", indent, attributes);
		String variableType = getType().toString();
		if (variableType.equals("Real") || variableType.equals("Integer") || 
				variableType.equals("Boolean") || variableType.equals("String")) {
			closedTagXML(str, "builtin", indentOneStep, "name", variableType);
		} else {
			closedTagXML(str, "local",  indentOneStep, "name", variableType);
		}
		if (getType().isArray()) {
			for (int i=0; i < getType().size().ndims(); i++) {
				openTagXML(str, "dimension", indentOneStep);
				closedTagXML(str, "integer", indentTwoSteps, "value", Integer.toString(getType().size().get(i)));
				closeTagXML(str, "dimension", indentOneStep);
			}
		}
		
		if (hasBindingExp()) {
			openTagXML(str, "bindingExp", indentOneStep);
			p.print(getBindingExp(), str, indentTwoSteps);
			closeTagXML(str, "bindingExp", indentOneStep);
		}
		closeTagXML(str, "component", indent);
	}
	
	/*public void FFunctionArray.prettyPrintXML(Printer p, CodeStream str, String indent) {
		for (int i=0; i < size().numElements(); i++) {
			openTagXML(str, "component", indent, "name", (getFQName().toString() + "_" + i));
			closeTagXML(str, "component", indent);
		}
	}*/
	
	public void FRecordDecl.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "classDefinition", indent, "name", name());
		openTagXML(str, "class", indentOneStep, "kind", "record");
		for (FVariable var : getFVariables()) {
			p.print(var, str, indentTwoSteps);
		}
		closeTagXML(str, "class", indentOneStep);
		closeTagXML(str, "classDefinition", indent);
	}
	
	public void FRecordConstructor.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "call", indent);
		openTagXML(str, "function", indentOneStep);
		closedTagXML(str, "local", indentTwoSteps, "name", getRecord().name());
		closeTagXML(str, "function", indentOneStep);
		for (FExp arg : getArgs()) {
			p.print(arg, str, indentOneStep);
		}
		closeTagXML(str, "call", indent);
	}
	
	//=========================================================================
	// Code generation for expressions
	//=========================================================================
	// if expressions, nested if expression is quite ugly maybe try to fix this if possible
	public void FIfExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "if", indent);
		openTagXML(str, "cond", indentOneStep);
		p.print(getIfExp(), str, indentTwoSteps);
		closeTagXML(str, "cond", indentOneStep);
		openTagXML(str, "then", indentOneStep);
		p.print(getThenExp(), str, indentTwoSteps);
		closeTagXML(str, "then", indentOneStep);
		openTagXML(str, "else", indentOneStep);
		p.print(getElseExp(), str, indentTwoSteps);
		closeTagXML(str, "else", indentOneStep);
		closeTagXML(str, "if", indent);
	}
	
	public void FArray.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "call", indent, "builtin", "array");
		for (FExp exp : getFExps()) {
			p.print(exp, str, p.indent(indent));
		}
		closeTagXML(str, "call", indent);
	}
	
	// binary operators
	syn String ASTNode.XMLOperator() = null;
	// < is represented by &lt; in xml
	eq FLtExp.XMLOperator() = "&lt;";
	eq FLeqExp.XMLOperator() = "&lt;=";
	// > is represented by &gt; in xml
	eq FGtExp.XMLOperator() = "&gt;";
	eq FGeqExp.XMLOperator() = "&gt;=";
	eq FEqExp.XMLOperator() = "==";
	eq FNeqExp.XMLOperator() = "&lt;&gt;";
	eq FOrExp.XMLOperator() = "or";
	eq FAndExp.XMLOperator() = "and";
	eq FAddExp.XMLOperator() = "+";
	eq FSubExp.XMLOperator() = "-";
	eq FMulExp.XMLOperator() = "*";
	eq FDivExp.XMLOperator() = "/";
	eq FPowExp.XMLOperator() = "^";
	eq FDotAddExp.XMLOperator() = ".+";
	eq FDotSubExp.XMLOperator() = ".-";
	eq FDotMulExp.XMLOperator() = ".*";
	eq FDotDivExp.XMLOperator() = "./";
	eq FDotPowExp.XMLOperator() = ".^";
	eq FStringAddExp.XMLOperator() = "+";
	eq FNegExp.XMLOperator() = "-";
	eq FNotExp.XMLOperator() = "not";
	
	// binary expressions, all binary expression should be function calls not operators
	public void FBinExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "call", indent, "builtin", XMLOperator());
		p.print(getLeft(), str, p.indent(indent));
		p.print(getRight(), str, p.indent(indent));
		closeTagXML(str, "call", indent);
	}
	
	public void FUnaryExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "call", indent, "builtin", XMLOperator());
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "call", indent);
	}
	
	// syn attributes for the base types
	syn String ASTNode.baseTypeXML() = null;
	eq FIntegerLitExp.baseTypeXML() = "integer";
    eq FRealLitExp.baseTypeXML()    = "real";
	eq FStringLitExp.baseTypeXML()  = "string";
	
	// literal expressions
	public void FRealLitExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, baseTypeXML(), indent, "value", Double.toString(getValue()));
	}
	
	public void FIntegerLitExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, baseTypeXML(), indent, "value", Integer.toString(getValue()));
	}
	
	public void FStringLitExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, baseTypeXML(), indent, "value", getString());
	}
	
	public void FBooleanLitExpFalse.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "false", indent);
	}
	
	public void FBooleanLitExpTrue.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "true", indent);
	}
	
	public void FEnumLitExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "local", indent, "name", (getEnum() + "." + getValue()));
	}
	
	// generate identifiers
	public void FIdUse.prettyPrintXML(Printer p, CodeStream str, String indent) {
		if (getFQName().isScalarized()) {
			closedTagXML(str, "local", indent, "name", name());
		} else {
			p.print(getFQName(), str, indent);
		}
	}
	
	public void FIdUseExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		p.print(getFIdUse(), str, indent);	
	}
	
	public void FQName.prettyPrintXML(Printer p, CodeStream str, String indent) {
		if (isScalarized()) {
			closedTagXML(str, "local", indent, "name", name());
		} else {
			p.print(asFQNameFull(), str, indent);
		}
	}
	
	public void FQNameString.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "local", indent, "name", getName());
	}
	
	public void FQNameFull.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		boolean firstPart = true;
		boolean isRef = false;
		for (int i=0; i < getNumFQNamePart(); i++) {
			FQNamePart part = getFQNamePart(i);
			if (firstPart) {
				if (part.hasFArraySubscripts() || getNumFQNamePart() > 1) {
					openTagXML(str, "reference", indent);
					isRef = true;
				}
				closedTagXML(str, "local", indentOneStep, "name", part.getName());
				firstPart = false;
			} else {
				closedTagXML(str, "member", indentOneStep, "name", part.getName());
			}
			if (part.hasFArraySubscripts()) {
				FArraySubscripts sub = part.getFArraySubscripts();
				if (sub.getNumFSubscript() > 0) {
					for (int j=0; j < sub.getNumFSubscript(); j++) {
						openTagXML(str, "subscripts", indentOneStep);
						p.print(sub.getFSubscript(j), str, indentTwoSteps);
						closeTagXML(str, "subscripts", indentOneStep);
					}
				}
			}
		}
		if (isRef) {
			closeTagXML(str, "reference", indent);
		}
	}
	
	public void FIntegerSubscript.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "integer", indent, "value", Integer.toString(getValue()));
	}
	
	public void FExpSubscript.prettyPrintXML(Printer p, CodeStream str, String indent) {
		p.print(getFExp(), str, indent);
	}
	
	//=========================================================================
	// Code generation for function declarations, general function calls and 
	// builtin functions
	//=========================================================================
	
	public void FFunctionDecl.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "classDefinition", indent, "name", name());
		openTagXML(str, "class", indentOneStep, "kind", "function");
		for (FFunctionVariable var : getFFunctionVariables()) {
			p.print(var, str, indentTwoSteps);
		}
		p.print(getFAlgorithm(), str, indentTwoSteps);
		closeTagXML(str, "class", indentOneStep);
		closeTagXML(str, "classDefinition", indent);
	}
	
	public void FFunctionCall.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "call", indent);
		openTagXML(str, "function", indentOneStep);
		closedTagXML(str, "local", indentTwoSteps, "name", name());
		closeTagXML(str, "function", indentOneStep);
		for (FExp arg : getArgs()) {
			p.print(arg, str, indentOneStep);
		}
		closeTagXML(str, "call", indent);
	}
	
	public void FBuiltInFunctionCall.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "call", indent, "builtin", builtInName());
		for (FExp exp : childFExps()) {
			p.print(exp, str, p.indent(indent));
		}
		closeTagXML(str, "call", indent);
	}
	
	public void FAssert.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "assert");
		p.print(getTest(), str, p.indent(indent));
		p.print(getMsg(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function der
	public void FDerExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "der");
		p.print(getFIdUse(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function pre
	public void FPreExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "pre");
		p.print(getFIdUse(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin variable time
	public void FTimeExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "builtin", indent, "name", "time");
	}
	
	// builtin function reinit
	public void FReinit.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "reinit");
		p.print(getVar(), str, p.indent(indent));
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function smooth
	public void FSmoothExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "smooth");
		p.print(getOrder(), str, p.indent(indent));
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function sample
	public void FSampleExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "sample");
		p.print(getOffset(), str, p.indent(indent));
		p.print(getInterval(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function change
	public void FChangeExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "change");
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function edge
	public void FEdgeExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "edge");
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function cardinality
	public void FCardinality.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "cardinality");
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function homotopy
	public void FHomotopyExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "homotopy");
		p.print(getActual(), str, p.indent(indent));
		p.print(getSimplified(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function finstream
	public void FInStream.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "instream");
		p.print(getFExp(), str, indent);
		closeTagXML(str, "operator", indent);
	}
	
	// builtin function fnoeventexp
	public void FNoEventExp.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "operator", indent, "name", "noevent");
		p.print(getFExp(), str, p.indent(indent));
		closeTagXML(str, "operator", indent);
	}
	
	syn String ASTNode.simulationFunction() = null;
	eq FTerminalExp.simulationFunction() = "terminal";
	eq FInitialExp.simulationFunction() = "initial";
	
	// builtin functions initial and terminal
	public void FSimulationStateBuiltIn.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "operator", indent, "name", simulationFunction());
	}
	
	syn boolean ASTNode.isAssert() = false;
	eq FAssert.isAssert() = true;
	
	//=========================================================================
	// Code generation for equations
	// TODO it seems like flattening is removing some equation constructs(for, connect and when) when i 
	// tried to test them, find test cases where its possible to test for and connect
	// if they are always flattened should we keep the generation for them?
	//=========================================================================
	public void FEquation.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		if (!getRight().isAssert()) {
			openTagXML(str, "equal", indent);
			p.print(getLeft(), str, indentOneStep);
			p.print(getRight(), str, indentOneStep);
			closeTagXML(str, "equal", indent);
		} else {
			p.print(getRight(), str, indent);
		}
	}
	
	public void FFunctionCallEquation.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		if (getNumLeft() > 0) {
			openTagXML(str, "equal", indent);
			if (getNumLeft() > 1) {
				openTagXML(str, "tuple", indentOneStep);
				for (FFunctionCallLeft left : getLefts()) {
					p.print(left, str, indentTwoSteps);
				}
				closeTagXML(str, "tuple", indentOneStep);
			} else {
				p.print(getLefts(), str, indentOneStep);
			}
		}
		p.print(getCall(), str, p.indent(indent));
		if (getNumLeft() > 0) {
			closeTagXML(str, "equal", indent);
		}
	}
	
	// lefthand side of special function call
	public void FFunctionCallLeft.prettyPrintXML(Printer p, CodeStream str, String indent) {
		if (getFExp() != null) {
			p.print(getFExp(), str, indent);
		} else {
			closedTagXML(str, "nothing", indent);
		}
	}
	
	public void FElseEquation.prettyPrintXML(Printer p, CodeStream str, String indent) {
		if (isElse()) {
			openTagXML(str, "else", indent);
		}
		for (FAbstractEquation equ : getFAbstractEquations()) {
			p.print(equ, str, p.indent(indent));
		}
		if (isElse()) {
			closeTagXML(str, "else", indent);
		}
	}
	
	public void FIfWhenEquation.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, ifWhenType(), indent);
		openTagXML(str, "cond", indentOneStep);
		p.print(getTest(), str, indentOneStep);
		closeTagXML(str, "cond", indentOneStep);
		openTagXML(str, "then", indentOneStep);
		for (FAbstractEquation equ : getFAbstractEquations()) {
			p.print(equ, str, indentTwoSteps);
		}
		closeTagXML(str, "then", indentOneStep);
		closeTagXML(str, ifWhenType(), indent);
	}
	
	public void FConnectClause.prettyPrintXML(Printer p, CodeStream str, String indent) {
		openTagXML(str, "connect", indent);
		p.print(getConnector1(), str, indent);
		p.print(getConnector2(), str, indent);
		closeTagXML(str, "connect", indent);
	}
	
	// instance tree identifier
	public void FIdUseInstAccess.prettyPrintXML(Printer p, CodeStream str, String indent) {
		p.print(getInstAccess(), str, indent);
	}
	
	public void InstAccess.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "local", indent, "name", name());
	}
	
	// loop in equation section
	// for loops are flattened in many cases? find test model where it is not flattened!
	public void FForClauseE.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		// generate xml for loop index
		openTagXML(str, "for", indent);
		openTagXML(str, "index", indentOneStep, "name", name());
		// generate local name of for index?
		closeTagXML(str, "index", indentOneStep);
		//getFForIndexList();
		openTagXML(str, "loop", indentOneStep);
		for (FAbstractEquation equ : getFAbstractEquations()) {
			p.print(equ, str, indentTwoSteps);
		}
		closeTagXML(str, "loop", indentOneStep);
		closeTagXML(str, "for", indent);
	}
	
	//=============================================================================
	// Code generation for algorithms
	//=============================================================================
	
	public void FAlgorithm.prettyPrintXML(Printer p, CodeStream str, String indent) {
		for (FStatement stmt : getFStatements()) {
			p.print(stmt, str, p.indent(indent));
		}
	}
	
	public void FAssignStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		if (!getRight().isAssert()) {
			openTagXML(str, "assign", indent);
			openTagXML(str, "to", indentOneStep);
			p.print(getLeft(), str, indentTwoSteps);
			closeTagXML(str, "to", indentOneStep);
			openTagXML(str, "from", indentOneStep);
			p.print(getRight(), str, indentTwoSteps);
			closeTagXML(str, "from", indentOneStep);
			closeTagXML(str, "assign", indent);
		} else {
			p.print(getRight(), str, indent);
		}
	}
	
	public void FBreakStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "break", indent);
	}
	
	public void FReturnStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		closedTagXML(str, "return", indent);
	}
	
	public void FWhenStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "when", indent);
		for (FIfWhenClause clause : getFIfWhenClauses()) {
			p.print(clause, str, indentTwoSteps);
		}
		closeTagXML(str, "when", indent);
	}
	
	public void FIfStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "if", indent);
		for (FIfWhenClause clause : getFIfWhenClauses()) {
			p.print(clause, str, indentOneStep);
		}
		if (getNumElseStmt() > 0) {
			openTagXML(str, "else", indentOneStep);
			for (FStatement stmt : getElseStmts()) {
				p.print(stmt, str, indentTwoSteps);
			}
			closeTagXML(str, "else", indentOneStep);
		}
		closeTagXML(str, "if", indent);
	}
	
	public void FIfWhenClause.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		openTagXML(str, "cond", indent);
		p.print(getTest(), str, indentOneStep);
		closeTagXML(str, "cond", indent);
		openTagXML(str, "then", indent);
		for (FStatement stmt : getFStatements()) {
			p.print(stmt, str, indentOneStep);
		}
		closeTagXML(str, "then", indent);
	}
	
	public void FForStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "for", indent);
		openTagXML(str, "index", indentOneStep);
		p.print(getIndex(), str, indentTwoSteps);
		closeTagXML(str, "index", indentOneStep);
		openTagXML(str, "loop", indentOneStep);
		p.print(getForStmts(), str, indentTwoSteps);
		closeTagXML(str, "loop", indentOneStep);
		closeTagXML(str, "for", indent);
	}
	
	public void FWhileStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		openTagXML(str, "while", indent);
		openTagXML(str, "cond", indentOneStep);
		p.print(getTest(), str, indentTwoSteps);
		closeTagXML(str, "cond", indentOneStep);
		openTagXML(str, "then", indentOneStep);
		for (FStatement fs : getWhileStmts()) {
			p.print(fs, str, indentTwoSteps);
		}
		closeTagXML(str, "then", indentOneStep);
		closeTagXML(str, "while", indent);
	}
	
	public void FFunctionCallStmt.prettyPrintXML(Printer p, CodeStream str, String indent) {
		String indentOneStep = p.indent(indent);
		String indentTwoSteps = p.indent(indentOneStep);
		String indentThreeSteps = p.indent(indentTwoSteps);
		if (!getCall().isAssert()) {
			openTagXML(str, "assign", indent);
			openTagXML(str, "to", indentOneStep);
			if (getNumLeft() > 1) {
				openTagXML(str, "tuple", indentTwoSteps);
				for (FFunctionCallLeft left : getLefts()) {
					p.print(left, str, indentThreeSteps);
				}
				closeTagXML(str, "tuple", indentTwoSteps);
			} else {
				for (FFunctionCallLeft left : getLefts()) {
					p.print(left, str, indentOneStep);
				}
			}
			closeTagXML(str, "to", indentOneStep);
			openTagXML(str, "from", indentOneStep);
			p.print(getCall(), str, indentTwoSteps);
			closeTagXML(str, "from", indentOneStep);
			closeTagXML(str, "assign", indent);
		} else {
			p.print(getCall(), str, indent);
		}
	}
}
