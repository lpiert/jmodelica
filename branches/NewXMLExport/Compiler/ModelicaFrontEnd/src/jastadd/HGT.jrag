/*
    Copyright (C) 2009-2014 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Collection;
import java.util.regex.Pattern;

aspect EquationName {
    
    public abstract class AttributeContributer {
        public static class EquationNameContributer extends AttributeContributer {
            
            public EquationNameContributer() {
                super("EquationNameContributer");
            }
            
            @Override
            protected void contribute(AbstractEquation src, FAbstractEquation dest) {
                AnnotationNode nameNode = src.annotation().vendorNode().forPath("name");
                if (!nameNode.exists())
                    return;
                Exp nameExp = nameNode.exp();
                if (nameExp == null)
                    nameNode.ast().error("Name annotation is incorrect");
                else if (!nameExp.isAccess() || !nameExp.asAccess().isNamed() || nameExp.asAccess().isArrayAccess())
                    nameExp.error("Illegal equation name \"%s\"", nameExp);
                else
                    dest.addFAttribute(new FIdDeclAttribute("name", nameExp.asAccess().asID()));
            }

            @Override
            protected void contribute(InstAssignable src, FVariable dest) {}

            @Override
            protected void contribute(InstBaseClassDecl src, FFunctionDecl dest) {}
        }
    }
    
    private static AttributeContributer ASTNode.EQUATION_NAME_CONTRIBUTER =
            addAttributeContributer(new AttributeContributer.EquationNameContributer());
    
}

aspect HGT {
    
    public abstract class AttributeContributer {
        public static class HGTContributer extends AttributeContributer {
            
            public HGTContributer() {
                super("HGTContributer");
            }
            
            @Override
            protected void contribute(AbstractEquation src, FAbstractEquation dest) {
                if (!src.root().options.getBooleanOption("hand_guided_tearing"))
                    return;
                AnnotationNode residualNode = src.annotation().vendorNode().forPath("ResidualEquation");
                if (!residualNode.exists())
                    return;
                
                FExp enabledFExp = null;
                boolean enabledEach = false;
                FExp iterationVariableFExp = null;
                AnnotationNode enabledAnnotation = residualNode.forPath("enabled");
                Exp enabledExp = enabledAnnotation.exp();
                if (enabledExp != null) {
                    enabledFExp = enabledExp.instantiate();
                    enabledEach = enabledAnnotation.isEachSet();
                }
                AnnotationNode iterationVariableAnnotation = residualNode.forPath("iterationVariable");
                Exp iterationVariableExp = iterationVariableAnnotation.exp();
                if (iterationVariableExp != null) {
                    if (!iterationVariableExp.isAccess()) {
                        iterationVariableExp.error("Expression \"%s\" is not a legal iteration variable reference", iterationVariableExp);
                        return;
                    } else {
                        iterationVariableFExp = iterationVariableExp.instantiate();
                    }
                }
                dest.setHGTResidual(Integer.MAX_VALUE, enabledFExp, enabledEach, iterationVariableFExp);
            }

            @Override
            protected void contribute(InstAssignable src, FVariable dest) {
                AnnotationNode iterationNode = src.annotation().vendorNode().forPath("IterationVariable");
                if (iterationNode.exists() && src.root().options.getBooleanOption("hand_guided_tearing")) {
                    AnnotationNode enabledNode = iterationNode.forPath("enabled");
                    if (!enabledNode.exists() || enabledNode.exp() == null) {
                        dest.addFAttribute(new FInternalAttribute(FAttribute.HGT, new FBooleanLitExpTrue(), FInternalAttribute.HGTVarLevel_COMPONENT, src.isArray()));
                    } else {
                        dest.addFAttribute(new FInternalAttribute(FAttribute.HGT, src.containingInstNode().dynamicFExp(enabledNode.exp().instantiate()).flatten(src.getFQNamePrefix()), 
                                FInternalAttribute.HGTVarLevel_COMPONENT, enabledNode.isEachSet()));
                    }
                }
            }

            @Override
            protected void contribute(InstBaseClassDecl src, FFunctionDecl dest) {}
        }
    }
    
    private static AttributeContributer ASTNode.HGT_CONTRIBUTER =
            addAttributeContributer(new AttributeContributer.HGTContributer());
    
    syn int FAbstractEquation.HGTLevel() {
        FAttribute residual = findAttribute("ResidualEquation");
        if (residual == null)
            return Integer.MAX_VALUE;
        else
            return residual.getLevel();
        
    }
    
    syn boolean FAbstractEquation.isHGTResidual() {
        FAttribute residual = findAttribute("ResidualEquation");
        if (residual == null)
            return false;
        FAttribute enabled = residual.findAttribute("enabled");
        return enabled == null || !enabled.hasValue() || enabled.getValue().ceval().booleanValue();
    }
    
    syn boolean FAbstractEquation.isHGTPairedResidual() {
        if (!isHGTResidual())
            return false;
        return getHGTIterationVariable() != null;
    }
    
    syn boolean FAbstractEquation.isHGTUnpairedResidual() {
        if (!isHGTResidual())
            return false;
        return getHGTIterationVariable() == null;
    }
    
    syn FVariable FAbstractEquation.getHGTIterationVariable() {
        if (!isHGTResidual())
            return null;
        FAttribute iterationVariable = findAttribute("ResidualEquation").findAttribute("iterationVariable");
        if (iterationVariable != null && iterationVariable.hasValue())
            return iterationVariable.getValue().asFIdUse().myFV().asFVariable();
        else
            return null;
    }
    
    public boolean Eq.markedAsResidualEquation() {
        return getEquation().isHGTResidual();
    }
    
    protected void FAbstractEquation.typeCheckHGTResidual(ErrorCheckType checkType) {
        FAttribute residualAttr = findAttribute("ResidualEquation");
        if (residualAttr == null)
            return;
        FAttribute enabledAttr = residualAttr.findAttribute("enabled");
        if (enabledAttr != null) {
            FExp enabledExp = enabledAttr.getValue();
            typeCheckHGTEnabled(checkType, enabledExp, type(), enabledAttr.hasFEach());
        }
        FAttribute iterVarAttr = residualAttr.findAttribute("iterationVariable");
        if (iterVarAttr != null && !iterVarAttr.getValue().asInstAccess().isUnknown()) {
            FExp var = iterVarAttr.getValue();
            FTypePrefixVariability variability = var.variability();
            if (!variability.continuousVariability())
                var.error("Iteration variable should have continuous variability, %s has %svariability", var, variability);
            Size size = size();
            Size varSize = var.size();
            if (!size.equals(varSize))
                var.error("Size of iteration variable %s is not the same size as the surrounding equation, size of variable %s, size of equation %s", var, varSize, size);
        }
    }
    
    protected static void ASTNode.typeCheckHGTEnabled(ErrorCheckType checkType, FExp enabledExp, FType parentType, boolean eachSet) {
        enabledExp.collectErrors(checkType);
        if (!enabledExp.type().isBoolean() && !enabledExp.type().isUnknown()) {
            enabledExp.error("The type of the enabled expression is not boolean");
        } else if (!eachSet && !parentType.dimensionCompatible(enabledExp.type())) { 
            if (enabledExp.ndims() == 0) {
                enabledExp.warning("Assuming 'each' for enabled expression");
            } else {
                enabledExp.error("Array size mismatch for the enabled attribute" +
                        ", size of component declaration is " + parentType.size() + 
                        " and size of expression is " + enabledExp.size());
            }
        } else if (eachSet) { 
            if (parentType.ndims() == 0) {
                enabledExp.error("The 'each' keyword cannot be applied to attributes of scalar components");
            } else if (enabledExp.ndims() > 0) {
                enabledExp.error("The enabled attribute is declared 'each' and the expression is not scalar");
            }
        }
    }
    
    public void FAbstractEquation.setHGTResidual(FVariable iterationVariable) {
        setHGTResidual(Integer.MAX_VALUE, iterationVariable.createUseExp());
    }
    
    public void FAbstractEquation.setHGTResidual(int level, FExp iterationVariable) {
        setHGTResidual(level, null, false, iterationVariable);
    }
    
    public void FAbstractEquation.setHGTResidual(int level, FExp enabledExp, boolean enabledEach, FExp iterationVariableExp) {
        FAttribute residualAttr = findAttribute("ResidualEquation");
        if (residualAttr == null) {
            residualAttr = new FInternalAttribute("ResidualEquation", level);
             addFAttribute(residualAttr);
        }
        FAttribute enabledAttr = residualAttr.findAttribute("enabled");
        if (enabledAttr == null) {
            if (enabledExp != null)
                residualAttr.addFAttribute(new FInternalAttribute("enabled", enabledExp, level, enabledEach));
        } else {
            if (enabledExp == null) // remove attribute?
                enabledAttr.setValueOpt(new Opt<FExp>());
            else
                enabledAttr.setValue(enabledExp);
        }
        FAttribute iterationVariableAttr = residualAttr.findAttribute("iterationVariable");
        if (iterationVariableAttr == null) {
            if (iterationVariableExp != null)
                residualAttr.addFAttribute(new FInternalAttribute("*", "iterationVariable", iterationVariableExp, level));
        } else {
            if (iterationVariableExp == null) // remove attribute?
                iterationVariableAttr.setValueOpt(new Opt<FExp>());
            else
                iterationVariableAttr.setValue(iterationVariableExp);
        }
    }
    
    public void FAbstractEquation.unsetHGT() {
        setHGTResidual(Integer.MAX_VALUE, new FBooleanLitExpFalse(), false, null);
    }
    
    private static Pattern Eq.tempPattern = Pattern.compile("(tmp|temp)_[0-9]+");
    
    public static Comparator<Eq> Eq.stringComparator() {
        return new Comparator<Eq>() {
            @Override
            public int compare(Eq eqn1, Eq eqn2) {
                String str1 = tempPattern.matcher(eqn1.getEquation().toString()).replaceAll("!TMP!");
                String str2 = tempPattern.matcher(eqn2.getEquation().toString()).replaceAll("!TMP!");
                return str1.compareTo(str2);
            }
        };
    }
    
    public static Comparator<Eq> Eq.stringMatchingComparator() {
        return new Comparator<Eq>() {
            @Override
            public int compare(Eq eqn1, Eq eqn2) {
                return eqn1.getMatching().toString().compareTo(eqn2.getMatching().toString());
            }
        };
    }
    
    public static Comparator<Var> Var.stringComparator() {
        return new Comparator<Var>() {
            @Override
            public int compare(Var var1, Var var2) {
                return var1.getVariable().toString().compareTo(var2.getVariable().toString());
            }
        };
    }
    
    private static void EquationBlockFactory.tearHandGuided(Collection<Eq> block, TornEquationBlock eb, boolean useTearing, int blockNumber) throws BLTException {
        boolean recalculateMatchings = false;
        Map<FVariable, Var> varsInBlock = new LinkedHashMap<FVariable, Var>();
        for (Eq e : block) {
            varsInBlock.put(e.getMatching().getVariable(), e.getMatching());
        }
        Iterator<Eq> it = block.iterator();
        java.util.List<Eq> unmatchedHGTEquations = new ArrayList<Eq>();
        java.util.List<Var> unmatchedHGTVariables = new ArrayList<Var>();
        for (Eq eqn : block) {
            if (eqn.getEquation().isHGTUnpairedResidual())
                unmatchedHGTEquations.add(eqn);
        }
        for (Var var : varsInBlock.values()) {
            if (var.getVariable().isHGTVarComponent())
                unmatchedHGTVariables.add(var);
        }
        if (unmatchedHGTEquations.size() == unmatchedHGTVariables.size()) {
            Collections.sort(unmatchedHGTEquations, Eq.stringComparator());
            Collections.sort(unmatchedHGTVariables, Var.stringComparator());
            Iterator<Eq> eqnIt = unmatchedHGTEquations.iterator();
            Iterator<Var> varIt = unmatchedHGTVariables.iterator();
            while (eqnIt.hasNext() && varIt.hasNext()) {
                Eq eqn = eqnIt.next();
                Var var = varIt.next();
                eqn.getEquation().setHGTResidual(var.getVariable());
                var.getVariable().setHGTAsPair();
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to apply hand-guided tearing selections on block " + blockNumber + ". The number of unmatched hand guided equations and variables are not equal.\n");
            sb.append("  Unmatched hand guided equations(" + unmatchedHGTEquations.size() + "):\n");
            for (Eq eqn : unmatchedHGTEquations)
                sb.append("    " + eqn.getEquation() + "\n");
            sb.append("\n");
            sb.append("  Unmatched hand guided variables(" + unmatchedHGTVariables.size() + "):\n");
            for (Var var : unmatchedHGTVariables)
                sb.append("    " + var.getName() + "\n");
            throw new BLTException(sb.toString());
        }
        java.util.List<Eq> HGTEquations = new ArrayList<Eq>();
        while (it.hasNext()) {
            Eq e = it.next();
            FVariable fIterVar = e.getEquation().getHGTIterationVariable();
            if (fIterVar != null) {
                Var iterVar = varsInBlock.get(fIterVar);
                if (iterVar == null) {
                    e.getEquation().warning("Can not use hand guided tearing pair, equation and variable resides in different blocks. Variable: " + fIterVar.name() + ". Equation: " + e.getEquation());
                    continue;
                }
                it.remove();
                HGTEquations.add(e);
                recalculateMatchings = true;
            }
        }
        Collections.sort(HGTEquations, Eq.stringMatchingComparator());
        for (Eq hgtEqn : HGTEquations) {
            Var iterVar = varsInBlock.remove(hgtEqn.getEquation().getHGTIterationVariable());
            ASTNode.log.info("Hand guided tearing pair, equation: %s, tearing with variable: %s", hgtEqn.getEquation(), iterVar.getVariable().name());
            eb.addUnsolvedBlock(EquationBlockFactory.createSimpleEquationBlock(hgtEqn, iterVar));
        }
        if (recalculateMatchings) {
            BiPGraph newGraph = new BiPGraph(block, varsInBlock.values());
            newGraph.maximumMatching(true);
            Collection<Eq> unmatchedEquations = newGraph.getUnmatchedEquations();
            Collection<Var> unmatchedVariables = newGraph.getUnmatchedVariables();
            if (unmatchedEquations.size() > 0 || unmatchedVariables.size() > 0) {
                ASTNode.log.info("Unmatched equations and variables in block after hand guided, adding them as tearing pairs!");
                Iterator<Eq> eIt = unmatchedEquations.iterator();
                Iterator<Var> vIt = unmatchedVariables.iterator();
                while (eIt.hasNext() && vIt.hasNext()) {
                    Eq e = eIt.next();
                    Var v = vIt.next();
                    ASTNode.log.info("Unmatched tearing pair, equation: %s, tearing with variable: %s", e.getEquation(), v.getVariable().name());
                    newGraph.removeEquation(e);
                    newGraph.removeVariable(v);
                    eb.addUnsolvedBlock(EquationBlockFactory.createSimpleEquationBlock(e, v));
                }
                if (eIt.hasNext() || vIt.hasNext()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Hand guided tearing selections in block " + blockNumber + " results in a structurally singular block.\n");
                    if (eIt.hasNext()) {
                        sb.append("  The follwowing equation(s) could not be matched to any variable:\n");
                            while (eIt.hasNext())
                                sb.append("    " + eIt.next().getEquation());
                    }
                    if (vIt.hasNext()) {
                        sb.append("  The following varible(s) could not be matched to any equation:\n");
                        while (vIt.hasNext())
                            sb.append("    " + vIt.next().getVariable().name());
                    }
                    throw new BLTException(sb.toString());
                }
            }
            Collection<Collection<Eq>> innerBLT = newGraph.tarjan(false);
            boolean allInnerAreSolved = true;
            Collection<SimpleEquationBlock> solvedBlocks = new ArrayList<SimpleEquationBlock>();
            for (Collection<Eq> innerBlock : innerBLT) {
                SimpleEquationBlock seb = EquationBlockFactory.createSimpleEquationBlock(innerBlock, true, true);
                if (seb != null && seb.isSolvable()) {
                    solvedBlocks.add(seb);
                } else {
                    allInnerAreSolved = false;
                    break;
                }
            }
            if (allInnerAreSolved) {
                ASTNode.log.info("Remaining system is solvable.");
                for (SimpleEquationBlock seb : solvedBlocks)
                    eb.addSolvedBlock(seb);
                return;
            }
            block = new ArrayList<Eq>();
            block.addAll(newGraph.getEquations());
            ASTNode.log.info("Additional tearing is needed, continuing with automatic tearing.");
        }
        if (!useTearing)
            throw new BLTException("Hand guided tearing selections in block " + blockNumber + " does not result in a torn system. Consider adding additional selections of hand guided equations and variables, or enable automatic tearing.");
        else
            tear(block, eb);
    }
    
}

aspect ResidualPairs{
    
    public static final int FInternalAttribute.HGTVarLevel_COMPONENT = 1;
    public static final int FInternalAttribute.HGTVarLevel_PAIR = 2;
    
    public static final String FAttribute.HGT         = "hgt()";
    
    syn CValue FVariable.HGTVarCValue() {
        FAttribute attr = findMatching(getFAttributes(), FAttribute.HGT);
        return attr == null ? null : attr.getValue().ceval();
    }
    
    syn boolean FVariable.isHGTVar() {
        FAttribute attr = findMatching(getFAttributes(), FAttribute.HGT);
        return attr != null && attr.getValue().ceval().booleanValue();
    }
    
    syn boolean FVariable.isHGTVarPair() {
        FAttribute attr = findMatching(getFAttributes(), FAttribute.HGT);
        return attr != null && attr.getLevel() == FInternalAttribute.HGTVarLevel_PAIR &&
                attr.getValue().ceval().booleanValue();
    }
    
    syn boolean FVariable.isHGTVarComponent() {
        FAttribute attr = findMatching(getFAttributes(), FAttribute.HGT);
        return attr != null && attr.getLevel() == FInternalAttribute.HGTVarLevel_COMPONENT &&
                attr.getValue().ceval().booleanValue();
    }
    
    public void FAbstractVariable.setHGTAsPair() {
        throw new UnsupportedOperationException();
    }
    public void FVariable.setHGTAsPair() {
        setAttribute(FAttribute.HGT, "Boolean", new FBooleanLitExpTrue(), FInternalAttribute.HGTVarLevel_PAIR);
    }
    
    public void FVariable.unsetHGT() {
        setAttribute(FAttribute.HGT, "Boolean", new FBooleanLitExpFalse());
    }
    
    public class FClass {
        public class propagateResidualPairs extends Transformation {
            public void perform() {
                for (FAbstractEquation eqn : getFAbstractEquations()) {
                    if (eqn.isHGTPairedResidual())
                        eqn.getHGTIterationVariable().setHGTAsPair();
                }
                
                for (FResidualPair pair : getFResidualPairs()) {
                    pair.propagate();
                }
            }
        }
    }
    
    protected void FResidualPair.propagate() {
        FAbstractEquation fae = getResidualEquation().myFEquation();
        if (fae == null)
            throw new UnsupportedOperationException("Internal HGT Error, unable to find equation declaration for tearing pair; equation: " + getResidualEquation().name() + ", variable: " + getIterationVariable().name() + ", level: " + getLevel() + "!");
        FAbstractVariable fv = getIterationVariable().myFV();
        if (fv == null)
            throw new UnsupportedOperationException("Internal HGT Error, unable to find variable declaration for tearing pair; equation: " + getResidualEquation().name() + ", variable: " + getIterationVariable().name() + ", level: " + getLevel() + "!");
        int otherLevel = fae.HGTLevel();
        if (otherLevel == getLevel())
            throw new UnsupportedOperationException("Internal HGT Error, found equation with two (or more) residual pairs declared at the same level. Should not be possible!\nCurrent tearing pair; equation: " + getResidualEquation().name() + ", variable: " + getIterationVariable().name() + ", level: " + getLevel() + "!\nTearing pair; equation: " + getResidualEquation().name() + ", variable: " + getIterationVariable().name() + ", level: " + getLevel() + "!");
        if (otherLevel > getLevel()) {
            fae.setHGTResidual(getLevel(), getIterationVariable().myFV().createUseExp());
            getIterationVariable().myFV().setHGTAsPair();
        }
    }
    
    /**
     * \brief Find functions, record and enumeration declarations that need to be flattened.
     */
    protected void ASTNode.createAndFlattenResidualPairs(FClass fc) {
        for (ASTNode n : this)
            n.createAndFlattenResidualPairs(fc);
    }
    
    @Override
    protected void InstNode.createAndFlattenResidualPairs(FClass fc) {
        if (!root().options.getBooleanOption("hand_guided_tearing"))
            return;
        getInstComponentDecls().createAndFlattenResidualPairs(fc);
        getInstExtendsList().createAndFlattenResidualPairs(fc);
        FQName prefix = getFQName();
        for (AnnotationNode pair : classAnnotation().vendorNode().forPath("tearingPairs")) {
            if (!pair.name().equals("Pair"))
                continue;
            Exp enabledExp = pair.forPath("enabled").exp();
            if (enabledExp != null && !dynamicFExp(enabledExp.instantiate()).ceval().booleanValue())
                continue;
            
            Exp iterationVariableExp = pair.forPath("iterationVariable").exp();
            InstAccess via = iterationVariableExp.asAccess().instantiate();
            FQName iterationVariableName = dynamicFExp(new FInstAccessExp(via)).asInstAccess().flatten(prefix);
            
            Exp residualEquationExp = pair.forPath("residualEquation").exp();
            InstAccess eia = residualEquationExp.asAccess().instantiate().convertToEquationAccess();
            FQName residualEquationName = dynamicFExp(new FInstAccessExp(eia)).asInstAccess().flatten(prefix);
            
            fc.addFResidualPair(new FResidualPair(new FIdUse(residualEquationName), 
                    new FIdUse(iterationVariableName), prefix.numDots() + prefix.numParts()));
        }
    }
    
    @Override
    protected void InstComponentDecl.createAndFlattenResidualPairs(FClass fc) {
        if (useInFlattening())
            super.createAndFlattenResidualPairs(fc);
    }
    
    public void InstNode.typeCheckSystemResiduals(ErrorCheckType checkType) {
        if (!root().options.getBooleanOption("hand_guided_tearing"))
            return;
        for (AnnotationNode pair : classAnnotation().vendorNode().forPath("tearingPairs")) {
            if (!pair.name().equals("Pair"))
                continue;
            Exp enabledExp = pair.forPath("enabled").exp();
            if (enabledExp != null) {
                FExp enabledFExp = dynamicFExp(enabledExp.instantiate());
                enabledFExp.collectErrors(checkType);
                try {
                    enabledFExp.ceval().booleanValue();
                } catch (ConstantEvaluationException e) {
                    enabledExp.error("Cannot evaluate boolean enabled expression: " + enabledExp);
                }
            }
            Exp iterationVariableExp = pair.forPath("iterationVariable").exp();
            InstAccess iterationVariable = null;
            if (iterationVariableExp == null) {
                pair.ast().error("Iteration variable definition is missing from tearing pair.");
            } else if (!iterationVariableExp.isAccess()) {
                iterationVariableExp.error("Expression \"%s\" is not a legal iteration variable reference", iterationVariableExp);
            } else {
                InstAccess ia = iterationVariableExp.asAccess().instantiate();
                iterationVariable = dynamicFExp(new FInstAccessExp(ia)).asInstAccess();
                iterationVariable.collectErrors(checkType);
                if (iterationVariable.isUnknown()) {
                    iterationVariable = null;
                } else {
                    FTypePrefixVariability variability = iterationVariable.myInstComponentDecl().variability();
                    if (!variability.continuousVariability()) {
                        iterationVariable.error("Iteration variable should have continuous variability, %s has %svariability", iterationVariable.qualifiedName(), variability);
                        iterationVariable = null;
                    }
                }
            }
            Exp residualEquationExp = pair.forPath("residualEquation").exp();
            InstAccess residualEquation = null;
            if (residualEquationExp == null) {
                pair.ast().error("Residual equation definition is missing from tearing pair.");
            } else if (!residualEquationExp.isAccess()) {
                residualEquationExp.error("Expression \"%s\" is not a legal residual equation reference", residualEquationExp);
            } else {
                InstAccess ia = residualEquationExp.asAccess().instantiate().convertToEquationAccess();
                residualEquation = dynamicFExp(new FInstAccessExp(ia)).asInstAccess();
                residualEquation.collectErrors(checkType);
                if (residualEquation.isUnknown())
                    residualEquation = null;
            }
            if (iterationVariable != null && residualEquation != null) {
                Size eqnSize = residualEquation.myEquation().totalSize();
                Size varSize = iterationVariable.myInstComponentDecl().size();
                if (!eqnSize.equals(varSize))
                    pair.ast().error("Size of the iteration variable is not the same size as the size of the residual equation, size of variable %s, size of equation %s", varSize, eqnSize);
            }
        }
    }

}

