<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Correctness Check for Both Simple and Fast Representations</title>
<meta name="description" id="description" content="Correctness Check for Both Simple and Fast Representations"/>
<meta name="keywords" id="keywords" content=" "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_ipopt_ode_check.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="ipopt_ode_run.hpp.xml" target="_top">Prev</a>
</td><td><a href="ipopt_ode_speed.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Example</option>
<option>General</option>
<option>cppad_ipopt_nlp</option>
<option>cppad_ipopt_ode</option>
<option>ipopt_ode_check.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>General-&gt;</option>
<option>ad_fun.cpp</option>
<option>ad_in_c.cpp</option>
<option>HesMinorDet.cpp</option>
<option>HesLuDet.cpp</option>
<option>cppad_ipopt_nlp</option>
<option>Interface2C.cpp</option>
<option>JacMinorDet.cpp</option>
<option>JacLuDet.cpp</option>
<option>mul_level</option>
<option>OdeStiff.cpp</option>
<option>ode_taylor.cpp</option>
<option>ode_taylor_adolc.cpp</option>
<option>StackMachine.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>cppad_ipopt_nlp-&gt;</option>
<option>cppad_ipopt_windows</option>
<option>ipopt_get_started.cpp</option>
<option>cppad_ipopt_ode</option>
<option>ipopt_ode_speed.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>cppad_ipopt_ode-&gt;</option>
<option>ipopt_ode_problem</option>
<option>ipopt_ode_simple</option>
<option>ipopt_ode_fast</option>
<option>ipopt_ode_run.hpp</option>
<option>ipopt_ode_check.cpp</option>
</select>
</td>
<td>ipopt_ode_check.cpp</td>
<td>Headings</td>
</tr></table><br/>


<center><b><big><big>Correctness Check for Both Simple and Fast Representations</big></big></b></center>
<code><font color="blue"><pre style='display:inline'> 
# include &quot;ode_run.hpp&quot;

bool ode_check(const SizeVector&amp; N, const NumberVector&amp; x)
{	bool ok = true;
	size_t i, j;

	// number of components of x corresponding to values for y
	size_t ny_inx = x.size() - Na;

	// compute the partial sums of the number of grid points
	// and the maximum step size for the trapezoidal approximation
	SizeVector S(Nz+1);
	S[0] = 0.;
	double max_step = 0.;
	for(i = 1; i &lt;= Nz; i++)
	{	S[i] = S[i-1] + N[i];
		max_step = std::max(max_step, (s[i] - s[i-1]) / N[i] );
	}

	// split out return values
	NumberVector a(Na), y_0(Ny), y_1(Ny), y_2(Ny);
	for(j = 0; j &lt; Na; j++)
		a[j] = x[ny_inx+j];
	for(j = 0; j &lt; Ny; j++)
	{	y_0[j] = x[j];
		y_1[j] = x[Ny + j];
		y_2[j] = x[2 * Ny + j];
	} 

	// Check some of the optimal a value
	Number rel_tol = max_step * max_step;
	Number abs_tol = rel_tol;
	Number check_a[] = {a0, a1, a2}; // see the y_one function
	for(j = 0; j &lt; Na; j++)
	{
		ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>( 
			check_a[j], a[j], rel_tol, abs_tol
		);
	}

	// check accuarcy of constraint equations
	rel_tol = 1e-9;
	abs_tol = 1e-9;

	// check the initial value constraint
	NumberVector F = eval_F(a);
	for(j = 0; j &lt; Ny; j++)
		ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>(F[j], y_0[j], rel_tol, abs_tol);

	// check the first trapezoidal equation
	NumberVector G_0 = eval_G(y_0, a);
	NumberVector G_1 = eval_G(y_1, a);
	Number dt = (s[1] - s[0]) / Number(N[1]);
	Number check;
	for(j = 0; j &lt; Ny; j++)
	{	check = y_1[j] - y_0[j] - (G_1[j]+G_0[j])*dt/2;
		ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>( check, 0., rel_tol, abs_tol);
	}
	//
	// check the second trapezoidal equation
	NumberVector G_2 = eval_G(y_2, a);
	if( N[1] == 1 )
		dt = (s[2] - s[1]) / Number(N[2]);
	for(j = 0; j &lt; Ny; j++)
	{	check = y_2[j] - y_1[j] - (G_2[j]+G_1[j])*dt/2;
		ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>( check, 0., rel_tol, abs_tol);
	}
	//
	// check the objective function (specialized to this case)
	check = 0.;
	NumberVector y_i(Ny);
	for(size_t k = 0; k &lt; Nz; k++)
	{	for(j = 0; j &lt; Ny; j++)
			y_i[j] =  x[S[k+1] * Ny + j];
		check += eval_H&lt;Number&gt;(k + 1, y_i, a);
	}
	Number obj_value = 0.; // optimal object (no noise in simulation)
	ok &amp;= CppAD::<a href="nearequal.xml" target="_top">NearEqual</a>(check, obj_value, rel_tol, abs_tol);

	return ok;
}</pre>
</font></code>


<hr/>Input File: cppad_ipopt/example/ode_check.cpp

</body>
</html>
