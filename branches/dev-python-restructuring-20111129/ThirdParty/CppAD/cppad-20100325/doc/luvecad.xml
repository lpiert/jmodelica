<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Lu Factor and Solve with Recorded Pivoting</title>
<meta name="description" id="description" content="Lu Factor and Solve with Recorded Pivoting"/>
<meta name="keywords" id="keywords" content=" Luvecad Lu linear equation determinant solve "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_luvecad_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="speed_example.cpp.xml" target="_top">Prev</a>
</td><td><a href="luvecadok.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Example</option>
<option>ExampleUtility</option>
<option>LuVecAD</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Example-&gt;</option>
<option>General</option>
<option>ExampleUtility</option>
<option>ListAllExamples</option>
<option>test_vector</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>ExampleUtility-&gt;</option>
<option>Example.cpp</option>
<option>speed_example.cpp</option>
<option>LuVecAD</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>LuVecAD-&gt;</option>
<option>LuVecADOk.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>Storage Convention</option>
<option>n</option>
<option>m</option>
<option>Matrix</option>
<option>Rhs</option>
<option>Result</option>
<option>logdet</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>











<center><b><big><big>Lu Factor and Solve with Recorded Pivoting</big></big></b></center>
<code><span style='white-space: nowrap'><br/>
</span></code><b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>int&#xA0;LuVecAD(<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>,<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;size_t&#xA0;</span></font></code><i><span style='white-space: nowrap'>m</span></i><code><font color="blue"><span style='white-space: nowrap'>,<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>double</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>Matrix</span></i><code><font color="blue"><span style='white-space: nowrap'>,<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>double</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>Rhs</span></i><code><font color="blue"><span style='white-space: nowrap'>,<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>double</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>Result</span></i><code><font color="blue"><span style='white-space: nowrap'>,<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>double</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>logdet</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Solves the linear equation

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>Matrix</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>Result</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>Rhs</mi>
</mrow></math>

where <i>Matrix</i> is an 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 matrix,
<i>Rhs</i> is an 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix, and
<i>Result</i> is an 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>The routine <a href="lusolve.xml" target="_top"><span style='white-space: nowrap'>LuSolve</span></a>
 uses an arbitrary vector type,
instead of <a href="vecad.xml" target="_top"><span style='white-space: nowrap'>VecAD</span></a>
,
to hold its elements.
The pivoting operations for a <code><font color="blue">ADFun</font></code> object
corresponding to an <code><font color="blue">LuVecAD</font></code> solution
will change to be optimal for the matrix being factored.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>It is often the case that
<code><font color="blue">LuSolve</font></code> is faster than <code><font color="blue">LuVecAD</font></code> when <code><font color="blue">LuSolve</font></code>
uses a simple vector class with 
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;double</span></a>
,
but the corresponding <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 objects have a fixed
set of pivoting operations.

<br/>
<br/>
<b><big><a name="Storage Convention" id="Storage Convention">Storage Convention</a></big></b>
<br/>
The matrices stored in row major order.
To be specific, if 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 contains the vector storage for an

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
</mrow></math>

 is between zero and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

,
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
</mrow></math>

 is between zero and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>A</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>A</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
</mrow></math>

(The length of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 must be equal to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

.)

<br/>
<br/>
<b><big><a name="n" id="n">n</a></big></b>
<br/>
is the number of rows in 
<i>Matrix</i>,
<i>Rhs</i>,
and <i>Result</i>.

<br/>
<br/>
<b><big><a name="m" id="m">m</a></big></b>
<br/>
is the number of columns in 
<i>Rhs</i>
and <i>Result</i>.
It is ok for <i>m</i> to be zero which is reasonable when
you are only interested in the determinant of <i>Matrix</i>.


<br/>
<br/>
<b><big><a name="Matrix" id="Matrix">Matrix</a></big></b>
<br/>
On input, this is an

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 matrix containing the variable coefficients for 
the equation we wish to solve.
On output, the elements of <i>Matrix</i> have been overwritten
and are not specified.

<br/>
<br/>
<b><big><a name="Rhs" id="Rhs">Rhs</a></big></b>
<br/>
On input, this is an

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix containing the right hand side
for the equation we wish to solve.
On output, the elements of <i>Rhs</i> have been overwritten
and are not specified.
If <i>m</i> is zero, <i>Rhs</i> is not used.

<br/>
<br/>
<b><big><a name="Result" id="Result">Result</a></big></b>
<br/>
On input, this is an

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix and the value of its elements do not matter.
On output, the elements of <i>Rhs</i> contain the solution
of the equation we wish to solve
(unless the value returned by <code><font color="blue">LuVecAD</font></code> is equal to zero).
If <i>m</i> is zero, <i>Result</i> is not used.

<br/>
<br/>
<b><big><a name="logdet" id="logdet">logdet</a></big></b>
<br/>
On input, the value of <i>logdet</i> does not matter.
On output, it has been set to the 
log of the determinant of <i>Matrix</i> (but not quite).
To be more specific,
if <i>signdet</i> is the value returned by <code><font color="blue">LuVecAD</font></code>,
the determinant of <i>Matrix</i> is given by the formula

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>det</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>signdet</mi>
<mi>exp</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>logdet</mi>
<mo stretchy="false">)</mo>
</mrow></math>

This enables <code><font color="blue">LuVecAD</font></code> to use logs of absolute values.


<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The file
<a href="luvecadok.cpp.xml" target="_top"><span style='white-space: nowrap'>LuVecADOk.cpp</span></a>

contains an example and test of <code><font color="blue">LuVecAD</font></code>.
It returns true if it succeeds and false otherwise.



<hr/>Input File: example/lu_vec_ad.cpp

</body>
</html>
