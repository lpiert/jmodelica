<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Reverse Mode General Case: Example and Test</title>
<meta name="description" id="description" content="Reverse Mode General Case: Example and Test"/>
<meta name="keywords" id="keywords" content=" general reverse example test composition "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_reverse_any.cpp_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="reverse_three.cpp.xml" target="_top">Prev</a>
</td><td><a href="checkpoint.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>FunEval</option>
<option>Reverse</option>
<option>reverse_any</option>
<option>reverse_any.cpp</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>FunEval-&gt;</option>
<option>Forward</option>
<option>Reverse</option>
<option>Sparse</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Reverse-&gt;</option>
<option>reverse_one</option>
<option>reverse_two</option>
<option>reverse_any</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>reverse_any-&gt;</option>
<option>reverse_three.cpp</option>
<option>reverse_any.cpp</option>
<option>checkpoint.cpp</option>
</select>
</td>
<td>reverse_any.cpp</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Purpose</option>
<option>Processing Steps</option>
</select>
</td>
</tr></table><br/>



<center><b><big><big>Reverse Mode General Case: Example and Test</big></big></b></center>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
Break a derivative computation into pieces and only store values at the 
interface of the pieces.
In actual applications, there may be many functions, but 
for this example there are only two.
The functions 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
</mrow></math>

 
and

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
</mrow></math>

 
defined by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">(</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="center" >
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
</mtd></mtr><mtr><mtd columnalign="center" >
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
</mtd></mtr></mtable>
</mrow><mo stretchy="true">)</mo></mrow>
<mspace width='.3em'/>
<mo stretchy="false">,</mo>
<mspace width='.3em'/>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">(</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="center" >
<msub><mi mathvariant='italic'>y</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>y</mi>
<mn>1</mn>
</msub>
</mtd></mtr><mtr><mtd columnalign="center" >
<msub><mi mathvariant='italic'>y</mi>
<mn>1</mn>
</msub>
<msub><mi mathvariant='italic'>y</mi>
<mn>0</mn>
</msub>
</mtd></mtr></mtable>
</mrow><mo stretchy="true">)</mo></mrow>
</mrow></math>

Another difference is that in actual applications,
the memory corresponding to function objects not currently being used
is sometimes returned to the system (see <a href="checkpoint.cpp.xml" target="_top"><span style='white-space: nowrap'>checkpoint.cpp</span></a>
).

<br/>
<br/>
<b><big><a name="Processing Steps" id="Processing Steps">Processing Steps</a></big></b>
<br/>
We apply reverse mode to compute the derivative of

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">:</mo>
<msup><mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
<mn>2</mn>
</msup>
<mo stretchy="false">&#x02192;</mo>
<mrow><mstyle mathvariant='bold'><mi mathvariant='bold'>R</mi>
</mstyle></mrow>
</mrow></math>


is defined by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>H</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>G</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='italic'>G</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">-</mo>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">(</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">)</mo>
<mo stretchy="false">-</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>1</mn>
</msub>
<mo stretchy="false">+</mo>
<msub><mi mathvariant='italic'>x</mi>
<mn>0</mn>
</msub>
</mtd></mtr></mtable>
</mrow></math>

Given the zero and first order Taylor coefficients 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

,
we use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>

, 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>


for the corresponding functions; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>O</mi>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>t</mi>
<mn>2</mn>
</msup>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<mi mathvariant='italic'>Z</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">{</mo>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">]</mo>
<mo stretchy="false">}</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>z</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>z</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>O</mi>
<mo stretchy="false">(</mo>
<msup><mi mathvariant='italic'>t</mi>
<mn>2</mn>
</msup>
<mo stretchy="false">)</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msubsup><mi mathvariant='italic'>z</mi>
<mn>0</mn>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msubsup>
<mo stretchy="false">+</mo>
<msubsup><mi mathvariant='italic'>z</mi>
<mn>1</mn>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msubsup>
</mtd></mtr><mtr><mtd columnalign="right" >
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msubsup><mi mathvariant='italic'>z</mi>
<mn>0</mn>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msubsup>
<mo stretchy="false">+</mo>
<msubsup><mi mathvariant='italic'>z</mi>
<mn>1</mn>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msubsup>
</mtd></mtr></mtable>
</mrow></math>

Here are the processing steps:
<ol type="1"><li>
Use forward mode on 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 to compute 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 
</li><li>

Use forward mode on 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 to compute 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>z</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>z</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 
</li><li>

Use reverse mode on 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>G</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>y</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 to compute the derivative of 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 with respect to

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

.
</li><li>

Use reverse mode on 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 to compute the derivative of

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 with respect to

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

.
</li></ol>

This uses the following relations for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mn>1</mn>
</mrow></math>

:

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mtable rowalign="center" ><mtr><mtd columnalign="right" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>y</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">+</mo>
</mtd><mtd columnalign="left" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>y</mi>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd><mtd columnalign="center" >
<mo stretchy="false">=</mo>
</mtd><mtd columnalign="left" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>y</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd></mtr><mtr><mtd columnalign="right" >
</mtd><mtd columnalign="center" >
<mo stretchy="false">+</mo>
</mtd><mtd columnalign="left" >
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>y</mi>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>h</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
<msup><mi mathvariant='italic'>y</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">[</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">,</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">]</mo>
</mtd></mtr></mtable>
</mrow></math>

where 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>x</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msub>
</mrow></math>

 denotes the partial with respect
to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
</mrow></math>

.

<code><font color="blue">
<pre style='display:inline'> 

# include &lt;cppad/cppad.hpp&gt;

namespace {
	template &lt;class Vector&gt;
	Vector F(const Vector&amp; x)
	{	Vector y(2);
		y[0] = x[0] * x[1];
		y[1] = x[1] - x[0];
		return y;
	}
	template &lt;class Vector&gt;
	Vector G(const Vector&amp; y)
	{	Vector z(2);
		z[0] = y[0] - y[1];
		z[1] = y[1] * y[0];
		return z;
	}
}

bool reverse_any(void)
{	bool ok = true;
     double eps = 10. * std::numeric_limits&lt;double&gt;::epsilon();

	using CppAD::AD;
	using CppAD::NearEqual;
	CppAD::<a href="funconstruct.xml" target="_top">ADFun</a>&lt;double&gt; f, g;

	// Record the function F(x)
	size_t n    = 2;
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; X(n), Y(n);
	X[0] = X[1] = 0.;
	CppAD::<a href="independent.xml" target="_top">Independent</a>(X);
	Y = F(X);
	f.Dependent(X, Y);

	// Record the function G(x)
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt; <a href="ad.xml" target="_top">AD</a>&lt;double&gt; &gt; Z(n);
	Y[0] = Y[1] = 0.;
	CppAD::<a href="independent.xml" target="_top">Independent</a>(Y);
	Z = G(Y);
	g.Dependent(Y, Z);

	// argument and function values
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;double&gt; x0(n), y0(n), z0(n);
	x0[0] = 1.;
	x0[1] = 2.;
	y0    = f.<a href="forward.xml" target="_top">Forward</a>(0, x0);
	z0    = g.<a href="forward.xml" target="_top">Forward</a>(0, y0);

	// check function value
	double check = x0[0] * x0[1] * (1. - x0[0] + x0[1]) - x0[1] + x0[0];
	double h0    = z0[0] + z0[1];
	ok          &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(h0, check, eps, eps);

	// first order Taylor coefficients
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;double&gt; x1(n), y1(n), z1(n);
	x1[0] = 3.;
	x1[1] = 4.;
	y1    = f.<a href="forward.xml" target="_top">Forward</a>(1, x1);
	z1    = g.<a href="forward.xml" target="_top">Forward</a>(1, y1);

	// check first order Taylor coefficients
	check     = x0[0] * x0[1] * (- x1[0] + x1[1]) - x1[1] + x1[0];
	check    += x1[0] * x0[1] * (1. - x0[0] + x0[1]);
	check    += x0[0] * x1[1] * (1. - x0[0] + x0[1]);
	double h1 = z1[0] + z1[1];
	ok       &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(h1, check, eps, eps);

	// ----------------------------------------------------------------
	// dw^0 (y) = \partial_y^0 h^0 (y)
	// dw^1 (y) = \partial_y^1 h^0 (y)
	size_t p = 2;
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;double&gt; w(n*p), dw(n*p);
	w[0*p+0] = 1.; // coefficient for z^0_0
	w[1*p+0] = 1.; // coefficient for z^0_1
	w[0*p+1] = 0.; // coefficient for z^1_0
	w[1*p+1] = 0.; // coefficient for z^1_1 
	dw       = g.<a href="reverse.xml" target="_top">Reverse</a>(p, w);

	// dv^0 = dw^0 * \partial_x^0 y^0 (x) + dw^1 * \partial_x^0 y^1 (x)  
	// dv^1 = dw^0 * \partial_x^1 y^0 (x) + dw^1 * \partial_x^1 y^1 (x)  
	<a href="test_vector.xml" target="_top">CPPAD_TEST_VECTOR</a>&lt;double&gt; dv(n*p);
	dv   = f.<a href="reverse.xml" target="_top">Reverse</a>(p, dw); 

	// check partial of h^0 w.r.t x^0_0
	check  = x0[1] * (1. - x0[0] + x0[1]) + 1.;
	check -= x0[0] * x0[1];
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[0*p+0], check, eps, eps);

	// check partial of h^0 w.r.t x^0_1
	check  = x0[0] * (1. - x0[0] + x0[1]) - 1.;
	check += x0[0] * x0[1];
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[1*p+0], check, eps, eps);

	// check partial of h^0 w.r.t x^1_0 and x^1_1
	check  = 0.;
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[0*p+1], check, eps, eps);
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[1*p+1], check, eps, eps);

	// ----------------------------------------------------------------
	// dw^0 (y) = \partial_y^0 h^1 (y)
	// dw^1 (y) = \partial_y^1 h^1 (y)
	w[0*p+0] = 0.; // coefficient for z^0_0
	w[1*p+0] = 0.; // coefficient for z^0_1
	w[0*p+1] = 1.; // coefficient for z^1_0
	w[1*p+1] = 1.; // coefficient for z^1_1 
	dw       = g.<a href="reverse.xml" target="_top">Reverse</a>(p, w);

	// dv^0 = dw^0 * \partial_x^0 y^0 (x) + dw^1 * \partial_x^0 y^1 (x)  
	// dv^1 = dw^0 * \partial_x^1 y^0 (x) + dw^1 * \partial_x^1 y^1 (x)  
	dv   = f.<a href="reverse.xml" target="_top">Reverse</a>(p, dw); 

	// check partial of h^1 w.r.t x^0_0
	check  = x0[1] * (- x1[0] + x1[1]);
	check -= x1[0] * x0[1];
	check += x1[1] * (1. - x0[0] + x0[1]) - x0[0] * x1[1];
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[0*p+0], check, eps, eps);

	// check partial of h^1 w.r.t x^0_1
	check  = x0[0] * (- x1[0] + x1[1]);
	check += x1[0] * (1. - x0[0] + x0[1]) + x1[0] * x0[1]; 
	check += x0[0] * x1[1];
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[1*p+0], check, eps, eps);

	// check partial of h^1 w.r.t x^1_0
	// (by reverse mode identity is equal to partial h^0 w.r.t. x^0_0)
	check  = 1. - x0[0] * x0[1];
	check += x0[1] * (1. - x0[0] + x0[1]);
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[0*p+1], check, eps, eps);

	// check partial of h^1 w.r.t x^1_1
	// (by reverse mode identity is equal to partial h^0 w.r.t. x^0_1)
	check  = x0[0] * x0[1] - 1.;
	check += x0[0] * (1. - x0[0] + x0[1]);
	ok    &amp;= <a href="nearequal.xml" target="_top">NearEqual</a>(dv[1*p+1], check, eps, eps);

	return ok;
}
</pre>
</font></code>



<hr/>Input File: example/reverse_any.cpp

</body>
</html>
