/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map.Entry;
import beaver.Symbol;

aspect MemoryUse {
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * As {@link #dumpMemoryUse(PrintStream, boolean, int, int)}, with <code>deep = false</code>, 
	 * <code>maxDepth = -1</code>, <code>minSize = 0</code>, and saving the output to a file.
	 * 
	 * @param file     filename to save output as
	 */
	public void ASTNode.dumpMemoryUse(String file) throws FileNotFoundException {
		dumpMemoryUse(file, false);
	}
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * As {@link #dumpMemoryUse(PrintStream, boolean, int, int)}, with <code>maxDepth = -1</code>, 
	 * <code>minSize = 0</code>, and saving the output to a file.
	 * 
	 * @param file     filename to save output as
	 * @param deep     if the memory calculation should include the contents of non-ASTNode members
	 */
	public void ASTNode.dumpMemoryUse(String file, boolean deep) throws FileNotFoundException {
		dumpMemoryUse(file, deep, -1, 0);
	}
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * As {@link #dumpMemoryUse(PrintStream, boolean, int, int)}, but saving the output to a file.
	 * 
	 * @param file     filename to save output as
	 * @param deep     if the memory calculation should include the contents of non-ASTNode members
	 * @param maxDepth the maximum depth to display nodes from, -1 means infinite depth
	 * @param minSize  the minimum memory size to display a node
	 */
	public void ASTNode.dumpMemoryUse(String file, boolean deep, int maxDepth, int minSize) 
			throws FileNotFoundException {
		dumpMemoryUse(new PrintStream(file), deep, maxDepth, minSize);
	}
	
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * As {@link #dumpMemoryUse(PrintStream, boolean, int, int)}, with <code>deep = false</code>, 
	 * <code>maxDepth = -1</code> and <code>minSize = 0</code>.
	 * 
	 * @param out      stream to use for output
	 */
	public void ASTNode.dumpMemoryUse(PrintStream out) {
		dumpMemoryUse(out, false, -1, 0);
	}
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * As {@link #dumpMemoryUse(PrintStream, boolean, int, int)}, with <code>maxDepth = -1</code> and 
	 * <code>minSize = 0</code>.
	 * 
	 * @param out      stream to use for output
	 * @param deep     if the memory calculation should include the contents of non-ASTNode members
	 */
	public void ASTNode.dumpMemoryUse(PrintStream out, boolean deep) {
		dumpMemoryUse(out, deep, -1, 0);
	}
	
	/**
	 * \brief Output a view of an AST, showing the classname and approximate memory footprint 
	 *        of the subtree.
	 * 
	 * @param out      stream to use for output
	 * @param deep     if the memory calculation should include the contents of non-ASTNode members
	 * @param maxDepth the maximum depth to display nodes from, -1 means infinite depth
	 * @param minSize  the minimum memory size to display a node
	 */
	public void ASTNode.dumpMemoryUse(PrintStream out, boolean deep, int maxDepth, int minSize) {
		Profiler.clear();   // Remove any stale data from profiler
		gatherMemoryUse(out, "", deep, maxDepth, minSize, 0);
		Profiler.clear();   // Free any memory used by the profiler's cache
	}
	
	/**
	 * \brief Traversal method for {@link #dumpMemoryUse(PrintStream, boolean, int, int)}.
	 * 
	 * @return approximation of the memory footprint for the subtree
	 */
	protected int ASTNode.gatherMemoryUse(
			PrintStream out, String indent, boolean deep, int maxDepth, int minSize, int depth) {
		int local = Profiler.getNodeSize(this, deep);
		int mem = local;
		String nextInd = indent + " ";
		if (children != null)
			for (int i = children.length -1; i >= 0; i--) 
				if (children[i] != null)
					mem += children[i].gatherMemoryUse(out, nextInd, deep, maxDepth, minSize, depth+1);
		if ((depth == 0 || mem >= minSize) && (maxDepth < 0 || depth < maxDepth))
			addMemoryUseRow(out, indent, mem, local);
		return mem;
	}
	
	protected int Opt.gatherMemoryUse(PrintStream out, String indent, boolean deep, int maxDepth, int minSize, int depth) {
		return super.gatherMemoryUse(out, indent.substring(1), deep, maxDepth, minSize, depth);
	}
	
	protected int InstExtends.gatherMemoryUse(PrintStream out, String indent, boolean deep, int maxDepth, int minSize, int depth) {
		return super.gatherMemoryUse(out, indent, deep, maxDepth, minSize, depth);
	}

	/**
	 * \brief Output method for {@link #dumpMemoryUse(PrintStream, boolean, int, int)}.
	 */
	protected void ASTNode.addMemoryUseRow(PrintStream out, String indent, int mem, int local) {
		out.println(indent + getClass().getSimpleName() + extraMemoryUseInfo() + ": " + 
				formatMem(mem) + " (" + formatMem(local) + ")");
	}
	
	protected void Opt.addMemoryUseRow(PrintStream out, String indent, int mem, int local) {}
	
	/**
	 * \brief Any extra info to add to the memory use output.
	 */
	syn String ASTNode.extraMemoryUseInfo()   = "";
	eq InstClassDecl.extraMemoryUseInfo()     = " \"" + name() + "\"";
	eq InstComponentDecl.extraMemoryUseInfo() = " \"" + name() + "\"";
	eq ClassDecl.extraMemoryUseInfo()         = " \"" + name() + "\"";
	eq ComponentDecl.extraMemoryUseInfo()     = " \"" + name() + "\"";
	eq InstExtends.extraMemoryUseInfo()       = " extending \"" + getClassName().name() + "\"";

	/**
	 * \brief Counts the number of ASTNodes of each type created.
	 * 
	 * Activated by changing the superclass of ASTNode to ProfilingNode.
	 */
	public class ProfilingNode extends Symbol {

		private static int objects = 0;
		private static HashMap<String, Counter> names = new HashMap<String, Counter>();

		/**
		 * \brief If set to <code>true</code>, each instance will be counted as an instance each of 
		 *        every super class.
		 */
		public static boolean COUNT_SUPER = true;
	    
		public ProfilingNode() {
			super();
			objects++;
			count(getClass());
		}
		
		private static void count(Class type) {
			String name = type.getSimpleName();
			if (names.containsKey(name)) 
				names.get(name).n++;
			else
				names.put(name, new Counter(type));
			
			if (COUNT_SUPER && !name.equals("ASTNode"))
				count(type.getSuperclass());
		}

		private static class Counter {
			
			public static int total;
			
			public int n = 1;
			public Class type;
			
			public Counter(Class t) {
				type = t;
			}
			
			public String toString() {
				String size = ASTNode.formatMem(Profiler.getTotalShellSize(type) * n);
				return n + " (" + ASTNode.roundFriendly(n * 100.0 / total) + "%, " + size + ")";
			}
			
		}

		/**
		 * \brief Resets the counters to 0.
		 */
		public static void reset() {
			objects = 0;
			names.clear();
		}
		
		/**
		 * \brief Prints out the collected info.
		 * 
		 * @param out     stream to write output to
		 * @param sorter  comparator for class count entries, use SORT_* constants
		 */
		public static void printInfo(PrintStream out, CountSorter sorter) {
			Counter.total = objects;
			out.println("Number of AST nodes: " + objects);
			ArrayList<Map.Entry<String, Counter>> list = new ArrayList<Map.Entry<String, Counter>>();
			list.addAll(names.entrySet());
			Collections.sort(list, sorter);
			for (Map.Entry<String, Counter> e : list) 
				out.println("  " + e.getKey() + ": " + e.getValue());
		}
		
		/**
		 * \brief Type for the sorter argument for {@link printInfo(PrintStream, CountSorter)}.
		 */
		public static abstract class CountSorter implements Comparator<Entry<String, Counter>> {}
		
		/**
		 * \brief Sort output by count, ascending.
		 */
		public static final CountSorter SORT_COUNT_DESC = new CmpCount(true);
		
		/**
		 * \brief Sort output by count, descending.
		 */
		public static final CountSorter SORT_COUNT_ASC  = new CmpCount(false);
		
		/**
		 * \brief Sort output by class name, ascending.
		 */
		public static final CountSorter SORT_CLASS_DESC = new CmpName(true);
		
		/**
		 * \brief Sort output by class name, descending.
		 */
		public static final CountSorter SORT_CLASS_ASC = new CmpName(false);
		
		private static class CmpCount extends CountSorter {
			
			private int mul;
			
			public CmpCount(boolean reverse) {
				mul = reverse ? -1 : 1;
			}

			public int compare(Entry<String, Counter> o1, Entry<String, Counter> o2) {
				int res = mul * (o1.getValue().n - o2.getValue().n);
				return res == 0 ? o1.getKey().compareTo(o2.getKey()) : res;
			}

		}
		
		private static class CmpName extends CountSorter {
			
			private int mul;
			
			public CmpName(boolean reverse) {
				mul = reverse ? -1 : 1;
			}

			public int compare(Entry<String, Counter> o1, Entry<String, Counter> o2) {
				return mul * o1.getKey().compareTo(o2.getKey());
			}

		}
		
	}

	/**
	 * \brief Contains methods for calculating the size of AST nodes.
	 * 
	 * Uses minimum sizes stipulated by language standard, and ignores padding for memory alignment 
	 * used by many JVMs. Thus values should be treated as minimum values.
	 */
	public abstract class Profiler {
		
		/**
		 * \brief Approximates the memory footprint of an AST node.
		 * 
		 * @param deep  if the approximation should include the contents of non-ASTNode members
		 */
		public static int getNodeSize(ASTNode node, boolean deep) {
			if (deep)
				return getObjectSize(node);
			else 
				return getTotalShellSize(node.getClass());
		}
		
		/**
		 * \brief Clear cached data.
		 */
		public static void clear() {
			visited.clear();
		}

	    private static final int OBJECT_SHELL_SIZE   = 8;
	    private static final int OBJREF_SIZE         = 4;
	    private static final int LONG_FIELD_SIZE     = 8;
	    private static final int INT_FIELD_SIZE      = 4;
	    private static final int SHORT_FIELD_SIZE    = 2;
	    private static final int CHAR_FIELD_SIZE     = 2;
	    private static final int BYTE_FIELD_SIZE     = 1;
	    private static final int BOOLEAN_FIELD_SIZE  = 1;
	    private static final int DOUBLE_FIELD_SIZE   = 8;
	    private static final int FLOAT_FIELD_SIZE    = 4;

		private static HashMap<Class, Integer> totalFieldSize = new HashMap<Class, Integer>();
		private static IdentityHashMap<Object, Object> visited = new IdentityHashMap<Object, Object>();
	    
	    private static class GetFieldsAction implements PrivilegedAction {
	    	
	    	private Class cls;
	    	
	    	public Field[] perform(Class cl) {
	    		cls = cl;
	    		return (Field[]) AccessController.doPrivileged(this);
	    	}

			public Object run() {
				return cls.getDeclaredFields();
			}
	    	
	    }
	    
	    private static class GetValueAction implements PrivilegedExceptionAction {
	    	
	    	private Field field;
	    	
	    	public Object perform(Field f, Object o) throws Exception {
	    		field = f;
	    		AccessController.doPrivileged(this);
	    		return f.get(o);
	    	}

			public Object run() throws Exception {
				field.setAccessible(true);
				return null;
			}
	    	
	    }
	    
		private static GetFieldsAction getFields = new GetFieldsAction();
		private static GetValueAction  getValue  = new GetValueAction();
		
		public static int getObjectSize(Object o) {
			if (o == null || visited.containsKey(o))
				return 0;
			visited.put(o, null);
			
			Class type = o.getClass();
			if (type.isArray())
				return getArraySize(o, type);
			
			int mem = getTotalShellSize(type);
			for (; type != null; type = type.getSuperclass())
				for (Field f : getFields.perform(type)) 
					if ((f.getModifiers() & Modifier.STATIC) == 0) 
						mem += getObjectFieldSize(f, o);
			return mem;
		}
		
		private static int getObjectFieldSize(Field f, Object o) {
			Class type = f.getType();
			if (type.isPrimitive())
				return 0;
			try {
				Object val = getValue.perform(f, o);
				return (val instanceof ASTNode) ? 0 : getObjectSize(val);
			} catch (Exception e) {
				System.err.println("Could not read member: " + o.getClass().getSimpleName() + "." + f.getName());
				return OBJECT_SHELL_SIZE;
			}
		}
		
		public static int getTotalShellSize(Class type) {
			if (totalFieldSize.containsKey(type))
				return totalFieldSize.get(type);
			
			int mem = OBJECT_SHELL_SIZE;
			if (type != Object.class)
				mem = getTotalShellSize(type.getSuperclass());
			
			for (Field f : getFields.perform(type)) 
				if ((f.getModifiers() & Modifier.STATIC) == 0) 
					mem += getFieldSize(f.getType());
			
			totalFieldSize.put(type, mem);
			return mem;
		}
		
		private static int getFieldSize(Class type) {
	        if (type == long.class)
	        	return LONG_FIELD_SIZE;
	        else if (type == double.class)
	        	return DOUBLE_FIELD_SIZE;
        	return INT_FIELD_SIZE;
		}
		
		private static int getArrayElementSize(Class type) {
	        if (type == int.class)
	            return INT_FIELD_SIZE;
	        else if (type == long.class)
	        	return LONG_FIELD_SIZE;
	        else if (type == short.class)
	        	return SHORT_FIELD_SIZE;
	        else if (type == byte.class)
	        	return BYTE_FIELD_SIZE;
	        else if (type == boolean.class)
	        	return BOOLEAN_FIELD_SIZE;
	        else if (type == char.class)
	        	return CHAR_FIELD_SIZE;
	        else if (type == double.class)
	        	return DOUBLE_FIELD_SIZE;
	        else if (type == float.class)
	        	return FLOAT_FIELD_SIZE;
        	return OBJREF_SIZE;
		}
		
		private static int getArraySize(Object o, Class type) {
			int len = java.lang.reflect.Array.getLength(o);
			Class comp = type.getComponentType();
			int size = getArrayElementSize(comp);
			int res = OBJECT_SHELL_SIZE + INT_FIELD_SIZE + OBJREF_SIZE + len * size;
			if (!comp.isPrimitive()) { 
				for (int i = 0; i < len; i++) {
					Object elem = java.lang.reflect.Array.get(o, i);
					if (!(elem instanceof ASTNode))
						res += getObjectSize(elem);
				}
			}
			return res;
		}

	}

}