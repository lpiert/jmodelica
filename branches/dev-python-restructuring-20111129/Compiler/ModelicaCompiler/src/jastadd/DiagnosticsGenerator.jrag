/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.File;

aspect DiagnosticsGeneration {

/**
 * A class for generating 
 */
public class DiagnosticsGenerator {

	private String modelName;
	private String modelNameUnderscore;
	private String diagnosticsDirString;
	
	private String rawFlattenedModelFileName;
	private String transformedModelFileName;	
	private String errorsFileName;
	private String bltFileName;	
	private String aliasFileName;		
	private String connectionsFileName;		
    private String indexFileName;			

	private String rawFlattenedModelAbsFileName;
	private String transformedModelAbsFileName;	
	private String errorsAbsFileName;
	private String bltAbsFileName;	
	private String aliasAbsFileName;		
	private String connectionsAbsFileName;		
    private String indexAbsFileName;		
	
	private int numErrors = -1;
	private int numComplianceErrors = -1;
	private int numWarnings = -1;

	/**
	 * Default constructor for the Diagnostics generator.
	 *
	 * The diagnostics generator generates a number of HTML files containing
	 * diagnostics for the compilation of a model.
	 */	
	private String html_header ="<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n" +   
								"<html>\n" + 
								"<head>\n" + 
  								"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n" +
    							"<title>Model diagnosis</title>\n" + 
    							"</head>\n" + 
							    "<body style=\"background-color: rgb(255, 255, 255);\">\n";
    
    private String html_tail = "</body>\n </html>\n";
	
	public DiagnosticsGenerator(String modelName) {
		this.modelName = modelName;
		this.modelNameUnderscore = modelName.replace('.','_');
		// Create directory containing the diagnostics files	
		File currDir = new File(".");
		String currDirString = currDir.getAbsolutePath().substring(0,currDir.getAbsolutePath().length()-2);
		File diagnosticsDir = new File(currDirString,this.modelNameUnderscore + "_html_diagnostics");
		boolean success = diagnosticsDir.mkdir();
		diagnosticsDirString = diagnosticsDir.getAbsolutePath();
		/*
		if (success) {
			System.out.println("dir created");
		} else {
			System.out.println("dir not created");
		}
*/
		this.rawFlattenedModelFileName = this.modelNameUnderscore + "_raw.html";
		this.transformedModelFileName = this.modelNameUnderscore + "_transformed.html";
		this.errorsFileName = this.modelNameUnderscore + "_errors.html";
		this.bltFileName = this.modelNameUnderscore + "_blt.html";
		this.aliasFileName = this.modelNameUnderscore + "_alias.html";
		this.connectionsFileName = this.modelNameUnderscore + "_connections.html";
		this.indexFileName = "index.html";
				
		this.rawFlattenedModelAbsFileName = (new File(diagnosticsDirString,this.rawFlattenedModelFileName)).getAbsolutePath();
		this.transformedModelAbsFileName = (new File(diagnosticsDirString,this.transformedModelFileName)).getAbsolutePath();
		this.errorsAbsFileName = (new File(diagnosticsDirString,this.errorsFileName)).getAbsolutePath();
		this.bltAbsFileName = (new File(diagnosticsDirString,this.bltFileName)).getAbsolutePath();
		this.aliasAbsFileName = (new File(diagnosticsDirString,this.aliasFileName)).getAbsolutePath();
		this.connectionsAbsFileName = (new File(diagnosticsDirString,this.connectionsFileName)).getAbsolutePath();
		this.indexAbsFileName = (new File(diagnosticsDirString,this.indexFileName)).getAbsolutePath();
		
		// Clear old files
		PrintStream out;
		try {
			out = new PrintStream(rawFlattenedModelAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(transformedModelAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(errorsAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(bltAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(aliasAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(connectionsAbsFileName,"UTF-8");
			out.close();
			out = new PrintStream(indexAbsFileName,"UTF-8");
			out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}

    /**
     *  Write a file containing the raw flattened model.
     */
	public void writeRawFlattenedModel(FClass fc) {
		// Dump model to file
		PrintStream out;
		try {
			out = new PrintStream(rawFlattenedModelAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<pre>\n");
			fc.prettyPrint(out, "");
			out.print("</pre>\n");
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		}  
		
	}

    /**
     *  Write a file containing the transformed scalarized model..
     */	
	public void writeTransformedFlattenedModel(FClass fc) {
		// Dump model to file
		try {
		    PrintStream out;
			out = new PrintStream(transformedModelAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<pre>\n");
			fc.prettyPrint(out, "");
			out.print("</pre>\n");	
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}

    /**
     *  Write a file containing BLT information
     */	
	public void writeBLTFile(FClass fc) {
		// Dump model to file
		try {
		    PrintStream out;
			out = new PrintStream(bltAbsFileName,"UTF-8");
			out.print(html_header);
			if (fc.root().options.getBooleanOption("equation_sorting")) {
				int ind = 0;
				out.print("<h2>BLT for initialization system </h2>\n");
				for (AbstractEquationBlock eb : fc.getDAEInitBLT()) {
					out.print("<pre>\n");
					out.print("--- Block " + ind++ + "---\n");
					out.print(eb.toString()+"\n");
					out.print("</pre>\n");
				}
				ind = 0;
				out.print("<h2>BLT for DAE system </h2>\n");	
				out.print("<h3> ODE blocks </h3>\n");	
				// Loop over all derivatives
				for (AbstractEquationBlock eb : fc.getDAEStructuredBLT().getOdeBlocks()) {
					out.print("<pre>\n");
					out.print("--- Block " + ind++ + "---\n");
					out.print(eb.toString()+"\n");
					out.print("</pre>\n");					
				}
				out.print("<h3> Real output blocks </h3>\n");	
				for (AbstractEquationBlock eb : fc.getDAEStructuredBLT().getRealOutputBlocks()) {
					out.print("<pre>\n");
					out.print("--- Block " + ind++ + "---\n");
					out.print(eb.toString()+"\n");
					out.print("</pre>\n");					
				}
				out.print("<h3> Integer and boolean output blocks </h3>\n");	
				for (AbstractEquationBlock eb : fc.getDAEStructuredBLT().getIntegerBooleanOutputBlocks()) {
					out.print("<pre>\n");
					out.print("--- Block " + ind++ + "---\n");
					out.print(eb.toString()+"\n");
					out.print("</pre>\n");	
				}
				out.print("<h3> Other output blocks </h3>\n");					
				for (AbstractEquationBlock eb : fc.getDAEStructuredBLT().getOtherBlocks()) {
					out.print("<pre>\n");
					out.print("--- Block " + ind++ + "---\n");
					out.print(eb.toString()+"\n");
					out.print("</pre>\n");					
				}		
			}
			out.print("</pre>\n");	
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}

    /**
     *  Write a file containing the alias elimination information
     */	
	public void writeAliasFile(FClass fc) {
		// Dump model to file
		try {
		    PrintStream out;
			out = new PrintStream(aliasAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<h2>Alias sets: " + modelName + "</h2>\n"); 
			out.print("<pre>\n");
			out.print(fc.aliasDiagnostics());
			out.print("</pre>\n");	
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}

    /**
     *  Write a file containing the alias elimination information
     */	
	public void writeConnectionsFile(FClass fc) {
		try {
		    PrintStream out;
			out = new PrintStream(connectionsAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<h2>Connection sets: " + modelName + "</h2>\n"); 
			out.print("<pre>\n");
			out.print(fc.getConnectionSetManager().printConnectionSets());
			out.print("</pre>\n");	
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}
	
	/**
     *  Write a file containing the problems in the model.
     */
	public void writeProblems(Collection<Problem> problems) {
		// Print problems
		try {
			PrintStream out;
			out = new PrintStream(errorsAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<h2>Problems: " + modelName + "</h2>\n"); 
			out.print("<pre>\n");
			StringBuffer errs = new StringBuffer();
			StringBuffer compErrs = new StringBuffer();
			StringBuffer warns = new StringBuffer();
			numErrors = 0;
			numComplianceErrors = 0;
			numWarnings = 0;
			for (Problem p : problems) {
				if (p.severity() == Problem.Severity.ERROR && 
				    p.kind() != Problem.Kind.COMPLIANCE) {
					errs.append(p.toString() + "\n");
					numErrors++; 
				} else if (p.severity() == Problem.Severity.ERROR &&
				           p.kind() == Problem.Kind.COMPLIANCE) {
					compErrs.append(p.toString() + "\n");
					numComplianceErrors++;
				} else if (p.severity() == Problem.Severity.WARNING) {
					warns.append(p.toString() + "\n");
					numWarnings++;
				}
			}
			out.print(errs.toString());
			out.print(compErrs.toString());
			out.print(warns.toString());
			out.print("</pre>\n");	
			out.print(html_tail);
		   	out.close();
		   	
		   	// Write a simple version of index.html in case there are errors
			out = new PrintStream(indexAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<h2>" + modelName + "</h2>\n"); 
			out.print("<a href=\"" + errorsFileName + "\"> Problems:</a><br>\n");
			out.print("  " + numErrors + " errors, " + numComplianceErrors + 
			          " compliance errors, " + numWarnings + " warnings<br><br>"); 
			out.print(html_tail);
		   	out.close();
			          
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
	}
	
	/**
     *  Write index.html.
     */
	public void writeDiagnostics(FClass fc) {
		// Create index.html containing:
		
	    try {
		    PrintStream out;
			out = new PrintStream(indexAbsFileName,"UTF-8");
			out.print(html_header);
			out.print("<h2>" + modelName + "</h2>\n"); 
			out.print("<pre>\n");
			// Diagnostics about model sizes
			out.print(fc.modelDiagnostics());
			out.print("</pre>\n");	
			// Model name and links to the flattened models
			out.print("<a href=\"" + rawFlattenedModelAbsFileName + "\">Flattened model</a><br><br>\n"); 
			out.print("<a href=\"" + transformedModelAbsFileName + "\">Transformed model</a><br><br>\n"); 
			out.print("<a href=\"" + errorsFileName + "\"> Problems:</a><br>\n");
			out.print("  " + numErrors + " errors, " + numComplianceErrors + 
			          " compliance errors, " + numWarnings + " warnings<br><br>"); 
			out.print("<a href=\"" + aliasFileName + "\"> Alias sets</a><br><br>\n"); 
			out.print("<a href=\"" + connectionsFileName + "\">Connection sets</a><br><br>\n"); 
			out.print("<a href=\"" + bltFileName + "\">BLT diagnostics</a><br>\n"); 
			if (fc.root().options.getBooleanOption("equation_sorting")) {
				ArrayList<Integer> unsolvedDAEInitBlockSizes =
				    fc.getDAEInitBLT().unsolvedBlockSizes(); 
				ArrayList<Integer> unsolvedDAEBlockSizes =
				    fc.getDAEBLT().unsolvedBlockSizes(); 
				out.print("  Number of unsolved equation blocks in DAE initialization system: " + 
			              unsolvedDAEInitBlockSizes.size() + ": {");   
				int ind = 0;
				for (Integer bs : unsolvedDAEInitBlockSizes) {
					out.print(bs.toString());
					if (ind<unsolvedDAEInitBlockSizes.size()-1) {
						out.print(",");
					}	
					ind++;
				}
				out.println("}<br>\n");
				out.print("  Number of unsolved equation blocks in DAE system: " + 
			              unsolvedDAEBlockSizes.size() + ": {");   
				ind = 0;
				for (Integer bs : unsolvedDAEBlockSizes) {
					out.print(bs.toString());
					if (ind<unsolvedDAEBlockSizes.size()-1) {
						out.print(",");
					}	
					ind++;
				}
				out.println("}<br>\n");
			}      
			out.print(html_tail);
		   	out.close();
		} catch(FileNotFoundException e) {
		} catch(UnsupportedEncodingException e) {		
		} 
		
		// Create BLT diagnostics file (if equation_sorting is true)
		writeBLTFile(fc);
				
		// Create connection set information file
		writeConnectionsFile(fc);
				
		// Create a file for alias information
		writeAliasFile(fc);
	}

	/**
     *  Finalize the generation.
     */
	public void finalize() {}

}
}