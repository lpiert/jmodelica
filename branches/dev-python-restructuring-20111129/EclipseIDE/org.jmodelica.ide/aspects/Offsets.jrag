/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.BadLocationException;

aspect Offset {
	
	public int StoredDefinition.calcOffset(int location) {
		int line = ASTNode.getLine(location);
		int col = ASTNode.getColumn(location);
		return lineBreakMap.get(line - 1) + col - 1;
	}
	
  	protected int ASTNode.createOffset(int location) {
		try {
			return getDefinition().calcOffset(location);
		} catch (Exception e) {
			return -1;
		}
    }
	
	/**
	 * Gets offset of the beginning or end of a line.
	 * 
	 * Adjust is typically 1 if finding the end of the line, 0 otherwise.
	 * 
	 * @param location  a location in the line to find the offset of.
	 * @param adjust    added to the line number if the location isn't at the beginning of the line.
	 */
	protected int ASTNode.createLineOffset(int location, int adjust) {
		int line = ASTNode.getLine(location);
		int col = ASTNode.getColumn(location);
		if (col > 1)
			line += adjust;
		return createOffset(ASTNode.makePosition(line, 1));
	}
	
	syn int ASTNode.getValidStart() = start == 0 ? getParent().getValidStart() : start;
	syn int ASTNode.getValidEnd()   = end == 0 ? getParent().getValidEnd() : end;
	eq Root.getValidStart()         = start;
	eq Root.getValidEnd()           = end;
	
	syn int ASTNode.getBeginOffset()     = createOffset(getValidStart());
	syn int ASTNode.getEndOffset()       = createOffset(getValidEnd());
	 
	syn int ASTNode.getBeginLineOffset() = createLineOffset(getValidStart(), 0);
	syn int ASTNode.getEndLineOffset()   = createLineOffset(getValidEnd(), 1);
	
	syn int ASTNode.offset()             = getBeginOffset();
	syn int ASTNode.length()             = getEndOffset() - getBeginOffset() + 1;
	

	syn boolean ASTNode.contains(int offset, int length) {
		int begin = getBeginOffset();
		int end = getEndOffset();
		// Assume no end info means until end of file
		return end < 0 || (begin <= offset && end >= offset + length);
	}
	
	syn ClassDecl ASTNode.containingClassDecl(int offset, int length) {
		if (!contains(offset, length))
			return null;
		for (ASTNode child : this) {
			ClassDecl res = child.containingClassDecl(offset, length);
			if (res != null)
				return res;
		}
		return containingClassDeclForThis();
	}
	
	syn ClassDecl ASTNode.containingClassDeclForThis() = null;
	eq BaseClassDecl.containingClassDeclForThis()      = this;

	
	syn ClassDecl ASTNode.firstClassDecl() {
		for (ASTNode child : this) {
			ClassDecl res = child.firstClassDecl();
			if (res != null)
				return res;
		}
		return null;
	}
	eq BaseClassDecl.firstClassDecl()    = this;
	eq SourceRoot.firstClassDecl()       = getProgram().firstClassDecl();
	eq Program.firstClassDecl()          = getUnstructuredEntitys().firstClassDecl();
	eq StoredDefinition.firstClassDecl() = getElements().firstClassDecl();
	
}