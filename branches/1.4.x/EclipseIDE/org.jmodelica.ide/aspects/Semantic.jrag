import org.jmodelica.ide.helpers.Maybe;
import org.jmodelica.ide.namecomplete.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


aspect DocumentLookup {

    /**
     * @return true iff. ASTNode contains point (line, col).
     */
    public boolean ASTNode.containsPoint(IDocument d, int offset) {
        
        int line, col;
        try {

            line = d.getLineOfOffset(offset);
            col = offset - d.getLineOffset(line);
            line++; // go from 0-based to 1-based line number
        
        } catch (BadLocationException e) { return false; }
        
        
        boolean beginsBefore = 
            getBeginLine() < line ||
            getBeginLine() == line && getBeginColumn() <= col;
        
        boolean endsAfter = 
            getEndLine() > line ||
            getEndLine() == line && getEndColumn() >= col;
        
        return beginsBefore && endsAfter;
    }    
    
       
    /**
     * Return class declaration at offset. Favour inner classes over outer.
     * Ignores short class declarations.
     */
    syn Maybe<ClassDecl> ASTNode.getClassDeclAt(IDocument d, int offset);
    
    eq ASTNode.getClassDeclAt(IDocument d, int offset) {
        for (ASTNode<?> child : this) {
            Maybe<ClassDecl> rec = child.getClassDeclAt(d, offset);  
            if (rec.hasValue())
                return rec;
        }
        return Maybe.<ClassDecl>Nothing();
        
    }
    
    eq FullClassDecl.getClassDeclAt(IDocument d, int offset) {
        return 
            super.getClassDeclAt(d, offset)
            .orElse(
            Maybe.guard(this, this.containsPoint(d, offset)));
    }
    
    /**
     * Return access at offset. Favour left children of Dots. E.g.
     *
     *  ...qualified.name      returns "qualified", while
     *  ...qualified.na^me      returns "qualified.name"
     *     
     */
    syn Maybe<Access> ASTNode.getAccessAt(IDocument d, int offset); 
    
    eq ASTNode.getAccessAt(IDocument d, int offset) {
        for (ASTNode<?> child : this) {
            Maybe<Access> rec = child.getAccessAt(d, offset);  
            if (rec.hasValue())
                return rec;
        }
        return Maybe.<Access>Nothing();
    }
    
    eq Dot.getAccessAt(IDocument d, int offset) {
        if (!containsPoint(d, offset))
            return Maybe.<Access>Nothing();
        return getLeft().getAccessAt(d, offset).orElse(Maybe.Just(this));
    }
    
    eq Access.getAccessAt(IDocument d, int offset) {
        return Maybe.guard(this, containsPoint(d, offset));
    } 
}
   




aspect Completion {

    InstNode implements CompletionNode; //{
    
        public String InstNode.completionName() { 
            return name(); 
        }
        public CompletionComment InstNode.completionDoc() { 
            return proposalComment(); 
        }
        public Image InstNode.completionImage() { 
            return contentOutlineImage(); 
        }
    //}
    
    InstImportRename implements CompletionNode; // {
        
        public String InstImportRename.completionName() { 
            return ((ImportClauseRename)getImportClause()).getIdDecl().getID(); 
        }
        public CompletionComment InstImportRename.completionDoc() {
            return getPackageName().myInstClassDecl().proposalComment();
        }
        public Image InstImportRename.completionImage() { 
            return null; 
        }
    //}
        
    LibNode implements CompletionNode; //{
        public String LibNode.completionName() {
            return getName();
        }
        public CompletionComment LibNode.completionDoc() {
            return CompletionComment.NULL;
        }
        public Image LibNode.completionImage() {
            return null;
        }
    //}
    
    syn ArrayList<CompletionNode> InstNode.completionNodes(
        boolean includeRenamedImports,
        boolean includeOuterScopes)  
    { 
        return new ArrayList<CompletionNode>();
    }
    
    /**
     * Completion proposals for InstClassDecl:s are all classes and
     * (constant) components.
     * 
     * @param includeRenamedImports renamed imports should only be included
     * in completions if looking up an unqualified import. qualified access
     * to renamed imports is not allowed in Modelica
     */
    eq InstClassDecl.completionNodes(
        boolean includeRenamedImports,
        boolean includeOuterScopes) 
    {

        ArrayList<CompletionNode> completions = new ArrayList<CompletionNode>();

        // class decls
        completions.addAll(instClassDecls());
        
        // component decls
        completions.addAll(instComponentDecls());

        // imports
        for (InstImport ii : instImports()) { 
            
            if (includeRenamedImports && ii instanceof InstImportRename) { 

                completions.add(
                    (InstImportRename)ii);
            
            } else if (ii instanceof InstImportUnqualified) {
                
                completions.addAll(
                    ii
                    .getPackageName()
                    .myInstClassDecl()
                    .completionNodes(
                        includeRenamedImports,
                        false));
            }
        }

        // extends
        for (InstExtends e : instExtends()) 
            completions.addAll(
                e
                .getClassName()
                .myInstClassDecl()
                .completionNodes(
                    includeRenamedImports,
                    false));

        // outer scopes
        if (includeOuterScopes) {
            completions.addAll(
                outerClassDecls());
        }
            
        return completions;
    }
    
    inh ArrayList<CompletionNode> InstClassDecl.outerClassDecls();
    
    eq Root.getChild().outerClassDecls() = new ArrayList<CompletionNode>();
    
    eq InstClassDecl.getChild().outerClassDecls() {
        
        ArrayList<CompletionNode> decls = 
            new ArrayList<CompletionNode>(this.outerClassDecls());
        
        for (InstNode node : this.getInstClassDecls()) 
            decls.add(node);

        return decls;
        
    }
    
    eq InstProgramRoot.getChild().outerClassDecls() {
        
        ArrayList<CompletionNode> decls =
            new ArrayList<CompletionNode>();
        
        for (InstNode node : this.getInstClassDecls())  
            decls.add(node);
        
        for (LibNode node : this.getProgram().getLibNodes()) 
            decls.add(node);
        
        return decls;
    }

    eq InstNode.getChild().outerClassDecls() {
        return new ArrayList<CompletionNode>();
    }
    
    /**
     * Completion proposals for InstComponentDecl:s are only components. 
     */
    eq InstComponentDecl.completionNodes(
        boolean includeRenamedImports,
        boolean includeOuterScopes)
    {
        ArrayList<CompletionNode> nodes = new ArrayList<CompletionNode>();
        for (CompletionNode c : myInstClass().completionNodes(includeRenamedImports, includeOuterScopes)) {
            if (c instanceof InstComponentDecl)
                nodes.add(c);
        }
        return nodes;
    }

    /**
     * Returns a list of completion suggestions for <code>decl</code>, filtering
     * out suggestions not matching <code>filter</code>.
     * 
     * @param access access to give completions for
     * @param filter Filter for matches
     * @return list of suggestions
     */   
    public ArrayList<CompletionNode> InstNode.completionProposals(
            CompletionFilter filter,
            boolean unqualified) {

        ArrayList<CompletionNode> proposals =
            new ArrayList<CompletionNode>();

        for (CompletionNode node : completionNodes(unqualified, unqualified))
            if (filter.matches(node.completionName()))
                proposals.add(node);
        
        return proposals;
    }

    /**
     * Dynamically add a StoredDefintion to Program
     */
    public void Program.dynamicAddStoredDefinition(StoredDefinition def) {
        def.setParent(this);
        this.addUnstructuredEntity(def);
    }
    
    /**
     * Used for lookup in name completion. Adds access, then caller calls
     * myInstClassDecl to lookup access in instance tree.
     */
    public InstAccess InstClassDecl.addClassDynamic(InstAccess access) {
        addDynamicClassName(access);
        // return value is equal to access, but this method needs to be called
        // for proper evaluation of attributes
        return 
            (InstAccess) getDynamicClassNameList().getLast();
    }
     
    /**
     * See above.
     */
    public InstAccess InstClassDecl.addComponentDynamic(InstAccess access) {
        addDynamicComponentName(access);
        // return value is equal to access, but this method needs to be called
        // for proper evaluation of attributes
        return 
            (InstAccess) getDynamicComponentNameList().getLast();
    }
}




/**
 * Get the string comment from classes to display in the content assist. 
 */
aspect CompletionComment {
    
    syn CompletionComment InstNode.proposalComment() = 
        CompletionComment.NULL;
    
    eq InstClassDecl.proposalComment() =
        new CompletionComment(
            myInstClass()
            .getClassDecl()
            .stringComment());
    
    syn String ASTNode.stringComment() = 
        null;
    
    eq FullClassDecl.stringComment() = 
        hasStringComment() 
            ? getStringComment().getComment() 
            : null;
    
}






//TODO: remove. only for debugging
aspect NADebugging {    
    
    private void ASTNode.debugPrintThis(String ind, PrintStream p) {
        p.printf("%s%s:\n", ind, this.getNodeName());
    }
    
    public void ASTNode.printDebugInfo() {
        this.printDebugInfo("", System.out);
    }
    
    public void ASTNode.printDebugInfo(String ind, PrintStream p) {
        
        this.debugPrintThis(ind, p);
        
        for (ASTNode node : this) 
            node.printDebugInfo(ind + "  ", p);
    }
    
    public void List.printDebugInfo(String ind, PrintStream p) {
        for (ASTNode node : this) 
            node.printDebugInfo(ind + "  ", p);
    }
    
    public void Opt.printDebugInfo(String ind, PrintStream p) {
        for (ASTNode node : this) 
            node.printDebugInfo(ind + "  ", p);
    }

    public String ASTNode.toStringPrim() {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        printDebugInfo("", new PrintStream(b));
        return b.toString();
    }

}

