/*
    Copyright (C) 2010 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect XMLTestAnnotationizer {

abstract public class TestAnnotationizerHelper {

	/**
	 * \brief An XML code generation test.
	 */
	public static class XMLCodeGenTestCase extends CodeGenTestCase {

		/**
		 * \brief Constructor for XML code generation test.
		 * 
		 * Delegates to super class.
		 */
		public XMLCodeGenTestCase(String filePath, String className, String description, String opts, String data) throws Exception {
			super(filePath, className, description, opts, data);
		}
		
		/**
		 * \brief Prints a usage message for this test case type.
		 */
		public static void usage(String cl, String extra) {
			TestAnnotationizerHelper.usage(cl, "XML code template");
		}

		/**
		 * \brief Create the code generator to use.
		 * 
		 * Creates an XMLGenerator.
		 */
		@Override
		public AbstractGenerator createGenerator(FClass fc) {
			return new XMLGenerator(new PrettyPrinter(), '$', fc);
		}
		
	}

	/**
	 * \brief An XML value generation test.
	 */
	public static class XMLValueGenTestCase extends XMLCodeGenTestCase {

		/**
		 * \brief Constructor for XML code generation test.
		 * 
		 * Delegates to super class.
		 */
		public XMLValueGenTestCase(String filePath, String className, String description, String opts, String data) throws Exception {
			super(filePath, className, description, opts, data);
		}

		/**
		 * \brief Create the code generator to use.
		 * 
		 * Creates an XMLGenerator.
		 */
		@Override
		public AbstractGenerator createGenerator(FClass fc) {
			return new XMLValueGenerator(new PrettyPrinter(), '$', fc);
		}
		
	}

}

}
