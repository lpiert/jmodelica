
/*
Copyright (C) 2009 Modelon AB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file CGenerator.java
*  \brief CGenerator class.
*/

import java.io.*;

public class CGenerator extends GenericGenerator {
	
	protected static final String INDENT = "    ";
	
	class DAETag_C_externalFuncIncludes extends DAETag {
		
		public DAETag_C_externalFuncIncludes(AbstractGenerator myGenerator,
				FClass fclass) {
			super("external_func_includes","C: external function includes",
					myGenerator,fclass);
		}
		
		public void generate(PrintStream genPrinter) {
			for(String incl : fclass.externalIncludes()) {
				genPrinter.print(incl+"\n");
			}
		}
	}

	class DAETag_C_scalingMethod extends DAETag {
		
		public DAETag_C_scalingMethod(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_scaling_method","C: scaling_method",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			if (fclass.root().options.getBooleanOption("enable_variable_scaling")) {
				genPrinter.print("JMI_SCALING_VARIABLES");
			} else {
				genPrinter.print("JMI_SCALING_NONE");
			}
		}
	
	}
	
	class DAETag_C_equationResiduals extends DAETag {
		
		public DAETag_C_equationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_equation_residuals","C: equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FAbstractEquation e : fclass.equations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i += e.numScalarEquations();
			}
		}
	
	}

	class DAETag_C_eventIndicatorResiduals extends DAETag {
		
		public DAETag_C_eventIndicatorResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_event_indicator_residuals","C: event indicator residuals in equations",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FRelExp e : fclass.relExpInEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}
		}
	
	}

	class DAETag_C_initialEquationResiduals extends DAETag {
		
		public DAETag_C_initialEquationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_equation_residuals","C: initial equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FAbstractEquation e : fclass.equations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i += e.numScalarEquations();
			}
			for (FAbstractEquation e : fclass.initialEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i += e.numScalarEquations();
			}
		}	
	}

	class DAETag_C_initialEventIndicatorResiduals extends DAETag {
		
		public DAETag_C_initialEventIndicatorResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_event_indicator_residuals","C: event indicator residuals in initial equations",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FRelExp e : fclass.relExpInEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}
			for (FRelExp e : fclass.relExpInInitialEquations()) {
				e.genResidual_C(i,INDENT,genPrinter);				
				i++;
			}
		}
	}
	
	class DAETag_C_initialGuessEquationResiduals extends DAETag {
		
		public DAETag_C_initialGuessEquationResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_guess_equation_residuals","C: initial guess equation residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FRealVariable fv : fclass.realVariables()) {
				if (!(fv.fixedAttribute() )) {
					fv.genStartAttributeResidual_C(i,"   ",genPrinter);
					i++;
				}
			}
		}
	
	}

	class DAETag_C_initialDependentParameterResiduals extends DAETag {
		
		public DAETag_C_initialDependentParameterResiduals(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_dependent_parameter_residuals","C: dependent parameter residuals",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			int i=0;
			for (FAbstractEquation e : fclass.getFParameterEquations()) {
				e.genResidual_C(i, INDENT, genPrinter);				
				i += e.numScalarEquations();
			}
		}
	
	}

	class DAETag_C_initialDependentParameterAssignments extends DAETag {
		
		public DAETag_C_initialDependentParameterAssignments(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_DAE_initial_dependent_parameter_assignments","C: dependent parameter assignments",
			  myGenerator,fclass);
		}
	
		public void generate(PrintStream genPrinter) {
			for (FAbstractEquation e : fclass.getFParameterEquations()) 
				e.genAssignment_C(INDENT, genPrinter);
		}
	
	}

	class DAETag_C_variableAliases extends DAETag {
		
		public DAETag_C_variableAliases(AbstractGenerator myGenerator, 
		  FClass fclass) {
			super("C_variable_aliases","C: macros for C variable aliases",
			  myGenerator,fclass);
		}
		
		private void generateVar(PrintStream genPrinter, FVariable fv, String offset, int index) {
			genPrinter.print("#define ");
			genPrinter.print(fv.name_C());
			genPrinter.print(" ((*(jmi->z))[jmi->offs_");
			genPrinter.print(offset);
			genPrinter.print("+");
			genPrinter.print(index);
			genPrinter.print("])\n");
		}
		
		private void generateVarList(PrintStream genPrinter, Iterable<? extends FVariable> list, String offset) {
			int index = 0;
			for (FVariable fv : list)
				generateVar(genPrinter, fv, offset, index++);
		}
	
		public void generate(PrintStream genPrinter) {
			generateVarList(genPrinter, fclass.independentRealConstants(),     "real_ci");
			generateVarList(genPrinter, fclass.dependentRealConstants(),       "real_cd");
			generateVarList(genPrinter, fclass.independentRealParameters(),    "real_pi");
			generateVarList(genPrinter, fclass.dependentRealParameters(),      "real_pd");
			// Handle enums as Integers
			generateVarList(genPrinter, fclass.independentIntegerConstants(),  "integer_ci");
			generateVarList(genPrinter, fclass.independentEnumConstants(),     "integer_ci");
			generateVarList(genPrinter, fclass.dependentIntegerConstants(),    "integer_cd");
			generateVarList(genPrinter, fclass.dependentEnumConstants(),       "integer_cd");
			generateVarList(genPrinter, fclass.independentIntegerParameters(), "integer_pi");
			generateVarList(genPrinter, fclass.independentEnumParameters(),    "integer_pi");
			generateVarList(genPrinter, fclass.dependentIntegerParameters(),   "integer_pd");
			generateVarList(genPrinter, fclass.dependentEnumParameters(),      "integer_pd");
			generateVarList(genPrinter, fclass.independentBooleanConstants(),  "boolean_ci");
			generateVarList(genPrinter, fclass.dependentBooleanConstants(),    "boolean_cd");
			generateVarList(genPrinter, fclass.independentBooleanParameters(), "boolean_pi");
			generateVarList(genPrinter, fclass.dependentBooleanParameters(),   "boolean_pd");
			generateVarList(genPrinter, fclass.derivativeVariables(),          "real_dx");
			generateVarList(genPrinter, fclass.differentiatedRealVariables(),  "real_x");
			generateVarList(genPrinter, fclass.realInputs(),                   "real_u");
			generateVarList(genPrinter, fclass.algebraicRealVariables(),       "real_w");
			genPrinter.print("#define time ((*(jmi->z))[jmi->offs_t])\n"); 
			generateVarList(genPrinter, fclass.discreteRealVariables(),        "real_d");
			generateVarList(genPrinter, fclass.discreteIntegerVariables(),     "integer_d");
			generateVarList(genPrinter, fclass.discreteEnumVariables(),        "integer_d");
			generateVarList(genPrinter, fclass.integerInputs(),                "integer_u");
			generateVarList(genPrinter, fclass.enumInputs(),                   "integer_u");
			generateVarList(genPrinter, fclass.discreteBooleanVariables(),     "boolean_d");
			generateVarList(genPrinter, fclass.booleanInputs(),                "boolean_u");
		}
	}
	
	/**
	 * Generates headers for Modelica functions.
	 */
	class DAETag_C_function_headers extends DAETag {
		
		public DAETag_C_function_headers(AbstractGenerator myGenerator, FClass fclass) {
			super("C_function_headers","C: C function headers representing Modelica functions",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FFunctionDecl func : fclass.getFFunctionDecls())
				func.genHeader_C(genPrinter, "");
		}
	}
	
	/**
	 * Generates definitions for Modelica functions.
	 */
	class DAETag_C_functions extends DAETag {
		
		public DAETag_C_functions(AbstractGenerator myGenerator, FClass fclass) {
			super("C_functions","C: C functions representing Modelica functions",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FFunctionDecl func : fclass.getFFunctionDecls())
				func.prettyPrint_C(genPrinter, "");
		}
	}
	
	/**
	 * Generates structs for Modelica records.
	 */
	class DAETag_C_records extends DAETag {
		
		public DAETag_C_records(AbstractGenerator myGenerator, FClass fclass) {
			super("C_records","C: C structs representing Modelica records",
			  myGenerator,fclass);
		}

		public void generate(PrintStream genPrinter) {
			for (FRecordDecl rec : fclass.getFRecordDecls())
				rec.prettyPrint_C(genPrinter, "");
		}
	}
	
	
	/**
	 * \brief Returns the string denoting the beginning of the copyright blurb.
	 */
	protected String startOfBlurb() { return "/*"; }
	
	/**
	 * Constructor.
	 * 
	 * @param expPrinter Printer object used to generate code for expressions.
	 * @param escapeCharacter Escape characters used to decode tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public CGenerator(Printer expPrinter, char escapeCharacter,
			FClass fclass) {
		super(expPrinter, escapeCharacter, fclass);
	}

}

