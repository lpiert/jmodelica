<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Changes and Additions to CppAD During 2010</title>
<meta name="description" id="description" content="Changes and Additions to CppAD During 2010"/>
<meta name="keywords" id="keywords" content=" "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_whats_new_10_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="whats_new.xml" target="_top">Prev</a>
</td><td><a href="whats_new_09.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>whats_new</option>
<option>whats_new_10</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>whats_new-&gt;</option>
<option>whats_new_10</option>
<option>whats_new_09</option>
<option>whats_new_08</option>
<option>whats_new_07</option>
<option>whats_new_06</option>
<option>whats_new_05</option>
<option>whats_new_04</option>
<option>whats_new_03</option>
</select>
</td>
<td>whats_new_10</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Introduction</option>
<option>03-11</option>
<option>03-10</option>
<option>03-09</option>
<option>03-03</option>
<option>02-11</option>
<option>02-08</option>
<option>02-06</option>
<option>02-05</option>
<option>02-03</option>
<option>01-26</option>
<option>01-24</option>
<option>01-23</option>
<option>01-20</option>
<option>01-18</option>
<option>01-04</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>Changes and Additions to CppAD During 2010</big></big></b></center>
<br/>
<b><big><a name="Introduction" id="Introduction">Introduction</a></big></b>
<br/>
This section contains a list of the changes to CppAD during 2010
(in reverse order by date).
The purpose of this section is to
assist you in learning about changes between various versions of CppAD.

<br/>
<br/>
<b><big><a name="03-11" id="03-11">03-11</a></big></b>
<br/>
The old <a href="reverse_any.xml" target="_top"><span style='white-space: nowrap'>reverse_any</span></a>
 example was moved to <a href="reverse_three.cpp.xml" target="_top"><span style='white-space: nowrap'>reverse_three.cpp</span></a>
,
the old checkpoint example is now the general case reverse example,
and a better <a href="checkpoint.cpp.xml" target="_top"><span style='white-space: nowrap'>checkpointing</span></a>
 example was created.

<br/>
<br/>
<b><big><a name="03-10" id="03-10">03-10</a></big></b>
<br/>
The <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 routine would mistakenly remove some
expressions that depended on the independent variables and that 
affected the result of certain <a href="condexp.xml" target="_top"><span style='white-space: nowrap'>CondExp</span></a>
 operations.
This has been fixed. 

<br/>
<br/>
<b><big><a name="03-09" id="03-09">03-09</a></big></b>
<br/>
Extend <a href="reverse_any.xml" target="_top"><span style='white-space: nowrap'>reverse_any</span></a>
 so that it can be used for 
<a href="checkpoint.cpp.xml" target="_top"><span style='white-space: nowrap'>checkpointing</span></a>
; i.e.,
splitting reverse mode calculations at function composition points.


<br/>
<br/>
<b><big><a name="03-03" id="03-03">03-03</a></big></b>
<br/>
Fixed a bug in handling
<a href="glossary.xml#Sparsity Pattern.Vector of Boolean" target="_top"><span style='white-space: nowrap'>vector&#xA0;of&#xA0;boolean</span></a>
 
sparsity patterns.
(when the number of elements per set was a multiple of
<code><font color="blue">sizeof(size_t))</font></code>.

<br/>
<br/>
<b><big><a name="02-11" id="02-11">02-11</a></big></b>
<br/>
The <code><font color="blue">speed</font></code> directory has been reorganized and the
common part of the <a href="speed_main.xml#Link Functions" target="_top"><span style='white-space: nowrap'>link&#xA0;functions</span></a>
,
as well as the <a href="microsoft_timer.xml" target="_top"><span style='white-space: nowrap'>microsoft_timer</span></a>
,
have been moved to the subdirectory <code><font color="blue">speed/src</font></code>
where a library is built.

<br/>
<br/>
<b><big><a name="02-08" id="02-08">02-08</a></big></b>
<br/>
A bug was introduced in the <a href="whats_new_10.xml#02-05" target="_top"><span style='white-space: nowrap'>02-05</span></a>

change whereby the <code><font color="blue">make</font></code> command tried to build the
<code><font color="blue">libcppad_ipopt.a</font></code> library even if <code><font color="blue">IPOPT_DIR</font></code> was
not specified on the <a href="installunix.xml#Configure" target="_top"><span style='white-space: nowrap'>configure</span></a>
 command line.
This has been fixed.

<br/>
<br/>
<b><big><a name="02-06" id="02-06">02-06</a></big></b>
<br/>
The Microsoft project files for the speed tests were extended 
so that the worked properly for the Release (as well as the Debug)
configuration, see
<a href="installwindows.xml#CppAD Speed Test" target="_top"><span style='white-space: nowrap'>cppad&#xA0;speed&#xA0;test</span></a>
,
<a href="installwindows.xml#Double Speed Test" target="_top"><span style='white-space: nowrap'>double&#xA0;speed&#xA0;test</span></a>
, and
<a href="installwindows.xml#Speed Utility Example" target="_top"><span style='white-space: nowrap'>speed&#xA0;utility&#xA0;example</span></a>
.
(This required conversion from Visual Studio Version 7 to Visual Studio 2008
format.)
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>Add an automated check for <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 bug 
fixed on <code><font color="blue">02-05</font></code>. 
(Automatic checking for <a href="printfor.xml" target="_top"><span style='white-space: nowrap'>PrintFor</span></a>
 bug was added on <code><font color="blue">02-05</font></code>.)

<br/>
<br/>
<b><big><a name="02-05" id="02-05">02-05</a></big></b>

<ol type="1"><li>
Simplify running all the tests by adding the
<a href="installunix.xml#make test" target="_top"><span style='white-space: nowrap'>make&#xA0;test</span></a>
 command.
</li><li>

Simplify the <a href="installunix.xml#Configure" target="_top"><span style='white-space: nowrap'>configure</span></a>
 command by removing
need for:
<code><font color="blue">--with-Speed</font></code>,
<code><font color="blue">--with-Introduction</font></code>,
<code><font color="blue">--with-Example</font></code>,
<code><font color="blue">--with-TestMore</font></code>,
and <code><font color="blue">--with-PrintFor</font></code>.
</li><li>

Add files that were missing in the Microsoft Visual Studio
projects; see <a href="installwindows.xml" target="_top"><span style='white-space: nowrap'>InstallWindows</span></a>
.
</li><li>

Fix two significant bugs. One in the <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 command
and the other in the <a href="printfor.xml" target="_top"><span style='white-space: nowrap'>PrintFor</span></a>
 command.
</li></ol>


<br/>
<br/>
<b><big><a name="02-03" id="02-03">02-03</a></big></b>
<br/>
Fix a mistakes in the test <a href="benderquad.cpp.xml" target="_top"><span style='white-space: nowrap'>BenderQuad.cpp</span></a>
.
In addition, the <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 routine was used to reduce the 
tape before doing the calculations.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>The routine <a href="opt_val_hes.xml" target="_top"><span style='white-space: nowrap'>opt_val_hes</span></a>
 was added as an alternative to
<a href="benderquad.xml" target="_top"><span style='white-space: nowrap'>BenderQuad</span></a>
.

<br/>
<br/>
<b><big><a name="01-26" id="01-26">01-26</a></big></b>
<br/>
Another speed improvement was made to <a href="cppad_ipopt_nlp.xml" target="_top"><span style='white-space: nowrap'>cppad_ipopt_nlp</span></a>
.
To be specific, the Lagragian multipliers where checked and components
that were zero were excluded form the Hessian evaluation.

<br/>
<br/>
<b><big><a name="01-24" id="01-24">01-24</a></big></b>
<br/>
It appears that in <a href="cppad_ipopt_nlp.xml" target="_top"><span style='white-space: nowrap'>cppad_ipopt_nlp</span></a>
, when <code><font color="blue">retape[k]</font></code> was true,
and <code><font color="blue">L[k] &gt; 1</font></code>, it was possible to use the wrong operation sequence
in the calculations (though a test case that demonstrated this could not be 
produced). This is because the argument value to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msub><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msub>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>u</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 depends
on the value of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mo stretchy="false">&#x02113;</mo>
</mrow></math>

 in the expression

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>r</mi>
<mi mathvariant='italic'>k</mi>
</msub>
<mo stretchy="false">&#x02218;</mo>
<mo stretchy="false">[</mo>
<msub><mi mathvariant='italic'>J</mi>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02113;</mo>
</mrow>
</msub>
<mo stretchy="false">&#x02297;</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

(even when the value of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>x</mi>
</mrow></math>

 does not change).
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>There was a bug in the <a href="ipopt_ode_check.cpp.xml" target="_top"><span style='white-space: nowrap'>ipopt_ode_check.cpp</span></a>
 program,
for a long time,  that did not show up until now. Basically,
the check had code of the was using an undefined value.
This has been fixed.

<br/>
<br/>
<b><big><a name="01-23" id="01-23">01-23</a></big></b>
<br/>
Improve the sparsity patterns and reduce the amount of memory
required for large sparse problems using <a href="cppad_ipopt_nlp.xml" target="_top"><span style='white-space: nowrap'>cppad_ipopt_nlp</span></a>
.
The speed test <code><font color="blue">cppad_ipopt/speed</font></code> showed significant improvement.


<br/>
<br/>
<b><big><a name="01-20" id="01-20">01-20</a></big></b>
<br/>
We plan to split up the 
<code><font color="blue">ipopt_cppad/src/ipopt_cppad_nlp.hpp</font></code> include file.
In preparation,
the example <code><font color="blue">ipopt_cppad</font></code> has been changed to <code><font color="blue">cppad_ipopt</font></code>.
This will facilitate using <code><font color="blue">CPPAD_IPOPT_*</font></code> for the
<code><font color="blue"># ifdef</font></code> commands in the new include files
(note that they must begin with <code><font color="blue">CPPAD</font></code>).


<br/>
<br/>
<b><big><a name="01-18" id="01-18">01-18</a></big></b>
<br/>
The <code><font color="blue">ipopt_cppad</font></code> subdirectory of the distribution
has been split into an <code><font color="blue">src</font></code>, <code><font color="blue">example</font></code>, and <code><font color="blue">speed</font></code>
subdirectories.
The <code><font color="blue">example</font></code> (<code><font color="blue">speed</font></code>)
subdirectory is where one builds the <a href="cppad_ipopt_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
 examples
(<a href="ipopt_ode_speed.cpp.xml" target="_top"><span style='white-space: nowrap'>speed&#xA0;tests</span></a>
).

<br/>
<br/>
<b><big><a name="01-04" id="01-04">01-04</a></big></b>
<br/>
The following items have been fulfilled and
hence were removed for the <a href="wishlist.xml" target="_top"><span style='white-space: nowrap'>WishList</span></a>
:
<ol type="1"><li>
If an exception occurs before the call to the corresponding
<a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 constructor or <a href="dependent.xml" target="_top"><span style='white-space: nowrap'>Dependent</span></a>
, 
the tape recording can be stopped using <a href="abort_recording.xml" target="_top"><span style='white-space: nowrap'>abort_recording</span></a>
.

</li><li>

A speed test for <a href="cppad_ipopt_nlp.xml" target="_top"><span style='white-space: nowrap'>ipopt_cppad_nlp</span></a>
 was added; see
<a href="ipopt_ode_speed.cpp.xml" target="_top"><span style='white-space: nowrap'>ipopt_ode_speed.cpp</span></a>
.

</li><li>

The <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 command uses hash coding to check
when an expression is already in the tape and can be reused.

</li><li>

The <a href="optimize.xml" target="_top"><span style='white-space: nowrap'>optimize</span></a>
 command removes operations that
are not used by any of the dependent variables.

</li><li>

The <a href="ad_in_c.cpp.xml" target="_top"><span style='white-space: nowrap'>ad_in_c.cpp</span></a>
 example demonstrates how to connect
CppAD to an arbitrary scripting language.

</li><li>

The vector of sets option has been added to sparsity calculations; 
see <a href="glossary.xml#Sparsity Pattern" target="_top"><span style='white-space: nowrap'>sparsity&#xA0;pattern</span></a>
.

</li></ol>




<hr/>Input File: omh/whats_new_10.omh

</body>
</html>
