/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Arrays;
import java.util.Collections;

aspect FlatTypeCheck {
	
    public void ASTNode.typeCheck() {}
 
    public void InstAssignable.typeCheck() {
    	FExp bexp = myBindingInstExp();
		boolean func = inFunction();
    	if (!size().isComplete() && !func && !inRecordDecl()) 
    		error("Can not infer array size of the variable " + name());
    	if (bexp != null && !inRecordWithBindingExp() && !bexp.type().isUnknown()) {
    		if (myBindingExpHasEach() && !inArrayComponent()) {
    			error("The 'each' keyword cannot be applied to members of non-array components");
     		} else {
				FType expectedBT = bindingType();
				FType actualBT = bexp.type();
    			if (!expectedBT.typeCompatible(actualBT, func) && !expectedBT.isUnknown()) {
    				if (!myBindingExpHasEach() && type().typeCompatible(actualBT, func)) {
						warning("Assuming 'each' for the binding expression to the variable " + name());
    				} else if (!expectedBT.scalarType().typeCompatible(actualBT.scalarType())) {
    					error("The binding expression of the variable " + name() +
    							" does not match the declared type of the variable");
    				} else {
    					error("Array size mismatch in declaration of " + 
    							(myBindingExpHasEach() ? "each " : "") + name() +
    							", size of declaration is " + expectedBT.size() + 
    							" and size of binding expression is " + actualBT.size());
     				}
    			}
    		}
	    }
    	if (hasConditionalAttribute()) {
    		if (!getConditionalAttribute().type().isScalar()) {
    			error ("The guard expression of a conditional component should be a scalar expression");
    		}
    		if (!getConditionalAttribute().type().isBoolean()) {
    			error ("The guard expression of a conditional component should be a boolean expression");
    		}
    		if (!getConditionalAttribute().variability().lessOrEqual(fParameter())) {
    			error ("The guard expression of a conditional component should have parameter or constant variability");
    		}
    	}
	  
    	typeCheckAttributes();
    }
    
    inh boolean InstComponentDecl.inRecordWithBindingExp();
    eq InstRecord.getChild().inRecordWithBindingExp()             = myBindingInstExp() != null;
    eq InstArrayComponentDecl.getChild().inRecordWithBindingExp() = inRecordWithBindingExp();
    eq InstNode.getChild().inRecordWithBindingExp()               = false;
    
    public void InstAssignable.typeCheckAttributes() {}
    
    public void InstPrimitive.typeCheckAttributes() {
	    for (InstModification im : totalMergedEnvironment()) 
	    	im.typeCheckAttribute(this);
     }
   
    public void InstModification.typeCheckAttribute(InstNode owner) {}

    public void InstComponentModification.typeCheckAttribute(InstNode owner) {
    	if (hasInstModification() && getInstModification().hasInstValueMod()) {
			FExp val_mod = getInstModification().instValueMod();
			if (!type().isUnknown() && !val_mod.type().isUnknown()) {
				FType attr_type = owner.attributeType(this);
				if (!type().typeCompatible(val_mod.type().scalarType())) {
					error("The type of the binding expression of the attribute " + name() +
					  " does not match the declared type of the variable");
				} else if (!getEach() && !attr_type.dimensionCompatible(val_mod.type())) { 
					if (val_mod.ndims() == 0) {
						warning("Assuming 'each' for the attribute " + name());
					} else {
						error("Array size mismatch for the attribute " + name() +
								", size of declaration is " + attr_type.size() + 
								" and size of " + name() + " expression is " + val_mod.size());
					}
				} else if (getEach()) { 
					if (owner.ndims() == 0) {
						error("The 'each' keyword cannot be applied to attributes of scalar components");
					} else if (val_mod.ndims() > 0) {
						error("The attribute " + name() + 
								" is declared 'each' and the binding expression is not scalar");
					}
				}
			}
    	}
    }
    
  public void FEquation.typeCheck() {
	  //log.debug("FEquation.typeCheck(): " + getLeft().type() + " " + getRight().type());
	  if (!getLeft().type().isUnknown() && !getRight().type().isUnknown()) {
		  if (!getLeft().type().equivalentTo(getRight().type())) {
			  error("The right and left expression types of equation are" +
		  			" not compatible");		  
		  }
	  }
  }
  
  // Generic typeCheck() that calls typeError() if type is unknown and no FExp child has unknown type
  public void FExp.typeCheck() {
	  if (type().isUnknown()) {
		  for (FExp exp : childFExps())
			  if (exp.type().isUnknown())
				  return;
		  typeError();
	  }
  }
  
  protected static boolean FExp.isUnknownFExp(ASTNode n) {
	  return n instanceof FExp && ((FExp) n).type().isUnknown();
  }
  
  // TODO: Add specialized typeError() methods that gives better error messages
  
  public void FExp.typeError() {
	  error("Type error in expression: " + this);	  
  }

  public void FInstAccessExp.typeError() {
	  getInstAccess().typeError();
  }
  
  public void InstAccess.typeError() {
	  typeError(this);
  }
  
  protected void InstAccess.typeError(InstAccess top) {}
  
  protected void InstDot.typeError(InstAccess top) {
	  getRight().typeError(top);
  }
  
  protected void InstGlobalAccess.typeError(InstAccess top) {
	  getInstAccess().typeError(top);
  }
  
  protected void InstComponentAccess.typeError(InstAccess top) {
	  if (!myInstComponentDecl().isUnknown())
		  top.error("Accesses to composite components other than records are not allowed: " + top.name());
  }
  
  protected void InstClassAccess.typeError(InstAccess top) {
	  if (!isComponentSizeClass() || !isInstComponentSize())
		  top.error("Illegal access to class in expression: " + top.name());
  }
  
  public void FIdUseExp.typeCheck() {
	  if (!getFIdUse().isUnknown() && type().isUnknown())
		  typeError();
  }
  public void FInstAccessExp.typeCheck() {
	  if (!getInstAccess().isUnknown() && type().isUnknown())
		  typeError();
  }

  /**
   * \brief Check if the FExp of this FIterExp must be scalar.
   */
  inh boolean FIterExp.iterExpMustBeScalar();
  eq FExp.getChild().iterExpMustBeScalar()       = false;
  eq Root.getChild().iterExpMustBeScalar()       = false;
  eq InstNode.getChild().iterExpMustBeScalar()   = false;
  eq FMinMaxExp.getChild().iterExpMustBeScalar() = true;
  // TODO: Add product() when it is implemented
  
  public void FIterExp.typeCheck() {
	  super.typeCheck();
	  if (iterExpMustBeScalar() && !getFExp().type().isUnknown() && getFExp().ndims() != 0)
		  error("The expression of a reduction-expression must be scalar, except for sum(): " +
				  getFExp() + " has " + getFExp().ndims() + " dimension(s)");
  }
  
  public void FStreamBuiltIn.typeCheck() {
	  if (!getFExp().isAccessToStream()) 
		  error("Argument of " + builtInName() + "() must be a stream variable");
  }
  
  syn boolean FExp.isAccess()  = false;
  eq FIdUseExp.isAccess()      = true;
  eq FInstAccessExp.isAccess() = true;
  
  syn boolean FExp.isAccessToStream()  = false;
  eq FIdUseExp.isAccessToStream()      = getFIdUse().isAccessToStream();
  eq FInstAccessExp.isAccessToStream() = getInstAccess().isAccessToStream();
  
  syn boolean FIdUse.isAccessToStream()  = false;
  eq FIdUseInstAccess.isAccessToStream() = getInstAccess().isAccessToStream();
  
  syn boolean InstAccess.isAccessToStream() = myInstComponentDecl().getComponentDecl().isStream();
  
  
  public void InstForIndex.typeCheck() {
	  if (hasFExp() && !getFExp().type().isUnknown() && getFExp().ndims() != 1)
		  error("The expression of for index " + name() + " must be a vector expression: " + 
				  getFExp() + " has " + getFExp().ndims() + " dimension(s)");
  }
  
  public void FExpSubscript.typeCheckAsSize() {
	  if (ndims() > 0) { 
		  error("Array size must be scalar expression: " + getFExp());
	  } else if (!type().isInteger()) { // TODO: Expand to support Boolean and enums
		  if (getFExp().canBeComponentSize())
			  compliance("Array sizes of Boolean or enumeration type are not supported: " + getFExp());
		  else if (!type().isUnknown())
			  error("Array size must be Integer expression: " + getFExp());
	  } else if (!inFunction()) { 
		  if (!getFExp().variability().lessOrEqual(fParameter())) {
			  error("Array size must be constant or parameter: " + getFExp());
		  } else {
			  try {
				  getFExp().ceval().intValue();
			  } catch (ConstantEvaluationException e) {
				  error("Could not evaluate array size expression: " + getFExp());
			  }
		  }
	  }
  }
  
  public void FExpSubscript.typeCheckAsIndex() {
	  if (ndims() > 1) { 
		  error("Array index must be scalar or vector expression: " + getFExp());
	  } else if (!type().isInteger()) { // TODO: Expand to support Boolean and enums
		  if (type().canBeIndex())
			  compliance("Array indices of Boolean or enumeration type not supported: " + getFExp());
		  else if (!type().isUnknown())
			  error("Array index must be Integer expression: " + getFExp());
	  } else if (!inAlgorithmOrFunction()) { 
		  if (!getFExp().variability().lessOrEqual(fParameter())) {
			  error("Array index in equation must be constant, parameter or loop index: " + getFExp());
		  } else { 
			  // Check array bounds
			  // TODO: Perform bounds check in functions if index has parameter variability or lower?
			  try {
				  int max = mySize().get(0);
				  for (FExp e : getExpanded().iterable()) {
					  int i = e.ceval().intValue();
					  if (i < 1 || i > max) {
						  error("Array index out of bounds: " + i + ", index expression: " + getFExp());
						  return;
					  }
				  }
			  } catch (ConstantEvaluationException e) {
				  error("Could not evaluate array index expression: " + getFExp());
			  }
		  }
	  }
  }
  
  public void FExpSubscript.typeCheck() {
	  if (isInstComponentSize()) 
		  typeCheckAsSize();
	  else 
		  typeCheckAsIndex();
  }
  
  syn boolean FExp.canBeComponentSize()  = type().isInteger();
  eq FInstAccessExp.canBeComponentSize() = type().isInteger() || getInstAccess().isComponentSizeClass();
  eq FIdUseExp.canBeComponentSize()      = type().isInteger() || getFIdUse().isComponentSizeClass();
  
  syn boolean InstAccess.isComponentSizeClass() = myInstClassDecl().isComponentSizeClass();
  syn boolean FIdUse.isComponentSizeClass()     = false;
  eq FIdUseInstAccess.isComponentSizeClass()    = getInstAccess().isComponentSizeClass();
  
  syn boolean InstClassDecl.isComponentSizeClass() = isBooleanClass();
  eq InstEnumClassDecl.isComponentSizeClass()      = true;
  
  inh boolean InstClassAccess.isInstComponentSize();
  inh boolean FInstAccessExp.isInstComponentSize();
  inh boolean FExpSubscript.isInstComponentSize();
  inh boolean FArraySubscripts.isInstComponentSize();
  eq InstComponentDecl.getLocalFArraySubscripts().isInstComponentSize() = true;
  eq InstComponentDecl.getFArraySubscripts().isInstComponentSize()      = true;
  eq InstShortClassDecl.getFArraySubscripts().isInstComponentSize()     = true;
  eq InstAccess.getChild().isInstComponentSize()                        = false;
  eq FQNamePart.getChild().isInstComponentSize()                        = false;
  eq FIdUse.getChild().isInstComponentSize()                            = false;
  eq FExp.getChild().isInstComponentSize()                              = false;
  eq FInstAccessExp.getChild().isInstComponentSize()                    = isInstComponentSize();
  eq AbstractFVariable.getChild().isInstComponentSize()                 = false;
  eq FAbstractEquation.getChild().isInstComponentSize()                 = false;
  eq InstNode.getChild().isInstComponentSize()                          = false;
  eq FlatRoot.getChild().isInstComponentSize()                          = false;
	
  
  public void InstComponentAccess.typeCheck() {
	//  log.debug("InstComponentAccess.typeCheck: " + printSlice());
  }
  
  
  public void InstAssignStmt.typeCheck() {
	  if (!getLeft().type().isUnknown() && !getRight().type().isUnknown()) {
		  if (getLeft().isForIndex())
			  error("Can not assign a value to a for loop index");
		  else if (!getLeft().type().typeCompatible(getRight().type(), true)) 
			  error("Types of right and left side of assignment are not compatible");		  
	  }
  }
  
  public void FWhileStmt.typeCheck() {
	  if (!getTest().type().isUnknown() && !fBooleanScalarType().typeCompatible(getTest().type())) 
		  error("Type of test expression of while statement is not Boolean");
  }
  
  public void FIfClause.typeCheck() {
	  if (!getTest().type().isUnknown() && !fBooleanScalarType().typeCompatible(getTest().type())) 
		  error("Type of test expression of if statement is not Boolean");
  }
  
  public void FWhenClause.typeCheck() {
	  if (!getTest().type().isUnknown()) {
		  if (!getTest().type().isBoolean() || getTest().type().ndims() > 1) 
			  error("Test expression of when statement isn't Boolean scalar or vector expression");
	  }
  }
  
  public void FIfEquation.typeCheck() {
	  if (!getTest().type().isUnknown() && !fBooleanScalarType().typeCompatible(getTest().type())) 
		  error("Type of test expression of if equation is not Boolean");
  }
  
  public void FWhenEquation.typeCheck() {
	  if (!getTest().type().isUnknown()) {
		  if (!getTest().type().isBoolean() || getTest().type().ndims() > 1) 
			  error("Test expression of when equation isn't Boolean scalar or vector expression");
	  }
  }
  
  
  public void FInfArgsFunctionCall.typeCheck() {
      super.typeCheck();
      if (getNumOriginalArg() < minNumArgs()) 
    	  error("Too few arguments to " + builtInName() + "(), must have at least " + minNumArgs());
      else 
    	  typeCheckFExps();
  }
  
  public void FInfArgsFunctionCall.typeCheckFExps() {
      for (FExp exp : getFExps()) {
          if (!exp.type().isUnknown() && !typeOfArgIsOK(exp)) {
              exp.error("Argument of " + builtInName() + "() is not " + 
            		  getOKArgTypeString() + ": " + exp);
          } else if (!variabilityOfArgIsOK(exp)) {
        	  exp.error("Argument of " + builtInName() + "() does not have " + 
        			  getOKArgVariabilityString() + " variability: " + exp);
          }
      }
  }
  
  public void FCatExp.typeCheckFExps() {
	  if (!getDim().type().isUnknown() && !fIntegerScalarType().typeCompatible(getDim().type()))
		  error("Dimension argument of cat() is not compatible with Integer: " + getDim());
	  else if (!getDim().type().isUnknown() && !getDim().variability().lessOrEqual(fParameter()))
		  error("Dimension argument of cat() does not have constant or parameter variability: " + getDim());
  }
  
  public void FAbstractCat.typeCheckFExps() {}
  
  public void FAbstractCat.typeError() {
	  error("Types do not match in array concatenation");	  
  }
  
  syn int FInfArgsFunctionCall.minNumArgs() = 1;
  eq FFillExp.minNumArgs()     = 2;
  eq FCatExp.minNumArgs()      = 2;
  eq FAbstractCat.minNumArgs() = 0;
  
  syn boolean FInfArgsFunctionCall.typeOfArgIsOK(FExp exp)        = fIntegerScalarType().typeCompatible(exp.type());
  syn boolean FInfArgsFunctionCall.variabilityOfArgIsOK(FExp exp) = exp.variability().lessOrEqual(fParameter());
  syn String FInfArgsFunctionCall.getOKArgTypeString()            = "compatible with Integer";
  syn String FInfArgsFunctionCall.getOKArgVariabilityString()     = "constant or parameter";
   
  public void FBuiltInFunctionCall.typeCheck() {
	  if (checkTypeAsExpression())
		  super.typeCheck();
	  int n = builtInHasOutput() ? 1 : 0;
	  if (myLefts().size() > n)
		  error("Too many components assigned from function call: " + builtInName() + 
				  "() has " + n + " output(s)");
	  else if (!isFunctionCallClause() && n == 0)
		  error("Function " + builtInName() + "() has no outputs, but is used in expression");
  }
  
  public void FEnumIntegerExp.typeCheck() {
	  super.typeCheck();
	  // We can't define the function Integer in PredefinedTypes.jrag - conflict with the type Integer
	  int n = getNumOriginalArg();
	  if (n != 1)
		  error("Calling function Integer(): too " + (n > 1 ? "many" : "few") + " arguments");
  }
  
  public void FPreExp.typeCheck() {
  	if (!getFExp().isAccess()) {
  		error("Calling function pre(): argument must be variable access");
  		return;  		
  	}
    if ((getFExp().isContinuousExp() && !getFExp().inWhen())) {
    	error("Calling built-in operator pre() with a continuous variable access as an argument can only be done inside when cluases");
    }     
  }
  
  syn boolean FBuiltInFunctionCall.builtInHasOutput() = true;
  eq FIgnoredBuiltIn.builtInHasOutput() = false;
  
  syn boolean FBuiltInFunctionCall.checkTypeAsExpression() = false;
  eq FSizeExp.checkTypeAsExpression()     = true;
  eq FMinMaxExp.checkTypeAsExpression()   = true;
  eq FIdentity.checkTypeAsExpression()    = true;
  eq FLinspace.checkTypeAsExpression()    = true;
  eq FAbstractCat.checkTypeAsExpression() = dimensionIsOk();
  
  syn boolean FAbstractCat.dimensionIsOk(); 
  eq FCatExp.dimensionIsOk()    = getDim().variability().lessOrEqual(fParameter()) && 
  								  getDim().type().isInteger() && getDim().type().isScalar();
  eq FMatrix.dimensionIsOk()    = true;
  eq FMatrixRow.dimensionIsOk() = true;
 
  public void InstFunctionCall.typeCheck() {
	  if (!isFunctionCallClause() && !getName().myInstClassDecl().isRecord() && !hasOutputs())
		  error("Function " + getName().name() + "() has no outputs, but is used in expression");
	  else if (!isFunctionCallClause() && !inFunction() && size().isUnknown())
		  error("Could not evaluate array size of output " + expOutput().name());
	  if (myOutputs().size() < myLefts().size())
		  error("Too many components assigned from function call: " + getName().name() + 
				  "() has " + myOutputs().size() + " output(s)");
  }
  
  public void FFunctionCallLeft.typeCheck() {
	  if (hasFExp() && !myOutput().isUnknown() && !type().isUnknown()) { // Avoid duplicate error
		  if (getFExp() instanceof FIdUseExp) {  // Should never be false - add check? 
			  FIdUse use = ((FIdUseExp) getFExp()).getFIdUse();
			  if (use.isForIndex())
				  error("Can not assign a value to a for loop index");
			  else if (size().isUnknown())
				  error(functionCallDecription() + ":could not evaluate array size of output " + 
						  myOutput().name());
			  else if (!use.type().typeCompatible(type())) 
				  error(functionCallDecription() + ": types of component " + use.name() + 
						  " and output " + myOutput().name() + " are not compatible");
		  }
	  }
  }
  
  inh String InstFunctionArgument.functionCallDecription();
  inh String FFunctionCallLeft.functionCallDecription();
  eq FAbstractFunctionCall.getChild().functionCallDecription() = functionCallDecription();
  eq FFunctionCallEquation.getChild().functionCallDecription() = getCall().functionCallDecription();
  eq FFunctionCallStmt.getChild().functionCallDecription()     = getCall().functionCallDecription();
  
  public void InstFunctionArgument.typeCheck() {
	  if (!getFExp().type().isUnknown() && (getBoundInput() == null || !getBoundInput().type().isUnknown())) {
		  FType type = getFExp().type();
		  boolean typeOk;
		  if (argumentDefinedTypeValid()) 
			  typeOk = getBoundInput().type().typeCompatible(type, true);		  
		  else
			  typeOk = argumentTypeValid(type);
		  if (!typeOk) {
			  error(functionCallDecription() + ": types of " + argumentDesc() + 
					  " and input " + getBoundInput().name() + " are not compatible");
		  }
	  }
  }
  
  public void InstMissingArgument.typeCheck() {
	  error(functionCallDecription() + ": missing argument for required input " + getBoundInput().name());
  }
  
  public void InstMultipleBoundArgument.typeCheck() {
	  error(functionCallDecription() + ": multiple arguments matches input " + getBoundInput().name());
  }
  
  public void InstDefaultArgument.typeCheck() {}
  
  protected String InstFunctionArgument.argumentDesc() { return null; }
  
  protected String InstPositionalArgument.argumentDesc() {
	  return "positional argument " + (getPos() + 1);
  }
  
  protected String InstNamedArgument.argumentDesc() {
	  return "named argument " + getName().prettyPrint("");
  }
  
  public void InstAccess.typeCheck() {
//	  if (getTopInstAccess() == this && !type().isUnknown()) {
		  // TODO: if this is a slice, check that all branches are of equal size
//	  }
  }
  
  public void InstDot.typeCheck() {
	  // Check for accesses to non-constant component in class
	  getLeft().checkAccessCompInClassLeft(getRight());
	  super.typeCheck();
  }
  
  public void InstAccess.checkAccessCompInClassLeft(InstAccess right) {}
  
  public void InstDot.checkAccessCompInClassLeft(InstAccess right) {
	  getRight().checkAccessCompInClassLeft(right);
  }
  
  public void InstClassAccess.checkAccessCompInClassLeft(InstAccess right) {
	  right.checkAccessCompInClassRight(myInstClassDecl());
  }
  
  public void InstComponentAccess.checkAccessCompInClassLeft(InstAccess right) {
	  if (myInstComponentDecl().isPrimitive()) {
		  InstAccess top = getTopInstAccess();
		  top.error("Can not access attribute of primitive with dot notation: " + top.name());
	  }
  }
  
  public void InstAccess.checkAccessCompInClassRight(InstClassDecl icd) {}
  
  public void InstDot.checkAccessCompInClassRight(InstClassDecl icd) {
	  getLeft().checkAccessCompInClassRight(icd);
  }
  
  public void InstComponentAccess.checkAccessCompInClassRight(InstClassDecl icd) {
	  String type = null;
	  if (icd.isPackage()) {
		  if (!myInstComponentDecl().isConstant())
			  type = "non-constant component in package";
	  } else if (icd.extendsEnum()) {
		  if (!myInstComponentDecl().isEnumLiteral())
			  type = "attribute of primitive with dot notation";
	  } else {
		  type = "component in non-package class";
	  }
	  if (type != null) {
		  InstAccess top = getTopInstAccess();
		  top.error("Can not access " + type + ": " + top.name());
	  }
  }
  
  /**
   * \brief Check if the type defined for this argument in the built-in function list 
   *        is valid.
   */
  inh boolean InstFunctionArgument.argumentDefinedTypeValid();
  eq FAbstractFunctionCall.getChild().argumentDefinedTypeValid() = true;
  eq FTranspose.getChild().argumentDefinedTypeValid()            = false;
  eq FMinMaxExp.getChild().argumentDefinedTypeValid()            = false;
  eq FSumExp.getChild().argumentDefinedTypeValid()               = false;
  eq FNdimsExp.getChild().argumentDefinedTypeValid()             = false;
  eq FSizeExp.getOriginalArg(int i).argumentDefinedTypeValid()   = i != 0;
  eq FInfArgsFunctionCall.getChild().argumentDefinedTypeValid()  = false;
  eq FEnumIntegerExp.getChild().argumentDefinedTypeValid()       = false;
  eq FVectUnaryBuiltIn.getChild().argumentDefinedTypeValid()     = false;
  eq FPreExp.getChild().argumentDefinedTypeValid()               = false;
  eq FSmoothExp.getChild(int i).argumentDefinedTypeValid()       = i != 0;

  /**
   * \brief Check if the type supplied is valid for this argument.
   * 
   * Only used if argumentDefinedTypeValid() returns <code>false</code>.
   */
  inh boolean InstFunctionArgument.argumentTypeValid(FType type);
  eq FAbstractFunctionCall.getChild().argumentTypeValid(FType type) = type.isPrimitive();
  eq FTranspose.getChild().argumentTypeValid(FType type)            = type.ndims() >= 2 && type.isPrimitive();
  eq FSumExp.getChild().argumentTypeValid(FType type)               = type.ndims() > 0 && type.hasAdd();
  eq FEnumIntegerExp.getChild().argumentTypeValid(FType type)       = type.isEnumScalar();
  eq FNoEventExp.getChild().argumentTypeValid(FType type)           = true;
  eq FAbsExp.getChild().argumentTypeValid(FType type)               = type.isNumeric();
  eq FMinMaxExp.getOriginalArg(int i).argumentTypeValid(FType type) = 
	  hasY() != getOriginalArg(i).getFExp().isArray() && type.isPrimitive();
  eq FSmoothExp.getChild().argumentTypeValid(FType type)            = type.onlyContainsReal();
  
}