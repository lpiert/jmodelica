/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.HashSet;

aspect InstLookupClasses {

	syn lazy HashSet InstAccess.lookupInstClass() circular [emptyHashSet()] = lookupInstClass(name());
	eq InstDot.lookupInstClass() = getRight().lookupInstClass();

	inh HashSet InstAccess.lookupInstClass(String name) circular [emptyHashSet()];
	inh HashSet InstNode.lookupInstClass(String name);
    inh HashSet InstModification.lookupInstClass(String name);
		
	eq InstNode.getInstClassDecl().lookupInstClass(String name)            = genericLookupInstClass(name); 
	eq InstNode.getRedeclaredInstClassDecl().lookupInstClass(String name)  = genericLookupInstClass(name); 
	eq InstNode.getInstComponentDecl().lookupInstClass(String name)        = genericLookupInstClass(name); 
	eq InstNode.getFAbstractEquation().lookupInstClass(String name)        = genericLookupInstClass(name); 
	eq InstNode.getInstExtends().lookupInstClass(String name)              = superLookupInstClass(name);
    eq InstNode.getDynamicClassName().lookupInstClass(String name)         = genericLookupInstClass(name);
                                                                           
	eq InstComponentDecl.getInstExtends().lookupInstClass(String name)     = superLookupInstClassInComponent(name);
                                                                           
    eq InstAssignable.getBindingFExp().lookupInstClass(String name)        = myInstValueMod().lookupInstClass(name);
	
    eq InstArrayComponentDecl.getChild().lookupInstClass(String name)      = lookupInstClass(name);
    
	eq InstExtends.getInstClassDecl().lookupInstClass(String name)         = genericLookupInstClassInExtends(name);	
	eq InstExtends.getInstComponentDecl().lookupInstClass(String name)     = genericLookupInstClassInExtends(name);	
	eq InstExtends.getInstExtends().lookupInstClass(String name)           = superLookupInstClassInExtends(name);	
    eq InstExtends.getDynamicClassName().lookupInstClass(String name)      = genericLookupInstClassInExtends(name);
	eq InstExtends.getFAbstractEquation().lookupInstClass(String name)     = genericLookupInstClassInExtends(name); 
	eq InstExtends.getInstClassModification().lookupInstClass(String name) = lookupInstClassFromMod(name);
	
	inh HashSet InstExtends.lookupInstClassFromMod(String name);
	eq InstNode.getInstExtends().lookupInstClassFromMod(String name)          = genericLookupInstClass(name);
	eq InstComponentDecl.getInstExtends().lookupInstClassFromMod(String name) = genericLookupInstClass(name);
	eq InstExtends.getInstExtends().lookupInstClassFromMod(String name)       = genericLookupInstClassInExtends(name);
   
    eq InstInlineExtends.getClassName().lookupInstClass(String name) = 
    	getClassName().name().equals(name) ? surroundingSuperLookupInstClass(name) : lookupInstClass(name);

	// The lexical scope of modifiers in short classes is the "outer" scope, although
	// the short class declaration is modeled as containing an extends clause
	eq InstExtendsShortClass.getInstClassModification().lookupInstClass(String name) = lookupInstClass(name);
	eq InstExtendsShortClass.getClassName().lookupInstClass(String name)             = lookupInstClassInChain(name);
	eq InstShortClassDecl.getChild().lookupInstClass(String name)                    = lookupInstClass(name);
	eq InstReplacingShortClassDecl.getChild().lookupInstClass(String name) = getInstClassRedeclare().lookupInstClass(name);
/* TODO: This makes RedeclareTests.RedeclareFunction3 fail. Shouldn't names be looked up *in* 
 *       redeclare, not *from* it? This should apply to the 3 other similar ones, but removing 
 *       them makes other tests fail. */
//	eq InstReplacingFullClassDecl.getChild().lookupInstClass(String name)  = getInstClassRedeclare().lookupInstClass(name);
	eq InstReplacingShortClassDecl.getOriginalInstClass().lookupInstClass(String name) = lookupInstClass(name);
	eq InstReplacingFullClassDecl.getOriginalInstClass().lookupInstClass(String name)  = lookupInstClass(name);
	
	eq InstReplacingComposite.getOriginalInstComponent().lookupInstClass(String name) = lookupInstClass(name);
	eq InstReplacingPrimitive.getOriginalInstComponent().lookupInstClass(String name) = lookupInstClass(name);
 	eq InstReplacingComposite.getDynamicClassName().lookupInstClass(String name) = lookupInstClass(name);
 	eq InstReplacingPrimitive.getDynamicClassName().lookupInstClass(String name) = lookupInstClass(name);
  
	eq InstReplacingComposite.getClassName().lookupInstClass(String name) = getInstComponentRedeclare().lookupInstClass(name);
	eq InstReplacingPrimitive.getClassName().lookupInstClass(String name) = getInstComponentRedeclare().lookupInstClass(name);
	
	eq InstConstrainingClass.getInstNode().lookupInstClass(String name) = getClassName().myInstClassDecl().lookupInstClass(name);

	syn HashSet InstClassDecl.lookupInstClassInInstClassRedeclare(String name) = emptyHashSet();
	eq InstReplacingShortClassDecl.lookupInstClassInInstClassRedeclare(String name) = getInstClassRedeclare().lookupInstClass(name);
	eq InstReplacingFullClassDecl.lookupInstClassInInstClassRedeclare(String name) = getInstClassRedeclare().lookupInstClass(name);

	inh HashSet InstExtendsShortClass.lookupInstClassInChain(String name);
	// Default to using normal lookup
	// TODO: don't use JastAdd internal stuff (child)
	eq InstNode.getChild().lookupInstClassInChain(String name) = ((InstNode) child).lookupInstClass(name);
	eq InstExtendsShortClass.getInstExtends().lookupInstClassInChain(String name) = 
		myInstClass().superLookupInstClassFromChain(name);
	
	syn HashSet InstClassDecl.superLookupInstClassFromChain(String name)      = superLookupInstClass(name);
	eq InstReplacingShortClassDecl.superLookupInstClassFromChain(String name) = getInstExtends(0).lookupInstClass(name);

	syn boolean InstClassDecl.isRedeclared() = false;
	eq InstReplacingShortClassDecl.isRedeclared() = true;
	eq InstReplacingFullClassDecl.isRedeclared() = true;

	eq SourceRoot.getChild().lookupInstClass(String name) = emptyHashSet();
	
	// This equation is necessary since InstAccesses may be present in FExps.
	eq FlatRoot.getChild().lookupInstClass(String name) = emptyHashSet();
	
	/**
	 * \brief Does class lookup in the superclass(es) of the class surrounding the class containing an InstInlineExtends.
	 * 
	 * Used to look up the class being extended in a "redeclare class extends" declaration.
	 */
	inh HashSet InstNode.surroundingSuperLookupInstClass(String name);
	eq SourceRoot.getChild().surroundingSuperLookupInstClass(String name) = emptyHashSet();
	eq FlatRoot.getChild().surroundingSuperLookupInstClass(String name)   = emptyHashSet();
	eq InstClassDecl.getInstExtends().surroundingSuperLookupInstClass(String name) = 
		doSurroundingSuperLookupInstClass(name);
	eq InstClassDecl.getChild().surroundingSuperLookupInstClass(String name) = 
		lookupInstClassInSuperClass(name);
	eq InstComponentDecl.getChild().surroundingSuperLookupInstClass(String name) = 
		myInstClass().doSurroundingSuperLookupInstClass(name);
	eq InstReplacingShortClassDecl.getInstExtends().surroundingSuperLookupInstClass(String name) {
		HashSet set = new HashSet(4);  // TODO: only valid for original purpose of attribute, needs rewite otherwise
		set.add(getOriginalInstClass());
		return set;
	}
	
	/**
	 * \brief Does lookup in all superclasses.
	 */
	syn lazy HashSet InstNode.lookupInstClassInSuperClass(String name) {
		HashSet set = new HashSet(4);
		for (InstExtends ie : getInstExtendss()) 
			set.addAll(ie.memberInstClass(name));
		return set.isEmpty() ? emptyHashSet() : set;
	}
	
	/**
	 * \brief Redirection method for surroundingSuperLookupInstClass().
	 * 
	 * Used to allow artificial calls to one of the cases.
	 */
	syn HashSet InstClassDecl.doSurroundingSuperLookupInstClass(String name) =
		surroundingSuperLookupInstClass(name);
	
	eq InstReplacingFullClassDecl.doSurroundingSuperLookupInstClass(String name) {
		HashSet set = new HashSet(1);
		set.add(getOriginalInstClass());
		return set;
	}
	
	syn lazy HashSet InstProgramRoot.lookupElementInstClass(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : instClassDecls()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	syn lazy HashSet InstProgramRoot.lookupInstPredefinedType(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstPredefinedTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}


	syn lazy HashSet InstProgramRoot.lookupInstBuiltInFunction(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstBuiltInFunctions()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		
		
		
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	InstFullClassDecl LibNode.instFullClassDecl = null;
	HashMap<String,LibNode> InstProgramRoot.libraryMap = 
		new HashMap<String,LibNode>();
	
	syn lazy HashSet<InstFullClassDecl> InstProgramRoot.lookupLibrary(String name) {
		HashSet<InstFullClassDecl> set = new HashSet<InstFullClassDecl>(4);
		Program prog = ((SourceRoot)root()).getProgram();
		
		LibNode ln = null;
		// Check if library is loaded
		if (libraryMap.get(name) != null) {
			ln = libraryMap.get(name);
		} else { // If not search amongst LibNodes
			int i = 0;
			for  (LibNode ln_tmp : prog.getLibNodes()) {
				if (ln_tmp.getName().equals(name)) {
					ln = ln_tmp;
					break;
				}
				i ++;
			}
			if (ln != null) {
				libraryMap.put(name,ln);
			}
		}

		if (ln != null && ln.instFullClassDecl != null) { // Lib found and loaded
			set.add(ln.instFullClassDecl);
		} else if (ln != null) { // Lib found but not loaded
			// Only need to consider one class according to spec.
			if (ln.getStoredDefinition().getNumElement() > 0) {
   	    		ClassDecl cd = (ClassDecl)ln.getStoredDefinition().getElement(0);
   	    		// Instantiate class
   			    InstNode icd = createInstClassDecl(cd);
   			    if (icd != null) {
   			    	if (icd instanceof InstFullClassDecl) {
   			    		// Add to instance tree
   			    		addInstLibClassDecl((InstFullClassDecl)icd);
   			    		// Make sure is$Final is true;
   			    		getInstLibClassDecl(getNumInstLibClassDecl()-1);
   			    		ln.instFullClassDecl = (InstFullClassDecl)icd;
   			    		set.add((InstFullClassDecl)icd);
   			    	}
    	    	}
    	    }
		}
		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstProgramRoot.getChild().lookupInstClass(String name) = genericLookupInstClass(name);
	//eq SourceRoot.getLibNode().lookupInstClass(String name) = genericLookupClass(name);
	

	eq InstProgramRoot.getInstPredefinedType().lookupInstClass(String name) {
		HashSet set = new HashSet(4);
		for (InstClassDecl bcd : getInstPredefinedTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		

		for (InstClassDecl bcd : getInstBuiltInTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}


	syn lazy  HashSet InstProgramRoot.genericLookupInstClass(String name) {
		HashSet set = new HashSet(4);
		
		if (lookupElementInstClass(name).size()>0)
			set.addAll(lookupElementInstClass(name));
		
		if (lookupInstPredefinedType(name).size()>0)
			set.addAll(lookupInstPredefinedType(name));
		
		if (lookupInstBuiltInFunction(name).size()>0)
			set.addAll(lookupInstBuiltInFunction(name));

		// TODO: propagate information about the class from where
		// the lookup is requested, so that version information
		// stored in annotations can be retreived. 
		if (set.size()==0 /* Enable shadowing of libraries */&& 
				lookupLibrary(name).size()>0)
			set.addAll(lookupLibrary(name));

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}
	
	eq InstDot.getRight().lookupInstClass(String name)                    = getLeft().qualifiedLookupInstClass(name);
	eq InstArrayAccess.getFArraySubscripts().lookupInstClass(String name) = getTopInstAccess().lookupInstClass(name);
	eq InstAccess.getExpandedSubscripts().lookupInstClass(String name)    = getTopInstAccess().lookupInstClass(name);
	
	syn HashSet InstAccess.qualifiedLookupInstClass(String name) circular [emptyHashSet()] = emptyHashSet();	
	eq InstClassAccess.qualifiedLookupInstClass(String name) = myInstClassDecl().memberInstClass(name);
	
	syn HashSet InstNode.lookupInstClassInSurrounding(String name) = lookupInstClass(name);
	eq InstReplacingFullClassDecl.lookupInstClassInSurrounding(String name) = 
		getInstClassRedeclare().lookupInstClass(name);
	eq InstReplacingShortClassDecl.lookupInstClassInSurrounding(String name) = 
		getInstClassRedeclare().lookupInstClass(name);

	syn lazy HashSet InstNode.genericLookupInstClass(String name) circular [emptyHashSet()] {
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}
		
		for (InstExtends ie : instExtends()) {
			set.addAll(ie.memberInstClass(name));
		}

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}

		if (set.size()>0)
			return set;
		else
			return lookupInstClassInSurrounding(name);
	}
	
	syn HashSet InstNode.superLookupInstClass(String name) circular [emptyHashSet()] {
		HashSet set = new HashSet();

		for (InstImport ii : instImports()) {
			set.addAll(ii.lookupInstClassInImport(name));		
		}
						
		for (InstClassDecl icd : instClassDecls()) {
			if (icd.matchInstClassDecl(name)) {
				set.add(icd);
			}
		}		
	
		if (set.size()>0)
			return set;
		else
			return lookupInstClass(name);
	}

	syn lazy HashSet InstComponentDecl.genericLookupInstClass(String name) circular [emptyHashSet()] {
		HashSet set = new HashSet(4);
		set.addAll(memberInstClass(name));

		for (InstImport ii : instImports()) 
			set.addAll(ii.lookupInstClassInImport(name));		

		if (set.size()>0)
			return set;
		else
			return myInstClass().genericLookupInstClass(name);
	}
	
	syn HashSet InstComponentDecl.superLookupInstClassInComponent(String name) circular [emptyHashSet()] {
		// TODO: Why do we do this? Seems to be needed, as tests fail without it
		if (myInstClass().isRedeclared())
			return myInstClass().lookupInstClassInInstClassRedeclare(name);
		
		HashSet set = new HashSet();

		for (InstImport ii : instImports()) 
			set.addAll(ii.lookupInstClassInImport(name));		
							
		for (InstClassDecl icd : instClassDecls()) 
			if (icd.matchInstClassDecl(name)) 
					set.add(icd);
	
		if (set.size()>0) //Found local class
			return set;
		else // else search for non-local class
			return myInstClass().superLookupInstClass(name);
	}

	
	// TODO: rewrite so that classes are only looked up in constraining types.	
	syn lazy HashSet InstExtends.genericLookupInstClassInExtends(String name) circular [emptyHashSet()] {
		HashSet set = new HashSet(4);
		
		for (InstClassDecl icd : instClassDecls()) 
			if (icd.matchInstClassDecl(name)) 
				set.add(icd);
		
		for (InstExtends ie : instExtends()) 
			set.addAll(ie.memberInstClass(name));
	
		if (set.size()>0) // Did not find any local class
			return set;
		else // continue search at myInstClass()
			return myInstClass().genericLookupInstClass(name);
	}
	
	syn HashSet InstExtends.superLookupInstClassInExtends(String name) circular [emptyHashSet()] {
		HashSet set = new HashSet();
		for (InstClassDecl icd : instClassDecls()) 
			if (icd.matchInstClassDecl(name)) 
					set.add(icd);

		if (set.size()>0) //Found local class
			return set;
		else // else search for non-local class
			return myInstClass().superLookupInstClass(name);
	}

	/**
	 * \brief Check if any constraining class/component has the given class.
	 */
	syn boolean InstNode.constrainMemberInstClass(String name) {
		if (!hasInstConstraining())
			return true;
		return getInstConstraining().getInstNode().memberInstClass(name).size() > 0;
	}
	
	syn HashSet InstNode.memberInstClass(String name) circular [emptyHashSet()] = emptyHashSet();
	
	eq InstFullClassDecl.memberInstClass(String name) {
		HashSet set = new HashSet();
		
		for (InstClassDecl icd : instClassDecls()) 
			if (icd.matchInstClassDecl(name)) 
				set.add(icd);
		
		for (InstExtends ie : instExtends()) 
			set.addAll(ie.memberInstClass(name));
		
		if (set.size() > 0 && constrainMemberInstClass(name)) 
			return set;
		return emptyHashSet();
	}
	
	eq InstShortClassDecl.memberInstClass(String name){
		HashSet set = getInstExtends(0).memberInstClass(name);		
		if (set.size() > 0 && constrainMemberInstClass(name)) 
			return set;
		return emptyHashSet();
	}
	
	eq InstExtends.memberInstClass(String name) {
		HashSet set = new HashSet();
		
		for (InstClassDecl icd : instClassDecls()) 
			if (icd.matchInstClassDecl(name)) 
				set.add(icd);
		
		for (InstExtends ie : instExtends()) 
			set.addAll(ie.memberInstClass(name));
		
		if (set.size() > 0 && constrainMemberInstClass(name)) 
			return set;
		return emptyHashSet();
	}
	
	/**
	 * Returns the InstComponentDecl representing the first cell of the array.
	 * 
	 * @param ndims  the number of dimensions of the array.
	 */
	syn InstComponentDecl InstComponentDecl.arrayCellInstComponent(int ndims) {
		if (ndims == 0) 
			return this;
		if (getNumInstComponentDecl() == 0)
			return unknownInstComponentDecl();
		return getInstComponentDecl(0).arrayCellInstComponent(ndims - 1);
	}
	
	eq InstComponentDecl.memberInstClass(String name) {
		if (isArray()) {
			return arrayCellInstComponent(ndims()).memberInstClass(name);
		} else {
			HashSet set = new HashSet();
			
			for (InstClassDecl icd : instClassDecls()) 
				if (icd.matchInstClassDecl(name)) 
					set.add(icd);
			
			for (InstExtends ie : instExtends()) 
				set.addAll(ie.memberInstClass(name));
			
			if (set.size() > 0 && constrainMemberInstClass(name)) 
				return set;
			return emptyHashSet();
		}
	}
	
	syn boolean InstClassDecl.matchInstClassDecl(String name) = false;
	eq InstBuiltInClassDecl.matchInstClassDecl(String name) = name().equals(name); 
	eq InstBaseClassDecl.matchInstClassDecl(String name) = name().equals(name);
	
	// This attribute should to be circular due to lookup in import-statements
	// If the lookup is triggered by lookup of A in 'extends A', and
	// there are import statements, evaluation might get back to the 
	// ClassAccess A when when looking up the ClassAccess to import.
	// See NameTests.ImportTest1 for an example. 
	syn lazy InstClassDecl InstAccess.myInstClassDecl() circular [unknownInstClassDecl()] {
		 return unknownInstClassDecl();
	}
	eq InstClassAccess.myInstClassDecl()  {
		HashSet set = lookupInstClass(name());
		if (set.size() > 0)
			return (InstClassDecl)set.iterator().next();
		else
			return unknownInstClassDecl();
	}
	eq InstDot.myInstClassDecl() = getRight().myInstClassDecl();
	
	syn InstClassDecl InstNode.myInstClass()  = unknownInstClassDecl(); 
	eq InstComponentDecl.myInstClass()        = getClassName().myInstClassDecl();
	//eq InstShortClassDecl.myInstClass()  = getClassName().myInstClassDecl();
	eq InstClassDecl.myInstClass()            = this;
	eq InstExtends.myInstClass()              = getClassName().myInstClassDecl();
	eq UnknownInstComponentDecl.myInstClass() = unknownInstClassDecl();	

}

aspect InstLookupImport {
	
	syn lazy InstClassDecl InstImport.getImportedClass() {
		// Use simple lookup: only classes which are not inherited can be
		// imported. 
    	return ((SourceRoot)root()).getProgram().getInstProgramRoot().
    		simpleLookupInstClassDecl(getPackageNameNoTransform().name());
	}

	syn HashSet InstImport.lookupInstClassInImport(String name) circular [emptyHashSet()] {
	    if (name.equals(name())) {	
	    	InstClassDecl icd = getImportedClass();
	    	if (!icd.isUnknown()) {
	    		HashSet set = new HashSet(4);
	    		set.add(icd);
	    		return set;
	    	} 
		}
		return emptyHashSet();
	}
	
	eq InstImportUnqualified.lookupInstClassInImport(String name)  {
        return getImportedClass().memberInstClass(name);
	}
	
}

aspect InstLocalClasses {

	syn InstClassDecl InstClassDecl.localInstClassDecl(int i) = null;
	eq InstFullClassDecl.localInstClassDecl(int i) = instClassDecls().get(i);
	
	syn int InstClassDecl.numLocalInstClassDecl() = 0;
	eq InstFullClassDecl.numLocalInstClassDecl() = instClassDecls().size();

}

aspect InstLookupClassesInModifications {

	inh HashSet InstElementModification.lookupInstClassInInstClass(String name);	

	eq InstElementModification.getName().lookupInstClass(String name) = lookupInstClassInInstClass(name);

	eq InstComponentDecl.getInstModification().lookupInstClassInInstClass(String name) = /*myInstClass().*/memberInstClass(name);		
	eq InstElementModification.getInstModification().lookupInstClassInInstClass(String name) = getName().myInstComponentDecl().memberInstClass(name);
	eq InstExtends.getInstClassModification().lookupInstClassInInstClass(String name) = /*myInstClass().*/memberInstClass(name);
	
//	eq InstShortClassDecl.getInstClassModification().lookupInstClassInInstClass(String name) = getClassName().myInstClassDecl().memberInstClass(name);
	
	inh HashSet InstClassRedeclare.lookupInstClassInInstClass(String name);
	eq InstClassRedeclare.getName().lookupInstClass(String name) = lookupInstClassInInstClass(name);
	
	/**
	 * Terminating equation for attribute lookupInstClassInInstClass.
	 */
	eq InstRoot.getChild().lookupInstClassInInstClass(String name) {return emptyHashSet();}
	eq FlatRoot.getChild().lookupInstClassInInstClass(String name) {return emptyHashSet();}
	  
}

aspect SimpleInstClassLookup {
	
	/**
	 * This method offers a simplified way of looking up qualified class names
	 * in the instance class structure.
	 * 
	 * @param name A string containing a qualified name, for example 'A.B.C'.
	 */
	syn lazy InstClassDecl InstProgramRoot.simpleLookupInstClassDecl(String name) = 
		simpleLookupInstClassDeclRecursive(name);

	// For findMatching()
	eq InstClassDecl.matches(String str) = name().equals(str);
	
	public InstClassDecl InstNode.simpleLookupInstClassDeclRecursive(String name) {
		String[] parts = name.split("\\.", 2);
	    InstClassDecl icd = findMatching(getInstClassDecls(), parts[0]);
	    if (icd != null)
	    	return icd = (parts.length == 1) ? icd : icd.simpleLookupInstClassDeclRecursive(parts[1]);
	    else
	    	return unknownInstClassDecl();
	}
	
	public InstClassDecl InstProgramRoot.simpleLookupInstClassDeclRecursive(String name) {
		String[] parts = name.split("\\.", 2);
	    InstClassDecl icd = findMatching(getInstClassDecls(), parts[0]);
	    if (icd == null) 
	        icd = findMatching(lookupLibrary(parts[0]), parts[0]);
	    if (icd != null)
	    	return icd = (parts.length == 1) ? icd : icd.simpleLookupInstClassDeclRecursive(parts[1]);
	    else
	    	return unknownInstClassDecl();
	}

	
}


aspect EnumLookup {
	
	inh HashSet InstEnumClassDecl.lookupInstBuiltInClass(String name);
	
	eq InstEnumClassDecl.getChild().lookupInstClass(String name) = lookupInstBuiltInClass(name);

	eq InstRoot.getChild().lookupInstBuiltInClass(String name) {return emptyHashSet();}
	eq FlatRoot.getChild().lookupInstBuiltInClass(String name) {return emptyHashSet();}
	
	eq InstProgramRoot.getChild().lookupInstBuiltInClass(String name) {
		HashSet set = new HashSet(4);
/*
		for (InstClassDecl bcd : getInstPredefinedTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		
*/
		for (InstClassDecl bcd : getInstBuiltInTypes()) {
			if (bcd.matchInstClassDecl(name))
				set.add(bcd);	
		}		

		if (set.size()>0)
			return set;
		else
			return emptyHashSet();
	}

	
	
}



