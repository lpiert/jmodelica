/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/** \file AbstractGenerator.java
 *  \brief AbstractGenerator class.
 */

/**
 * A package offering a basic code generation framework.
 */

import java.util.HashMap;
import java.io.*;
import java.lang.reflect.Constructor;

/**
 * Abstract base class for code generation classes. 
 * 
 * The code generation framework is based on templates and tags. A template is
 * a file containing the structure of the generated code, annotated with
 * tags. During code generation, tags are then replaced by the generated code.
 * As an example, consider the template code fragment:
 * 
 *  ...
 *  #define numEquations = $n_equations$
 *  ...
 *  
 * Here, the tag $n_equations$ corresponds to the number of equations in a 
 * model. Accordingly, the generated code would then look like, e.g.,
 * 
 *  ...
 *  #define numEquations = 22
 *  ...
 *  
 * Tags are represented in the generator by Tag objects. Each Tag contains
 * essentially its name (the string enclosed by an escape character, $ in the 
 * example above), and a method for generating the corresponding code.
 * 
 * A subclass need only define Tag classes as inner classes for them to be 
 * instantiated and added to the list of Tags.
 * 
 * When the method generate is invoked, template file is loaded and an output
 * file is written where all tags has been replaced with their corresponding
 * generated code.
 *
 */

public abstract class AbstractGenerator {
    
	/** \brief A HasMap containing all tags */
	protected HashMap<String,AbstractTag> tagMap;
	/** \brief A Printer object used to generated code for expressions */
	protected Printer expPrinter;
	/** \brief A character used as escape character when decoding tags during
	 * code generation. 
	 */
	protected int escapeCharacter;
	
	/**
	 * \brief Constructor for AbstractGenerator.
	 * 
	 * All inner classes inheriting {@link AbstractTag} in the subclass being instantited 
	 * (and in any superclasses) are automatically instantiated and added to {@link #tagMap}.
	 * 
	 * @param expPrinter A Printer for code generation of expressions.
	 * @param escapeCharacter Escape character used when decoding tags.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	public AbstractGenerator(Printer expPrinter, char escapeCharacter, FClass fclass) {
		this.expPrinter = expPrinter;
		this.escapeCharacter = (int)escapeCharacter;
		tagMap = new HashMap<String,AbstractTag>();
		instantiateTags(getClass(), fclass);
	}
	
	/**
	 * \brief Recursive method to instantiate all inner classes inheriting AbstractTag.
	 * 
	 * Recursion is needed to instantiate classes from a superclass before classes from a subclass. 
	 * This ensures that subclasses can override tags added by superclasses.
	 * 
	 * @param cur The class to search for classes in.
	 * @param fclass An FClass object used as a basis for the code generation.
	 */
	private void instantiateTags(Class<?> cur, FClass fclass) {
		if (cur.equals(AbstractGenerator.class)) return;
		instantiateTags(cur.getSuperclass(), fclass);
		for (Class<?> inner : cur.getDeclaredClasses()) {
			for (Constructor<?> con : inner.getConstructors()) {
				try {
					AbstractTag tag = (AbstractTag) con.newInstance(this, this, fclass);
					tagMap.put(tag.getName(), tag);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * \brief Method for performing code generation. 
	 * 
	 * The method performs the following steps:
	 *  
	 *   1. Load the template file
	 *   2. If the file starts with the string returned by startOfBlurb(), 
	 *      skip all characters until the string returned by endOfBlurb() is 
	 *      encountered, and all whitespace immediately following it
	 *   3. Read a character, 'c', from the template
	 *   4. If 'c' is not the escape character 
	 *   4a.  Write 'c' to the output file
	 *   4b.  Else continue to read characters until the escape character is
	 *        encountered. Build the tag name from the read characters.
	 *   4c.  Retrieve the corresponding tag
	 *   4d.  Invoke the code generation method for the tag
	 *   5.   Repeat 3 until EOF 
	 *   
	 * 
	 * @param templateFile A file containing the code generation template
	 * @param outputFile The name of the output file
	 * @throws FileNotFoundException An exception is thrown if the template
	 * file is not found.
	 */
	public void generate(String templateFile, String outputFile) 
	  throws FileNotFoundException {
		try {
			BufferedReader template = new BufferedReader(new FileReader(new File(templateFile)));
			PrintStream output = new PrintStream(new File(outputFile), "UTF-8");
			generate(template, output);
			template.close();
			output.close();
		} catch (IOException e) { // TODO: introduce specific exception type
			throw new RuntimeException("File I/O problem during code generation", e);
		}
	}
	
	/**
	 * \brief Returns the string denoting the beginning of the copyright blurb.
	 * 
	 * Must not contain the tag escape character.
	 * 
	 * Subclasses should override if their templates may contain copyright blurb.
	 */
	protected String startOfBlurb() { return null; }
	
	/**
	 * \brief Returns the string denoting the end of the copyright blurb.
	 * 
	 * Must not contain the tag escape character.
	 * 
	 * Default behavior is to return the same as startOfBlurb(), but in reverse order.
	 */
	protected String endOfBlurb() {
		String end = startOfBlurb();
		if (end == null)
			return null;
		StringBuilder buf = new StringBuilder(end);
		buf.reverse();
		return buf.toString();
	}
	
	/**
	 * \brief Internal modes for {@see #generate(Reader, PrintStream)}.
	 */
	private enum Mode { NORMAL, TAG, START, BLURB, END, END_WS, BEFORE };

	/**
	 * \brief See generate(String templateFile, String outputFile).
	 * 
	 * @param templateReader A Reader object from which the template 
	 * file can be used.
	 * @param genPrinter A PrintStream object to which the generated code
	 * is written.
	 */
	public void generate(Reader templateReader, PrintStream genPrinter) {
		Mode mode = Mode.BEFORE;

		final String startStr = startOfBlurb();
		final String endStr = endOfBlurb();
		if (startStr == null || endStr == null)
			mode = Mode.NORMAL;
		
		StringBuilder tag_name = new StringBuilder();
		int i = 0;
		boolean read = true;
		try {
			AbstractTag tag = null;
			int c = templateReader.read();
			while (c != -1) {
				switch (mode) {
				case NORMAL:
					if (c == escapeCharacter) {
						mode = Mode.TAG;
						tag_name.delete(0, tag_name.length());
					} else {
						genPrinter.print((char)c);
					}
					break;
					
				case TAG:
					if (c == escapeCharacter) {
					    mode = Mode.NORMAL;
					    String name = tag_name.toString();
						tag = tagMap.get(name);
						if (tag != null)
							tag.generate(genPrinter);
						else // TODO: introduce specific exception type
							throw new RuntimeException("Unknown tag: " + name);
					} else {
						tag_name.append((char) c);
					}
					break;
					
				case BEFORE:
					if (c == startStr.charAt(0)) {
						mode = Mode.START;
						i = 1;
					} else {
						mode = Mode.NORMAL;
						read = false;
					}
					break;
					
				case START:
					if (c == startStr.charAt(i)) {
						if (++i >= startStr.length())
							mode = Mode.BLURB;
					} else {
						mode = Mode.NORMAL;
						genPrinter.print(startStr.substring(0, i));
						read = false;
					}
					break;
					
				case BLURB:
					if (c == endStr.charAt(0)) {
						mode = Mode.END;
						i = 1;
					}
					break;
					
				case END:
					// TODO: Does not handle the case endStr = "-->", blurb ends with "--->"
					//       This can be solved with a pre-calculated prefix table
					if (c == endStr.charAt(i)) {
						if (++i >= endStr.length())
							mode = Mode.END_WS;
					} else {
						mode = Mode.BLURB;
					}
					break;
					
				case END_WS:
					if (!Character.isWhitespace((char) c)) {
						mode = Mode.NORMAL;
						read = false;
					}
					break;
				}
				
				if (read)
					c = templateReader.read();
				else
					read = true;
			}
		} catch (IOException e) { // TODO: introduce specific exception type
			throw new RuntimeException("File I/O problem during code generation", e);
		}
	}	

	/**
	 * \brief Prints out all registered tags in the generator.	
	 */
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (AbstractTag t : tagMap.values()) {
			str.append(t.toString()+"\n");
		}
		return str.toString();
	}
}
