/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Collection;
import java.util.LinkedList;
import java.util.ArrayList;
  
  class Problem implements Comparable {
    public int compareTo(Object o) {
      if(o instanceof Problem) {
        Problem other = (Problem)o;
        if(!fileName.equals(other.fileName))
          return fileName.compareTo(other.fileName);
        if(!(beginLine == other.beginLine))
          return beginLine > other.beginLine? 1 : -1;
        if(!(beginColumn == other.beginColumn))
          return beginColumn > other.beginColumn? 1 : -1;          
        return message.compareTo(other.message);
      }
      return 0;
    }
    public enum Severity { ERROR, WARNING }
    public enum Kind { OTHER, LEXICAL, SYNTACTIC, SEMANTIC, COMPLIANCE }
    protected int beginLine = 0;
    protected int beginColumn = 0;
    public int beginLine() { return beginLine; }
    public void setBeginLine(int beginLine) { this.beginLine = beginLine; }
    public int beginColumn() { return beginColumn; }
    public void setBeginColumn(int beginColumn) { this.beginColumn = beginColumn; }
  
    protected String fileName;
    public String fileName() { return fileName; }
    public void setFileName(String fileName) { this.fileName = fileName; }
    protected String message;
    public String message() { return message; }
    protected Severity severity = Severity.ERROR;
    public Severity severity() { return severity; }
    protected Kind kind = Kind.OTHER;
    public Kind kind() { return kind; }
    
    public Problem(String fileName, String message) {
      this.fileName = fileName;
      this.message = message;
    }
    public Problem(String fileName, String message, Severity severity) {
      this(fileName, message);
      this.severity = severity;
    }
    public Problem(String fileName, String message, Severity severity, Kind kind) {
        this(fileName, message, severity);
        this.kind = kind;
    }    
    public Problem(String fileName, String message, Severity severity, Kind kind, int beginLine, int beginColumn) {
        this(fileName, message, severity);
        this.kind = kind;
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
    }
    
    public boolean isTestError(boolean checkAll) {
    	return checkAll || (severity == Severity.ERROR && kind != Kind.COMPLIANCE);
    }
  
    public boolean equals(Object o) {
    	return (o instanceof Problem) && (compareTo(o) == 0);
    }
    
    private static String name(Object o) {
		String name = o.toString();
		return name.charAt(0) + name.substring(1).toLowerCase();
    }
  
    public String toString() {
    	String kindStr = (kind == Kind.OTHER) ? "At " : name(kind) + " error at ";
    	return name(severity) + ": in file '" + fileName + "':\n" + 
    			kindStr + "line " + beginLine + ", column " + beginColumn + ":\n" + 
    			"  " + message;
    }
  }
  
  
  /**
   * \brief Common super class for all JModelica exceptions.
   */
  public class ModelicaException extends Exception {
	  public ModelicaException() {
	  }
	  
	  public ModelicaException(String message) {
		  super(message);
	  }
  }
  
  /**
   * Exception containing a list of compiler errors/warnings. 
   */
  public class CompilerException extends ModelicaException {
	  private Collection<Problem> problems;
	  
	  /**
	   * Default constructor.
	   */
	  public CompilerException() {
		  problems = new ArrayList<Problem>();
	  }
	  
	  /**
	   * Construct from a list of problems.
	   */
	  public CompilerException(Collection<Problem> problems) {
		  this.problems = problems;
	  }
	  
	  /**
	   * Add a new problem.
	   */
	  public void addProblem(Problem p) {
		  problems.add(p);
	  }
	  
	  /**
	   * Get the list of problems.
	   */
	  public Collection<Problem> getProblems() {
		  return problems;
	  }

	  /**
	   * Convert to error message.
	   */
	  public String getMessage() {
		  StringBuilder str = new StringBuilder();
		  str.append(problems.size());
		  str.append(" problems found:\n");
		  for (Problem p : problems) {
			  str.append(p);
			  str.append('\n');
		  }
		  return str.toString();
	  }
  }
  
  /**
   * Exception to be thrown when the Modelica class to instantiate is not
   * found.
   */
  public class ModelicaClassNotFoundException extends ModelicaException {
	  private String className;
	  
	  public ModelicaClassNotFoundException(String className) {
		  super("Class "+ className + " not found");
		  this.className = className;
	  }
	  
	  public String getClassName() {
		  return className;
	  }
	  
  }
  
  /**
   * Interface for handling semantic errors.
   * $see Root#setErrorHandler(IErrorHandler)
   */
  public interface IErrorHandler {
	  /**
	   * \brief Called when a semantic error is found. 
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   * @see ASTNode#error(String)
	   */
	  public void error(String s, ASTNode n);
	  
	  /**
	   * \brief Called when a compiler compliance error is found.
	   * 
	   * These errors are generated when compiling code that is legal Modelica, 
	   * but uses features that aren't implemented. Compliance errors are ignored 
	   * by test cases (except ComplianceErrorTestCase).
	   *  
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   * @see ASTNode#compliance(String)
	   */
	  public void compliance(String s, ASTNode n);
	  
	  /**
	   * \brief Called when a warning is issued during semantic error checking.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   * @see ASTNode#warning(String)
	   */
	  public void warning(String s, ASTNode n);
  }
  
  /**
   * \brief Default implementation of {@link IErrorHandler}.
   *  
   * Collects a list of {@link Problem} for all found errors.
   */
  public class DefaultErrorHandler implements IErrorHandler {
	  protected Root root;
	  
	  public DefaultErrorHandler(Root root) {
		  this.root = root;
	  }

	  /**
	   * \brief Creates a new {@link Problem} and adds it to root.errors, ignoring duplicates.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void error(String s, ASTNode n) {
		  problem(s, n, root.errors, Problem.Severity.ERROR, Problem.Kind.SEMANTIC);
	  }

	  /**
	   * \brief Creates a new {@link Problem} with kind COMPLIANCE 
	   *        and adds it to root.errors, ignoring duplicates.
	   * 
	   * @param s	error message.
	   * @param n	the node the error originated from.
	   */
	  public void compliance(String s, ASTNode n) {
		  problem(s, n, root.errors, Problem.Severity.ERROR, Problem.Kind.COMPLIANCE);
	  }

	  /**
	   * \brief Creates a new {@link Problem} and adds it to root.warnings, ignoring duplicates.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   */
	  public void warning(String s, ASTNode n) {
		  problem(s, n, root.warnings, Problem.Severity.WARNING, Problem.Kind.OTHER);
	  }
	  
	  protected void problem(String s, ASTNode n, ArrayList<Problem> list, Problem.Severity sev, Problem.Kind kind) {
		  Problem p = new Problem(n.fileName(), s, sev, kind, n.lineNumber(), n.columnNumber());
		  if (!list.contains(p))
			  list.add(p);
	  }
  }
  
  /**
   * \brief Error handler that generates warnings for compliance errors, 
   *        but otherwise works as {@link DefaultErrorHandler}.
   */
  public class ComplianceWarnErrorHandler extends DefaultErrorHandler {

	  public ComplianceWarnErrorHandler(Root root) {
		  super(root);
	  }

	  /**
	   * \brief Creates a new {@link Problem} with kind COMPLIANCE 
	   *        and adds it to root.warnings, ignoring duplicates.
	   * 
	   * @param s	warning message.
	   * @param n	the node the warning originated from.
	   */
	  public void compliance(String s, ASTNode n) {
		  problem(s, n, root.warnings, Problem.Severity.WARNING, Problem.Kind.COMPLIANCE);
	  }
	  
  }
  

aspect ErrorCheck {

  public ArrayList<Problem> Root.errors = new ArrayList<Problem>();
  public ArrayList<Problem> Root.warnings = new ArrayList<Problem>();

  private IErrorHandler Root.errorHandler = new DefaultErrorHandler(this);
  
  /**
   * Set the handler for semantic errors.
   * @see IErrorHandler 
   */
  public void Root.setErrorHandler(IErrorHandler handler) {
	  errorHandler = handler;
  }
  
  /**
   * Get the handler for semantic errors.
   * @see IErrorHandler 
   */
  public IErrorHandler Root.getErrorHandler() {
	  return errorHandler;
  }

  syn int ASTNode.lineNumber() = (start != 0 || getParent()==null)? beginLine() : getParent().lineNumber();
  syn int ASTNode.columnNumber() = (start != 0 || getParent()==null)? beginColumn() : getParent().columnNumber();
 
  /**
   * Register an error. Delegates to an {@link IErrorHandler}.
   * @param s	the error message.
   */
  void ASTNode.error(String s) {
	  root().getErrorHandler().error(s, this);
  }

  /**
   * Register a compliance error. Delegates to an {@link IErrorHandler}.
   * @param s	the error message.
   */
  void ASTNode.compliance(String s) {
	  root().getErrorHandler().compliance(s, this);
  }

  /**
   * Register a warning. Delegates to an {@link IErrorHandler}.
   * @param s	the warning message.
   */
  void ASTNode.warning(String s) {
	  root().getErrorHandler().warning(s, this);
  }

  /**
   * \brief Call all *Check() methods for this node.
   * 
   * Helper method to make it easier to add new check methods 
   * for all nodes.
   */
  protected void ASTNode.allChecks() {
	  nameCheck();
	  typeCheck();
	  contentCheck();	  
	  complianceCheck();	  
  }
  
  public void ASTNode.collectErrors() {
	  allChecks();
	  for(int i = 0; i < getNumChild(); i++) {
		  getChild(i).collectErrors();
	  }
  }


  public Collection<Problem> ASTNode.errorCheck(){
    ArrayList allErrors = root().errors;
    ArrayList allWarnings = root().warnings;
    collectErrors();  
    java.util.Collections.sort(allErrors);
    java.util.Collections.sort(allWarnings);
    
    ArrayList<Problem> problems = new ArrayList<Problem>();
    problems.addAll(allWarnings);
    problems.addAll(allErrors);    
    
    /*
    if (!allWarnings.isEmpty()) {
    	str.append("\n");        	
    	str.append("Warnings:\n");    
    	for(Iterator iter = allWarnings.iterator(); iter.hasNext(); ) {
    		Problem problem = (Problem) iter.next();
    		str.append(problem+"\n");
    	}
    }
   if(allErrors.isEmpty())
      return false;
    str.append("\n");        	
    str.append(allErrors.size() + " error(s) found...\n");
    for(Iterator iter = allErrors.iterator(); iter.hasNext(); ) {
      Problem problem = (Problem)iter.next();
      str.append(problem+"\n");
    }
    */
    
    return problems;
  }

/*
    syn lazy FullClassDecl ASTNode.retrieveFullClassDecl(String className) {
      for(int i = 0; i < getNumChild(); i++) {
		  FullClassDecl fcd = getChild(i).retrieveFullClassDecl(className);
	  	  if (fcd != null)
	  	  	return fcd;
	  }
	  return null;
    }   
  	
  	eq FullClassDecl.retrieveFullClassDecl(String className) {
	   	if (className.equals(qualifiedName())) {
	   		return this;
		} else
			return getClassDeclList().retrieveFullClassDecl(className);
	}
*/
   
}

aspect InstanceErrorCheck {

 // Error checking in instance tree

	// We don't want to error check an entire model, just the classes
	//   that are used. 
	public Collection<Problem> InstProgramRoot.checkErrorsInInstClass(String className) throws ModelicaClassNotFoundException{
		InstClassDecl icd = simpleLookupInstClassDecl(className);
		if (icd.isUnknown()) 
			throw new ModelicaClassNotFoundException(className);
		else
			return icd.errorCheck();
	}

	boolean InstNode.errorChecked = false;
	boolean InstModification.errorChecked = false;


  public void InstNode.collectErrors() {
    if (!errorChecked) {
      errorChecked = true;
	  allChecks();
	  for(InstNode n : getInstComponentDecls()) 
		  n.collectErrors();
	  collectErrorsInClassDecls();
	  for(InstNode n : getInstExtendss()) 
		  n.collectErrors();
	  for(FAbstractEquation e : getFAbstractEquations()) 
		  e.collectErrors();
	}
  }
  
    public void InstNode.collectErrorsInClassDecls() {
    	for (InstNode n : getInstClassDecls()) 
    		n.collectErrors();
    }
    
    public void InstComponentDecl.collectErrorsInClassDecls() {}
    
    public void InstExtends.collectErrorsInClassDecls() {
    	if (!inInstComponent())
    		super.collectErrorsInClassDecls();
    }
    
    /**
     * \brief Check if this node is in an InstComponentDecl.
     */
    inh boolean InstExtends.inInstComponent();
    inh boolean InstClassRedeclare.inInstComponent();
    eq InstComponentDecl.getChild().inInstComponent() = true;
    eq InstClassDecl.getChild().inInstComponent()     = false;
    eq InstRoot.getChild().inInstComponent()          = false;
    eq FlatRoot.getChild().inInstComponent()          = false;

  	public void InstBaseClassDecl.collectErrors() {
    	if (!errorChecked) {
//    	errorChecked = true;
        super.collectErrors();
 //       for (InstClassDecl icd : instClassDecls())
 //       	icd.collectErrors();
 /*
        for (InstComponentDecl icd : instComponentDecls())
        	icd.collectErrors();
        for (InstExtends ie : instExtends())
        	ie.collectErrors();	
*/        	
        for (InstImport ii : getInstImports())
        	ii.collectErrors();		
		if (hasInstConstraining())
			getInstConstraining().collectErrors();
//        for (FAbstractEquation e : getFAbstractEquations())
//        	e.collectErrors();
//    	getEquationList().collectErrors();
//    	getAlgorithmList().collectErrors();
//    	getSuperList().collectErrors();
//    	getImportList().collectErrors();
//    	getComponentDeclList().collectErrors();
			if (getBaseClassDecl() instanceof FullClassDecl) { 	
				FullClassDecl fcd = (FullClassDecl)getBaseClassDecl();
				//log.debug(fcd.getName().getID() +  " " + fcd.getEndName());
				if (!(fcd.getName().getID().equals(fcd.getEndName()))) {
					error("The declaration and end names of a class should be the same");
				}
			}
  		}
  	}
  	
  	public void InstFullClassDecl.collectErrors() {
    	if (!errorChecked) {
            super.collectErrors();
            getInstExternalOpt().collectErrors();
    	}
  	}

//  	public void InstEnumClassDecl.collectErrors() {  		
//  		// TODO: Error checking of enumeration declarations
//  	}
  	
	boolean InstImport.errorChecked = false;
    public void InstImport.collectErrors() {
    	if (!errorChecked) {
    		errorChecked = true;
			getPackageName().collectErrors();
		}
	}
	
	public void InstComponentDecl.collectErrors() {
	    if (!errorChecked) {
	    	if (isRecursed()) {
	    		errorChecked = true;
	    		error("Recursive class structure");
	    	} else {
	    		super.collectErrors();
	    		getClassName().collectErrors();
	    		if (hasInstModification())
	    			getInstModification().collectErrors();
	    		if (hasInstConstraining())
	    			getInstConstraining().collectErrors();
	    	}
		}
	}
	
	public void FExp.checkConstantExpression(InstPrimitive node, String varKind, String varName) {
		String exp = "'" + prettyPrint("") + "'";
		try {
			if (isCircular()) {
				node.error("Could not evaluate binding expression for " + varKind + " '" +
						varName + "' due to circularity: " + exp);
			} else {
				CValue val = ceval();
				if (val.isUnknown()) {
					if (val.isUnsupported()) {
						if (node.isConstant()) // TODO: Produce error for structural parameters as well
							node.compliance("Constant evaluation not supported for expression(s) directly or indirectly " + 
									"used by the binding expression for " + varKind + " '" + varName + "': " + exp);
					} else {
						node.error("Could not evaluate binding expression for " + 
								varKind + " '" + varName + "': " + exp);
					}
				}	
			}
		} catch (ConstantEvaluationException e) {
			node.error("Could not evaluate binding expression for " + 
					varKind + " '" + varName + "': " + exp);
		}
	}
	
	public void InstPrimitive.collectErrors() {
		//log.debug(toString());
        if (!errorChecked) {
        	super.collectErrors();
	
			// Check binding expression
			if (isParameter() || isConstant()) {
				String type = isParameter() ? "parameter" : "constant";
				// Check if the binding expression of constants and parameters can be evaluated
				if (myBindingInstExp() != null)
					myBindingInstExp().checkConstantExpression(this, type, qualifiedName());
				// Warn if constant or parameter does not have a binding expression (start is used)
				else if (!isForIndex())
					warning("The " + type + " " + qualifiedName() + " does not have a binding expression");
			}
			
			// Check array indices
			getClassName().collectErrors();
			getLocalFArraySubscriptsOpt().collectErrors();
			
			// Check if the expressions of the attributes can be evaluated
			// Note that this check has to be done locally in the
			// context of an InstAssignable node in order to avoid
			// evaluation of all value modifications also for non
			// parameters.
			for (InstModification im : totalMergedEnvironment()) {
				// Only check attributes, value modifications are checked above
				if (im instanceof InstComponentModification) {
					InstComponentModification icm = (InstComponentModification)im;
					if (icm.hasInstModification() && icm.getInstModification().hasInstValueMod()) {
						FExp val_mod = icm.getInstModification().instValueMod();
						val_mod.checkConstantExpression(this, "attribute", icm.getName().name());
					}
				}
			}
		}
	}
	
	public void InstArrayComponentDecl.collectErrors() {
	    if (!errorChecked) {
	  	  for(InstNode n : getInstComponentDecls()) 
			  n.collectErrors();
		}
	}
	
	public void InstExtends.collectErrors() {
	    if (!errorChecked) {
	    	if (isRecursed()) {
	    		error("Recursive class structure");
	    	} else {
				super.collectErrors();
				getClassName().collectErrors();
				if (hasInstClassModification())
					getInstClassModification().collectErrors();
			}
	    }
	}	
	
	public void InstExtendsShortClass.collectErrors() {
	    if (!errorChecked) {
    		errorChecked = true;
	    	if (isRecursed()) {
	    		error("Recursive class structure");
	    	} else {
	    		allChecks();
	    		// Normally the class modifications in an InstExtendsShortClass
	    		// does not need to be checked, since they are checked in InstShortClassDecl.
	    		// This is not the case if the short class decl references
	    		// an primitive variable, however, and in this case the
	    		// class modification needs to be checked for errors.
	    		if (extendsPrimitive() && hasInstClassModification())
	    			getInstClassModification().collectErrors();

	    		getClassName().collectErrors();
	    		for(InstNode n : getInstComponentDecls()) 
	    			n.collectErrors();
	    		collectErrorsInClassDecls();
	    		for(InstNode n : getInstExtendss()) 
	    			n.collectErrors();
	    	}
		}
    }
	
	//public void InstPrimitiveClassDecl.collectErrors() {}

	public void InstShortClassDecl.collectErrors() {
	    if (!errorChecked) {
	      if (!hasFArraySubscripts() && !hasInstClassModification()) {
	    	  errorChecked = true;
	    	  return;
	      }
	      
		  super.collectErrors();
		  // The localInstModifications should only be checked if
		  // the node is not a InstReplacingShortClassDecl. This
		  // is accomplished by the method collectInstModificationErrors.
		  collectInstModificationErrors();
		  if (hasInstConstraining())
				getInstConstraining().collectErrors();		  
		}
	}

	public void InstShortClassDecl.collectInstModificationErrors() {
		for (InstModification mod : localInstModifications())
			mod.collectErrors();
    }
    public void InstReplacingShortClassDecl.collectInstModificationErrors() { }

	public void InstReplacingShortClassDecl.collectErrors() {
	    if (!errorChecked) {
		  super.collectErrors();
		  getOriginalInstClass().collectErrors();
		}
		
	}

	public void InstReplacingFullClassDecl.collectErrors() {
	    if (!errorChecked) {
		  super.collectErrors();
		  getOriginalInstClass().collectErrors();
		}
		
	}

	public void InstBuiltIn.collectErrors() {}

	public void InstComponentRedeclare.collectErrors() {
		super.collectErrors();
		getInstComponentDecl().collectErrors();
	}

	public void InstClassRedeclare.collectErrors() {
		super.collectErrors();
		if (!inInstComponent())
			getInstClassDecl().collectErrors();	
	}

	public void InstValueModification.collectErrors() {
		getFExp().collectErrors();
	}

	boolean InstAccess.errorChecked = false;
	public void InstDot.collectErrors() {
	    if (!errorChecked) {	
			errorChecked = true;
			getLeft().collectErrors();
			if (!getLeft().isUnknown())
				getRight().collectErrors();
			allChecks();
	    }
	}

	public void InstClassAccess.collectErrors() {
	    if (!errorChecked) {
		    //super.collectErrors();
		    nameCheck();
		    errorChecked = true;
			// If the class access is to be checked, then the corresponding
			// InstClassDecl is checked as well, since it is being accessed at
			// some point... ...But it is probably too drastic since it means
			// that if Modelica.Electrical.Analog.Basic.Resistor is accessed,
			// then first Modelica is completely checked, then Electrical etc.
			// Probably, only the name should be checked here, and the 
			// actually target class of an access should be checked. 
		    //
		    // One could do this check if (getLastAccess() == this) - 
		    // then only target class would be checked.
		    //
			//if (!myInstClassDecl().isUnknown())
			//	myInstClassDecl().collectErrors();
		}
	}
	
	public void FArraySubscripts.collectErrors() {
		// Should this check be in the access instead?
		int ndims = mySize().ndims();
		if (getNumFSubscript() > ndims && !isInstComponentSize()) {
			// TODO: shouldn't this check for to few as well? (no [] or all dimensions given)
			error("Too many array subscripts for access: " + getNumFSubscript() + 
					" subscripts given, component has " + mySize().ndims() + " dimensions");
			allChecks();
			for (int i = 0; i < ndims; i++)
				getFSubscript(i).collectErrors();
		} else {
    		super.collectErrors();
		}
	}
	
	/**
	 * \brief Check if class has exactly one algorithm section or external function declaration.
	 */
	public boolean InstClassDecl.isCompleteFunction() {
		return (numFAlgorithmBlock() == 1) != hasInstExternal();
	}
	
	syn boolean InstClassDecl.hasInstExternal() = false;
	
	syn int InstClassDecl.numFAlgorithmBlock() {
		int n = 0;
		for (FAbstractEquation e : getFAbstractEquations())
			if (e instanceof FAlgorithmBlock)
				n++;
		return n;
	}
	
	protected boolean FAbstractFunctionCall.errorChecked = false;
	public void InstFunctionCall.collectErrors() {
	    if (!errorChecked) {
	    	errorChecked = true;
	    	// Check that the function exists
	    	InstClassDecl func = getName().myInstClassDecl();
	    	if (!func.isCallable()) {
	    		// Report that function does not exist
		    	String name = getName().name();
	    		if (func.isUnknown()) 
	    			error("The function " + name + "() is undeclared");
	    		else
	    			error("The class " + name + " is not a function");
	    	} else if (!func.isRecord() && !func.isCompleteFunction()) {
	    		// TODO: add check if function is partial?
	    		error("Calling function " + getName().name() + 
	    				"(): can only call functions that have one algorithm section or external function specification");
	    	} else {
	    		// Function exists, check everything
	    		super.collectErrors();
	    		
	    		// We need to check the function definition as well.
	    	    func.collectErrors();
	    	    
	    	    // Check if there were any unbindable args
			    boolean pos = true;
			    String desc = functionCallDecription();
			    for (InstFunctionArgument arg : unbindableArgs) 
			    	pos = arg.generateUnbindableError(desc, pos);
	    	}
	    }
	}
	
	syn String FAbstractFunctionCall.functionCallDecription() = "Calling function " + name() + "()";
	eq FRecordConstructor.functionCallDecription() = "Record constructor for " + name();
	eq InstFunctionCall.functionCallDecription()   = getName().myInstClassDecl().isRecord() ? 
			"Record constructor for " + name() : super.functionCallDecription();
	  
	public boolean InstFunctionArgument.generateUnbindableError(String desc, boolean genForPos) {
		return genForPos;
	}
	  
	public boolean InstPositionalArgument.generateUnbindableError(String desc, boolean genForPos) {
		if (genForPos)
			error(desc + ": too many positional arguments");
		return false;
	}
	  
	public boolean InstNamedArgument.generateUnbindableError(String desc, boolean genForPos) {
		error(desc + ": no input matching named argument " + getName().name() + " found");
		return genForPos;
	}
	
	public void FBuiltInFunctionCall.collectErrors() {
	    if (!errorChecked) {
	    	errorChecked = true;
	    	getOriginalArgs().collectErrors();
	    }
    	super.collectErrors();
	}
	
	public void FUnsupportedBuiltIn.collectErrors() {
		// Don't check arguments
		allChecks();
	}
 
	protected boolean InstNamedArgument.errorChecked = false;
	public void InstNamedArgument.collectErrors() {
		// TODO: This way, the FExp for each argument to a built-in function is checked twice - fix that
	    if (!errorChecked) {
			allChecks();
			getFExp().collectErrors();
	    }
	}
	
	/**
	 * \brief Check if this component is in a recursive component structure.
	 */
	syn boolean InstComponentDecl.isRecursed() = isWithin(myInstClass());
	
	/**
	 * \brief Check if this component is in a recursive class structure.
	 */
	syn boolean InstExtends.isRecursed()       = isWithin(myInstClass());
	
	/**
	 * \brief Check if extends tree is recursive.
	 */
	public boolean InstExtends.isRecursive() {
		if (recursiveCache == RECURSIVE_UNKNOWN)
			calcIsRecursive(new HashSet<InstExtends>());
		return recursiveCache == RECURSIVE_YES;
	}
	
	/**
	 * \brief Examine extends tree to find recursive extends nodes.
	 */
	private void InstExtends.calcIsRecursive(HashSet<InstExtends> visited) {
		recursiveCache = visited.contains(this) ? RECURSIVE_YES : RECURSIVE_NO;
		visited.add(this);
		if (recursiveCache == RECURSIVE_NO) 
			for (InstExtends ie : myInstClass().getInstExtendss())
				ie.calcIsRecursive(visited);
	}
	
	private byte InstExtends.recursiveCache = RECURSIVE_UNKNOWN;
	private static final byte InstExtends.RECURSIVE_UNKNOWN = 0;
	private static final byte InstExtends.RECURSIVE_YES     = 1;
	private static final byte InstExtends.RECURSIVE_NO      = 2;
	
	/**
	 * \brief Check if <code>icd</code> is an ancestor of this node or any ancestor is an 
	 *        instance of <code>icd</code>.
	 */
	inh boolean InstNode.isWithin(InstClassDecl icd);
	eq InstNode.getChild().isWithin(InstClassDecl icd) = isOfInstClassDecl(icd) || isWithin(icd);
	eq InstRoot.getChild().isWithin(InstClassDecl icd) = false;
	
	/**
	 * \brief Check if this node is equal to or an instance of <code>icd</code>.
	 */
	syn boolean InstNode.isOfInstClassDecl(InstClassDecl icd) = false;
	eq InstClassDecl.isOfInstClassDecl(InstClassDecl icd)     = icd == this;
	eq InstComponentDecl.isOfInstClassDecl(InstClassDecl icd) = icd == myInstClass();
	eq InstExtends.isOfInstClassDecl(InstClassDecl icd)       = icd == myInstClass();
	
}

