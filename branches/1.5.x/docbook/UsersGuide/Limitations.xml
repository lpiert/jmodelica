<?xml version="1.0" encoding="UTF-8"?>
<chapter version="5.0" xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:svg="http://www.w3.org/2000/svg"
         xmlns:m="http://www.w3.org/1998/Math/MathML"
         xmlns:html="http://www.w3.org/1999/xhtml"
         xmlns:db="http://docbook.org/ns/docbook">
  <title>Limitations</title>

  <para>This page lists the current limitations of the JModelica.org platform,
  as of version 1.5.0. The development of the platform can be followed at the
  <link xlink:href="http://trac.jmodelica.org">Trac</link> site, where future
  releases and associated features are planned. In order to get an idea of the
  current Modelica compliance of the compiler front-end, you may look at the
  associated test suite. All models with a test annotation can be
  flattened.</para>

  <itemizedlist>
    <listitem>
      <para>The Modelica compliance of the front-end is limited; the following
      features are currently not supported:</para>

      <itemizedlist>
        <listitem>
          <para>Parsing of full Modelica 3.2 (Modelica 3.0 is
          supported)</para>
        </listitem>

        <listitem>
          <para>Integer, Boolean and enumeration variables for optimizations
          (parameters and constants are supported, as are discrete variables
          in simulations)</para>
        </listitem>

        <listitem>
          <para>Only partial support for enumeration variables in FMUs
          (parameters and constants are supported)</para>
        </listitem>

        <listitem>
          <para>Strings</para>
        </listitem>

        <listitem>
          <para>Arrays indexed with enumerations or Booleans</para>
        </listitem>

        <listitem>
          <para>If equations are only supported with parameter or constant
          test expressions, and recompilation is required for a change of a
          parameter used in the test expression to take effect on the if
          equation.</para>
        </listitem>

        <listitem>
          <para>Functions with array inputs with sizes declared as ':' are
          only partially supported.</para>
        </listitem>

        <listitem>
          <para>Connect clauses does not handle arrays of connectors
          properly.</para>
        </listitem>

        <listitem>
          <para>Partial support for external functions, only external C
          functions with scalar inputs and outputs are supported.</para>
        </listitem>

        <listitem>
          <para>The following built-in functions are not
          supported:<informaltable border="1" frame="void" width="75%">
              <tr>
                <td>sign(v)</td>

                <td>cardinality()</td>

                <td>reinit(x, expr)</td>
              </tr>

              <tr>
                <td>semiLinear(...)</td>

                <td>scalar(A)</td>

                <td>String(...)</td>
              </tr>

              <tr>
                <td>Subtask.decouple(v)</td>

                <td>vector(A)</td>

                <td>div(x,y)</td>
              </tr>

              <tr>
                <td>delay(...)</td>

                <td>matrix(A)</td>

                <td>mod(x,y)</td>
              </tr>

              <tr>
                <td>terminal()</td>

                <td>diagonal(v)</td>

                <td>rem(x,y)</td>
              </tr>

              <tr>
                <td>smooth(p, expr)</td>

                <td>product(...)</td>

                <td>ceil(x)</td>
              </tr>

              <tr>
                <td>sample(start, interval)</td>

                <td>outerProduct(v1, v2)</td>

                <td>floor(x)</td>
              </tr>

              <tr>
                <td>edge(b)</td>

                <td>symmetric(A)</td>

                <td>integer(x)</td>
              </tr>

              <tr>
                <td>skew(x)</td>
              </tr>
            </informaltable></para>
        </listitem>

        <listitem>
          <para>Overloaded operators (Modelica Language Specification, chapter
          14)</para>
        </listitem>

        <listitem>
          <para>Stream connections with more than two connectors are not
          supported.</para>
        </listitem>

        <listitem>
          <para>Mapping of models to execution environments (Modelica Language
          Specification, chapter 16)</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>In the Optimica front-end the following constructs are not
      supported:</para>

      <itemizedlist>
        <listitem>
          <para>Annotations for transcription information</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The JModelica.org Model Interface (JMI) has the following
      Limitations:</para>

      <itemizedlist>
        <listitem>
          <para>The ODE interface requires the Modelica model to be written on
          explicit ODE form in order to work.</para>
        </listitem>

        <listitem>
          <para>Second order derivatives (Hessians) are not provided</para>
        </listitem>

        <listitem>
          <para>The interface does not yet comply with FMI
          specification</para>
        </listitem>
      </itemizedlist>
    </listitem>

    <listitem>
      <para>The JModelica.org FMI Model Interface (FMI) has the following
      Limitations:</para>

      <itemizedlist>
        <listitem>
          <para>The FMI interface only supports FMUs distributed with
          binaries, not source code.</para>
        </listitem>

        <listitem>
          <para>Options for setting and getting string variables does not
          work</para>
        </listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</chapter>
