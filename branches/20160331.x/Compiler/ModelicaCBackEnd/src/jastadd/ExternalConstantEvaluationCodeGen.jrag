/*
Copyright (C) 2009 Modelon AB
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

aspect Serialize {
    public class DeclPrinter_ECE extends DeclPrinter_C {
        private CodeGenContext cgc;
        private Map<String,String> tempMap;
        public DeclPrinter_ECE(CodePrinter p, CodeStream str, Map<String,String> tempMap, CodeGenContext cgc) {
            super(p, str);
            this.tempMap = tempMap;
            this.cgc = cgc;
        }
        protected String acquireTemp(String key) {
            String s = cgc.nextTempName_C();
            tempMap.put(key, s);
            return s;
        }
        public void print(FExternalObjectType type) {
            printComposite(type);
        }
        protected void printComps(FType type) {
            if (type.isExternalObject()) {
                FExternalObjectType t = (FExternalObjectType) type;
                FExternalStmt stmt = t.myConstructorStmt();
                stmt.setCodeGenContext(cgc.createProxy());
                stmt.genVarDecls_C(p, str, indent());
                stmt.setCodeGenContext(null);
                printComps(t);
            } else {
                super.printComps(type);
            }
        }
    }
    
    public class InitPrinter_ECE extends InitPrinter_C {
        protected CodeGenContext cgc;
        private Map<String,String> tempMap;
        public InitPrinter_ECE(CodePrinter p, CodeStream str, Map<String,String> tempMap, CodeGenContext cgc) {
            super(p, str);
            this.tempMap = tempMap;
            this.cgc = cgc;
        }
        protected String acquireTemp(String key) {
            return tempMap.get(key);
        }
        public void print(FExternalObjectType type) {
            printComposite(type);
        }
        protected void printComps(FType type) {
            if (type.isExternalObject())
                printComps((FExternalObjectType) type);
            else
                super.printComps(type);
        }
        protected void printArray(FType type) {
            str.formatln("%sJMCEVAL_parseArrayDims(%d);", indent(), type.ndims());
            super.printArray(type);
            if (type.isExternalObject()) {
                
            } else {
                type.genSerialize_C(str, indent(), name(), true);
            }
        }
        protected void printNumElements(FType type) {
            str.print("d[0]");
            for (int i = 1; i < type.ndims(); i++)
                str.print("*d["+i+"]");
        }
        protected void printDimensions(FType type) {
            for (int i = 0; i < type.ndims(); i++)
                str.print(", d["+i+"]");
        }
        protected void printScalar(FType type) {
            if (!type.isExternalObject()) {
                type.genSerialize_C(str, indent(), name(), true);
            }
        }
        
        protected void printComps(FExternalObjectType type) {
            super.printComps(type);
            printComps_sub(type);
        }
        
        protected void printComps_sub(FExternalObjectType type) {
            int i = 0;
            FExternalStmt stmt = ((FExternalObjectType)type).myConstructorStmt();
            stmt.setCodeGenContext(cgc.createProxy());
            ArrayList<FExp> args = stmt.myConstructorArgs();
            CommonVariableDecl out = stmt.myConstructorOutput();
            String name = acquireTemp(name());
            stmt.getCodeGenContext().setAlias(out.name_C(), name());
            for (FExp arg : args)
                stmt.getCodeGenContext().setAlias(arg.prettyPrint_C(""), name + "_arg" + i++);
            stmt.prettyPrint_C(p, str, indent());
            stmt.setCodeGenContext(null);
        }
    }
    public class FreePrinter_ECE extends InitPrinter_ECE {
        public FreePrinter_ECE(CodePrinter p, CodeStream str, Map<String,String> tempMap, CodeGenContext cgc) {
            super(p, str, tempMap, cgc);
        }
        public void print(FType type) {
            type.genFreeStrings_C(str, indent(), name());
        }
        protected void printComps_sub(FExternalObjectType type) {
            FExternalStmt stmt = type.myDestructorStmt();
            ArrayList<FExp> args = stmt.myConstructorArgs();
            stmt.setCodeGenContext(cgc.createProxy());
            stmt.getCodeGenContext().setAlias(args.get(0).prettyPrint_C(""), name());
            stmt.prettyPrint_C(p, str, indent());
            stmt.setCodeGenContext(null);
        }
    }
    
    /**
     * Generate all C-declarations necessary for an external evaluation
     * 
     * @param p       ASTNode code generation visitor
     * @param str     output code stream
     * @param indent  indentation string
     * @param cgc     context for generating temporaries
     * @param tempMap maps expressions that should be replaced with temporaries
     */
    public void FExternalStmt.genSerializeDecl_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        ArrayList<CommonVariableDecl> vars  = varsToSerialize();
        TypePrinter_C dp = new DeclPrinter_ECE(p, str, tempMap, cgc);
        for (CommonVariableDecl cvd : vars) {
             dp.reset(cvd.name_C(), null, cvd.size().isUnknown(), false, indent);
             cvd.type().print(dp);
        }
        genVarDecls_C(p, str, indent);
        str.println();
    }
    
    /**
     * Generate statements for initializing, include read from stdin, 
     * variables that only need to be initialized once.
     * 
     * For parameters {@link FExternalStmt.genSerializeDecl_C}
     */
    public void FExternalStmt.genSerializeInit_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        TypePrinter_C ip = new InitPrinter_ECE(p, str, tempMap, cgc);
        for (CommonVariableDecl eo : externalObjectsToSerialize()) {
             ip.reset(eo.name_C(), null, eo.size().isUnknown(), false, indent);
             eo.type().print(ip);
        }
        str.println();
    }
    
    /**
     * Generate statements for initializing, include read from stdin, 
     * variables that need to be initialized each function call.
     * 
     * For parameters {@link FExternalStmt.genSerializeDecl_C}
     */
    public void FExternalStmt.genSerializeCalcInit_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        TypePrinter_C ip = new InitPrinter_ECE(p, str, tempMap, cgc);
        for (CommonVariableDecl arg : functionArgsToSerialize()) {
             ip.reset(arg.name_C(), null, arg.size().isUnknown(), false, indent);
             arg.type().print(ip);
        }
        str.println();
    }
    
    /**
     * Generate statements for type conversions, calling the external function,
     * and printing return values on stdout.
     * 
     * For parameters {@link FExternalStmt.genSerializeDecl_C}
     */
    public void FExternalStmt.genSerializeCalc_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        prettyPrint_C(p, str, indent);
        genCheckPoint(str, indent, "DONE");
        for (CommonVariableDecl cvd : varsToDeserialize())
            cvd.type().genSerialize_C(str, indent, cvd.name_C(), false);
        str.println();
    }
    
    /**
     * Generate statements for deallocating variables that 
     * are instantiated in {@link FExternalStmt.genSerializeCalcInit_C}.
     * 
     * For parameters {@link FExternalStmt.genSerializeDecl_C}
     */
    public void FExternalStmt.genSerializeCalcFree_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        TypePrinter_C fp = new FreePrinter_ECE(p, str, tempMap, cgc);
        for (CommonVariableDecl cvd : functionArgsToSerialize()) {
             fp.reset(cvd.name_C(), null, cvd.size().isUnknown(), false, indent);
             cvd.type().print(fp);
        }
    }
    
    /**
     * Generate statements for deallocating variables that 
     * are instantiated in {@link FExternalStmt.genSerializeInit_C}.
     * 
     * For parameters {@link FExternalStmt.genSerializeDecl_C}
     */
    public void FExternalStmt.genSerializeEnd_C(CodePrinter p, CodeStream str, String indent,
            CodeGenContext cgc, Map<String,String> tempMap) {
        TypePrinter_C fp = new FreePrinter_ECE(p, str, tempMap, cgc);
        for (CommonVariableDecl cvd : externalObjectsToSerialize()) {
             fp.reset(cvd.name_C(), null, cvd.size().isUnknown(), false, indent);
             cvd.type().print(fp);
        }
    }
    
    public void FExternalStmt.genCheckPoint(CodeStream str, String indent, String token) {
        str.print(indent);
        str.print("JMCEVAL_check(\"");
        str.print(token);
        str.print("\");\n");
    }
    
    /**
     * Generate code that parses the variable <code>name</code> of type 
     * <code>this</code> from stdin.
     */
    public void FType.genSerialize_C(CodeStream str, String indent, String name, boolean parse) {
        str.formatln("%sJMCEVAL_%s%s(%s, %s);",
                indent,
                parse ? "parse" : "print",
                isArray() ? "Array" : "",
                isEnum() ? "Enum" : name(),
                name);
    }
    
    /**
     * Generate code to free strings.
     */
    public void FType.genFreeStrings_C(CodeStream str, String indent, String name) {
        if (isString()) {
            str.formatln("%sJMCEVAL_free%s(%s);", indent, isArray() ? "Array" : "", name);
        }
    }
    
    /**
     * List of CommonVariableDecl which has to be sent to the process
     * when evaluating an external function.
     */
    syn ArrayList<CommonVariableDecl> FExternalStmt.varsToSerialize() {
        if (!hasArg() && !hasReturnVar())
            return myCommonVarDecls();
        
        ArrayList<CommonVariableDecl> res = new ArrayList<CommonVariableDecl>();
        if (hasReturnVar())
            res.add(getReturnVar().myCommonVarDecl());
        for (FExp e : getArgs()) {
            e.varsToSerialize(res);
        }
        return res;
    }
    
    public void FExp.varsToSerialize(ArrayList<CommonVariableDecl> decls) {
        
    }
    
    public void FIdUseExp.varsToSerialize(ArrayList<CommonVariableDecl> decls) {
        CommonVariableDecl cvd = myCommonVarDecl();
        if (!decls.contains(cvd))
            decls.add(cvd);
    }
    
    public void FSizeExp.varsToSerialize(ArrayList<CommonVariableDecl> decls) {
        getFExp().varsToSerialize(decls);
        //getDim().varsToSerialize(decls);
    }
    
    syn ArrayList<CommonVariableDecl> FExternalStmt.externalObjectsToSerialize() {
        ArrayList<CommonVariableDecl> externalObjects = new ArrayList<CommonVariableDecl>();

        for (CommonVariableDecl var : varsToSerialize()) {
            if (var.type().isExternalObject()) {
                externalObjects.add(var);
            }
        }

        return externalObjects;
    }
    
    syn ArrayList<CommonVariableDecl> FExternalStmt.functionArgsToSerialize() {
        ArrayList<CommonVariableDecl> functionArgs = new ArrayList<CommonVariableDecl>();

        for (CommonVariableDecl var : varsToSerialize()) {
            if (!var.type().isExternalObject()) {
                functionArgs.add(var);
            }
        }

        return functionArgs;
    }
    
    /**
     * List of CommonVariableDecl which has to be read from the process
     * when evaluating an external function.
     */
    syn ArrayList<CommonVariableDecl> FExternalStmt.varsToDeserialize() {
        ArrayList<CommonVariableDecl> res = new ArrayList<CommonVariableDecl>();
        for (CommonVariableDecl cvd : varsToSerialize())
            if (cvd.isOutput())
                res.add(cvd);
        return res;
    }
    
    /**
     * CommonVariableDecls from enclosing function
     */
    private ArrayList<CommonVariableDecl> FExternalStmt.myCommonVarDecls() {
        FFunctionDecl decl = containingFFunctionDecl();
        if (decl != null)
            return new ArrayList<CommonVariableDecl>(decl.getFFunctionVariables().toArrayList());
        else
            return new ArrayList<CommonVariableDecl>(enclosingInstClassDecl().getInstComponentDecls().toArrayList());
    }

    /**
     * Declarations of records used in this function
     */
    syn Collection<FRecordDecl> FExternalStmt.usedRecords() {
        Set<FRecordDecl> res = new LinkedHashSet<FRecordDecl>();
        for (CommonVariableDecl cvd : varsToSerialize())
            if (cvd.type().isRecord())
                res.add(cvd.type().myFRecordDecl());
        return res;
    }
    
    /*
     * Some code generation for the instance tree.
     */
    syn String FIdUseInstAccess.name_C() = toString_C(printer_C);
    
    syn String FIdUseInstAccess.toString_C(CodePrinter p) {
        return getInstAccess().toString_C(p);
    }

    syn String InstAccess.toString_C(CodePrinter p) =
        myInstLookupComponent().target().name_C();

    public String InstComponentDecl.name_C() {
        StringBuilder buf = new StringBuilder();
        buf.append(getFQName().nameUnderscore());
        buf.append('_');
        buf.append(type().isArray() ? C_SUFFIX_ARRAY : C_SUFFIX_VARIABLE);
        return buf.toString();
    }
}
