import org.jmodelica.icons.Icon;
import org.jmodelica.icons.Component;
import org.jmodelica.icons.exceptions.FailedConstructionException;
import org.jmodelica.icons.drawing.IconConstants.Context;
import org.jmodelica.ide.ui.ImageLoader;
import org.jmodelica.icons.drawing.AWTIconDrawer;
import java.awt.image.BufferedImage;

aspect ModelicaIcons
{
	syn lazy Icon BaseNode.icon() 		= Icon.NULL_ICON;
	syn lazy Icon BaseNode.diagram() 	= Icon.NULL_ICON;
	
	eq UnknownClassDecl.icon()	 		= Icon.NULL_ICON;
	eq UnknownClassDecl.diagram() 		= Icon.NULL_ICON;
	eq UnknownInstClassDecl.icon() 		= Icon.NULL_ICON;
	eq UnknownInstClassDecl.diagram() 	= Icon.NULL_ICON;

	eq ComponentDecl.icon() 		= 	findClassDecl().icon();
	eq ComponentDecl.diagram() 		= 	findClassDecl().diagram();
	eq InstComponentDecl.icon() 	= 	myInstClass().icon();
	eq InstComponentDecl.diagram() 	= 	myInstClass().diagram();
	eq InstShortClassDecl.icon()	= 	getInstExtends(0).myInstClass().icon();
	eq InstShortClassDecl.diagram()	= 	getInstExtends(0).myInstClass().diagram();


	protected boolean ClassDecl.visitingDuringIconRendering;
	
	eq ClassDecl.icon() { 
		visitingDuringIconRendering = true;
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		icon = new Icon(qualifiedName(), layer, Context.ICON); 
		icon = addSuperClasses(icon);
		icon = addSubComponents(icon);
		visitingDuringIconRendering = false;
		return icon;
	}
	eq ShortClassDecl.icon() {
		visitingDuringIconRendering = true;
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		icon = new Icon(qualifiedName(), layer, Context.ICON);
		Icon superIcon = getExtendsClauseShortClass().findClassDecl().icon();
		if (superIcon != Icon.NULL_ICON) {
			icon.addSuperclass(superIcon);
		}
		visitingDuringIconRendering = false;
		return icon;
	}
	eq ClassDecl.diagram() { 
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createDiagramLayer();
		}	
		icon = new Icon(qualifiedName(), layer, Context.DIAGRAM); 
		icon = addSuperClasses(icon);
		icon = addSubComponents(icon);
		return icon;
	}
	eq ShortClassDecl.diagram() {
		visitingDuringIconRendering = true;
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		icon = new Icon(qualifiedName(), layer, Context.DIAGRAM);
		Icon superIcon = getExtendsClauseShortClass().findClassDecl().diagram();
		if (superIcon != Icon.NULL_ICON) {
			icon.addSuperclass(superIcon);
		}
		visitingDuringIconRendering = false;
		return icon;
	}
	eq InstClassDecl.icon() {
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		icon = new Icon(qualifiedName(), layer, Context.ICON);
		icon = addSuperClasses(icon);
		icon = addSubComponents(icon);
		return icon;
	}
	
	eq InstClassDecl.diagram() {
		Icon icon;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = annotation();
		if (annotation.exists()) {
			layer = annotation.createDiagramLayer();
		}
		icon = new Icon(qualifiedName(), layer, Context.DIAGRAM);
		icon = addSuperClasses(icon);
		icon = addSubComponents(icon);
		return icon;
	}
	
	public Image BaseNode.renderIcon(Icon icon) {
		if (icon.isEmpty()) 
			return defaultIcon();
		else
			return new AWTIconDrawer(icon).getImage();
	}
	
	public BufferedImage BaseNode.render(Icon icon, int w, int h) {
		return new AWTIconDrawer(icon, w, h).getAWTImage();
	}
	
	public static final int BaseNode.iconImageSize = 400;
	
	public BufferedImage BaseNode.createIconImage() {
		return render(icon(), iconImageSize, iconImageSize);
	}
	
	syn boolean BaseNode.hasIcon() = !icon().isEmpty();
	
	syn Image BaseNode.defaultIcon() = null;
	eq ClassDecl.defaultIcon() = ImageLoader.getFrequentImage(ImageLoader.GENERIC_CLASS_IMAGE);

	eq ClassDecl.contentOutlineImage()         = cachedIcon();
	eq ComponentDecl.contentOutlineImage()     = cachedIcon();
	eq InstClassDecl.contentOutlineImage()     = cachedIcon();
	eq InstComponentDecl.contentOutlineImage() = cachedIcon();
	
	syn boolean ASTNode.contentOutlineImageCalculated() = true;
	eq BaseNode.contentOutlineImageCalculated()         = myCachedIcon != null && myCachedIcon.iscurrent;
	
	public void ASTNode.updateCachedIcon() {}
	
	
   	// Add caching with delayed calculation for icons
	public class BaseNode {
		
		private CachedIcon myCachedIcon = null;
		
		private CachedIcon myCachedIcon() {
			if (myCachedIcon == null)
				myCachedIcon = new CachedIcon();
			return myCachedIcon;
		}
		
		public Image cachedIcon() {
			return myCachedIcon().icon;
		}
		
		public void updateCachedIcon() {
			myCachedIcon();
			try {
				myCachedIcon.icon = renderIcon(icon());
			} catch (Exception e) {
				// Can't really do anything constructive here
				e.printStackTrace();
			}
			myCachedIcon.iscurrent = true;
		}

		private static class CachedIcon {
			public Image icon = null;
			public boolean iscurrent = false;
		}
	}
	
	private Icon ClassDecl.addSuperClasses(Icon icon) {	
		for(ExtendsClause ext: superClasses()) {
			ClassDecl superClass = ext.findClassDecl();
			if(superClass.visitingDuringIconRendering) {		
				continue;
			}
			Icon superIcon;
			if (icon.getContext().equals(Context.ICON)) {
				superIcon = superClass.icon();
			} else {
				superIcon = superClass.diagram();
			}
			if(superIcon != Icon.NULL_ICON) {
				icon.addSuperclass(superIcon);
			}
		}
		
		return icon;
	}
	
	private Icon ClassDecl.addSubComponents(Icon icon) {
		for(ComponentDecl componentDecl : components()) {
			ClassDecl subDecl = componentDecl.findClassDecl();
			if(subDecl.isUnknown() || subDecl.visitingDuringIconRendering) {
				continue;
			}
			AnnotationNode compAnnotation = componentDecl.annotation();
			if(compAnnotation.hasPlacement()) {
				try {
					if (subDecl instanceof BaseClassDecl) {
						Context componentContext = Context.ICON;
						boolean isProtected = componentDecl.isProtected();
						boolean isConnector = ((BaseClassDecl)subDecl).getRestriction() instanceof Connector;
						Icon subIcon = Icon.NULL_ICON;
						if (icon.getContext().equals(Context.ICON)) {
							if (isConnector && !isProtected) {
								subIcon = subDecl.icon();
							}
						} else {
							if (isConnector) {
								subIcon = subDecl.diagram();
								componentContext = Context.DIAGRAM;
								
							} else {
								subIcon = subDecl.icon();
							}
						}
						if(subIcon != Icon.NULL_ICON) {
							Placement placement = compAnnotation.createPlacement(componentContext);
							icon.addSubcomponent(
									new Component(subIcon, placement)
							);
						}
					}
				}
				catch(FailedConstructionException fe) {
//					System.out.println(fe.getMessage());
				}
			}
		}
		return icon;
	}
	
	private Icon InstClassDecl.addSuperClasses(Icon icon) {	
		for (InstExtends ie : instExtends()) {
			Icon superIcon;
			InstClassDecl superDecl = ie.myInstClass();
			if (icon.getContext().equals(Context.ICON)) {
				superIcon = superDecl.icon();
			} else {
				superIcon = superDecl.diagram();
			}
			if (superIcon != Icon.NULL_ICON) {
				icon.addSuperclass(superIcon);
			}
	 	}
	 	return icon;
	}

	private Icon InstClassDecl.addSubComponents(Icon icon) {
//		for(InstComponentDecl componentDecl : getInstComponentDecls()) {
//			InstClassDecl subDecl = componentDecl.myInstClass();		
//			//sortera ut unknown classDecl
//			if(subDecl.isUnknown()) {
//				continue;
//			}
//			AnnotationNode placementAnnotation = componentDecl.annotation();
//			if(placementAnnotation.hasPlacement()) {	
//				try {
//					Placement placement = placementAnnotation.createPlacement(icon.getContext());
//					if (subDecl instanceof InstBaseClassDecl) {
//						boolean isProtected = componentDecl.getComponentDecl().isProtected();
//						boolean isConnector = ((InstBaseClassDecl)subDecl).isConnector();
//						Icon subIcon = Icon.NULL_ICON;
//						if (icon.getContext().equals(Context.ICON)) {
//							if (isConnector && !isProtected) {
//								subIcon = subDecl.icon();
//							}
//						} else {
//							if (isConnector) {
//								subIcon = subDecl.diagram();
//							} else {
//								subIcon = subDecl.icon();
//							}
//						}
//						if(!subIcon.equals(Icon.NULL_ICON)) {
//							icon.addSubcomponent(
//									new Component(subIcon, placement)
//							);
//						}
//					}
//				}
//				catch(FailedConstructionException fe) {
//					System.out.println(fe.getMessage());
//				}
//			}
//		}
		return icon;
	}
	
	/*
	 * F�r tidtagning.
	 */
//	eq ClassDecl.contentOutlineImage() {
//		System.out.println("ClassDecl.contentOutlineImage(), Namn = " + name());
//	
//		java.util.Date before = new Date();
//		Icon icon = icon();
//		java.util.Date after = new Date();
//		long duration = after.getTime()-before.getTime();
//		System.out.println("icon() tog " + duration + " ms.");
//		
//		if (icon.equals(Icon.NULL_ICON)) {
//			return ImageLoader.getImage("dummy.png");
//		}
//		
//		before = new Date();
//		Image image = icon.draw(new AWTIconDrawer(icon));
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("draw() tog " + duration + " ms.");
//		
//		return image;
//	}
//	eq ComponentDecl.contentOutlineImage() {
//		
//		System.out.println("ComponentDecl.contentOutlineImage(), Namn = " + name());
//		
//		java.util.Date before = new Date();
//		ClassDecl decl = findClassDecl();
//		java.util.Date after = new Date();
//		long duration = after.getTime()-before.getTime();
//		System.out.println("findClassDecl() tog " + duration + " ms.");
//		
//		before = new Date();
//		Icon icon = icon();
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("icon() tog " + duration + " ms.");
//	
//		if (icon.equals(Icon.NULL_ICON)) {
//			return ImageLoader.getImage("dummy.png");
//		}
//		
//		before = new Date();
//		Image image = icon.draw(new AWTIconDrawer(icon));
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("draw() tog " + duration + " ms.");
//		return image;
//	}
}