/*
    Copyright (C) 2010 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ComplianceCheck {

	/**
	 * \brief Check for code that is not allowed in its current context.
	 * 
	 * Examples would be checking that classes follow the requirements of 
	 * their restriction. 
	 */
	public void ASTNode.complianceCheck() {}
	
	public void FIfWhenClause.complianceCheck() {
		super.complianceCheck();
		if (!getTest().variability().lessOrEqual(fParameter()) && 
				!root().options.getBooleanOption("generate_ode"))
			compliance("Using if statements is currently only supported when compiling FMUs");
	}
	
	public void FWhileStmt.complianceCheck() {
		super.complianceCheck();
		if (!getTest().variability().lessOrEqual(fParameter()) && 
				!root().options.getBooleanOption("generate_ode"))
			compliance("Using while statements is currently only supported when compiling FMUs");
	}
	
	public void InstAccess.complianceCheck() {
		super.complianceCheck();
		if (myInstComponentDecl().isRecord() && hasFArraySubscripts() && inFunction() && 
				!getFArraySubscripts().variability().lessOrEqual(fParameter()) && 
				!root().options.getBooleanOption("generate_ode")) {
			compliance("Using arrays of records with indices of higher than parameter variability is currently only supported when compiling FMUs");
		}
	}

	public void FWhenEquation.complianceCheck() {
		if (!root().options.getBooleanOption("generate_ode")) {
			compliance("When equations are currently only supported when compiling FMUs");
		}
	}

	public void FIfEquation.complianceCheck() {
		if (!shouldEliminateIf())
			compliance("If equations are currently only supported with constant or parameter test expressions");
	}

	public void FUnsupportedEquation.collectErrors() {
		compliance("Unsupported equation type");
	}

	public void FUnsupportedExp.collectErrors() {
		compliance("Unsupported expression type");
	}
	
	public void FWhenStmt.complianceCheck() {
		super.complianceCheck();
		compliance("When statements are not supported");
	}
	
	public void FUnsupportedBuiltIn.complianceCheck() {
		compliance("The " + getName() + "() function-like operator is not supported");
	}
	
	public void FSampleExp.complianceCheck() {
	    if (!root().options.getBooleanOption("generate_ode")) {
			compliance("The sample() function-like operator is supported only when compiling FMUs");
		}
	}

	public void FPreExp.complianceCheck() {
	    if (!root().options.getBooleanOption("generate_ode")) {
			compliance("The pre() function-like operator is supported only when compiling FMUs");
		}
	}
	
	public void FInitialExp.complianceCheck() {
		if (!root().options.getBooleanOption("generate_ode")) {	
			compliance("The initial() function-like operator supported only when compiling FMUs");
		}
	}
	
	public void FIntegerExp.complianceCheck() {
		compliance("The integer() function-like operator is not supported");
	}
	
	public void FIgnoredBuiltIn.complianceCheck() {
		warning("The " + getName() + 
				"() function-like operator is not supported, and is currently ignored");
	}
	
	public void FSmoothExp.complianceCheck() {
		warning("The smooth() function-like operator is not fully supported, unnecessary events may be generated");
	}
	
	public void FDotAddExp.complianceCheck() {
		super.complianceCheck();
		if (type().isString())
			compliance("String concatenation is not supported");
	}
	
	public void InstComponentDecl.complianceCheck() {
		super.complianceCheck();
		if (getComponentDecl().isInner() || getComponentDecl().isOuter())
			compliance("Inner/outer components are not supported");
	}
	
	public void InstPrimitive.complianceCheck() {
		super.complianceCheck();
		if (isString())
			compliance("String variables are not supported");
		if (!root().options.getBooleanOption("generate_ode")) {
			if (isInteger() && !variability().lessOrEqual(fParameter()) && !inFunction())
				compliance("Integer variables are supported only when compiling FMUs (constants and parameters are always supported)");
			if (isBoolean() && !variability().lessOrEqual(fParameter()))
				compliance("Boolean variables are supported only when compiling FMUs (constants and parameters are always supported)");
		}
	}
	
	public void InstEnum.complianceCheck() {
		super.complianceCheck();
		if (!variability().lessOrEqual(fParameter()) && !root().options.getBooleanOption("generate_ode"))
			compliance("Enumeration variables are supported only when compiling FMUs (constants and parameters are always supported)");
	}
	
	public void InstNamedModification.complianceCheck() {
		if (getName().hasFArraySubscripts())
			compliance("Modifiers of specific array elements are not supported");
	}
	
	public void FClass.checkDuplicateVariables() {
		for (String name : nonUniqueVars.keySet()) {
			compliance("The variable " + name + " is declared multiple times and is not " +
			            "identical to other declaration(s) with the same name.");
		}

	}
	
	public void FClass.checkUnsupportedStreamConnections() {
		for (ConnectionSet cse : getConnectionSetManager().getConnectionSetList()) {
			if (cse.numStreamVariables() > 2) {
				compliance("Stream connections with more than two connectors are not supported: " + cse);
			}
		}
	}
	
}