/*
    Copyright (C) 2011 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect SimpleClassLookup {
	
	/*
	 * Warning: This is only a prototype and is currently not tested.
	 *          Use with care.
	 */

	// TODO: Add javadoc

	syn ClassDecl ExtendsClause.findClassDecl()     = getSuper().findClassDecl();
	syn ClassDecl BaseComponentDecl.findClassDecl() = getClassName().findClassDecl();
	syn ClassDecl ImportClause.findClassDecl()      = getPackageName().findClassDecl();
	
	// Used to prevent circular lookups without the repetitions imposed by declaring attributes circular.
	protected boolean Access.duringFindClassDecl = false;
	
	syn lazy ClassDecl Access.findClassDecl() {
		if (duringFindClassDecl)
			return unknownClassDecl();
		duringFindClassDecl = true;
		try {
			return simpleLookupClass(getID());
		} finally {
			duringFindClassDecl = false;
		}
	}
	eq Dot.findClassDecl()          = getRight().findClassDecl();
	eq GlobalAccess.findClassDecl() = getAccess().findClassDecl();
	
	
	inh ClassDecl Access.simpleLookupClass(String name);
	inh ClassDecl ClassDecl.simpleLookupClass(String name);
	eq Dot.getRight().simpleLookupClass(String name)           = 
		getLeft().findClassDecl().simpleLookupClassMemberScope(name);
	eq GlobalAccess.getAccess().simpleLookupClass(String name)      = simpleLookupClassGlobalScope(name);
	eq ImportClause.getPackageName().simpleLookupClass(String name) = simpleLookupClassGlobalScope(name);
	eq FullClassDecl.getSuper().simpleLookupClass(String name)      = simpleLookupClassLocalScope(name);
	eq FullClassDecl.getChild().simpleLookupClass(String name)      = simpleLookupClassDefaultScope(name);
	eq Program.getChild().simpleLookupClass(String name)            = simpleLookupClassDefaultScope(name);
	
	syn ClassDecl ClassDecl.simpleLookupClassDotted(String name) {
		String[] parts = name.split("\\.");
		ClassDecl cd = this;
		for (String part : parts) {
			cd = cd.simpleLookupClass(part);
			if (cd == null)
				return null;
		}
		return cd;
	}
	
	syn ClassDecl Program.simpleLookupClassDotted(String name) {
		String[] parts = name.split("\\.", 2);
		ClassDecl base = simpleLookupClassDefaultScope(parts[0]);
		return (base == null || parts.length == 1) ? base : base.simpleLookupClassDotted(parts[1]);
	}
	
	inh ClassDecl Access.simpleLookupClassGlobalScope(String name);
	inh ClassDecl ImportClause.simpleLookupClassGlobalScope(String name);
	eq Program.getChild().simpleLookupClassGlobalScope(String name) = simpleLookupClassDefaultScope(name);
	
	
	syn lazy ClassDecl Program.simpleLookupClassDefaultScope(String name) {
		for (StoredDefinition sd : getUnstructuredEntitys())
			for (Element e : sd.getElements())
				if (e instanceof ClassDecl && e.matches(name))
					return (ClassDecl) e;
		
		ClassDecl res = simpleLookupInClassList(getPredefinedTypes(), name);
		if (res != null)
			return res;
			
		res = simpleLookupInClassList(getBuiltInTypes(), name);
		if (res != null)
			return res;
			
		res = simpleLookupInClassList(getBuiltInFunctions(), name);
		if (res != null)
			return res;
		
		res = simpleLookupInLibNodeList(getLibNodes(), name);
		return (res != null) ? res : getUnknownClassDecl();
	}
	
	/**
	 * Lookup in contained classes, extended classes, imports and surrounding classes.
	 */
	syn lazy ClassDecl FullClassDecl.simpleLookupClassDefaultScope(String name) {
		ClassDecl res = simpleLookupClassMemberScope(name);
		if (res.isUnknown())
			res = simpleLookupClassInImports(name);
		
		return res.isUnknown() ? simpleLookupClass(name) : res;
	}

	
	/**
	 * Lookup in contained classes, imports and surrounding classes.
	 */
	syn lazy ClassDecl FullClassDecl.simpleLookupClassLocalScope(String name) {
		ClassDecl res = simpleLookupInClassList(getClassDecls(), name);
		if (res == null)
			res = simpleLookupClassInImports(name);
		
		return res.isUnknown() ? simpleLookupClass(name) : res;
	}
	
	
	/**
	 * Lookup in contained classes and extended classes.
	 */
	syn ClassDecl ClassDecl.simpleLookupClassMemberScope(String name) = unknownClassDecl();
	eq UnknownClassDecl.simpleLookupClassMemberScope(String name)     = unknownClassDecl();
	
	syn lazy ClassDecl FullClassDecl.simpleLookupClassMemberScope(String name) {
		ClassDecl res = simpleLookupInClassList(getClassDecls(), name);
		if (res == null)
			res = simpleLookupClassInExtends(name);
		
		return res;
	}
	
	eq LibClassDecl.simpleLookupClassMemberScope(String name) {
		ClassDecl res = super.simpleLookupClassMemberScope(name);
		if (res.isUnknown()) {
			ClassDecl libRes = simpleLookupInLibNodeList(getLibNodes(), name);
			if (libRes != null)
				return libRes;
		}
		return res;
	}
	
	eq ShortClassDecl.simpleLookupClassMemberScope(String name) = 
		getExtendsClauseShortClass().findClassDecl().simpleLookupClassMemberScope(name);
	
	
	syn ClassDecl ImportClause.simpleLookupClassInImport(String name) = 
		matches(name) ? findClassDecl() : unknownClassDecl();
	eq ImportClauseUnqualified.simpleLookupClassInImport(String name) = 
		findClassDecl().simpleLookupClassMemberScope(name);
		

	protected ClassDecl FullClassDecl.simpleLookupClassInImports(String name) {
		ClassDecl res;
		for (ImportClause imp : getImports()) {
			res = imp.simpleLookupClassInImport(name);
			if (!res.isUnknown())
				return res;
		}
		
		return unknownClassDecl();
	}

	protected ClassDecl FullClassDecl.simpleLookupClassInExtends(String name) {
		ClassDecl res;
		for (ExtendsClause sup : getSupers()) {
			res = sup.findClassDecl().simpleLookupClassMemberScope(name);
			if (!res.isUnknown())
				return res;
		}
		
		return unknownClassDecl();
	}
	
	
	/**
	 * Convenience method for looking up a class in a List of ClassDecls.
	 *
	 * Unlike the other lookup methods, this one returns null when the class isn't found.
	 */
	public static ClassDecl ASTNode.simpleLookupInClassList(List<? extends ClassDecl> list, String name) {
		for (ClassDecl cd : list)
			if (cd.matches(name))
				return cd;
		return null;
	}
	
	
	/**
	 * Convenience method for looking up a class in a List of ClassDecls.
	 *
	 * Unlike the other lookup methods, this one returns null when the class isn't found.
	 */
	public static ClassDecl ASTNode.simpleLookupInLibNodeList(List<LibNode> list, String name) {
		for (LibNode ln : list)
			if (ln.matches(name))
				return (ClassDecl) ln.getStoredDefinition().getElement(0);
		return null;
	}
	
	
	eq IdDecl.matches(String str)                = getID().equals(str);
	eq LibNode.matches(String str)               = getName().equals(str);
	eq BaseClassDecl.matches(String str)         = getName().matches(str);
	eq ImportClauseRename.matches(String str)    = getIdDecl().matches(str);
	eq ImportClauseQualified.matches(String str) = 
		getPackageName().getLastAccess().getID().equals(str);

}