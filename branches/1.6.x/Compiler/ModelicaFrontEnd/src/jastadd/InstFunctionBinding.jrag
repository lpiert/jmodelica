/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.HashMap;


aspect InstFunctionBinding {
	
	/**
	 * \brief Finds which, if any, of the inputs for its function a single argument should 
	 * bind to, and adds it to an array in the corresponding place.
	 * 
	 * If the place in the array is taken, an InstMultipleBoundArgument containing both arguments 
	 * is placed there.
	 * 
	 * @param arr     the array to place the argument in
	 * @param inputs  the list of inputs for the function
	 * @return        <code>true</code> it the position for the argument could be found, 
	 *                <code>false</code> otherwise.
	 */
	public boolean InstFunctionArgument.bindArgument(InstFunctionArgument[] arr, 
			                                         ArrayList<InstComponentDecl> inputs) {
		int pos = findArgumentPosition(inputs);
		if (pos < 0 || pos >= arr.length)
			return false;
		
		if (arr[pos] == null)
			arr[pos] = (InstFunctionArgument) fullCopy();
		else
			arr[pos] = arr[pos].addMultipleBoundArgument(this);
		
		arr[pos].setBoundInput(inputs.get(pos));
		return true;
	}
	
	/**
	 * \brief Return an InstMultipleBoundArgument containing both <code>this</code> and <code>arg</code>.
	 * 
	 * If <code>this</code> is an InstMultipleBoundArgument, <code>arg</code> is simply added to it.
	 */
	protected InstMultipleBoundArgument InstFunctionArgument.addMultipleBoundArgument(InstFunctionArgument arg) {
		return new InstMultipleBoundArgument(new List().add(this).add(arg));
	}
	
	protected InstMultipleBoundArgument InstMultipleBoundArgument.addMultipleBoundArgument(InstFunctionArgument arg) {
		addArg(arg);
		return this;
	}
	
	/**
	 * \brief Return the position of the input the argument corresponds to.
	 */
	protected int InstFunctionArgument.findArgumentPosition(ArrayList<InstComponentDecl> inputs) {
		return -1;
	}
	
	protected int InstPositionalArgument.findArgumentPosition(ArrayList<InstComponentDecl> inputs) {
		return getPos();
	}
	
	protected int InstNamedArgument.findArgumentPosition(ArrayList<InstComponentDecl> inputs) {
		int i = 0;
		for (InstComponentDecl in : inputs) {
			if (getName().name().equals(in.name()))
				return i;
			i++;
		}
		return -1;
	}
	
	/**
	 * \brief Check if the argument was explicitly given in the call.
	 */
	syn boolean InstFunctionArgument.isGiven() = false;
	eq InstPositionalArgument.isGiven() = true;
	eq InstNamedArgument.isGiven()      = true;
	
	/**
	 * \brief The input component this argument is bound to.
	 */
	protected InstComponentDecl InstFunctionArgument.boundInput = null;
	
	/**
	 * \brief Set the input component this argument is bound to.
	 */
	public void InstFunctionArgument.setBoundInput(InstComponentDecl input) {
		boundInput = input;
	}
	
	public void InstMultipleBoundArgument.setBoundInput(InstComponentDecl input) {
		boundInput = input;
		for (InstFunctionArgument arg : getArgs())
			arg.setBoundInput(input);
	}
	
	/**
	 * \brief Get the input component this argument is bound to.
	 */
	syn InstComponentDecl InstFunctionArgument.getBoundInput() = boundInput;
	
	/**
	 * \brief Argument nodes representing arguments that are missing or given multiple times do 
	 * not get a location set when they are created. Set the location for those nodes, do nothing 
	 * for other nodes.
	 */
	public void InstFunctionArgument.setErrorArgLocation(ASTNode node) {}
	public void InstMissingArgument.setErrorArgLocation(ASTNode node) { setLocation(node); }
	public void InstMultipleBoundArgument.setErrorArgLocation(ASTNode node) { setLocation(node); }
	
	/**
	 * \brief Returns <code>false</code> if this argument node represents an error, 
	 * e.g. a missing argument.
	 */
	syn boolean InstFunctionArgument.isOKArg() = true;
	eq InstMissingArgument.isOKArg() = false;
	eq InstMultipleBoundArgument.isOKArg() = false;
	
	/**
	 * \brief List of arguments that could not be bound to inputs.
	 */
	private ArrayList<InstFunctionArgument> InstFunctionCall.unbindableArgs = 
		new ArrayList<InstFunctionArgument>();
	
	private ArrayList<InstFunctionArgument> FBuiltInFunctionCall.originalArgs = 
		new ArrayList<InstFunctionArgument>();
	public void FBuiltInFunctionCall.addToOriginalArgs(InstFunctionArgument arg) { 
		originalArgs.add(arg);
	}

	syn lazy List<InstFunctionArgument> FBuiltInFunctionCall.getOriginalArgList() {
		List<InstFunctionArgument> res = new List<InstFunctionArgument>();
		for (InstFunctionArgument arg : originalArgs)
			res.add(arg);
		return res;
	}
	
	/**
	 * \brief Called for inputs that no argument was bound to. Creates an InstDefaultArgument 
	 * if the input has a default value and an InstMissingArgument otherwise.
	 */
	public InstFunctionArgument InstComponentDecl.createInstDefaultArgument() {
		InstFunctionArgument arg = null;
		if (hasInstModification() && getInstModification() instanceof InstValueModification) {
			InstValueModification mod = (InstValueModification) getInstModification();
			FExp exp = mod.getFExp();
			arg = new InstDefaultArgument(exp);
		} else {
			arg = new InstMissingArgument();
		}
		arg.setBoundInput(this);
		return arg;
	}
	
	/**
	 * \brief Add a new argument to list of arguments. (Only valid for some subclasses.)
	 * 
	 * Default implementation does nothing.
	 */
	public void FAbstractFunctionCall.addArgument(InstFunctionArgument arg) {}
	
	/**
	 * \brief Add a new argument to list of arguments.
	 * 
	 * This implementation adds the argument as a child.
	 */
	public void InstFunctionCall.addArgument(InstFunctionArgument arg) {
		addArg(arg);
	}
	
	/**
	 * \brief Add a new argument to list of arguments.
	 * 
	 * This implementation adds the argument to the original arguments 
	 * and the expression from the argument as a child.
	 */
	public void FRecordConstructor.addArgument(InstFunctionArgument arg) {
		addArg((FExp) arg.getFExp().fullCopy());
		addToOriginalArgs(arg);
	}
	
	public class InstFunctionCall {
		
		private static BuiltInTranslator builtInTranslator = new BuiltInTranslator();
		
		/**
		 * \brief Helper class for rewrite InstFunctionCall -> FAbstractFunctionCall. 
		 * 
		 * Creates an instance of the specific subclass of FBuiltInFunctionCall matching 
		 * the name of the function, if one matches. 
		 */
		private static class BuiltInTranslator {
			
			private HashMap<String, BuiltInCreator> map;
			private static final String MATH_PREF = "Modelica.Math.";
			private static final int MATH_LEN = MATH_PREF.length();
			
			public FBuiltInFunctionCall create(String name, InstFunctionArgument[] args) {
				boolean math = name.startsWith(MATH_PREF);
				if (math) 
					name = name.substring(MATH_LEN);
				BuiltInCreator creator = map.get(name);
				if (creator == null || (math && !creator.matchMath))
					return null;
				FBuiltInFunctionCall res = creator.create(args);
				if (res != null)
					for (InstFunctionArgument arg : args)
						res.addToOriginalArgs(arg);
				return res;
			}
			
			public boolean isInfArgs(String name) {
				BuiltInCreator creator = map.get(name);
				return creator != null && creator.isInfArgs();
			}
			
			public BuiltInTranslator() {
				map = new HashMap<String, BuiltInCreator>();
				//       name                              math       node               # args
				map.put("sin",          new BuiltInCreator(true,  new FSinExp(),         1));
				map.put("cos",          new BuiltInCreator(true,  new FCosExp(),         1));
				map.put("tan",          new BuiltInCreator(true,  new FTanExp(),         1));
				map.put("asin",         new BuiltInCreator(true,  new FAsinExp(),        1));
				map.put("acos",         new BuiltInCreator(true,  new FAcosExp(),        1));
				map.put("atan",         new BuiltInCreator(true,  new FAtanExp(),        1));
				map.put("atan2",        new BuiltInCreator(true,  new FAtan2Exp(),       2));
				map.put("sinh",         new BuiltInCreator(true,  new FSinhExp(),        1));
				map.put("cosh",         new BuiltInCreator(true,  new FCoshExp(),        1));
				map.put("tanh",         new BuiltInCreator(true,  new FTanhExp(),        1));
				map.put("exp",          new BuiltInCreator(true,  new FExpExp(),         1));
				map.put("log",          new BuiltInCreator(true,  new FLogExp(),         1));
				map.put("log10",        new BuiltInCreator(true,  new FLog10Exp(),       1));
				map.put("noEvent",      new BuiltInCreator(false, new FNoEventExp(),     1));
				map.put("smooth",       new BuiltInCreator(false, new FSmoothExp(),      2));
				map.put("pre",          new BuiltInCreator(false, new FPreExp(),         1));
				map.put("sample",       new BuiltInCreator(false, new FSampleExp(),      2));
				map.put("initial",      new BuiltInCreator(false, new FInitialExp(),     0));
				map.put("identity",     new BuiltInCreator(false, new FIdentity(),       1));
				map.put("sqrt",         new BuiltInCreator(false, new FSqrtExp(),        1));
				map.put("transpose",    new BuiltInCreator(false, new FTranspose(),      1));
				map.put("cross",        new BuiltInCreator(false, new FCross(),          2));
				map.put("sum",          new BuiltInCreator(false, new FSumExp(),         1));
				map.put("abs",          new BuiltInCreator(false, new FAbsExp(),         1));
				map.put("ndims",        new BuiltInCreator(false, new FNdimsExp(),       1));
				map.put("linspace",     new BuiltInCreator(false, new FLinspace(),       3));
				map.put("integer",      new BuiltInCreator(false, new FIntegerExp(),     1));
				map.put("Integer",      new BuiltInCreator(false, new FEnumIntegerExp(), 1));
				map.put("inStream",     new BuiltInCreator(false, new FInStream(),       1));
				map.put("actualStream", new BuiltInCreator(false, new FActualStream(),   1));
				//       name                                         node           # fixed args
				map.put("array",        new InfArgsBuiltInCreator(new FParseArray(), 0));
				map.put("ones",         new InfArgsBuiltInCreator(new FOnes(),       0));
				map.put("zeros",        new InfArgsBuiltInCreator(new FZeros(),      0));
				map.put("fill",         new InfArgsBuiltInCreator(new FFillExp(),    1));
				map.put("cat",          new InfArgsBuiltInCreator(new FCatExp(),     1));
				//       name                                         node        min/max # args
				map.put("size",         new VarArgsBuiltInCreator(new FSizeExp(), 1, 2));
				map.put("min",          new VarArgsBuiltInCreator(new FMinExp(),  1, 2));
				map.put("max",          new VarArgsBuiltInCreator(new FMaxExp(),  1, 2));
				
				String[] unsupported = new String[] { 
						// Ticket 678: Support for array related functions
						"scalar", "vector", "matrix", "diagonal", "product", "outerProduct", "symmetric", "skew", 
						// Ticket 664: Support all function-like operators
						"sign", "String", "div", "mod", "rem", "ceil", "floor", "delay", "cardinality", 
						"semiLinear", "terminal", "smooth", "edge", "reinit", "terminate"
					};
				for (String name : unsupported)
					if (!map.containsKey(name))
						map.put(name, new UnsupportedBuiltInCreator(name));
				
				String[] ignored = new String[] { 
						// Ticket 664: Support all function-like operators
						"assert"
					};
				for (String name : ignored)
					if (!map.containsKey(name))
						map.put(name, new IgnoredBuiltInCreator(name));
			}
			
			private static class BuiltInCreator {
				
				public boolean matchMath;
				protected FBuiltInFunctionCall template;
				protected int numArgs;
				
				public BuiltInCreator(boolean math, FBuiltInFunctionCall tmpl, int nArgs) { 
					matchMath = math;
					template = tmpl;
					numArgs = nArgs;
				}
				
				public FBuiltInFunctionCall create(InstFunctionArgument[] args) {
					if (args.length < numArgs)
						return null;
					FBuiltInFunctionCall res = template.createEmptyNode();
					setArguments(res, args);
					return res;
				}
				
				protected void setArguments(FBuiltInFunctionCall res, InstFunctionArgument[] args) {
					for (int i = 0; i < numArgs; i++)
						res.setChild(extractFExp(args[i]), i);
				}
				
				public boolean isInfArgs() {
					return false;
				}
				
				protected static FExp extractFExp(InstFunctionArgument arg) {
					return (FExp) arg.getFExp().fullCopy();
				}
				
			}
			
			private static class InfArgsBuiltInCreator extends BuiltInCreator {
				
				public InfArgsBuiltInCreator(FBuiltInFunctionCall tmpl, int nFixed) {
					super(false, tmpl, nFixed);
				}
				
				protected void setArguments(FBuiltInFunctionCall res, InstFunctionArgument[] args) {
					List<FExp> exps = new List<FExp>();
					for (int i = numArgs; i < args.length; i++)
						exps.add(extractFExp(args[i]));
					res.setChild(exps, 0);
					for (int i = 0; i < numArgs; i++)
						res.setChild(extractFExp(args[i]), i + 1);
				}
				
				public boolean isInfArgs() {
					return true;
				}
				
			}
			
			private static class VarArgsBuiltInCreator extends BuiltInCreator {
				
				private int maxArgs;
				
				public VarArgsBuiltInCreator(FBuiltInFunctionCall tmpl, int minArgs, int maxArgs) {
					super(false, tmpl, minArgs);
					this.maxArgs = maxArgs;
				}
				
				protected void setArguments(FBuiltInFunctionCall res, InstFunctionArgument[] args) {
					super.setArguments(res, args);
					for (int i = numArgs; i < maxArgs; i++)
						res.setChild(args[i].isGiven() ? new Opt(extractFExp(args[i])) : new Opt(), i);
				}
				
			}
			
			private static class UnsupportedBuiltInCreator extends InfArgsBuiltInCreator {
				
				private static final FUnsupportedBuiltIn tmpl = new FUnsupportedBuiltIn();
				
				private String name;
				
				public UnsupportedBuiltInCreator(String name) {
					this(name, tmpl);
				}
				
				protected UnsupportedBuiltInCreator(String name, FUnsupportedBuiltIn tmpl2) {
					super(tmpl2, 0);
					this.name = name;
				}
				
				protected void setArguments(FBuiltInFunctionCall res, InstFunctionArgument[] args) {
					((FUnsupportedBuiltIn) res).setName(name);
				}
				
			}
			
			private static class IgnoredBuiltInCreator extends UnsupportedBuiltInCreator {
				
				private static final FIgnoredBuiltIn tmpl = new FIgnoredBuiltIn();
				
				public IgnoredBuiltInCreator(String name) {
					super(name, tmpl);
				}
				
			}
			
		}
		
	}

	/**
	 * \brief Bind arguments to inputs and replace InstFunctionCall with specialized nodes 
	 * for built-in functions.
	 * 
	 * If arguments could not be bound to inputs properly (to few args, to many args, bad 
	 * named args, etc), then the node remains an InstFunctionCall.
	 * 
	 * Any arguments that could not be bound to an input are gathered in unbindableArgs.
	 * 
	 * If function call was rewritten to a FBuiltinFunctionCall, then the InstFunctionArgument 
	 * nodes describing them are gathered in originalArgs.
	 */
	boolean InstFunctionCall.rewritten = false;
	rewrite InstFunctionCall {
    	when (!rewritten) to FAbstractFunctionCall {
		    // Make sure we only do this once.
		    rewritten = true;

		    // Get info
		    InstClassDecl target = getName().myInstClassDecl();
	    	String name = target.qualifiedName();
		    if (target.isPrimitive()) {
		    	// Look for conversion operator with same name as primitive class (if one exists)
				HashSet set = getName().lookupInstClass("!" + target.name());
				if (set.size() > 0)
					target = (InstClassDecl) set.iterator().next();
		    }
    		ArrayList<InstComponentDecl> inputs = myInputs(target);
		    
		    /* If we can't resolve the name as a function or record, don't try to bind arguments, 
		     * just leave it for the error check. */
		    if (!target.isCallable())
		    	return this;

	    	// Check if we should try to bind arguments
	    	InstFunctionArgument[] temp;
	    	boolean argsOK = true;
	    	if (builtInTranslator.isInfArgs(name)) {
	    		
	    		// Just collect all positional arguments
	    		ArrayList<InstFunctionArgument> args = new ArrayList<InstFunctionArgument>();
	    		for (InstFunctionArgument arg : getArgs()) {
	    			if (arg instanceof InstPositionalArgument)
	    				args.add(arg);
	    			else
	    				unbindableArgs.add(arg);
	    		}
	    		
	    		// Make sure we have any fixed arguments
	    		for (int i = args.size(); i < inputs.size(); i++) {
	    			InstMissingArgument arg = new InstMissingArgument();
	    			arg.setBoundInput(inputs.get(i));
	    			args.add(arg);
	    			argsOK = false;
	    		}
	    		temp = args.toArray(new InstFunctionArgument[args.size()]);
	    		
	    	} else {
		    
	    		// Bind arguments
	    		temp = new InstFunctionArgument[inputs.size()];
	    		for (InstFunctionArgument arg : getArgs()) 
	    			if (!arg.bindArgument(temp, inputs))
	    				unbindableArgs.add(arg);
		    		
	    		// Add default argument and check for problems.
	    		for (int i = 0; i < temp.length; i++) {
	    			if (temp[i] == null) 
	    				temp[i] = inputs.get(i).createInstDefaultArgument();
	    			temp[i].setErrorArgLocation(this);
	    			argsOK = argsOK && temp[i].isOKArg();
	    		}
		
	    	}
	    	argsOK = argsOK && unbindableArgs.isEmpty();
	    		
    		// Create replacement node.
		    FAbstractFunctionCall n = null;
		    if (argsOK) 
		    	n = builtInTranslator.create(name, temp);
		    if (n == null) {
				if (target.isRecord() && argsOK) {
					FIdUse rec = new FIdUseInstAccess(new FQName(), getName());
					n = new FRecordConstructor(rec, new List());
				} else {
			    	InstFunctionCall ifc = new InstFunctionCall(getName(), new List());
					ifc.unbindableArgs = unbindableArgs;
					ifc.rewritten = true;
					n = ifc;
				}
				for (InstFunctionArgument arg : temp)
					n.addArgument(arg);
			}
			n.setLocation(this);
			
			return n;	
    	}
  	}
	
	/**
	 * \brief Replace initial node for construct "array(...)" with permanent one.
	 */
	rewrite FParseArray {
    	when (!isFunctionCallClause()) to FLongArray {
    		return new FLongArray(getFExps());
    	}
	}
	
}