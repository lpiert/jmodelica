/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import java.net.URI;

aspect CompileInIDE {
	
	/*
	 * Core needs this for lookup in registry. 
	 */
	eq StoredDefinition.hasLookupKey() = true;
	eq StoredDefinition.lookupKey() = fileName();

	/*
	 * We can refine child AST lookup since we know the possible places they can be
	 * and some things about the keys.
	 */
	eq Program.lookupChildAST(String key) {
		for (StoredDefinition sd : getUnstructuredEntitys()) {
			if (key.equals(sd.fileName()))
				return sd;
		}
		for (LibNode ln : getLibNodes()) {
			IASTNode childMatch = ln.lookupChildAST(key);
			if (childMatch != null) 
				return childMatch;
		}
		return null;
	}
	eq LibNode.lookupChildAST(String key)      = 
		key.startsWith(getFileName()) ?	getStoredDefinition().lookupChildAST(key) : null;
	eq ClassDecl.lookupChildAST(String key)    = null;
	eq LibClassDecl.lookupChildAST(String key) = getLibNodes().lookupChildAST(key);
	
	
	/*
	 * IJastAddNode requires this for some reason.
	 */
	public String ASTNode.getFileName() { return fileName(); }
	
	/*
	 * A link between an AST node and an IFile is useful in e.g. error collection
	 */
	private IFile StoredDefinition.file = null;
	public void StoredDefinition.setFile(IFile file) {
		this.file = file;
		if (file != null)
			setFileName(file.getRawLocation().toOSString());
	}
	public IFile StoredDefinition.getFile() {
		if (file == null) {
			String path = fileName();
			IWorkspaceRoot workspace = ResourcesPlugin.getWorkspace().getRoot();
			if (path.startsWith(workspace.getRawLocation().toOSString())) {
				URI uri = new File(path).toURI();
				IFile files[] = workspace.findFilesForLocationURI(uri);
				if (files.length > 0)
					file = files[0];
			}
		}
		return file;
	}
	
	/*
	 * Connects each SourceRoot to the project it is associated with.
	 */
	private IProject SourceRoot.project = null;
	public void SourceRoot.setProject(IProject proj) {
		project = proj;
	}
	public IProject SourceRoot.getProject() {
		return project;
	}
	
	public void ASTNode.forceRewrites() {
		for (ASTNode child : this)
			child.forceRewrites();
	}
	
	/*
	 * Workaround for bug where a node replaced in the ast by recompilation is an NTA.
	 * The base replaceWith() fails in this case.
	 */
	public void StoredDefinition.replaceWith(IASTNode node) {
		ASTNode parent = getParent();
		if (parent instanceof LibNode) {
			LibNode ln = (LibNode) parent;
			ln.setChild((ASTNode) node, ln.getStoredDefinitionChildPosition());
		} else {
			super.replaceWith(node);
		}
	}

	/*
	 * Core ignores root nodes that doesn't return true for this. 
	 * Might need to actually check later.
	 */
	eq SourceRoot.isProjectAST() = true;
}