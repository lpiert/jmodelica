aspect InnerOuterComponents {
	
	inh boolean InstNode.inOuter();
	eq InstRoot.getChild().inOuter()          = false;
	eq SourceRoot.getChild().inOuter()        = false;
	eq InstComponentDecl.getChild().inOuter() = inOrIsOuter();
	
	inh InstComponentDecl InstComponentDecl.surroundingOuterComponentDecl();
	eq InstRoot.getChild().surroundingOuterComponentDecl()          = null;
	eq SourceRoot.getChild().surroundingOuterComponentDecl()        = null;
	eq InstComponentDecl.getChild().surroundingOuterComponentDecl() = isOuter() ? this : surroundingOuterComponentDecl();
			
	syn boolean InstComponentDecl.inOrIsOuter() = isOuter() || inOuter();	
			
	syn lazy InstComponentDecl InstComponentDecl.myInnerInstComponentDecl() {
		InstComponentDecl res = null;
		if (isOuter()) 
			res = lookupInnerInstComponent(name(), true);
		else if (inOuter())
			res = lookupInInnerInstComponent(name());
		return (res == null) ? unknownInstComponentDecl() : res;
	}
	
	inh InstComponentDecl InstNode.lookupInnerInstComponent(String name, boolean firstScope);
	
	eq InstRoot.getChild().lookupInnerInstComponent(String name, boolean firstScope)   = null;
	eq SourceRoot.getChild().lookupInnerInstComponent(String name, boolean firstScope) = null;
	
	eq InstNode.getChild().lookupInnerInstComponent(String name, boolean firstScope) {
		if (!firstScope) {
			InstComponentDecl icd = genericLookupInstComponent(name);
			if (icd != null && icd.isInner())
				return icd;
		}
		return lookupInnerInstComponent(name, false);
	}
	
	inh InstComponentDecl InstNode.lookupInInnerInstComponent(String name);
	
	eq InstRoot.getChild().lookupInInnerInstComponent(String name) = null;
	eq SourceRoot.getChild().lookupInInnerInstComponent(String name) = null;
	
	eq InstComponentDecl.getChild().lookupInInnerInstComponent(String name) = myInnerInstComponentDecl().memberInstComponent(name);
	
}

aspect InnerOuterClasses {
	
	eq InstClassDecl.getChild().inOuter() = isOuter()? true: inOuter();
			
	syn boolean InstClassDecl.inOrIsOuter() = isOuter() || inOuter();	
			
	syn InstClassDecl InstClassDecl.myInnerInstClassDecl() {
		return isOuter() ? lookupInnerInstClass(name(), true) : null;
	}
	
	inh InstClassDecl InstNode.lookupInnerInstClass(String name, boolean firstScope);
	
	eq InstRoot.getChild().lookupInnerInstClass(String name, boolean firstScope)  = null;
	eq SourceRoot.getChild().lookupInnerInstClass(String name, boolean firstScope) = null;
	
	eq InstNode.getChild().lookupInnerInstClass(String name, boolean firstScope) {
		if (!firstScope) {
			InstClassDecl icd = genericLookupInstClass(name);
			if (icd != null && icd.isInner())
				return icd;
		}
		return lookupInnerInstClass(name, false);
	}
}