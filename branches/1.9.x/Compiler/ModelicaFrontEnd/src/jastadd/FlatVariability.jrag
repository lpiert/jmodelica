/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

aspect FlatVariability {
	
	syn boolean FTypePrefixVariability.constantVariability() = false;
	eq FConstant.constantVariability() = true;	
	syn boolean FTypePrefixVariability.parameterVariability() = false;
	eq FParameter.parameterVariability() = true;	
	syn boolean FTypePrefixVariability.discreteVariability() = false;
	eq FDiscrete.discreteVariability() = true;	
	syn boolean FTypePrefixVariability.continuousVariability() = false;
	eq FContinuous.continuousVariability() = true;	

	syn boolean TypePrefixVariability.constantVariability() = false;
	eq Constant.constantVariability() = true;	
	syn boolean TypePrefixVariability.parameterVariability() = false;
	eq Parameter.parameterVariability() = true;	
	syn boolean TypePrefixVariability.discreteVariability() = false;
	eq Discrete.discreteVariability() = true;	
	syn boolean TypePrefixVariability.continuousVariability() = false;
	eq Continuous.continuousVariability() = true;
	
	
	/**
	 * \brief Test if variablilty is at most the same as <code>other</code>.
	 * 
	 * Uses ordering of variabilities imposed by {@link #variabilityLevel()}.
	 */
	syn boolean FTypePrefixVariability.lessOrEqual(FTypePrefixVariability other) = 
		variabilityLevel() <= other.variabilityLevel();
	
	syn boolean FTypePrefixVariability.structuralOrLess() = lessOrEqual(fStructParameter());
	syn boolean FTypePrefixVariability.parameterOrLess()  = lessOrEqual(fParameter());
	syn boolean FTypePrefixVariability.discreteOrLess()   = lessOrEqual(fDiscrete());
	syn boolean FTypePrefixVariability.continuousOrLess() = lessOrEqual(fContinuous());
	
	/**
	 * \brief An ordering of the variability types.
	 * 
	 * To be used by methods for comparing variabilities. 
	 * Should <em>never</em> be compared to literals, only to the return value from other 
	 * FTypePrefixVariability objects. This simplifies adding new variabilities.
	 *  
	 * Also used to determine the behaviour of {@link #combine(FTypePrefixVariability)}.
	 */
	abstract protected int FTypePrefixVariability.variabilityLevel();
	protected int FConstant.variabilityLevel()        { return VARIABILITY_LEVEL; }
	protected int FStructParameter.variabilityLevel() { return VARIABILITY_LEVEL; }
	protected int FParameter.variabilityLevel()       { return VARIABILITY_LEVEL; }
	protected int FDiscrete.variabilityLevel()        { return VARIABILITY_LEVEL; }
	protected int FContinuous.variabilityLevel()      { return VARIABILITY_LEVEL; }
	protected static final int FConstant.VARIABILITY_LEVEL        = 0;
	protected static final int FStructParameter.VARIABILITY_LEVEL = 4;
	protected static final int FParameter.VARIABILITY_LEVEL       = 10;
	protected static final int FDiscrete.VARIABILITY_LEVEL        = 20;
	protected static final int FContinuous.VARIABILITY_LEVEL      = 30;
	
	/**
	 * \brief An ordering of the variability types.
	 * 
	 * To be used by methods for comparing variabilities. 
	 * Should <em>never</em> be compared to literals, only to the return value from other 
	 * FTypePrefixVariability objects. This simplifies adding new variabilities.
	 *  
	 * Also used to determine the behaviour of {@link #combine(FTypePrefixVariability)}.
	 */
	abstract protected int TypePrefixVariability.variabilityLevel();
	protected int Constant.variabilityLevel()   { return VARIABILITY_LEVEL; }
	protected int Parameter.variabilityLevel()  { return VARIABILITY_LEVEL; }
	protected int Discrete.variabilityLevel()   { return VARIABILITY_LEVEL; }
	protected int Continuous.variabilityLevel() { return VARIABILITY_LEVEL; }
	protected static final int Constant.VARIABILITY_LEVEL   = 0;
	protected static final int Parameter.VARIABILITY_LEVEL  = 10;
	protected static final int Discrete.VARIABILITY_LEVEL   = 20;
	protected static final int Continuous.VARIABILITY_LEVEL = 30;
	
	syn FTypePrefixVariability FAbstractEquation.variability() = fContinuous();
	eq FEquation.variability() = getLeft().variability().combine(getRight().variability());
	eq FFunctionCallEquation.variability() {
		FTypePrefixVariability var = fConstant();
		// Check all output arguments
		for (FFunctionCallLeft fl : getLefts()) {
			if (fl.hasFExp()) {
				var = var.combine(fl.getFExp().variability());
			}	
		}
		// Check all input arguments
		for (FVariable fv : referencedFVariablesInRHS()) {
			var = var.combine(fv.variability());
		}
		return var;
	}
	
	syn boolean FAbstractEquation.isConstant() = false;
	syn boolean FAbstractEquation.isParameter() = false;
	syn boolean FAbstractEquation.isDiscrete() = false;
	syn boolean FAbstractEquation.isContinuous() = false;
	
	eq FEquation.isConstant() = variability().constantVariability();
	eq FEquation.isParameter() = variability().parameterVariability();
	eq FEquation.isDiscrete() = variability().discreteVariability();
	eq FEquation.isContinuous() = variability().continuousVariability();

	eq FFunctionCallEquation.isConstant() = variability().constantVariability();
	eq FFunctionCallEquation.isParameter() = variability().parameterVariability();
	eq FFunctionCallEquation.isDiscrete() = variability().discreteVariability();
	eq FFunctionCallEquation.isContinuous() = variability().continuousVariability();
	
	syn boolean FAbstractVariable.isConstant()   = false;
    syn boolean FAbstractVariable.isParameter()  = false;
    syn boolean FAbstractVariable.isDiscrete()   = false;
    syn boolean FAbstractVariable.isContinuous() = false;
	
	eq FVariable.isConstant()   = variability().constantVariability();
    eq FVariable.isParameter()  = variability().parameterVariability();
    eq FVariable.isDiscrete()   = variability().discreteVariability();
    eq FVariable.isContinuous() = variability().continuousVariability();
    
    
    syn lazy TypePrefixVariability InstComponentDecl.definedVariability() {
    	ComponentDecl cd = getComponentDecl();
    	TypePrefixVariability var = cd.hasTypePrefixVariability() ? 
    			cd.getTypePrefixVariability() : continuous();
    	return overrideVariability(var);
    }
    
    inh TypePrefixVariability InstComponentDecl.overrideVariability(TypePrefixVariability var);
    eq InstNode.getChild().overrideVariability(TypePrefixVariability var)      = var;
    eq InstPrimitive.getChild().overrideVariability(TypePrefixVariability var) = var;
    eq InstComponentDecl.getInstComponentDecl().overrideVariability(TypePrefixVariability var) =
    	var.combineDown(definedVariability());
    
    /* Using combinedVariability() for constant & parameter to avoid circular dependency.
     * It can only differ from variability() when combinedVariability() gives continuous. */
	syn boolean InstComponentDecl.isConstant()   = combinedVariability().constantVariability();
	syn boolean InstComponentDecl.isParameter()  = combinedVariability().parameterVariability();
	syn boolean InstComponentDecl.isDiscrete()   = variability().discreteVariability();
	syn boolean InstComponentDecl.isContinuous() = variability().continuousVariability();
	
	syn FTypePrefixVariability InstComponentDecl.combinedVariability() = 
		definedVariability().flatten().combineDown(defaultVariability());
	eq InstAssignable.combinedVariability() = 
		isStructuralParam ? fStructParameter() : super.combinedVariability();
	
	syn lazy FTypePrefixVariability InstComponentDecl.variability() {
		FTypePrefixVariability var = combinedVariability();
		if (!var.discreteOrLess() && isAssignedInWhen())
			var = fDiscrete();
		return var;
	}
	eq InstEnumLiteral.variability() = fConstant();
	
	syn FTypePrefixVariability InstComponentDecl.defaultVariability() = fConstant();
	eq InstPrimitive.defaultVariability() {
		if (isReal())
			return fContinuous();
		else if (isExternalObject())
			return fParameter();
		else
			return fDiscrete();
	}
	eq InstRecord.defaultVariability() {
		InstComponentDecl rec = this;
		if (isArray()) {
			if (getNumInstComponentDecl() == 0)
				return fConstant();
			rec = getInstComponentDecl(0);
		}
		FTypePrefixVariability var = fConstant();
		for (InstComponentDecl icd : rec.allInstComponentDecls()) 
			var = var.combine(icd.variability());
		return var;
	}
	
	
	syn boolean InstComponentDecl.isAssignedInWhen() = retrieveAssignedInWhenSet().contains(this);
	
	inh Set<InstComponentDecl> InstComponentDecl.retrieveAssignedInWhenSet();
	eq InstClassDecl.getChild().retrieveAssignedInWhenSet() = assignedInWhenSet();
	eq Root.getChild().retrieveAssignedInWhenSet()          = Collections.emptySet();
	
	syn lazy Set<InstComponentDecl> InstNode.assignedInWhenSet() {
		Set<InstComponentDecl> res = new HashSet<InstComponentDecl>();
		for (InstComponentDecl icd : getInstComponentDecls())
			res.addAll(icd.assignedInWhenSet());
		for (FAbstractEquation eqn : getFAbstractEquations())
			if (eqn instanceof FWhenEquation)
				res.addAll(eqn.assignedSet());
		return res;
	}
	
	protected static Set<InstComponentDecl> FAbstractEquation.assignedSetFromEqns(List<FAbstractEquation> eqns) {
		Set<InstComponentDecl> res = new HashSet<InstComponentDecl>();
		for (FAbstractEquation eqn : eqns)
			res.addAll(eqn.assignedSet());
		return res;
	}
	
	/**
	 * Gives the set of components assigned in this equation.
	 * 
	 * Only works in instance tree.
	 * For if and when equations, only the first branch is considered.
	 */
	syn lazy Set<InstComponentDecl> FAbstractEquation.assignedSet() = Collections.emptySet();
	eq InstForClauseE.assignedSet()      = assignedSetFromEqns(getFAbstractEquations());
	eq FIfWhenElseEquation.assignedSet() = assignedSetFromEqns(getFAbstractEquations());
	eq FEquation.assignedSet()           = getLeft().accessedVarSet();
	eq FFunctionCallEquation.assignedSet() {
		LinkedHashSet<InstComponentDecl> s = new LinkedHashSet<InstComponentDecl>();
		for (FFunctionCallLeft left : getLefts()) {
			if (left.hasFExp()) {
				s.addAll(left.getFExp().accessedVarSet());
			}
		}
		return s;
	}
	
	/**
	 * If this is an instance tree access, return set containing accessed var, otherwise empty set.
	 */
	syn Set<InstComponentDecl> FExp.accessedVarSet() = Collections.emptySet();
	eq FInstAccessExp.accessedVarSet()               = getInstAccess().accessedVarSet();
	eq FIdUseExp.accessedVarSet()                    = getFIdUse().accessedVarSet();
	
	/**
	 * If this is an instance tree access, return set containing accessed var, otherwise empty set.
	 */
	syn Set<InstComponentDecl> FIdUse.accessedVarSet() = Collections.emptySet();
	eq FIdUseInstAccess.accessedVarSet()               = getInstAccess().accessedVarSet();
	
	/**
	 * Get set containing accessed var.
	 */
	syn Set<InstComponentDecl> InstAccess.accessedVarSet() = 
		Collections.singleton(myInstComponentDecl());
	
	/**
	 * If this is an assignment equation, return the variable assigned.
	 * 
	 * Only works in flat tree.
	 */
	syn FAbstractVariable FAbstractEquation.assignedFV() = null;
	eq FEquation.assignedFV()                            = getLeft().assignedFV();
	
	/**
	 * If this is an flat tree access, return set containing accessed var, otherwise empty set.
	 */
	syn FAbstractVariable FExp.assignedFV() = null;
	eq FIdUseExp.assignedFV()               = myFV();
	
	
	syn FTypePrefixVariability FVariable.variability() = getFTypePrefixVariability();
		
		
	syn boolean FExp.isConstantExp()   = variability().constantVariability();
	syn boolean FExp.isParameterExp()  = variability().parameterVariability();
	syn boolean FExp.isDiscreteExp()   = variability().discreteVariability();
	syn boolean FExp.isContinuousExp() = variability().continuousVariability();
	
	syn boolean FSubscript.isConstant() = true;
	eq FExpSubscript.isConstant() = getFExp().isConstantExp();
	
	inh boolean FExp.inDiscreteLocation();
	eq FFunctionDecl.getChild().inDiscreteLocation()             = true;
	eq FWhenEquation.getFAbstractEquation().inDiscreteLocation() = true;
	eq FWhenClause.getFStatement().inDiscreteLocation()          = true;
	eq FClass.getChild().inDiscreteLocation()                    = false;
	eq InstNode.getChild().inDiscreteLocation()                  = false;
	
	syn FTypePrefixVariability FExp.variability() = expVariability();
	syn lazy FTypePrefixVariability FAbstractExp.variability() = 
		inDiscreteLocation() ? expVariability().combineDown(fDiscrete()) : expVariability();
	
	syn FTypePrefixVariability FExp.expVariability() = combineFExpListVariability(childFExps());
	
	syn FTypePrefixVariability FExp.variabilityInNoEventExp() = 
		variabilityInNoEventExp(combineFExpListVariability(childFExps()));
	syn FTypePrefixVariability FExp.variabilityInNoEventExp(FTypePrefixVariability var) =
		inNoEventExp() ? var : var.combineDown(fDiscrete());
	
	eq FInstAccessExp.variability() = getInstAccess().myInstComponentDecl().variability();
	
	eq FUnsupportedExp.expVariability() = fContinuous(); // Only here to avoid null pointer 
	
	public static FTypePrefixVariability FExp.combineFExpListVariability(Iterable<? extends FExp> exps) {
		Iterator<? extends FExp> it = exps.iterator();
		FTypePrefixVariability total = it.hasNext() ? it.next().variability() : fConstant();
		while (it.hasNext()) 
			total = total.combine(it.next().variability());
		return total;
	}
	
	eq FLitExp.expVariability()   = fConstant();
	eq FNoExp.expVariability()    = fConstant();
	eq FNdimsExp.expVariability() = fConstant();
	eq FEndExp.expVariability()   = fParameter();
	eq FTimeExp.expVariability()  = fContinuous();
	
	eq FAbsExp.expVariability() = variabilityInNoEventExp();
	eq FRelExp.expVariability() = variabilityInNoEventExp();

	eq FSizeExp.expVariability() = hasDim() ? getDim().variability() : fParameter();
	
	eq FIntegerExp.expVariability() = getFExp().variability().combineDown(fDiscrete());
	eq FFloorExp.expVariability()   = getFExp().variability().combineDown(fDiscrete());
	
	eq FIterExp.expVariability() {
		FTypePrefixVariability total = getArray().iteratorFExp().next().variability();
		for (CommonForIndex ind : getForIndexList())
			if (ind.hasFExp())
				total = total.combine(ind.getFExp().variability());
		return total;
	}

	eq InstFunctionCall.expVariability() {
		FTypePrefixVariability total = fConstant();
		for (InstFunctionArgument arg : getArgs()) 
			total = total.combine(arg.getFExp().variability());
		return total;
	}
	
	eq FIdUseExp.expVariability() = getFIdUse().variability();
	
	syn FTypePrefixVariability FIdUse.variability() {
		FAbstractVariable variable = myFV();
		if (variable instanceof FVariable) {
			FVariable fVariable = (FVariable) variable;
			return(fVariable.variability());
		} else {
			return (fContinuous());
		}
	}
	eq FIdUseInstAccess.variability() = getInstAccess().myInstComponentDecl().variability();

	syn FTypePrefixVariability FForIndex.variability() = hasFExp() ? getFExp().variability() : fParameter();

	syn FTypePrefixVariability FSubscript.variability() = fParameter();
	eq FExpSubscript.variability() = getFExp().variability();
	
	syn FTypePrefixVariability FArraySubscripts.variability() {
		FTypePrefixVariability total = fConstant();
		for (FSubscript arg : getFSubscripts()) 
			total = total.combine(arg.variability());
		return total;
	}
	
	eq FLinspace.expVariability() = getStartExp().variability().combine(getStopExp().variability());
	
	eq FIdentity.expVariability()  = fConstant();
	eq FOnes.expVariability()      = fConstant();
	eq FZeros.expVariability()     = fConstant();
	eq FFillExp.expVariability()   = getFillExp().variability();
	
	eq FPreExp.expVariability()    = fDiscrete();
	eq FSampleExp.expVariability() = fDiscrete();
	
	eq FSimulationStateBuiltIn.expVariability() = fParameter();
	
	syn int FTypePrefixVariability.combineLevel() = variabilityLevel() * 10;
	
   	public FTypePrefixVariability FTypePrefixVariability.combine(FTypePrefixVariability other) {
   		return (other.combineLevel() > combineLevel()) ? other : this;
	}	
	
   	public FTypePrefixVariability FTypePrefixVariability.combineDown(FTypePrefixVariability other) {
   		return (other.combineLevel() < combineLevel()) ? other : this;
	}	
	
	syn int TypePrefixVariability.combineLevel() = variabilityLevel() * 10;
	
   	public TypePrefixVariability TypePrefixVariability.combine(TypePrefixVariability other) {
   		return (other.combineLevel() > combineLevel()) ? other : this;
	}	
	
   	public TypePrefixVariability TypePrefixVariability.combineDown(TypePrefixVariability other) {
   		return (other.combineLevel() < combineLevel()) ? other : this;
	}	
	
}

aspect VariabilitySingletons {
	
	public static final Continuous Continuous.instance = new Continuous();	
	public static final Discrete   Discrete.instance   = new Discrete();	
	public static final Parameter  Parameter.instance  = new Parameter();
	public static final Constant   Constant.instance   = new Constant();	
	
	public static final FContinuous      FContinuous.instance      = new FContinuous();	
	public static final FDiscrete        FDiscrete.instance        = new FDiscrete();	
	public static final FParameter       FParameter.instance       = new FParameter();
	public static final FStructParameter FStructParameter.instance = new FStructParameter();
	public static final FConstant        FConstant.instance        = new FConstant();	
	
	public static Continuous ASTNode.continuous() {
		return Continuous.instance;
	}

	public static Discrete ASTNode.discrete() {
		return Discrete.instance;
	}	

	public static Parameter ASTNode.parameter() {
		return Parameter.instance;
	}
	
	public static Constant ASTNode.constant() {
		return Constant.instance;
	}
	
	public static FContinuous ASTNode.fContinuous() {
		return FContinuous.instance;
	}

	public static FDiscrete ASTNode.fDiscrete() {
		return FDiscrete.instance;
	}	

	public static FParameter ASTNode.fParameter() {
		return FParameter.instance;
	}

	public static FStructParameter ASTNode.fStructParameter() {
		return FStructParameter.instance;
	}
	
	public static FConstant ASTNode.fConstant() {
		return FConstant.instance;
	}
}

