<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Choosing The Vector Testing Template Class</title>
<meta name="description" id="description" content="Choosing The Vector Testing Template Class"/>
<meta name="keywords" id="keywords" content=" Cppad_test_vector vector test Cppadvector deprecated "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_test_vector_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="listallexamples.xml" target="_top">Prev</a>
</td><td><a href="configure.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Example</option>
<option>test_vector</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Example-&gt;</option>
<option>General</option>
<option>ExampleUtility</option>
<option>ListAllExamples</option>
<option>test_vector</option>
</select>
</td>
<td>test_vector</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Introduction</option>
<option>MS Windows</option>
<option>CppAD::vector</option>
<option>std::vector</option>
<option>boost::numeric::ublas::vector</option>
<option>Deprecated</option>
</select>
</td>
</tr></table><br/>







<center><b><big><big>Choosing The Vector Testing Template Class</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>CPPAD_TEST_VECTOR&lt;</span></font></code><i><span style='white-space: nowrap'>Scalar</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;<br/>
</span></font></code><br/>
<b><big><a name="Introduction" id="Introduction">Introduction</a></big></b>
<br/>
Many of the CppAD <a href="example.xml" target="_top"><span style='white-space: nowrap'>examples</span></a>
 and tests use 
the <code><font color="blue">CPPAD_TEST_VECTOR</font></code> template class to pass information.
The default definition for this template class is
<a href="cppad_vector.xml" target="_top"><span style='white-space: nowrap'>CppAD::vector</span></a>
.

<br/>
<br/>
<b><big><a name="MS Windows" id="MS Windows">MS Windows</a></big></b>
<br/>
The include path for boost is not defined in the Windows project files.
If we are using Microsofts compiler, the following code overrides the setting
of <code><font color="blue">CPPAD_BOOSTVECTOR</font></code>:
<code><font color='blue'><pre style='display:inline'> 
// The next 7 lines are C++ source code.
# ifdef _MSC_VER
# if CPPAD_BOOSTVECTOR
# undef  CPPAD_BOOSTVECTOR
# define CPPAD_BOOSTVECTOR 0
# undef  CPPAD_CPPADVECTOR
# define CPPAD_CPPADVECTOR 1
# endif
# endif
</pre></font></code>


<br/>
<br/>
<b><big><a name="CppAD::vector" id="CppAD::vector">CppAD::vector</a></big></b>
<br/>
By default <code><font color="blue">CPPAD_CPPADVECTOR</font></code> is true
and <code><font color="blue">CPPAD_TEST_VECTOR</font></code> is defined by the following source code
<code><font color='blue'><pre style='display:inline'> 
// The next 3 line are C++ source code.
# if CPPAD_CPPADVECTOR
# define CPPAD_TEST_VECTOR CppAD::vector
# endif
</pre></font></code>

You can replace this definition of the preprocessor symbol
<code><font color="blue">CPPAD_TEST_VECTOR</font></code> by any other <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 template class.
This will test using your replacement template vector class with CppAD.

<br/>
<br/>
<b><big><a name="std::vector" id="std::vector">std::vector</a></big></b>
<br/>
If you specify <code><font color="blue">--with-stdvector</font></code> on the 
<a href="installunix.xml#Configure" target="_top"><span style='white-space: nowrap'>configure</span></a>

command line during CppAD installation,
<code><font color="blue">CPPAD_STDVECTOR</font></code> is true
and <code><font color="blue">CPPAD_TEST_VECTOR</font></code> is defined by the following source code
<code><font color='blue'><pre style='display:inline'> 
// The next 4 lines are C++ source code.
# if CPPAD_STDVECTOR
# include &lt;vector&gt;
# define CPPAD_TEST_VECTOR std::vector
# endif
</pre></font></code>

In this case CppAD will use <code><font color="blue">std::vector</font></code> for its examples and tests.
Use of <code><font color="blue">CppAD::vector</font></code>, <code><font color="blue">std::vector</font></code>,
and <code><font color="blue">std::valarray</font></code> with CppAD is always tested to some degree.
Specifying <code><font color="blue">--with-stdvector</font></code> will increase the amount of
<code><font color="blue">std::vector</font></code> testing. 

<br/>
<br/>
<b><big><a name="boost::numeric::ublas::vector" id="boost::numeric::ublas::vector">boost::numeric::ublas::vector</a></big></b>
<br/>
If you specify a value for <i>BoostDir</i> on the configure
command line during CppAD installation,
<code><font color="blue">CPPAD_BOOSTVECTOR</font></code> is true 
and <code><font color="blue">CPPAD_TEST_VECTOR</font></code> is defined by the following source code
<code><font color='blue'><pre style='display:inline'> 
// The next 4 lines are C++ source code.
# if CPPAD_BOOSTVECTOR
# include &lt;boost/numeric/ublas/vector.hpp&gt;
# define CPPAD_TEST_VECTOR boost::numeric::ublas::vector
# endif
</pre></font></code>

In this case CppAD will use Ublas vectors for its examples and tests.
Use of <code><font color="blue">CppAD::vector</font></code>, <code><font color="blue">std::vector</font></code>,
and <code><font color="blue">std::valarray</font></code> with CppAD is always tested to some degree.
Specifying <i>BoostDir</i> will increase the amount of
Ublas vector testing. 

<br/>
<br/>
<b><big><a name="Deprecated" id="Deprecated">Deprecated</a></big></b>


<br/>
The preprocessor symbol <code><font color="blue">CppADvector</font></code> is defined to
have the same value as <code><font color="blue">CPPAD_TEST_VECTOR</font></code> but its use is deprecated
<code><font color='blue'><pre style='display:inline'> 
# define CppADvector CPPAD_TEST_VECTOR
</pre></font></code>


<hr/>Input File: cppad/local/test_vector.hpp

</body>
</html>
