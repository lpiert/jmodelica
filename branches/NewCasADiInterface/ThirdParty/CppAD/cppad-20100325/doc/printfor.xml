<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Printing AD Values During Forward Mode</title>
<meta name="description" id="description" content="Printing AD Values During Forward Mode"/>
<meta name="keywords" id="keywords" content=" print forward mode text output debug "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_printfor_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="output.cpp.xml" target="_top">Prev</a>
</td><td><a href="printfor.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>AD</option>
<option>Convert</option>
<option>PrintFor</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>Example</option>
<option>configure</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>AD-&gt;</option>
<option>Default</option>
<option>ad_copy</option>
<option>Convert</option>
<option>ADValued</option>
<option>BoolValued</option>
<option>VecAD</option>
<option>base_require</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Convert-&gt;</option>
<option>Value</option>
<option>Integer</option>
<option>Output</option>
<option>PrintFor</option>
<option>Var2Par</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>PrintFor-&gt;</option>
<option>PrintFor.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>text</option>
<option>y</option>
<option>f.Forward(0, x)</option>
<option>Discussion</option>
<option>Alternative</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>










<center><b><big><big>Printing AD Values During Forward Mode</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"><span style='white-space: nowrap'>PrintFor(</span></font></code><i><span style='white-space: nowrap'>text</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>
<code><span style='white-space: nowrap'><br/>
</span></code><code><font color="blue"></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Forward(0,&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
The current value of an <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> 
object <i>y</i> is the result of an AD of <i>Base</i> operation.
This operation may be part of the 
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>

that is transferred to an <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i>.
The <code><font color="blue">ADFun</font></code> object can be evaluated at different values for the
<a href="glossary.xml#Tape.Independent Variable" target="_top"><span style='white-space: nowrap'>independent&#xA0;variables</span></a>
.
This may result in a corresponding value for <i>y</i> 
that is different from when the operation sequence was recorded.
The routine <code><font color="blue">PrintFor</font></code> requests a printing,
when <code><font color="blue"></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Forward(0,&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> is executed,
of the value for <i>y</i> that corresponds to the 
independent variable values specified by <i>x</i>.

<br/>
<br/>
<b><big><a name="text" id="text">text</a></big></b>
<br/>
The argument <i>text</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;char&#xA0;*</span></font></code><i><span style='white-space: nowrap'>text</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The corresponding text is written to <code><font color="blue">std::cout</font></code> before the 
value of <i>y</i>. 

<br/>
<br/>
<b><big><a name="y" id="y">y</a></big></b>
<br/>
The argument <i>y</i> has one of the following prototypes
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>The value of <i>y</i> that corresponds to <i>x</i>
is written to <code><font color="blue">std::cout</font></code> during the execution of 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Forward(0,&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code><br/>
<b><big><a name="f.Forward(0, x)" id="f.Forward(0, x)">f.Forward(0, x)</a></big></b>
<br/>
The objects <i>f</i>, <i>x</i>, and the purpose
for this operation, are documented in <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
.


<br/>
<br/>
<b><big><a name="Discussion" id="Discussion">Discussion</a></big></b>
<br/>
This is can be helpful for understanding why tape evaluations
have trouble, for example, if the result of a tape calculation
is the IEEE code for not a number <code><font color="blue">Nan</font></code>.

<br/>
<br/>
<b><big><a name="Alternative" id="Alternative">Alternative</a></big></b>
<br/>
The <a href="output.xml" target="_top"><span style='white-space: nowrap'>Output</span></a>
 section describes the normal 
printing of values; i.e., printing when the corresponding
code is executed.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The program
<a href="printfor.cpp.xml" target="_top"><span style='white-space: nowrap'>PrintFor.cpp</span></a>

is an example and test of this operation.
The output of this program
states the conditions for passing and failing the test.


<hr/>Input File: cppad/local/print_for.hpp

</body>
</html>
