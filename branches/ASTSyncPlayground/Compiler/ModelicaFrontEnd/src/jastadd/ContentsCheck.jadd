/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect ContentCheck {

	/**
	 * \brief Check for code that is not allowed in its current context.
	 * 
	 * Examples would be checking that classes follow the requirements of 
	 * their restriction. 
	 */
	public void ASTNode.contentCheck() {}
	
	public void FAbstractEquation.contentCheck() {
		if (inFunction()) 
			error("Equations are not allowed in functions");
	}
	
	public void FAlgorithm.contentCheck() {}
	
	public void InstDerExp.contentCheck() {
		if (inFunction()) {
			error("The der() operator may not be used inside functions");
		} else {
			try {
				getFExp().diff(FExp.TIME);
			} catch (ExpressionDifferentiationException e) {
				e.generateError();
			}
		}
	}
	
	public void InstExternal.contentCheck() {
		if (!inFunction())
			error("External function declarations are only allowed in functions");
	}
	
	public void FReturnStmt.contentCheck() {
		if (!inFunction())
			error("Return statements are only allowed in functions");
	}
	
	public void FWhenStmt.contentCheck() {
		if (inFunction())
			error("When statements are not allowed in functions");
		else if (insideBlockStmt())
			error("When statements are not allowed inside if, for, while and when clauses");
	}
	
	public void InstForIndex.contentCheck() {
		if (!hasFExp())
			error("For index without in expression isn't supported");
	}
	
	public void FParseArray.contentCheck() {
		// If not already rewritten to FLongArray, this is an error.
		error("The array() operator may not be used in function call equations or function call statements");
	}
	
	public void FEndExp.contentCheck() {
		if (!inArraySubscripts())
			error("The end operator may only be used in array subscripts");
	}
	
	public void FPreExp.contentCheck() {
		if (inFunction()) { 
			error("The pre() operator may not be used inside functions");
		}
	}
	
	public void InstExtends.contentCheck() {
		if (myInstClass().isExternalObject())
			error("Classed derived from ExternalObject can neither be used in an extends-clause nor in a short class defenition");
	}
	
	public void InstFunctionCall.contentCheck() {
		if (!generated) {
			InstClassDecl target = getName().myInstClassDecl();
			if (target.isConstructor() || target.isDestructor())
				error("Constructors and destructors for ExternalObjects can not be used directly");
		}
	}
	
	public void FConnRootedDep.contentCheck() {
		warning("The rooted() operator has been deprecated in favor of Connections.rooted()");
	}
	
	public void InstClassDecl.contentCheck() {
		if (isExternalObject()) {
			checkContentsOKInExternalObject();
			myConstructor().checkConstructor(primitiveScalarType());
			myDestructor().checkDestructor(primitiveScalarType());
		}
	}
	
	public void InstNode.checkContentsOKInExternalObject() {
		for (InstExtends ie : getInstExtendss())
			ie.checkContentsOKInExternalObject();
		boolean ok = true;
		for (InstClassDecl icd : getInstClassDecls())
			if (!icd.name().equals("constructor") && !icd.name().equals("destructor"))
				ok = false;
		if (getNumInstComponentDecl() > 0)
			ok = false;
		if (getNumRedeclaredInstClassDecl() > 0)
			ok = false;
		if (getNumFAbstractEquation() > 0)
			ok = false;
		if (!ok)
			error("External object classes may not contain any elements except the constructor and destructor");
	}
	
	public void InstClassDecl.checkDestructor(FType eoType) {
		if (isUnknown())
			return;
		if (myOutputs().size() != 0 || myInputs().size() != 1 || !myInputs().get(0).type().typeCompatible(eoType))
			error("An external object destructor must have exactly one input of the same type as the constructor, and no outputs");
	}
	
	public void InstClassDecl.checkConstructor(FType eoType) {
		if (isUnknown())
			return;
		if (myOutputs().size() != 1 || !myOutputs().get(0).type().typeCompatible(eoType))
			error("An external object constructor must have exactly one output of the same type as the constructor");
	}
	
	// TODO: check if this is a builtin function (with bad arguments), error otherwise
//	public void FBuiltinExternalLanguage.contentCheck() {
//		error("The \"builtin\" external language specitication may only be used for functions that are a part of the Modelica specification");
//	}
	
	public void FUnknownExternalLanguage.contentCheck() {
		error("The external language specification \"" + getLanguage() + "\" is not supported");
	}
	
	inh boolean FIterExp.iterExpUseOK();
	eq FExp.getChild().iterExpUseOK()       = false;
	eq Root.getChild().iterExpUseOK()       = false;
	eq InstNode.getChild().iterExpUseOK()   = false;
	eq FArray.getChild().iterExpUseOK()     = true;
	eq FMinMaxExp.getChild().iterExpUseOK() = true;
	eq FSumExp.getChild().iterExpUseOK()    = true;
	// TODO: Add product() when it is implemented
	
	public void FIterExp.contentCheck() {
		if (!iterExpUseOK())
			error("Reduction-expressions are only allowed with sum(), min(), max() and product()");
	}
	
	public void FOverflowIntLitExp.contentCheck() {
		warning("Integer literal \"%s\" is too large to represent as 32-bit Integer, using Real instead.", 
				getString(), getString(), getValue());
	}
	
	/**
     * Check if this equation is in a place that allows connect clauses.
	 */
	inh boolean FAbstractEquation.connectAllowed();
	eq FAbstractEquation.getChild().connectAllowed() = mayContainConnect() && connectAllowed();
	eq FClass.getChild().connectAllowed()            = true;
	eq InstNode.getChild().connectAllowed()          = true;
	
	/**
	 * Check if this equation may legally contain a conncect clause.
	 */
	syn boolean FAbstractEquation.mayContainConnect() = false;
	eq InstForClauseE.mayContainConnect()             = true;
	eq FIfEquation.mayContainConnect()                = 
		getTest().variability().parameterOrLess() && getTest().canCeval();
	
	public void FConnectClause.contentCheck() {
		super.contentCheck();
		if (!connectAllowed())
			error("Connect clauses are not allowed in if equations with non-parameter conditions, or in when equations"); 
	}
	
	public void FWhenEquation.contentCheck() {
		// TODO: check that two when clauses do not assign the same variable
		super.contentCheck();
		if (inWhen()) {
			error("Nestled when clauses are not allowed");
		} else {
			if (!isBalancedAssignments())
				error("All branches in when equation must assign the same variables");
		}
	}
	
	public static final String FIfEquation.UNBALANCED_ERROR = 
		"All branches in if equation with non-parameter tests must have the same number of equations";
	public static final String FIfEquation.UNBALANCED_ERROR_WHEN = 
		"All branches in if equation with non-parameter tests within when equation must assign the same variables";
	
	public void FIfEquation.contentCheck() {
		super.contentCheck();
		if (isTopWhenIfEquation()) {
			boolean inWhen = inWhen();
			boolean balanced = inWhen ? isBalancedAssignments() : isBalancedEquations();
			boolean functionCallEqu = hasFunctionCallEquations(); 
			if (hasOnlyParamTests()) {
				setEliminateOnParamTest();
			} else {
				if (!balanced) {
					error(inWhen ? UNBALANCED_ERROR_WHEN : UNBALANCED_ERROR);
				} else if (functionCallEqu) {
					if (hasFunctionCallEquationsWithLefts())
						compliance("If equations that has non-parameter tests and contains " + 
								"function calls using multiple outputs are not supported");
					else
						warning("Function calls without outputs in if equations that has " + 
								"non-parameter tests are not supported, and are currently ignored");
				}
			}
		}
	}
	
	public void FEquation.contentCheck() {
		super.contentCheck();
		if (inWhen() && !isAssignmentEqn())
			error("Only assignment equations are allowed in when clauses");
	}
	
	syn boolean FIfWhenElseEquation.isBalancedEquations() = true; 
	eq FIfEquation.isBalancedEquations() = hasElse() && elseIsBalancedEquations();
	
	syn boolean FIfWhenEquation.elseIsBalancedEquations() = 
		numScalarEquations() == getElse().numScalarEquations() && 
		getElse().isBalancedEquations();
	
	syn boolean FIfWhenElseEquation.isBalancedAssignments() = true; 
	eq FIfEquation.isBalancedAssignments()   = hasElse() && elseIsBalancedAssignments();
	eq FWhenEquation.isBalancedAssignments() = !hasElse() || elseIsBalancedAssignments();
	
	syn boolean FIfWhenEquation.elseIsBalancedAssignments() = 
		assignedSet().equals(getElse().assignedSet()) && getElse().isBalancedAssignments();
	
	inh boolean FIfEquation.isTopWhenIfEquation();
	eq ASTNode.getChild().isTopWhenIfEquation()        = true;
	eq FIfWhenEquation.getElse().isTopWhenIfEquation() = false;
	
	syn boolean FIfWhenElseEquation.hasOnlyParamTests() = true;
	eq FIfWhenEquation.hasOnlyParamTests()              = 
		getTest().variability().parameterOrLess() && (!hasElse() || getElse().hasOnlyParamTests());
	
	syn boolean FAbstractEquation.isAssignmentEqn() = false;
	eq FEquation.isAssignmentEqn()                  = getLeft().isAccess();
	
	syn boolean FAbstractEquation.hasFunctionCallEquations() = false;
	eq FFunctionCallEquation.hasFunctionCallEquations()      = true;
	eq FIfWhenEquation.hasFunctionCallEquations()            = 
		super.hasFunctionCallEquations() || (hasElse() && getElse().hasFunctionCallEquations());
	eq FIfWhenElseEquation.hasFunctionCallEquations() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquations())
				return true;
		return false;
	}
	eq FForClauseE.hasFunctionCallEquations() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquations())
				return true;
		return false;
	}
	
	syn boolean FAbstractEquation.hasFunctionCallEquationsWithLefts() = false;
	eq FFunctionCallEquation.hasFunctionCallEquationsWithLefts()      = getNumLeft() > 0;
	eq FIfWhenEquation.hasFunctionCallEquationsWithLefts()            = 
		super.hasFunctionCallEquationsWithLefts() || (hasElse() && getElse().hasFunctionCallEquationsWithLefts());
	eq FIfWhenElseEquation.hasFunctionCallEquationsWithLefts() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquationsWithLefts())
				return true;
		return false;
	}
	eq FForClauseE.hasFunctionCallEquationsWithLefts() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquationsWithLefts())
				return true;
		return false;
	}
	
	
	public void InstMPackage.contentCheck() {
		if (!myInstClassDecl().isOkPackage())
			error("Packages may only contain classes and constants");
	}
	
	/**
	 * Check that the restriction of this class is fulfilled.
	 */
	public void InstClassDecl.checkRestriction() {}
	
	public void InstBaseClassDecl.checkRestriction() {
		getInstRestriction().contentCheck();
	}
	
	/**
	 * Get the class this restriction is attached to.
	 */
	inh InstClassDecl InstRestriction.myInstClassDecl();
	eq InstBaseClassDecl.getInstRestriction().myInstClassDecl() = this;
	
	/**
	 * Check if this class fulfills the requirements of a package.
	 */
	syn boolean InstClassDecl.isOkPackage() {
		for (InstComponentDecl icd : getInstComponentDecls())
			if (!icd.isConstant())
				return false;
		return getNumFAbstractEquation() == 0;
	}
	eq InstSimpleShortClassDecl.isOkPackage() = actualInstClass().isOkPackage();
	
}

aspect StructuralParams {
	
	private boolean InstAssignable.isStructuralParam = false;
	
	public void FExp.markAsStructuralParameter() {
		if (inFunction())
			return;
		if (!canCeval())
			compliance("Could not evaluate expression used as structural parameter: " + this);
		markUsesAsStructuralParameter();
	}
	
	
	public void ASTNode.markUsesAsStructuralParameter() {
		for (ASTNode n : this)
			n.markUsesAsStructuralParameter();
	}
	
	public void InstAccess.markUsesAsStructuralParameter() {
		myInstComponentDecl().markAsStructuralParameter();
		markUsesInSubscriptsAsStructuralParameter();
	}
	
	public void InstAccess.markUsesInSubscriptsAsStructuralParameter() {}
	
	public void InstDot.markUsesInSubscriptsAsStructuralParameter() {
		for (InstAccess ia : getInstAccesss())
			ia.markUsesInSubscriptsAsStructuralParameter();
	}
	
	public void InstArrayAccess.markUsesInSubscriptsAsStructuralParameter() {
		getFArraySubscripts().markUsesAsStructuralParameter();
	}
	
	
	public void InstComponentDecl.markAsStructuralParameter() {
		// TODO: should probably do something constructive here?
	}
	
	public void InstAssignable.markAsStructuralParameter() {
		if (isStructuralParam || !isParameter() || inFunction())
			return;
		isStructuralParam = true;
		// Flush cache for variability()
		variability_computed = false;
		if (hasBindingFExp()) {
			FExp e = getBindingFExp();
			if (!e.canCeval())
				compliance("Could not evaluate binding expression of structural parameter " + 
						qualifiedName());
			e.markUsesAsStructuralParameter();
		}
	}

	// TODO: Propagate structural parameter information during flattening
	
}
