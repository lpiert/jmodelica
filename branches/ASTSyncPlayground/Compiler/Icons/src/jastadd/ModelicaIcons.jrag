import org.jmodelica.icons.Icon;
import org.jmodelica.icons.Diagram;
import org.jmodelica.icons.Connection;
import org.jmodelica.icons.Component;
import org.jmodelica.icons.DiagramFactory;
import org.jmodelica.icons.exceptions.FailedConstructionException;
import org.jmodelica.icons.exceptions.ASTEditException;
import org.jmodelica.icons.drawing.IconConstants.Context;
import org.jmodelica.icons.drawing.AWTIconDrawer;
import java.awt.image.BufferedImage;
import org.jmodelica.icons.Observer;
import org.jmodelica.icons.Observable;

aspect ModelicaIcons
{
	syn lazy Icon BaseNode.icon() 		= Icon.NULL_ICON;
	syn lazy Diagram BaseNode.diagram() = Diagram.NULL_DIAGRAM;
	
	eq UnknownClassDecl.icon()	 		= Icon.NULL_ICON;
	eq UnknownClassDecl.diagram() 		= Diagram.NULL_DIAGRAM;
	eq UnknownInstClassDecl.icon() 		= Icon.NULL_ICON;
	eq UnknownInstClassDecl.diagram() 	= Diagram.NULL_DIAGRAM;

	eq ComponentDecl.icon()               = findClassDecl().icon();
	eq ComponentDecl.diagram()            = findClassDecl().diagram();
	eq InstShortClassDecl.icon()          = getInstExtends(0).myInstClass().icon();
	eq InstShortClassDecl.diagram()       = getInstExtends(0).myInstClass().diagram();
	eq InstSimpleShortClassDecl.icon()    = actualInstClass().icon();
	eq InstSimpleShortClassDecl.diagram() = actualInstClass().diagram();

	protected boolean ClassDecl.visitingDuringIconRendering;
	
	syn AnnotationNode BaseNode.iconAnnotation() = annotation();
	eq InstComponentDecl.iconAnnotation() = myInstClass().annotation();
	eq InstExtends.iconAnnotation() = myInstClass().annotation();
	
	
	eq ClassDecl.icon() {
		visitingDuringIconRendering = true;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = iconAnnotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		Icon icon = new Icon(qualifiedName(), layer, Context.ICON); 
		icon = addSuperClasses(icon);
		icon = addSubComponents(icon);
		visitingDuringIconRendering = false;
		return icon;
	}
	
	eq ShortClassDecl.icon() {
		visitingDuringIconRendering = true;
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = iconAnnotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		Icon icon = new Icon(qualifiedName(), layer, Context.ICON);
		Icon superIcon = getExtendsClauseShortClass().findClassDecl().icon();
		if (superIcon != Icon.NULL_ICON) {
			icon.addSuperclass(superIcon);
		}
		visitingDuringIconRendering = false;
		return icon;
	}

	eq InstClassDecl.icon() {
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = iconAnnotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		Icon icon = new Icon(qualifiedName(), layer, Context.ICON);
		addSuperClasses(icon);
		addSubComponents(icon);
		return icon;
	}
	
	eq InstComponentDecl.icon() {
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = iconAnnotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		Icon icon = new Icon(qualifiedName(), layer, Context.ICON);
		addSuperClasses(icon);
		addSubComponents(icon);
		return icon;
	}
	
	eq InstExtends.icon() {
		Layer layer = Layer.NO_LAYER;
		AnnotationNode annotation = iconAnnotation();
		if (annotation.exists()) {
			layer = annotation.createIconLayer();
		}
		Icon icon = new Icon(myInstClass().qualifiedName(), layer, Context.ICON);
		addSuperClasses(icon);
		addSubComponents(icon);
		return icon;
	}
	
//	eq InstClassDecl.diagram() {
//		Diagram diagram;
//		Layer layer = iconAnnotation().createDiagramLayer();
//		diagram = new Diagram(qualifiedName(), layer, Context.DIAGRAM, new DiagramFactory() {
//			public Component createComponent(String className, String componentName) {
//				ComponentDecl componentDecl = new ComponentDecl();
//				componentDecl.setClassName(Access.fromClassName(className));
//				componentDecl.setVisibilityType(new PublicVisibilityType());
//				componentDecl.setName(new IdDecl(componentName));
//				componentDecl.setComment(new Comment());
//				componentDecl.setCCComment(new Comment());
//				((FullClassDecl)getClassDecl()).addNewComponentDecl(componentDecl);
//				InstComponentDecl icd = (InstComponentDecl)createInstComponentDecl(componentDecl);
//				addInstComponentDecl(icd);
//				
//				icd.is$Final(true);
//				
//				Component c = icd.component(Context.DIAGRAM);
//				
//				removeInstComponentDecl(icd);
//				((FullClassDecl)getClassDecl()).removeComponentDecl(icd.getComponentDecl());
//				
//				if (c == null)
//					return null;
//				icd.setComponent(c);
//				childComponentMap.put(c, icd);
//				return c;
//			}
//		});
//		addSuperClasses(diagram);
//		addSubComponents(diagram);
//		for (FAbstractEquation fae : getFAbstractEquations()) {
//			if (fae instanceof FConnectClause) {
//				FConnectClause fcc = (FConnectClause) fae;
//				Connection con;
//				try {
//					con = fcc.getConnectClause().annotation().createConnection();
//				} catch (FailedConstructionException e) {
//					con = new Connection();
//				}
//				con.setSourceConnector((org.jmodelica.icons.Connector)fcc.getConnector1().getInstAccess().myInstComponentDecl().getComponent());
//				con.setTargetConnector((org.jmodelica.icons.Connector)fcc.getConnector2().getInstAccess().myInstComponentDecl().getComponent());
//				fcc.setConnection(con);
//				con.connect();
//				con.addObserver(fcc);
//			}
//		}
//		diagram.addObserver(this);
//		return diagram;
//	}
//	
//	eq InstNode.diagram() {
//		Layer layer = iconAnnotation().createDiagramLayer();
//		Diagram diagram = new Diagram(myInstClass().qualifiedName(), layer, Context.DIAGRAM, null);
//		addSuperClasses(diagram);
//		addSubComponents(diagram);
//		for (FAbstractEquation fae : getFAbstractEquations()) {
//			if (fae instanceof FConnectClause) {
//				FConnectClause fcc = (FConnectClause) fae;
//				Connection con;
//				try {
//					con = fcc.getConnectClause().annotation().createConnection();
//				} catch (FailedConstructionException e) {
//					con = new Connection();
//				}
//				con.setSourceConnector((org.jmodelica.icons.Connector)fcc.getConnector1().getInstAccess().myInstComponentDecl().getComponent());
//				con.setTargetConnector((org.jmodelica.icons.Connector)fcc.getConnector2().getInstAccess().myInstComponentDecl().getComponent());
//				fcc.setConnection(con);
//				con.connect();
//			}
//		}
//		return diagram;
//	}
	
	public BufferedImage BaseNode.render(Icon icon, int w, int h) {
		return new AWTIconDrawer(icon, w, h).getImage();
	}
	
	public static final int BaseNode.iconImageSize = 400;
	
	public BufferedImage BaseNode.createIconImage() {
		return render(icon(), iconImageSize, iconImageSize);
	}
	
	syn boolean BaseNode.hasIcon() = !icon().isEmpty();
	
	private Icon ClassDecl.addSuperClasses(Icon icon) {	
		for(ExtendsClause ext: superClasses()) {
			ClassDecl superClass = ext.findClassDecl();
			if(superClass.visitingDuringIconRendering) {		
				continue;
			}
			Icon superIcon;
			if (icon.getContext().equals(Context.ICON)) {
				superIcon = superClass.icon();
			} else {
				superIcon = superClass.diagram();
			}
			if(superIcon != Icon.NULL_ICON) {
				icon.addSuperclass(superIcon);
			}
		}
		
		return icon;
	}
	
	private Icon ClassDecl.addSubComponents(Icon icon) {
		for(ComponentDecl componentDecl : components()) {
			ClassDecl subDecl = componentDecl.findClassDecl();
			if(subDecl.isUnknown() || subDecl.visitingDuringIconRendering) {
				continue;
			}
			AnnotationNode compAnnotation = componentDecl.annotation();
			if(compAnnotation.hasPlacement()) {
				try {
					if (subDecl instanceof BaseClassDecl) {
						Context componentContext = Context.ICON;
						boolean isProtected = componentDecl.isProtected();
						boolean isConnector = ((BaseClassDecl)subDecl).getRestriction() instanceof Connector;
						Icon subIcon = Icon.NULL_ICON;
						if (icon.getContext().equals(Context.ICON)) {
							if (isConnector && !isProtected) {
								subIcon = subDecl.icon();
							}
						} else {
							if (isConnector) {
								subIcon = subDecl.diagram();
								componentContext = Context.DIAGRAM;
								
							} else {
								subIcon = subDecl.icon();
							}
						}
						if(subIcon != Icon.NULL_ICON) {
							Placement placement = compAnnotation.createPlacement(componentContext);
							icon.addSubcomponent(
									new Component(subIcon, placement)
							);
						}
					}
				}
				catch(FailedConstructionException fe) {
//					System.out.println(fe.getMessage());
				}
			}
		}
		return icon;
	}
	
	protected void InstNode.addSuperClasses(Icon icon) {	
		for (InstExtends ie : instExtends()) {
			Icon superIcon;
			if (icon.getContext().equals(Context.ICON)) {
				superIcon = ie.icon();
			} else {
				superIcon = ie.diagram();
			}
			if (superIcon != Icon.NULL_ICON) {
				icon.addSuperclass(superIcon);
			}
	 	}
	}
	
	protected void InstNode.addSubComponents(Icon icon) {
		for(InstComponentDecl componentDecl : getInstComponentDecls()) {
			Component c = componentDecl.component(icon.getContext());
			if (c != null) {
				icon.addSubcomponent(c);
//				componentDecl.setComponent(c);
			}
		}
	}
	
	public Component InstComponentDecl.component(Context context) {
//		if (getComponent() != null)
//			return getComponent();
		InstClassDecl subDecl = myInstClass();
		if(subDecl.isUnknown()) {
			return null;
		}
		Placement placement;
		try {
			placement = annotation().createPlacement(context);
		}
		catch(FailedConstructionException fe) {
			placement = new Placement(new Transformation(new Extent(new Point(-10, -10), new Point(10, 10))));
		}
		if (subDecl instanceof InstBaseClassDecl) {
			boolean isProtected = getComponentDecl().isProtected();
			boolean isConnector = ((InstBaseClassDecl)subDecl).isConnector();
			if (context.equals(Context.ICON)) {
				if (isConnector && !isProtected) {
					if (hasConditionalAttribute()) {
						CValue val = getConditionalAttribute().ceval();
						if (val instanceof CValueBoolean && !val.booleanValue()) {
							return null;
						}
					}
					return new org.jmodelica.icons.Connector(icon(), placement, qualifiedName());
				}
			} else {
				Component c;
				if (isConnector) {
					c = new org.jmodelica.icons.Connector(diagram(), placement, qualifiedName());
				} else {
					c = new Component(icon(), placement, qualifiedName());
				}
//				c.addObserver(this);
				return c;
			}
		}
		return null;
	}
	
	
	
	/*
	 * =========== Methods for Connection->FConnectClause lookup ==============
	 */

	private Connection FConnectClause.connection = null;
	
	public void FConnectClause.setConnection(Connection con) {
		if (connection != null)
			throw new ASTEditException("Changing of Connection once it has been set is now allowed.");
		connection = con;
	}
	
	public Connection FConnectClause.getConnection() {
		return connection;
	}
	
	private FConnectClause InstClassDecl.findOwnerOf(Connection con) {
		for (FAbstractEquation fae : getFAbstractEquations()) {
			if (fae instanceof FConnectClause) {
				FConnectClause fcc = (FConnectClause) fae;
				if (fcc.getConnection() == con)
					return fcc;
			}
		}
		return null;
	}
	
	/*
	 * =========== Methods for Component->InstComponentDecl lookup ============
	 */

//	private Component InstComponentDecl.component = null;
//	
//	public void InstComponentDecl.setComponent(Component c) {
//		if (component != null)
//			throw new ASTEditException("Changing of Component once it has been set is now allowed.");
//		component = c;
//	}
//	
//	public Component InstComponentDecl.getComponent() {
//		return component;
//	}
//	
//	protected InstComponentDecl InstNode.findOwnerOf(Component c) {
//		for(InstComponentDecl icd : getInstComponentDecls()) {
//			if (icd.getComponent() == c)
//				return icd;
//		}
//		for (InstExtends ie : instExtends()) {
//			InstComponentDecl result = ie.findOwnerOf(c);
//			if (result != null)
//				return result;
//		}
//		for(InstComponentDecl icd : getInstComponentDecls()) {
//			InstComponentDecl result = icd.findOwnerOf(c);
//			if (result != null)
//				return result;
//		}
//		return null;
//	}
	
	/*
	 * ======================= Observer Related Methods =======================
	 */
	
//	private Map<Component, InstComponentDecl> InstClassDecl.childComponentMap = new HashMap<Component, InstComponentDecl>();
//
//	public class InstClassDecl implements Observer{}
//	public void InstClassDecl.update(Observable o, Object flag, Object additionalInfo) {
//		if (o == diagram() && flag == Icon.SUBCOMPONENT_ADDED) {
//			InstComponentDecl icd = findOwnerOf((Component)additionalInfo);
//			if (icd != null) {
//				System.err.println("Child already added!");
//				return;
//			}
//			icd = childComponentMap.remove(additionalInfo);
//			if (icd == null)
//				throw new ASTEditException("This should not be null, the component has been created else where");
//			((FullClassDecl)getClassDecl()).addNewComponentDecl(icd.getComponentDecl());
//			addInstComponentDecl(icd);
//		} else if (o == diagram() && flag == Icon.SUBCOMPONENT_REMOVED) {
//			InstComponentDecl icd = findOwnerOf((Component)additionalInfo);
//			if (icd == null)
//				throw new ASTEditException("This should not be null, the component has been created else where");
//			removeInstComponentDecl(icd);
//			((FullClassDecl)getClassDecl()).removeComponentDecl(icd.getComponentDecl());
//			childComponentMap.put((Component)additionalInfo, icd);
//		} else if (o == diagram() && flag == Diagram.CONNECTION_ADDED) {
//			Connection con = (Connection)additionalInfo;
//			FConnectClause fcc = findOwnerOf(con);
//			if (fcc == null) {
//				ConnectClause cc = new ConnectClause();
//				cc.setComment(new Comment());
//				cc.annotation().saveConnectionLine(con);
//				cc.setConnector1(Access.fromClassName(con.getSourceConnector().getComponentName()));
//				cc.setConnector2(Access.fromClassName(con.getTargetConnector().getComponentName()));
//				((FullClassDecl)getClassDecl()).addNewEquation(cc);
//				fcc = (FConnectClause)cc.instantiate();
//				fcc.setConnection(con);
//				addFAbstractEquation(fcc);
//				con.addObserver(fcc);
//			} else {
//				((FullClassDecl)getClassDecl()).addNewEquation(fcc.getConnectClause());
//				addFAbstractEquation(fcc);
//			}
//			if (findOwnerOf((Component)con.getSourceConnector()) != fcc.getConnector1().getInstAccess().myInstComponentDecl()) {
//				// Reconnect?
//			}
//			if (findOwnerOf((Component)con.getTargetConnector()) != fcc.getConnector2().getInstAccess().myInstComponentDecl()) {
//				// Reconnect?
//			}
//		} else if (o == diagram() && flag == Diagram.CONNECTION_REMOVED) {
//			FConnectClause fcc = findOwnerOf((Connection)additionalInfo);
//			if (fcc == null)
//				throw new ASTEditException("This should not be null, the connection has been created else where");
//			removeFAbstractEquation(fcc);
//			((FullClassDecl)getClassDecl()).removeEquation(fcc.getConnectClause());
//		}
//	}
//	  
	public boolean FullClassDecl.removeComponentDecl(ComponentDecl cd) {
		int componentIndex = getComponentDecls().getIndexOfChild(cd);
		if (componentIndex < 0) {
			return false;
		}

		int clauseIndex = originalComposition.getClauses().getIndexOfChild(cd);
		if (clauseIndex >= 0) {
				originalComposition.getClauses().removeChild(clauseIndex);
		}
		
		getComponentDecls().removeChild(componentIndex);

		return true;
	}
	
	public boolean InstClassDecl.removeInstComponentDecl(InstComponentDecl icd) {
		for (int i = 0; i < getNumInstComponentDecl(); i++) {
			if (getInstComponentDecl(i) == icd) {
				getInstComponentDecls().removeChild(i);
				return true;
			}
		}
		return false;
	}
	
	public boolean InstClassDecl.removeFAbstractEquation(FAbstractEquation fae) {
		for (int i = 0; i < getNumFAbstractEquation(); i++) {
			if (getFAbstractEquation(i) == fae) {
				getFAbstractEquations().removeChild(i);
				return true;
			}
		}
		return false;
	}
	
	public boolean FullClassDecl.removeEquation(AbstractEquation ae) {
		int equationIndex = getEquations().getIndexOfChild(ae);
		if (equationIndex < 0) {
			return false;
		}

		int removeClause = -1;
		for (int i = 0; i < originalComposition.getNumClause(); i++) {
			if (originalComposition.getClause(i) instanceof EquationClause) {
				EquationClause equationClause = (EquationClause) originalComposition.getClause(i);
				int childIndex = equationClause.getAbstractEquations().getIndexOfChild(ae);
				if (childIndex >= 0) {
					equationClause.getAbstractEquations().removeChild(childIndex);
					if (equationClause.getNumAbstractEquation() == 0) {
						removeClause = i;
					}
					break;
				}
			}
		}
		if (removeClause >= 0) {
			originalComposition.getClauses().removeChild(removeClause);
		}
		
		getEquations().removeChild(equationIndex);

		return true;
	}
//	
//	public class InstComponentDecl implements Observer{}
//	public void InstComponentDecl.update(Observable o, Object flag, Object additionalInfo) {
//		if (o == getComponent() && flag == Component.PLACEMENT_UPDATED) {
//			getComponentDecl().annotation().savePlacement(((Component)o).getPlacement());
//		}
//	}
	
	/*
	 * F�r tidtagning.
	 */
//	eq ClassDecl.contentOutlineImage() {
//		System.out.println("ClassDecl.contentOutlineImage(), Namn = " + name());
//	
//		java.util.Date before = new Date();
//		Icon icon = icon();
//		java.util.Date after = new Date();
//		long duration = after.getTime()-before.getTime();
//		System.out.println("icon() tog " + duration + " ms.");
//		
//		if (icon.equals(Icon.NULL_ICON)) {
//			return ImageLoader.getImage("dummy.png");
//		}
//		
//		before = new Date();
//		Image image = icon.draw(new AWTIconDrawer(icon));
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("draw() tog " + duration + " ms.");
//		
//		return image;
//	}
//	eq ComponentDecl.contentOutlineImage() {
//		
//		System.out.println("ComponentDecl.contentOutlineImage(), Namn = " + name());
//		
//		java.util.Date before = new Date();
//		ClassDecl decl = findClassDecl();
//		java.util.Date after = new Date();
//		long duration = after.getTime()-before.getTime();
//		System.out.println("findClassDecl() tog " + duration + " ms.");
//		
//		before = new Date();
//		Icon icon = icon();
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("icon() tog " + duration + " ms.");
//	
//		if (icon.equals(Icon.NULL_ICON)) {
//			return ImageLoader.getImage("dummy.png");
//		}
//		
//		before = new Date();
//		Image image = icon.draw(new AWTIconDrawer(icon));
//		after = new Date();
//		duration = after.getTime()-before.getTime();
//		System.out.println("draw() tog " + duration + " ms.");
//		return image;
//	}
}