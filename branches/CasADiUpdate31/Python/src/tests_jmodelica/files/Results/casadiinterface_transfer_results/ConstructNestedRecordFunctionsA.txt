ModelFunction : AtomicModelRecordNestedArray_generateCurves
 Number of inputs: 1
  Input 0 ("i0"): 1-by-1 (dense)
 Number of outputs: 8
  Output 0 ("o0"): 1-by-1 (dense)
  Output 1 ("o1"): 1-by-1 (dense)
  Output 2 ("o2"): 1-by-1 (dense)
  Output 3 ("o3"): 1-by-1 (dense)
  Output 4 ("o4"): 1-by-1 (dense)
  Output 5 ("o5"): 1-by-1 (dense)
  Output 6 ("o6"): 1-by-1 (dense)
  Output 7 ("o7"): 1-by-1 (dense)
@0 = 0
output[0] = @0
@0 = input[0][0]
output[1] = @0
@0 = 2
output[2] = @0
@1 = 3
output[3] = @1
@2 = 6
output[4] = @2
@2 = 7
output[5] = @2
output[6] = @0
output[7] = @1

