ModelFunction : AtomicModelVector3_f
 Number of inputs: 4
  Input 0 ("i0"): 1-by-1 (dense)
  Input 1 ("i1"): 1-by-1 (dense)
  Input 2 ("i2"): 1-by-1 (dense)
  Input 3 ("i3"): 1-by-1 (dense)
 Number of outputs: 4
  Output 0 ("o0"): 1-by-1 (dense)
  Output 1 ("o1"): 1-by-1 (dense)
  Output 2 ("o2"): 1-by-1 (dense)
  Output 3 ("o3"): 1-by-1 (dense)
@0 = input[0][0]
@0 = (-@0)
output[0] = @0
@0 = input[1][0]
@0 = (-@0)
output[1] = @0
@0 = input[2][0]
@0 = (2.*@0)
output[2] = @0
@0 = input[3][0]
@0 = (2.*@0)
output[3] = @0

