ModelFunction : AtomicModelAtomicBooleanFunctions_polyInPolyOut
 Number of inputs: 2
  Input 0 ("i0"): 1-by-1 (dense)
  Input 1 ("i1"): 1-by-1 (dense)
 Number of outputs: 2
  Output 0 ("o0"): 1-by-1 (dense)
  Output 1 ("o1"): 1-by-1 (dense)
@0 = input[0][0]
output[0] = @0
@0 = input[1][0]
output[1] = @0

