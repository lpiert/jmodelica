
/*
    Copyright (C) 2016 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect AccessSizes {
    
    syn Size InstAccess.declarationSize() = Size.SCALAR;
    eq InstNamedAccess.declarationSize()  = myInstComponentDecl().size();
    
    /**
     * Get the array sizes. 
     */
    syn Size FSubscript.size();
    eq FExpSubscript.size()     = getFExp().size();
    eq FColonSubscript.size()   = inSubscriptedExp() ? mySize() : accessContextSize();
    eq FIntegerSubscript.size() = Size.SCALAR;

    inh boolean FSubscript.inSubscriptedExp();
    eq FSubscriptedExp.getFArraySubscripts().inSubscriptedExp()        = true;
    eq InstArrayAccess.getFArraySubscripts().inSubscriptedExp()        = false;
    eq InstScalarAccess.getExpandedSubscripts().inSubscriptedExp()     = false;
    eq InstShortClassDecl.getFArraySubscripts().inSubscriptedExp()     = false;
    eq InstComponentDecl.getFArraySubscripts().inSubscriptedExp()      = false;
    eq InstComponentDecl.getLocalFArraySubscripts().inSubscriptedExp() = false;
    eq FQNamePartArray.getFArraySubscripts().inSubscriptedExp()        = false;
    eq FIdUse.getExpandedSubscripts().inSubscriptedExp()               = false;

    /**
     * Get the size this subscript(s) would have if all subscripts were colons.
     */
    inh Size FSubscript.mySize();
    inh Size FArraySubscripts.mySize();
    eq Root.getChild().mySize()                       = Size.SCALAR;
    eq FArraySubscripts.getFSubscript(int i).mySize() = mySize().contract(i);
    eq FSubscriptedExp.getFArraySubscripts().mySize() = getFExp().size();
    eq InstNamedAccess.getChild().mySize()            = declarationSize();
    
    inh Size FEndExp.mySize();
    eq FExpSubscript.getChild().mySize()   = accessContextSize();
    
    eq FIdUse.getChild().mySize() = getFQName().calcMySize(getFQName().numParts());
    eq FQNameFull.getFQNamePart(int i).mySize() = calcMySize(i+1);
    
    syn Size FQName.calcMySize(int i) = lookupFV(asFQNameFull()).size();
    eq FQNameFull.calcMySize(int i) {
        if (numParts() > 1) {
            int j;
            FAbstractVariable fv = null;
            FQNameFull fqn = new FQNameFull();
            fqn.parent = this;
            for (j = 0; j < i; j++) {
                fqn.addFQNamePart(getFQNamePart(j).copyFirstElement());
                fv = lookupFV(fqn);
                if (!fv.isUnknown()) {
                    break;
                }
            }
            return fv.lookupHierarchicalSize(this, j, i);
        }
        return super.calcMySize(i);
    }
    
    syn FQNamePart FQNamePart.copyFirstElement() = new FQNamePart(getName());
    eq FQNamePartArray.copyFirstElement()        = new FQNamePartArray(getName(), accessContextFAS());
    
    public Size FAbstractVariable.lookupHierarchicalSize(FQNameFull name, int part, int last) {
        return type().lookupHierarchical(name, part+1, last).size();
    }
    
    public Size FRecordVariable.lookupHierarchicalSize(FQNameFull name, int part, int last) {
        FExp res = findHierarchicalAttributeValue(name, last, FAttribute.SIZE);
        if (res != null) {
            for (int i = part; i < last - 1; i++) {
                FArraySubscripts fas = name.getFQNamePart(i).getFArraySubscripts();
                if (fas != null) {
                    res = res.splitArrayExp(fas.createIndex());
                }
            }
            return new MutableSize(res);
        }
        return super.lookupHierarchicalSize(name, part, last);
    }
    
    public Size Size.contract(int i) {
        if (has(i)) {
            return contract(i,size.length-i-1);
        } else {
            return Size.SCALAR;
        }
    }
    
    syn Size FSubscript.accessContextSize() = mySize().accessContextSize(this);
    
    public Size Size.accessContextSize(FSubscript sub) {
        return this;
    }
    
    public Size MutableSize.accessContextSize(FSubscript sub) {
        if (isCircular(0) || size[0] != Size.UNKNOWN) {
            return super.accessContextSize(sub);
        }
        MutableSize ms = new MutableSize(1);
        ms.set(0, sub.getAccessContextSizeExp());
        return ms;
    }
    
    syn FExp FSubscript.getAccessContextSizeExp()      = null;
    syn lazy FExp FColonSubscript.getAccessContextSizeExp() = mySize().buildAccessContextSizeExp(this);
    syn lazy FExp FExpSubscript.getAccessContextSizeExp()   = mySize().buildAccessContextSizeExp(this);
    
    public FExp Size.buildAccessContextSizeExp(FSubscript fs)        { throw new UnsupportedOperationException(); }
    public FExp MutableSize.buildAccessContextSizeExp(FSubscript fs) {
        if (exps[0] == null || exps[0] instanceof FColonSizeExp) {
            return new FUnknownSizeExp(fs.buildAccessContextSizeExp(), fs.myDim());
        }
        return exps[0].copyReplaceAccess(new FExp.SizeContextReplacer(fs));
    }
    
    class FExp {
        public static class SizeContextReplacer implements ContextReplacer {
            private FSubscript fs;
            public SizeContextReplacer(FSubscript fs) {
                this.fs = fs;
            }
            
            public FExp copyReplaceAccess(FIdUse use) {
                return use.buildAccessContextSizeExp(this);
            }
            
            public FExp copyReplaceAccess(FSizeExp exp) {
                return exp.copyReplaceAccessExp(this);
            }
            
            public List<InstAccess> buildContext() {
                 return fs.myInstAccess().accessContextInst();
            }
        }
    }
    
    public FExp FIdUse.buildAccessContextSizeExp(FExp.SizeContextReplacer crp) {
        return new FIdUseExp(treeCopy());
    }
    
    public FExp FIdUseInstAccess.buildAccessContextSizeExp(FExp.SizeContextReplacer crp) {
        return new FIdUseExp(new FIdUseInstAccess(getInstAccess().buildAccessContextSizeExp(crp)));
    }
    
    public InstAccess InstAccess.buildAccessContextSizeExp(FExp.SizeContextReplacer crp) {
        List<InstAccess> res = crp.buildContext();
        for (InstAccess part : allParts()) {
            res.addChild(part.copyReplaceAccess(crp));
        }
        return new InstDot(res);
    }
    
    inh FIdUseExp FSubscript.buildAccessContextSizeExp();
    eq Root.getChild().buildAccessContextSizeExp()   = null;
    eq InstNamedAccess.getChild().buildAccessContextSizeExp() =
            new FIdUseExp(new FIdUseInstAccess(new InstDot(buildAccessContext(name(), null))));
    eq FQNamePart.getChild().buildAccessContextSizeExp() =
            new FIdUseExp(new FIdUse(new FQNameFull(buildAccessContext(name(), null))));
    eq FIdUse.getChild().buildAccessContextSizeExp() =
            new FIdUseExp(new FIdUse(new FQNameFull(getFQName().buildAccessContext())));
    
    syn List<FQNamePart> FQName.buildAccessContext() = null;
    eq FQNameString.buildAccessContext() {
        List<FQNamePart> l = new List<FQNamePart>();
        l.add(new FQNamePart(name()));
        return l;
    }
    eq FQNameFull.buildAccessContext() {
        FQNamePart last = getFQNamePart(getNumFQNamePart()-1);
        return last.buildAccessContext(last.name(), null);
    }
    
    public List<InstAccess> InstAccess.buildAccessContext(String name, FArraySubscripts fas) {
        List<InstAccess> res = accessContextInst();
        if (fas == null) {
            res.add(new InstAmbiguousAccess(name));
        } else {
            res.add(new InstAmbiguousArrayAccess(name, fas));
        }
        return res;
    }
    
    public List<FQNamePart> FQNamePart.buildAccessContext(String name, FArraySubscripts fas) {
        List<FQNamePart> res = accessContextFlat();
        if (fas == null) {
            res.add(new FQNamePart(name));
        } else {
            res.add(new FQNamePartArray(name, fas));
        }
        return res;
    }
    
    inh List<InstAccess> InstAccess.accessContextInst();
    eq Root.getChild().accessContextInst()     = new List<InstAccess>();
    eq FExp.getChild().accessContextInst()     = new List<InstAccess>();
    eq InstDot.getInstAccess(int i).accessContextInst() = 
            i == 0 ? new List<InstAccess>() : getInstAccess(i-1).buildAccessContext();
    
    inh List<FQNamePart> FQNamePart.accessContextFlat();
    eq Root.getChild().accessContextFlat()     = new List<FQNamePart>();
    eq FExp.getChild().accessContextFlat()     = new List<FQNamePart>();
    eq FQNameFull.getFQNamePart(int i).accessContextFlat() = 
            i == 0 ? new List<FQNamePart>() : getFQNamePart(i-1).buildAccessContext();
    
    syn List<InstAccess> InstAccess.buildAccessContext() = buildAccessContext(name(), accessContextFAS());
    syn List<FQNamePart> FQNamePart.buildAccessContext() = buildAccessContext(name(), accessContextFAS());
    
    syn FArraySubscripts InstAccess.accessContextFAS() {
        FArraySubscripts res = null;
        if (declarationDims() > 0) {
            for (FArraySubscripts fas : allFArraySubscripts()) {
                res = fas.buildAccessContext();
            }
        }
        return res;
    }
    
    syn FArraySubscripts FQNamePart.accessContextFAS() = null;
    eq FQNamePartArray.accessContextFAS() = getFArraySubscripts().buildAccessContext();
    
    syn FArraySubscripts FArraySubscripts.buildAccessContext() {
        FArraySubscripts res = new FArraySubscripts();
        for (FSubscript fs : getFSubscripts()) {
            res.addFSubscript(fs.buildAccessContext());
        }
        return res;
    }
    
    syn FSubscript FSubscript.buildAccessContext() = treeCopy();
    eq FColonSubscript.buildAccessContext()   = new FIntegerSubscript(1);
    eq FExpSubscript.buildAccessContext()     = getFExp().isArray() ? 
            new FExpSubscript(getFExp().createFirstIndexExp()) : super.buildAccessContext();
    
    syn FExp FExp.createFirstIndexExp() {
        FArraySubscripts fas = new FArraySubscripts();
        fas.addFSubscript(new FIntegerSubscript(1));
        return new FSubscriptedExp(treeCopy(), fas);
    }
}

aspect FunctionCallSizes {
    
    syn Size FAbstractFunctionCall.sizeOfOutput(int i) = (i == 0) ? size() : Size.SCALAR;
    eq CommonFunctionCall.sizeOfOutput(int i)          = typeOfOutput(i).size();
    
    /**
     * Get array sizes of function argument.
     */
    syn Size InstFunctionArgument.size() = null;
    eq InstDefaultArgument.size()        = getFExp().size();
    eq InstGivenArgument.size()          = getFExp().size();

    eq FArgumentExp.size() = copiedFExp().size();
    
    public FType FType.createSizeFExp(InstFunctionCall call) {
        FType res = treeCopy();
        res.replaceSizeFExp(call);
        return res;
    }
    
    public void ASTNode.replaceSizeFExp(InstFunctionCall call) {
        for (ASTNode n : this) {
            n.replaceSizeFExp(call);
        }
    }
    
    public void FType.replaceSizeFExp(InstFunctionCall call) {
        setSize(getSize().createSizeFExp(call));
        super.replaceSizeFExp(call);
    }
    
    public Size Size.createSizeFExp(InstFunctionCall call) {
        
        if (!isUnknown() || call.isCircular())
            return this;
        
        HashMap<InstComponentDecl, InstFunctionArgument> varMap = new HashMap<InstComponentDecl, InstFunctionArgument>();
        for (int j = 0; j < call.getNumArg(); j++) {
            call.getArg(j).sizeOfOutput(varMap, call.myCallInputs().get(j));
        }
        
        MutableSize ms = new MutableSize(ndims());
        FExp.FunctionSizeContextReplacer crp = new FExp.FunctionSizeContextReplacer(varMap);
        for (int j = 0; j < ndims(); j++) {
            try {
                ms.append(call.dynamicFExp(copyReplaceAccess(j, crp)));
            } catch (ConstantEvaluationException e) {
                ms.append(Size.UNKNOWN);
            }
        }
        return ms;
    }
    
    class FExp {
        public static class FunctionSizeContextReplacer implements ContextReplacer {
            Map<InstComponentDecl, InstFunctionArgument> varMap;
            public FunctionSizeContextReplacer(Map<InstComponentDecl, InstFunctionArgument> varMap) {
                this.varMap = varMap;
            }
            
            public FExp copyReplaceAccess(FIdUse use) {
                return use.createSizeFExp(this);
            }
            
            public FExp copyReplaceAccess(FSizeExp exp) {
                return exp.createSizeFExp(this);
            }
            
            public Map<InstComponentDecl, InstFunctionArgument> getMap() {
                return varMap;
            }
        }
    }
    
    public void InstFunctionArgument.sizeOfOutput(Map<InstComponentDecl, InstFunctionArgument> varMap, InstComponentDecl icd) {
        getFExp().size();      // Cash sizes to prevent false circularity in nested function calls
        varMap.put(icd, this);
    }
    public void InstMissingArgument.sizeOfOutput(Map<InstComponentDecl, InstFunctionArgument> varMap, InstComponentDecl icd) {
        
    }
    
    protected boolean FSizeExp.circ = false;
    public FExp FSizeExp.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        if (circ) {
            throw new ConstantEvaluationException();
        }
        circ = true;
        FExp res = getFExp().size().copyReplaceAccess(dimension(), crp);
        circ = false;
        return res;
    }
    
    public FExp FUnknownSizeExp.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        if (circ) {
            throw new ConstantEvaluationException();
        }
        circ = true;
        FExp res = getFExp().copyReplaceAccess(crp).size().createFExp(dimension());
        circ = false;
        return res;
    }
    
    public FExp FIdUse.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        throw new UnsupportedOperationException();
    }
    
    public FExp FIdUseInstAccess.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        return getInstAccess().createSizeFExp(crp);
    }
    
    public FExp InstAccess.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        InstComponentDecl var = myInstComponentDecl();
        InstFunctionArgument arg = crp.getMap().get(var);
        if (arg != null) {
            return arg.createSizeFExp(crp).cell(crp, getFArraySubscripts());
        } else {
            return copyReplaceAccess(crp).createFExp();
        }
    }
    
    public FExp InstFunctionArgument.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        // TODO: Should use a copy, since this is a create method, but that gives circularity when calculating size(),
        //       see InstFunctionArgument.sizeOfOutput(). We need a copy, but with the pre-calculated size. 
        return getFExp();
    }
    
    public FExp InstDefaultArgument.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        return getFExp().dynamicFExp(getFExp().copyReplaceAccess(crp));
    }
    
    public FExp InstDot.createSizeFExp(FExp.FunctionSizeContextReplacer crp) {
        FExp exp = null;
        for (InstAccess ia : getInstAccesss()) {
            if (exp == null) {
                exp = ia.createSizeFExp(crp);
            } else {
                exp = exp.component(ia.name()).cell(crp, ia.getFArraySubscripts());
            }
        }
        return exp;
    }
    
    public FExp FExp.cell(FExp.FunctionSizeContextReplacer crp, FArraySubscripts fas) {
        if (fas != null) {
            if (fas.variability().knownParameterOrLess()) {
                try {
                    return splitArrayExp(fas.createIndex());
                } catch (ConstantEvaluationException e) {
                    
                }
            }
            return new FSubscriptedExp(this, fas.copyReplaceAccess(crp));
        }
        return this;
    }
    
    /**
     * Create a copy of this expression suitable for when it is used to describe a size.
     * 
     * Default implementation simply returns a copy.
     */
    public FExp FExp.createSizeFExp() {
        return unboundCopy();
    }
    
    public FExp FColonSizeExp.createSizeFExp() {
        return new FSizeExp(getVariable().createUseExp(), getDim());
    }
    
    
    /**
     * Return the size the function call is expanded to. This is the size of the 
     * entire function call, and the size of a vectorized argument to a scalar input.
     * 
     * If arguments doesn't match, the size matching the most arguments is returned.
     */
    syn lazy Size InstVectorFunctionCall.vectorizedSize() {
        boolean func = inFunction();
        int tot = getNumArg(), n = 0, ndims = ndims();
        Size[] candidates = new Size[tot];
        int[] freq = new int[tot];
        
        for (InstFunctionArgument arg : getArgs()) {
            if (arg.isVectorized()) {
                Size s = arg.size().contractRight(ndims);
                for (int i = 0; i < n && s != null; i++) {
                    if (!candidates[i].equivalent(s, func)) {
                        if (func && candidates[i].isUnknown())
                            candidates[i] = candidates[i].createKnown(s);
                        s = null;
                        freq[i]++;
                    }
                }
                if (s != null)
                    candidates[n++] = s;
            }
        }
    
        int index = 0;
        for (int i = 1; i < n; i++) 
            if (freq[index] < freq[i]) 
                index = i;
        return candidates[index];
    }
    
    inh Size InstFunctionArgument.vectorizedSize();
    eq FAbstractFunctionCall.getChild().vectorizedSize() = Size.SCALAR;
    eq InstVectorFunctionCall.getArg().vectorizedSize()  = vectorizedSize();
    eq FDelayExp.getChild().vectorizedSize()             = vectorizedSize();
    eq FSpatialDistExp.getChild().vectorizedSize()       = vectorizedSize();
}

aspect SizesUtil {
    
    class FExp {
        public interface ContextReplacer {
            public FExp copyReplaceAccess(FIdUse use);
            public FExp copyReplaceAccess(FSizeExp exp);
        }
    }
    
    /**
     * Create a copy of this FExp with variable uses and function calls replaced with expressions 
     * that allow calculations of unknown sizes.
     * 
     * For non-FExp nodes, this simply makes a full copy.
     */
    public ASTNode ASTNode.copyReplaceAccess(FExp.ContextReplacer crp) {
        return treeCopy();
    }
    
    public List List.copyReplaceAccess(FExp.ContextReplacer crp) {
        List node = new List();
        for (ASTNode ch : this)
            node.add(ch.copyReplaceAccess(crp));
        return node;
    }
    
    public Opt Opt.copyReplaceAccess(FExp.ContextReplacer crp) {
        return getNumChild() == 0 ? new Opt() : new Opt(getChild(0).copyReplaceAccess(crp));
    }
    
    public FExp FExp.copyReplaceAccess(FExp.ContextReplacer crp) {
        FExp exp = copy();
        for (int i = 0; i < getNumChild(); i++) {
            exp.setChild(getChild(i).copyReplaceAccess(crp), i);
        }
        return exp;
    }
    
    public FExp FSizeExp.copyReplaceAccess(FExp.ContextReplacer crp) {
        return crp.copyReplaceAccess(this);
    }
    
    public FExp FSizeExp.copyReplaceAccessExp(FExp.ContextReplacer crp) {
        return super.copyReplaceAccess(crp);
    }
    
    public FExp InstFunctionCall.copyReplaceAccess(FExp.ContextReplacer crp) {
        InstAccess name = InstAccess.fromName(getName().myInstCallable().actualInstCallable().qualifiedName());
        return new InstFunctionCall(name, getArgs().copyReplaceAccess(crp));
    }
    
    public InstFunctionArgument InstFunctionArgument.copyReplaceAccess(FExp.ContextReplacer crp) {
        return fullCopy();
    }
    
    public InstPositionalArgument InstPositionalArgument.copyReplaceAccess(FExp.ContextReplacer crp) {
        return new InstPositionalArgument(getFExp().copyReplaceAccess(crp), getPos());
    }
    
    public InstNamedArgument InstNamedArgument.copyReplaceAccess(FExp.ContextReplacer crp) {
        return new InstNamedArgument(getFExp().copyReplaceAccess(crp), getName().fullCopy());
    }
    
    public FExp FIdUseExp.copyReplaceAccess(FExp.ContextReplacer crp) {
        return crp.copyReplaceAccess(getFIdUse());
    }
    
    public InstAccess InstAccess.copyReplaceAccess(FExp.ContextReplacer crp) {
        if (hasFArraySubscripts()) {
            return new InstAmbiguousArrayAccess(name(), getFArraySubscripts().copyReplaceAccess(crp));
        } else {
            return new InstAmbiguousAccess(name());
        }
    }
    
    public FArraySubscripts FArraySubscripts.copyReplaceAccess(FExp.ContextReplacer crp) {
        FArraySubscripts res = new FArraySubscripts();
        for (FSubscript f : getFSubscripts()) {
            res.addFSubscript(f.copyReplaceAccess(crp));
        }
        return res;
    }
    
    public FSubscript FSubscript.copyReplaceAccess(FExp.ContextReplacer crp) {
        return treeCopy();
    }
    
    public FSubscript FExpSubscript.copyReplaceAccess(FExp.ContextReplacer crp) {
        return new FExpSubscript(getFExp().copyReplaceAccess(crp));
    }
    
    /**
     * Create an FExp that describes the length of dimension <code>d</code>, 
     *        given the supplied variable values.
     *        
     * @param varMap  a map of variables to replace and expressions to replace them with
     */
    public FExp Size.copyReplaceAccess(int d, FExp.ContextReplacer crp) {
        if (d < 0 || d >= ndims()) {
            throw new ConstantEvaluationException();
        }
        return createFExp(d);
    }
    
    public FExp MutableSize.copyReplaceAccess(int d, FExp.ContextReplacer crp) {
        evaluate(d);
        return (okExp(d, true)) ?
                exps[d].copyReplaceAccess(crp) :
                super.copyReplaceAccess(d, crp);
    }
    
}