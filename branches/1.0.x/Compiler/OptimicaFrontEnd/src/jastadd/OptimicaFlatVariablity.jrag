
/**
 * This aspect introduces a new type of variability: timed. An expression has 
 * timed variability if it is composed of parameters, literals and timed
 * variables but no continuous or discrete variables.
 */
aspect OptimicaFlatVariability {

	syn boolean FTypePrefixVariability.timedVariability() = false;
	eq FTimed.timedVariability() = true;	
	syn boolean FExp.isTimedExp() = variability().timedVariability();
	
	eq FTimedVariable.variability() = fTimed();
	eq FStartTimeExp.variability() = fParameter();
	eq FFinalTimeExp.variability() = fParameter();
	
	public static final FTimed FTimed.singleton = new FTimed();	

	public static FTimed ASTNode.fTimed() {
		return FTimed.singleton;
	}
	
	syn boolean FTypePrefixVariability.optParameterVariability() = false;
	eq FTimed.optParameterVariability() = true;	
	syn boolean FExp.isOptParameterExp() = variability().optParameterVariability();
		
	public static final FOptParameter FOptParameter.singleton = new FOptParameter();	
	
	// Notice that the attribute FVariable.isParameter is not redefined in order
	// not to render a lot of attributes in FlatAPI to be redefined as a
	// consequence.
    syn boolean FVariable.isOptParameter() = 
    	getFTypePrefixVariability().parameterVariability() &&
    	freeAttribute();
	
	public static FOptParameter ASTNode.fOptParameter() {
		return FOptParameter.singleton;
	}
	
	refine FlatVariability eq FVariable.variability() {
   		if (!isContinuous()) {
   			if (isOptParameter()) {
   				return fOptParameter();
   			}
   			return getFTypePrefixVariability();
   		} else if (isInteger() || isBoolean() || isString()) {
   			return(fDiscrete());
   		} else {
   			return fContinuous();
   		}
	}
	
   	refine FlatVariability public FTypePrefixVariability FTypePrefixVariability.combine(FTypePrefixVariability other) {   			
   		if (this instanceof FContinuous) {
   			return this;
   		} else if (other instanceof FContinuous) {
   			return other;
   		// neither is a continuous variable
   		} else if (this instanceof FDiscrete) {
   			return this;
   		} else if (other instanceof FDiscrete) {
   			return other;
   	   		// neither is a discrete variable or discrete variable   			
   		} else if (this instanceof FTimed) {
   			return this;
   		} else if (other instanceof FTimed) {
   			return other;
   		} else if (this instanceof FOptParameter) {
   			return this;
   		} else if (other instanceof FOptParameter) {
   			return other;
   		// neither is a optimized parameter variable, discrete variable, or continuous value
   		} else if (this instanceof FParameter) {
   			return this;
   		} else if (other instanceof FParameter) {
   			return other;
   		// neither is a continuous variable, discrete variable, or a parameter value
   		} else {
   			return this;
   		}
	}	

   	eq FTimed.toString() = "timed";
   	eq FOptParameter.toString() = "opt_parameter";
	
	syn boolean FConstraint.isTimed() = 
		(getRight().isTimedExp() | getRight().isParameterExp() | getRight().isConstantExp()) & 
		(getLeft().isTimedExp() | getLeft().isParameterExp() | getLeft().isConstantExp());

	syn boolean FConstraint.isParameter() = 
		(getRight().isParameterExp() | getRight().isConstantExp()) & 
		(getLeft().isParameterExp() | getLeft().isConstantExp());

	
}