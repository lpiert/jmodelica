/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect OptimicaFlattening {

    public InstFullClassDecl OptClassDecl.newInstClassDecl() {
    	InstOptClassDecl fcd = new InstOptClassDecl(this,new Opt(),new Opt());
	    fcd.setInstConstrainingOpt(newInstConstrainingClassOpt());
	    if (hasClassModification()) {
	    	fcd.setInstClassModification(getClassModification().newInstModification());
	    }
    	fcd.setStart(getStart());
    	return fcd;
    }
	
        refine Flattening public InstNode InstProgramRoot.findFlattenInst(String className,FClass fc) throws ModelicaClassNotFoundException {
    	
    	InstClassDecl icd = simpleLookupInstClassDecl(className);
    	if (!icd.isUnknown()) {
    		icd.flattenInstClassDecl(fc);
    		
    	} else {
        	throw new ModelicaClassNotFoundException("Class "+ className + "not found");
    	}
    	
    	return icd;
    }
    
        
    refine Flattening void InstClassDecl.flattenInstClassDecl(FClass fc) {
   		FQName fqn = new FQName();
		int index = 0;
		int new_index = 0;
		String qn = qualifiedName();
		while (index>=0) {
			new_index = qn.indexOf('.',index);
 			if (new_index>=0) {
				fqn.addFQNamePart(new FQNamePart(qn.substring(index,new_index),new Opt()));
    			index = new_index + 1;
			} else {
				fqn.addFQNamePart(new FQNamePart(qn.substring(index,qn.length()),new Opt()));
    			index = new_index;
			}
		}
		fc.setFQName(fqn); 
		fc.addFEquationBlock(new FEquationBlock(new List()));			
		for (FAbstractEquation ae : getFAbstractEquations()) {
		       ae.flatten(getFQName(),fc);
		    }
		getInstComponentDeclList().flatten(fc);
		getInstExtendsList().flatten(fc);
		
		if (this instanceof InstOptClassDecl) {
			FOptClass foc = (FOptClass)fc;
			InstOptClassDecl iocd = (InstOptClassDecl)this;
			for (FConstraint c : iocd.getFConstraints()) {
			       c.flatten(iocd.getFQName(),foc);
			    }
			OptClassDecl ocd = (OptClassDecl)iocd.getClassDecl();
			if (iocd.hasInstClassModification()) {
				List attr = new List();
				iocd.getInstClassModification().collectAttributes(attr,new FQName(new List()));
				foc.setFAttributeList(attr);
			}
		}
		
		fc.genConnectionEquations();

		//System.out.println(fc.prettyPrint(""));
		//fc.dumpTree("");
					
		HashSet<InstAccess> instAccesses = fc.collectInstAccesses();
		if (instAccesses.size()>0) {
			System.out.println("Flat model contains InstAccesses!!!");
			//return null;
		}
	 	
    }
	
	refine Flattening public void InstNode.flatten(FClass fc) {
		for (FAbstractEquation ae : getFAbstractEquations()) {
		   ae.flatten(getFQName(),fc);
		}
        for (FConstraint c : getFConstraints()) {
           c.flatten(getFQName(),fc);
        }
		getInstComponentDeclList().flatten(fc);
		getInstExtendsList().flatten(fc);
	}	

	syn lazy ArrayList<Constraint> ClassDecl.constraints() = new ArrayList<Constraint>();
	eq OptClassDecl.constraints() {
	    ArrayList<Constraint> l = new ArrayList<Constraint>();
		for (Constraint c : getConstraints())
			l.add(c);
		return l;
	}
	eq ShortClassDecl.constraints() = getExtendsClauseShortClass().getSuper().myClassDecl().constraints();
	
	syn ArrayList<Constraint> InstNode.constraints() = new ArrayList<Constraint>();
	eq InstClassRoot.constraints() = getClassDecl().constraints();
	eq InstOptClassDecl.constraints() = getClassDecl().constraints();
	eq InstComponentDecl.constraints() = getComponentDecl().myClass().constraints();
	eq InstExtends.constraints() = getExtendsClause().getSuper().myClassDecl().constraints();
	
    public abstract FConstraint Constraint.instantiate();
	
	public FConstraintEq ConstraintEq.instantiate() {
		return new FConstraintEq(getLeft().instantiate(),getRight().instantiate());
	}

	public FConstraintLeq ConstraintLeq.instantiate() {
		return new FConstraintLeq(getLeft().instantiate(),getRight().instantiate());
	}

	
	
	public FConstraintGeq ConstraintGeq.instantiate() {
		return new FConstraintGeq(getLeft().instantiate(),getRight().instantiate());
	}

	public FTimedVariable TimedVariable.instantiate() {
		return new FTimedVariable(new FIdUseInstAccess(new FQName(),getName().newInstAccess()),
		   getArg().instantiate());
	}
	
	
    public void FConstraint.flatten(FQName prefix, FClass fc) {}
	
	public void FConstraintEq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintEq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public void FConstraintLeq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintLeq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public void FConstraintGeq.flatten(FQName prefix, FClass fc) {
		FOptClass foc = (FOptClass)fc;
		foc.addFConstraint(new FConstraintGeq(getLeft().flatten(prefix),getRight().flatten(prefix)));
	}

	public FExp FTimedVariable.flatten(FQName prefix) {
		return new FTimedVariable(getName().flatten(prefix),
		   getArg().flatten(prefix));
	}
	
	syn nta List<FConstraint> InstNode.getFConstraintList()  { 
	    List l = new List();
	    for (Constraint c : constraints()) {
	    	l.add(c.instantiate());
	    }
	    return l;

	}

	syn List<FConstraint> InstNode.getFConstraints() {
		return getFConstraintList();	
	}
	
	
}