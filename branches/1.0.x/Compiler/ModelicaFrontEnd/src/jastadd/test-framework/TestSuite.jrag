/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.io.IOException;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class TestSuite {
	String name;
	private ArrayList<TestCase> l;

	public TestSuite() {
		l = new ArrayList<TestCase>();
	}

	public TestSuite(String fileName, String className) {
		name = className;
		l = new ArrayList<TestCase>();
		ParserHandler ph = new ParserHandler();
		SourceRoot sr = null;
		try {
			sr = ph.parseFile(fileName);
		} catch (Exception e) {
			System.out.println("Error when parsing file: " + fileName);
			System.exit(0);
		}
		String filesep = System.getProperty("file.separator");
		String optionsfile = System.getenv("JMODELICA_HOME")+filesep+"Options"+filesep+"options.xml";
		OptionRegistry or = null;
		try {
			or = new OptionRegistry(optionsfile);
		} catch(XPathExpressionException e) {
			System.out.println("The options XML file could not be loaded.");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		} catch(ParserConfigurationException e) {
			System.out.println("The options XML file could not be loaded.");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		} catch(IOException e) {
			System.out.println("The options XML file could not be loaded.");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		} catch(SAXException e) {
			System.out.println("The options XML file could not be loaded.");
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		
		String modelica_path = System.getenv("JMODELICA_HOME")+filesep+"ThirdParty/MSL";
		or.setStringOption("MODELICAPATH",modelica_path);
		
		sr.options = or;

		sr.collectTestCases(this,className);
	}

	public void dump(StringBuffer str,String indent) {
		str.append(indent+"TestSuite: " + name + "\n");
		for (int i=0;i<l.size();i++) {
			get(i).dump(str,indent+" ");
			str.append("\n");
		}
	}

	public boolean printTests(StringBuffer str) {
		int numFail = 0;
		str.append("TestSuite: " + name + "\n");
		for (int i=0;i<l.size();i++) {
			if (!get(i).printTest(str))
				numFail++;
		}
		str.append("Summary: ");
		if (numFail==0)
			str.append("All tests in test suite passed\n");
		else
			str.append(numFail + " of " + l.size() +" test in test suite failed\n");
		return numFail==0;
	}
	
	public void dumpJunit(String testFile, String dir) {
		StringBuffer str = new StringBuffer();
		str.append("package org.jmodelica.test.junitgenerated;\n\n");
		str.append("import org.junit.*;\n");
		str.append("import static org.junit.Assert.*;\n");
		
		SourceRoot sr = new SourceRoot();
		if (sr.language().equals("Modelica")) {
			str.append("import org.jmodelica.modelica.compiler.*;\n");
		} else if (sr.language().equals("Optimica")) {
			str.append("import org.jmodelica.optimica.compiler.*;\n");	
		}
		str.append("\n");
		str.append("public class " + name + " {\n\n");
		str.append("  static TestSuite ts;\n\n");
		
		str.append("  @BeforeClass public static void setUp() {\n");
		str.append("    ts = new TestSuite(\"" + testFile
				   + "\",\"" + getName() + "\");\n");
		str.append("  }\n\n");
		for (int i=0;i<l.size();i++) {
			get(i).dumpJunit(str,i);
		}
		str.append("  @AfterClass public static void tearDown() {\n");
		str.append("    ts = null;\n");
		str.append("  }\n\n");
		str.append("}\n\n");
		
		File file = new File(dir+"/"+getName()+".java");
		try {
			FileWriter writer = new FileWriter(file);
			writer.append(str.toString());
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void add(TestCase tc) {
		l.add(tc);
	}

	public TestCase get(int i) {
		return l.get(i);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	private static Document parseAndGetDOM(String xmlfile) throws ParserConfigurationException, IOException, SAXException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(true);
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document doc = builder.parse(new File(xmlfile));
		return doc;
	}
}
