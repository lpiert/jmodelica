<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Glossary</title>
<meta name="description" id="description" content="Glossary"/>
<meta name="keywords" id="keywords" content=" ad function of base levels above level Ad type elementary vector operation atomic sequence dependent independent parameter sparsity pattern efficient tape active inactive variable variables taylor coefficient "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_glossary_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="reverse_identity.xml" target="_top">Prev</a>
</td><td><a href="bib.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>Appendix</option>
<option>glossary</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Appendix-&gt;</option>
<option>Faq</option>
<option>speed</option>
<option>Theory</option>
<option>glossary</option>
<option>Bib</option>
<option>Bugs</option>
<option>WishList</option>
<option>whats_new</option>
<option>include_deprecated</option>
<option>License</option>
</select>
</td>
<td>glossary</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>AD Function</option>
<option>AD of Base</option>
<option>AD Levels Above Base</option>
<option>Base Function</option>
<option>Base Type</option>
<option>Elementary Vector</option>
<option>Operation</option>
<option>---..Atomic</option>
<option>---..Sequence</option>
<option>---..Dependent</option>
<option>---..Independent</option>
<option>Parameter</option>
<option>Sparsity Pattern</option>
<option>Tape</option>
<option>---..Active</option>
<option>---..Inactive</option>
<option>---..Independent Variable</option>
<option>---..Dependent Variables</option>
<option>Taylor Coefficient</option>
<option>Variable</option>
</select>
</td>
</tr></table><br/>




<center><b><big><big>Glossary</big></big></b></center>
<br/>
<b><big><a name="AD Function" id="AD Function">AD Function</a></big></b>
<br/>
Given an <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i>
there is a corresponding
AD of <i>Base</i> <a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>
.
This operation sequence
defines a function 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>


where <i>B</i> is the space corresponding to objects of type <i>Base</i>.
We refer to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
</mrow></math>

 as the AD function corresponding to 
the operation sequence and to the object <i>f</i>.
(See the <a href="funcheck.xml#Discussion" target="_top"><span style='white-space: nowrap'>FunCheck&#xA0;discussion</span></a>
 for
possible differences between 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 and the algorithm that defined
the operation sequence.)

<br/>
<br/>
<b><big><a name="AD of Base" id="AD of Base">AD of Base</a></big></b>
<br/>
An object is called an AD of <i>Base</i> object its type is 
either <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> 
(see <a href="default.xml" target="_top"><span style='white-space: nowrap'>default</span></a>
 or 
<a href="ad_copy.xml#Syntax.Constructor" target="_top"><span style='white-space: nowrap'>constructor</span></a>
)
or <code><font color="blue"><span style='white-space: nowrap'>VecAD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;::reference</span></font></code> (see <a href="vecad.xml" target="_top"><span style='white-space: nowrap'>VecAD</span></a>
)
for some <i>Base</i> type.

<br/>
<br/>
<b><big><a name="AD Levels Above Base" id="AD Levels Above Base">AD Levels Above Base</a></big></b>


<br/>
If <i>Base</i> is a type, 
the AD levels above <i>Base</i> 
is the following sequence of types:
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>,</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;AD&lt;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>,</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;AD&lt;&#xA0;AD&lt;&#xA0;AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;&gt;&#xA0;&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>,</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>...</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code><br/>
<b><big><a name="Base Function" id="Base Function">Base Function</a></big></b>
<br/>
A function 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>f</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x02192;</mo>
<mi mathvariant='italic'>B</mi>
</mrow></math>

 
is referred to as a <i>Base</i> function,
if <i>Base</i> is a C++ type that represent elements of
the domain and range space of <i>f</i>; i.e. elements of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>B</mi>
</mrow></math>

.

<br/>
<br/>
<b><big><a name="Base Type" id="Base Type">Base Type</a></big></b>
<br/>
If <i>x</i> is an <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object,
<i>Base</i> is referred to as the base type for <i>x</i>.

<br/>
<br/>
<b><big><a name="Elementary Vector" id="Elementary Vector">Elementary Vector</a></big></b>
<br/>
The <i>j</i>-th elementary vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>e</mi>
<mi mathvariant='italic'>j</mi>
</msup>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 is defined by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msubsup><mi mathvariant='italic'>e</mi>
<mi mathvariant='italic'>i</mi>
<mi mathvariant='italic'>j</mi>
</msubsup>
<mo stretchy="false">=</mo>
<mrow><mo stretchy="true">{</mo><mrow><mtable rowalign="center" ><mtr><mtd columnalign="left" >
<mn>1</mn>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>if</mi>
</mstyle></mrow>
<mspace width='.3em'/>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>j</mi>
</mtd></mtr><mtr><mtd columnalign="left" >
<mn>0</mn>
</mtd><mtd columnalign="left" >
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>otherwise</mi>
</mstyle></mrow>
</mtd></mtr></mtable>
</mrow><mo stretchy="true"> </mo></mrow>
</mrow></math>

<br/>
<b><big><a name="Operation" id="Operation">Operation</a></big></b>


<br/>
<br/>
<b><a name="Operation.Atomic" id="Operation.Atomic">Atomic</a></b>
<br/>
An atomic <i>Type</i> operation is an operation that
has a <i>Type</i> result and is not made up of other
more basic operations.

<br/>
<br/>
<b><a name="Operation.Sequence" id="Operation.Sequence">Sequence</a></b>
<br/>
A sequence of atomic <i>Type</i> operations 
is called a <i>Type</i> operation sequence.
A sequence of atomic <a href="glossary.xml#AD of Base" target="_top"><span style='white-space: nowrap'>AD&#xA0;of&#xA0;Base</span></a>
 operations
is referred to as an AD of <i>Base</i> operation sequence.
The abbreviated notation AD operation sequence is often used
when it is not necessary to specify the base type.

<br/>
<br/>
<b><a name="Operation.Dependent" id="Operation.Dependent">Dependent</a></b>
<br/>
Suppose that <i>x</i> and <i>y</i> are <i>Type</i> objects and
the result of 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&lt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>has type <code><font color="blue">bool</font></code> (where <i>Type</i> is not the same as <code><font color="blue">bool</font></code>).
If one executes the following code
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;if(&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&lt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;)<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;cos(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;else&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;sin(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);&#xA0;<br/>
</span></font></code>the choice above depends on the value of <i>x</i> and <i>y</i>
and the two choices result in a different <i>Type</i> operation sequence.
In this case, we say that the <i>Type</i> operation sequence depends
on <i>x</i> and <i>y</i>.

<br/>
<br/>
<b><a name="Operation.Independent" id="Operation.Independent">Independent</a></b>
<br/>
Suppose that <i>i</i> and <i>n</i> are <code><font color="blue">size_t</font></code> objects,
and <code><font color="blue"></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>]</span></font></code>, <i>y</i> are <i>Type</i> objects,
where <i>Type</i> is different from <code><font color="blue">size_t</font></code>.
The <i>Type</i> sequence of operations corresponding to
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>Type</span></i><code><font color="blue"><span style='white-space: nowrap'>(0);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;for(</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&lt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>;&#xA0;</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>++)<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;+=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[</span></font></code><i><span style='white-space: nowrap'>i</span></i><code><font color="blue"><span style='white-space: nowrap'>];<br/>
</span></font></code>does not depend on the value of <i>x</i> or <i>y</i>.
In this case, we say that the <i>Type</i> operation sequence 
is independent of <i>y</i> and the elements of <i>x</i>.

<br/>
<br/>
<b><big><a name="Parameter" id="Parameter">Parameter</a></big></b>
<br/>
All <i>Base</i> objects are parameters.
An <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object <i>u</i> is currently a parameter if
its value does not depend on the value of 
an <a href="independent.xml" target="_top"><span style='white-space: nowrap'>Independent</span></a>
 variable vector for an
<a href="glossary.xml#Tape.Active" target="_top"><span style='white-space: nowrap'>active&#xA0;tape</span></a>
.
If <i>u</i> is a parameter, the function 
<a href="parvar.xml" target="_top"><span style='white-space: nowrap'>Parameter(u)</span></a>
 returns true
and <a href="parvar.xml" target="_top"><span style='white-space: nowrap'>Variable(u)</span></a>
 returns false.

<br/>
<br/>
<b><big><a name="Sparsity Pattern" id="Sparsity Pattern">Sparsity Pattern</a></big></b>



<br/>
Given a matrix 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mrow><mi mathvariant='italic'>m</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow>
</msup>
</mrow></math>

,
a boolean valued 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">&#x000D7;</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 matrix 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>P</mi>
</mrow></math>

 is a 
sparsity pattern for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>A</mi>
</mrow></math>

 if 
for 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mn>-1</mn>
</mrow></math>

 and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mi mathvariant='italic'>n</mi>
<mn>-1</mn>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>A</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">&#x02260;</mo>
<mn>0</mn>
<mspace width='.3em'/>
<mo stretchy="false">&#x021D2;</mo>
<mspace width='.3em'/>
<msub><mi mathvariant='italic'>P</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">=</mo>
<mrow><mstyle mathvariant='normal'><mi mathvariant='normal'>true</mi>
</mstyle></mrow>
</mrow></math>

Given two sparsity patterns 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>P</mi>
</mrow></math>

 and <i>Q</i> 
for a matrix <i>A</i>, we say that <i>P</i> is more efficient than
<i>Q</i> if <i>P</i> has fewer true elements than <i>Q</i>.

<br/>
<br/>
<b><big><a name="Tape" id="Tape">Tape</a></big></b>


<br/>
<br/>
<b><a name="Tape.Active" id="Tape.Active">Active</a></b>
<br/>
A new tape is created and becomes active 
after each call of the form (see <a href="independent.xml" target="_top"><span style='white-space: nowrap'>Independent</span></a>
)
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>All operations that depend on the elements of <i>x</i> are
recorded on this active tape.

<br/>
<br/>
<b><a name="Tape.Inactive" id="Tape.Inactive">Inactive</a></b>
<br/>
The
<a href="glossary.xml#Operation.Sequence" target="_top"><span style='white-space: nowrap'>operation&#xA0;sequence</span></a>

stored in a tape must be transferred to a function object using the syntax
(see <a href="funconstruct.xml" target="_top"><span style='white-space: nowrap'>ADFun&lt;Base&gt;&#xA0;f(x,&#xA0;y)</span></a>
)
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>(&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>or using the syntax (see <a href="dependent.xml" target="_top"><span style='white-space: nowrap'>f.Dependent(x,&#xA0;y)</span></a>
)
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Dependent(&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>After such a transfer, the tape becomes inactive.

<br/>
<br/>
<b><a name="Tape.Independent Variable" id="Tape.Independent Variable">Independent Variable</a></b>
<br/>
While the tape is active, we refer to the elements of <i>x</i> 
as the independent variables for the tape.
When the tape becomes inactive,
the corresponding objects become 
<a href="glossary.xml#Parameter" target="_top"><span style='white-space: nowrap'>parameters</span></a>
.

<br/>
<br/>
<b><a name="Tape.Dependent Variables" id="Tape.Dependent Variables">Dependent Variables</a></b>
<br/>
While the tape is active, we use the term dependent variables for the tape
for any objects whose value depends on the independent variables for the tape.
When the tape becomes inactive,
the corresponding objects become 
<a href="glossary.xml#Parameter" target="_top"><span style='white-space: nowrap'>parameters</span></a>
.

 
<br/>
<br/>
<b><big><a name="Taylor Coefficient" id="Taylor Coefficient">Taylor Coefficient</a></big></b>
<br/>
Suppose 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">:</mo>
<mi mathvariant='italic'>B</mi>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

 is a 
is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
</mrow></math>

 times continuously differentiable function
in some neighborhood of zero.
For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
</mrow></math>

, 
we use the column vector 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">&#x02208;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
</mrow></math>

 for the <i>k</i>-th order 
Taylor coefficient corresponding to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>X</mi>
</mrow></math>

 
which is defined by

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>k</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">=</mo>
<mfrac><mrow><mn>1</mn>
</mrow>
<mrow><mi mathvariant='italic'>k</mi>
<mo stretchy="false">!</mo>
</mrow>
</mfrac>
<mfrac><mrow><msup><mo stretchy="false">&#x02202;</mo>
<mrow><mi mathvariant='italic'>k</mi>
</mrow>
</msup>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msup><mrow><mi mathvariant='italic'>t</mi>
</mrow>
<mrow><mi mathvariant='italic'>k</mi>
</mrow>
</msup>
</mrow>
</mfrac>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow></math>

It follows that 

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>0</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">+</mo>
<mo stretchy="false">&#x022EF;</mo>
<mo stretchy="false">+</mo>
<msup><mi mathvariant='italic'>x</mi>
<mrow><mo stretchy="false">(</mo>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>p</mi>
</msup>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>

where the remainder 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>R</mi>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>t</mi>
<mo stretchy="false">)</mo>
</mrow></math>

 divided by 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<msup><mi mathvariant='italic'>t</mi>
<mi mathvariant='italic'>p</mi>
</msup>
</mrow></math>

 
converges to zero and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>t</mi>
</mrow></math>

 goes to zero.


<br/>
<br/>
<b><big><a name="Variable" id="Variable">Variable</a></big></b>
<br/>
An <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object <i>u</i> is a variable if
its value depends on an independent variable vector for
a currently <a href="glossary.xml#Tape.Active" target="_top"><span style='white-space: nowrap'>active&#xA0;tape</span></a>
.
If <i>u</i> is a variable,
<a href="parvar.xml" target="_top"><span style='white-space: nowrap'>Variable(u)</span></a>
 returns true and 
<a href="parvar.xml" target="_top"><span style='white-space: nowrap'>Parameter(u)</span></a>
 returns false. 
For example,
directly after the code sequence
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;double&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>u</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[0];<br/>
</span></font></code>the <code><font color="blue"><span style='white-space: nowrap'>AD&lt;double&gt;</span></font></code> object <i>u</i> is currently a variable.
Directly after the code sequence
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;Independent(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>);<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;AD&lt;double&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>u</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>[0];<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>u</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;5;<br/>
</span></font></code><i>u</i>  is currently a <a href="glossary.xml#Parameter" target="_top"><span style='white-space: nowrap'>parameter</span></a>

(not a variable).
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>Note that we often drop the word currently and 
just refer to an <code><font color="blue"><span style='white-space: nowrap'>AD&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;</span></font></code> object as a variable
or parameter.


<hr/>Input File: omh/glossary.omh

</body>
</html>
