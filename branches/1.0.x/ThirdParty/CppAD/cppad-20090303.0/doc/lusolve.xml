<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Compute Determinant and Solve Linear Equations</title>
<meta name="description" id="description" content="Compute Determinant and Solve Linear Equations"/>
<meta name="keywords" id="keywords" content=" Lusolve linear equation determinant Lu solve "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_lusolve_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="ludetandsolve.xml" target="_top">Prev</a>
</td><td><a href="lusolve.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>library</option>
<option>LuDetAndSolve</option>
<option>LuSolve</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>library-&gt;</option>
<option>ErrorHandler</option>
<option>NearEqual</option>
<option>speed_test</option>
<option>SpeedTest</option>
<option>NumericType</option>
<option>CheckNumericType</option>
<option>SimpleVector</option>
<option>CheckSimpleVector</option>
<option>nan</option>
<option>pow_int</option>
<option>Poly</option>
<option>LuDetAndSolve</option>
<option>RombergOne</option>
<option>RombergMul</option>
<option>Runge45</option>
<option>Rosen34</option>
<option>OdeErrControl</option>
<option>OdeGear</option>
<option>OdeGearControl</option>
<option>BenderQuad</option>
<option>LuRatio</option>
<option>std_math_unary</option>
<option>CppAD_vector</option>
<option>TrackNewDel</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>LuDetAndSolve-&gt;</option>
<option>LuSolve</option>
<option>LuFactor</option>
<option>LuInvert</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>LuSolve-&gt;</option>
<option>LuSolve.cpp</option>
<option>lu_solve.hpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Description</option>
<option>Include</option>
<option>Factor and Invert</option>
<option>Matrix Storage</option>
<option>signdet</option>
<option>n</option>
<option>m</option>
<option>A</option>
<option>B</option>
<option>X</option>
<option>logdet</option>
<option>Float</option>
<option>FloatVector</option>
<option>LeqZero</option>
<option>AbsGeq</option>
<option>Example</option>
<option>Source</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>Compute Determinant and Solve Linear Equations</big></big></b></center>
<code><span style='white-space: nowrap'><br/>
</span></code><b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>
 <code><font color="blue"><br/>
# include &lt;cppad/lu_solve.hpp&gt;</font></code>
<code><span style='white-space: nowrap'><br/>
</span></code><code><font color="blue"></font></code><i><span style='white-space: nowrap'>signdet</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;LuSolve(</span></font></code><i><span style='white-space: nowrap'>n</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>m</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>A</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>B</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>X</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;</span></font></code><i><span style='white-space: nowrap'>logdet</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Description" id="Description">Description</a></big></b>
<br/>
Use an LU factorization of the matrix <i>A</i> to
compute its determinant 
and solve for <i>X</i> in the linear of equation

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>A</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>X</mi>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>B</mi>
</mrow></math>

where <i>A</i> is an 
<i>n</i> by <i>n</i> matrix,
<i>X</i> is an 
<i>n</i> by <i>m</i> matrix, and
<i>B</i> is an 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>m</mi>
</mrow></math>

 matrix.

<br/>
<br/>
<b><big><a name="Include" id="Include">Include</a></big></b>
<br/>
The file <code><font color="blue">cppad/lu_solve.hpp</font></code> is included by <code><font color="blue">cppad/cppad.hpp</font></code>
but it can also be included separately with out the rest of 
the <code><font color="blue">CppAD</font></code> routines.

<br/>
<br/>
<b><big><a name="Factor and Invert" id="Factor and Invert">Factor and Invert</a></big></b>
<br/>
This routine is an easy to user interface to
<a href="lufactor.xml" target="_top"><span style='white-space: nowrap'>LuFactor</span></a>
 and <a href="luinvert.xml" target="_top"><span style='white-space: nowrap'>LuInvert</span></a>
 for computing determinants and
solutions of linear equations.
These separate routines should be used if
one right hand side <i>B</i>
depends on the solution corresponding to another
right hand side (with the same value of <i>A</i>).
In this case only one call to <code><font color="blue">LuFactor</font></code> is required
but there will be multiple calls to <code><font color="blue">LuInvert</font></code>.


<br/>
<br/>
<b><big><a name="Matrix Storage" id="Matrix Storage">Matrix Storage</a></big></b>
<br/>
All matrices are stored in row major order.
To be specific, if 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
</mrow></math>

 is a vector
that contains a 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
</mrow></math>

 by 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>q</mi>
</mrow></math>

 matrix,
the size of 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>Y</mi>
</mrow></math>

 must be equal to 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>p</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
</mrow></math>

 and for

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>p</mi>
<mn>-1</mn>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>q</mi>
<mn>-1</mn>
</mrow></math>

,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<msub><mi mathvariant='italic'>Y</mi>
<mrow><mi mathvariant='italic'>i</mi>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>j</mi>
</mrow>
</msub>
<mo stretchy="false">=</mo>
<mi mathvariant='italic'>Y</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>q</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
</mrow></math>

<br/>
<b><big><a name="signdet" id="signdet">signdet</a></big></b>
<br/>
The return value <i>signdet</i> is a <code><font color="blue">int</font></code> value
that specifies the sign factor for the determinant of <i>A</i>.
This determinant of <i>A</i> is zero if and only if <i>signdet</i>
is zero. 

<br/>
<br/>
<b><big><a name="n" id="n">n</a></big></b>
<br/>
The argument <i>n</i> has type <code><font color="blue">size_t</font></code> 
and specifies the number of rows in the matrices
<i>A</i>,
<i>X</i>,
and <i>B</i>.
The number of columns in <i>A</i> is also equal to <i>n</i>.

<br/>
<br/>
<b><big><a name="m" id="m">m</a></big></b>
<br/>
The argument <i>m</i> has type <code><font color="blue">size_t</font></code> 
and specifies the number of columns in the matrices
<i>X</i>
and <i>B</i>.
If <i>m</i> is zero,
only the determinant of <i>A</i> is computed and
the matrices <i>X</i> and <i>B</i> are not used.

<br/>
<br/>
<b><big><a name="A" id="A">A</a></big></b>
<br/>
The argument <i>A</i> has the prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>FloatVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>A</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and the size of <i>A</i> must equal 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>


(see description of <a href="lusolve.xml#FloatVector" target="_top"><span style='white-space: nowrap'>FloatVector</span></a>
 below).
This is the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 by <i>n</i> matrix that 
we are computing the determinant of 
and that defines the linear equation.

<br/>
<br/>
<b><big><a name="B" id="B">B</a></big></b>
<br/>
The argument <i>B</i> has the prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>FloatVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>B</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and the size of <i>B</i> must equal 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>


(see description of <a href="lusolve.xml#FloatVector" target="_top"><span style='white-space: nowrap'>FloatVector</span></a>
 below).
This is the 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
</mrow></math>

 by <i>m</i> matrix that 
defines the right hand side of the linear equations.
If <i>m</i> is zero, <i>B</i> is not used.

<br/>
<br/>
<b><big><a name="X" id="X">X</a></big></b>
<br/>
The argument <i>X</i> has the prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>FloatVector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>X</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>and the size of <i>X</i> must equal 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>m</mi>
</mrow></math>


(see description of <a href="lusolve.xml#FloatVector" target="_top"><span style='white-space: nowrap'>FloatVector</span></a>
 below).
The input value of <i>X</i> does not matter.
On output, the elements of <i>X</i> contain the solution
of the equation we wish to solve
(unless <i>signdet</i> is equal to zero).
If <i>m</i> is zero, <i>X</i> is not used.

<br/>
<br/>
<b><big><a name="logdet" id="logdet">logdet</a></big></b>
<br/>
The argument <i>logdet</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>logdet</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>On input, the value of <i>logdet</i> does not matter.
On output, it has been set to the 
log of the determinant of <i>A</i> 
(but not quite).
To be more specific,
the determinant of <i>A</i> is given by the formula
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>det</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>signdet</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;*&#xA0;exp(&#xA0;</span></font></code><i><span style='white-space: nowrap'>logdet</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;)<br/>
</span></font></code>This enables <code><font color="blue">LuSolve</font></code> to use logs of absolute values
in the case where <i>Float</i> corresponds to a real number.

<br/>
<br/>
<b><big><a name="Float" id="Float">Float</a></big></b>
<br/>
The type <i>Float</i> must satisfy the conditions
for a <a href="numerictype.xml" target="_top"><span style='white-space: nowrap'>NumericType</span></a>
 type.
The routine <a href="checknumerictype.xml" target="_top"><span style='white-space: nowrap'>CheckNumericType</span></a>
 will generate an error message
if this is not the case.
In addition, the following operations must be defined for any pair
of <i>Float</i> objects <i>x</i> and <i>y</i>:

<table><tr><td align='left'  valign='top'>

<b>Operation</b> </td><td align='left'  valign='top'>
 <b>Description</b>  </td></tr><tr><td align='left'  valign='top'>

<code><font color="blue"><span style='white-space: nowrap'>log(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code> </td><td align='left'  valign='top'>

	returns the logarithm of <i>x</i> as a <i>Float</i> object
</td></tr>
</table>
<br/>
<b><big><a name="FloatVector" id="FloatVector">FloatVector</a></big></b>
<br/>
The type <i>FloatVector</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type&#xA0;Float</span></a>
.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="LeqZero" id="LeqZero">LeqZero</a></big></b>
<br/>
Including the file <code><font color="blue">lu_solve.hpp</font></code> defines the template function 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;template&#xA0;&lt;typename&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;LeqZero&lt;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;(const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>in the <code><font color="blue">CppAD</font></code> namespace.
This function returns true if <i>x</i> is less than or equal to zero
and false otherwise.
It is used by <code><font color="blue">LuSolve</font></code> to avoid taking the log of
zero (or a negative number if <i>Float</i> corresponds to real numbers).
This template function definition assumes that the operator 
<code><font color="blue">&lt;=</font></code> is defined for <i>Float</i> objects. 
If this operator is not defined for your use of <i>Float</i>,
you will need to specialize this template so that it works for your
use of <code><font color="blue">LuSolve</font></code>.
<code><span style='white-space: nowrap'><br/>
<br/>
</span></code>Complex numbers do not have the operation or <code><font color="blue">&lt;=</font></code> defined.
In addition, in the complex case, 
one can take the log of a negative number.
The specializations
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;LeqZero&lt;&#xA0;std::complex&lt;float&gt;&#xA0;&gt;&#xA0;(const&#xA0;std::complex&lt;float&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;LeqZero&lt;&#xA0;std::complex&lt;double&gt;&#xA0;&gt;(const&#xA0;std::complex&lt;double&gt;&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>are defined by including <code><font color="blue">lu_solve.hpp</font></code>.
These return true if <i>x</i> is zero and false otherwise.

<br/>
<br/>
<b><big><a name="AbsGeq" id="AbsGeq">AbsGeq</a></big></b>
<br/>
Including the file <code><font color="blue">lu_solve.hpp</font></code> defines the template function 
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;template&#xA0;&lt;typename&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;<br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;bool&#xA0;AbsGeq&lt;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;(const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>,&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Float</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>y</span></i><code><font color="blue"><span style='white-space: nowrap'>)<br/>
</span></font></code>If the type <i>Float</i> does not support the <code><font color="blue">&lt;=</font></code> operation
and it is not <code><font color="blue">std::complex&lt;float&gt;</font></code> or <code><font color="blue">std::complex&lt;double&gt;</font></code>,
see the documentation for <code><font color="blue">AbsGeq</font></code> in <a href="lufactor.xml#AbsGeq" target="_top"><span style='white-space: nowrap'>LuFactor</span></a>
. 


<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>
<br/>
The file 
<a href="lusolve.cpp.xml" target="_top"><span style='white-space: nowrap'>LuSolve.cpp</span></a>

contains an example and test of using this routine.
It returns true if it succeeds and false otherwise.

<br/>
<br/>
<b><big><a name="Source" id="Source">Source</a></big></b>
<br/>
The file <a href="lu_solve.hpp.xml" target="_top"><span style='white-space: nowrap'>lu_solve.hpp</span></a>
 contains the
current source code that implements these specifications.


<hr/>Input File: cppad/lu_solve.hpp

</body>
</html>
