<?xml version='1.0'?>
<?xml-stylesheet type='text/xsl' href='pmathml.xsl'?>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<title>Jacobian: Driver Routine</title>
<meta name="description" id="description" content="Jacobian: Driver Routine"/>
<meta name="keywords" id="keywords" content=" Jacobian driver first derivative "/>
<style type='text/css'>
body { color : black }
body { background-color : white }
A:link { color : blue }
A:visited { color : purple }
A:active { color : purple }
</style>
<script type='text/javascript' language='JavaScript' src='_jacobian_xml.js'>
</script>
</head>
<body>
<table><tr>
<td>
<a href="http://www.coin-or.org/CppAD/" target="_top"><img border="0" src="_image.gif"/></a>
</td>
<td><a href="drivers.xml" target="_top">Prev</a>
</td><td><a href="jacobian.cpp.xml" target="_top">Next</a>
</td><td>
<select onchange='choose_across0(this)'>
<option>Index-&gt;</option>
<option>contents</option>
<option>reference</option>
<option>index</option>
<option>search</option>
<option>external</option>
</select>
</td>
<td>
<select onchange='choose_up0(this)'>
<option>Up-&gt;</option>
<option>CppAD</option>
<option>ADFun</option>
<option>Drivers</option>
<option>Jacobian</option>
</select>
</td>
<td>
<select onchange='choose_down3(this)'>
<option>CppAD-&gt;</option>
<option>Install</option>
<option>Introduction</option>
<option>AD</option>
<option>ADFun</option>
<option>library</option>
<option>preprocessor</option>
<option>Example</option>
<option>Appendix</option>
</select>
</td>
<td>
<select onchange='choose_down2(this)'>
<option>ADFun-&gt;</option>
<option>Independent</option>
<option>FunConstruct</option>
<option>Dependent</option>
<option>abort_recording</option>
<option>SeqProperty</option>
<option>FunEval</option>
<option>Drivers</option>
<option>FunCheck</option>
<option>omp_max_thread</option>
<option>FunDeprecated</option>
</select>
</td>
<td>
<select onchange='choose_down1(this)'>
<option>Drivers-&gt;</option>
<option>Jacobian</option>
<option>ForOne</option>
<option>RevOne</option>
<option>Hessian</option>
<option>ForTwo</option>
<option>RevTwo</option>
<option>sparse_jacobian</option>
<option>sparse_hessian</option>
</select>
</td>
<td>
<select onchange='choose_down0(this)'>
<option>Jacobian-&gt;</option>
<option>Jacobian.cpp</option>
</select>
</td>
<td>
<select onchange='choose_current0(this)'>
<option>Headings-&gt;</option>
<option>Syntax</option>
<option>Purpose</option>
<option>f</option>
<option>x</option>
<option>jac</option>
<option>Vector</option>
<option>Forward or Reverse</option>
<option>Example</option>
</select>
</td>
</tr></table><br/>
<center><b><big><big>Jacobian: Driver Routine</big></big></b></center>
<br/>
<b><big><a name="Syntax" id="Syntax">Syntax</a></big></b>

<br/>
<code><font color="blue"></font></code><i><span style='white-space: nowrap'>jac</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;=&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'>.Jacobian(</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'>)</span></font></code>


<br/>
<br/>
<b><big><a name="Purpose" id="Purpose">Purpose</a></big></b>
<br/>
We use 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>F</mi>
<mo stretchy="false">:</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>n</mi>
</msup>
<mo stretchy="false">&#x02192;</mo>
<msup><mi mathvariant='italic'>B</mi>
<mi mathvariant='italic'>m</mi>
</msup>
</mrow></math>

 to denote the
<a href="glossary.xml#AD Function" target="_top"><span style='white-space: nowrap'>AD&#xA0;function</span></a>
 corresponding to <i>f</i>.
The syntax above sets <i>jac</i> to the
Jacobian of <i>F</i> evaluated at <i>x</i>; i.e.,

<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mi mathvariant='italic'>jac</mi>
<mo stretchy="false">=</mo>
<msup><mi mathvariant='italic'>F</mi>
<mrow><mo stretchy="false">(</mo>
<mn>1</mn>
<mo stretchy="false">)</mo>
</mrow>
</msup>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="f" id="f">f</a></big></b>
<br/>
The object <i>f</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;ADFun&lt;</span></font></code><i><span style='white-space: nowrap'>Base</span></i><code><font color="blue"><span style='white-space: nowrap'>&gt;&#xA0;</span></font></code><i><span style='white-space: nowrap'>f</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>Note that the <a href="adfun.xml" target="_top"><span style='white-space: nowrap'>ADFun</span></a>
 object <i>f</i> is not <code><font color="blue">const</font></code>
(see <a href="jacobian.xml#Forward or Reverse" target="_top"><span style='white-space: nowrap'>Forward&#xA0;or&#xA0;Reverse</span></a>
 below).

<br/>
<br/>
<b><big><a name="x" id="x">x</a></big></b>
<br/>
The argument <i>x</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;const&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;&amp;</span></font></code><i><span style='white-space: nowrap'>x</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="jacobian.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size 
must be equal to <i>n</i>, the dimension of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>
 space for <i>f</i>.
It specifies
that point at which to evaluate the Jacobian.

<br/>
<br/>
<b><big><a name="jac" id="jac">jac</a></big></b>
<br/>
The result <i>jac</i> has prototype
<code><font color="blue"><span style='white-space: nowrap'><br/>
&#xA0;&#xA0;&#xA0;&#xA0;&#xA0;</span></font></code><i><span style='white-space: nowrap'>Vector</span></i><code><font color="blue"><span style='white-space: nowrap'>&#xA0;</span></font></code><i><span style='white-space: nowrap'>jac</span></i><code><font color="blue"><span style='white-space: nowrap'><br/>
</span></font></code>(see <a href="jacobian.xml#Vector" target="_top"><span style='white-space: nowrap'>Vector</span></a>
 below)
and its size is 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
</mrow></math>

; i.e., the product of the
<a href="seqproperty.xml#Domain" target="_top"><span style='white-space: nowrap'>domain</span></a>

and
<a href="seqproperty.xml#Range" target="_top"><span style='white-space: nowrap'>range</span></a>

dimensions for <i>f</i>.
For 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>m</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>

 
and 
<math xmlns="http://www.w3.org/1998/Math/MathML" display="inline"><mrow>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">=</mo>
<mn>0</mn>
<mo stretchy="false">,</mo>
<mo stretchy="false">&#x02026;</mo>
<mo stretchy="false">,</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">-</mo>
<mn>1</mn>
</mrow></math>



<math xmlns="http://www.w3.org/1998/Math/MathML" display="block"><mrow>
<mo stretchy="false">.</mo>
<mi mathvariant='italic'>jac</mi>
<mo stretchy="false">[</mo>
<mi mathvariant='italic'>i</mi>
<mo stretchy="false">*</mo>
<mi mathvariant='italic'>n</mi>
<mo stretchy="false">+</mo>
<mi mathvariant='italic'>j</mi>
<mo stretchy="false">]</mo>
<mo stretchy="false">=</mo>
<mfrac><mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>F</mi>
<mi mathvariant='italic'>i</mi>
</msub>
</mrow>
<mrow><mo stretchy="false">&#x02202;</mo>
<msub><mi mathvariant='italic'>x</mi>
<mi mathvariant='italic'>j</mi>
</msub>
</mrow>
</mfrac>
<mo stretchy="false">(</mo>
<mi mathvariant='italic'>x</mi>
<mo stretchy="false">)</mo>
</mrow></math>

<br/>
<b><big><a name="Vector" id="Vector">Vector</a></big></b>
<br/>
The type <i>Vector</i> must be a <a href="simplevector.xml" target="_top"><span style='white-space: nowrap'>SimpleVector</span></a>
 class with
<a href="simplevector.xml#Elements of Specified Type" target="_top"><span style='white-space: nowrap'>elements&#xA0;of&#xA0;type</span></a>

<i>Base</i>.
The routine <a href="checksimplevector.xml" target="_top"><span style='white-space: nowrap'>CheckSimpleVector</span></a>
 will generate an error message
if this is not the case.

<br/>
<br/>
<b><big><a name="Forward or Reverse" id="Forward or Reverse">Forward or Reverse</a></big></b>
<br/>
This will use order zero Forward mode and either
order one Forward or order one Reverse to compute the Jacobian
(depending on which it estimates will require less work).
After each call to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
,
the object <i>f</i> contains the corresponding 
<a href="glossary.xml#Taylor Coefficient" target="_top"><span style='white-space: nowrap'>Taylor&#xA0;coefficients</span></a>
.
After each call to <code><font color="blue">Jacobian</font></code>,
the previous calls to <a href="forward.xml" target="_top"><span style='white-space: nowrap'>Forward</span></a>
 are unspecified.

<br/>
<br/>
<b><big><a name="Example" id="Example">Example</a></big></b>

<br/>
The routine 
<a href="jacobian.cpp.xml" target="_top"><span style='white-space: nowrap'>Jacobian</span></a>
 is both an example and test.
It returns <code><font color="blue">true</font></code>, if it succeeds and <code><font color="blue">false</font></code> otherwise.


<hr/>Input File: cppad/local/jacobian.hpp

</body>
</html>
