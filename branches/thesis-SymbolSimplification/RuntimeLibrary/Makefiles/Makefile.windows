
#    Copyright (C) 2009 Modelon AB
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License version 3 as published 
#    by the Free Software Foundation, or optionally, under the terms of the 
#    Common Public License version 1.0 as published by IBM.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License, or the Common Public License, for more details.
#
#    You should have received copies of the GNU General Public License
#    and the Common Public License along with this program.  If not, 
#    see <http://www.gnu.org/licenses/> or 
#    <http://www.ibm.com/developerworks/library/os-cpl.html/> respectively.

#
# NOTICE: This make file is intended to be run from the standard Windows
# command prompt cmd.exe. Also, the MinGW bin directory needs to be in PATH. 
#
# Usage:
#   This makefile is intended to be used by the code generated by the
#   JModelica compilers. The following arguments are supported:
#   
#   FILE_NAME             (mandatory) The name of the C file.
#   PLATFORM              (mandatory) The platform we are compiling for, 
#                         currently only used to decide where to place binary.
#                         Should be win32 or win64.
#   JMODELICA_HOME        (mandatory) The directory where JModelica has been built,
#                         or equivalently, the directory of a binary JModelica
#                         distribution.
#   CPPAD_HOME            (mandatory) The directory of CppAD.
#   IPOPT_HOME            (mandatory if BUILD_WITH_ALGORITHMS=true) An IPOPT build
#                         directory.
#   SUNDIALS_HOME         (mandatory) The directory of Sundials.
#   EXT_LIB_DIRS          List of external library directories.
#   EXT_LIBS              List of external libraries.
#   EXT_INC_DIRS          List of external include directories.
#   EXTRA_CFLAGS          Extra CFLAGS for specific files
#                         Format is (with e1.c and e2.c) "e1:O2:pedantic e2:O1"
#
# The following targets are supported:
#    jmu          Build a shared object file including the low level JMI interface 
#                 (contained in jmi.a), JMI algorithms and the Ipopt algorithm.
#    fmume10      Build a shared object file containing the generated code and the
#                 Model Exchange for API FMI 1.0.
#    fmucs10      Build a shared object file containing the generated code and the
#                 co-simulation API for FMI 1.0.
#    fmume20      Build a shared object file containing the generated code and the
#                 Model Exchange for API FMI 2.0.
#    fmucs20      Build a shared object file containing the generated code and the
#                 co-simulation API for FMI 2.0.
#    fmumecs20    Build a shared object file containing the generated code and the
#                 Model Exchange API and co-simulation API for FMI 2.0.
#
# Example:
#  make -f Makefile.windows ipopt
#       FILE_NAME=CCodeGenTests.CCodeGenTest2 \
#       JMODELICA_HOME=/c/jmodelica_install \
#       CPPAD_HOME=/c/CppAD-20090303.0/ \
#       IPOPT_HOME=/c/Ipopt-3.5.5/build \
#
#
#
# Make sure that cmd.exe is used, even if cygwin or mingw-sh are on the path
SHELL = cmd.exe

# Name of shared library
FILE_NAME = 

# Extra CFLAGS for specific source files.
EXTRA_CFLAGS =

# Platform name - controls what sub-directory the binary is placed in
PLATFORM = 

# Platform specific compiler flag
PLATFORM_FLAG = -m32
PLATFORM_64 = -m64

# JModelica home
JMODELICA_HOME = 

# CPPAD home
CPPAD_HOME = 

# IPOPT home
IPOPT_HOME = 

# SUNDIALS home
SUNDIALS_HOME = 

MODULE_HOME =

# STATIC PTHREADS home
STATIC_PTHREADS_HOME = $(JMODELICA_HOME)/ThirdParty/pthreads

# Additional library directories
EXT_LIB_DIRS =

# Additional libraries
# The compiler is assumed to always include lapack since some runtime
# configurations depends on it. Our lapack library is located in the
# runtime library directory and depends on blas and gfortran. Blas 
# should also be included from the runtime library directory. Gfortran
# is found in the mingw configuration and we need to specify that we
# want the static version.
EXT_LIBS = 
EXTERNAL_LIBS = $(EXT_LIBS:%=-l%) -llapack -lblas -l:libgfortran.a

# Additional include directories
EXT_INC_DIRS =

# Build with algorithms (requires IPOPT)
BUILD_WITH_ALGORITHMS = false

# Directory to place binary in
BINARY_BASE_DIR = binaries
BINARY_DIR = $(BINARY_BASE_DIR)/$(PLATFORM)
# And with \ to make the cmd version of mkdir happy
BINARY_MKDIR = $(BINARY_BASE_DIR)\$(PLATFORM)

SHARED = $(BINARY_DIR)/$(FILE_NAME).dll

EXECUTABLE = $(BINARY_BASE_DIR)/$(FILE_NAME).exe

# Object files to be included in the shared library
OBJS = $(patsubst sources/%.c, %.o, $(wildcard sources/*.c)) $(patsubst sources/%.cpp, %.o, $(wildcard sources/*.cpp))

# C++ Compiler command
CXX = g++

# C Compiler command
CC = gcc

# ar command
AR = ar

# Command to remove temporary files
RM = del

# Compile with or without CppAD
JMI_AD=JMI_AD_CPPAD

# Flags for which FMU type, me, cs or me+cs.
FMU_TYPES =

# Extra compiler options
EXT_OPTION = 

# C++ Compiler options
CXXFLAGS = $(PLATFORM_FLAG) $(EXT_OPTION) -g -DJMI_AD=${JMI_AD}

# C Compiler options
CFLAGS = $(PLATFORM_FLAG) $(EXT_OPTION) -g -std=c89 -pedantic -DJMI_AD=$(JMI_AD) ${FMU_TYPES} -msse2 -mfpmath=sse

SHARED_LDFLAGS = $(PLATFORM_FLAG) -shared

# Directories with header files
JMI_INC_DIR = ${JMODELICA_HOME}/include
RUNTIMELIBRARY_INC_DIR = ${JMODELICA_HOME}/include/RuntimeLibrary
STANDARD_HEADER_INC_DIR = ${JMODELICA_HOME}/ThirdParty/FMI/2.0
CPPAD_INC_DIR = $(CPPAD_HOME)
SUNDIALS_INC_DIR = $(SUNDIALS_HOME)/include

# Directories with libraries
JMI_LIB_DIR = ${JMODELICA_HOME}/lib
RUNTIMELIBRARY_LIB_DIR = ${JMODELICA_HOME}/lib/RuntimeLibrary
RUNTIMELIBRARY_LIB_DIR64 = ${JMODELICA_HOME}/lib/RuntimeLibrary64
MINPACK_LIB_DIR = ${JMODELICA_HOME}/ThirdParty/Minpack/lib
MINPACK_LIB_DIR64 = ${JMODELICA_HOME}/ThirdParty/Minpack/lib64
SUNDIALS_LIB_DIR = $(SUNDIALS_HOME)/lib
SUNDIALS_LIB_DIR64 = $(SUNDIALS_HOME)/lib64
MODULE_LIB_DIR = $(MODULE_HOME)

# Flags needed for specific libs
ifdef IPOPT_HOME
LIB_FLAGS_IPOPT := $(shell set "PKG_CONFIG_PATH=$(IPOPT_HOME)/lib/pkgconfig" && pkg-config --libs ipopt)
else
LIB_FLAGS_IPOPT = 
endif
LIB_FLAGS_IPOPT := $(shell set "PKG_CONFIG_PATH=$(IPOPT_HOME)/lib/pkgconfig" && pkg-config --libs ipopt)
LIB_MINPACK  = "-L$(MINPACK_LIB_DIR)" -l:libcminpack.a
LIB_SUNDIALS = "-L$(SUNDIALS_LIB_DIR)" -l:libsundials_kinsol.a -l:libsundials_nvecserial.a -l:libsundials_cvode.a
LIB_MODULE = "-L$(MODULE_LIB_DIR)" -l:libjmi_get_set_lazy.a
STATIC_PTHREADS_CONT = $(shell dir "$(STATIC_PTHREADS_HOME)")
ifneq ($(findstring ..,$(STATIC_PTHREADS_CONT)),)
LIB_PTHREADS = "-L$(STATIC_PTHREADS_HOME)/lib" -lpthreadGC2-2-9-1
else
LIB_PTHREADS =
endif

# Libraries necessary to link with jmi
LIBS_FMU_STD = -ljmi "-L$(JMI_LIB_DIR)" $(EXT_LIB_DIRS) $(EXTERNAL_LIBS) -lModelicaExternalC -lstdc++ -static-libstdc++
LIBS_FMU_ALL = $(LIBS_FMU_STD) $(LIB_SUNDIALS) $(LIB_MINPACK) $(LIB_PTHREADS) $(LIB_MODULE)
LIBS_FMUME10 = -lfmi1_me           $(LIBS_FMU_ALL)
LIBS_FMUCS10 = -lfmi1_cs -lfmi1_me $(LIBS_FMU_ALL)
LIBS_FMU20   = -lfmi2              $(LIBS_FMU_ALL)
LIBS_IPOPT = -lfmi -lModelicaExternalC -ljmi_cppad -ljmi_algorithm_cppad -ljmi_solver_cppad $(LIB_FLAGS_IPOPT) "-L$(SUNDIALS_LIB_DIR)" -lsundials_kinsol -lsundials_nvecserial $(LIB_MINPACK)
LIBS_JMU = $(LIBS_IPOPT) -lpthread
LIBS_CEVAL = $(LIBS_FMU_STD)

# Include paths for compilation
INCL_CPPAD = "-I$(JMI_INC_DIR)" "-I$(CPPAD_INC_DIR)" "-I$(SUNDIALS_INC_DIR)"
INCL_NOCPPAD = "-I$(RUNTIMELIBRARY_INC_DIR)" "-I${STANDARD_HEADER_INC_DIR}" $(EXT_INC_DIRS) "-I$(SUNDIALS_INC_DIR)"

# set variable to correct makefile (for recursive call in fmume/fmucs)
MAKEFILE = $(lastword $(MAKEFILE_LIST))

ceval:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 ceval_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE PLATFORM_FLAG=$(PLATFORM_64) "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" ceval_
endif

ceval_: $(OBJS)
	if not exist "$(BINARY_BASE_DIR)" mkdir "$(BINARY_BASE_DIR)"
	$(CC) $(CFLAGS) -o "$(EXECUTABLE)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_CEVAL)
	$(RM) $(OBJS)

fmume10:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 fmume10_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE PLATFORM_FLAG=$(PLATFORM_64) "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" fmume10_
endif

fmume10_: $(OBJS)
	if not exist "$(BINARY_MKDIR)" mkdir "$(BINARY_MKDIR)"
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o "$(SHARED)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_FMUME10)
	$(RM) $(OBJS)

fmucs10:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 fmucs10_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE PLATFORM_FLAG=$(PLATFORM_64) "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" fmucs10_
endif

fmucs10_: $(OBJS)
	mkdir "$(BINARY_MKDIR)"
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o "$(SHARED)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_FMUCS10)
	$(RM) $(OBJS)

fmume20:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 FMU_TYPES+=-DFMUME20 fmume20_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE "PLATFORM_FLAG=$(PLATFORM_64)" "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" FMU_TYPES+=-DFMUME20 fmume20_
endif

fmume20_: $(OBJS)
	mkdir $(BINARY_MKDIR)
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o "$(SHARED)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_FMU20)
	$(RM) $(OBJS)

fmucs20:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 FMU_TYPES+=-DFMUCS20 fmucs20_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE "PLATFORM_FLAG=$(PLATFORM_64)" "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" FMU_TYPES+=-DFMUCS20 fmucs20_
endif

fmucs20_: $(OBJS)
	mkdir $(BINARY_MKDIR)
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o "$(SHARED)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_FMU20)
	$(RM) $(OBJS)

fmumecs20:
ifeq ($(PLATFORM),win32)
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE EXT_OPTION=-mincoming-stack-boundary=2 FMU_TYPES+=-DFMUME20 FMU_TYPES+=-DFMUCS20 fmucs20_
else
	"$(MAKE)" -f "$(MAKEFILE)" JMI_AD=JMI_AD_NONE PLATFORM_FLAG=$(PLATFORM_64) "RUNTIMELIBRARY_LIB_DIR=$(RUNTIMELIBRARY_LIB_DIR64)" "MINPACK_LIB_DIR=$(MINPACK_LIB_DIR64)" "SUNDIALS_LIB_DIR=$(SUNDIALS_LIB_DIR64)" FMU_TYPES+=-DFMUME20 FMU_TYPES+=-DFMUCS20 fmucs20_
endif

fmumecs20_: $(OBJS)
	mkdir $(BINARY_MKDIR)
	$(CC) $(SHARED_LDFLAGS) $(CFLAGS) -o "$(SHARED)" $(OBJS) "-L${RUNTIMELIBRARY_LIB_DIR}" $(LIBS_FMU20)
	$(RM) $(OBJS)

jmu: $(OBJS)
# This is to ensure that the contents of libjmi_algorithm_cppad and
# libjmi_solver_cppade is included in the resulting shared object file.
ifeq "$(wildcard .jmi_objs )" ""
	mkdir .jmi_objs
endif
	mkdir $(BINARY_MKDIR)
	cd .jmi_objs && $(AR) -x "$(JMODELICA_HOME)/lib/libjmi_algorithm_cppad.a"  && $(AR) -x "$(JMODELICA_HOME)/lib/libjmi_solver_cppad.a"  
	$(CXX) $(SHARED_LDFLAGS) $(CXXLINKFLAGS) $(CXXFLAGS) -o "$(SHARED)" $(OBJS) ./.jmi_objs/libjmi_algorithm_cppad_la-jmi_init_opt.o ./.jmi_objs/libjmi_algorithm_cppad_la-jmi_opt_coll.o ./.jmi_objs/libjmi_algorithm_cppad_la-jmi_opt_coll_radau.o ./.jmi_objs/libjmi_solver_cppad_la-jmi_init_opt_ipopt.o ./.jmi_objs/libjmi_solver_cppad_la-jmi_init_opt_TNLP.o ./.jmi_objs/libjmi_solver_cppad_la-jmi_opt_coll_ipopt.o ./.jmi_objs/libjmi_solver_cppad_la-jmi_opt_coll_TNLP.o "-L$(JMI_LIB_DIR)" $(LIBS_JMU)
	$(RM) .jmi_objs\*.o
	rmdir .jmi_objs
	$(RM) $(OBJS)

all: $(SHARED)

.SUFFIXES: .cpp .c .o .obj

# Compile
%.o: sources/%.cpp
ifeq ($(JMI_AD),JMI_AD_CPPAD)
	$(CXX) $(CXXFLAGS) $(INCL_CPPAD) -c -o "$@" $<
else
	$(CXX) $(CXXFLAGS) $(INCL_NOCPPAD) -c -o "$@" $<
endif

%.o: sources/%.c
ifeq ($(JMI_AD),JMI_AD_CPPAD)
	$(CXX) $(CXXFLAGS) $(INCL_CPPAD) -c -o "$@" $<
else
	$(CC) $(CFLAGS) $(INCL_NOCPPAD) -c -o "$@" $<
endif

# Create a rule which uses the first item in arg as src name and
# following items as additional CFLAGS. Argument "file:-O2:-d"
# will expand to the rule "file.o : CFLAGS = $(CFLAGS) -O2 -d"
define cflagrule
$(word 1,$(1)).o : CFLAGS = $(CFLAGS) $(wordlist 2,$(words $(1)),$(1))
endef

# For each word in variable EXTRA_CFLAGS, create a cflagrule
# Example "main2 main3:O3 main:O2:pedantic"
$(foreach src, $(EXTRA_CFLAGS),$(eval $(call cflagrule, $(subst :, -,$(src)))))

clean:
	$(RM) $(SHARED) $(OBJS)
