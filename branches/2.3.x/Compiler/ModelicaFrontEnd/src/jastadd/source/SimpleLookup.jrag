/*
    Copyright (C) 2011 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

aspect SimpleClassLookup {
	
	/*
	 * Warning: This is only a prototype and is currently not tested.
	 *          Use with care.
	 */

	// TODO: Add javadoc

    syn SrcClassDecl SrcExtendsClause.findClassDecl()   = getSuper().findClassDecl();
    syn SrcClassDecl SrcComponentDecl.findClassDecl()   = myComponentClause().findClassDecl();
    syn SrcClassDecl SrcComponentClause.findClassDecl() = getClassName().findClassDecl();
    syn SrcClassDecl SrcImportClause.findClassDecl()    = getPackageName().findClassDecl();

	// Used to prevent circular lookups without the repetitions imposed by declaring attributes circular.
	protected boolean SrcAccess.duringFindClassDecl = false;
	protected int SrcClassDecl.duringFindClassDeclRevisits = 0;
	protected static final int SrcClassDecl.MAX_FIND_CLASS_DECL_REVISITS = 20;
		
	syn SrcClassDecl SrcAccess.findClassDecl();
	syn lazy SrcClassDecl SrcNamedAccess.findClassDecl() {
		if (duringFindClassDecl)
			return unknownClassDecl();
		duringFindClassDecl = true;
		try {
			return simpleLookupClass(getID());
		} finally {
			duringFindClassDecl = false;
		}
	}
	eq SrcDot.findClassDecl()          = getLastAccess().findClassDecl();
	eq SrcGlobalAccess.findClassDecl() = getSrcAccess().findClassDecl();


    inh SrcClassDecl SrcAccess.simpleLookupClass(String name);
    inh SrcClassDecl SrcClassDecl.simpleLookupClass(String name);
    eq SrcDot.getSrcAccess(int i).simpleLookupClass(String name)          = 
        (i == 0) ? simpleLookupClass(name) : getSrcAccess(i - 1).findClassDecl().simpleLookupClassMemberScope(name);
    eq SrcGlobalAccess.getSrcAccess().simpleLookupClass(String name)      = simpleLookupClassGlobalScope(name);
    eq SrcImportClause.getPackageName().simpleLookupClass(String name) = simpleLookupClassGlobalScope(name);
    eq SrcExtendsClause.getChild().simpleLookupClass(String name)      = simpleLookupClassFromExtends(name);
    eq SrcFullClassDecl.getChild().simpleLookupClass(String name)      = simpleLookupClassDefaultScope(name);
    eq Program.getChild().simpleLookupClass(String name)            = simpleLookupClassDefaultScope(name);

    inh SrcClassDecl SrcExtendsClause.simpleLookupClassFromExtends(String name);
    eq SrcFullClassDecl.getChild().simpleLookupClassFromExtends(String name) = simpleLookupClassLocalScope(name);
    eq Program.getChild().simpleLookupClassFromExtends(String name)       = simpleLookupClassDefaultScope(name);

	syn SrcClassDecl SrcClassDecl.simpleLookupClassDotted(String name) {
		String[] parts = name.split("\\.");
		SrcClassDecl cd = this;
		for (String part : parts) {
			cd = cd.simpleLookupClassMemberScope(part);
			if (cd == null)
				return null;
		}
		return cd;
	}

    syn SrcClassDecl SrcClassDecl.simpleLookupClassDottedGlobal(String name) {
        String[] parts = name.split("\\.", 2);
        SrcClassDecl base = simpleLookupClassGlobalScope(parts[0]);
        return (base == null || parts.length == 1) ? base : base.simpleLookupClassDotted(parts[1]);
    }

	syn SrcClassDecl Program.simpleLookupClassDotted(String name) {
		String[] parts = name.split("\\.", 2);
		SrcClassDecl base = simpleLookupClassDefaultScope(parts[0]);
		return (base == null || parts.length == 1) ? base : base.simpleLookupClassDotted(parts[1]);
	}

    syn SrcClassDecl Program.simpleLookupClassDottedGlobal(String name) {
        return simpleLookupClassDotted(name);
    }

    inh SrcClassDecl SrcAccess.simpleLookupClassGlobalScope(String name);
    inh SrcClassDecl SrcClassDecl.simpleLookupClassGlobalScope(String name);
    inh SrcClassDecl SrcImportClause.simpleLookupClassGlobalScope(String name);
	eq Program.getChild().simpleLookupClassGlobalScope(String name) = simpleLookupClassDefaultScope(name);


    syn lazy SrcClassDecl Program.simpleLookupClassDefaultScope(String name) {
        for (SrcStoredDefinition sd : getUnstructuredEntitys())
            for (SrcClassDecl cd : sd.getSrcClassDecls())
                if (cd.matches(name))
                    return cd;
        
        SrcClassDecl res = simpleLookupInClassList(getPredefinedTypes(), name);
        if (res != null)
            return res;
            
        res = simpleLookupInClassList(getBuiltInTypes(), name);
        if (res != null)
            return res;
            
        res = simpleLookupInClassList(getBuiltInFunctions(), name);
        if (res != null)
            return res;
        
        res = simpleLookupInLibNodeList(getSrcLibNodes(), name);
        return (res != null) ? res : getSrcUnknownClassDecl();
    }

	/**
	 * Lookup in contained classes, extended classes, imports and surrounding classes.
	 */
	syn lazy SrcClassDecl SrcFullClassDecl.simpleLookupClassDefaultScope(String name) {
		SrcClassDecl res = simpleLookupClassMemberScope(name);
		if (res.isUnknown())
			res = simpleLookupClassInImports(name);
		
		return res.isUnknown() ? simpleLookupClass(name) : res;
	}

	
	/**
	 * Lookup in contained classes, imports and surrounding classes.
	 */
	syn lazy SrcClassDecl SrcFullClassDecl.simpleLookupClassLocalScope(String name) {
		if (duringFindClassDeclRevisits > MAX_FIND_CLASS_DECL_REVISITS) 
			return unknownClassDecl();
		duringFindClassDeclRevisits++;
		
		SrcClassDecl res = simpleLookupInClassList(classes(), name);
		if (res == null)
			res = simpleLookupClassInImports(name);
		
		duringFindClassDeclRevisits--;
		return res.isUnknown() ? simpleLookupClass(name) : res;
	}
	
	
	/**
	 * Lookup in contained classes and extended classes.
	 */
	syn SrcClassDecl SrcClassDecl.simpleLookupClassMemberScope(String name) = unknownClassDecl();
	eq SrcUnknownClassDecl.simpleLookupClassMemberScope(String name)     = unknownClassDecl();
	
	syn lazy SrcClassDecl SrcFullClassDecl.simpleLookupClassMemberScope(String name) {
		if (duringFindClassDeclRevisits > MAX_FIND_CLASS_DECL_REVISITS) 
			return unknownClassDecl();
		duringFindClassDeclRevisits++;
		
		SrcClassDecl res = simpleLookupInClassList(classes(), name);
		if (res == null)
			res = simpleLookupClassInExtends(name);
		
		duringFindClassDeclRevisits--;
		return res;
	}
	
	eq SrcLibClassDecl.simpleLookupClassMemberScope(String name) {
		if (duringFindClassDeclRevisits > MAX_FIND_CLASS_DECL_REVISITS) 
			return unknownClassDecl();
		duringFindClassDeclRevisits++;
		
		SrcClassDecl res = super.simpleLookupClassMemberScope(name);
		if (res.isUnknown()) {
			SrcClassDecl libRes = simpleLookupInLibNodeList(getSrcLibNodes(), name);
			if (libRes != null)
				res = libRes;
		}
		duringFindClassDeclRevisits--;
		return res;
	}
	
	eq SrcShortClassDecl.simpleLookupClassMemberScope(String name) = 
		getSrcExtendsClauseShortClass().findClassDecl().simpleLookupClassMemberScope(name);
    eq SrcLibNode.simpleLookupClassMemberScope(String name)        = 
        myClass().simpleLookupClassMemberScope(name);

	syn SrcClassDecl SrcImportClause.simpleLookupClassInImport(String name) = 
		matches(name) ? findClassDecl() : unknownClassDecl();
	eq SrcImportClauseUnqualified.simpleLookupClassInImport(String name) = 
		findClassDecl().simpleLookupClassMemberScope(name);
		

    protected SrcClassDecl SrcFullClassDecl.simpleLookupClassInImports(String name) {
        SrcClassDecl res;
        for (SrcImportClause imp : imports()) {
            res = imp.simpleLookupClassInImport(name);
            if (!res.isUnknown())
                return res;
        }
        
        return unknownClassDecl();
    }

    protected SrcClassDecl SrcFullClassDecl.simpleLookupClassInExtends(String name) {
        SrcClassDecl res;
        for (SrcExtendsClause sup : superClasses()) {
            res = sup.findClassDecl().simpleLookupClassMemberScope(name);
            if (!res.isUnknown())
                return res;
        }
        
        return unknownClassDecl();
    }


    /**
     * Convenience method for looking up a class in a List of SrcClassDecls.
     *
     * Unlike the other lookup methods, this one returns null when the class isn't found.
     */
    public static SrcClassDecl ASTNode.simpleLookupInClassList(Iterable<? extends SrcClassDecl> list, String name) {
        for (SrcClassDecl cd : list)
            if (cd.matches(name))
                return cd;
        return null;
    }


	/**
	 * Convenience method for looking up a class in a List of SrcClassDecls.
	 *
	 * Unlike the other lookup methods, this one returns null when the class isn't found.
	 */
	public static SrcClassDecl ASTNode.simpleLookupInLibNodeList(List<SrcLibNode> list, String name) {
		for (SrcLibNode ln : list)
			if (ln.matches(name))
				return (SrcClassDecl) ln;
		return null;
	}


    eq SrcIdDecl.matches(String str)                = getID().equals(str);
    eq SrcBaseClassDecl.matches(String str)         = getName().matches(str);
    eq SrcImportClauseRename.matches(String str)    = getSrcIdDecl().matches(str);
    eq SrcImportClauseQualified.matches(String str) = 
        getPackageName().getLastAccess().matches(str);
    eq SrcLibNode.matches(String str) {
        if (name().equalsIgnoreCase(str)) {
            myClass();
        }
        return name().equals(str);
    }

}