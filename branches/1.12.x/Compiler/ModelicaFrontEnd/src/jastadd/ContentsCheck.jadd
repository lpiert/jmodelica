/*
    Copyright (C) 2009 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import org.jmodelica.util.ErrorCheckType;

aspect ContentCheck {

	/**
	 * \brief Check for code that is not allowed in its current context.
	 * 
	 * Examples would be checking that classes follow the requirements of 
	 * their restriction. 
	 */
	public void ASTNode.contentCheck(ErrorCheckType checkType) {}
	
	public void FAbstractEquation.contentCheck(ErrorCheckType checkType) {
		if (inFunction()) 
			error("Equations are not allowed in functions");
	}
	
	public void FAlgorithm.contentCheck(ErrorCheckType checkType) {}
	
	public void InstDerExp.contentCheck(ErrorCheckType checkType) {
		if (inFunction()) {
			error("The der() operator is not allowed in functions");
		} else {
			try {
				getFExp().diff(FExp.TIME);
			} catch (ExpressionDifferentiationException e) {
				e.generateError();
			}
		}
	}
    
    public void FTerminate.contentCheck(ErrorCheckType checkType) {
        if (inFunction()) 
            error("The terminate() statement is not allowed in functions");
    }
    
    public void FReinit.contentCheck(ErrorCheckType checkType) {
        if (!inWhen() || inAlgorithm()) 
            error("The reinit() operator is only allowed in when equations");
    }
	
	public void InstExternal.contentCheck(ErrorCheckType checkType) {
		if (!inFunction())
			error("External function declarations are only allowed in functions");
	}
	
	public void FReturnStmt.contentCheck(ErrorCheckType checkType) {
		if (!inFunction())
			error("Return statements are only allowed in functions");
	}
	
	public void FWhenStmt.contentCheck(ErrorCheckType checkType) {
		if (myFAlgorithm().getType() instanceof FInitialEquation || myFAlgorithm().isInitial())
			error("When statements are not allowed in initial algorithms");
		else if (inFunction())
			error("When statements are not allowed in functions");
		else if (insideBlockStmt())
			error("When statements are not allowed inside if, for, while and when clauses");
	}
	
	public void FParseArray.contentCheck(ErrorCheckType checkType) {
		// If not already rewritten to FLongArray, this is an error.
		error("The array() operator may not be used in function call equations or function call statements");
	}
	
	public void FEndExp.contentCheck(ErrorCheckType checkType) {
		if (!inArraySubscripts())
			error("The end operator may only be used in array subscripts");
	}
	
	public void InstPreExp.contentCheck(ErrorCheckType checkType) {
		if (inFunction()) { 
			error("The pre() operator may not be used inside functions");
		}
	}
	
	public void FEdgeExp.contentCheck(ErrorCheckType checkType) {
		if (inFunction()) { 
			error("The edge() operator may not be used inside functions");
		}
	}
	
	public void FChangeExp.contentCheck(ErrorCheckType checkType) {
		if (inFunction()) { 
			error("The change() operator may not be used inside functions");
		}
	}
	
	public void InstExtends.contentCheck(ErrorCheckType checkType) {
		if (myInstClass().isExternalObject())
			error("Classed derived from ExternalObject can neither be used in an extends-clause nor in a short class defenition");
	}
	
	public void InstFunctionCall.contentCheck(ErrorCheckType checkType) {
		if (!generated) {
			InstClassDecl target = getName().myInstClassDecl();
			if (target.isConstructor() || target.isDestructor())
				error("Constructors and destructors for ExternalObjects can not be used directly");
		}
	}
	
	public void FConnRootedDep.contentCheck(ErrorCheckType checkType) {
		warning("The rooted() operator has been deprecated in favor of Connections.rooted()");
	}
	
	public void InstComponentDecl.contentCheck(ErrorCheckType checkType) {
		if (!inFunction() && hasFArraySubscripts()) // Array sizes outside functions should be structural parameters
			getFArraySubscripts().markAsStructuralParameter(checkType);
	}
	
    public void InstAssignable.contentCheck(ErrorCheckType checkType) {
        super.contentCheck(checkType);
        if (isDeclaredInput() || isDeclaredOutput()) {
            InstComponentDecl inherit = inheritsInputOutputFrom();
            if (inherit != null)
                inherit.error("Can't declare %s as %s, since it contains a component declared as input or output", 
                        name(), (isDeclaredInput() ? "input" : "output"));
        }
        if (isDeclaredFlow()) {
            InstComponentDecl inherit = inheritsFlowFrom();
            if (inherit != null)
                inherit.error("Can't declare %s as flow, since it contains a component declared as flow", name());
        }
    }
	
	public void InstClassDecl.contentCheck(ErrorCheckType checkType) {
		if (isExternalObject()) {
			checkContentsOKInExternalObject();
			myConstructor().checkConstructor(primitiveScalarType());
			myDestructor().checkDestructor(primitiveScalarType());
		}
	}
	
	public void InstNode.checkContentsOKInExternalObject() {
		for (InstExtends ie : getInstExtendss())
			ie.checkContentsOKInExternalObject();
		boolean ok = true;
		for (InstClassDecl icd : getInstClassDecls())
			if (!icd.name().equals("constructor") && !icd.name().equals("destructor"))
				ok = false;
		if (getNumInstComponentDecl() > 0)
			ok = false;
		if (getNumRedeclaredInstClassDecl() > 0)
			ok = false;
		if (getNumFAbstractEquation() > 0)
			ok = false;
		if (!ok)
			error("External object classes may not contain any elements except the constructor and destructor");
	}
	
	public void InstClassDecl.checkDestructor(FType eoType) {
		if (isUnknown())
			return;
		if (myOutputs().size() != 0 || myInputs().size() != 1 || !myInputs().get(0).type().typeCompatible(eoType))
			error("An external object destructor must have exactly one input of the same type as the constructor, and no outputs");
	}
	
	public void InstClassDecl.checkConstructor(FType eoType) {
		if (isUnknown())
			return;
		if (myOutputs().size() != 1 || !myOutputs().get(0).type().typeCompatible(eoType))
			error("An external object constructor must have exactly one output of the same type as the constructor");
	}
	
	// TODO: check if this is a builtin function (with bad arguments), error otherwise
//	public void FBuiltinExternalLanguage.contentCheck(ErrorCheckType checkType) {
//		error("The \"builtin\" external language specitication may only be used for functions that are a part of the Modelica specification");
//	}
	
	public void FUnknownExternalLanguage.contentCheck(ErrorCheckType checkType) {
		error("The external language specification \"" + getLanguage() + "\" is not supported");
	}
	
	inh boolean FIterExp.iterExpUseOK();
	eq FExp.getChild().iterExpUseOK()          = false;
	eq Root.getChild().iterExpUseOK()          = false;
	eq InstNode.getChild().iterExpUseOK()      = false;
	eq FArray.getChild().iterExpUseOK()        = true;
	eq FMinMaxExp.getChild().iterExpUseOK()    = true;
	eq FReductionExp.getChild().iterExpUseOK() = true;
	
	public void FIterExp.contentCheck(ErrorCheckType checkType) {
		if (!iterExpUseOK())
			error("Reduction-expressions are only allowed with sum(), min(), max() and product()");
	}
	
	public void FOverflowIntLitExp.contentCheck(ErrorCheckType checkType) {
		warning("Integer literal \"%s\" is too large to represent as 32-bit Integer, using Real instead.", 
				getString(), getString(), getValue());
	}
	
	/**
     * Check if this equation is in a place that allows connect clauses.
	 */
	inh boolean FAbstractEquation.connectAllowed();
	eq FAbstractEquation.getChild().connectAllowed() = mayContainConnect() && connectAllowed();
	eq FClass.getChild().connectAllowed()            = true;
	eq InstNode.getChild().connectAllowed()          = true;
	
	/**
	 * Check if this equation may legally contain a conncect clause.
	 */
	syn boolean FAbstractEquation.mayContainConnect() = false;
	eq InstForClauseE.mayContainConnect()             = true;
	eq FElseEquation.mayContainConnect()              = true;
	eq FIfEquation.mayContainConnect()                = 
		getTest().variability().parameterOrLess() && getTest().canCeval();
	
	public void FConnectClause.contentCheck(ErrorCheckType checkType) {
		super.contentCheck(checkType);
		if (!connectAllowed())
			error("Connect clauses are not allowed in if equations with non-parameter conditions, or in when equations"); 
	}
	
	public void FWhenEquation.contentCheck(ErrorCheckType checkType) {
		// TODO: check that two when clauses do not assign the same variable
		super.contentCheck(checkType);
		if (inWhen())
			error("Nestled when clauses are not allowed");
		else if (getType() instanceof FInitialEquation || isInitial()) {
			error("When equations are not allowed in initial equation sections");
		} else {
			if (!isBalancedAssignments())
				error("All branches in when equation must assign the same variables");
		}
	}
    
    public void InstNamedModification.contentCheck(ErrorCheckType checkType) {
        if (getName().hasFArraySubscripts())
            error("Modifiers of specific array elements are not allowed");
    }
    
    public void InstComponentRedeclare.contentCheck(ErrorCheckType checkType) {}
	
	public static final String FIfEquation.UNBALANCED_ERROR = 
		"All branches in if equation with non-parameter tests must have the same number of equations";
	public static final String FIfEquation.UNBALANCED_ERROR_WHEN = 
		"All branches in if equation with non-parameter tests within when equation must assign the same variables";
	
	public void FIfEquation.contentCheck(ErrorCheckType checkType) {
		super.contentCheck(checkType);
		if (isTopWhenIfEquation()) {
			boolean inWhen = inWhen();
			boolean balanced = inWhen ? isBalancedAssignments() : isBalancedEquations();
			boolean functionCallEqu = hasFunctionCallEquations(); 
			if (hasOnlyParamTests()) {
				setEliminateOnParamTest(checkType);
			} else {
				if (!balanced) {
					error(inWhen ? UNBALANCED_ERROR_WHEN : UNBALANCED_ERROR);
				}
			}
		}
	}
	
	public void FEquation.contentCheck(ErrorCheckType checkType) {
		super.contentCheck(checkType);
		if (inWhen() && !isAssignmentEqn())
			error("Only assignment equations are allowed in when clauses");
	}
	
	syn boolean FIfWhenElseEquation.isBalancedEquations() = true; 
	eq FIfEquation.isBalancedEquations() = hasElse() && elseIsBalancedEquations();
	
	syn boolean FIfWhenEquation.elseIsBalancedEquations() = 
		numScalarEquations() == getElse().numScalarEquations() && 
		getElse().isBalancedEquations();
	
	syn boolean FIfWhenElseEquation.isBalancedAssignments() = true; 
	eq FIfEquation.isBalancedAssignments()   = hasElse() && elseIsBalancedAssignments();
	eq FWhenEquation.isBalancedAssignments() = !hasElse() || elseIsBalancedAssignments();
	
	syn boolean FIfWhenEquation.elseIsBalancedAssignments() = 
		assignedSet().equals(getElse().assignedSet()) && getElse().isBalancedAssignments();
	
	inh boolean FIfEquation.isTopWhenIfEquation();
	eq ASTNode.getChild().isTopWhenIfEquation()        = true;
	eq FIfWhenEquation.getElse().isTopWhenIfEquation() = false;
	
	syn boolean FIfWhenElseEquation.hasOnlyParamTests() = true;
	eq FIfWhenEquation.hasOnlyParamTests()              = 
		getTest().variability().parameterOrLess() && (!hasElse() || getElse().hasOnlyParamTests());
	
	syn boolean FAbstractEquation.isAssignmentEqn() = false;
	eq FEquation.isAssignmentEqn()                  = getLeft().isAccess();
	
	syn boolean FAbstractEquation.hasFunctionCallEquations() = false;
	eq FFunctionCallEquation.hasFunctionCallEquations()      = true;
	eq FIfWhenEquation.hasFunctionCallEquations()            = 
		super.hasFunctionCallEquations() || (hasElse() && getElse().hasFunctionCallEquations());
	eq FIfWhenElseEquation.hasFunctionCallEquations() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquations())
				return true;
		return false;
	}
	eq FForClauseE.hasFunctionCallEquations() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquations())
				return true;
		return false;
	}
	
	syn boolean FAbstractEquation.hasFunctionCallEquationsWithLefts() = false;
	eq FFunctionCallEquation.hasFunctionCallEquationsWithLefts()      = getNumLeft() > 0;
	eq FIfWhenEquation.hasFunctionCallEquationsWithLefts()            = 
		super.hasFunctionCallEquationsWithLefts() || (hasElse() && getElse().hasFunctionCallEquationsWithLefts());
	eq FIfWhenElseEquation.hasFunctionCallEquationsWithLefts() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquationsWithLefts())
				return true;
		return false;
	}
	eq FForClauseE.hasFunctionCallEquationsWithLefts() {
		for (FAbstractEquation equ : getFAbstractEquations())
			if (equ.hasFunctionCallEquationsWithLefts())
				return true;
		return false;
	}
	
	
	public void InstMPackage.contentCheck(ErrorCheckType checkType) {
		if (!myInstClassDecl().isOkPackage())
			error("Packages may only contain classes and constants");
	}
	
	/**
	 * Check that the restriction of this class is fulfilled.
	 */
	public void InstClassDecl.checkRestriction(ErrorCheckType checkType) {}
	
	public void InstBaseClassDecl.checkRestriction(ErrorCheckType checkType) {
		getInstRestriction().contentCheck(checkType);
	}
	
	/**
	 * Get the class this restriction is attached to.
	 */
	inh InstClassDecl InstRestriction.myInstClassDecl();
	eq InstBaseClassDecl.getInstRestriction().myInstClassDecl() = this;
	
	/**
	 * Check if this class fulfills the requirements of a package.
	 */
	syn boolean InstClassDecl.isOkPackage() {
		for (InstComponentDecl icd : getInstComponentDecls())
			if (!icd.isConstant())
				return false;
		return getNumFAbstractEquation() == 0;
	}
	eq InstSimpleShortClassDecl.isOkPackage() = actualInstClass().isOkPackage();
	
}

aspect StructuralParams {
	
	private boolean InstAssignable.isStructuralParam = false;
	
	public void FExp.markAsStructuralParameter(ErrorCheckType checkType) {
		if (inFunction())
			return;
		if (!checkType.allowConstantNoValue() && !canCeval())
			compliance("Could not evaluate expression used as structural parameter: " + this);
		markUsesAsStructuralParameter(checkType);
	}
	
	public void FArraySubscripts.markAsStructuralParameter(ErrorCheckType checkType) {
		for (FSubscript s : getFSubscripts())
			s.markAsStructuralParameter(checkType);
	}
	
	public void FSubscript.markAsStructuralParameter(ErrorCheckType checkType) {}
	public void FExpSubscript.markAsStructuralParameter(ErrorCheckType checkType) {
		getFExp().markAsStructuralParameter(checkType);
	}  

	
	public void ASTNode.markUsesAsStructuralParameter(ErrorCheckType checkType) {
		for (ASTNode n : this)
			n.markUsesAsStructuralParameter(checkType);
	}
    
    public void FSizeExp.markUsesAsStructuralParameter(ErrorCheckType checkType) {
        getDimOpt().markUsesAsStructuralParameter(checkType);
    }
    
    public void FNdimsExp.markUsesAsStructuralParameter(ErrorCheckType checkType) {}
	
	public void InstAccess.markUsesAsStructuralParameter(ErrorCheckType checkType) {
		myInstComponentDecl().markAsStructuralParameter(checkType);
		markUsesInSubscriptsAsStructuralParameter(checkType);
	}
	
	public void InstAccess.markUsesInSubscriptsAsStructuralParameter(ErrorCheckType checkType) {}
	
	public void InstDot.markUsesInSubscriptsAsStructuralParameter(ErrorCheckType checkType) {
		for (InstAccess ia : getInstAccesss())
			ia.markUsesInSubscriptsAsStructuralParameter(checkType);
	}
	
	public void InstArrayAccess.markUsesInSubscriptsAsStructuralParameter(ErrorCheckType checkType) {
		getFArraySubscripts().markUsesAsStructuralParameter(checkType);
	}
	
	
	public void InstComponentDecl.markAsStructuralParameter(ErrorCheckType checkType) {
		// TODO: should probably do something constructive here?
	}
	
	public void InstAssignable.markAsStructuralParameter(ErrorCheckType checkType) {
		if (isStructuralParam || !isParameter() || inFunction() || isForIndex())
			return;
		if (indices().numElements() > 0 && !fixedAttributeCValue().reduceBooleanOr()) {
		    compliance("Parameters with fixed=false can not be marked as structural parameter");
		    return;
		}
		isStructuralParam = true;
		// Flush cache for variability()
		variability_computed = false;
		if (hasBindingFExp()) {
			FExp e = getBindingFExp();
			if (!checkType.allowConstantNoValue() && !e.canCeval())
				compliance("Could not evaluate binding expression of structural parameter " + qualifiedName());
			e.markUsesAsStructuralParameter(checkType);
		}
	}
	
}
