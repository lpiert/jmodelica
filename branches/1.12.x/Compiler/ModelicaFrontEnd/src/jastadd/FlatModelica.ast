/*
    Copyright (C) 2009-2013 Modelon AB

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \brief Root node for flat Modelica models.
 */
FlatRoot : Root ::= FClass;

/**
 * \brief A flat Modelica model containing variables, initial equations,
 * DAE equations and functions.
 *
 * FClass corresponds to a flattened Modelica model and contains essentially
 * a list of variables and variables, a list of functions and a list of equations, 
 * of which some are initial equations.
 */ 
FClass : BaseNode ::= FQName 
                      FVariable* 
                      AliasVariable:FVariable* 
                      FInitialEquation:FAbstractEquation*
                      FParameterEquation:FAbstractEquation*
                      FAbstractEquation*
                      FFunctionDecl*
                      FRecordDecl*
                      FEnumDecl*
                      FDerivedType*
                      /UnknownFVariable/
                      FResidualPair*
                      DummyVariable:FVariable*;

/**
 * \brief A flattened user-defined function.
 *
 * Results from a class with the restriction "function" and an algorithm block.
 */
FFunctionDecl : BaseNode ::= FQName 
                             FFunctionVariable* 
                             FAlgorithm 
                             [FDerivativeFunction] 
                             /DummyExp:FExp/;

/**
 * A function that is the constructor of an external object.
 */
FConstructorDecl : FFunctionDecl;

/**
 * A function that is the destructor of an external object.
 */
FDestructorDecl : FFunctionDecl;

/**
 * \brief A derivative function reference.
 * 
 * The information in this node is extracted from the derivative 
 * annotation in a function declaration.
 */
FDerivativeFunction : BaseNode ::= Name:FIdUse <Order:int> NoDerivative:FIdUse* ZeroDerivative:FIdUse*;

/**
 * \brief A flattenend record declaration.
 */
FRecordDecl : BaseNode ::= FQName FVariable*;

/**
 * \brief A flattenend enumeration declaration.
 */
FEnumDecl : BaseNode ::= Name:FIdDecl FEnumSpecification [FStringComment];
abstract FEnumSpecification : BaseNode;
FEnumLiteralList : FEnumSpecification ::= FEnumLiteral*;
FEnumUnspecified : FEnumSpecification;
FEnumLiteral : FAbstractVariable ::= Name:FIdUse [FStringComment];

/**
 * \brief Abstract base class for flat variables.
 */
abstract FAbstractVariable : BaseNode;

/**
 * \brief A flat variable in a function.
 */
FFunctionVariable : FAbstractVariable ::= [FTypePrefixInputOutput]
                                          Type:FType
                                          [BindingExp:FExp]
                                          FQName
                                          <Order:int>;

/**
 * \brief An array variable in a function. 
 */
FFunctionArray : FFunctionVariable;

/**
 * \brief A flat variable corresponding to an unknown declaration.
 */
UnknownFVariable : FAbstractVariable;

/**
 * \brief A class representing a flattened Modelica variable.
 *
 * FVariable contains information about the variable's visibility (public
 * or protected), variability (parameter, discrete or continuous) and causality
 * (input or output). In addition, FVariable contains a list of attributes
 * and, optionally, a binding expression. FVariables may represent both array
 * variables and scalar variables: this information is embedded in the 
 * FQName class.
 */
abstract FVariable : FAbstractVariable ::= FVisibilityType
                                           FTypePrefixVariability
                                           [FTypePrefixInputOutput]
                                           FAttribute*
                                           <DerivedType>
                                           [BindingExp:FExp]
                                           [FStringComment]
                                           FQName;

/**
 * Represents a set of attributes that can be used by several variables.
 * 
 * Any attributes on the variable itself overrides attributes with the same name in the set.
 */
FDerivedType : BaseNode ::= <Name> BaseType:FType FAttribute*;
 
/**
 * \brief Base class for variable type prefixes.
 */
abstract FTypePrefix : BaseNode;

/**
 * \brief Base class for variability type prefixes.
 */
abstract FTypePrefixVariability : FTypePrefix;

/**
 * \brief Continuous (default) variability.
 *
 * Continuous is not a valid prefix but is used to identify expressions that 
 * are not declared as parameters, constants, or discrete.
 */
FContinuous : FTypePrefixVariability;

/**
 * \brief Discrete variability.
 */
FDiscrete : FTypePrefixVariability;

/**
 * \brief Parameter variability.
 */
FParameter : FTypePrefixVariability;

/**
 * Structural parameter variability.
 */
FStructParameter : FParameter;

/**
 * \brief Constant variability.
 */
FConstant : FTypePrefixVariability;

/**
 * \brief Base class for causality type prefixes.
 */
abstract FTypePrefixInputOutput : FTypePrefix;

/**
 * \brief Input causality.
 */ 
FInput : FTypePrefixInputOutput;

/**
 * \brief Output causality.
 */
FOutput : FTypePrefixInputOutput;

/**
 * \brief FVariable of type Real.
 */
FRealVariable : FVariable;

/**
 * \brief FVariable of type Integer.
 */
FIntegerVariable : FVariable;

/**
 * \brief FVariable of type Boolean.
 */
FBooleanVariable : FVariable;

/**
 * \brief FVariable of type String.
 */
FStringVariable : FVariable;

/**
 * \brief FVariable of type Enumeration.
 */
FEnumVariable : FVariable ::= Enum:FQName /Type:FType/;

/**
 * \brief An external object variable.
 */
FExternalObjectVariable : FVariable ::= [DestructorCall:FFunctionCallStmt];

/**
 * \brief Pre FVariable of type Real.
 */
FPreRealVariable : FRealVariable;

/**
 * \brief FVariable of type Integer.
 */
FPreIntegerVariable : FIntegerVariable;

/**
 * \brief FVariable of type Boolean.
 */
FPreBooleanVariable : FBooleanVariable;

/**
 * \brief FVariable of type String.
 */
FPreStringVariable : FStringVariable;

/**
 * \brief FVariable of type Enumeration.
 */
FPreEnumVariable : FEnumVariable;

/**
 * \brief FVariable of a record type.
 */
FRecordVariable : FVariable ::= Record:FQName /Type:FType/;

/**
 * \brief FVariable corresponding to a derivative variable.
 *
 * Notice that this kind of variables does not result from flattening, but are
 * introduced when a flattened model is transformed into a canonical form.
 */
FDerivativeVariable : FRealVariable;

FDummyDerivativeVariable : FRealVariable;

FHDerivativeVariable : FDerivativeVariable ::= <Order:int>;

FHDummyDerivativeVariable : FDummyDerivativeVariable ::= <Order:int>;

/**
 * \brief Node representing the visibility of an FVariable object.
 */
abstract FVisibilityType : BaseNode;

/**
 * \brief Public visibility.
 */
FPublicVisibilityType : FVisibilityType;

/**
 * \brief Protected visibility.
 */
FProtectedVisibilityType : FVisibilityType;

/**
 * \brief Visibility for temporary variable. 
 *
 * Used to mark temporary variables added during elaboration, e.g. to handle function calls returning array.
 */
FTemporaryVisibilityType : FPublicVisibilityType;

/**
 * Visibility for runtime option parameters. 
 */
FRuntimeOptionVisibilityType : FPublicVisibilityType;

/**
 * \brief Base class for all type nodes.
 */
abstract FType : BaseNode ::= <Size:Size>;

/**
 * \brief A record type.
 * 
 * In the instance tree, the components are stored alphabetically, 
 * but in the flat tree they are in the order declared. 
 */
FRecordType : FType ::= <Name> Component:FRecordComponentType* <FClass:FClass>;

/**
 * \brief The type description of a record component.
 */
FRecordComponentType : BaseNode ::= <Name> FType;

/**
 * \brief Base class for primitive type nodes.
 *
 * The flat type system is based on explicit representations of the primitive
 * types Real, Integer, Boolean and String, where each type corresponds to
 * an AST node. FPrimitiveType serves as base class for these AST nodes.
 */
abstract FPrimitiveType : FType;

/**
 * Type representing no value at all, equivalent to the void type in C-like languages.
 */
FNoType : FType;

/**
 * \brief Base class for the numeric types Real and Integer.
 */
abstract FPrimitiveNumericType : FPrimitiveType;

/**
 * \brief Class representing the primitive type Real.
 */
FRealType : FPrimitiveNumericType;

/**
 * \brief Class representing the primitive type Integer.
 */
FIntegerType : FPrimitiveNumericType;

/**
 * \brief Class representing the primitive type Boolean.
 */
FBooleanType : FPrimitiveType;

/**
 * \brief Class representing the primitive type String.
 */
FStringType : FPrimitiveType;

/**
 * \brief Class representing an enumeration type.
 */
FEnumType : FPrimitiveType ::= <Name> FEnumLiteralType*;

FEnumLiteralType : FPrimitiveType ::= <Name>;

/**
 * \brief Class representing an unknown type.
 *
 * Used to represent type errors. 
 */
FUnknownType : FType;

/**
 * \brief Class representing the external object type.
 */
FExternalObjectType : FPrimitiveType;

/**
 * \brief A qualified name supporting array subscripts.
 *
 * Flattened variables and identifiers typically have qualified names, which
 * are represented by FQName. FQName contains a list of FQNamePart objects
 * which in turn contains a name and, optionally, array subscripts. The 
 * structure of the FQName class is motivated by the fact that flat names
 * needs to be printed in a number of different way (dot notation, underscore
 * notation etc).
 */
abstract FQName : BaseNode;

/**
 * An empty FQName, used as the start of prefixes.
 */
FQNameEmpty : FQName;

/**
 * An FQName that is a simple string, used for scalarized names and simple names. 
 */
FQNameString : FQName ::= <Name:String>;

/**
 * A full FQName, with a list of FQNameParts.
 */
FQNameFull : FQName ::= FQNamePart*;

/**
 * A temporary FQNameFull, rewritten to an FQNameFull if in a function, and FQNameString otherwise.
 */
FQNameFullScalarize : FQNameFull;

/**
 * A name part without array subscripts.
 * 
 * To be used in FQName:s.
 */
FQNamePart : BaseNode ::= <Name:String>;

/**
 * A name part with array subscripts.
 * 
 * To be used in FQName:s.
 */
FQNamePartArray : FQNamePart ::= FArraySubscripts;

/**
 * \brief Representation of an attribute for a built-in types.
 *
 * The attributes (start, unit etc.) of the built-in types are represented by
 * objects of the FAttribute class, which in turned are stored in the 
 * FVariables. FAttributes contain information about the attribute name and
 * type, its value, whether it is set explicitly by the user and the prefixes
 * each and final. 
 */
FAttribute : BaseNode ::= Type:FIdUse
                          Name:FIdDecl 			   
                          [Value:FExp]
                          <AttributeSet:boolean>
                          <Level:int>
                          [FEach]
                          [FFinal]
                          FAttribute*;

/**
 * \brief An internal generated attribute that is removed during the scalarization step.
 */
FInternalAttribute : FAttribute;

/**
 * \brief Node representing the each prefix for attributes.
 */
FEach : BaseNode;

/**
 * \brief Node representing the final prefix for attributes.
 */
FFinal : BaseNode;

/**
 * \brief A string comment node.
 */
FStringComment : BaseNode ::= <Comment:String>;

/**
 * \brief Array subscripts used in FQNames.
 */
FArraySubscripts : BaseNode ::= FSubscript*;

/**
 * \brief Base class for array subscripts.
 */ 
abstract FSubscript : BaseNode;

/**
 * The colon subscript used to denote that the array size is inferred.
 */
FColonSubscript : FSubscript ::= /SizeExp:FExp/;

/**
 * Expression subscript.
 */
FExpSubscript : FSubscript ::= FExp /Expanded:Array/;

/**
 * A literal integer subscript.
 */
FIntegerSubscript : FSubscript ::= <Value:int>;

/**
 * \brief Classifies a flattened equation as initial or normal.
 */
abstract FEquationType : BaseNode;

/**
 * \brief Classifies a flattened equation as normal.
 */
FNormalEquation : FEquationType;

/**
 * \brief Classifies a flattened equation as initial.
 */
FInitialEquation : FEquationType;

/**
 * \brief Base class for flattened equations.
 *
 * The NTA InitialEquation is used to represent default initial equations for
 * when equations that are not active during initialization.
 */
abstract FAbstractEquation : BaseNode ::= Type:FEquationType
                                          /InitialEquation:FAbstractEquation*/
                                          /[DynamicFAbstractEquation:FAbstractEquation]/
                                          [Name:FQName]
                                          [Residual:FResidual]
                                          [Nominal:FNominal];

/**
 * \brief Unsupported equation.
 */
FUnsupportedEquation : FAbstractEquation;

/**
 * \brief An equation consisting of a right and a left hand side expression.
 */
FEquation : FAbstractEquation ::= Left:FExp Right:FExp;

/**
 * \brief An equation consisting of a left hand side that is a list of component 
 *        references and a right hand side that is a function call.
 *
 * Example: (x, y, z) = func(a, b);
 */
FFunctionCallEquation : FAbstractEquation ::= Left:FFunctionCallLeft* Call:FAbstractFunctionCall;

/**
 * A class for storing hand guided tearing on component level.
 */
FResidual : BaseNode ::= [IterationVariable:FIdUse] <Level:int>;
InstResidual : FResidual ::= [Enabled:FExp];

/**
 * A class for storing hand guided tearing on system level.
 */
FResidualPair : BaseNode ::= ResidualEquation:FIdUse IterationVariable:FIdUse <Level:int>;

/**
 * A class for storing nominal equation values.
 */
FNominal : BaseNode ::= Value:FExp;
InstNominal : FNominal ::= [Enabled:FExp];

/**
 * \brief One of the left hand side variables in a function call equation. 
 *
 * Needed because it is allowed to leave places in the variable list empty.
 * Example: (x, , z) = func(a, b);
 */
FFunctionCallLeft : BaseNode ::= [FExp];

/**
 * \brief A flattened connect statement.
 *
 * Notice that FConnectClauses are not present in flattened models, where
 * connect statements has been transformed into regular equations, but only
 * as an intermediate node type.
 */
FConnectClause : FAbstractEquation ::= [FStringComment]
                                       <ConnectClause:ConnectClause>
                                       Connector1:FIdUseInstAccess 
                                       Connector2:FIdUseInstAccess;

/**
 * Base class for operators concerning overconstrained connection graphs.
 */
abstract FConnectionsOp : FBuiltInFunctionCall ::= A:FExp;

/**
 * Connections.branch() overconstrained connection graph operator.
 */
FConnBranch : FConnectionsOp ::= B:FExp;

/**
 * Connections.root() overconstrained connection graph operator.
 */
FConnRoot : FConnectionsOp;

/**
 * Connections.potentialRoot() overconstrained connection graph operator.
 */
FConnPotentialRoot : FConnectionsOp ::= [Priority:FExp];

/**
 * Base class for isRoot(), Connections.rooted() & rooted().
 */
abstract FConnBoolOp : FConnectionsOp;

/**
 * Connections.isRoot() overconstrained connection graph operator.
 */
FConnIsRoot : FConnBoolOp;

/**
 * Connections.rooted() overconstrained connection graph operator.
 */
FConnRooted : FConnBoolOp;

/**
 * rooted() overconstrained connection graph operator.
 *
 * This is a deprecated variant of Connections.rooted().
 */
FConnRootedDep : FConnRooted;

/**
 * The cardinality() function-like operator.
 */
FCardinality : FUnaryBuiltIn;

/**
 * \brief For equation clause.
 */
FForClauseE : FAbstractEquation ::= FForIndex* FAbstractEquation*;

/**
 * Superclass for when equations, if equations and else clause of if equations.
 */
abstract FIfWhenElseEquation : FAbstractEquation ::= FAbstractEquation*;

/**
 * Superclass for when equations and if equations.
 */
abstract FIfWhenEquation : FIfWhenElseEquation ::= Test:FExp [Else:FIfWhenElseEquation];

/**
 * When equation clause.
 */
FWhenEquation : FIfWhenEquation;

/**
 * If equation clause.
 */
FIfEquation : FIfWhenEquation;

/**
 * Else clause of if equation clause.
 */
FElseEquation : FIfWhenElseEquation;

/**
 * \brief A flattened algorithm clause.
 */
FAlgorithm : FAbstractEquation ::= FStatement*;

/**
 * A flattened derivative algorithm clause.
 * Created when a FAlgorithm is differentiated.
 */
FDerivativeAlgorithm : FAlgorithm ::=  <Order:int>;

/**
 * \brief Base class for statements in flattened algorithms.
 */
abstract FStatement : BaseNode;

/**
 * \brief An assignment statement in the instance tree.
 */
InstAssignStmt : FStatement ::= Left:FIdUseInstAccess Right:FExp;

/**
 * \brief A flattened assignment statement.
 */
FAssignStmt : FStatement ::= Left:FIdUseExp Right:FExp;

/**
 * A flattened derivative assignment statement.
 * Created when a FAssignStmt is differentiated.
 */
FDerivativeAssignStmt : FAssignStmt ::= <Order:int>;

/**
 * \brief A flattened function call statement.
 * 
 * This covers function calls with no used return value or when using 
 * the syntax <code>(vars) := function(args);</code>. Function calls using 
 * exactly one return value are handled by FAssignStmt through FAbstractFunctionCall. 
 * (FAbstractFunctionCall inherits FExp, since it can be used in expressions).
 */
FFunctionCallStmt : FStatement ::= Left:FFunctionCallLeft* Call:FAbstractFunctionCall;

/**
 * A flattened derivative function call statement.
 * Created when a FFunctionCallStmt is differentiated.
 */
FDerivativeFunctionCallStmt : FFunctionCallStmt ::= <Order:int>;

/**
 * \brief Flattened break statement.
 */
FBreakStmt : FStatement;

/**
 * \brief Flattened return statement.
 */
FReturnStmt : FStatement;

/**
 * Class for array initalization statements.
 */
FInitArrayStmt : FStatement ::= FIdUseExp;

/**
 * Class for derivated array initalization statements.
 */
FDerivatedInitArrayStmt : FInitArrayStmt ::= <Order:int>;

/**
 * \brief Base class for flattened if and when statements.
 */
abstract FIfWhenStmt : FStatement ::= FIfWhenClause*;

/**
 * \brief Flattened if statement.
 */
FIfStmt : FIfWhenStmt ::= ElseStmt:FStatement*;

/**
 * \brief Flattened when statement.
 */
FWhenStmt : FIfWhenStmt;

/**
 * \brief Base class for flattened [else]if and [else]when clauses in if/when statements.

 */
abstract FIfWhenClause : BaseNode ::= Test:FExp FStatement*;

/**
 * \brief Flattened [else]if clause in if statement.
 */
FIfClause : FIfWhenClause;

/**
 * \brief Flattened [else]when clause in when statement.
 */
FWhenClause : FIfWhenClause;

/**
 * \brief Flattened for statement.
 */
FForStmt : FStatement ::= Index:FForIndex ForStmt:FStatement*;

/**
 * \brief Flattened while statement.
 */
FWhileStmt : FStatement ::= Test:FExp WhileStmt:FStatement*;

/**
 * \brief An external function interface in the flat tree. Implemented as a statement.
 */
FExternalStmt : FStatement ::= FExternalLanguage [ReturnVar:FIdUse] <Name> Arg:FExp*;

/**
 * \brief A language specification for an external function.
 */
abstract FExternalLanguage : BaseNode;

/**
 * \brief Language specification for "C".
 */
FCExternalLanguage : FExternalLanguage;

/**
 * \brief Language specification for "FORTRAN 77"
 */
FFortran77ExternalLanguage : FExternalLanguage;

/**
 * \brief Language specification for "builtin"
 */
FBuiltinExternalLanguage : FExternalLanguage;

/**
 * \brief Unknown language specification
 */
FUnknownExternalLanguage : FExternalLanguage ::= <Language>;


/**
 * \brief Describes the set of scalarized FExp nodes corresponding to an array expression.
 */
Array : BaseNode ::= FExp*;

/**
 * \brief Dummy array for representing a scalar expression in algorithms handling Arrays. 
 * 
 * Iterator acts as if on a vector of length 1.
 * 
 * The get() and set() methods ignore the Index and always operate on the single element.
 * This allows a ScalarAsArray to be used with an Array in an algorithm that expects 
 * two Array objects, as long as the Indices from a real Array is used to iterate. 
 * 
 * Since the indices of a scalar makes no sense, the Iterator returned by 
 * indices().iterator() will have no elements. 
 * 
 * The Iterator returned by iterator() will have one element.
 */
ScalarAsArray : Array;

/**
 * \brief Abstract base class for all flattened expressions.
 */
abstract FExp : BaseNode ::= /Array/;

/**
 * \brief Abstract base class for all flattened expressions that is not literal or unsupported.
 */
abstract FAbstractExp : FExp;

/**
 * \brief Abstract base class for all flattened scalar expressions.
 */
abstract FAbstractScalarExp : FAbstractExp;

/**
 * \brief Abstract base class for all flattened expressions that can be an array.
 */
abstract FAbstractArrayExp : FAbstractExp;

/**
 * \brief Unsupported expression.
 *
 * Used for reporting errors.
 */
FUnsupportedExp : FExp;

/**
 * \brief Base class for binary expressions.
 */
abstract FBinExp : FAbstractArrayExp ::= Left:FExp Right:FExp;

/**
 * \brief Base class for unary expressions.
 */
abstract FUnaryExp : FAbstractArrayExp ::= FExp;

/**
 * \brief Base class for arithmetic binary expressions.
 */
abstract FArtmBinExp : FBinExp;

/**
 * \brief Element-wise addition expression.
 */
FDotAddExp : FArtmBinExp;

/**
 * \brief Addition expression.
 */
FAddExp : FDotAddExp;

/**
 * String concatenation expression.
 */
FStringAddExp : FDotAddExp;

/**
 * \brief Element-wise subtraction expression.
 */
FDotSubExp : FArtmBinExp;

/**
 * \brief Subtraction expression.
 */
FSubExp : FDotSubExp;

/**
 * \brief Element-wise multiplicative expression.
 */
FDotMulExp : FArtmBinExp;

/**
 * \brief Multiplicative expression.
 */
FMulExp : FDotMulExp;

/**
 * \brief Element-wise division expression.
 */
FDotDivExp : FArtmBinExp;

/**
 * \brief Division expression.
 */
FDivExp : FDotDivExp;

/**
 * \brief Element-wise power expression.
 */
FDotPowExp : FArtmBinExp;

/**
 * \brief Power expression.
 */
FPowExp : FDotPowExp;

/**
 * \brief Unary negation expression.
 */
FNegExp : FUnaryExp;

/**
 * \brief Base class for logical binary expressions.
 */
abstract FBoolBinExp : FBinExp;

/**
 * \brief Base class for relational expressions.
 */
abstract FRelExp : FBoolBinExp ::= /[Indicator:FIdUseExp]/;

/**
 * \brief Less than expression.
 */
FLtExp : FRelExp;

/**
 * \brief Less than or equal expression.
 */
FLeqExp : FRelExp;

/**
 * \brief Greater than expression.
 */
FGtExp : FRelExp;

/**
 * \brief Greater than or equal expression.
 */
FGeqExp : FRelExp;

/**
 * \brief Base class for equality expressions (== and <>).
 */
abstract FEqRelExp : FRelExp;

/**
 * \brief Equality expression.
 */
FEqExp : FEqRelExp;

/**
 * \brief Inequality expression.
 */
FNeqExp : FEqRelExp;

/**
 * \brief Not expression.
 */
FNotExp : FUnaryExp;

/**
 * \brief Base class for binary boolean operators.
 */
abstract FLogBinExp : FBoolBinExp;

/**
 * \brief Or expression.
 */
FOrExp : FLogBinExp;

/**
 * \brief And expression.
 */
FAndExp : FLogBinExp;

/**
 * \brief Base class for literal expressions.
 */
abstract FLitExp : FExp;

/**
 * \brief Real literal expression.
 */
FRealLitExp : FLitExp ::= <Value:double>;

/**
 * \brief Integer literal expression.
 */
FIntegerLitExp : FLitExp ::= <Value:int>;

/**
 * Integer literal expression that is too large to actually fit in an int.
 */
FOverflowIntLitExp : FRealLitExp ::= <String>;

/**
 * \brief Boolean literal expression.
 */
abstract FBooleanLitExp : FLitExp ::=;

/**
 * \brief True boolean literal expression.
 */
FBooleanLitExpTrue : FBooleanLitExp;

/**
 * \brief False boolean literal expression.
 */
FBooleanLitExpFalse : FBooleanLitExp;

/**
 * \brief String literal expression.
 */
FStringLitExp : FLitExp ::= <String>;

/**
 * \brief Enumeration literal expression.
 * 
 * All accesses to enumeration literals should be replaced by FEnumLitExps in flat tree.
 * Note that Type may be <code>null</code> in the flat tree - use type().
 */
FEnumLitExp : FLitExp ::= <Enum> <Value> [Type:FEnumType];


/**
 * Expressions of the form "(exp) for i in (exp)" in instance and flat tree.
 */
FIterExp : FAbstractArrayExp ::= FExp ForIndex:CommonForIndex*;

/**
 * \brief Common subclass for for indices in instance and flat trees.
 */
abstract CommonForIndex : BaseNode ::= [FExp];

/**
 * \brief For indices expression.
 */
FForIndex : CommonForIndex ::= FVariable;

/**
 * \brief If expression.
 */
FIfExp : FAbstractArrayExp ::= IfExp:FExp ThenExp:FExp ElseExp:FExp;

/**
 * \brief Declaration of a qualified name. 
 */
FIdDecl : BaseNode ::= FQName;

/**
 * \brief A qualified identifier.
 */
FIdUse : BaseNode ::= FQName /ExpandedSubscripts:FArraySubscripts/;

/**
 * \brief A qualifed identifier in an expression.
 */
FIdUseExp : FAbstractArrayExp ::= FIdUse;

/**
 * \brief Identifier of temporary variable, used during scalarization
 */
FIdTempExp : FIdUseExp;

/**
 * \brief The built-in function der in the instance tree.
 */
InstDerExp : FUnaryExp; 

/**
 * \brief The built-in function der.
 */

FDerExp : FIdUseExp; 

/**
 * Pre built-in function in the instance tree.
 */
InstPreExp : FUnaryBuiltIn; 

/**
 * Pre built-in function.
 */
FPreExp : FIdUseExp;

/**
 * \brief The built-in function der converted into a dummy derivative.
 */
FDummyDerExp : FIdUseExp; 

/**
 * \brief Higher order derivatives occurring as a result of index reduction.
 */
InstHDerExp : InstDerExp ::= <Order:int>; 

/**
 * \brief Higher order derivatives occurring as a result of index reduction.
 */
FHDerExp : FDerExp ::= <Order:int>; 

/**
 * \brief Higher order derivatives occurring as a result of index reduction, 
 * converted into a dummy derivative.
 */
FHDummyDerExp : FDummyDerExp ::= <Order:int>; 

/**
 * \brief Identifier in the instance tree.
 */
FIdUseInstAccess : FIdUse ::= InstAccess;

/**
 * \brief Identifiers in instantiated expressions.
 *
 * Instantiated expressions occurs in the instance tree.
 */
FInstAccessExp : FAbstractArrayExp ::= InstAccess;

/**
 * \brief Record constructor.
 */
FRecordConstructor : FBuiltInFunctionCall ::= Record:FIdUse Arg:FExp* /Type:FType/;

/**
 * \brief Array constructor expression.
 */
FArray : FAbstractArrayExp ::= FExp*;

/**
 * \brief Long form of array constructor expression.
 */
FLongArray : FArray;

/**
 * \brief Initial node for long form of array constructor expression.
 *
 * Rewritten to FLongArray immediately.
 */
FParseArray : FInfArgsFunctionCall;

/**
 * \brief Abstract super class for all forms of array concatenation expression.
 */
abstract FAbstractCat : FInfArgsFunctionCall;
 
/**
 * \brief Array concatenation expression.
 */
FCatExp : FAbstractCat ::= Dim:FExp;

/**
 * \brief Short form for array concatenation expression.
 */
FMatrix : FAbstractCat;

/**
 * \brief Row in short form for array concatenation expression.
 */
FMatrixRow : FAbstractCat;

/**
 * \brief Generated expression that applies array subscripts to any expression.
 */
FSubscriptedExp : FAbstractArrayExp ::= FExp FArraySubscripts;

/**
 * Generated expression referring to the value of another argument to the same function call.
 */
FArgumentExp : FAbstractArrayExp ::= <Index:int> <Depth:int>;

/**
 * \brief Internal expression that describes an unknown size (for a single dimension).
 * 
 * Only used in Size objects.
 */
FUnknownSizeExp : FAbstractArrayExp ::= <Variable:CommonVariableDecl> <Dim:int>;

/**
 * \brief Dummy node.
 */
FNoExp : FAbstractScalarExp;

/**
 * \brief Range expression.
 */
FRangeExp : FAbstractArrayExp ::= FExp*;

/**
 * \brief Built-in variable time.
 */
FTimeExp : FAbstractScalarExp;

/**
 * \brief Built-in expression 'end', used in arrays.
 */
FEndExp : FAbstractScalarExp;


// Functions and function-like operators
/**
 * \brief Base class for all function calls and function call-like operators.
 */
abstract FAbstractFunctionCall : FAbstractArrayExp;
 
/**
 * \brief Generic function call expression.
 */
FFunctionCall : FAbstractFunctionCall ::= Name:FIdUse Arg:FExp* <Sizes:Size[]>;
 
/**
 * Generic vectorized function call expression.
 */
FVectorFunctionCall : FFunctionCall ::= <VectorSize:Size> <Vectorized:boolean[]>;

/**
 * \brief Generic function call expression in instance trees.
 */
InstFunctionCall : FAbstractFunctionCall ::= Name:InstAccess Arg:InstFunctionArgument*;

/**
 * \brief Partial function call expression in instance trees.
 */
InstPartialFunctionCall : InstFunctionCall;

/**
 * Generic vectorized function call expression in instance trees.
 */
InstVectorFunctionCall : InstFunctionCall ::= <Dims:int>;

/**
 * \brief Base class for function call arguments in the instance tree.
 */
abstract InstFunctionArgument : BaseNode;

/**
 * \brief Positional function call argument in the instance tree.
 */
InstPositionalArgument : InstFunctionArgument ::= <Pos:int> FExp;

/**
 * \brief Named function call argument in the instance tree.
 */
InstNamedArgument : InstFunctionArgument ::= Name:InstAccess FExp;

/**
 * \brief Missing function call argument in the instance tree.
 *  
 * This node indicates a semantic error.
 */
InstMissingArgument : InstFunctionArgument;

/**
 * \brief Function call argument in the instance tree for when several 
 * arguments are bound to the same input parameter.
 *  
 * This node indicates a semantic error.
 */
InstMultipleBoundArgument : InstFunctionArgument ::= Arg:InstFunctionArgument*;

/**
 * \brief Function call argument using default value in the instance tree.
 */
InstDefaultArgument : InstFunctionArgument ::= <FExp:FExp>;

// Built in functions
/**
 * \brief Base class for calls to all built-in functions. 
 *
 * OriginalArg list is only used for error checking and is empty in the flattened tree.
 */
abstract FBuiltInFunctionCall : FAbstractFunctionCall ::= /OriginalArg:InstFunctionArgument*/;

/**
 * \brief Base class for calls to built-in unary functions.
 */
abstract FUnaryBuiltIn : FBuiltInFunctionCall ::= FExp;

/**
 * Base class for calls to vectorizeable unary functions.
 *
 * Methods on this class assumes that subclasses adhere to MLS v3.2 section 12.4.6.
 */
abstract FVectUnaryBuiltIn : FUnaryBuiltIn;

/**
 * \brief Base class for calls to built-in functions with no arguments.
 */
abstract FNoArgBuiltIn : FBuiltInFunctionCall;

/**
 * \brief noEvent built-in function.
 */
FNoEventExp : FVectUnaryBuiltIn;

/**
 * \brief smooth built-in function.
 * 
 * Handled like noEvent(), but with a warning. To be implemented properly later.
 */
FSmoothExp : FBuiltInFunctionCall ::= Order:FExp FExp;

/**
 * Edge built-in function.
 */
FEdgeExp : FUnaryBuiltIn;

/**
 * Change built-in function.
 */
FChangeExp : FUnaryBuiltIn;

/**
 * Reinit built-in function.
 */
FReinit : FBuiltInFunctionCall ::= Var:FExp FExp;

/**
 * Sample built-in function.
 */
FSampleExp : FBuiltInFunctionCall ::= Offset:FExp Interval:FExp;

/**
 * Built-in functions that return flags describing the simulation state.
 */
abstract FSimulationStateBuiltIn : FNoArgBuiltIn;

/**
 * Initial built-in function.
 */
FInitialExp : FSimulationStateBuiltIn;

/**
 * Terminal built-in function.
 */
FTerminalExp : FSimulationStateBuiltIn;

/**
 * Terminate built-in function.
 */
FTerminate : FUnaryBuiltIn;

/**
 * Assert built-in function.
 */
FAssert : FBuiltInFunctionCall ::= Test:FExp Msg:FExp [Level:FExp];

/**
 * \brief Abs built-in function.
 */
FAbsExp : FVectUnaryBuiltIn;

/**
 * \brief Sign built-in function.
 */
FSignExp : FVectUnaryBuiltIn;

/**
 * \brief Sqrt built-in function.
 */
FSqrtExp : FMathematicalFunctionCall;

/**
 * \brief The Integer() built-in function (convert enumeration value to Integer).
 */
FEnumIntegerExp : FVectUnaryBuiltIn;

/**
 * \brief Abstract base class for event generating expressions.
 */
abstract FEventGenExp : FBuiltInFunctionCall ::= X:FExp;

/**
 * \brief Abstract base class for binary event generating expressions.
 */
abstract FBinEventGenExp : FEventGenExp ::= Y:FExp;

/**
 * \brief The div() built-in function.
 */
FDivFuncExp : FBinEventGenExp;

/**
 * \brief The mod() built-in function.
 */
FModFuncExp : FBinEventGenExp;

/**
 * \brief The rem() built-in function.
 */
FRemFuncExp : FBinEventGenExp;

/**
 * \brief The floor() built-in function.
 */
FFloorFuncExp : FEventGenExp;

/**
 * \brief The ceil() built-in function.
 */
FCeilFuncExp : FEventGenExp;

/**
 * \brief The integer() built-in function (convert Real to Integer).
 */
FIntegerFuncExp : FFloorFuncExp;

/**
 * \brief Base class for calls to built-in mathematical functions.
 */
abstract FMathematicalFunctionCall : FBuiltInFunctionCall ::= FExp;

/**
 * \brief Sin built-in function.
 */
FSinExp : FMathematicalFunctionCall;

/**
 * \brief Cos built-in function.
 */
FCosExp : FMathematicalFunctionCall;

/**
 * \brief Tan built-in function.
 */
FTanExp : FMathematicalFunctionCall;

/**
 * \brief Asin built-in function.
 */
FAsinExp : FMathematicalFunctionCall;

/**
 * \brief ACos built-in function.
 */
FAcosExp : FMathematicalFunctionCall;

/**
 * \brief ATan built-in function.
 */
FAtanExp : FMathematicalFunctionCall;

/**
 * \brief Atan2 built-in function.
 */
FAtan2Exp : FMathematicalFunctionCall ::= Y:FExp;

/**
 * \brief Sinh built-in function.
 */
FSinhExp : FMathematicalFunctionCall;

/**
 * \brief Cosh built-in function.
 */
FCoshExp : FMathematicalFunctionCall;

/**
 * \brief Tanh built-in function.
 */
FTanhExp : FMathematicalFunctionCall;

/**
 * \brief Exp built-in function.
 */
FExpExp : FMathematicalFunctionCall;

/**
 * \brief Log built-in function.
 */
FLogExp : FMathematicalFunctionCall;

/**
 * \brief Log10 built-in function.
 */
FLog10Exp : FMathematicalFunctionCall;

/**
 * \brief Ndims built-in function.
 */
FNdimsExp : FUnaryBuiltIn;

/**
 * \brief Size built-in function.
 */
FSizeExp : FBuiltInFunctionCall ::= FExp [Dim:FExp];

/**
 * Abstract base class for dimension conversion operators (scalar(), vector(), matrix()).
 */
abstract FDimensionConvert : FUnaryBuiltIn;

/**
 * Scalar built-in function.
 */
FScalarExp : FDimensionConvert;

/**
 * Vector built-in function.
 */
FVectorExp : FDimensionConvert;

/**
 * Vector built-in function.
 */
FMatrixExp : FDimensionConvert;

/**
 * \brief Transpose built-in function.
 */
FTranspose : FUnaryBuiltIn;

/**
 * \brief Symmetric built-in function.
 */
FSymmetric : FUnaryBuiltIn;

/**
 * \brief Cross built-in function.
 */
FCross : FBuiltInFunctionCall ::= X:FExp Y:FExp;

/**
 * \brief Skew built-in function.
 */
FSkew : FUnaryBuiltIn;

/**
 * \brief Identity built-in function.
 */
FIdentity : FUnaryBuiltIn;

/**
 * Diagonal built-in function.
 */
FDiagonal : FUnaryBuiltIn;

/**
 * Outer product built-in function.
 */
FOuterProduct : FBuiltInFunctionCall ::= X:FExp Y:FExp;

/**
 * \brief Identity built-in function.
 */
FLinspace : FBuiltInFunctionCall ::= StartExp:FExp StopExp:FExp N:FExp;

/**
 * \brief Base class for function call-like operators with no maximum number of arguments.
 */
abstract FInfArgsFunctionCall : FBuiltInFunctionCall ::= FExp*;

/**
 * Base class for operators that creates an array with dimensions given as arguments, e.g. ones().
 */
abstract FArrayDimAsArgsExp : FInfArgsFunctionCall;

/**
 * \brief Ones built-in function.
 */
FOnes : FArrayDimAsArgsExp;

/**
 * \brief Zeros built-in function.
 */
FZeros : FArrayDimAsArgsExp;

/**
 * \brief Fill built-in function.
 */
FFillExp : FArrayDimAsArgsExp ::= FillExp:FExp;

/**
 * \brief Common base class for FMinExp & FMaxExp.
 */
abstract FMinMaxExp : FBuiltInFunctionCall ::= X:FExp [Y:FExp];

/**
 * String conversion operator.
 */
FStringExp : FBuiltInFunctionCall ::= Value:FExp [MinimumLength:FExp] [LeftJustified:FExp] [SignificantDigits:FExp] [Format:FExp];

/**
 * \brief Min built-in function.
 */
FMinExp : FMinMaxExp;

/**
 * \brief Max built-in function.
 */
FMaxExp : FMinMaxExp;

/**
 * \brief Abstract base class for reduction expressions (sum(), product())
 */
abstract FReductionExp : FUnaryBuiltIn;

/**
 * \brief Sum built-in function.
 */
FSumExp : FReductionExp;

/**
 * \brief Product built-in function.
 */
FProductExp : FReductionExp;

/**
 * \brief SemiLinear built-in function.
 */
FHomotopyExp : FBuiltInFunctionCall ::= Actual:FExp Simplified:FExp;

/**
 * \brief SemiLinear built-in function.
 */
FSemiLinearExp : FBuiltInFunctionCall ::= X:FExp PosSlope:FExp NegSlope:FExp;

/**
 * \brief Common base class for FInStream & FActualStream
 */
abstract FStreamBuiltIn : FVectUnaryBuiltIn;

/**
 * \brief InStream built-in function.
 */
FInStream : FStreamBuiltIn;

/**
 * \brief ActualStream built-in function.
 */
FActualStream : FStreamBuiltIn;

/**
 * \brief getInstanceName built-in function.
 */
FGetInstanceName : FNoArgBuiltIn;


/**
 * \brief Dummy node for unsupported built-in functions.
 */
FUnsupportedBuiltIn : FBuiltInFunctionCall ::= <Name>;

/**
 * \brief Dummy node for built-in functions that are ignored with a warning.
 */
FIgnoredBuiltIn : FUnsupportedBuiltIn;
